using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace Rename2
{
    public partial class Rename2 : Form
    {
        public Rename2()
        {
            InitializeComponent();
        }

        private void b�ffnen_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                tbVerzeichnis.Text = folderBrowserDialog1.SelectedPath;
                UpdatelvVerzeichnis(folderBrowserDialog1.SelectedPath);
            }

            lvQuelle.Items.Clear();
        }

        void UpdatelvVerzeichnis(string path)
        {
            DirectoryInfo di = new DirectoryInfo(path);

            if (di.Exists)
            {
                lvVerzeichnis.Enabled = true;
                lvVerzeichnis.Items.Clear();
                FileInfo[] fiArr = di.GetFiles();

                foreach (FileInfo fi in fiArr)
                {
                    lvVerzeichnis.Items.Add(fi.Name);
                    lvVerzeichnis.Items[lvVerzeichnis.Items.Count - 1].SubItems.Add(Convert.ToString(fi.Length));
                }
            }
            else
            {
                lvVerzeichnis.Items.Clear();
                lvVerzeichnis.Items.Add("Kann Ordner nicht finden");
                lvVerzeichnis.Enabled = false;
            }
        }

        private void cbFilter_CheckedChanged(object sender, EventArgs e)
        {
            tbFilter.Enabled = cbFilter.Checked;
        }

        private void b�bertragen_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem i in lvVerzeichnis.SelectedItems)
            {
                lvVerzeichnis.Items.Remove(i);
                lvQuelle.Items.Add(i);
            }
        }

        private void bAlle�bertragen_Click(object sender, EventArgs e)
        {
            if (cbFilter.Checked && tbFilter.Text != "")
            {
                foreach (ListViewItem i in lvVerzeichnis.Items)
                {
                    if (i.Text.IndexOf(tbFilter.Text) != -1)
                    {
                        lvVerzeichnis.Items.Remove(i);
                        lvQuelle.Items.Add(i);
                    }
                }
            }
            else
            {
                foreach (ListViewItem i in lvVerzeichnis.Items)
                {
                    lvVerzeichnis.Items.Remove(i);
                    lvQuelle.Items.Add(i);
                }
            }
        }

        private void bAlleEntfernen_Click(object sender, EventArgs e)
        {
            if (cbFilter.Checked && tbFilter.Text != "")
            {
                foreach (ListViewItem i in lvQuelle.Items)
                {
                    if (i.Text.IndexOf(tbFilter.Text) != -1)
                    {
                        lvQuelle.Items.Remove(i);
                        lvVerzeichnis.Items.Add(i);
                    }
                }
            }
            else
            {
                foreach (ListViewItem i in lvQuelle.Items)
                {
                    lvQuelle.Items.Remove(i);
                    lvVerzeichnis.Items.Add(i);
                }
            }
        }

        private void bEntfernen_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem i in lvQuelle.SelectedItems)
            {
                lvQuelle.Items.Remove(i);
                lvVerzeichnis.Items.Add(i);
            }
        }

        private void bNachOben_Click(object sender, EventArgs e)
        {
            //Erstellt ein ListViewItem Array um die selektierten ListViewItems
            //aufnehmen zu k�nnen
            ListViewItem[] SelectedItemsArr = new ListViewItem[lvQuelle.SelectedItems.Count];
            //Erstellt ein weiteres ListViewItem Arry f�r alle List View Items nach
            //der Selektion mit entsprechender Gr��e (Gesamtzahl aller Items minus
            //dem Index des Letztmarkierten Eintrags)
            ListViewItem[] AfterItemsArr = new ListViewItem[lvQuelle.Items.Count
                                                            - lvQuelle.SelectedItems[lvQuelle.SelectedItems.Count - 1].Index - 1];

            //Kopiert alle markierten Items in das entsprehende Array
            lvQuelle.SelectedItems.CopyTo(SelectedItemsArr, 0);

            //Kopiert alle Items nach der Selektion in das entsprechende Array
            foreach (ListViewItem i in lvQuelle.Items)
            {
                if (i.Index > lvQuelle.SelectedItems[lvQuelle.SelectedItems.Count - 1].Index)
                {
                    //Subtrahiert den Index des ersten unmarkierten Eintrags vom aktuellen Index um die
                    //richtige Position im Array zu erhalten
                    AfterItemsArr[i.Index - lvQuelle.SelectedItems[lvQuelle.SelectedItems.Count - 1].Index - 1] = i;
                }
            }

            //L�scht alle Eintr�ge nach der Selektion aus dem ListView
            foreach (ListViewItem i in lvQuelle.Items)
            {
                if (i.Index > lvQuelle.SelectedItems[lvQuelle.SelectedItems.Count - 1].Index)
                {
                    i.Remove();
                }
            }

            //L�scht alle markierten Eintr�ge
            foreach (ListViewItem i in lvQuelle.Items)
            {
                if (i.Selected)
                {
                    i.Remove();
                }
            }

            //Erstellt ein Tempor�res Item und kopiert das letzte Item des momentanen ListViews
            //rein um das Item dann aus dem ListView zu l�schen
            ListViewItem TempItem = lvQuelle.Items[lvQuelle.Items.Count - 1];
            lvQuelle.Items[lvQuelle.Items.Count - 1].Remove();

            //F�gt dem ListView alle Eintr�ge aus dem Array mit den Selektierten Items hinzu
            foreach (ListViewItem i in SelectedItemsArr)
            {
                lvQuelle.Items.Add(i);
            }

            //H�ngt das TempItem an
            lvQuelle.Items.Add(TempItem);

            //F�gt alle Eintr�ge des Arrays mit Items nach der anf�nglichen Selektion ein
            foreach (ListViewItem i in AfterItemsArr)
            {
                lvQuelle.Items.Add(i);
            }

            bNachUnten.Enabled = true;
            if (lvQuelle.SelectedItems[0].Index == 0)
            {
                bNachOben.Enabled = false;
            }
            else
            {
                bNachOben.Enabled = true;
            }
        }

        private void lvQuelle_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lvQuelle.SelectedItems.Count == 0)
            {
                bNachOben.Enabled = false;
                bNachUnten.Enabled = false;
            }
            else
            {
                if (lvQuelle.SelectedItems[0].Index == 0)
                {
                    bNachOben.Enabled = false;
                }
                else
                {
                    bNachOben.Enabled = true;
                }

                if (lvQuelle.SelectedItems[lvQuelle.SelectedItems.Count - 1].Index == lvQuelle.Items.Count - 1)
                {
                    bNachUnten.Enabled = false;
                }
                else
                {
                    bNachUnten.Enabled = true;
                }
            }
        }

        private void bNachUnten_Click(object sender, EventArgs e)
        {
            ListViewItem[] SelectedItemsArr = new ListViewItem[lvQuelle.SelectedItems.Count];
            ListViewItem[] AfterItemsArr = new ListViewItem[lvQuelle.Items.Count
                                                            - lvQuelle.SelectedItems[lvQuelle.SelectedItems.Count - 1].Index - 2];

            lvQuelle.SelectedItems.CopyTo(SelectedItemsArr, 0);

            ListViewItem TempItem = lvQuelle.Items[lvQuelle.SelectedItems[lvQuelle.SelectedItems.Count - 1].Index + 1];
            lvQuelle.Items[lvQuelle.SelectedItems[lvQuelle.SelectedItems.Count - 1].Index + 1].Remove();

            foreach (ListViewItem i in lvQuelle.Items)
            {
                if (i.Index > lvQuelle.SelectedItems[lvQuelle.SelectedItems.Count - 1].Index)
                {
                    AfterItemsArr[i.Index - lvQuelle.SelectedItems[lvQuelle.SelectedItems.Count - 1].Index - 1] = i;
                }
            }

            foreach (ListViewItem i in lvQuelle.Items)
            {
                if (i.Index > lvQuelle.SelectedItems[lvQuelle.SelectedItems.Count - 1].Index)
                {
                    i.Remove();
                }
            }

            foreach (ListViewItem i in lvQuelle.Items)
            {
                if (i.Selected)
                {
                    i.Remove();
                }
            }
			
			lvQuelle.Items.Add(TempItem);

            foreach (ListViewItem i in SelectedItemsArr)
            {
                lvQuelle.Items.Add(i);
            }

            foreach (ListViewItem i in AfterItemsArr)
            {
                lvQuelle.Items.Add(i);
            }

            bNachOben.Enabled = true;
            if (lvQuelle.SelectedItems[lvQuelle.SelectedItems.Count - 1].Index == lvQuelle.Items.Count - 1)
            {
                bNachUnten.Enabled = false;
            }
            else
            {
                bNachUnten.Enabled = true;
            }
        }

        private void bAktualisieren_Click(object sender, EventArgs e)
        {
            if (tbVerzeichnis.Text != "")
            {
                UpdatelvVerzeichnis(tbVerzeichnis.Text);
                lvQuelle.Items.Clear();
            }
        }

        private void nudVon_ValueChanged(object sender, EventArgs e)
        {
            nudBis.Value = lvQuelle.Items.Count + nudVon.Value - 1;
            if (nudVon.Value.ToString().Length == 1)
            {
                lVorschau.Text = tbVorne.Text + "0" + nudVon.Value.ToString() + tbHinten.Text;
            }
            else
            {
                lVorschau.Text = tbVorne.Text + nudVon.Value.ToString() + tbHinten.Text;
            }
        }

        private void bTesten_Click(object sender, EventArgs e)
        {
            lvZiel.Items.Clear();

            foreach(ListViewItem i in lvQuelle.Items)
            {
                ListViewItem Temp = (ListViewItem)i.Clone();
                lvZiel.Items.Add(Temp);
            }

            for (int i = 0; i < lvZiel.Items.Count; i++)
            {
                string temp = (nudVon.Value + i).ToString();
                if (temp.Length == 1)
                {
                    lvZiel.Items[i].SubItems[1].Text = lvZiel.Items[i].SubItems[0].Text;
                    lvZiel.Items[i].SubItems[0].Text = tbVorne.Text + "0" + temp + tbHinten.Text;
                }
                else
                {
                    lvZiel.Items[i].SubItems[1].Text = lvZiel.Items[i].SubItems[0].Text;
                    lvZiel.Items[i].SubItems[0].Text = tbVorne.Text + temp + tbHinten.Text;
                }
            }
        }

        private void bUmbenennen_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem i in lvZiel.Items)
            {
                FileInfo fi = new FileInfo(tbVerzeichnis.Text + "\\" + i.SubItems[1].Text);
                DirectoryInfo di = new DirectoryInfo(tbVerzeichnis.Text);
                if (di.GetFiles(i.SubItems[0].Text).Length == 0)
                {
                    fi.MoveTo(tbVerzeichnis.Text + "\\"+ i.SubItems[0].Text);
                }
                else
                {
                    MessageBox.Show("hiho");
                }
            }

            lvQuelle.Items.Clear();
            lvZiel.Items.Clear();
            UpdatelvVerzeichnis(tbVerzeichnis.Text);
        }

        private void tbVorne_TextChanged(object sender, EventArgs e)
        {
            if (nudVon.Value.ToString().Length == 1)
            {
                lVorschau.Text = tbVorne.Text + "0" + nudVon.Value.ToString() + tbHinten.Text;
            }
            else
            {
                lVorschau.Text = tbVorne.Text + nudVon.Value.ToString() + tbHinten.Text;
            }
        }

        private void tbHinten_TextChanged(object sender, EventArgs e)
        {
            if (nudVon.Value.ToString().Length == 1)
            {
                lVorschau.Text = tbVorne.Text + "0" + nudVon.Value.ToString() + tbHinten.Text;
            }
            else
            {
                lVorschau.Text = tbVorne.Text + nudVon.Value.ToString() + tbHinten.Text;
            }
        }
    }
}