namespace Rename2
{
    partial class Rename2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Button bTesten;
            this.lvVerzeichnis = new System.Windows.Forms.ListView();
            this.chName = new System.Windows.Forms.ColumnHeader();
            this.chGr��e = new System.Windows.Forms.ColumnHeader();
            this.tbVerzeichnis = new System.Windows.Forms.TextBox();
            this.bAktualisieren = new System.Windows.Forms.Button();
            this.b�ffnen = new System.Windows.Forms.Button();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.lvQuelle = new System.Windows.Forms.ListView();
            this.chQuelleName = new System.Windows.Forms.ColumnHeader();
            this.b�bertragen = new System.Windows.Forms.Button();
            this.bAlle�bertragen = new System.Windows.Forms.Button();
            this.cbFilter = new System.Windows.Forms.CheckBox();
            this.tbFilter = new System.Windows.Forms.TextBox();
            this.bAlleEntfernen = new System.Windows.Forms.Button();
            this.bEntfernen = new System.Windows.Forms.Button();
            this.bNachOben = new System.Windows.Forms.Button();
            this.bNachUnten = new System.Windows.Forms.Button();
            this.lvZiel = new System.Windows.Forms.ListView();
            this.chZielName = new System.Windows.Forms.ColumnHeader();
            this.chZielUmbenannt = new System.Windows.Forms.ColumnHeader();
            this.tbVorne = new System.Windows.Forms.TextBox();
            this.nudVon = new System.Windows.Forms.NumericUpDown();
            this.nudBis = new System.Windows.Forms.NumericUpDown();
            this.tbHinten = new System.Windows.Forms.TextBox();
            this.lVorschau = new System.Windows.Forms.Label();
            this.bUmbenennen = new System.Windows.Forms.Button();
            bTesten = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.nudVon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBis)).BeginInit();
            this.SuspendLayout();
            // 
            // bTesten
            // 
            bTesten.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            bTesten.Location = new System.Drawing.Point(590, 397);
            bTesten.Name = "bTesten";
            bTesten.Size = new System.Drawing.Size(75, 23);
            bTesten.TabIndex = 19;
            bTesten.Text = "Testen";
            bTesten.Click += new System.EventHandler(this.bTesten_Click);
            // 
            // lvVerzeichnis
            // 
            this.lvVerzeichnis.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lvVerzeichnis.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.chName,
            this.chGr��e});
            this.lvVerzeichnis.Enabled = false;
            this.lvVerzeichnis.Location = new System.Drawing.Point(12, 12);
            this.lvVerzeichnis.Name = "lvVerzeichnis";
            this.lvVerzeichnis.Size = new System.Drawing.Size(223, 310);
            this.lvVerzeichnis.TabIndex = 0;
            this.lvVerzeichnis.View = System.Windows.Forms.View.Details;
            // 
            // chName
            // 
            this.chName.Text = "Name";
            this.chName.Width = 125;
            // 
            // chGr��e
            // 
            this.chGr��e.Text = "Gr��e";
            this.chGr��e.Width = 85;
            // 
            // tbVerzeichnis
            // 
            this.tbVerzeichnis.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbVerzeichnis.Enabled = false;
            this.tbVerzeichnis.Location = new System.Drawing.Point(12, 328);
            this.tbVerzeichnis.Name = "tbVerzeichnis";
            this.tbVerzeichnis.Size = new System.Drawing.Size(223, 20);
            this.tbVerzeichnis.TabIndex = 1;
            // 
            // bAktualisieren
            // 
            this.bAktualisieren.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bAktualisieren.Location = new System.Drawing.Point(79, 354);
            this.bAktualisieren.Name = "bAktualisieren";
            this.bAktualisieren.Size = new System.Drawing.Size(75, 23);
            this.bAktualisieren.TabIndex = 2;
            this.bAktualisieren.Text = "Aktualisieren";
            this.bAktualisieren.Click += new System.EventHandler(this.bAktualisieren_Click);
            // 
            // b�ffnen
            // 
            this.b�ffnen.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.b�ffnen.Location = new System.Drawing.Point(160, 354);
            this.b�ffnen.Name = "b�ffnen";
            this.b�ffnen.Size = new System.Drawing.Size(75, 23);
            this.b�ffnen.TabIndex = 3;
            this.b�ffnen.Text = "�ffnen";
            this.b�ffnen.Click += new System.EventHandler(this.b�ffnen_Click);
            // 
            // folderBrowserDialog1
            // 
            this.folderBrowserDialog1.Description = "Hier den Ordner ausw�hlen in dem sich die umzubenennenden Dateien befinden:";
            this.folderBrowserDialog1.RootFolder = System.Environment.SpecialFolder.MyComputer;
            this.folderBrowserDialog1.SelectedPath = "folderBrowserDialog1";
            this.folderBrowserDialog1.ShowNewFolderButton = false;
            // 
            // lvQuelle
            // 
            this.lvQuelle.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lvQuelle.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.chQuelleName});
            this.lvQuelle.HideSelection = false;
            this.lvQuelle.Location = new System.Drawing.Point(354, 12);
            this.lvQuelle.Name = "lvQuelle";
            this.lvQuelle.Size = new System.Drawing.Size(154, 310);
            this.lvQuelle.TabIndex = 4;
            this.lvQuelle.View = System.Windows.Forms.View.Details;
            this.lvQuelle.SelectedIndexChanged += new System.EventHandler(this.lvQuelle_SelectedIndexChanged);
            // 
            // chQuelleName
            // 
            this.chQuelleName.Text = "Name";
            this.chQuelleName.Width = 143;
            // 
            // b�bertragen
            // 
            this.b�bertragen.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.b�bertragen.Location = new System.Drawing.Point(241, 69);
            this.b�bertragen.Name = "b�bertragen";
            this.b�bertragen.Size = new System.Drawing.Size(107, 23);
            this.b�bertragen.TabIndex = 5;
            this.b�bertragen.Text = "�bertragen >";
            this.b�bertragen.Click += new System.EventHandler(this.b�bertragen_Click);
            // 
            // bAlle�bertragen
            // 
            this.bAlle�bertragen.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bAlle�bertragen.Location = new System.Drawing.Point(241, 98);
            this.bAlle�bertragen.Name = "bAlle�bertragen";
            this.bAlle�bertragen.Size = new System.Drawing.Size(107, 23);
            this.bAlle�bertragen.TabIndex = 6;
            this.bAlle�bertragen.Text = "Alle �bertragen >>";
            this.bAlle�bertragen.Click += new System.EventHandler(this.bAlle�bertragen_Click);
            // 
            // cbFilter
            // 
            this.cbFilter.AutoSize = true;
            this.cbFilter.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbFilter.Location = new System.Drawing.Point(241, 127);
            this.cbFilter.Name = "cbFilter";
            this.cbFilter.Size = new System.Drawing.Size(86, 17);
            this.cbFilter.TabIndex = 7;
            this.cbFilter.Text = "(Filter) Enth�lt:";
            this.cbFilter.CheckedChanged += new System.EventHandler(this.cbFilter_CheckedChanged);
            // 
            // tbFilter
            // 
            this.tbFilter.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbFilter.Enabled = false;
            this.tbFilter.Location = new System.Drawing.Point(241, 150);
            this.tbFilter.Name = "tbFilter";
            this.tbFilter.Size = new System.Drawing.Size(107, 20);
            this.tbFilter.TabIndex = 8;
            // 
            // bAlleEntfernen
            // 
            this.bAlleEntfernen.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bAlleEntfernen.Location = new System.Drawing.Point(241, 176);
            this.bAlleEntfernen.Name = "bAlleEntfernen";
            this.bAlleEntfernen.Size = new System.Drawing.Size(107, 23);
            this.bAlleEntfernen.TabIndex = 9;
            this.bAlleEntfernen.Text = "<< Alle entfernen";
            this.bAlleEntfernen.Click += new System.EventHandler(this.bAlleEntfernen_Click);
            // 
            // bEntfernen
            // 
            this.bEntfernen.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bEntfernen.Location = new System.Drawing.Point(241, 205);
            this.bEntfernen.Name = "bEntfernen";
            this.bEntfernen.Size = new System.Drawing.Size(107, 23);
            this.bEntfernen.TabIndex = 10;
            this.bEntfernen.Text = "< Entfernen";
            this.bEntfernen.Click += new System.EventHandler(this.bEntfernen_Click);
            // 
            // bNachOben
            // 
            this.bNachOben.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bNachOben.Location = new System.Drawing.Point(514, 12);
            this.bNachOben.Name = "bNachOben";
            this.bNachOben.Size = new System.Drawing.Size(70, 23);
            this.bNachOben.TabIndex = 11;
            this.bNachOben.Text = "nach oben";
            this.bNachOben.Click += new System.EventHandler(this.bNachOben_Click);
            // 
            // bNachUnten
            // 
            this.bNachUnten.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bNachUnten.Location = new System.Drawing.Point(514, 41);
            this.bNachUnten.Name = "bNachUnten";
            this.bNachUnten.Size = new System.Drawing.Size(70, 23);
            this.bNachUnten.TabIndex = 12;
            this.bNachUnten.Text = "nach unten";
            this.bNachUnten.Click += new System.EventHandler(this.bNachUnten_Click);
            // 
            // lvZiel
            // 
            this.lvZiel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lvZiel.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.chZielUmbenannt,
            this.chZielName});
            this.lvZiel.FullRowSelect = true;
            this.lvZiel.LabelEdit = true;
            this.lvZiel.Location = new System.Drawing.Point(590, 12);
            this.lvZiel.MultiSelect = false;
            this.lvZiel.Name = "lvZiel";
            this.lvZiel.Size = new System.Drawing.Size(367, 310);
            this.lvZiel.TabIndex = 13;
            this.lvZiel.View = System.Windows.Forms.View.Details;
            // 
            // chZielName
            // 
            this.chZielName.Text = "Alter Name";
            this.chZielName.Width = 189;
            // 
            // chZielUmbenannt
            // 
            this.chZielUmbenannt.Text = "Umbenannt";
            this.chZielUmbenannt.Width = 165;
            // 
            // tbVorne
            // 
            this.tbVorne.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbVorne.Location = new System.Drawing.Point(590, 340);
            this.tbVorne.Name = "tbVorne";
            this.tbVorne.Size = new System.Drawing.Size(194, 20);
            this.tbVorne.TabIndex = 14;
            this.tbVorne.TextChanged += new System.EventHandler(this.tbVorne_TextChanged);
            // 
            // nudVon
            // 
            this.nudVon.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nudVon.Location = new System.Drawing.Point(790, 328);
            this.nudVon.Maximum = new decimal(new int[] {
            99,
            0,
            0,
            0});
            this.nudVon.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudVon.Name = "nudVon";
            this.nudVon.Size = new System.Drawing.Size(52, 20);
            this.nudVon.TabIndex = 15;
            this.nudVon.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudVon.ValueChanged += new System.EventHandler(this.nudVon_ValueChanged);
            // 
            // nudBis
            // 
            this.nudBis.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nudBis.Location = new System.Drawing.Point(790, 354);
            this.nudBis.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.nudBis.Name = "nudBis";
            this.nudBis.ReadOnly = true;
            this.nudBis.Size = new System.Drawing.Size(52, 20);
            this.nudBis.TabIndex = 16;
            // 
            // tbHinten
            // 
            this.tbHinten.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbHinten.Location = new System.Drawing.Point(848, 340);
            this.tbHinten.Name = "tbHinten";
            this.tbHinten.Size = new System.Drawing.Size(106, 20);
            this.tbHinten.TabIndex = 17;
            this.tbHinten.TextChanged += new System.EventHandler(this.tbHinten_TextChanged);
            // 
            // lVorschau
            // 
            this.lVorschau.AutoSize = true;
            this.lVorschau.Location = new System.Drawing.Point(589, 381);
            this.lVorschau.Name = "lVorschau";
            this.lVorschau.Size = new System.Drawing.Size(51, 13);
            this.lVorschau.TabIndex = 18;
            this.lVorschau.Text = "Vorschau:";
            // 
            // bUmbenennen
            // 
            this.bUmbenennen.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bUmbenennen.Location = new System.Drawing.Point(671, 397);
            this.bUmbenennen.Name = "bUmbenennen";
            this.bUmbenennen.Size = new System.Drawing.Size(90, 23);
            this.bUmbenennen.TabIndex = 20;
            this.bUmbenennen.Text = "Umbenennen!";
            this.bUmbenennen.Click += new System.EventHandler(this.bUmbenennen_Click);
            // 
            // Rename2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(969, 429);
            this.Controls.Add(this.bUmbenennen);
            this.Controls.Add(bTesten);
            this.Controls.Add(this.lVorschau);
            this.Controls.Add(this.tbHinten);
            this.Controls.Add(this.nudBis);
            this.Controls.Add(this.nudVon);
            this.Controls.Add(this.tbVorne);
            this.Controls.Add(this.lvZiel);
            this.Controls.Add(this.bNachUnten);
            this.Controls.Add(this.bNachOben);
            this.Controls.Add(this.bEntfernen);
            this.Controls.Add(this.bAlleEntfernen);
            this.Controls.Add(this.tbFilter);
            this.Controls.Add(this.cbFilter);
            this.Controls.Add(this.bAlle�bertragen);
            this.Controls.Add(this.b�bertragen);
            this.Controls.Add(this.lvQuelle);
            this.Controls.Add(this.b�ffnen);
            this.Controls.Add(this.bAktualisieren);
            this.Controls.Add(this.tbVerzeichnis);
            this.Controls.Add(this.lvVerzeichnis);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Rename2";
            this.Text = "Rename2";
            ((System.ComponentModel.ISupportInitialize)(this.nudVon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBis)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView lvVerzeichnis;
        private System.Windows.Forms.ColumnHeader chName;
        private System.Windows.Forms.ColumnHeader chGr��e;
        private System.Windows.Forms.TextBox tbVerzeichnis;
        private System.Windows.Forms.Button bAktualisieren;
        private System.Windows.Forms.Button b�ffnen;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.ListView lvQuelle;
        private System.Windows.Forms.ColumnHeader chQuelleName;
        private System.Windows.Forms.Button b�bertragen;
        private System.Windows.Forms.Button bAlle�bertragen;
        private System.Windows.Forms.CheckBox cbFilter;
        private System.Windows.Forms.TextBox tbFilter;
        private System.Windows.Forms.Button bAlleEntfernen;
        private System.Windows.Forms.Button bEntfernen;
        private System.Windows.Forms.Button bNachOben;
        private System.Windows.Forms.Button bNachUnten;
        private System.Windows.Forms.ListView lvZiel;
        private System.Windows.Forms.ColumnHeader chZielName;
        private System.Windows.Forms.ColumnHeader chZielUmbenannt;
        private System.Windows.Forms.TextBox tbVorne;
        private System.Windows.Forms.NumericUpDown nudVon;
        private System.Windows.Forms.NumericUpDown nudBis;
        private System.Windows.Forms.TextBox tbHinten;
        private System.Windows.Forms.Label lVorschau;
        private System.Windows.Forms.Button bUmbenennen;

    }
}

