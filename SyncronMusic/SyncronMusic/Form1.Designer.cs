namespace SyncronMusic
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbIP = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.bFile = new System.Windows.Forms.Button();
            this.tbPath = new System.Windows.Forms.TextBox();
            this.rbServer = new System.Windows.Forms.RadioButton();
            this.rbClient = new System.Windows.Forms.RadioButton();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.lZeit = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // tbIP
            // 
            this.tbIP.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbIP.Location = new System.Drawing.Point(51, 12);
            this.tbIP.Name = "tbIP";
            this.tbIP.Size = new System.Drawing.Size(134, 20);
            this.tbIP.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(33, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Ziel-IP";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // bFile
            // 
            this.bFile.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bFile.Location = new System.Drawing.Point(189, 69);
            this.bFile.Name = "bFile";
            this.bFile.Size = new System.Drawing.Size(91, 25);
            this.bFile.TabIndex = 2;
            this.bFile.Text = "�ffnen";
            this.bFile.Click += new System.EventHandler(this.bFile_Click);
            // 
            // tbPath
            // 
            this.tbPath.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbPath.Location = new System.Drawing.Point(13, 43);
            this.tbPath.Name = "tbPath";
            this.tbPath.Size = new System.Drawing.Size(267, 20);
            this.tbPath.TabIndex = 3;
            // 
            // rbServer
            // 
            this.rbServer.AutoSize = true;
            this.rbServer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rbServer.Location = new System.Drawing.Point(12, 77);
            this.rbServer.Name = "rbServer";
            this.rbServer.Size = new System.Drawing.Size(51, 17);
            this.rbServer.TabIndex = 4;
            this.rbServer.Text = "Server";
            // 
            // rbClient
            // 
            this.rbClient.AutoSize = true;
            this.rbClient.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rbClient.Location = new System.Drawing.Point(12, 100);
            this.rbClient.Name = "rbClient";
            this.rbClient.Size = new System.Drawing.Size(46, 17);
            this.rbClient.TabIndex = 5;
            this.rbClient.Text = "Client";
            // 
            // textBox1
            // 
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox1.Location = new System.Drawing.Point(39, 123);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(93, 20);
            this.textBox1.TabIndex = 6;
            // 
            // lZeit
            // 
            this.lZeit.AutoSize = true;
            this.lZeit.Location = new System.Drawing.Point(12, 126);
            this.lZeit.Name = "lZeit";
            this.lZeit.Size = new System.Drawing.Size(21, 13);
            this.lZeit.TabIndex = 7;
            this.lZeit.Text = "Zeit";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(292, 273);
            this.Controls.Add(this.lZeit);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.rbClient);
            this.Controls.Add(this.rbServer);
            this.Controls.Add(this.tbPath);
            this.Controls.Add(this.bFile);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbIP);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbIP;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button bFile;
        private System.Windows.Forms.TextBox tbPath;
        private System.Windows.Forms.RadioButton rbServer;
        private System.Windows.Forms.RadioButton rbClient;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label lZeit;
    }
}

