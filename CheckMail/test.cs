/*
 * Created by SharpDevelop.
 * User: Johannes
 * Date: 11.06.2005
 * Time: 10:04
 */

using System;
using System.Drawing;
using System.Windows.Forms;

namespace CheckMail
{
	/// <summary>
	/// Description of test.
	/// </summary>
	public class test : System.Windows.Forms.Form
	{
		private System.Windows.Forms.NotifyIcon notifyIcon1;
		private System.Windows.Forms.Button button1;
		private System.ComponentModel.IContainer components;
		
		public test()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			
			//
			// TODO: Add constructor code after the InitializeComponent() call.
			//
		}
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}
		
		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(MainForm));
			this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
			this.button1 = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// notifyIcon1
			// 
			this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
			this.notifyIcon1.Text = "notifyIcon1";
			this.notifyIcon1.Visible = true;
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(72, 56);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(88, 23);
			this.button1.TabIndex = 0;
			this.button1.Text = "Show Balloon";
			this.button1.Click += new System.EventHandler(this.OnShowBalloon);
			// 
			// Form1
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(240, 142);
			this.Controls.AddRange(new System.Windows.Forms.Control[] {
			                       	this.button1});
			this.Name = "Form1";
			this.Text = "Form1";
			this.ResumeLayout(false);
			
		}
		#endregion
		
		
		private void OnShowBalloon(object sender, System.EventArgs e)
		{
			CheckMail.NotifyIcon notifyIcon = new CheckMail.NotifyIcon();
			notifyIcon.ShowBalloon(1, "My Title", "My Text", 15000);
		}
	}
}
