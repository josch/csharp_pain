/*
 * Created by SharpDevelop.
 * User: Johannes
 * Date: 10.06.2005
 * Time: 12:55
 */
using System;
using System.Drawing;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;

namespace CheckMail
{
	/// <summary>
	/// Description of MainForm.
	/// </summary>
	public class MainForm : System.Windows.Forms.Form
	{
		private System.ComponentModel.IContainer components;
		private System.Windows.Forms.ListBox lbMsgDetails;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button btnLogin;
		private System.Windows.Forms.ListBox lbFolders;
		private System.Windows.Forms.Button btnGetFolderList;
		private System.Windows.Forms.TextBox textBox1;
		private System.Windows.Forms.Button btnOK;
		private System.Windows.Forms.NotifyIcon notifyIcon1;
		private System.Windows.Forms.Button btnLogout;
		
		private TcpClient cl = null;
		private NetworkStream str = null;
		private int neueEmails = 0;
		
		public MainForm()
		{
			InitializeComponent();
		}
		
		[STAThread]
		public static void Main(string[] args)
		{
			Application.Run(new MainForm());
		}
		
		private void DoStuff()
		{
			
		}
		
		#region Windows Forms Designer generated code
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent() {
			this.components = new System.ComponentModel.Container();
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(MainForm));
			this.btnLogout = new System.Windows.Forms.Button();
			this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
			this.btnOK = new System.Windows.Forms.Button();
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.btnGetFolderList = new System.Windows.Forms.Button();
			this.lbFolders = new System.Windows.Forms.ListBox();
			this.btnLogin = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.button2 = new System.Windows.Forms.Button();
			this.lbMsgDetails = new System.Windows.Forms.ListBox();
			this.SuspendLayout();
			// 
			// btnLogout
			// 
			this.btnLogout.Location = new System.Drawing.Point(176, 8);
			this.btnLogout.Name = "btnLogout";
			this.btnLogout.TabIndex = 4;
			this.btnLogout.Text = "Logout";
			this.btnLogout.Click += new System.EventHandler(this.BtnLogoutClick);
			// 
			// notifyIcon1
			// 
			this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
			this.notifyIcon1.Text = "notifyIcon1";
			this.notifyIcon1.Visible = true;
			// 
			// btnOK
			// 
			this.btnOK.Location = new System.Drawing.Point(568, 536);
			this.btnOK.Name = "btnOK";
			this.btnOK.TabIndex = 7;
			this.btnOK.Text = "OK";
			this.btnOK.Click += new System.EventHandler(this.BtnOKClick);
			// 
			// textBox1
			// 
			this.textBox1.Location = new System.Drawing.Point(24, 40);
			this.textBox1.Multiline = true;
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new System.Drawing.Size(304, 376);
			this.textBox1.TabIndex = 1;
			this.textBox1.Text = "";
			// 
			// btnGetFolderList
			// 
			this.btnGetFolderList.Location = new System.Drawing.Point(528, 8);
			this.btnGetFolderList.Name = "btnGetFolderList";
			this.btnGetFolderList.Size = new System.Drawing.Size(104, 23);
			this.btnGetFolderList.TabIndex = 5;
			this.btnGetFolderList.Text = "Get Folder List";
			this.btnGetFolderList.Click += new System.EventHandler(this.BtnGetFolderListClick);
			// 
			// lbFolders
			// 
			this.lbFolders.Location = new System.Drawing.Point(368, 48);
			this.lbFolders.Name = "lbFolders";
			this.lbFolders.Size = new System.Drawing.Size(296, 368);
			this.lbFolders.TabIndex = 2;
			// 
			// btnLogin
			// 
			this.btnLogin.Location = new System.Drawing.Point(24, 8);
			this.btnLogin.Name = "btnLogin";
			this.btnLogin.TabIndex = 0;
			this.btnLogin.Text = "Login";
			this.btnLogin.Click += new System.EventHandler(this.BtnLoginClick);
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(24, 536);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(208, 24);
			this.label1.TabIndex = 8;
			this.label1.Text = "Neue Emails:";
			// 
			// button2
			// 
			this.button2.Location = new System.Drawing.Point(400, 8);
			this.button2.Name = "button2";
			this.button2.TabIndex = 3;
			this.button2.Text = "Angelika";
			this.button2.Click += new System.EventHandler(this.Button2Click);
			// 
			// lbMsgDetails
			// 
			this.lbMsgDetails.Location = new System.Drawing.Point(24, 432);
			this.lbMsgDetails.Name = "lbMsgDetails";
			this.lbMsgDetails.Size = new System.Drawing.Size(640, 95);
			this.lbMsgDetails.TabIndex = 6;
			// 
			// MainForm
			// 
			this.AcceptButton = this.btnOK;
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(712, 567);
			this.ControlBox = false;
			this.Controls.Add(this.label1);
			this.Controls.Add(this.btnOK);
			this.Controls.Add(this.lbMsgDetails);
			this.Controls.Add(this.btnGetFolderList);
			this.Controls.Add(this.btnLogout);
			this.Controls.Add(this.button2);
			this.Controls.Add(this.lbFolders);
			this.Controls.Add(this.textBox1);
			this.Controls.Add(this.btnLogin);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.Name = "MainForm";
			this.Text = "MainForm";
			this.Closed += new System.EventHandler(this.MainFormClosed);
			this.ResumeLayout(false);
		}
		#endregion
		
		void BtnLoginClick(object sender, System.EventArgs e)
		{
			textBox1.Text += Connect();
			textBox1.Text += SendCommand("? LOGIN J.Schauer porenta\r\n");
		}
		
		string SendCommand(string command)
		{
			byte[] send = Encoding.ASCII.GetBytes(command);
			str.Write(send, 0, send.Length);
			
			byte[] receive = new byte[4096];
			int k = str.Read(receive, 0, receive.Length);
			
			string msg = "";
			for(int i = 0; i<k; i++)
			{
				msg += Convert.ToChar(receive[i]).ToString();
			}
			
			return msg;
		}
		
		string Connect()
		{
			cl = new TcpClient("imap.web.de", 143);
			str = cl.GetStream();
			byte[] buffer = new byte[4096];
			
			int k = str.Read(buffer, 0, buffer.Length);
			
			string msg = "";
			for(int i = 0; i<k; i++)
			{
				msg += Convert.ToChar(buffer[i]).ToString();
			}
			
			return msg;
		}
		
		void MainFormClosed(object sender, System.EventArgs e)
		{
			notifyIcon1.Dispose();
			if(str!=null)
			{
				str.Close();
			}
			if(cl!=null)
			{
				cl.Close();
			}
		}
		void Button2Click(object sender, System.EventArgs e)
		{
			
		}
		
		int[] GetUnreadIDs(string folder)
		{
			SendCommand("? SELECT "+folder+"\r\n");
			string response = SendCommand("? SEARCH UNSEEN\r\n");
			if(response.Length > 34)
			{
				response = response.Remove(0,9);
				response = response.Remove(response.Length-25, 25);
				string[] collection = response.Split(' ');
				int[] IDs = new int[collection.Length];
				if(collection.Length>0)
				{
					for(int i=0; i<collection.Length; i++)
					{
						IDs[i] = int.Parse(collection[i]);
					}
				}
				return IDs;
			}
			return null;
		}
		
		string GetMailSubject(string folder, int ID)
		{
			return "Betreff";
		}
		
		string GetMailFrom(string folder, int ID)
		{
			return "Angelika.Fertig@web.de";
		}
		
		void GetFolderList()
		{
			neueEmails = 0;
			lbFolders.Items.Clear();
			
			string list = SendCommand("? LIST \"\" *\r\n");
			list = list.Remove(list.Length-22,22);
			string[] collection = list.Split('\n');
			
			for(int i = 0; i<collection.Length; i++)
			{
				collection[i] = collection[i].Remove(0, 27);
				collection[i] = collection[i].Remove(collection[i].Length-2, 2);
				if(collection[i]!="Unerw&APw-nscht")
				{
					int[] IDs = GetUnreadIDs(collection[i]);
					if(IDs!=null)
					{
						UpdateUnreadMsgDetails(collection[i], IDs);
						collection[i] += " ("+IDs.Length.ToString()+")";
						neueEmails += IDs.Length;
						label1.Text = "Neue Emails: " +neueEmails.ToString();
					}
					
					lbFolders.Items.Add(collection[i]);
				}
			}
		}
		
		void UpdateUnreadMsgDetails(string folder, int[] IDs)
		{
			lbMsgDetails.Items.Clear();
			textBox1.Text += SendCommand("? SELECT "+folder+"\r\n");
			
			for(int i = 0; i<IDs.Length; i++)
			{
				string sender = SendCommand("? FETCH "+IDs[i]+" (FLAGS BODY[HEADER.FIELDS (SENDER)])\r\n");
				Regex re1 = new Regex("Sender: (?<sender>\\S+)", RegexOptions.IgnoreCase);
				Match m1 = re1.Match(sender);
				sender = m1.Groups["sender"].Value;
				
				string subject = SendCommand("? FETCH "+IDs[i]+" (FLAGS BODY[HEADER.FIELDS (SUBJECT)])\r\n");
				Regex re2 = new Regex("Subject: (?<subject>\\S+)", RegexOptions.IgnoreCase);
				Match m2 = re2.Match(subject);
				subject = m2.Groups["subject"].Value;
				
				SendCommand("? STORE "+IDs[i]+" -FLAGS (\\SEEN)\r\n");
				lbMsgDetails.Items.Add(sender+" "+subject);
			}
		}
		void BtnLogoutClick(object sender, System.EventArgs e)
		{
			textBox1.Text += SendCommand("? LOGOUT\r\n");
			str.Close();
			cl.Close();
		}
		
		void BtnGetFolderListClick(object sender, System.EventArgs e)
		{
			Thread t = new Thread(new ThreadStart(GetFolderList));
			t.Start();
		}
		void BtnOKClick(object sender, System.EventArgs e)
		{
			this.Close();
		}
	}
}
