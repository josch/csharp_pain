using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.IO;

namespace TextToSpeech
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        void TextToSpeech(string text)
        {
            foreach(string line in textBox1.Lines)
            {
                HttpWebRequest wrq = (HttpWebRequest)WebRequest.Create("http://192.20.225.55/tts/cgi-bin/nph-talk");
                byte[] postData = Encoding.ASCII.GetBytes("voice=crystal&txt=" + line + "&speakButton=SPEAK");
                wrq.Method = "POST";
                wrq.ContentType = "application/x-www-form-urlencoded";
                wrq.UserAgent = "Mozilla/5.0 (Windows; U; Windows NT 5.1; de; rv:1.8.0.3) Gecko/20060426 Firefox/1.5.0.3";
                wrq.Accept = "text/xml,application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5";
                wrq.Referer = "http://www.research.att.com/~ttsweb/tts/demo.php";
                wrq.ContentLength = postData.Length;
                Stream stream = wrq.GetRequestStream();
                stream.Write(postData, 0, postData.Length);
                stream.Close();
                HttpWebResponse wrp = (HttpWebResponse)wrq.GetResponse();
                Stream stream2 = wrp.GetResponseStream();
                int data = 0;
                FileStream file = File.Create(line + ".wav");
                while (data != -1)
                {
                    data = stream2.ReadByte();
                    file.WriteByte((byte)data);
                }
                file.Close();
                stream2.Close();
            }
            label1.Text = "fertig";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            TextToSpeech("blub");
        }
    }
}