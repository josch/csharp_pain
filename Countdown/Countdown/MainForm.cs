using System;
using System.Drawing;
using System.Windows.Forms;

namespace Countdown
{
	public class MainForm : System.Windows.Forms.Form
	{
		private System.ComponentModel.IContainer components;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label DauerLabel;
		private System.Windows.Forms.Timer timer1;

        DateTime Date = DateTime.Parse("25.2.2006 13:54");
        TimeSpan Dauer;
		
		public MainForm()
		{
			InitializeComponent();

            label1.Text = "So viel Zeit vergeht noch bis ich meine Liebste am " + Date.ToLongDateString() + " um " + Date.ToLongTimeString() + " endlich wiedersehe...";

			Dauer = Date - DateTime.Now;
			
			DauerLabel.Text = Dauer.Days.ToString()
                + " d "
                + Dauer.Hours.ToString()
                + " h "
                + Dauer.Minutes.ToString()
                + " min "
                + Dauer.Seconds.ToString()
                + " s";
		}
		
		[STAThread]
		public static void Main(string[] args)
		{
			Application.Run(new MainForm());
		}
		
		#region Windows Forms Designer generated code
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.DauerLabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.Timer1Tick);
            // 
            // DauerLabel
            // 
            this.DauerLabel.Location = new System.Drawing.Point(72, 48);
            this.DauerLabel.Name = "DauerLabel";
            this.DauerLabel.Size = new System.Drawing.Size(168, 16);
            this.DauerLabel.TabIndex = 0;
            this.DauerLabel.Text = "Dauer";
            this.DauerLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(304, 40);
            this.label1.TabIndex = 4;
            this.label1.Text = "So viel Zeit vergeht noch bis ich meine Liebste am Sonntag dem 6. März 2005 21:00" +
                " endlich wiedersehe...";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // MainForm
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(304, 77);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.DauerLabel);
            this.Name = "MainForm";
            this.Text = "Zähler für Anja :-)";
            this.ResumeLayout(false);

		}
		#endregion
		void Timer1Tick(object sender, System.EventArgs e)
		{
			Dauer = Date - DateTime.Now;

            DauerLabel.Text = Dauer.Days.ToString()
                + " d "
                + Dauer.Hours.ToString()
                + " h "
                + Dauer.Minutes.ToString()
                + " min "
                + Dauer.Seconds.ToString()
                + " s";
        }
	}
}
