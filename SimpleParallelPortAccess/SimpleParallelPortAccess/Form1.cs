using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Threading;

namespace SimpleParallelPortAccess
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        [DllImport("inpout32.dll", EntryPoint = "Out32")]
        static extern void Output(int adress, int value);

        [DllImport("inpout32.dll", EntryPoint = "Inp32")]
        static extern int Input(int adress);

        void SendCommand(int value)
        {
            Output(0x378, value);
            Output(0x37A, 0x1);
            Output(0x37A, 0x0);
            for (int i = 0; i < 200000; i++)
            { }
            //Thread.Sleep(1);
        }

        void PrintChar(char value)
        {
            switch (value)
            {
                case ' ': SendCommand(0x20); break;
                case '!': SendCommand(0x21); break;
                case '\"': SendCommand(0x22); break;
                case '#': SendCommand(0x23); break;
                case '$': SendCommand(0x24); break;
                case '%': SendCommand(0x25); break;
                case '&': SendCommand(0x26); break;
                case '\'': SendCommand(0x27); break;
                case '(': SendCommand(0x28); break;
                case ')': SendCommand(0x29); break;
                case '*': SendCommand(0x2A); break;
                case '+': SendCommand(0x2B); break;
                case ',': SendCommand(0x2C); break;
                case '-': SendCommand(0x2D); break;
                case '.': SendCommand(0x2E); break;
                case '/': SendCommand(0x2F); break;
                case '0': SendCommand(0x30); break;
                case '1': SendCommand(0x31); break;
                case '2': SendCommand(0x32); break;
                case '3': SendCommand(0x33); break;
                case '4': SendCommand(0x34); break;
                case '5': SendCommand(0x35); break;
                case '6': SendCommand(0x36); break;
                case '7': SendCommand(0x37); break;
                case '8': SendCommand(0x38); break;
                case '9': SendCommand(0x39); break;
                case ':': SendCommand(0x3A); break;
                case ';': SendCommand(0x3B); break;
                case '<': SendCommand(0x3C); break;
                case '=': SendCommand(0x3D); break;
                case '>': SendCommand(0x3E); break;
                case '?': SendCommand(0x3F); break;
                case '@': SendCommand(0x40); break;
                case 'A': SendCommand(0x41); break;
                case 'B': SendCommand(0x42); break;
                case 'C': SendCommand(0x43); break;
                case 'D': SendCommand(0x44); break;
                case 'E': SendCommand(0x45); break;
                case 'F': SendCommand(0x46); break;
                case 'G': SendCommand(0x47); break;
                case 'H': SendCommand(0x48); break;
                case 'I': SendCommand(0x49); break;
                case 'J': SendCommand(0x4A); break;
                case 'K': SendCommand(0x4B); break;
                case 'L': SendCommand(0x4C); break;
                case 'M': SendCommand(0x4D); break;
                case 'N': SendCommand(0x4E); break;
                case 'O': SendCommand(0x4F); break;
                case 'P': SendCommand(0x50); break;
                case 'Q': SendCommand(0x51); break;
                case 'R': SendCommand(0x52); break;
                case 'S': SendCommand(0x53); break;
                case 'T': SendCommand(0x54); break;
                case 'U': SendCommand(0x55); break;
                case 'V': SendCommand(0x56); break;
                case 'W': SendCommand(0x57); break;
                case 'X': SendCommand(0x58); break;
                case 'Y': SendCommand(0x59); break;
                case 'Z': SendCommand(0x5A); break;
                case '[': SendCommand(0x5B); break;
                case '\\': SendCommand(0x5C); break;
                case ']': SendCommand(0x5D); break;
                case '^': SendCommand(0x5E); break;
                case '_': SendCommand(0x5F); break;
                case '`': SendCommand(0x60); break;
                case 'a': SendCommand(0x61); break;
                case 'b': SendCommand(0x62); break;
                case 'c': SendCommand(0x63); break;
                case 'd': SendCommand(0x64); break;
                case 'e': SendCommand(0x65); break;
                case 'f': SendCommand(0x66); break;
                case 'g': SendCommand(0x67); break;
                case 'h': SendCommand(0x68); break;
                case 'i': SendCommand(0x69); break;
                case 'j': SendCommand(0x6A); break;
                case 'k': SendCommand(0x6B); break;
                case 'l': SendCommand(0x6C); break;
                case 'm': SendCommand(0x6D); break;
                case 'n': SendCommand(0x6E); break;
                case 'o': SendCommand(0x6F); break;
                case 'p': SendCommand(0x70); break;
                case 'q': SendCommand(0x71); break;
                case 'r': SendCommand(0x72); break;
                case 's': SendCommand(0x73); break;
                case 't': SendCommand(0x74); break;
                case 'u': SendCommand(0x75); break;
                case 'v': SendCommand(0x76); break;
                case 'w': SendCommand(0x77); break;
                case 'x': SendCommand(0x78); break;
                case 'y': SendCommand(0x79); break;
                case 'z': SendCommand(0x7A); break;
                case '{': SendCommand(0x7B); break;
                case '|': SendCommand(0x7C); break;
                case '}': SendCommand(0x7D); break;
                case '~': SendCommand(0x7E); break;
                case ' ': SendCommand(0x7F); break;
                case 'Ç': SendCommand(0x80); break;
                case 'ü': SendCommand(0x81); break;
                case 'é': SendCommand(0x82); break;
                case 'â': SendCommand(0x83); break;
                case 'ä': SendCommand(0x84); break;
                case 'à': SendCommand(0x85); break;
                case 'å': SendCommand(0x86); break;
                case 'ç': SendCommand(0x87); break;
                case 'ê': SendCommand(0x88); break;
                case 'ë': SendCommand(0x89); break;
                case 'è': SendCommand(0x8A); break;
                case 'ï': SendCommand(0x8B); break;
                case 'î': SendCommand(0x8C); break;
                case 'ì': SendCommand(0x8D); break;
                case 'Ä': SendCommand(0x8E); break;
                case 'Å': SendCommand(0x8F); break;
                case 'È': SendCommand(0x90); break;
                case 'æ': SendCommand(0x91); break;
                case 'Æ': SendCommand(0x92); break;
                case 'ô': SendCommand(0x93); break;
                case 'ö': SendCommand(0x94); break;
                case 'ò': SendCommand(0x95); break;
                case 'û': SendCommand(0x96); break;
                case 'ù': SendCommand(0x97); break;
                case 'ÿ': SendCommand(0x98); break;
                case 'Ö': SendCommand(0x99); break;
                case 'Ü': SendCommand(0x9A); break;
                case '¢': SendCommand(0x9B); break;
                case '£': SendCommand(0x9C); break;
                case '¥': SendCommand(0x9D); break;
                case '₧': SendCommand(0x9E); break;
                case 'ƒ': SendCommand(0x9F); break;
                case 'á': SendCommand(0xA0); break;
                case 'í': SendCommand(0xA1); break;
                case 'ó': SendCommand(0xA2); break;
                case 'ú': SendCommand(0xA3); break;
                case 'ň': SendCommand(0xA4); break;
                case 'Ň': SendCommand(0xA5); break;
                //case '': SendCommand(0xA6); break; was soll das sein?
                //case '': SendCommand(0xA7); break; die gibt es net
                case '¿': SendCommand(0xA8); break;
                case '⌐': SendCommand(0xA9); break;
                case '¬': SendCommand(0xAA); break;
                case '½': SendCommand(0xAB); break;
                case '¼': SendCommand(0xAC); break;
                case '¡': SendCommand(0xAD); break;
                case '«': SendCommand(0xAE); break;
                case '»': SendCommand(0xAF); break;
                case 'α': SendCommand(0xB0); break;
                case 'β': SendCommand(0xB1); break;
                case 'γ': SendCommand(0xB2); break;
                case 'Δ': SendCommand(0xB3); break;
                case 'ε': SendCommand(0xB4); break;
                case 'η': SendCommand(0xB5); break;
                case 'θ': SendCommand(0xB6); break;
                case 'λ': SendCommand(0xB7); break;
                case 'μ': SendCommand(0xB8); break;
                case 'π': SendCommand(0xB9); break;
                case 'ρ': SendCommand(0xBA); break;
                case 'σ': SendCommand(0xBB); break;
                case 'τ': SendCommand(0xBC); break;
                case 'φ': SendCommand(0xBD); break;
                case 'Ω': SendCommand(0xBE); break;
                case 'Σ': SendCommand(0xBF); break;
                case '§': SendCommand(0xC0); break;
                //case '': SendCommand(0xC1); break; gibbet
                //case '': SendCommand(0xC2); break; auch net
                case '∫': SendCommand(0xC3); break;
                //case '': SendCommand(0xC4); break; watn scheiß
                //case '': SendCommand(0xC5); break; nix da
                case '²': SendCommand(0xC6); break;
                case '³': SendCommand(0xC7); break;
                case 'ⁿ': SendCommand(0xC8); break; //eigentlich ein x - aber egal
                case '±': SendCommand(0xC9); break;
                case '√': SendCommand(0xCA); break;
                //case '': SendCommand(0xCB); break; alles leer
                //case '': SendCommand(0xCC); break;
                //case '': SendCommand(0xCD); break;
                //case '': SendCommand(0xCE); break;
                //case '': SendCommand(0xCF); break;
                case 'À': SendCommand(0xD0); break;
                case 'É': SendCommand(0xD1); break;
                case 'Ù': SendCommand(0xD2); break;
                case 'Ë': SendCommand(0xD3); break;
                case 'Ï': SendCommand(0xD4); break;
                case 'Â': SendCommand(0xD5); break;
                case 'Ê': SendCommand(0xD6); break;
                case 'Î': SendCommand(0xD7); break;
                case 'Ô': SendCommand(0xD8); break;
                case 'Û': SendCommand(0xD9); break;
                case 'ь': SendCommand(0xDA); break;
                case 'г': SendCommand(0xDB); break;
                case 'д': SendCommand(0xDC); break;
                case 'Ж': SendCommand(0xDD); break;
                case 'з': SendCommand(0xDE); break;
                case 'и': SendCommand(0xDF); break;
                case 'Й': SendCommand(0xD0); break;
                case 'Л': SendCommand(0xE1); break;
                case 'П': SendCommand(0xE2); break;
                case 'У': SendCommand(0xE3); break;
                case 'Ф': SendCommand(0xE4); break;
                case 'Ц': SendCommand(0xE5); break;
                case 'Ч': SendCommand(0xE6); break;
                case 'Ш': SendCommand(0xE7); break;
                case 'Щ': SendCommand(0xE8); break;
                case 'Ъ': SendCommand(0xE9); break;
                case 'Ы': SendCommand(0xEA); break;
                case 'Э': SendCommand(0xEB); break;
                case 'Ю': SendCommand(0xEC); break;
                case 'Я': SendCommand(0xED); break;
                //case '': SendCommand(0xEE); break; leer
                //case '': SendCommand(0xEF); break;
                case '≤': SendCommand(0xF0); break;
                case '≥': SendCommand(0xF1); break;
                case '≠': SendCommand(0xF2); break;
                //case '': SendCommand(0xF3); break; senkrecht gibt net
                case '∞': SendCommand(0xF4); break;
                case '≈': SendCommand(0xF5); break;
                case '≡': SendCommand(0xF6); break;
                //case '': SendCommand(0xF7); break;
                //case '': SendCommand(0xF8); break;
                case '←': SendCommand(0xF9); break;
                case '→': SendCommand(0xFA); break;
                //case '': SendCommand(0xFB); break;
                //case '': SendCommand(0xFC); break;
                //case '': SendCommand(0xFD); break;
                //case '': SendCommand(0xFE); break;
                //case '': SendCommand(0xFF); break;
            }
        }

        void PrintString(string value)
        {
            foreach (char c in value)
            {
                PrintChar(c);
            }
        }

        void DimDisplay(int percent)
        {
            percent = ((percent * 4) / 100) * 25;
            switch (percent)
            {
                case 100: SendCommand(0x04); SendCommand(0xFF); break;
                case 80: SendCommand(0x04); SendCommand(0x80); break;
                case 60: SendCommand(0x04); SendCommand(0x60); break;
                case 40: SendCommand(0x04); SendCommand(0x40); break;
                case 20: SendCommand(0x04); SendCommand(0x20); break;
                default: SendCommand(0x04); SendCommand(0x20); break;
            }
        }

        void Backspace() //Löscht des letzte Zeichen vor dem Cursor
        {
            SendCommand(0x08);
        }

        void Tab() //Cursor ein Feld weiter
        {
            SendCommand(0x09);
        }

        void LineFeed() //Cursor wechselt die Zeile bei selber Spalte
        {
            SendCommand(0x0A);
        }

        void CarriageReturn() //Cursor an den Anfang der aktuellen Zeile
        {
            SendCommand(0x0D);
        }

        void Reset() //Alle Zeichen werden gelöscht und der Cursor an den Anfang gesetzt
        {
            SendCommand(0x1F);
        }

        void SetCursorPosition(int row, int col)
        {
            if (row < 0)
                row = 0;
            if (row > 1)
                row = 1;
            if (col < 0)
                col = 0;
            if (col > 39)
                col = 39;
            SendCommand(0x10);
            SendCommand(40 * row + col);
        }

        void SetDisplayMode(bool scrolling, bool cursor)
        {
            if (scrolling)
            {
                SendCommand(0x12);
            }
            else
            {
                SendCommand(0x11);
            }
            if (cursor)
            {
                SendCommand(0x13);
            }
            else
            {
                SendCommand(0x14);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            PrintString(tbCommand.Text);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            MessageBox.Show(Input(0x370).ToString());
        }
    }
}