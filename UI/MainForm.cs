/* aputze */

using System;
using System.Drawing;
using System.Windows.Forms;

using System.Threading;

namespace DynaDis {
	public class MainForm : System.Windows.Forms.Form {
		private System.Windows.Forms.Label maxWaitingLabel;
		private System.Windows.Forms.DataGridTextBoxColumn arrivalStyle;
		private System.Windows.Forms.DataGridTextBoxColumn toDoStyle;
		private System.Windows.Forms.TabPage deviceTabPage;
		private System.Windows.Forms.MenuItem databaseMenu;
		private System.Windows.Forms.MenuItem updateMenuItem;
		private System.Windows.Forms.TextBox dataSourceTextBox;
		private System.Windows.Forms.TabPage employeeTabPage;
		private System.Windows.Forms.DataGridTextBoxColumn doneStyle;
		private System.Windows.Forms.DataGridTableStyle trainTableStyle;
		private System.Windows.Forms.TextBox initialCatalogTextBox;
		private System.Windows.Forms.TabPage selectTabPage;
		private System.Windows.Forms.MenuItem disconnectMenuItem;
		private System.Windows.Forms.DataGrid employeeGrid;
		private System.Windows.Forms.ProgressBar selectProgressBar;
		private System.Windows.Forms.TabPage trainTabPage;
		private System.Windows.Forms.Label dayLabel;
		private System.Windows.Forms.TextBox maxTouringTextBox;
		private System.Windows.Forms.Label maxTouringLabel;
		private System.Windows.Forms.TabPage propertiesTabPage;
		private System.Windows.Forms.DataGridTextBoxColumn departureStationStyle;
		private System.Windows.Forms.TabPage tourTabPage;
		private System.Windows.Forms.MenuItem fillMenuItem;
		private System.Windows.Forms.DataGrid trainGrid;
		private System.Windows.Forms.DataGridTextBoxColumn trainIdStyle;
		private System.Windows.Forms.Button selectButton;
		private System.Windows.Forms.DataGrid deviceGrid;
		private System.Windows.Forms.Label dataSourceLabel;
		private System.Windows.Forms.StatusBar statusBar;
		private System.Windows.Forms.Label initialCatalogLabel;
		private System.Windows.Forms.DataGridTextBoxColumn availableStyle;
		private System.Windows.Forms.TextBox minWaitingTextBox;
		private System.Windows.Forms.DataGridTextBoxColumn departureStyle;
		private System.Windows.Forms.ComboBox dayComboBox;
		private System.Windows.Forms.TextBox maxWaitingTextBox;
		private System.Windows.Forms.Label minWaitingLabel;
		private System.Windows.Forms.MenuItem connectMenuItem;
		private System.Windows.Forms.MainMenu mainMenu;
		private System.Windows.Forms.DataGridTextBoxColumn arrivalStationStyle;
		private System.Windows.Forms.TabControl tabControl;
		private System.Windows.Forms.MenuItem prepareMenuItem;
		
		private DynaDis dynaDis=new DynaDis();
		
		public MainForm() {
			InitializeComponent();
			
			dynaDis.selectProgress+=new ProgressHandler(SelectProgress);
			
			trainGrid.SetDataBinding(dynaDis.dataSet,"TrainTable");
			deviceGrid.SetDataBinding(dynaDis.dataSet,"DeviceTable");
			employeeGrid.SetDataBinding(dynaDis.dataSet,"EmployeeTable");
		}
		
		#region Windows Forms Designer generated code
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent() {
			this.prepareMenuItem = new System.Windows.Forms.MenuItem();
			this.tabControl = new System.Windows.Forms.TabControl();
			this.arrivalStationStyle = new System.Windows.Forms.DataGridTextBoxColumn();
			this.mainMenu = new System.Windows.Forms.MainMenu();
			this.connectMenuItem = new System.Windows.Forms.MenuItem();
			this.minWaitingLabel = new System.Windows.Forms.Label();
			this.maxWaitingTextBox = new System.Windows.Forms.TextBox();
			this.dayComboBox = new System.Windows.Forms.ComboBox();
			this.departureStyle = new System.Windows.Forms.DataGridTextBoxColumn();
			this.minWaitingTextBox = new System.Windows.Forms.TextBox();
			this.availableStyle = new System.Windows.Forms.DataGridTextBoxColumn();
			this.initialCatalogLabel = new System.Windows.Forms.Label();
			this.statusBar = new System.Windows.Forms.StatusBar();
			this.dataSourceLabel = new System.Windows.Forms.Label();
			this.deviceGrid = new System.Windows.Forms.DataGrid();
			this.selectButton = new System.Windows.Forms.Button();
			this.trainIdStyle = new System.Windows.Forms.DataGridTextBoxColumn();
			this.trainGrid = new System.Windows.Forms.DataGrid();
			this.fillMenuItem = new System.Windows.Forms.MenuItem();
			this.tourTabPage = new System.Windows.Forms.TabPage();
			this.departureStationStyle = new System.Windows.Forms.DataGridTextBoxColumn();
			this.propertiesTabPage = new System.Windows.Forms.TabPage();
			this.maxTouringLabel = new System.Windows.Forms.Label();
			this.maxTouringTextBox = new System.Windows.Forms.TextBox();
			this.dayLabel = new System.Windows.Forms.Label();
			this.trainTabPage = new System.Windows.Forms.TabPage();
			this.selectProgressBar = new System.Windows.Forms.ProgressBar();
			this.employeeGrid = new System.Windows.Forms.DataGrid();
			this.disconnectMenuItem = new System.Windows.Forms.MenuItem();
			this.selectTabPage = new System.Windows.Forms.TabPage();
			this.initialCatalogTextBox = new System.Windows.Forms.TextBox();
			this.trainTableStyle = new System.Windows.Forms.DataGridTableStyle();
			this.doneStyle = new System.Windows.Forms.DataGridTextBoxColumn();
			this.employeeTabPage = new System.Windows.Forms.TabPage();
			this.dataSourceTextBox = new System.Windows.Forms.TextBox();
			this.updateMenuItem = new System.Windows.Forms.MenuItem();
			this.databaseMenu = new System.Windows.Forms.MenuItem();
			this.deviceTabPage = new System.Windows.Forms.TabPage();
			this.toDoStyle = new System.Windows.Forms.DataGridTextBoxColumn();
			this.arrivalStyle = new System.Windows.Forms.DataGridTextBoxColumn();
			this.maxWaitingLabel = new System.Windows.Forms.Label();
			this.tabControl.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.deviceGrid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.trainGrid)).BeginInit();
			this.propertiesTabPage.SuspendLayout();
			this.trainTabPage.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.employeeGrid)).BeginInit();
			this.selectTabPage.SuspendLayout();
			this.employeeTabPage.SuspendLayout();
			this.deviceTabPage.SuspendLayout();
			this.SuspendLayout();
			// 
			// prepareMenuItem
			// 
			this.prepareMenuItem.Index = 2;
			this.prepareMenuItem.Shortcut = System.Windows.Forms.Shortcut.CtrlP;
			this.prepareMenuItem.Text = "Prepare";
			this.prepareMenuItem.Click += new System.EventHandler(this.PrepareMenuItemClick);
			// 
			// tabControl
			// 
			this.tabControl.Controls.Add(this.propertiesTabPage);
			this.tabControl.Controls.Add(this.selectTabPage);
			this.tabControl.Controls.Add(this.trainTabPage);
			this.tabControl.Controls.Add(this.deviceTabPage);
			this.tabControl.Controls.Add(this.employeeTabPage);
			this.tabControl.Controls.Add(this.tourTabPage);
			this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tabControl.Location = new System.Drawing.Point(0, 0);
			this.tabControl.Name = "tabControl";
			this.tabControl.SelectedIndex = 0;
			this.tabControl.Size = new System.Drawing.Size(292, 244);
			this.tabControl.TabIndex = 2;
			// 
			// arrivalStationStyle
			// 
			this.arrivalStationStyle.Format = "";
			this.arrivalStationStyle.HeaderText = "ArrivalStation";
			this.arrivalStationStyle.MappingName = "ArrivalStation";
			this.arrivalStationStyle.Width = 125;
			// 
			// mainMenu
			// 
			this.mainMenu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
						this.databaseMenu});
			// 
			// connectMenuItem
			// 
			this.connectMenuItem.Index = 0;
			this.connectMenuItem.Shortcut = System.Windows.Forms.Shortcut.CtrlC;
			this.connectMenuItem.Text = "Connect";
			this.connectMenuItem.Click += new System.EventHandler(this.ConnectMenuItemClick);
			// 
			// minWaitingLabel
			// 
			this.minWaitingLabel.Location = new System.Drawing.Point(8, 80);
			this.minWaitingLabel.Name = "minWaitingLabel";
			this.minWaitingLabel.TabIndex = 14;
			this.minWaitingLabel.Text = "Min Waiting";
			// 
			// maxWaitingTextBox
			// 
			this.maxWaitingTextBox.Location = new System.Drawing.Point(120, 112);
			this.maxWaitingTextBox.Name = "maxWaitingTextBox";
			this.maxWaitingTextBox.Size = new System.Drawing.Size(152, 20);
			this.maxWaitingTextBox.TabIndex = 17;
			this.maxWaitingTextBox.Text = "";
			this.maxWaitingTextBox.Validating += new System.ComponentModel.CancelEventHandler(this.MaxWaitingTextBoxValidating);
			// 
			// dayComboBox
			// 
			this.dayComboBox.Items.AddRange(new object[] {
						"1",
						"2",
						"4",
						"8",
						"16",
						"32",
						"64"});
			this.dayComboBox.Location = new System.Drawing.Point(120, 176);
			this.dayComboBox.Name = "dayComboBox";
			this.dayComboBox.Size = new System.Drawing.Size(152, 21);
			this.dayComboBox.TabIndex = 21;
			this.dayComboBox.Validating += new System.ComponentModel.CancelEventHandler(this.DayComboBoxValidating);
			// 
			// departureStyle
			// 
			this.departureStyle.Format = "g";
			this.departureStyle.HeaderText = "Departure";
			this.departureStyle.MappingName = "Departure";
			this.departureStyle.Width = 125;
			// 
			// minWaitingTextBox
			// 
			this.minWaitingTextBox.Location = new System.Drawing.Point(120, 80);
			this.minWaitingTextBox.Name = "minWaitingTextBox";
			this.minWaitingTextBox.Size = new System.Drawing.Size(152, 20);
			this.minWaitingTextBox.TabIndex = 15;
			this.minWaitingTextBox.Text = "";
			this.minWaitingTextBox.Validating += new System.ComponentModel.CancelEventHandler(this.MinWaitingTextBoxValidating);
			// 
			// availableStyle
			// 
			this.availableStyle.Format = "";
			this.availableStyle.HeaderText = "Available";
			this.availableStyle.MappingName = "Available";
			this.availableStyle.Width = 50;
			// 
			// initialCatalogLabel
			// 
			this.initialCatalogLabel.Location = new System.Drawing.Point(8, 40);
			this.initialCatalogLabel.Name = "initialCatalogLabel";
			this.initialCatalogLabel.TabIndex = 12;
			this.initialCatalogLabel.Text = "Initial Catalog";
			// 
			// statusBar
			// 
			this.statusBar.Location = new System.Drawing.Point(0, 244);
			this.statusBar.Name = "statusBar";
			this.statusBar.Size = new System.Drawing.Size(292, 22);
			this.statusBar.TabIndex = 1;
			// 
			// dataSourceLabel
			// 
			this.dataSourceLabel.Location = new System.Drawing.Point(8, 8);
			this.dataSourceLabel.Name = "dataSourceLabel";
			this.dataSourceLabel.TabIndex = 10;
			this.dataSourceLabel.Text = "Data Source";
			// 
			// deviceGrid
			// 
			this.deviceGrid.CaptionVisible = false;
			this.deviceGrid.DataMember = "";
			this.deviceGrid.Dock = System.Windows.Forms.DockStyle.Fill;
			this.deviceGrid.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.deviceGrid.Location = new System.Drawing.Point(0, 0);
			this.deviceGrid.Name = "deviceGrid";
			this.deviceGrid.Size = new System.Drawing.Size(284, 218);
			this.deviceGrid.TabIndex = 0;
			// 
			// selectButton
			// 
			this.selectButton.Location = new System.Drawing.Point(8, 8);
			this.selectButton.Name = "selectButton";
			this.selectButton.TabIndex = 0;
			this.selectButton.Text = "Select";
			this.selectButton.Click += new System.EventHandler(this.SelectButtonClick);
			// 
			// trainIdStyle
			// 
			this.trainIdStyle.Format = "";
			this.trainIdStyle.HeaderText = "TrainId";
			this.trainIdStyle.MappingName = "TrainId";
			this.trainIdStyle.Width = 75;
			// 
			// trainGrid
			// 
			this.trainGrid.CaptionVisible = false;
			this.trainGrid.DataMember = "";
			this.trainGrid.Dock = System.Windows.Forms.DockStyle.Fill;
			this.trainGrid.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.trainGrid.Location = new System.Drawing.Point(0, 0);
			this.trainGrid.Name = "trainGrid";
			this.trainGrid.Size = new System.Drawing.Size(284, 218);
			this.trainGrid.TabIndex = 0;
			this.trainGrid.TableStyles.AddRange(new System.Windows.Forms.DataGridTableStyle[] {
						this.trainTableStyle});
			// 
			// fillMenuItem
			// 
			this.fillMenuItem.Index = 3;
			this.fillMenuItem.Shortcut = System.Windows.Forms.Shortcut.CtrlF;
			this.fillMenuItem.Text = "Fill";
			this.fillMenuItem.Click += new System.EventHandler(this.FillMenuItemClick);
			// 
			// tourTabPage
			// 
			this.tourTabPage.Location = new System.Drawing.Point(4, 22);
			this.tourTabPage.Name = "tourTabPage";
			this.tourTabPage.Size = new System.Drawing.Size(284, 218);
			this.tourTabPage.TabIndex = 4;
			this.tourTabPage.Text = "TourTable";
			// 
			// departureStationStyle
			// 
			this.departureStationStyle.Format = "";
			this.departureStationStyle.HeaderText = "DepartureStation";
			this.departureStationStyle.MappingName = "DepartureStation";
			this.departureStationStyle.Width = 125;
			// 
			// propertiesTabPage
			// 
			this.propertiesTabPage.Controls.Add(this.dayComboBox);
			this.propertiesTabPage.Controls.Add(this.dayLabel);
			this.propertiesTabPage.Controls.Add(this.maxTouringTextBox);
			this.propertiesTabPage.Controls.Add(this.maxTouringLabel);
			this.propertiesTabPage.Controls.Add(this.maxWaitingTextBox);
			this.propertiesTabPage.Controls.Add(this.maxWaitingLabel);
			this.propertiesTabPage.Controls.Add(this.minWaitingTextBox);
			this.propertiesTabPage.Controls.Add(this.minWaitingLabel);
			this.propertiesTabPage.Controls.Add(this.initialCatalogTextBox);
			this.propertiesTabPage.Controls.Add(this.initialCatalogLabel);
			this.propertiesTabPage.Controls.Add(this.dataSourceTextBox);
			this.propertiesTabPage.Controls.Add(this.dataSourceLabel);
			this.propertiesTabPage.Location = new System.Drawing.Point(4, 22);
			this.propertiesTabPage.Name = "propertiesTabPage";
			this.propertiesTabPage.Size = new System.Drawing.Size(284, 218);
			this.propertiesTabPage.TabIndex = 0;
			this.propertiesTabPage.Text = "Properties";
			// 
			// maxTouringLabel
			// 
			this.maxTouringLabel.Location = new System.Drawing.Point(8, 144);
			this.maxTouringLabel.Name = "maxTouringLabel";
			this.maxTouringLabel.TabIndex = 18;
			this.maxTouringLabel.Text = "Max Touring";
			// 
			// maxTouringTextBox
			// 
			this.maxTouringTextBox.Location = new System.Drawing.Point(120, 144);
			this.maxTouringTextBox.Name = "maxTouringTextBox";
			this.maxTouringTextBox.Size = new System.Drawing.Size(152, 20);
			this.maxTouringTextBox.TabIndex = 19;
			this.maxTouringTextBox.Text = "";
			this.maxTouringTextBox.Validating += new System.ComponentModel.CancelEventHandler(this.MaxTouringTextBoxValidating);
			// 
			// dayLabel
			// 
			this.dayLabel.Location = new System.Drawing.Point(8, 176);
			this.dayLabel.Name = "dayLabel";
			this.dayLabel.TabIndex = 20;
			this.dayLabel.Text = "Day";
			// 
			// trainTabPage
			// 
			this.trainTabPage.Controls.Add(this.trainGrid);
			this.trainTabPage.Location = new System.Drawing.Point(4, 22);
			this.trainTabPage.Name = "trainTabPage";
			this.trainTabPage.Size = new System.Drawing.Size(284, 218);
			this.trainTabPage.TabIndex = 1;
			this.trainTabPage.Text = "TrainTable";
			// 
			// selectProgressBar
			// 
			this.selectProgressBar.Location = new System.Drawing.Point(8, 40);
			this.selectProgressBar.Name = "selectProgressBar";
			this.selectProgressBar.Size = new System.Drawing.Size(272, 23);
			this.selectProgressBar.TabIndex = 1;
			// 
			// employeeGrid
			// 
			this.employeeGrid.CaptionVisible = false;
			this.employeeGrid.DataMember = "";
			this.employeeGrid.Dock = System.Windows.Forms.DockStyle.Fill;
			this.employeeGrid.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.employeeGrid.Location = new System.Drawing.Point(0, 0);
			this.employeeGrid.Name = "employeeGrid";
			this.employeeGrid.Size = new System.Drawing.Size(284, 218);
			this.employeeGrid.TabIndex = 0;
			// 
			// disconnectMenuItem
			// 
			this.disconnectMenuItem.Index = 1;
			this.disconnectMenuItem.Shortcut = System.Windows.Forms.Shortcut.CtrlD;
			this.disconnectMenuItem.Text = "Disconnect";
			this.disconnectMenuItem.Click += new System.EventHandler(this.DisconnectMenuItemClick);
			// 
			// selectTabPage
			// 
			this.selectTabPage.Controls.Add(this.selectProgressBar);
			this.selectTabPage.Controls.Add(this.selectButton);
			this.selectTabPage.Location = new System.Drawing.Point(4, 22);
			this.selectTabPage.Name = "selectTabPage";
			this.selectTabPage.Size = new System.Drawing.Size(284, 218);
			this.selectTabPage.TabIndex = 5;
			this.selectTabPage.Text = "Select";
			// 
			// initialCatalogTextBox
			// 
			this.initialCatalogTextBox.Location = new System.Drawing.Point(120, 40);
			this.initialCatalogTextBox.Name = "initialCatalogTextBox";
			this.initialCatalogTextBox.Size = new System.Drawing.Size(152, 20);
			this.initialCatalogTextBox.TabIndex = 13;
			this.initialCatalogTextBox.Text = "";
			this.initialCatalogTextBox.Validating += new System.ComponentModel.CancelEventHandler(this.InitialCatalogTextBoxValidating);
			// 
			// trainTableStyle
			// 
			this.trainTableStyle.DataGrid = this.trainGrid;
			this.trainTableStyle.GridColumnStyles.AddRange(new System.Windows.Forms.DataGridColumnStyle[] {
						this.trainIdStyle,
						this.departureStyle,
						this.departureStationStyle,
						this.arrivalStyle,
						this.arrivalStationStyle,
						this.availableStyle,
						this.toDoStyle,
						this.doneStyle});
			this.trainTableStyle.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.trainTableStyle.MappingName = "TrainTable";
			// 
			// doneStyle
			// 
			this.doneStyle.Format = "";
			this.doneStyle.HeaderText = "Done";
			this.doneStyle.MappingName = "Done";
			this.doneStyle.Width = 50;
			// 
			// employeeTabPage
			// 
			this.employeeTabPage.Controls.Add(this.employeeGrid);
			this.employeeTabPage.Location = new System.Drawing.Point(4, 22);
			this.employeeTabPage.Name = "employeeTabPage";
			this.employeeTabPage.Size = new System.Drawing.Size(284, 218);
			this.employeeTabPage.TabIndex = 3;
			this.employeeTabPage.Text = "EmployeeTable";
			// 
			// dataSourceTextBox
			// 
			this.dataSourceTextBox.Location = new System.Drawing.Point(120, 8);
			this.dataSourceTextBox.Name = "dataSourceTextBox";
			this.dataSourceTextBox.Size = new System.Drawing.Size(152, 20);
			this.dataSourceTextBox.TabIndex = 11;
			this.dataSourceTextBox.Text = "";
			this.dataSourceTextBox.Validating += new System.ComponentModel.CancelEventHandler(this.DataSourceTextBoxValidating);
			// 
			// updateMenuItem
			// 
			this.updateMenuItem.Index = 4;
			this.updateMenuItem.Shortcut = System.Windows.Forms.Shortcut.CtrlU;
			this.updateMenuItem.Text = "Update";
			this.updateMenuItem.Click += new System.EventHandler(this.UpdateMenuItemClick);
			// 
			// databaseMenu
			// 
			this.databaseMenu.Index = 0;
			this.databaseMenu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
						this.connectMenuItem,
						this.disconnectMenuItem,
						this.prepareMenuItem,
						this.fillMenuItem,
						this.updateMenuItem});
			this.databaseMenu.Text = "Database";
			// 
			// deviceTabPage
			// 
			this.deviceTabPage.Controls.Add(this.deviceGrid);
			this.deviceTabPage.Location = new System.Drawing.Point(4, 22);
			this.deviceTabPage.Name = "deviceTabPage";
			this.deviceTabPage.Size = new System.Drawing.Size(284, 218);
			this.deviceTabPage.TabIndex = 2;
			this.deviceTabPage.Text = "DeviceTable";
			// 
			// toDoStyle
			// 
			this.toDoStyle.Format = "";
			this.toDoStyle.HeaderText = "ToDo";
			this.toDoStyle.MappingName = "ToDo";
			this.toDoStyle.Width = 50;
			// 
			// arrivalStyle
			// 
			this.arrivalStyle.Format = "g";
			this.arrivalStyle.HeaderText = "Arrival";
			this.arrivalStyle.MappingName = "Arrival";
			this.arrivalStyle.Width = 125;
			// 
			// maxWaitingLabel
			// 
			this.maxWaitingLabel.Location = new System.Drawing.Point(8, 112);
			this.maxWaitingLabel.Name = "maxWaitingLabel";
			this.maxWaitingLabel.TabIndex = 16;
			this.maxWaitingLabel.Text = "Max Waiting";
			// 
			// MainForm
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(292, 266);
			this.Controls.Add(this.tabControl);
			this.Controls.Add(this.statusBar);
			this.Menu = this.mainMenu;
			this.Name = "MainForm";
			this.Text = "DynaDis";
			this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
			this.Load += new System.EventHandler(this.MainFormLoad);
			this.tabControl.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.deviceGrid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.trainGrid)).EndInit();
			this.propertiesTabPage.ResumeLayout(false);
			this.trainTabPage.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.employeeGrid)).EndInit();
			this.selectTabPage.ResumeLayout(false);
			this.employeeTabPage.ResumeLayout(false);
			this.deviceTabPage.ResumeLayout(false);
			this.ResumeLayout(false);
		}
		#endregion
		
		[STAThread]
		public static void Main() {
			//Application.Run(new MainForm());
		}
		
		void ConnectMenuItemClick(object sender, System.EventArgs e) {
			dynaDis.Connect();
			statusBar.Text="Connected";
		}
		void DisconnectMenuItemClick(object sender, System.EventArgs e) {
			dynaDis.Disconnect();
			statusBar.Text="Disconnected";
		}
		
		void PrepareMenuItemClick(object sender, System.EventArgs e) {
			dynaDis.Prepare();
			statusBar.Text="Prepared";
		}
		
		void FillMenuItemClick(object sender, System.EventArgs e) {
			dynaDis.Fill();
			statusBar.Text="Filled";
		}
		void UpdateMenuItemClick(object sender, System.EventArgs e) {
			dynaDis.Update();
			statusBar.Text="Updated";
		}
		
		void SelectButtonClick(object sender, System.EventArgs e) {
			new Thread(new ThreadStart(SelectStart)).Start();
		}
		void SelectStart() {
			dynaDis.Select();
			statusBar.Text="Selected";
		}
		void SelectProgress(int progress) {
			selectProgressBar.Value=progress;
		}
		
		void MainFormLoad(object sender, System.EventArgs e) {
			dataSourceTextBox.Text=dynaDis.dataSource;
			initialCatalogTextBox.Text=dynaDis.initialCatalog;
			
			minWaitingTextBox.Text=dynaDis.minWaiting.ToString();
			maxWaitingTextBox.Text=dynaDis.maxWaiting.ToString();
			maxTouringTextBox.Text=dynaDis.maxTouring.ToString();
			
			dayComboBox.Text=dynaDis.day.ToString();
		}
		
		void DataSourceTextBoxValidating(object sender, System.ComponentModel.CancelEventArgs e) {
			dynaDis.dataSource=dataSourceTextBox.Text;
		}
		void InitialCatalogTextBoxValidating(object sender, System.ComponentModel.CancelEventArgs e) {
			dynaDis.initialCatalog=initialCatalogTextBox.Text;
		}
		void MinWaitingTextBoxValidating(object sender, System.ComponentModel.CancelEventArgs e) {
			try {
				dynaDis.minWaiting=TimeSpan.Parse(minWaitingTextBox.Text);
			} catch { e.Cancel=true; }
		}
		void MaxWaitingTextBoxValidating(object sender, System.ComponentModel.CancelEventArgs e) {
			try {
				dynaDis.maxWaiting=TimeSpan.Parse(maxWaitingTextBox.Text);
			} catch { e.Cancel=true; }
		}
		void MaxTouringTextBoxValidating(object sender, System.ComponentModel.CancelEventArgs e) {
			try {
				dynaDis.maxTouring=TimeSpan.Parse(maxTouringTextBox.Text);
			} catch { e.Cancel=true; }
		}
		void DayComboBoxValidating(object sender, System.ComponentModel.CancelEventArgs e) {
			if(!dayComboBox.Items.Contains(dayComboBox.Text)) {
				e.Cancel=true;
			} else {
				try {
					dynaDis.day=Int32.Parse(dayComboBox.Text);
				} catch { e.Cancel=true; }
			}
		}
		
	}
}
