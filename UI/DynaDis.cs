/* aputze */

using System;
using System.Collections;

using System.Data;
using System.Data.SqlClient;

namespace DynaDis {
	public delegate void ProgressHandler(int progress);
	
	public class DynaDis {
		public string dataSource="localhost";
		public string initialCatalog="DynaDis";
		
		public TimeSpan minWaiting=new TimeSpan(0,5,0);
		public TimeSpan maxWaiting=new TimeSpan(0,15,0);
		
		public TimeSpan maxTouring=new TimeSpan(8,0,0);
		
		public int day=1;
		
		public DataSet dataSet;
		
		protected DataTable trainTable;
		protected DataTable deviceTable;
		protected DataTable employeeTable;
		
		protected SqlConnection connection;
		protected SqlDataAdapter dataAdapter1; //TrainTable
		protected SqlDataAdapter dataAdapter2; //DeviceTable
		protected SqlDataAdapter dataAdapter3; //EmployeeTable
		
		protected Hashtable A;
		
		protected ArrayList T;
		protected ArrayList t;
		
		public DynaDis() {
			dataSet=new DataSet("DynaDis");
			
			trainTable=dataSet.Tables.Add("TrainTable");
			deviceTable=dataSet.Tables.Add("DeviceTable");
			employeeTable=dataSet.Tables.Add("EmployeeTable");
		}
		
		public void Connect() {
			connection=new SqlConnection("Data Source="+dataSource+";"+
			                             "Initial Catalog="+initialCatalog+";"+
			                             "Integrated Security=true;");
			connection.Open();
		}
		public void Disconnect() {
			connection.Close();
		}
		
		public void Prepare() {
			dataAdapter1=new SqlDataAdapter("SELECT * FROM TrainTable WHERE (Available & "+day+")=1", connection);
			
			dataAdapter1.UpdateCommand=new SqlCommand("UPDATE TrainTable SET "+
			                                          "Departure=@1,DepartureStation=@2,"+
			                                          "Arrival=@3,ArrivalStation=@4,"+
			                                          "Available=@5,"+
			                                          "ToDo=@6,Done=@7"+
			                                          " WHERE TrainId=@0", connection);
			dataAdapter1.UpdateCommand.Parameters.Add("@0",SqlDbType.Int,4,"TrainId");
			dataAdapter1.UpdateCommand.Parameters.Add("@1",SqlDbType.DateTime,8,"Departure");
			dataAdapter1.UpdateCommand.Parameters.Add("@2",SqlDbType.VarChar,50,"DepartureStation");
			dataAdapter1.UpdateCommand.Parameters.Add("@3",SqlDbType.DateTime,8,"Arrival");
			dataAdapter1.UpdateCommand.Parameters.Add("@4",SqlDbType.VarChar,50,"ArrivalStation");
			dataAdapter1.UpdateCommand.Parameters.Add("@5",SqlDbType.Int,4,"Available");
			dataAdapter1.UpdateCommand.Parameters.Add("@6",SqlDbType.Int,4,"ToDo");
			dataAdapter1.UpdateCommand.Parameters.Add("@7",SqlDbType.Int,4,"Done");
			
			dataAdapter1.InsertCommand=new SqlCommand("INSERT INTO TrainTable "+
			                                          "(Departure,DepartureStation,"+
			                                          "Arrival,ArrivalStation,"+
			                                          "Available,"+
			                                          "ToDo,Done)"+
			                                          " VALUES "+
			                                          "(@0,@1,@2,@3,@4,@5,@6)", connection);
			dataAdapter1.InsertCommand.Parameters.Add("@0",SqlDbType.DateTime,8,"Departure");
			dataAdapter1.InsertCommand.Parameters.Add("@1",SqlDbType.VarChar,50,"DepartureStation");
			dataAdapter1.InsertCommand.Parameters.Add("@2",SqlDbType.DateTime,8,"Arrival");
			dataAdapter1.InsertCommand.Parameters.Add("@3",SqlDbType.VarChar,50,"ArrivalStation");
			dataAdapter1.InsertCommand.Parameters.Add("@4",SqlDbType.Int,4,"Available");
			dataAdapter1.InsertCommand.Parameters.Add("@5",SqlDbType.Int,4,"ToDo");
			dataAdapter1.InsertCommand.Parameters.Add("@6",SqlDbType.Int,4,"Done");
			
			dataAdapter1.DeleteCommand=new SqlCommand("DELETE FROM TrainTable WHERE TrainId=@0", connection);
			dataAdapter1.DeleteCommand.Parameters.Add("@0",SqlDbType.Int,4,"TrainId");
			
			dataAdapter2=new SqlDataAdapter("SELECT * FROM DeviceTable WHERE (Available & "+day+")=1", connection);
			
			dataAdapter2.UpdateCommand=new SqlCommand("UPDATE DeviceTable SET "+
			                                          "Vendor=@1,Model=@2,"+
			                                          "Available=@3"+
			                                          " WHERE DeviceId=@0", connection);
			dataAdapter2.UpdateCommand.Parameters.Add("@0",SqlDbType.Int,4,"DeviceId");
			dataAdapter2.UpdateCommand.Parameters.Add("@1",SqlDbType.VarChar,50,"Vendor");
			dataAdapter2.UpdateCommand.Parameters.Add("@2",SqlDbType.VarChar,50,"Model");
			dataAdapter2.UpdateCommand.Parameters.Add("@3",SqlDbType.Int,4,"Available");
			
			dataAdapter2.InsertCommand=new SqlCommand("INSERT INTO DeviceTable "+
			                                          "(Vendor,Model,Available)"+
			                                          " VALUES "+
			                                          "(@0,@1,@2)", connection);
			dataAdapter2.InsertCommand.Parameters.Add("@0",SqlDbType.VarChar,50,"Vendor");
			dataAdapter2.InsertCommand.Parameters.Add("@1",SqlDbType.VarChar,50,"Model");
			dataAdapter2.InsertCommand.Parameters.Add("@2",SqlDbType.Int,4,"Available");
			
			dataAdapter2.DeleteCommand=new SqlCommand("DELETE FROM DeviceTable WHERE DeviceId=@0", connection);
			dataAdapter2.DeleteCommand.Parameters.Add("@0",SqlDbType.Int,4,"DeviceId");
			
			dataAdapter3=new SqlDataAdapter("SELECT * FROM EmployeeTable WHERE (Available & "+day+")=1", connection);
			
			dataAdapter3.UpdateCommand=new SqlCommand("UPDATE EmployeeTable SET "+
			                                          "DeviceId=@1,"+
			                                          "Name=@2,Address=@3,"+
			                                          "Station=@4,Available=@5"+
			                                          " WHERE EmployeeId=@0", connection);
			dataAdapter3.UpdateCommand.Parameters.Add("@0",SqlDbType.Int,4,"EmployeeId");
			dataAdapter3.UpdateCommand.Parameters.Add("@1",SqlDbType.Int,4,"DeviceId");
			dataAdapter3.UpdateCommand.Parameters.Add("@2",SqlDbType.VarChar,50,"Name");
			dataAdapter3.UpdateCommand.Parameters.Add("@3",SqlDbType.VarChar,50,"Address");
			dataAdapter3.UpdateCommand.Parameters.Add("@4",SqlDbType.VarChar,50,"Station");
			dataAdapter3.UpdateCommand.Parameters.Add("@5",SqlDbType.Int,4,"Available");
			
			dataAdapter3.InsertCommand=new SqlCommand("INSERT INTO EmployeeTable "+
			                                          "(DeviceId,"+
			                                          "Name,Address,"+
			                                          "Station,Available)"+
			                                          " VALUES "+
			                                          "(@0,@1,@2,@3,@4)", connection);
			dataAdapter3.InsertCommand.Parameters.Add("@0",SqlDbType.Int,4,"DeviceId");
			dataAdapter3.InsertCommand.Parameters.Add("@1",SqlDbType.VarChar,50,"Name");
			dataAdapter3.InsertCommand.Parameters.Add("@2",SqlDbType.VarChar,50,"Address");
			dataAdapter3.InsertCommand.Parameters.Add("@3",SqlDbType.VarChar,50,"Station");
			dataAdapter3.InsertCommand.Parameters.Add("@4",SqlDbType.Int,4,"Available");
			
			dataAdapter3.DeleteCommand=new SqlCommand("DELETE FROM EmployeeTable WHERE EmployeeId=@0", connection);
			dataAdapter3.DeleteCommand.Parameters.Add("@0",SqlDbType.Int,4,"EmployeeId");
		}
		
		public void Fill() {
			dataSet.Clear();
			
			dataAdapter1.FillSchema(dataSet,SchemaType.Source,"TrainTable");
			dataAdapter1.Fill(dataSet,"TrainTable");
			dataAdapter2.FillSchema(dataSet,SchemaType.Source,"DeviceTable");
			dataAdapter2.Fill(dataSet,"DeviceTable");
			dataAdapter3.FillSchema(dataSet,SchemaType.Source,"EmployeeTable");
			dataAdapter3.Fill(dataSet,"EmployeeTable");
		}
		public void Update() {
			dataAdapter1.Update(dataSet,"TrainTable");
			dataAdapter2.Update(dataSet,"DeviceTable");
			dataAdapter3.Update(dataSet,"EmployeeTable");
		}
		
		public event ProgressHandler selectProgress;
		public void Select() {
			if(selectProgress!=null) { selectProgress(0); }
			
			DateTime dateTime1=DateTime.Now;
			Create_A();
			DateTime dateTime2=DateTime.Now;
			Create_T();
			DateTime dateTime3=DateTime.Now;
			Create_t();
			DateTime dateTime4=DateTime.Now;
			
			Console.WriteLine("Create_A() {0}",dateTime2-dateTime1);
			Console.WriteLine("Create_T() {0}",dateTime3-dateTime2);
			Console.WriteLine("Create_t() {0}",dateTime4-dateTime3);
			
			Console.WriteLine("|T|={0}",T.Count);
			Console.WriteLine("|t|={0}",t.Count);
			
			if(selectProgress!=null) { selectProgress(100); }
		}
		
		protected void Create_A() {
			A=new Hashtable();
			
			foreach(DataRow trainRow1 in trainTable.Rows) {
				foreach(DataRow trainRow2 in trainTable.Rows) {
					if((string)trainRow2["DepartureStation"]==(string)trainRow1["ArrivalStation"] &&
					   ((DateTime)trainRow2["Departure"]-(DateTime)trainRow1["Arrival"])>=minWaiting &&
					   ((DateTime)trainRow2["Departure"]-(DateTime)trainRow1["Arrival"])<=maxWaiting) {
						
						if(!A.Contains(trainRow1["TrainId"])) {
							A.Add(trainRow1["TrainId"],new ArrayList());
						}
						((ArrayList)A[trainRow1["TrainId"]]).Add(trainRow2);
					}
				}
			}
		}
		
		protected void Create_T() {
			T=new ArrayList();
			
			foreach(DataRow employeeRow in employeeTable.Rows) {
				ArrayList T1=new ArrayList();
				ArrayList T2=new ArrayList();
				
				foreach(DataRow trainRow1 in trainTable.Rows) {
					if((string)trainRow1["DepartureStation"]==(string)employeeRow["Station"]) {
						ArrayList tour1=new ArrayList();
						tour1.Add(trainRow1); T1.Add(tour1);
					}
				}
				
				while(T1.Count>0) {
					foreach(ArrayList tour1 in T1) {
						DataRow trainRow0=(DataRow)tour1[0];
						DataRow trainRow1=(DataRow)tour1[tour1.Count-1];
						
						try {
							foreach(DataRow trainRow2 in (ArrayList)A[trainRow1["TrainId"]]) {
								if(((DateTime)trainRow2["Arrival"]-(DateTime)trainRow0["Departure"])<=maxTouring) {
									ArrayList tour2=(ArrayList)tour1.Clone();
									tour2.Add(trainRow2); T2.Add(tour2);
									
									if((string)trainRow2["ArrivalStation"]==(string)employeeRow["Station"]) {
										T.Add(tour2);
									}
								}
							}
						} catch { continue; }
					}
					
					T1=T2;
					T2=new ArrayList();
				}
			}
		}
		
		protected void Create_t() {
			t=new ArrayList();
		}
	}
}
