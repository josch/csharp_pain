using System;
using System.Collections.Generic;
using System.Text;

namespace Explorer
{
    class LogicalDiskInfoCollection : System.Collections.CollectionBase
    {
        public void Add(LogicalDiskInfo ldi)
        {
            List.Add(ldi);
        }

        public void Remove(int index)
        {
            if (index > 0 || index < Count - 1)
            {
                List.RemoveAt(index);
            }
        }

        public LogicalDiskInfo Item(int index)
        {
            if (index > 0 || index < Count - 1)
            {
                return (LogicalDiskInfo)List[index];
            }
            else
            {
                return null;
            }
        }

        public LogicalDiskInfo Item(string name)
        {
            foreach (LogicalDiskInfo ldi in List)
            {
                if (ldi.Name == name)
                {
                    return ldi;
                }
            }
            return null;
        }
    }
}
