/*
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Management;
using System.Collections;
using System.Threading;
using System.IO;

namespace Explorer
{
    public partial class Explorer : Form
    {
        private LogicalDiskInfoCollection ldic = new LogicalDiskInfoCollection();
        private bool firstlyactualized = false;
        private Thread t;

        private delegate void RefreshlvMainHandler();

        public Explorer()
        {
            InitializeComponent();

            string[] drives = Directory.GetLogicalDrives();
            foreach (string drive in drives)
            {
                lvArbeitsplatz.Items.Add(drive.Substring(0, 2));
                lvArbeitsplatz.Items[lvArbeitsplatz.Items.Count - 1].SubItems.Add("Bitte warten...");
                lvArbeitsplatz.Items[lvArbeitsplatz.Items.Count - 1].SubItems.Add("Bitte warten...");
                lvArbeitsplatz.Items[lvArbeitsplatz.Items.Count - 1].SubItems.Add("Bitte warten...");

                tsddbArbeitsplatz.DropDownItems.Add(drive);
            }

            t = new Thread(new ThreadStart(FillLogicalDiskInfoCollection));
            t.Start();
        }

        void FillLogicalDiskInfoCollection()
        {
            ManagementObjectSearcher mos = new ManagementObjectSearcher("SELECT * FROM Win32_LogicalDisk");

            ManagementObjectCollection moc = mos.Get();

            foreach (ManagementBaseObject mo in moc)
            {
                LogicalDiskInfo ldi = new LogicalDiskInfo();
                ldi.Name = mo["Name"] != null ? mo["Name"].ToString() : "";
                ldi.Size = mo["Size"] != null ? UInt64.Parse(mo["Size"].ToString()) : 0;
                ldi.Freespace = mo["FreeSpace"] != null ? UInt64.Parse(mo["FreeSpace"].ToString()) : 0;
                ldi.Description = mo["Description"] != null ? mo["Description"].ToString() : "";
                ldi.VolumeName = mo["VolumeName"] != null ? mo["VolumeName"].ToString() : "";

                bool exists = false;

                foreach (LogicalDiskInfo ldi2 in ldic)
                {
                    if (ldi.Name == ldi2.Name)
                    {
                        exists = true;
                        break;
                    }
                }

                if (exists)
                {
                    LogicalDiskInfo ldi3 = ldic.Item(ldi.Name);
                    ldi3.VolumeName = ldi.VolumeName;
                    ldi3.Size = ldi.Size;
                    ldi3.Freespace = ldi.Freespace;
                    ldi3.Description = ldi.Description;
                }
                else
                {
                    ldic.Add(ldi);
                }
            }

            if (!firstlyactualized)
            {
                if (this != null)
                {
                    this.Invoke(new RefreshlvMainHandler(RefreshlvMain));
                    firstlyactualized = true;
                }
            }
        }

        private void lvMain_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            lvArbeitsplatz.ListViewItemSorter = new ListViewItemComparer(e.Column);
        }

        class ListViewItemComparer : IComparer
        {
            private int col;
            public ListViewItemComparer(int column)
            {
                col = column;
            }
            public int Compare(object x, object y)
            {
                switch (col)
                {
                    case 0:
                        return String.Compare(((ListViewItem)x).SubItems[col].Text, ((ListViewItem)y).SubItems[col].Text);
                    case 1:
                        string[] tokenA = ((ListViewItem)x).SubItems[col].Text.Split(' ');
                        decimal sizeA = 0;
                        switch (tokenA[1])
                        {
                            case "kB":
                                sizeA = decimal.Parse(tokenA[0]) * 1024;
                                break;
                            case "MB":
                                sizeA = decimal.Parse(tokenA[0]) * 1048576;
                                break;
                            case "GB":
                                sizeA = decimal.Parse(tokenA[0]) * 1073741824;
                                break;
                            default:
                                sizeA = decimal.Parse(tokenA[0]);
                                break;
                        }

                        string[] tokenB = ((ListViewItem)y).SubItems[col].Text.Split(' ');
                        decimal sizeB = 0;
                        switch (tokenB[1])
                        {
                            case "kB":
                                sizeB = decimal.Parse(tokenB[0]) * 1024;
                                break;
                            case "MB":
                                sizeB = decimal.Parse(tokenB[0]) * 1048576;
                                break;
                            case "GB":
                                sizeB = decimal.Parse(tokenB[0]) * 1073741824;
                                break;
                            default:
                                sizeB = decimal.Parse(tokenB[0]);
                                break;
                        }
                        return Math.Sign(sizeA - sizeB);
                    case 2:
                        goto case 1;
                    case 3:
                        goto case 0;
                    default:
                        return 0;
                }
            }
        }

        private void lvMain_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (lvArbeitsplatz.SelectedItems.Count)
            {
                case 0:
                    lProperties.Text = "Keine Elemente ausgew�hlt";
                    break;
                case 1:
                    LogicalDiskInfo ldi = ldic.Item(lvArbeitsplatz.SelectedItems[0].SubItems[0].Text.Substring(0, 2));
                    if (ldi != null)
                    {
                        string text = "Name: " + ldi.Name + "\n"
                            + "Size: " + ldi.Size + "\r\n"
                            + "Bezeichnung: " + ldi.VolumeName + "\r\n";
                        lProperties.Text = text;
                    }
                    else
                    {
                        lProperties.Text = "wat?\"" + lvArbeitsplatz.SelectedItems[0].SubItems[0].Text.Substring(0, 2) + "\"";
                    }
                    break;
                default:
                    lProperties.Text = lvArbeitsplatz.SelectedItems.Count.ToString() + " Elemente ausgew�hlt";
                    break;
            }
        }

        void RefreshlvMain()
        {
            foreach (LogicalDiskInfo ldi in ldic)
            {
                bool lviisfound = false;
                foreach (ListViewItem lvi in lvArbeitsplatz.Items)
                {
                    if (ldi.Name == lvi.SubItems[0].Text.Substring(0, 2))
                    {
                        switch (ldi.Description)
                        {
                            case "Lokale Festplatte":
                                lvi.ImageIndex = 2;
                                break;
                            case "CD":
                                lvi.ImageIndex = 3;
                                break;
                        }
                        lvi.SubItems[0].Text = ldi.Name + " (" + ldi.VolumeName + ")";
                        lvi.SubItems[1].Text = ConvertToUsableWithUnit(ldi.Size);
                        lvi.SubItems[2].Text = ConvertToUsableWithUnit(ldi.Freespace);
                        lvi.SubItems[3].Text = ldi.Description;
                        lviisfound = true;
                        break;
                    }
                }

                if (lviisfound == false)
                {
                    switch (ldi.Description)
                    {
                        case "Lokale Festplatte":
                            lvArbeitsplatz.Items.Add(ldi.Name + " (" + ldi.VolumeName + ")", 2);
                            break;
                        case "CD":
                            lvArbeitsplatz.Items.Add(ldi.Name + " (" + ldi.VolumeName + ")", 3);
                            break;
                        default:
                            lvArbeitsplatz.Items.Add(ldi.Name + " (" + ldi.VolumeName + ")");
                            break;
                    }
                    lvArbeitsplatz.Items[lvArbeitsplatz.Items.Count - 1].SubItems.Add(ConvertToUsableWithUnit(ldi.Size));
                    lvArbeitsplatz.Items[lvArbeitsplatz.Items.Count - 1].SubItems.Add(ConvertToUsableWithUnit(ldi.Freespace));
                    lvArbeitsplatz.Items[lvArbeitsplatz.Items.Count - 1].SubItems.Add(ldi.Description);
                }

                bool tsiisfound = false;

                foreach (ToolStripItem tsi in tsddbArbeitsplatz.DropDownItems)
                {
                    if (tsi.Name != "toolStripSeparator8")
                    {
                        if (ldi.Name == tsi.Text.Substring(0, 2))
                        {
                            switch (ldi.Description)
                            {
                                case "Lokale Festplatte":
                                    tsi.Image = FileSystemIcons.Images[2];
                                    break;
                                case "CD":
                                    tsi.Image = FileSystemIcons.Images[3];
                                    break;
                            }
                            tsiisfound = true;
                            break;
                        }
                    }
                }

                if (tsiisfound == false)
                {
                    switch (ldi.Description)
                    {
                        case "Lokale Festplatte":
                            tsddbArbeitsplatz.DropDownItems.Add(ldi.Name + "\\", FileSystemIcons.Images[2]);
                            break;
                        case "CD":
                            tsddbArbeitsplatz.DropDownItems.Add(ldi.Name + "\\", FileSystemIcons.Images[3]);
                            break;
                        default:
                            tsddbArbeitsplatz.DropDownItems.Add(ldi.Name + "\\");
                            break;
                    }
                }
            }
        }

        private void tsbAktualisieren_Click(object sender, EventArgs e)
        {
            RefreshlvMain();
        }

        private void GlobalTimer_Tick(object sender, EventArgs e)
        {
            Thread t = new Thread(new ThreadStart(FillLogicalDiskInfoCollection));
            t.Start();
        }

        string ConvertToUsableWithUnit(UInt64 number)
        {
            if (number < 1024)
            {
                return number.ToString() + " Bytes";
            }
            else if (number >= 1024 && number < 1048576)
            {
                return Math.Round(Convert.ToDecimal(number) / 1024, 2).ToString() + " kB";
            }
            else if (number >= 1048576 && number < 1073741824)
            {
                return Math.Round(Convert.ToDecimal(number) / 1048576, 2).ToString() + " MB";
            }
            else if (number >= 1073741824)
            {
                return Math.Round(Convert.ToDecimal(number) / 1073741824, 2).ToString() + " GB";
            }
            return "";
        }

        private void tsbWechselnZu_Click(object sender, EventArgs e)
        {
            string[] blah = Directory.GetLogicalDrives();
            string text = "";
            foreach (string b in blah)
            {
                text += b;
            }
            MessageBox.Show(text);
        }

        /*
         * lvArbeitsplatz_ItemActivate
         * 
         * Wird ausgef�hrt, wenn im Arbeitsplatz ListView ein Item Aktiviert wird
         * ein Click auf ein
         *
        private void lvArbeitsplatz_ItemActivate(object sender, EventArgs e)
        {
            //Angew�hltes Laufwerk bestimmen
            string drive = lvArbeitsplatz.SelectedItems[0].SubItems[0].Text.Substring(0, 2) + "\\";

            //Ein Click Event auf den Arbeitsplatz Dropdownbutton ausf�hren, bei dem das im
            //Listview angew�hlte Laufwerk �bergeben wird

            //Toolstripitem des entsprechenden Laufwerksbuchstaben bestimmen
            ToolStripItem toolstripitem = null;
            foreach (ToolStripItem tsi in tsddbArbeitsplatz.DropDownItems)
            {
                if (tsi.Text == drive)
                {
                    toolstripitem = tsi;
                    break;
                }
            }
            //Das Click Event Argument erstellen
            ToolStripItemClickedEventArgs b = new ToolStripItemClickedEventArgs(toolstripitem);
            //und ausf�hren
            tsddbArbeitsplatz_DropDownItemClicked(new object(), b);
            //In den Vordergrund bringen
            lvFolder.BringToFront();
        }

        /*
         * tsddbArbeitsplatz_DropDownItemClicked
         * 
         * Wird ausgef�hrt, wenn ein Element des Arbeitsplatz Dropdown Men�s angeklickt wurde
         * und wenn aus dem Listview ein Element aktiviert wurde
         *
        private void tsddbArbeitsplatz_DropDownItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            //angeklicktes Item holen
            ToolStripItem tsi = e.ClickedItem;
            //Button nach dem angew�hlten Element benennen
            tsddbArbeitsplatz.Text = tsi.Text;
            //alle eventuellen dropdownitems nach dem Arbeitsplatz dropdownitem l�schen
            while (tsFolders.Items.Count - 1 > 0)
            {
                tsFolders.Items.RemoveAt(tsFolders.Items.Count - 1);
            }

            //�berpr�fen ob der Eintrag Arbeitsplatz angew�hlt wurde
            if (tsi.Text != "Arbeitsplatz")
            {
                //die Folder Liste von Eintr�gen befreien
                lvFolder.Items.Clear();

                //Alle Ordner des angew�hlten Laufwerks holen
                DirectoryInfo di = new DirectoryInfo(tsi.Text);
                DirectoryInfo[] dirs = di.GetDirectories();
                FileInfo[] files = di.GetFiles();

                if (dirs.Length > 0)
                {
                    //wenn nicht, dann neuen Dropdownbutton erstellen und ihm ein Clickevent zuweisen
                    ToolStripDropDownButton tsddbNeu = new ToolStripDropDownButton("Bitte Ausw�hlen...");
                    tsddbNeu.DropDownItemClicked += new ToolStripItemClickedEventHandler(tsddbNeu_DropDownItemClicked);

                    //jeden Ordner dem neuen Dropdownmen� und dem Folder Listview hinzuf�gen
                    foreach (DirectoryInfo dir in dirs)
                    {
                        tsddbNeu.DropDownItems.Add(dir.Name + "\\");
                        ListViewItem lvi = new ListViewItem(dir.Name, 0);
                        lvFolder.Items.Add(lvi);
                    }

                    //das dropdownmen� dem Toolstrip hinzuf�gen
                    tsFolders.Items.Add(tsddbNeu);
                }

                foreach (FileInfo fi in files)
                {
                    ListViewItem lvi = new ListViewItem(fi.Name, 1);
                    lvFolder.Items.Add(lvi);
                }


                //die Adressleiste Aktualisieren
                tscbAdresse.Text = tsi.Text;
                lvFolder.BringToFront();
            }
            else
            {
                //die Adressleiste aktualisieren
                tscbAdresse.Text = "Arbeitsplatz";
                lvArbeitsplatz.BringToFront();
            }

            tsddbArbeitsplatz.Image = e.ClickedItem.Image;
        }

        private void tsddbNeu_DropDownItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            string path = "";
            bool found = false;
            int lastfolderitemindex = 0;

            foreach (ToolStripItem folderitem in tsFolders.Items)
            {
                ToolStripDropDownButton dropdownitem = (ToolStripDropDownButton)folderitem;
                foreach (ToolStripItem toolstripitem in dropdownitem.DropDownItems)
                {
                    if (toolstripitem == e.ClickedItem)
                    {
                        path += toolstripitem.Text;
                        folderitem.Text = toolstripitem.Text;
                        found = true;
                        break;
                    }
                }
                if (found)
                {
                    break;
                }
                else
                {
                    path += folderitem.Text;
                    lastfolderitemindex++;
                }
            }
            while (tsFolders.Items.Count - 1 > lastfolderitemindex)
            {
                tsFolders.Items.RemoveAt(tsFolders.Items.Count - 1);
            }

            DirectoryInfo di = new DirectoryInfo(path);
            DirectoryInfo[] dirs = di.GetDirectories();
            FileInfo[] files = di.GetFiles();

            lvFolder.Items.Clear();

            if (dirs.Length > 0)
            {
                ToolStripDropDownButton tsddbNeu = new ToolStripDropDownButton("Bitte Ausw�hlen...");
                tsddbNeu.DropDownItemClicked += new ToolStripItemClickedEventHandler(tsddbNeu_DropDownItemClicked);

                foreach (DirectoryInfo dir in dirs)
                {
                    tsddbNeu.DropDownItems.Add(dir.Name + "\\");
                    ListViewItem lvi = new ListViewItem(dir.Name, 0);
                    lvFolder.Items.Add(lvi);
                }

                tsFolders.Items.Add(tsddbNeu);
            }

            foreach (FileInfo fi in files)
            {
                ListViewItem lvi = new ListViewItem(fi.Name, 1);
                lvFolder.Items.Add(lvi);
            }

            tscbAdresse.Text = path;
        }

        private void Explorer_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (t.ThreadState == ThreadState.Running)
            {
                t.Abort();
            }
        }

        private void lvFolder_ItemActivate(object sender, EventArgs e)
        {
            string folder = lvFolder.SelectedItems[0].SubItems[0].Text + "\\";

            ToolStripItem toolstripitem = null;
            ToolStripDropDownButton dropdownbutton = (ToolStripDropDownButton)tsFolders.Items[tsFolders.Items.Count - 1];
            foreach (ToolStripItem tsi in dropdownbutton.DropDownItems)
            {
                if (tsi.Text == folder)
                {
                    toolstripitem = tsi;
                    break;
                }
            }
            tsddbNeu_DropDownItemClicked(new object(), new ToolStripItemClickedEventArgs(toolstripitem));
        }

        private void tsbAufw�rts_Click(object sender, EventArgs e)
        {
            if (tsFolders.Items[tsFolders.Items.Count - 1].Text == "Bitte Ausw�hlen...")
            {
                if (tsFolders.Items.Count == 2)
                {
                    tsddbArbeitsplatz_DropDownItemClicked(new object(), new ToolStripItemClickedEventArgs(tsmiArbeitsplatz));
                }
                else if (tsFolders.Items.Count == 3)
                {
                    ToolStripItem toolstripitem = null;
                    foreach (ToolStripItem tsi in tsddbArbeitsplatz.DropDownItems)
                    {
                        if (tsi.Text == tsddbArbeitsplatz.Text)
                        {
                            toolstripitem = tsi;
                            break;
                        }
                    }
                    tsddbArbeitsplatz_DropDownItemClicked(new object(), new ToolStripItemClickedEventArgs(toolstripitem));
                }
                else if (tsFolders.Items.Count > 3)
                {
                    ToolStripItem toolstripitem = null;
                    ToolStripDropDownButton dropdownbutton = (ToolStripDropDownButton)tsFolders.Items[tsFolders.Items.Count - 3];
                    foreach (ToolStripItem tsi in dropdownbutton.DropDownItems)
                    {
                        if (tsi.Text == dropdownbutton.Text)
                        {
                            toolstripitem = tsi;
                            break;
                        }
                    }
                    tsddbNeu_DropDownItemClicked(new object(), new ToolStripItemClickedEventArgs(toolstripitem));
                }
            }
        }

        private void tscbAdresse_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return)
            {
                MessageBox.Show(tscbAdresse.Text);
                e.Handled = true;
            }
        }
    }
}
*/