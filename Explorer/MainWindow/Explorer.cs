using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Management;
using System.Collections;
using System.Threading;
using System.IO;
using System.Media;

namespace Explorer
{
    public partial class Explorer : Form
    {
        private LogicalDiskInfoCollection ldic = new LogicalDiskInfoCollection();
        private bool firstlyactualized = false;
        private Thread t;
        private string[] History = new string[10];
        private string[] Future = new string[10];
        private string[] Past = new string[10];
        private SoundPlayer sp = null;

        private delegate void RefreshlvMainHandler();

        public Explorer()
        {
            InitializeComponent();

            string[] drives = Directory.GetLogicalDrives();
            foreach (string drive in drives)
            {
                ListViewItem lvi = new ListViewItem();
                lvi.Name = drive;
                lvi.Text = drive.Substring(0, 2);
                lvi.SubItems.Add("Bitte warten...");
                lvi.SubItems.Add("Bitte warten...");
                lvi.SubItems.Add("Bitte warten...");
                lvArbeitsplatz.Items.Add(lvi);

                ToolStripMenuItem tsmi = new ToolStripMenuItem();
                tsmi.Name = drive;
                tsmi.Text = drive;
                tssbArbeitsplatz.DropDownItems.Add(tsmi);
            }

            t = new Thread(new ThreadStart(FillLogicalDiskInfoCollection));
            t.Start();
        }

        void FillLogicalDiskInfoCollection()
        {
            ManagementObjectSearcher mos = new ManagementObjectSearcher("SELECT * FROM Win32_LogicalDisk");

            ManagementObjectCollection moc = mos.Get();

            foreach (ManagementBaseObject mo in moc)
            {
                LogicalDiskInfo ldi = new LogicalDiskInfo();
                ldi.Name = mo["Name"] != null ? mo["Name"].ToString() : "";
                ldi.Size = mo["Size"] != null ? UInt64.Parse(mo["Size"].ToString()) : 0;
                ldi.Freespace = mo["FreeSpace"] != null ? UInt64.Parse(mo["FreeSpace"].ToString()) : 0;
                ldi.Description = mo["Description"] != null ? mo["Description"].ToString() : "";
                ldi.VolumeName = mo["VolumeName"] != null ? mo["VolumeName"].ToString() : "";

                bool exists = false;

                foreach (LogicalDiskInfo ldi2 in ldic)
                {
                    if (ldi.Name == ldi2.Name)
                    {
                        exists = true;
                        break;
                    }
                }

                if (exists)
                {
                    LogicalDiskInfo ldi3 = ldic.Item(ldi.Name);
                    ldi3.VolumeName = ldi.VolumeName;
                    ldi3.Size = ldi.Size;
                    ldi3.Freespace = ldi.Freespace;
                    ldi3.Description = ldi.Description;
                }
                else
                {
                    ldic.Add(ldi);
                }
            }

            if(!firstlyactualized)
            {
                if (this != null)
                {
                    this.Invoke(new RefreshlvMainHandler(RefreshlvArbeitsplatz));
                    firstlyactualized = true;
                }
            }
        }

        class ListViewItemComparer : IComparer
        {
            private int col;
            public ListViewItemComparer(int column)
            {
                col = column;
            }
            public int Compare(object x, object y)
            {
                switch (col)
                {
                    case 0:
                        return String.Compare(((ListViewItem)x).SubItems[col].Text, ((ListViewItem)y).SubItems[col].Text);
                    case 1:
                        string[] tokenA = ((ListViewItem)x).SubItems[col].Text.Split(' ');
                        decimal sizeA = 0;
                        switch(tokenA[1])
                        {
                            case "kB":
                                sizeA = decimal.Parse(tokenA[0])*1024;
                                break;
                            case "MB":
                                sizeA = decimal.Parse(tokenA[0]) * 1048576;
                                break;
                            case "GB":
                                sizeA = decimal.Parse(tokenA[0]) * 1073741824;
                                break;
                            default:
                                sizeA = decimal.Parse(tokenA[0]);
                                break;
                        }

                        string[] tokenB = ((ListViewItem)y).SubItems[col].Text.Split(' ');
                        decimal sizeB = 0;
                        switch (tokenB[1])
                        {
                            case "kB":
                                sizeB = decimal.Parse(tokenB[0]) * 1024;
                                break;
                            case "MB":
                                sizeB = decimal.Parse(tokenB[0]) * 1048576;
                                break;
                            case "GB":
                                sizeB = decimal.Parse(tokenB[0]) * 1073741824;
                                break;
                            default:
                                sizeB = decimal.Parse(tokenB[0]);
                                break;
                        }
                        return Math.Sign(sizeA - sizeB);
                    case 2:
                        goto case 1;
                    case 3:
                        goto case 0;
                    default:
                        return 0;
                }
            }
        }

        void RefreshlvArbeitsplatz()
        {
            foreach (LogicalDiskInfo ldi in ldic)
            {
                bool lviisfound = false;
                foreach (ListViewItem lvi in lvArbeitsplatz.Items)
                {
                    if (ldi.Name == lvi.SubItems[0].Text.Substring(0, 2))
                    {
                        switch (ldi.Description)
                        {
                            case "Lokale Festplatte":
                                lvi.ImageIndex = 2;
                                break;
                            case "CD":
                                lvi.ImageIndex = 3;
                                break;
                        }
                        lvi.SubItems[0].Text = ldi.Name + " (" + ldi.VolumeName + ")";
                        lvi.SubItems[1].Text = ConvertToUsableWithUnit(ldi.Size);
                        lvi.SubItems[2].Text = ConvertToUsableWithUnit(ldi.Freespace);
                        lvi.SubItems[3].Text = ldi.Description;
                        lviisfound = true;
                        break;
                    }
                }
                
                if(lviisfound == false)
                {
                    switch (ldi.Description)
                    {
                        case "Lokale Festplatte":
                            lvArbeitsplatz.Items.Add(ldi.Name + " (" + ldi.VolumeName + ")", 2);
                            break;
                        case "CD":
                            lvArbeitsplatz.Items.Add(ldi.Name + " (" + ldi.VolumeName + ")", 3);
                            break;
                        default:
                            lvArbeitsplatz.Items.Add(ldi.Name + " (" + ldi.VolumeName + ")");
                            break;
                    }
                    lvArbeitsplatz.Items[lvArbeitsplatz.Items.Count - 1].SubItems.Add(ConvertToUsableWithUnit(ldi.Size));
                    lvArbeitsplatz.Items[lvArbeitsplatz.Items.Count - 1].SubItems.Add(ConvertToUsableWithUnit(ldi.Freespace));
                    lvArbeitsplatz.Items[lvArbeitsplatz.Items.Count - 1].SubItems.Add(ldi.Description);
                }
                
                bool tsiisfound = false;

                foreach (ToolStripItem tsi in tssbArbeitsplatz.DropDownItems)
                {
                    if (tsi.Name != "toolStripSeparator8")
                    {
                        if (ldi.Name == tsi.Text.Substring(0, 2))
                        {
                            switch (ldi.Description)
                            {
                                case "Lokale Festplatte":
                                    tsi.Image = FileSystemIcons.Images[2];
                                    break;
                                case "CD":
                                    tsi.Image = FileSystemIcons.Images[3];
                                    break;
                            }
                            tsiisfound = true;
                            break;
                        }
                    }
                }
                
                if (tsiisfound == false)
                {
                    switch (ldi.Description)
                    {
                        case "Lokale Festplatte":
                            tssbArbeitsplatz.DropDownItems.Add(ldi.Name + "\\", FileSystemIcons.Images[2]);
                            break;
                        case "CD":
                            tssbArbeitsplatz.DropDownItems.Add(ldi.Name + "\\", FileSystemIcons.Images[3]);
                            break;
                        default:
                            tssbArbeitsplatz.DropDownItems.Add(ldi.Name + "\\");
                            break;
                    }
                }
            }
        }

        private void tsbAktualisieren_Click(object sender, EventArgs e)
        {
            RefreshlvArbeitsplatz();
        }

        private void GlobalTimer_Tick(object sender, EventArgs e)
        {
            Thread t = new Thread(new ThreadStart(FillLogicalDiskInfoCollection));
            t.Start();
        }

        string ConvertToUsableWithUnit(UInt64 number)
        {
            if (number < 1024)
            {
                return number.ToString() + " Bytes";
            }
            else if (number >= 1024 && number < 1048576)
            {
                return Math.Round(Convert.ToDecimal(number) / 1024, 2).ToString() + " kB";
            }
            else if (number >= 1048576 && number < 1073741824)
            {
                return Math.Round(Convert.ToDecimal(number) / 1048576, 2).ToString() + " MB";
            }
            else if (number >= 1073741824)
            {
                return Math.Round(Convert.ToDecimal(number) / 1073741824, 2).ToString() + " GB";
            }
            return "";
        }

        private void Explorer_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (t.ThreadState == ThreadState.Running)
            {
                t.Abort();
            }
        }

        private void lvArbeitsplatz_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            lvArbeitsplatz.ListViewItemSorter = new ListViewItemComparer(e.Column);
        }

        private void lvArbeitsplatz_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (lvArbeitsplatz.SelectedItems.Count)
            {
                case 0:
                    lProperties.Text = "Keine Elemente ausgew�hlt";
                    break;
                case 1:
                    LogicalDiskInfo ldi = ldic.Item(lvArbeitsplatz.SelectedItems[0].SubItems[0].Text.Substring(0, 2));
                    if (ldi != null)
                    {
                        string text = "Name: " + ldi.Name + "\n"
                            + "Size: " + ldi.Size + "\r\n"
                            + "Bezeichnung: " + ldi.VolumeName + "\r\n";
                        lProperties.Text = text;
                    }
                    else
                    {
                        lProperties.Text = "wat?\"" + lvArbeitsplatz.SelectedItems[0].SubItems[0].Text.Substring(0, 2) + "\"";
                    }
                    break;
                default:
                    lProperties.Text = lvArbeitsplatz.SelectedItems.Count.ToString() + " Elemente ausgew�hlt";
                    break;
            }
        }

        private void tssbArbeitsplatz_DropDownItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            ToolStripItem clickeditem = e.ClickedItem;

            if (clickeditem.Name == "tsmiArbeitsplatz")
            {
                lvArbeitsplatz.BringToFront();
                CleartsFolders();
                tssbArbeitsplatz.Text = "Arbeitsplatz";
                tscbAdresse.Text = "Arbeitsplatz";
                UpdateHistory("Arbeitsplatz");
                BuildHistoryItems();
            }
            else
            {
                DisplayFolder(clickeditem.Name);
                BuildtsFoldersFromPath(clickeditem.Name);
                tscbAdresse.Text = clickeditem.Name;
                UpdateHistory(clickeditem.Name);
                BuildHistoryItems();
            }
        }

        void DisplayFolder(string path)
        {
            DirectoryInfo di = new DirectoryInfo(path);
            if (di.Exists)
            {
                DirectoryInfo[] dirs = di.GetDirectories();
                FileInfo[] files = di.GetFiles();

                lvFolder.Items.Clear();

                foreach (DirectoryInfo directory in dirs)
                {
                    ListViewItem item = new ListViewItem();
                    item.Name = path + directory.Name + "\\";
                    item.Text = directory.Name;
                    item.ImageIndex = 0;
                    lvFolder.Items.Add(item);
                }

                foreach (FileInfo file in files)
                {
                    ListViewItem item = new ListViewItem();
                    item.Name = path + file.Name;
                    item.Text = file.Name;
                    item.ImageIndex = 1;
                    lvFolder.Items.Add(item);
                }

                lvFolder.BringToFront();
            }
            else
            {
                MessageBox.Show("Pfad existiert nicht: " + path);
            }
        }

        void CleartsFolders()
        {
            while(tsFolders.Items.Count > 1)
            {
                tsFolders.Items.RemoveAt(tsFolders.Items.Count - 1);
            }
        }

        void AddtsFoldersItem(string path, DirectoryInfo[] dirs, string text)
        {
            if (text != "")
            {
                ToolStripSplitButton dropdown = new ToolStripSplitButton();
                dropdown.Name = path;
                dropdown.Text = text;
                dropdown.DropDownItemClicked += new ToolStripItemClickedEventHandler(dropdown_DropDownItemClicked);
                dropdown.ButtonClick += new EventHandler(dropdown_ButtonClick);

                foreach (DirectoryInfo dir in dirs)
                {
                    ToolStripMenuItem item = new ToolStripMenuItem();
                    item.Name = path + dir.Name + "\\";
                    item.Text = dir.Name;
                    dropdown.DropDownItems.Add(item);
                }

                tsFolders.Items.Add(dropdown);
            }
            else
            {
                ToolStripDropDownButton dropdown = new ToolStripDropDownButton();
                dropdown.Name = path;
                dropdown.Text = "Bitte Ausw�hlen...";
                dropdown.DropDownItemClicked += new ToolStripItemClickedEventHandler(dropdown_DropDownItemClicked);

                foreach (DirectoryInfo dir in dirs)
                {
                    ToolStripMenuItem item = new ToolStripMenuItem();
                    item.Name = path + dir.Name + "\\";
                    item.Text = dir.Name;
                    dropdown.DropDownItems.Add(item);
                }

                tsFolders.Items.Add(dropdown);
            }
        }

        private void lvFolder_ItemActivate(object sender, EventArgs e)
        {
            if (lvFolder.SelectedItems.Count == 1)
            {
                ListViewItem activated = lvFolder.SelectedItems[0];
                DisplayFolder(activated.Name);
                BuildtsFoldersFromPath(activated.Name);
                tscbAdresse.Text = activated.Name;
                UpdateHistory(activated.Name);
                BuildHistoryItems();
            }
        }

        private void lvArbeitsplatz_ItemActivate(object sender, EventArgs e)
        {
            if (lvArbeitsplatz.SelectedItems.Count == 1)
            {
                ListViewItem activated = lvArbeitsplatz.SelectedItems[0];
                DisplayFolder(activated.Name);
                BuildtsFoldersFromPath(activated.Name);
                tscbAdresse.Text = activated.Name;
                UpdateHistory(activated.Name);
                BuildHistoryItems();
            }
        }

        void dropdown_DropDownItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            ToolStripItem clickeditem = e.ClickedItem;
            DisplayFolder(clickeditem.Name);
            BuildtsFoldersFromPath(clickeditem.Name);
            tscbAdresse.Text = clickeditem.Name;
            UpdateHistory(clickeditem.Name);
            BuildHistoryItems();
        }

        void dropdown_ButtonClick(object sender, EventArgs e)
        {
            ToolStripItem item = (ToolStripItem)sender;
            DisplayFolder(item.Name + item.Text + "\\");
            BuildtsFoldersFromPath(item.Name + item.Text + "\\");
            tscbAdresse.Text = item.Name + item.Text + "\\";
            UpdateHistory(item.Name + item.Text + "\\");
            BuildHistoryItems();
        }

        void BuildtsFoldersFromPath(string path)
        {
            CleartsFolders();
            string[] tokens = path.Split('\\');

            string actualpath = tokens[0] + "\\";

            tssbArbeitsplatz.Text = actualpath;

            for(int i = 1; i<tokens.Length; i++)
            {
                DirectoryInfo di = new DirectoryInfo(actualpath);
                if (di.Exists)
                {
                    DirectoryInfo[] dirs = di.GetDirectories();
                    if (dirs.Length > 0)
                    {
                        if (actualpath != path)
                        {
                            AddtsFoldersItem(actualpath, dirs, tokens[i]);
                            actualpath += tokens[i] + "\\";
                        }
                        else
                        {
                            AddtsFoldersItem(actualpath, dirs, "");
                        }
                    }
                }
            }
        }

        private void tssbArbeitsplatz_ButtonClick(object sender, EventArgs e)
        {
            if (tssbArbeitsplatz.Text == "Arbeitsplatz")
            {
                lvArbeitsplatz.BringToFront();
                CleartsFolders();
                tscbAdresse.Text = "Arbeitsplatz";
                UpdateHistory("Arbeitsplatz");
                BuildHistoryItems();
            }
            else
            {
                DisplayFolder(tssbArbeitsplatz.Text);
                BuildtsFoldersFromPath(tssbArbeitsplatz.Text);
                tscbAdresse.Text = tssbArbeitsplatz.Text;
                UpdateHistory(tssbArbeitsplatz.Text);
                BuildHistoryItems();
            }
        }

        private void tsbWechselnZu_Click(object sender, EventArgs e)
        {
            DisplayFolder(tscbAdresse.Text + "\\");
            BuildtsFoldersFromPath(tscbAdresse.Text + "\\");
            UpdateHistory(tscbAdresse.Text + "\\");
            BuildHistoryItems();
        }

        void UpdateHistory(string item)
        {
            if (string.Compare(item, History[0], true) != 0)
            {
                for (int i = 8; i >= 0; i--)
                {
                    History[i + 1] = History[i];
                }

                History[0] = item;
            }
        }

        void BuildHistoryItems()
        {
            UpdatetsmiGehe();
            UpdatetssbZur�ck();
        }

        void UpdatetsmiGehe()
        {
            CleartsmiGehe();
            foreach (string path in History)
            {
                if (path != null)
                {
                    ToolStripMenuItem button = new ToolStripMenuItem();
                    button.Name = path;
                    string[] tokens = path.Split('\\');
                    if (tokens.Length >= 2)
                    {
                        button.Text = tokens[tokens.Length - 2];
                    }
                    button.Click += new EventHandler(Historybutton_Click);
                    tsmiGehe.DropDownItems.Add(button);
                }
            }
        }

        void CleartsmiGehe()
        {
            while (tsmiGehe.DropDownItems.Count > 4)
            {
                tsmiGehe.DropDownItems.RemoveAt(tsmiGehe.DropDownItems.Count - 1);
            }
        }

        void Historybutton_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem clicked = (ToolStripMenuItem)sender;
            DisplayFolder(clicked.Name);
            BuildtsFoldersFromPath(clicked.Name);
            tscbAdresse.Text = clicked.Name;
            UpdateHistory(clicked.Name);
            BuildHistoryItems();
        }

        void UpdatetssbZur�ck()
        {
            CleartssbZur�ck();
            foreach (string path in History)
            {
                if (path != null)
                {
                    ToolStripMenuItem button = new ToolStripMenuItem();
                    button.Name = path;
                    string[] tokens = path.Split('\\');
                    if (tokens.Length >= 2)
                    {
                        button.Text = tokens[tokens.Length - 2];
                    }
                    tssbZur�ck.DropDownItems.Add(button);
                }
            }
            tssbZur�ck.Enabled = History.Length > 0;
        }

        void CleartssbZur�ck()
        {
            tssbZur�ck.DropDownItems.Clear();
        }

        private void tssbZur�ck_ButtonClick(object sender, EventArgs e)
        {
            DisplayFolder(History[1]);
            BuildtsFoldersFromPath(History[1]);
            tscbAdresse.Text = History[1];
            UpdateHistory(History[1]);
            BuildHistoryItems();
        }

        private void tsbAufw�rts_Click(object sender, EventArgs e)
        {
            string[] tokens = History[0].Split('\\');
            string path = tokens[0] + "\\";
            if (tokens.Length > 2)
            {
                for (int i = 1; i < tokens.Length - 2; i++)
                {
                    path += tokens[i] + "\\";
                }
                DisplayFolder(path);
                BuildtsFoldersFromPath(path);
                tscbAdresse.Text = path;
                UpdateHistory(path);
                BuildHistoryItems();
            }
            else
            {
                lvArbeitsplatz.BringToFront();
                CleartsFolders();
                tssbArbeitsplatz.Text = "Arbeitsplatz";
                tscbAdresse.Text = "Arbeitsplatz";
                UpdateHistory("Arbeitsplatz");
                BuildHistoryItems();
            }
        }

        private void tssbZur�ck_DropDownItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            ToolStripItem clicked = e.ClickedItem;
            DisplayFolder(clicked.Name);
            BuildtsFoldersFromPath(clicked.Name);
            tscbAdresse.Text = clicked.Name;
            UpdateHistory(clicked.Name);
            BuildHistoryItems();
        }

        private void lvFolder_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (lvFolder.SelectedItems.Count)
            {
                case 0:
                    lProperties.Text = "Kein Objekt ausgew�hlt";
                    break;
                case 1:
                    DirectoryInfo di = new DirectoryInfo(lvFolder.SelectedItems[0].Name);
                    if (di.Exists)
                    {
                        lProperties.Text = "directory";
                    }
                    else
                    {
                        FileInfo fi = new FileInfo(lvFolder.SelectedItems[0].Name);
                        if (fi.Exists)
                        {
                            lProperties.Text = "file";
                            tsbPlayPause.Enabled = true;
                        }
                    }
                    break;
                default:
                    lProperties.Text = lvFolder.SelectedItems.Count.ToString() + " Elemente ausw�hlt";
                    break;
            }
        }

        private void tsbPlayPause_Click(object sender, EventArgs e)
        {
            
            if (lvFolder.SelectedItems.Count == 1)
            {
                sp = new SoundPlayer(lvFolder.SelectedItems[0].Name);
                sp.Play();
            }
        }
    }
}