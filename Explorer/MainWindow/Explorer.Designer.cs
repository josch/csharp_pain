namespace Explorer
{
    partial class Explorer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Explorer));
            this.StatusZeile = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.tsbPlayPause = new System.Windows.Forms.ToolStripButton();
            this.toolStripProgressBar1 = new System.Windows.Forms.ToolStripProgressBar();
            this.Men�Leiste = new System.Windows.Forms.MenuStrip();
            this.dateiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.neuesFensterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fensterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ordnerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.bitmapToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.textdokumentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.archivToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.speichernunterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.druckenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.schlie�enToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bearbeitenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.r�ckg�ngigToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.ausschneidenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.kopierenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.einf�genToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.allesMarkierenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.markierungumkehrenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.suchenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ansichtToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripTextBox1 = new System.Windows.Forms.ToolStripTextBox();
            this.tsmiGehe = new System.Windows.Forms.ToolStripMenuItem();
            this.zur�ckToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vorw�rtsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aufw�rtsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
            this.extrasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.hilfeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.�berToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Werkzeugleiste = new System.Windows.Forms.ToolStrip();
            this.tssbZur�ck = new System.Windows.Forms.ToolStripSplitButton();
            this.tssbVorw�rts = new System.Windows.Forms.ToolStripSplitButton();
            this.tsbAufw�rts = new System.Windows.Forms.ToolStripButton();
            this.tsbAbbrechen = new System.Windows.Forms.ToolStripButton();
            this.tsbAktualisieren = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tslAdresse = new System.Windows.Forms.ToolStripLabel();
            this.tscbAdresse = new System.Windows.Forms.ToolStripComboBox();
            this.tsbWechselnZu = new System.Windows.Forms.ToolStripButton();
            this.toolStripTextBox2 = new System.Windows.Forms.ToolStripTextBox();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lProperties = new System.Windows.Forms.Label();
            this.lvArbeitsplatz = new System.Windows.Forms.ListView();
            this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader2 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader4 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader3 = new System.Windows.Forms.ColumnHeader();
            this.FileSystemIcons = new System.Windows.Forms.ImageList(this.components);
            this.tsFolders = new System.Windows.Forms.ToolStrip();
            this.tssbArbeitsplatz = new System.Windows.Forms.ToolStripSplitButton();
            this.tsmiArbeitsplatz = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.lvFolder = new System.Windows.Forms.ListView();
            this.columnHeader5 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader6 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader7 = new System.Windows.Forms.ColumnHeader();
            this.GlobalTimer = new System.Windows.Forms.Timer(this.components);
            this.process1 = new System.Diagnostics.Process();
            this.StatusZeile.SuspendLayout();
            this.Men�Leiste.SuspendLayout();
            this.Werkzeugleiste.SuspendLayout();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tsFolders.SuspendLayout();
            this.SuspendLayout();
            // 
            // StatusZeile
            // 
            this.StatusZeile.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.tsbPlayPause,
            this.toolStripProgressBar1});
            this.StatusZeile.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.Table;
            this.StatusZeile.Location = new System.Drawing.Point(0, 458);
            this.StatusZeile.Name = "StatusZeile";
            this.StatusZeile.RenderMode = System.Windows.Forms.ToolStripRenderMode.ManagerRenderMode;
            this.StatusZeile.Size = new System.Drawing.Size(816, 27);
            this.StatusZeile.SizingGrip = false;
            this.StatusZeile.TabIndex = 0;
            this.StatusZeile.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Spring = true;
            this.toolStripStatusLabel1.Text = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tsbPlayPause
            // 
            this.tsbPlayPause.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbPlayPause.Enabled = false;
            this.tsbPlayPause.Image = ((System.Drawing.Image)(resources.GetObject("tsbPlayPause.Image")));
            this.tsbPlayPause.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbPlayPause.Name = "tsbPlayPause";
            this.tsbPlayPause.Click += new System.EventHandler(this.tsbPlayPause_Click);
            // 
            // toolStripProgressBar1
            // 
            this.toolStripProgressBar1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.ImageAndText;
            this.toolStripProgressBar1.Name = "toolStripProgressBar1";
            this.toolStripProgressBar1.Size = new System.Drawing.Size(100, 20);
            this.toolStripProgressBar1.Text = "toolStripProgressBar1";
            // 
            // Men�Leiste
            // 
            this.Men�Leiste.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dateiToolStripMenuItem,
            this.bearbeitenToolStripMenuItem,
            this.ansichtToolStripMenuItem,
            this.toolStripTextBox1,
            this.tsmiGehe,
            this.extrasToolStripMenuItem,
            this.hilfeToolStripMenuItem});
            this.Men�Leiste.Location = new System.Drawing.Point(0, 0);
            this.Men�Leiste.Name = "Men�Leiste";
            this.Men�Leiste.Size = new System.Drawing.Size(816, 25);
            this.Men�Leiste.TabIndex = 1;
            // 
            // dateiToolStripMenuItem
            // 
            this.dateiToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.neuesFensterToolStripMenuItem,
            this.speichernunterToolStripMenuItem,
            this.toolStripSeparator4,
            this.druckenToolStripMenuItem,
            this.toolStripSeparator5,
            this.schlie�enToolStripMenuItem});
            this.dateiToolStripMenuItem.Name = "dateiToolStripMenuItem";
            this.dateiToolStripMenuItem.Text = "Datei";
            // 
            // neuesFensterToolStripMenuItem
            // 
            this.neuesFensterToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fensterToolStripMenuItem,
            this.ordnerToolStripMenuItem,
            this.toolStripSeparator6,
            this.bitmapToolStripMenuItem,
            this.textdokumentToolStripMenuItem,
            this.archivToolStripMenuItem});
            this.neuesFensterToolStripMenuItem.Name = "neuesFensterToolStripMenuItem";
            this.neuesFensterToolStripMenuItem.Text = "Neu";
            // 
            // fensterToolStripMenuItem
            // 
            this.fensterToolStripMenuItem.Name = "fensterToolStripMenuItem";
            this.fensterToolStripMenuItem.Text = "Fenster";
            // 
            // ordnerToolStripMenuItem
            // 
            this.ordnerToolStripMenuItem.Name = "ordnerToolStripMenuItem";
            this.ordnerToolStripMenuItem.Text = "Ordner";
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            // 
            // bitmapToolStripMenuItem
            // 
            this.bitmapToolStripMenuItem.Name = "bitmapToolStripMenuItem";
            this.bitmapToolStripMenuItem.Text = "Bitmap";
            // 
            // textdokumentToolStripMenuItem
            // 
            this.textdokumentToolStripMenuItem.Name = "textdokumentToolStripMenuItem";
            this.textdokumentToolStripMenuItem.Text = "Textdokument";
            // 
            // archivToolStripMenuItem
            // 
            this.archivToolStripMenuItem.Name = "archivToolStripMenuItem";
            this.archivToolStripMenuItem.Text = "Archiv";
            // 
            // speichernunterToolStripMenuItem
            // 
            this.speichernunterToolStripMenuItem.Name = "speichernunterToolStripMenuItem";
            this.speichernunterToolStripMenuItem.Text = "Speichern unter...";
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            // 
            // druckenToolStripMenuItem
            // 
            this.druckenToolStripMenuItem.Name = "druckenToolStripMenuItem";
            this.druckenToolStripMenuItem.Text = "Drucken...";
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            // 
            // schlie�enToolStripMenuItem
            // 
            this.schlie�enToolStripMenuItem.Name = "schlie�enToolStripMenuItem";
            this.schlie�enToolStripMenuItem.Text = "Schlie�en";
            // 
            // bearbeitenToolStripMenuItem
            // 
            this.bearbeitenToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.r�ckg�ngigToolStripMenuItem,
            this.toolStripSeparator7,
            this.ausschneidenToolStripMenuItem,
            this.kopierenToolStripMenuItem,
            this.einf�genToolStripMenuItem,
            this.toolStripSeparator2,
            this.allesMarkierenToolStripMenuItem,
            this.markierungumkehrenToolStripMenuItem,
            this.toolStripSeparator3,
            this.suchenToolStripMenuItem});
            this.bearbeitenToolStripMenuItem.Name = "bearbeitenToolStripMenuItem";
            this.bearbeitenToolStripMenuItem.Text = "Bearbeiten";
            // 
            // r�ckg�ngigToolStripMenuItem
            // 
            this.r�ckg�ngigToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("r�ckg�ngigToolStripMenuItem.Image")));
            this.r�ckg�ngigToolStripMenuItem.Name = "r�ckg�ngigToolStripMenuItem";
            this.r�ckg�ngigToolStripMenuItem.Text = "R�ckg�ngig";
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            // 
            // ausschneidenToolStripMenuItem
            // 
            this.ausschneidenToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("ausschneidenToolStripMenuItem.Image")));
            this.ausschneidenToolStripMenuItem.Name = "ausschneidenToolStripMenuItem";
            this.ausschneidenToolStripMenuItem.Text = "Ausschneiden";
            // 
            // kopierenToolStripMenuItem
            // 
            this.kopierenToolStripMenuItem.Name = "kopierenToolStripMenuItem";
            this.kopierenToolStripMenuItem.Text = "Kopieren";
            // 
            // einf�genToolStripMenuItem
            // 
            this.einf�genToolStripMenuItem.Name = "einf�genToolStripMenuItem";
            this.einf�genToolStripMenuItem.Text = "Einf�gen";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            // 
            // allesMarkierenToolStripMenuItem
            // 
            this.allesMarkierenToolStripMenuItem.Name = "allesMarkierenToolStripMenuItem";
            this.allesMarkierenToolStripMenuItem.Text = "Alles Markieren";
            // 
            // markierungumkehrenToolStripMenuItem
            // 
            this.markierungumkehrenToolStripMenuItem.Name = "markierungumkehrenToolStripMenuItem";
            this.markierungumkehrenToolStripMenuItem.Text = "Markierung umkehren";
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            // 
            // suchenToolStripMenuItem
            // 
            this.suchenToolStripMenuItem.Name = "suchenToolStripMenuItem";
            this.suchenToolStripMenuItem.Text = "Suchen";
            // 
            // ansichtToolStripMenuItem
            // 
            this.ansichtToolStripMenuItem.Name = "ansichtToolStripMenuItem";
            this.ansichtToolStripMenuItem.Text = "Ansicht";
            // 
            // toolStripTextBox1
            // 
            this.toolStripTextBox1.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripTextBox1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.ImageAndText;
            this.toolStripTextBox1.Name = "toolStripTextBox1";
            this.toolStripTextBox1.Size = new System.Drawing.Size(100, 21);
            this.toolStripTextBox1.Text = "Google Suche";
            // 
            // tsmiGehe
            // 
            this.tsmiGehe.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.zur�ckToolStripMenuItem,
            this.vorw�rtsToolStripMenuItem,
            this.aufw�rtsToolStripMenuItem,
            this.toolStripSeparator9});
            this.tsmiGehe.Name = "tsmiGehe";
            this.tsmiGehe.Text = "Gehe";
            // 
            // zur�ckToolStripMenuItem
            // 
            this.zur�ckToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("zur�ckToolStripMenuItem.Image")));
            this.zur�ckToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.zur�ckToolStripMenuItem.Name = "zur�ckToolStripMenuItem";
            this.zur�ckToolStripMenuItem.Text = "Zur�ck";
            // 
            // vorw�rtsToolStripMenuItem
            // 
            this.vorw�rtsToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("vorw�rtsToolStripMenuItem.Image")));
            this.vorw�rtsToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.vorw�rtsToolStripMenuItem.Name = "vorw�rtsToolStripMenuItem";
            this.vorw�rtsToolStripMenuItem.Text = "Vorw�rts";
            // 
            // aufw�rtsToolStripMenuItem
            // 
            this.aufw�rtsToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("aufw�rtsToolStripMenuItem.Image")));
            this.aufw�rtsToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.aufw�rtsToolStripMenuItem.Name = "aufw�rtsToolStripMenuItem";
            this.aufw�rtsToolStripMenuItem.Text = "Aufw�rts";
            // 
            // toolStripSeparator9
            // 
            this.toolStripSeparator9.Name = "toolStripSeparator9";
            // 
            // extrasToolStripMenuItem
            // 
            this.extrasToolStripMenuItem.Name = "extrasToolStripMenuItem";
            this.extrasToolStripMenuItem.Text = "Extras";
            // 
            // hilfeToolStripMenuItem
            // 
            this.hilfeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.�berToolStripMenuItem});
            this.hilfeToolStripMenuItem.Name = "hilfeToolStripMenuItem";
            this.hilfeToolStripMenuItem.Text = "Hilfe";
            // 
            // �berToolStripMenuItem
            // 
            this.�berToolStripMenuItem.Name = "�berToolStripMenuItem";
            this.�berToolStripMenuItem.Text = "�ber...";
            // 
            // Werkzeugleiste
            // 
            this.Werkzeugleiste.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.Werkzeugleiste.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tssbZur�ck,
            this.tssbVorw�rts,
            this.tsbAufw�rts,
            this.tsbAbbrechen,
            this.tsbAktualisieren,
            this.toolStripSeparator1,
            this.tslAdresse,
            this.tscbAdresse,
            this.tsbWechselnZu,
            this.toolStripTextBox2});
            this.Werkzeugleiste.Location = new System.Drawing.Point(0, 25);
            this.Werkzeugleiste.Name = "Werkzeugleiste";
            this.Werkzeugleiste.Size = new System.Drawing.Size(816, 25);
            this.Werkzeugleiste.TabIndex = 2;
            this.Werkzeugleiste.Text = "toolStrip1";
            // 
            // tssbZur�ck
            // 
            this.tssbZur�ck.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tssbZur�ck.Enabled = false;
            this.tssbZur�ck.Image = ((System.Drawing.Image)(resources.GetObject("tssbZur�ck.Image")));
            this.tssbZur�ck.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tssbZur�ck.Name = "tssbZur�ck";
            this.tssbZur�ck.ButtonClick += new System.EventHandler(this.tssbZur�ck_ButtonClick);
            this.tssbZur�ck.DropDownItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.tssbZur�ck_DropDownItemClicked);
            // 
            // tssbVorw�rts
            // 
            this.tssbVorw�rts.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tssbVorw�rts.Enabled = false;
            this.tssbVorw�rts.Image = ((System.Drawing.Image)(resources.GetObject("tssbVorw�rts.Image")));
            this.tssbVorw�rts.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tssbVorw�rts.Name = "tssbVorw�rts";
            this.tssbVorw�rts.Text = "toolStripSplitButton1";
            // 
            // tsbAufw�rts
            // 
            this.tsbAufw�rts.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbAufw�rts.Image = ((System.Drawing.Image)(resources.GetObject("tsbAufw�rts.Image")));
            this.tsbAufw�rts.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbAufw�rts.Name = "tsbAufw�rts";
            this.tsbAufw�rts.Text = "toolStripButton2";
            this.tsbAufw�rts.Click += new System.EventHandler(this.tsbAufw�rts_Click);
            // 
            // tsbAbbrechen
            // 
            this.tsbAbbrechen.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbAbbrechen.Enabled = false;
            this.tsbAbbrechen.Image = ((System.Drawing.Image)(resources.GetObject("tsbAbbrechen.Image")));
            this.tsbAbbrechen.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbAbbrechen.Name = "tsbAbbrechen";
            this.tsbAbbrechen.Text = "toolStripButton1";
            // 
            // tsbAktualisieren
            // 
            this.tsbAktualisieren.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbAktualisieren.Image = ((System.Drawing.Image)(resources.GetObject("tsbAktualisieren.Image")));
            this.tsbAktualisieren.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbAktualisieren.Name = "tsbAktualisieren";
            this.tsbAktualisieren.Text = "toolStripButton2";
            this.tsbAktualisieren.Click += new System.EventHandler(this.tsbAktualisieren_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            // 
            // tslAdresse
            // 
            this.tslAdresse.Name = "tslAdresse";
            this.tslAdresse.Text = "Adresse:";
            // 
            // tscbAdresse
            // 
            this.tscbAdresse.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.tscbAdresse.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.FileSystemDirectories;
            this.tscbAdresse.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.ImageAndText;
            this.tscbAdresse.Name = "tscbAdresse";
            this.tscbAdresse.Size = new System.Drawing.Size(421, 25);
            this.tscbAdresse.Text = "Arbeitsplatz";
            // 
            // tsbWechselnZu
            // 
            this.tsbWechselnZu.Image = ((System.Drawing.Image)(resources.GetObject("tsbWechselnZu.Image")));
            this.tsbWechselnZu.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbWechselnZu.Name = "tsbWechselnZu";
            this.tsbWechselnZu.Text = "Wechseln zu";
            this.tsbWechselnZu.Click += new System.EventHandler(this.tsbWechselnZu_Click);
            // 
            // toolStripTextBox2
            // 
            this.toolStripTextBox2.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripTextBox2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.ImageAndText;
            this.toolStripTextBox2.Name = "toolStripTextBox2";
            this.toolStripTextBox2.Size = new System.Drawing.Size(100, 25);
            this.toolStripTextBox2.Text = "Dateifilter";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 50);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.panel1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.lvArbeitsplatz);
            this.splitContainer1.Panel2.Controls.Add(this.tsFolders);
            this.splitContainer1.Panel2.Controls.Add(this.lvFolder);
            this.splitContainer1.Size = new System.Drawing.Size(816, 408);
            this.splitContainer1.SplitterDistance = 202;
            this.splitContainer1.TabIndex = 3;
            this.splitContainer1.TabStop = false;
            this.splitContainer1.Text = "splitContainer1";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.lProperties);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(202, 408);
            this.panel1.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 380);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(202, 28);
            this.panel2.TabIndex = 3;
            // 
            // lProperties
            // 
            this.lProperties.AutoSize = true;
            this.lProperties.Location = new System.Drawing.Point(28, 87);
            this.lProperties.Name = "lProperties";
            this.lProperties.Size = new System.Drawing.Size(0, 0);
            this.lProperties.TabIndex = 0;
            // 
            // lvArbeitsplatz
            // 
            this.lvArbeitsplatz.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lvArbeitsplatz.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader4,
            this.columnHeader3});
            this.lvArbeitsplatz.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvArbeitsplatz.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lvArbeitsplatz.Location = new System.Drawing.Point(0, 23);
            this.lvArbeitsplatz.Name = "lvArbeitsplatz";
            this.lvArbeitsplatz.Size = new System.Drawing.Size(610, 385);
            this.lvArbeitsplatz.SmallImageList = this.FileSystemIcons;
            this.lvArbeitsplatz.TabIndex = 2;
            this.lvArbeitsplatz.View = System.Windows.Forms.View.Details;
            this.lvArbeitsplatz.ItemActivate += new System.EventHandler(this.lvArbeitsplatz_ItemActivate);
            this.lvArbeitsplatz.SelectedIndexChanged += new System.EventHandler(this.lvArbeitsplatz_SelectedIndexChanged);
            this.lvArbeitsplatz.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.lvArbeitsplatz_ColumnClick);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Name";
            this.columnHeader1.Width = 232;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Gesamtgr��e";
            this.columnHeader2.Width = 105;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Freier Speicher";
            this.columnHeader4.Width = 94;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Typ";
            this.columnHeader3.Width = 108;
            // 
            // FileSystemIcons
            // 
            this.FileSystemIcons.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("FileSystemIcons.ImageStream")));
            this.FileSystemIcons.Images.SetKeyName(0, "Folder.bmp");
            this.FileSystemIcons.Images.SetKeyName(1, "DefaultFile.bmp");
            this.FileSystemIcons.Images.SetKeyName(2, "HD.bmp");
            this.FileSystemIcons.Images.SetKeyName(3, "CD.bmp");
            // 
            // tsFolders
            // 
            this.tsFolders.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tssbArbeitsplatz});
            this.tsFolders.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.Flow;
            this.tsFolders.Location = new System.Drawing.Point(0, 0);
            this.tsFolders.Name = "tsFolders";
            this.tsFolders.Size = new System.Drawing.Size(610, 23);
            this.tsFolders.TabIndex = 0;
            this.tsFolders.Text = "toolStrip1";
            // 
            // tssbArbeitsplatz
            // 
            this.tssbArbeitsplatz.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiArbeitsplatz,
            this.toolStripSeparator8});
            this.tssbArbeitsplatz.Image = ((System.Drawing.Image)(resources.GetObject("tssbArbeitsplatz.Image")));
            this.tssbArbeitsplatz.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tssbArbeitsplatz.Name = "tssbArbeitsplatz";
            this.tssbArbeitsplatz.Text = "Arbeitsplatz";
            this.tssbArbeitsplatz.ButtonClick += new System.EventHandler(this.tssbArbeitsplatz_ButtonClick);
            this.tssbArbeitsplatz.DropDownItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.tssbArbeitsplatz_DropDownItemClicked);
            // 
            // tsmiArbeitsplatz
            // 
            this.tsmiArbeitsplatz.Image = ((System.Drawing.Image)(resources.GetObject("tsmiArbeitsplatz.Image")));
            this.tsmiArbeitsplatz.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsmiArbeitsplatz.Name = "tsmiArbeitsplatz";
            this.tsmiArbeitsplatz.Text = "Arbeitsplatz";
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            // 
            // lvFolder
            // 
            this.lvFolder.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lvFolder.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader5,
            this.columnHeader6,
            this.columnHeader7});
            this.lvFolder.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvFolder.Location = new System.Drawing.Point(0, 0);
            this.lvFolder.Name = "lvFolder";
            this.lvFolder.Size = new System.Drawing.Size(610, 408);
            this.lvFolder.SmallImageList = this.FileSystemIcons;
            this.lvFolder.TabIndex = 2;
            this.lvFolder.View = System.Windows.Forms.View.Details;
            this.lvFolder.ItemActivate += new System.EventHandler(this.lvFolder_ItemActivate);
            this.lvFolder.SelectedIndexChanged += new System.EventHandler(this.lvFolder_SelectedIndexChanged);
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Name";
            this.columnHeader5.Width = 204;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "Gr��e";
            this.columnHeader6.Width = 167;
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "Typ";
            this.columnHeader7.Width = 151;
            // 
            // GlobalTimer
            // 
            this.GlobalTimer.Enabled = true;
            this.GlobalTimer.Interval = 20000;
            this.GlobalTimer.Tick += new System.EventHandler(this.GlobalTimer_Tick);
            // 
            // process1
            // 
            this.process1.StartInfo.Domain = "";
            this.process1.StartInfo.LoadUserProfile = false;
            this.process1.StartInfo.Password = null;
            this.process1.StartInfo.StandardErrorEncoding = null;
            this.process1.StartInfo.StandardOutputEncoding = null;
            this.process1.StartInfo.UserName = "";
            this.process1.SynchronizingObject = this;
            // 
            // Explorer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(816, 485);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.StatusZeile);
            this.Controls.Add(this.Werkzeugleiste);
            this.Controls.Add(this.Men�Leiste);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.Men�Leiste;
            this.Name = "Explorer";
            this.Text = "Explorer";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Explorer_FormClosed);
            this.StatusZeile.ResumeLayout(false);
            this.Men�Leiste.ResumeLayout(false);
            this.Men�Leiste.PerformLayout();
            this.Werkzeugleiste.ResumeLayout(false);
            this.Werkzeugleiste.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            this.splitContainer1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tsFolders.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip StatusZeile;
        private System.Windows.Forms.MenuStrip Men�Leiste;
        private System.Windows.Forms.ToolStripMenuItem dateiToolStripMenuItem;
        private System.Windows.Forms.ToolStrip Werkzeugleiste;
        private System.Windows.Forms.ToolStripLabel tslAdresse;
        private System.Windows.Forms.ToolStripComboBox tscbAdresse;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem bearbeitenToolStripMenuItem;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripProgressBar toolStripProgressBar1;
        private System.Windows.Forms.ToolStripMenuItem ansichtToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem kopierenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem einf�genToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem allesMarkierenToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem suchenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem neuesFensterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem speichernunterToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem druckenToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem schlie�enToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fensterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ordnerToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripMenuItem bitmapToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem textdokumentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem archivToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem markierungumkehrenToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ToolStrip tsFolders;
        private System.Windows.Forms.ListView lvArbeitsplatz;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.Label lProperties;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.Timer GlobalTimer;
        private System.Windows.Forms.ListView lvFolder;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBox1;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBox2;
        private System.Windows.Forms.ImageList FileSystemIcons;
        private System.Diagnostics.Process process1;
        private System.Windows.Forms.ToolStripMenuItem tsmiGehe;
        private System.Windows.Forms.ToolStripMenuItem extrasToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
        private System.Windows.Forms.ToolStripMenuItem hilfeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem �berToolStripMenuItem;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ToolStripButton tsbPlayPause;
        private System.Windows.Forms.ToolStripMenuItem r�ckg�ngigToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ausschneidenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem zur�ckToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vorw�rtsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aufw�rtsToolStripMenuItem;
        private System.Windows.Forms.ToolStripSplitButton tssbZur�ck;
        private System.Windows.Forms.ToolStripSplitButton tssbVorw�rts;
        private System.Windows.Forms.ToolStripButton tsbAufw�rts;
        private System.Windows.Forms.ToolStripButton tsbAbbrechen;
        private System.Windows.Forms.ToolStripButton tsbAktualisieren;
        private System.Windows.Forms.ToolStripButton tsbWechselnZu;
        private System.Windows.Forms.ToolStripSplitButton tssbArbeitsplatz;
        private System.Windows.Forms.ToolStripMenuItem tsmiArbeitsplatz;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
    }
}

