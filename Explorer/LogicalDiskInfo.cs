using System;
using System.Collections.Generic;
using System.Text;

namespace Explorer
{
    class LogicalDiskInfo
    {
        public string Name;
        public UInt64 Size;
        public UInt64 Freespace;
        public string Description;
        public string VolumeName;

        public LogicalDiskInfo(string name, UInt64 size, UInt64 freespace, string description)
        {
            Name = name;
            Size = size;
            Freespace = freespace;
            Description = description;
        }

        public LogicalDiskInfo()
        {
            Name = "";
            Size = 0;
            Freespace = 0;
            Description = "";
            VolumeName = "";
        }
    }
}
