3-Heights(TM) PDF Export API
============================

Version 1.50     Pre-Release

Copyright (C) 1995-2005 PDF Tools AG

http://www.pdf-tools.com
pdfsupport@pdf-tools.com


Windows
=======

File list
---------
bin/PDFParser.dll      		Dynamic link library (required)
bin/pdcjk.dll         		This dll contains support for asian languages (optional)
bin/unicows.dll       		This dll contains the Microsoft Unicode Layer for Windows98 (required)
bin/Icc/*.*            		Color profiles (optional)
doc/pdf-license-v1.4.pdf    License terms for binary pdf tools
doc/expa.pdf           		API documentation (COM API notation, semantics identical for all language bindings)
doc/EXPA130HTDoc.zip   		HTML documentation kit for the native (C) interface
doc/readme.txt         		This file
doc/PDFParser.idl      		COM interface definition (WIN32 kit only, for documentation purposes)
include/expa_c.h       		C header file
include/pdferror.h     		supplementary C header file containting error codes
jar/EXPA.jar           		Java interface classes archive
lib/PDFParser.lib      		Stub library for C Programs
samples/*.*            		VB sample project (WIN32 kit only)


Installation on Windows Systems for use with the COM API
--------------------------------------------------------
1. Unpack the archive in an installation directory, i.e. C:/Program Files/pdf-tools/
2. Register the dll, using the following command:
     regsvr32.exe bin\PDFParser.dll

Installation for use with the native C Interface and Java
---------------------------------------------------------
1. Unpack the archive in an installation directory, i.e. C:/Program Files/pdf-tools/
2. Add the bin directory to the PATH variable

For Java, add the jar/EXPA.jar file to the CLASSPATH.



Mac OSX
=======

File list
---------
bin/libPDFPARSER.dylib Dynamic Library
doc/expa.pdf           API documentation
doc/EXPA130HTDoc.zip   HTML documentation kit for the native (C) interface
doc/readme.txt         This file
doc/license.pdf        License terms for binary pdf tools
include/expa_c.h       C header file
include/pdferror.h     supplementary C header file containting error codes
jar/EXPA.jar           Java interface classes archive

Installation for use with the native C Interface and Java
---------------------------------------------------------
1. Unpack the archive in an installation directory, i.e. /User/lib/pdf-tools
2. Add the directory containing libPDFPARSER.dylib to the DYLD_LIBRARY_PATH.

For Java, rename the file libPDFPARSER.dylib to libPDFPARSER.jnilib, add the jar/EXPA.jar file to the CLASSPATH.