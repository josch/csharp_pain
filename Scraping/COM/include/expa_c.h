/*
    File:         expa_c.h
    Copyright:    PDF-Tools AG, Switzerland (www.pdf-tools.com)
    Description:  C/C++ API definition for the PDF export tool (EXPA)
*/
#ifndef __EXPA_H_
#define __EXPA_H_

#include "pdfcdecl.h"
#include "pdfcodecdecl.h"
#include "pdferror.h"
#include "pdfcommondecl.h"
#include "pdfcontentdecl.h"

#ifdef __cplusplus
extern "C" {
#endif

#ifndef EXPAAPI
#   ifdef WIN32
#       ifdef EXPA_EXPORTS
#           define EXPAAPI __declspec(dllexport)
#       else
#           define EXPAAPI __declspec(dllimport)
#       endif
#   else
#       define EXPAAPI
#   endif
#endif

#ifndef EXPACALL
#   ifdef WIN32
#       define EXPACALL __stdcall
#   else
#       define EXPACALL
#   endif
#endif


#ifdef _UNICODE
#define ExpaDocOpen              ExpaDocOpenW
#define ExpaDocGetTitle          ExpaDocGetTitleW
#define ExpaDocGetAuthor         ExpaDocGetAuthorW
#define ExpaDocGetSubject        ExpaDocGetSubjectW
#define ExpaDocGetKeywords       ExpaDocGetKeywordsW
#define ExpaDocGetInfoEntry      ExpaDocGetInfoEntryW
#define ExpaImageStore           ExpaImageStoreW
#define ExpaAnotationGetContents ExpaAnotationGetContentsW
#define ExpaAnnotationGetTextLabel ExpaAnnotationGetTextLabelW
#define ExpaOutlineItemGetTitle  ExpaOutlineItemGetTitleW
#define ExpaDocOpenMem           ExpaDocOpenMemW
#define ExpaImageStoreInMemory   ExpaImageStoreInMemoryW

#else
#define ExpaDocOpen              ExpaDocOpenA
#define ExpaDocGetTitle          ExpaDocGetTitleA
#define ExpaDocGetAuthor         ExpaDocGetAuthorA
#define ExpaDocGetSubject        ExpaDocGetSubjectA
#define ExpaDocGetKeywords       ExpaDocGetKeywordsA
#define ExpaDocGetInfoEntry      ExpaDocGetInfoEntryA
#define ExpaImageStore           ExpaImageStoreA
#define ExpaAnotationGetContents ExpaAnotationGetContentsA
#define ExpaAnnotationGetTextLabel ExpaAnnotationGetTextLabelA
#define ExpaOutlineItemGetTitle  ExpaOutlineItemGetTitleA
#define ExpaDocOpenMem           ExpaDocOpenMemA
#define ExpaImageStoreInMemory   ExpaImageStoreInMemoryA

#endif

/** \defgroup types Type definitions
 * @{
 */

typedef struct TPdfExpaDocument			TPdfExpaDocument;       /**< document handle type @see \ref document */
typedef struct TPdfExpaPage				TPdfExpaPage;           /**< page handle type @see \ref page */
typedef struct TPdfExpaContent			TPdfExpaContent;        /**< content handle type @see \ref content */
typedef struct TPdfExpaText				TPdfExpaText;           /**< text handle type @see \ref text */
typedef struct TPdfExpaFont				TPdfExpaFont;           /**< font handle type @see \ref font */
typedef struct TPdfExpaColorSpace		TPdfExpaColorSpace;     /**< color space handle type @see \ref cs */
typedef struct TPdfExpaGraphicsState	TPdfExpaGraphicsState;  /**< color space handle type @see \ref gs */
typedef struct TPdfExpaImage			TPdfExpaImage;          /**< image handle type @see \ref image */
typedef struct TPdfExpaOutlineItem		TPdfExpaOutlineItem;
typedef struct TPdfExpaDestination		TPdfExpaDestination;
typedef struct TPdfExpaAlternateImage	TPdfExpaAlternateImage;
typedef struct TPdfExpaAnnotation		TPdfExpaAnnotation;
typedef struct TPdfExpaAlternateImageArray	TPdfExpaAlternateImageArray;

typedef TPdfExpaDocument* TExpaDocument;
typedef TPdfExpaPage* TExpaPage;
typedef TPdfExpaContent* TExpaContent;
typedef TPdfExpaText* TExpaText;
typedef TPdfExpaFont* TExpaFont;
typedef TPdfExpaColorSpace* TExpaColorSpace;
typedef TPdfExpaGraphicsState* TExpaGraphicsState;
typedef TPdfExpaImage* TExpaImage;


typedef struct TPDFTransformMatrix /** data structure representing a PDF text matrix */
{
	float a, b, c, d, e, f;
	float XSkew;
	float YSkew;
	float Rotation;
	float XTranslation;
	float YTranslation;
	float XScaling;
	float YScaling;
	TPDFOrientation Orientation;
} TPDFTransformMatrix;

typedef struct TPDFVector
{
    float x;
    float y;
} TPDFVector;

  /** @}
 * \defgroup api API initialization and termination functions
 * @{
 */

EXPAAPI void EXPACALL ExpaInitialize();  /**< this function must be called before using any other functions of the API */
EXPAAPI void EXPACALL ExpaUnInitialize();/**< call this function to free resources allocated during InitializeAPI */

/************************** document related calls *****************************/
/** @}
 * \defgroup document Document related functions
 * @{
 */


EXPAAPI TPdfExpaDocument* EXPACALL ExpaCreateObject();                /**< create a document object    */
EXPAAPI void EXPACALL ExpaDestroyObject(TPdfExpaDocument* handledoc); /**< dispose the document object */

/** open a PDF file for data extraction;
 * @return non-zero on success */
EXPAAPI int EXPACALL ExpaDocOpenA(TPdfExpaDocument* handledoc, const char* szFilename, const char* szPassword);
EXPAAPI int EXPACALL ExpaDocOpenW(TPdfExpaDocument* handledoc, const WCHAR* pFilename, const WCHAR* szPassword);
EXPAAPI int EXPACALL ExpaDocOpenMemA(TPdfExpaDocument* handledoc,const unsigned char* pDocumentBytes, int nDocumentLength, const char* szPassword);
EXPAAPI int EXPACALL ExpaDocOpenMemW(TPdfExpaDocument* handledoc,const unsigned char* pDocumentBytes, int nDocumentLength, const WCHAR* szPassword);

EXPAAPI int EXPACALL ExpaDocClose(TPdfExpaDocument* handledoc);/**< close the document and free all associated resources;
																 *  multiple Open/Close calls are permitted */
EXPAAPI TPDFErrorCode EXPACALL ExpaDocGetLastError(TPdfExpaDocument* handledoc);


EXPAAPI int                 EXPACALL ExpaDocIsEncrypted(TPdfExpaDocument* handledoc);/**< return non-zero if document is encrypted */
EXPAAPI int                 EXPACALL ExpaDocGetMajorVersion(TPdfExpaDocument* handledoc);
EXPAAPI int                 EXPACALL ExpaDocGetMinorVersion(TPdfExpaDocument* handledoc);
EXPAAPI const char*         EXPACALL ExpaDocGetTitleA(TPdfExpaDocument* handledoc);
EXPAAPI const WCHAR*        EXPACALL ExpaDocGetTitleW(TPdfExpaDocument* handledoc);
EXPAAPI const char*         EXPACALL ExpaDocGetAuthorA(TPdfExpaDocument* handledoc);
EXPAAPI const WCHAR*        EXPACALL ExpaDocGetAuthorW(TPdfExpaDocument* handledoc);
EXPAAPI const char*         EXPACALL ExpaDocGetSubjectA(TPdfExpaDocument* handledoc);
EXPAAPI const WCHAR*        EXPACALL ExpaDocGetSubjectW(TPdfExpaDocument* handledoc);
EXPAAPI const char*         EXPACALL ExpaDocGetKeywordsA(TPdfExpaDocument* handledoc);
EXPAAPI const WCHAR*        EXPACALL ExpaDocGetKeywordsW(TPdfExpaDocument* handledoc);
EXPAAPI TPDFDate            EXPACALL ExpaDocGetModDate(TPdfExpaDocument* handledoc);
EXPAAPI TPDFDate            EXPACALL ExpaDocGetCreationDate(TPdfExpaDocument* handledoc);
EXPAAPI const char*         EXPACALL ExpaDocGetInfoEntryA(TPdfExpaDocument* handledoc, const char* szKey);
EXPAAPI const WCHAR*        EXPACALL ExpaDocGetInfoEntryW(TPdfExpaDocument* handledoc, const WCHAR* szKey);
				
EXPAAPI int                 EXPACALL ExpaDocGetPageCount(TPdfExpaDocument* handledoc);/**< return the number of pages in the document */
EXPAAPI int                 EXPACALL ExpaDocSetPageNo(TPdfExpaDocument* handledoc, int iPageNo);/** acquire the specified page for data extraction. @return non-zero on success */
EXPAAPI int                 EXPACALL ExpaDocGetPageNo(TPdfExpaDocument* handledoc);/**< return the current page number */
EXPAAPI TPdfExpaPage*       EXPACALL ExpaDocGetPage(TPdfExpaDocument* handledoc);

EXPAAPI TPdfExpaFont*       EXPACALL ExpaDocGetFirstFontResource(TPdfExpaDocument* handledoc);
EXPAAPI TPdfExpaFont*       EXPACALL ExpaDocGetNextFontResource(TPdfExpaDocument* handledoc);
EXPAAPI TPdfExpaImage*      EXPACALL ExpaDocGetFirstImageResource(TPdfExpaDocument* handledoc);
EXPAAPI TPdfExpaImage*      EXPACALL ExpaDocGetNextImageResource(TPdfExpaDocument* handledoc);
EXPAAPI TPdfExpaColorSpace* EXPACALL ExpaDocGetFirstColorSpaceResource(TPdfExpaDocument* handledoc);
EXPAAPI TPdfExpaColorSpace* EXPACALL ExpaDocGetNextColorSpaceResource(TPdfExpaDocument* handledoc);
EXPAAPI TPdfExpaOutlineItem*
                            EXPACALL ExpaDocGetFirstOutlineItem(TPdfExpaDocument* handledoc);

EXPAAPI TPdfExpaOutlineItem*
                            EXPACALL ExpaDocGetNextOutlineItem(TPdfExpaDocument* handledoc, int nMaxLevel, int bReturnOpenOnly);
EXPAAPI int                 EXPACALL ExpaDocGetCurrentOutlineLevel(TPdfExpaDocument* handledoc);

/************************** page related calls *****************************/
/** @}
 * \defgroup page Page related functions
 * @{ */
EXPAAPI TPdfExpaDocument*   EXPACALL ExpaPageGetDocument(TPdfExpaPage* handlepage);
EXPAAPI TPDFRect            EXPACALL ExpaPageGetMediaBox(TPdfExpaPage* handlepage);
EXPAAPI TPDFRect            EXPACALL ExpaPageGetCropBox(TPdfExpaPage* handlepage);
EXPAAPI TPDFRect            EXPACALL ExpaPageGetBleedBox(TPdfExpaPage* handlepage);
EXPAAPI TPDFRect            EXPACALL ExpaPageGetTrimBox(TPdfExpaPage* handlepage);
EXPAAPI TPDFRect            EXPACALL ExpaPageGetArtBox(TPdfExpaPage* handlepage);
EXPAAPI int                 EXPACALL ExpaPageGetRotate(TPdfExpaPage* handlepage);
EXPAAPI TPdfExpaContent*    EXPACALL ExpaPageGetContent(TPdfExpaPage* handlepage);
EXPAAPI	TPdfExpaAnnotation*
                            EXPACALL ExpaPageGetFirstAnnotation(TPdfExpaPage* handlepage);

EXPAAPI	TPdfExpaAnnotation*	
                            EXPACALL ExpaPageGetNextAnnotation(TPdfExpaPage* handlepage);


/************************** content related calls *****************************/
/** @}
 * \defgroup content       Content related functions
 * @{ */
EXPAAPI void                EXPACALL ExpaContentResetContent(TPdfExpaContent* handlecont, int iBoolIncludeRotate);
EXPAAPI TPdfExpaGraphicsState*	
                            EXPACALL ExpaContentGetGraphicsState(TPdfExpaContent* handlecont);
EXPAAPI TPdfExpaText*       EXPACALL ExpaContentGetText(TPdfExpaContent* handlecont);
EXPAAPI TPdfExpaImage*      EXPACALL ExpaContentGetImage(TPdfExpaContent* handlecont);
EXPAAPI const char*         EXPACALL ExpaContentGetPath(TPdfExpaContent* handlecont);
EXPAAPI void                EXPACALL ExpaContentSetBreakWords(TPdfExpaContent* handlecont, int iBoolOn);
EXPAAPI TPdfExpaText*       EXPACALL ExpaContentGetNextText(TPdfExpaContent* handlecont);
EXPAAPI TPdfExpaImage*      EXPACALL ExpaContentGetNextImage(TPdfExpaContent* handlecont);
EXPAAPI const char*         EXPACALL ExpaContentGetNextPath(TPdfExpaContent* handlecont);
EXPAAPI TPDFContentObject   EXPACALL ExpaContentGetNextObject(TPdfExpaContent* handlecont);
EXPAAPI int                 EXPACALL ExpaContentGetFlags(TPdfExpaContent* handlecont);

/************************** text related calls *****************************/
/** @}
 * \defgroup text       Text extraction related functions
 * @{ */

EXPAAPI int                 EXPACALL ExpaTextGetStringLength(TPdfExpaText* handletext);/**< @return the number of characters contained in the unicode text string */
EXPAAPI const WCHAR*        EXPACALL ExpaTextGetUnicodeString(TPdfExpaText* handletext);/**< @return the unicode string, having possibly more than GetStringLength() elements, and terminated with 0x0000 */
EXPAAPI TPDFVector*         EXPACALL ExpaTextGetPosition(TPdfExpaText* handletext);
EXPAAPI float               EXPACALL ExpaTextGetFontSize(TPdfExpaText* handletext);
EXPAAPI float               EXPACALL ExpaTextGetWidth(TPdfExpaText* handletext);
EXPAAPI float               EXPACALL ExpaTextGetRotation(TPdfExpaText* handletext);
EXPAAPI const float*        EXPACALL ExpaTextGetBoundingBox(TPdfExpaText* handletext);

/************************** font related calls *****************************/
/** @}
 * \defgroup font       Font related functions
 * @{ */
EXPAAPI const char*         EXPACALL ExpaFontGetType(TPdfExpaFont* handlefont);
EXPAAPI const char*         EXPACALL ExpaFontGetBaseName(TPdfExpaFont* handlefont);
EXPAAPI const float*        EXPACALL ExpaFontGetWidths(TPdfExpaFont* handlefont);/**< @return the character widths of the font
                                                                            * there are 256 elements in the array that can directly be indexed with the (unsigned) raw string character number */
EXPAAPI const char*         EXPACALL ExpaFontGetEncoding(TPdfExpaFont* handlefont, int iCharIndex);/**< @return the glyph name of each character with CharIndex running from 0 to 255 */
EXPAAPI long                EXPACALL ExpaFontGetFlags(TPdfExpaFont* handlefont);
EXPAAPI const float*        EXPACALL ExpaFontGetBBox(TPdfExpaFont* handlefont);/**< @return the bounding box as array of four floats */
EXPAAPI float               EXPACALL ExpaFontGetItalicAngle(TPdfExpaFont* handlefont);
EXPAAPI float               EXPACALL ExpaFontGetAscent(TPdfExpaFont* handlefont);
EXPAAPI float               EXPACALL ExpaFontGetDescent(TPdfExpaFont* handlefont);
EXPAAPI float               EXPACALL ExpaFontGetCapHeight(TPdfExpaFont* handlefont);
EXPAAPI float               EXPACALL ExpaFontGetStemV(TPdfExpaFont* handlefont);
EXPAAPI float               EXPACALL ExpaFontGetStemH(TPdfExpaFont* handlefont);
EXPAAPI float               EXPACALL ExpaFontGetAvgWidth(TPdfExpaFont* handlefont);
EXPAAPI float               EXPACALL ExpaFontGetMaxWidth(TPdfExpaFont* handlefont);
EXPAAPI float               EXPACALL ExpaFontGetMissingWidth(TPdfExpaFont* handlefont);
EXPAAPI const char*         EXPACALL ExpaFontGetCharSet(TPdfExpaFont* handlefont);
EXPAAPI int                 EXPACALL ExpaFontGetFontFileType(TPdfExpaFont* handlefont);
EXPAAPI const TPDFByteArray* 
                            EXPACALL ExpaFontGetFontFile(TPdfExpaFont* handlefont);
EXPAAPI float               EXPACALL ExpaFontGetLeading(TPdfExpaFont* handlefont);
EXPAAPI float               EXPACALL ExpaFontGetXHeight(TPdfExpaFont* handlefont);

/************************** image related calls *****************************/
/** @}
 * \defgroup image       Image related functions
 * @{ */
EXPAAPI int                 EXPACALL ExpaImageGetWidth(TPdfExpaImage* handleimg);
EXPAAPI int                 EXPACALL ExpaImageGetHeight(TPdfExpaImage* handleimg);
EXPAAPI int                 EXPACALL ExpaImageGetBitsPerComponent(TPdfExpaImage* handleimg);
EXPAAPI const TPDFByteArray* 
                            EXPACALL ExpaImageGetSamples(TPdfExpaImage* handleimg);
EXPAAPI const TPDFByteArray* 
                            EXPACALL ExpaImageGetMask(TPdfExpaImage* handleimg);
EXPAAPI TPdfExpaColorSpace* EXPACALL ExpaImageGetColorSpace(TPdfExpaImage* handleimg);                        /**< return color space information for the image */
EXPAAPI int                 EXPACALL ExpaImageConvertToRGB(TPdfExpaImage* handleimg);                         /**< convert image to RGB; @return non-zero on success; BitsPerComponent must be >= 8 */
EXPAAPI int                 EXPACALL ExpaImageChangeOrientation(TPdfExpaImage* handleimg, TPDFOrientation iOrientation);  /**< change to image according to the specified orientation */
EXPAAPI int                 EXPACALL ExpaImageStoreA(TPdfExpaImage* handleimg, const char* szFileName, TPDFCompression iCompression);            /**< store image in a TIFF file; returns non-zero on success */
EXPAAPI int                 EXPACALL ExpaImageStoreW(TPdfExpaImage* handleimg, const WCHAR* szFileName, TPDFCompression iCompression); /**< store image in a file; returns non-zero on success */
EXPAAPI int                 EXPACALL ExpaImageStoreInMemoryA(TPdfExpaImage* handleimg, const char* szExtension, TPDFCompression iCompression);   /**< store image in Memory; returns non-zero on success */
EXPAAPI int                 EXPACALL ExpaImageStoreInMemoryW(TPdfExpaImage* handleimg, const WCHAR* szExtension, TPDFCompression iCompression);    /**< store image in Memory; returns non-zero on success */
EXPAAPI TPDFByteArray*      EXPACALL ExpaImageGetImage(TPdfExpaImage* handleimg);
EXPAAPI TPdfExpaAlternateImageArray*
                            EXPACALL ExpaImageGetAlternates(TPdfExpaImage* handleimg);
/************************** color space related calls ************************/
/** @}
 * \defgroup cs       Color Space related functions
 * @{ */
EXPAAPI int                 EXPACALL ExpaCSGetComponentsPerPixel(TPdfExpaColorSpace* handleclsp); /**< return the number of components per pixel; for RGB images, this will be 3; for CMYK images: 4 */
EXPAAPI TPdfExpaColorSpace* EXPACALL ExpaCSGetBaseColorSpace(TPdfExpaColorSpace* handleclsp);
EXPAAPI int                 EXPACALL ExpaCSIsIndexed(TPdfExpaColorSpace* handleclsp);           /**< return non-zero if the color space is indexed (if there is a palette) */
EXPAAPI int                 EXPACALL ExpaCSGetHighIndex(TPdfExpaColorSpace* handleclsp);        /**< return the number of index values minus 1 */

/** @return the colorspace lookup data (palette)
  * length is (HighIndex+1)*ComponentsPerPixel */
EXPAAPI const TPDFByteArray* 
                            EXPACALL ExpaCSGetLookup(TPdfExpaColorSpace* handleclsp);
EXPAAPI const char*         EXPACALL ExpaCSGetName(TPdfExpaColorSpace* handleclsp);

/************************** graphics space related calls **********************/
/** @}
 * \defgroup gs       Graphics State related functions
 * @{ */
EXPAAPI TPDFTransformMatrix*
                            EXPACALL ExpaGSGetCTM(TPdfExpaGraphicsState* handlegrst);
EXPAAPI TPdfExpaColorSpace* EXPACALL ExpaGSGetStrokeColorSpace(TPdfExpaGraphicsState* handlegrst);
EXPAAPI TPdfExpaColorSpace* EXPACALL ExpaGSGetFillColorSpace(TPdfExpaGraphicsState* handlegrst);
EXPAAPI int                 EXPACALL ExpaGSGetStrokeColorRGB(TPdfExpaGraphicsState* handlegrst);
EXPAAPI int                 EXPACALL ExpaGSGetFillColorRGB(TPdfExpaGraphicsState* handlegrst);
EXPAAPI int                 EXPACALL ExpaGSGetStrokeColorCMYK(TPdfExpaGraphicsState* handlegrst);
EXPAAPI int                 EXPACALL ExpaGSGetFillColorCMYK(TPdfExpaGraphicsState* handlegrst);
EXPAAPI float               EXPACALL ExpaGSGetCharSpacing(TPdfExpaGraphicsState* handlegrst);
EXPAAPI float               EXPACALL ExpaGSGetWordSpacing(TPdfExpaGraphicsState* handlegrst);
EXPAAPI float               EXPACALL ExpaGSGetHorizontalScaling(TPdfExpaGraphicsState* handlegrst);
EXPAAPI float               EXPACALL ExpaGSGetLeading(TPdfExpaGraphicsState* handlegrst);
EXPAAPI TPdfExpaFont*       EXPACALL ExpaGSGetFont(TPdfExpaGraphicsState* handlegrst);
EXPAAPI float               EXPACALL ExpaGSGetFontSize(TPdfExpaGraphicsState* handlegrst);
EXPAAPI int                 EXPACALL ExpaGSGetTextRenderingMode(TPdfExpaGraphicsState* handlegrst);
EXPAAPI float               EXPACALL ExpaGSGetTextRise(TPdfExpaGraphicsState* handlegrst);
EXPAAPI int                 EXPACALL ExpaGSGetTextKnockout(TPdfExpaGraphicsState* handlegrst);
EXPAAPI float               EXPACALL ExpaGSGetLineWidth(TPdfExpaGraphicsState* handlegrst);
EXPAAPI int                 EXPACALL ExpaGSGetLineCap(TPdfExpaGraphicsState* handlegrst);
EXPAAPI int                 EXPACALL ExpaGSGetLineJoin(TPdfExpaGraphicsState* handlegrst);
EXPAAPI float               EXPACALL ExpaGSGetMiterLimit(TPdfExpaGraphicsState* handlegrst);
EXPAAPI float               EXPACALL ExpaGSGetDashPhase(TPdfExpaGraphicsState* handlegrst);
/** retrieve the dash array elements;
 * @return the dash array elements
 */
EXPAAPI const TPDFFloatArray*		
                            EXPACALL ExpaGSGetDashArray(TPdfExpaGraphicsState* handlegrst);

/************************** destination related calls **********************/
/** @}
 * \defgroup Dest       Destination related functions
 * @{ */
EXPAAPI const char*         EXPACALL ExpaDestGetType(TPdfExpaDestination* handledest);
EXPAAPI int                 EXPACALL ExpaDestGetPageNo(TPdfExpaDestination* handledest);
EXPAAPI float               EXPACALL ExpaDestGetLeft(TPdfExpaDestination* handledest);
EXPAAPI float               EXPACALL ExpaDestGetBottom(TPdfExpaDestination* handledest);
EXPAAPI float               EXPACALL ExpaDestGetRight(TPdfExpaDestination* handledest);
EXPAAPI float               EXPACALL ExpaDestGetTop(TPdfExpaDestination* handledest);
EXPAAPI float               EXPACALL ExpaDestGetZoom(TPdfExpaDestination* handledest);

/************************** AlternateImage related calls **********************/
/** @}
 * \defgroup Alte       AlternateImage related functions
 * @{ */
EXPAAPI TPdfExpaAlternateImage*
                            EXPACALL ExpaAlternateArrayGetElement(TPdfExpaAlternateImageArray* handle, int inx);
EXPAAPI int                 EXPACALL ExpaAlternateArrayGetCount(TPdfExpaAlternateImageArray* handle);
EXPAAPI TPdfExpaImage*      EXPACALL ExpaAlternateGetImage(TPdfExpaAlternateImage* handlealte);
EXPAAPI int                 EXPACALL ExpaAlternateGetDefaultForPrinting(TPdfExpaAlternateImage* handlealte);

/************************** Annotation related calls **********************/
/** @}
 * \defgroup Anot       Annotation related functions
 * @{ */
EXPAAPI const char*         EXPACALL ExpaAnnotationGetSubtype(TPdfExpaAnnotation* handleanot);
EXPAAPI TPDFRect            EXPACALL ExpaAnnotationGetRect(TPdfExpaAnnotation* handleanot);
EXPAAPI const char*         EXPACALL ExpaAnnotationGetContentsA(TPdfExpaAnnotation* handleanot);
EXPAAPI const WCHAR*        EXPACALL ExpaAnnotationGetContentsW(TPdfExpaAnnotation* handleanot);
EXPAAPI const char*         EXPACALL ExpaAnnotationGetName(TPdfExpaAnnotation* handleanot);
EXPAAPI TPDFDate            EXPACALL ExpaAnnotationGetDate(TPdfExpaAnnotation* handleanot);
EXPAAPI int                 EXPACALL ExpaAnnotationGetFlags(TPdfExpaAnnotation* handleanot);
EXPAAPI int                 EXPACALL ExpaAnnotationGetColor(TPdfExpaAnnotation* handleanot);
EXPAAPI const WCHAR*        EXPACALL ExpaAnnotationGetTextLabelW(TPdfExpaAnnotation* handleanot);
EXPAAPI const char*         EXPACALL ExpaAnnotationGetTextLabelA(TPdfExpaAnnotation* handleanot);

/************************** OutlineItem related calls **********************/
/** @}
 * \defgroup Olit    OutlineItem related functions
 * @{ */
EXPAAPI const char*          EXPACALL ExpaOutlineItemGetTitleA(TPdfExpaOutlineItem* handleolit);
EXPAAPI const WCHAR*         EXPACALL ExpaOutlineItemGetTitleW(TPdfExpaOutlineItem* handleolit);
EXPAAPI int                  EXPACALL ExpaOutlineItemGetCount(TPdfExpaOutlineItem* handleolit);
EXPAAPI TPdfExpaDestination* EXPACALL ExpaOutlineItemGetDest(TPdfExpaOutlineItem* handleolit);
	
#ifdef __cplusplus
}
#endif

#endif
