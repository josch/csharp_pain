/* this ALWAYS GENERATED file contains the definitions for the interfaces */


/* File created by MIDL compiler version 5.01.0164 */
/* at Tue Dec 06 22:40:04 2005
 */
/* Compiler settings for P:\PDF\1.4.1.25\PDFParser\PDFParser.idl:
    Oicf (OptLev=i2), W1, Zp8, env=Win32, ms_ext, c_ext
    error checks: allocation ref bounds_check enum stub_data 
*/
//@@MIDL_FILE_HEADING(  )


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 440
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __PDFParser_h__
#define __PDFParser_h__

#ifdef __cplusplus
extern "C"{
#endif 

/* Forward Declarations */ 

#ifndef __IPDFColorSpace_FWD_DEFINED__
#define __IPDFColorSpace_FWD_DEFINED__
typedef interface IPDFColorSpace IPDFColorSpace;
#endif 	/* __IPDFColorSpace_FWD_DEFINED__ */


#ifndef __IPDFTransformMatrix_FWD_DEFINED__
#define __IPDFTransformMatrix_FWD_DEFINED__
typedef interface IPDFTransformMatrix IPDFTransformMatrix;
#endif 	/* __IPDFTransformMatrix_FWD_DEFINED__ */


#ifndef __IPDFFont_FWD_DEFINED__
#define __IPDFFont_FWD_DEFINED__
typedef interface IPDFFont IPDFFont;
#endif 	/* __IPDFFont_FWD_DEFINED__ */


#ifndef __IPDFGraphicsState_FWD_DEFINED__
#define __IPDFGraphicsState_FWD_DEFINED__
typedef interface IPDFGraphicsState IPDFGraphicsState;
#endif 	/* __IPDFGraphicsState_FWD_DEFINED__ */


#ifndef __IPDFText_FWD_DEFINED__
#define __IPDFText_FWD_DEFINED__
typedef interface IPDFText IPDFText;
#endif 	/* __IPDFText_FWD_DEFINED__ */


#ifndef __IPDFImage_FWD_DEFINED__
#define __IPDFImage_FWD_DEFINED__
typedef interface IPDFImage IPDFImage;
#endif 	/* __IPDFImage_FWD_DEFINED__ */


#ifndef __IPDFAlternateImage_FWD_DEFINED__
#define __IPDFAlternateImage_FWD_DEFINED__
typedef interface IPDFAlternateImage IPDFAlternateImage;
#endif 	/* __IPDFAlternateImage_FWD_DEFINED__ */


#ifndef __IPDFDocument_FWD_DEFINED__
#define __IPDFDocument_FWD_DEFINED__
typedef interface IPDFDocument IPDFDocument;
#endif 	/* __IPDFDocument_FWD_DEFINED__ */


#ifndef __IPDFContent_FWD_DEFINED__
#define __IPDFContent_FWD_DEFINED__
typedef interface IPDFContent IPDFContent;
#endif 	/* __IPDFContent_FWD_DEFINED__ */


#ifndef __IPDFAnnotation_FWD_DEFINED__
#define __IPDFAnnotation_FWD_DEFINED__
typedef interface IPDFAnnotation IPDFAnnotation;
#endif 	/* __IPDFAnnotation_FWD_DEFINED__ */


#ifndef __IPDFPage_FWD_DEFINED__
#define __IPDFPage_FWD_DEFINED__
typedef interface IPDFPage IPDFPage;
#endif 	/* __IPDFPage_FWD_DEFINED__ */


#ifndef __IPDFDestination_FWD_DEFINED__
#define __IPDFDestination_FWD_DEFINED__
typedef interface IPDFDestination IPDFDestination;
#endif 	/* __IPDFDestination_FWD_DEFINED__ */


#ifndef __IPDFOutlineItem_FWD_DEFINED__
#define __IPDFOutlineItem_FWD_DEFINED__
typedef interface IPDFOutlineItem IPDFOutlineItem;
#endif 	/* __IPDFOutlineItem_FWD_DEFINED__ */


#ifndef __ColorSpace_FWD_DEFINED__
#define __ColorSpace_FWD_DEFINED__

#ifdef __cplusplus
typedef class ColorSpace ColorSpace;
#else
typedef struct ColorSpace ColorSpace;
#endif /* __cplusplus */

#endif 	/* __ColorSpace_FWD_DEFINED__ */


#ifndef __TransformMatrix_FWD_DEFINED__
#define __TransformMatrix_FWD_DEFINED__

#ifdef __cplusplus
typedef class TransformMatrix TransformMatrix;
#else
typedef struct TransformMatrix TransformMatrix;
#endif /* __cplusplus */

#endif 	/* __TransformMatrix_FWD_DEFINED__ */


#ifndef __Font_FWD_DEFINED__
#define __Font_FWD_DEFINED__

#ifdef __cplusplus
typedef class Font Font;
#else
typedef struct Font Font;
#endif /* __cplusplus */

#endif 	/* __Font_FWD_DEFINED__ */


#ifndef __Text_FWD_DEFINED__
#define __Text_FWD_DEFINED__

#ifdef __cplusplus
typedef class Text Text;
#else
typedef struct Text Text;
#endif /* __cplusplus */

#endif 	/* __Text_FWD_DEFINED__ */


#ifndef __Image_FWD_DEFINED__
#define __Image_FWD_DEFINED__

#ifdef __cplusplus
typedef class Image Image;
#else
typedef struct Image Image;
#endif /* __cplusplus */

#endif 	/* __Image_FWD_DEFINED__ */


#ifndef __GraphicsState_FWD_DEFINED__
#define __GraphicsState_FWD_DEFINED__

#ifdef __cplusplus
typedef class GraphicsState GraphicsState;
#else
typedef struct GraphicsState GraphicsState;
#endif /* __cplusplus */

#endif 	/* __GraphicsState_FWD_DEFINED__ */


#ifndef __Content_FWD_DEFINED__
#define __Content_FWD_DEFINED__

#ifdef __cplusplus
typedef class Content Content;
#else
typedef struct Content Content;
#endif /* __cplusplus */

#endif 	/* __Content_FWD_DEFINED__ */


#ifndef __Page_FWD_DEFINED__
#define __Page_FWD_DEFINED__

#ifdef __cplusplus
typedef class Page Page;
#else
typedef struct Page Page;
#endif /* __cplusplus */

#endif 	/* __Page_FWD_DEFINED__ */


#ifndef __Document_FWD_DEFINED__
#define __Document_FWD_DEFINED__

#ifdef __cplusplus
typedef class Document Document;
#else
typedef struct Document Document;
#endif /* __cplusplus */

#endif 	/* __Document_FWD_DEFINED__ */


#ifndef __AlternateImage_FWD_DEFINED__
#define __AlternateImage_FWD_DEFINED__

#ifdef __cplusplus
typedef class AlternateImage AlternateImage;
#else
typedef struct AlternateImage AlternateImage;
#endif /* __cplusplus */

#endif 	/* __AlternateImage_FWD_DEFINED__ */


#ifndef __Annotation_FWD_DEFINED__
#define __Annotation_FWD_DEFINED__

#ifdef __cplusplus
typedef class Annotation Annotation;
#else
typedef struct Annotation Annotation;
#endif /* __cplusplus */

#endif 	/* __Annotation_FWD_DEFINED__ */


#ifndef __OutlineItem_FWD_DEFINED__
#define __OutlineItem_FWD_DEFINED__

#ifdef __cplusplus
typedef class OutlineItem OutlineItem;
#else
typedef struct OutlineItem OutlineItem;
#endif /* __cplusplus */

#endif 	/* __OutlineItem_FWD_DEFINED__ */


#ifndef __Destination_FWD_DEFINED__
#define __Destination_FWD_DEFINED__

#ifdef __cplusplus
typedef class Destination Destination;
#else
typedef struct Destination Destination;
#endif /* __cplusplus */

#endif 	/* __Destination_FWD_DEFINED__ */


/* header files for imported files */
#include "oaidl.h"
#include "ocidl.h"
#include "pdferror.h"
#include "pdfcodecdecl.h"
#include "pdfcommondecl.h"
#include "pdfcontentdecl.h"

void __RPC_FAR * __RPC_USER MIDL_user_allocate(size_t);
void __RPC_USER MIDL_user_free( void __RPC_FAR * ); 


#ifndef __PDFPARSERLib_LIBRARY_DEFINED__
#define __PDFPARSERLib_LIBRARY_DEFINED__

/* library PDFPARSERLib */
/* [helpstring][version][uuid] */ 



EXTERN_C const IID LIBID_PDFPARSERLib;

#ifndef __IPDFColorSpace_INTERFACE_DEFINED__
#define __IPDFColorSpace_INTERFACE_DEFINED__

/* interface IPDFColorSpace */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_IPDFColorSpace;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("BD23962F-DCCC-4324-9766-DD844AC272EB")
    IPDFColorSpace : public IDispatch
    {
    public:
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ComponentsPerPixel( 
            /* [retval][out] */ short __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_BaseColorSpace( 
            /* [retval][out] */ IPDFColorSpace __RPC_FAR *__RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_IsIndexed( 
            /* [retval][out] */ VARIANT_BOOL __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_High( 
            /* [retval][out] */ short __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Lookup( 
            /* [retval][out] */ VARIANT __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Name( 
            /* [retval][out] */ BSTR __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_IsMonochrome( 
            /* [retval][out] */ VARIANT_BOOL __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_IsColor( 
            /* [retval][out] */ VARIANT_BOOL __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Colorant( 
            /* [retval][out] */ VARIANT __RPC_FAR *pVal) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IPDFColorSpaceVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *QueryInterface )( 
            IPDFColorSpace __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void __RPC_FAR *__RPC_FAR *ppvObject);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *AddRef )( 
            IPDFColorSpace __RPC_FAR * This);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *Release )( 
            IPDFColorSpace __RPC_FAR * This);
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetTypeInfoCount )( 
            IPDFColorSpace __RPC_FAR * This,
            /* [out] */ UINT __RPC_FAR *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetTypeInfo )( 
            IPDFColorSpace __RPC_FAR * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo __RPC_FAR *__RPC_FAR *ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetIDsOfNames )( 
            IPDFColorSpace __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR __RPC_FAR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID __RPC_FAR *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Invoke )( 
            IPDFColorSpace __RPC_FAR * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS __RPC_FAR *pDispParams,
            /* [out] */ VARIANT __RPC_FAR *pVarResult,
            /* [out] */ EXCEPINFO __RPC_FAR *pExcepInfo,
            /* [out] */ UINT __RPC_FAR *puArgErr);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_ComponentsPerPixel )( 
            IPDFColorSpace __RPC_FAR * This,
            /* [retval][out] */ short __RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_BaseColorSpace )( 
            IPDFColorSpace __RPC_FAR * This,
            /* [retval][out] */ IPDFColorSpace __RPC_FAR *__RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_IsIndexed )( 
            IPDFColorSpace __RPC_FAR * This,
            /* [retval][out] */ VARIANT_BOOL __RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_High )( 
            IPDFColorSpace __RPC_FAR * This,
            /* [retval][out] */ short __RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_Lookup )( 
            IPDFColorSpace __RPC_FAR * This,
            /* [retval][out] */ VARIANT __RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_Name )( 
            IPDFColorSpace __RPC_FAR * This,
            /* [retval][out] */ BSTR __RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_IsMonochrome )( 
            IPDFColorSpace __RPC_FAR * This,
            /* [retval][out] */ VARIANT_BOOL __RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_IsColor )( 
            IPDFColorSpace __RPC_FAR * This,
            /* [retval][out] */ VARIANT_BOOL __RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_Colorant )( 
            IPDFColorSpace __RPC_FAR * This,
            /* [retval][out] */ VARIANT __RPC_FAR *pVal);
        
        END_INTERFACE
    } IPDFColorSpaceVtbl;

    interface IPDFColorSpace
    {
        CONST_VTBL struct IPDFColorSpaceVtbl __RPC_FAR *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IPDFColorSpace_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IPDFColorSpace_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IPDFColorSpace_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IPDFColorSpace_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IPDFColorSpace_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IPDFColorSpace_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IPDFColorSpace_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define IPDFColorSpace_get_ComponentsPerPixel(This,pVal)	\
    (This)->lpVtbl -> get_ComponentsPerPixel(This,pVal)

#define IPDFColorSpace_get_BaseColorSpace(This,pVal)	\
    (This)->lpVtbl -> get_BaseColorSpace(This,pVal)

#define IPDFColorSpace_get_IsIndexed(This,pVal)	\
    (This)->lpVtbl -> get_IsIndexed(This,pVal)

#define IPDFColorSpace_get_High(This,pVal)	\
    (This)->lpVtbl -> get_High(This,pVal)

#define IPDFColorSpace_get_Lookup(This,pVal)	\
    (This)->lpVtbl -> get_Lookup(This,pVal)

#define IPDFColorSpace_get_Name(This,pVal)	\
    (This)->lpVtbl -> get_Name(This,pVal)

#define IPDFColorSpace_get_IsMonochrome(This,pVal)	\
    (This)->lpVtbl -> get_IsMonochrome(This,pVal)

#define IPDFColorSpace_get_IsColor(This,pVal)	\
    (This)->lpVtbl -> get_IsColor(This,pVal)

#define IPDFColorSpace_get_Colorant(This,pVal)	\
    (This)->lpVtbl -> get_Colorant(This,pVal)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFColorSpace_get_ComponentsPerPixel_Proxy( 
    IPDFColorSpace __RPC_FAR * This,
    /* [retval][out] */ short __RPC_FAR *pVal);


void __RPC_STUB IPDFColorSpace_get_ComponentsPerPixel_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFColorSpace_get_BaseColorSpace_Proxy( 
    IPDFColorSpace __RPC_FAR * This,
    /* [retval][out] */ IPDFColorSpace __RPC_FAR *__RPC_FAR *pVal);


void __RPC_STUB IPDFColorSpace_get_BaseColorSpace_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFColorSpace_get_IsIndexed_Proxy( 
    IPDFColorSpace __RPC_FAR * This,
    /* [retval][out] */ VARIANT_BOOL __RPC_FAR *pVal);


void __RPC_STUB IPDFColorSpace_get_IsIndexed_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFColorSpace_get_High_Proxy( 
    IPDFColorSpace __RPC_FAR * This,
    /* [retval][out] */ short __RPC_FAR *pVal);


void __RPC_STUB IPDFColorSpace_get_High_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFColorSpace_get_Lookup_Proxy( 
    IPDFColorSpace __RPC_FAR * This,
    /* [retval][out] */ VARIANT __RPC_FAR *pVal);


void __RPC_STUB IPDFColorSpace_get_Lookup_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFColorSpace_get_Name_Proxy( 
    IPDFColorSpace __RPC_FAR * This,
    /* [retval][out] */ BSTR __RPC_FAR *pVal);


void __RPC_STUB IPDFColorSpace_get_Name_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFColorSpace_get_IsMonochrome_Proxy( 
    IPDFColorSpace __RPC_FAR * This,
    /* [retval][out] */ VARIANT_BOOL __RPC_FAR *pVal);


void __RPC_STUB IPDFColorSpace_get_IsMonochrome_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFColorSpace_get_IsColor_Proxy( 
    IPDFColorSpace __RPC_FAR * This,
    /* [retval][out] */ VARIANT_BOOL __RPC_FAR *pVal);


void __RPC_STUB IPDFColorSpace_get_IsColor_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFColorSpace_get_Colorant_Proxy( 
    IPDFColorSpace __RPC_FAR * This,
    /* [retval][out] */ VARIANT __RPC_FAR *pVal);


void __RPC_STUB IPDFColorSpace_get_Colorant_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IPDFColorSpace_INTERFACE_DEFINED__ */


#ifndef __IPDFTransformMatrix_INTERFACE_DEFINED__
#define __IPDFTransformMatrix_INTERFACE_DEFINED__

/* interface IPDFTransformMatrix */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_IPDFTransformMatrix;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("CC7AEA7A-40DD-4BB2-A9A7-6C34B85AF9EB")
    IPDFTransformMatrix : public IDispatch
    {
    public:
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_a( 
            /* [retval][out] */ float __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_b( 
            /* [retval][out] */ float __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_c( 
            /* [retval][out] */ float __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_d( 
            /* [retval][out] */ float __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_e( 
            /* [retval][out] */ float __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_f( 
            /* [retval][out] */ float __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_XSkew( 
            /* [retval][out] */ float __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_YSkew( 
            /* [retval][out] */ float __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_XTranslation( 
            /* [retval][out] */ float __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_YTranslation( 
            /* [retval][out] */ float __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_XScaling( 
            /* [retval][out] */ float __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_YScaling( 
            /* [retval][out] */ float __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Rotation( 
            /* [retval][out] */ float __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Orientation( 
            /* [retval][out] */ TPDFOrientation __RPC_FAR *pVal) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IPDFTransformMatrixVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *QueryInterface )( 
            IPDFTransformMatrix __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void __RPC_FAR *__RPC_FAR *ppvObject);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *AddRef )( 
            IPDFTransformMatrix __RPC_FAR * This);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *Release )( 
            IPDFTransformMatrix __RPC_FAR * This);
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetTypeInfoCount )( 
            IPDFTransformMatrix __RPC_FAR * This,
            /* [out] */ UINT __RPC_FAR *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetTypeInfo )( 
            IPDFTransformMatrix __RPC_FAR * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo __RPC_FAR *__RPC_FAR *ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetIDsOfNames )( 
            IPDFTransformMatrix __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR __RPC_FAR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID __RPC_FAR *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Invoke )( 
            IPDFTransformMatrix __RPC_FAR * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS __RPC_FAR *pDispParams,
            /* [out] */ VARIANT __RPC_FAR *pVarResult,
            /* [out] */ EXCEPINFO __RPC_FAR *pExcepInfo,
            /* [out] */ UINT __RPC_FAR *puArgErr);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_a )( 
            IPDFTransformMatrix __RPC_FAR * This,
            /* [retval][out] */ float __RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_b )( 
            IPDFTransformMatrix __RPC_FAR * This,
            /* [retval][out] */ float __RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_c )( 
            IPDFTransformMatrix __RPC_FAR * This,
            /* [retval][out] */ float __RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_d )( 
            IPDFTransformMatrix __RPC_FAR * This,
            /* [retval][out] */ float __RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_e )( 
            IPDFTransformMatrix __RPC_FAR * This,
            /* [retval][out] */ float __RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_f )( 
            IPDFTransformMatrix __RPC_FAR * This,
            /* [retval][out] */ float __RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_XSkew )( 
            IPDFTransformMatrix __RPC_FAR * This,
            /* [retval][out] */ float __RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_YSkew )( 
            IPDFTransformMatrix __RPC_FAR * This,
            /* [retval][out] */ float __RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_XTranslation )( 
            IPDFTransformMatrix __RPC_FAR * This,
            /* [retval][out] */ float __RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_YTranslation )( 
            IPDFTransformMatrix __RPC_FAR * This,
            /* [retval][out] */ float __RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_XScaling )( 
            IPDFTransformMatrix __RPC_FAR * This,
            /* [retval][out] */ float __RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_YScaling )( 
            IPDFTransformMatrix __RPC_FAR * This,
            /* [retval][out] */ float __RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_Rotation )( 
            IPDFTransformMatrix __RPC_FAR * This,
            /* [retval][out] */ float __RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_Orientation )( 
            IPDFTransformMatrix __RPC_FAR * This,
            /* [retval][out] */ TPDFOrientation __RPC_FAR *pVal);
        
        END_INTERFACE
    } IPDFTransformMatrixVtbl;

    interface IPDFTransformMatrix
    {
        CONST_VTBL struct IPDFTransformMatrixVtbl __RPC_FAR *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IPDFTransformMatrix_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IPDFTransformMatrix_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IPDFTransformMatrix_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IPDFTransformMatrix_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IPDFTransformMatrix_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IPDFTransformMatrix_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IPDFTransformMatrix_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define IPDFTransformMatrix_get_a(This,pVal)	\
    (This)->lpVtbl -> get_a(This,pVal)

#define IPDFTransformMatrix_get_b(This,pVal)	\
    (This)->lpVtbl -> get_b(This,pVal)

#define IPDFTransformMatrix_get_c(This,pVal)	\
    (This)->lpVtbl -> get_c(This,pVal)

#define IPDFTransformMatrix_get_d(This,pVal)	\
    (This)->lpVtbl -> get_d(This,pVal)

#define IPDFTransformMatrix_get_e(This,pVal)	\
    (This)->lpVtbl -> get_e(This,pVal)

#define IPDFTransformMatrix_get_f(This,pVal)	\
    (This)->lpVtbl -> get_f(This,pVal)

#define IPDFTransformMatrix_get_XSkew(This,pVal)	\
    (This)->lpVtbl -> get_XSkew(This,pVal)

#define IPDFTransformMatrix_get_YSkew(This,pVal)	\
    (This)->lpVtbl -> get_YSkew(This,pVal)

#define IPDFTransformMatrix_get_XTranslation(This,pVal)	\
    (This)->lpVtbl -> get_XTranslation(This,pVal)

#define IPDFTransformMatrix_get_YTranslation(This,pVal)	\
    (This)->lpVtbl -> get_YTranslation(This,pVal)

#define IPDFTransformMatrix_get_XScaling(This,pVal)	\
    (This)->lpVtbl -> get_XScaling(This,pVal)

#define IPDFTransformMatrix_get_YScaling(This,pVal)	\
    (This)->lpVtbl -> get_YScaling(This,pVal)

#define IPDFTransformMatrix_get_Rotation(This,pVal)	\
    (This)->lpVtbl -> get_Rotation(This,pVal)

#define IPDFTransformMatrix_get_Orientation(This,pVal)	\
    (This)->lpVtbl -> get_Orientation(This,pVal)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFTransformMatrix_get_a_Proxy( 
    IPDFTransformMatrix __RPC_FAR * This,
    /* [retval][out] */ float __RPC_FAR *pVal);


void __RPC_STUB IPDFTransformMatrix_get_a_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFTransformMatrix_get_b_Proxy( 
    IPDFTransformMatrix __RPC_FAR * This,
    /* [retval][out] */ float __RPC_FAR *pVal);


void __RPC_STUB IPDFTransformMatrix_get_b_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFTransformMatrix_get_c_Proxy( 
    IPDFTransformMatrix __RPC_FAR * This,
    /* [retval][out] */ float __RPC_FAR *pVal);


void __RPC_STUB IPDFTransformMatrix_get_c_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFTransformMatrix_get_d_Proxy( 
    IPDFTransformMatrix __RPC_FAR * This,
    /* [retval][out] */ float __RPC_FAR *pVal);


void __RPC_STUB IPDFTransformMatrix_get_d_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFTransformMatrix_get_e_Proxy( 
    IPDFTransformMatrix __RPC_FAR * This,
    /* [retval][out] */ float __RPC_FAR *pVal);


void __RPC_STUB IPDFTransformMatrix_get_e_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFTransformMatrix_get_f_Proxy( 
    IPDFTransformMatrix __RPC_FAR * This,
    /* [retval][out] */ float __RPC_FAR *pVal);


void __RPC_STUB IPDFTransformMatrix_get_f_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFTransformMatrix_get_XSkew_Proxy( 
    IPDFTransformMatrix __RPC_FAR * This,
    /* [retval][out] */ float __RPC_FAR *pVal);


void __RPC_STUB IPDFTransformMatrix_get_XSkew_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFTransformMatrix_get_YSkew_Proxy( 
    IPDFTransformMatrix __RPC_FAR * This,
    /* [retval][out] */ float __RPC_FAR *pVal);


void __RPC_STUB IPDFTransformMatrix_get_YSkew_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFTransformMatrix_get_XTranslation_Proxy( 
    IPDFTransformMatrix __RPC_FAR * This,
    /* [retval][out] */ float __RPC_FAR *pVal);


void __RPC_STUB IPDFTransformMatrix_get_XTranslation_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFTransformMatrix_get_YTranslation_Proxy( 
    IPDFTransformMatrix __RPC_FAR * This,
    /* [retval][out] */ float __RPC_FAR *pVal);


void __RPC_STUB IPDFTransformMatrix_get_YTranslation_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFTransformMatrix_get_XScaling_Proxy( 
    IPDFTransformMatrix __RPC_FAR * This,
    /* [retval][out] */ float __RPC_FAR *pVal);


void __RPC_STUB IPDFTransformMatrix_get_XScaling_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFTransformMatrix_get_YScaling_Proxy( 
    IPDFTransformMatrix __RPC_FAR * This,
    /* [retval][out] */ float __RPC_FAR *pVal);


void __RPC_STUB IPDFTransformMatrix_get_YScaling_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFTransformMatrix_get_Rotation_Proxy( 
    IPDFTransformMatrix __RPC_FAR * This,
    /* [retval][out] */ float __RPC_FAR *pVal);


void __RPC_STUB IPDFTransformMatrix_get_Rotation_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFTransformMatrix_get_Orientation_Proxy( 
    IPDFTransformMatrix __RPC_FAR * This,
    /* [retval][out] */ TPDFOrientation __RPC_FAR *pVal);


void __RPC_STUB IPDFTransformMatrix_get_Orientation_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IPDFTransformMatrix_INTERFACE_DEFINED__ */


#ifndef __IPDFFont_INTERFACE_DEFINED__
#define __IPDFFont_INTERFACE_DEFINED__

/* interface IPDFFont */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_IPDFFont;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("A7A57D1D-1041-401F-B836-BA1A0F1E54D7")
    IPDFFont : public IDispatch
    {
    public:
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Type( 
            /* [retval][out] */ BSTR __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_BaseName( 
            /* [retval][out] */ BSTR __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Widths( 
            /* [retval][out] */ VARIANT __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Encoding( 
            /* [retval][out] */ VARIANT __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Flags( 
            /* [retval][out] */ long __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_FontBBox( 
            /* [retval][out] */ VARIANT __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ItalicAngle( 
            /* [retval][out] */ float __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Ascent( 
            /* [retval][out] */ float __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Descent( 
            /* [retval][out] */ float __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Leading( 
            /* [retval][out] */ float __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_CapHeight( 
            /* [retval][out] */ float __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_XHeight( 
            /* [retval][out] */ float __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_StemV( 
            /* [retval][out] */ float __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_StemH( 
            /* [retval][out] */ float __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_AvgWidth( 
            /* [retval][out] */ float __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_MaxWidth( 
            /* [retval][out] */ float __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_MissingWidth( 
            /* [retval][out] */ float __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_FontFile( 
            /* [retval][out] */ VARIANT __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Charset( 
            /* [retval][out] */ BSTR __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_FontFileType( 
            /* [retval][out] */ short __RPC_FAR *pVal) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IPDFFontVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *QueryInterface )( 
            IPDFFont __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void __RPC_FAR *__RPC_FAR *ppvObject);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *AddRef )( 
            IPDFFont __RPC_FAR * This);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *Release )( 
            IPDFFont __RPC_FAR * This);
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetTypeInfoCount )( 
            IPDFFont __RPC_FAR * This,
            /* [out] */ UINT __RPC_FAR *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetTypeInfo )( 
            IPDFFont __RPC_FAR * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo __RPC_FAR *__RPC_FAR *ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetIDsOfNames )( 
            IPDFFont __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR __RPC_FAR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID __RPC_FAR *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Invoke )( 
            IPDFFont __RPC_FAR * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS __RPC_FAR *pDispParams,
            /* [out] */ VARIANT __RPC_FAR *pVarResult,
            /* [out] */ EXCEPINFO __RPC_FAR *pExcepInfo,
            /* [out] */ UINT __RPC_FAR *puArgErr);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_Type )( 
            IPDFFont __RPC_FAR * This,
            /* [retval][out] */ BSTR __RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_BaseName )( 
            IPDFFont __RPC_FAR * This,
            /* [retval][out] */ BSTR __RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_Widths )( 
            IPDFFont __RPC_FAR * This,
            /* [retval][out] */ VARIANT __RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_Encoding )( 
            IPDFFont __RPC_FAR * This,
            /* [retval][out] */ VARIANT __RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_Flags )( 
            IPDFFont __RPC_FAR * This,
            /* [retval][out] */ long __RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_FontBBox )( 
            IPDFFont __RPC_FAR * This,
            /* [retval][out] */ VARIANT __RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_ItalicAngle )( 
            IPDFFont __RPC_FAR * This,
            /* [retval][out] */ float __RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_Ascent )( 
            IPDFFont __RPC_FAR * This,
            /* [retval][out] */ float __RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_Descent )( 
            IPDFFont __RPC_FAR * This,
            /* [retval][out] */ float __RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_Leading )( 
            IPDFFont __RPC_FAR * This,
            /* [retval][out] */ float __RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_CapHeight )( 
            IPDFFont __RPC_FAR * This,
            /* [retval][out] */ float __RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_XHeight )( 
            IPDFFont __RPC_FAR * This,
            /* [retval][out] */ float __RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_StemV )( 
            IPDFFont __RPC_FAR * This,
            /* [retval][out] */ float __RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_StemH )( 
            IPDFFont __RPC_FAR * This,
            /* [retval][out] */ float __RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_AvgWidth )( 
            IPDFFont __RPC_FAR * This,
            /* [retval][out] */ float __RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_MaxWidth )( 
            IPDFFont __RPC_FAR * This,
            /* [retval][out] */ float __RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_MissingWidth )( 
            IPDFFont __RPC_FAR * This,
            /* [retval][out] */ float __RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_FontFile )( 
            IPDFFont __RPC_FAR * This,
            /* [retval][out] */ VARIANT __RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_Charset )( 
            IPDFFont __RPC_FAR * This,
            /* [retval][out] */ BSTR __RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_FontFileType )( 
            IPDFFont __RPC_FAR * This,
            /* [retval][out] */ short __RPC_FAR *pVal);
        
        END_INTERFACE
    } IPDFFontVtbl;

    interface IPDFFont
    {
        CONST_VTBL struct IPDFFontVtbl __RPC_FAR *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IPDFFont_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IPDFFont_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IPDFFont_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IPDFFont_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IPDFFont_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IPDFFont_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IPDFFont_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define IPDFFont_get_Type(This,pVal)	\
    (This)->lpVtbl -> get_Type(This,pVal)

#define IPDFFont_get_BaseName(This,pVal)	\
    (This)->lpVtbl -> get_BaseName(This,pVal)

#define IPDFFont_get_Widths(This,pVal)	\
    (This)->lpVtbl -> get_Widths(This,pVal)

#define IPDFFont_get_Encoding(This,pVal)	\
    (This)->lpVtbl -> get_Encoding(This,pVal)

#define IPDFFont_get_Flags(This,pVal)	\
    (This)->lpVtbl -> get_Flags(This,pVal)

#define IPDFFont_get_FontBBox(This,pVal)	\
    (This)->lpVtbl -> get_FontBBox(This,pVal)

#define IPDFFont_get_ItalicAngle(This,pVal)	\
    (This)->lpVtbl -> get_ItalicAngle(This,pVal)

#define IPDFFont_get_Ascent(This,pVal)	\
    (This)->lpVtbl -> get_Ascent(This,pVal)

#define IPDFFont_get_Descent(This,pVal)	\
    (This)->lpVtbl -> get_Descent(This,pVal)

#define IPDFFont_get_Leading(This,pVal)	\
    (This)->lpVtbl -> get_Leading(This,pVal)

#define IPDFFont_get_CapHeight(This,pVal)	\
    (This)->lpVtbl -> get_CapHeight(This,pVal)

#define IPDFFont_get_XHeight(This,pVal)	\
    (This)->lpVtbl -> get_XHeight(This,pVal)

#define IPDFFont_get_StemV(This,pVal)	\
    (This)->lpVtbl -> get_StemV(This,pVal)

#define IPDFFont_get_StemH(This,pVal)	\
    (This)->lpVtbl -> get_StemH(This,pVal)

#define IPDFFont_get_AvgWidth(This,pVal)	\
    (This)->lpVtbl -> get_AvgWidth(This,pVal)

#define IPDFFont_get_MaxWidth(This,pVal)	\
    (This)->lpVtbl -> get_MaxWidth(This,pVal)

#define IPDFFont_get_MissingWidth(This,pVal)	\
    (This)->lpVtbl -> get_MissingWidth(This,pVal)

#define IPDFFont_get_FontFile(This,pVal)	\
    (This)->lpVtbl -> get_FontFile(This,pVal)

#define IPDFFont_get_Charset(This,pVal)	\
    (This)->lpVtbl -> get_Charset(This,pVal)

#define IPDFFont_get_FontFileType(This,pVal)	\
    (This)->lpVtbl -> get_FontFileType(This,pVal)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFFont_get_Type_Proxy( 
    IPDFFont __RPC_FAR * This,
    /* [retval][out] */ BSTR __RPC_FAR *pVal);


void __RPC_STUB IPDFFont_get_Type_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFFont_get_BaseName_Proxy( 
    IPDFFont __RPC_FAR * This,
    /* [retval][out] */ BSTR __RPC_FAR *pVal);


void __RPC_STUB IPDFFont_get_BaseName_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFFont_get_Widths_Proxy( 
    IPDFFont __RPC_FAR * This,
    /* [retval][out] */ VARIANT __RPC_FAR *pVal);


void __RPC_STUB IPDFFont_get_Widths_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFFont_get_Encoding_Proxy( 
    IPDFFont __RPC_FAR * This,
    /* [retval][out] */ VARIANT __RPC_FAR *pVal);


void __RPC_STUB IPDFFont_get_Encoding_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFFont_get_Flags_Proxy( 
    IPDFFont __RPC_FAR * This,
    /* [retval][out] */ long __RPC_FAR *pVal);


void __RPC_STUB IPDFFont_get_Flags_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFFont_get_FontBBox_Proxy( 
    IPDFFont __RPC_FAR * This,
    /* [retval][out] */ VARIANT __RPC_FAR *pVal);


void __RPC_STUB IPDFFont_get_FontBBox_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFFont_get_ItalicAngle_Proxy( 
    IPDFFont __RPC_FAR * This,
    /* [retval][out] */ float __RPC_FAR *pVal);


void __RPC_STUB IPDFFont_get_ItalicAngle_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFFont_get_Ascent_Proxy( 
    IPDFFont __RPC_FAR * This,
    /* [retval][out] */ float __RPC_FAR *pVal);


void __RPC_STUB IPDFFont_get_Ascent_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFFont_get_Descent_Proxy( 
    IPDFFont __RPC_FAR * This,
    /* [retval][out] */ float __RPC_FAR *pVal);


void __RPC_STUB IPDFFont_get_Descent_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFFont_get_Leading_Proxy( 
    IPDFFont __RPC_FAR * This,
    /* [retval][out] */ float __RPC_FAR *pVal);


void __RPC_STUB IPDFFont_get_Leading_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFFont_get_CapHeight_Proxy( 
    IPDFFont __RPC_FAR * This,
    /* [retval][out] */ float __RPC_FAR *pVal);


void __RPC_STUB IPDFFont_get_CapHeight_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFFont_get_XHeight_Proxy( 
    IPDFFont __RPC_FAR * This,
    /* [retval][out] */ float __RPC_FAR *pVal);


void __RPC_STUB IPDFFont_get_XHeight_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFFont_get_StemV_Proxy( 
    IPDFFont __RPC_FAR * This,
    /* [retval][out] */ float __RPC_FAR *pVal);


void __RPC_STUB IPDFFont_get_StemV_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFFont_get_StemH_Proxy( 
    IPDFFont __RPC_FAR * This,
    /* [retval][out] */ float __RPC_FAR *pVal);


void __RPC_STUB IPDFFont_get_StemH_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFFont_get_AvgWidth_Proxy( 
    IPDFFont __RPC_FAR * This,
    /* [retval][out] */ float __RPC_FAR *pVal);


void __RPC_STUB IPDFFont_get_AvgWidth_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFFont_get_MaxWidth_Proxy( 
    IPDFFont __RPC_FAR * This,
    /* [retval][out] */ float __RPC_FAR *pVal);


void __RPC_STUB IPDFFont_get_MaxWidth_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFFont_get_MissingWidth_Proxy( 
    IPDFFont __RPC_FAR * This,
    /* [retval][out] */ float __RPC_FAR *pVal);


void __RPC_STUB IPDFFont_get_MissingWidth_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFFont_get_FontFile_Proxy( 
    IPDFFont __RPC_FAR * This,
    /* [retval][out] */ VARIANT __RPC_FAR *pVal);


void __RPC_STUB IPDFFont_get_FontFile_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFFont_get_Charset_Proxy( 
    IPDFFont __RPC_FAR * This,
    /* [retval][out] */ BSTR __RPC_FAR *pVal);


void __RPC_STUB IPDFFont_get_Charset_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFFont_get_FontFileType_Proxy( 
    IPDFFont __RPC_FAR * This,
    /* [retval][out] */ short __RPC_FAR *pVal);


void __RPC_STUB IPDFFont_get_FontFileType_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IPDFFont_INTERFACE_DEFINED__ */


#ifndef __IPDFGraphicsState_INTERFACE_DEFINED__
#define __IPDFGraphicsState_INTERFACE_DEFINED__

/* interface IPDFGraphicsState */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_IPDFGraphicsState;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("91E60779-E8CF-4810-8233-9DBB12682DD6")
    IPDFGraphicsState : public IDispatch
    {
    public:
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_CTM( 
            /* [retval][out] */ IPDFTransformMatrix __RPC_FAR *__RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_StrokeColorSpace( 
            /* [retval][out] */ IPDFColorSpace __RPC_FAR *__RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_FillColorSpace( 
            /* [retval][out] */ IPDFColorSpace __RPC_FAR *__RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_StrokeColorRGB( 
            /* [retval][out] */ long __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_FillColorRGB( 
            /* [retval][out] */ long __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_StrokeColorCMYK( 
            /* [retval][out] */ long __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_FillColorCMYK( 
            /* [retval][out] */ long __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_CharSpacing( 
            /* [retval][out] */ float __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_WordSpacing( 
            /* [retval][out] */ float __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_HorizontalScaling( 
            /* [retval][out] */ float __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Leading( 
            /* [retval][out] */ float __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Font( 
            /* [retval][out] */ IPDFFont __RPC_FAR *__RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_FontSize( 
            /* [retval][out] */ float __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_TextRenderingMode( 
            /* [retval][out] */ short __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_TextRise( 
            /* [retval][out] */ float __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_TextKnockout( 
            /* [retval][out] */ VARIANT_BOOL __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_LineWidth( 
            /* [retval][out] */ float __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_LineCap( 
            /* [retval][out] */ short __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_LineJoin( 
            /* [retval][out] */ short __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_MiterLimit( 
            /* [retval][out] */ float __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_DashPhase( 
            /* [retval][out] */ float __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_DashArray( 
            /* [retval][out] */ VARIANT __RPC_FAR *pVal) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IPDFGraphicsStateVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *QueryInterface )( 
            IPDFGraphicsState __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void __RPC_FAR *__RPC_FAR *ppvObject);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *AddRef )( 
            IPDFGraphicsState __RPC_FAR * This);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *Release )( 
            IPDFGraphicsState __RPC_FAR * This);
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetTypeInfoCount )( 
            IPDFGraphicsState __RPC_FAR * This,
            /* [out] */ UINT __RPC_FAR *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetTypeInfo )( 
            IPDFGraphicsState __RPC_FAR * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo __RPC_FAR *__RPC_FAR *ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetIDsOfNames )( 
            IPDFGraphicsState __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR __RPC_FAR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID __RPC_FAR *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Invoke )( 
            IPDFGraphicsState __RPC_FAR * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS __RPC_FAR *pDispParams,
            /* [out] */ VARIANT __RPC_FAR *pVarResult,
            /* [out] */ EXCEPINFO __RPC_FAR *pExcepInfo,
            /* [out] */ UINT __RPC_FAR *puArgErr);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_CTM )( 
            IPDFGraphicsState __RPC_FAR * This,
            /* [retval][out] */ IPDFTransformMatrix __RPC_FAR *__RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_StrokeColorSpace )( 
            IPDFGraphicsState __RPC_FAR * This,
            /* [retval][out] */ IPDFColorSpace __RPC_FAR *__RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_FillColorSpace )( 
            IPDFGraphicsState __RPC_FAR * This,
            /* [retval][out] */ IPDFColorSpace __RPC_FAR *__RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_StrokeColorRGB )( 
            IPDFGraphicsState __RPC_FAR * This,
            /* [retval][out] */ long __RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_FillColorRGB )( 
            IPDFGraphicsState __RPC_FAR * This,
            /* [retval][out] */ long __RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_StrokeColorCMYK )( 
            IPDFGraphicsState __RPC_FAR * This,
            /* [retval][out] */ long __RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_FillColorCMYK )( 
            IPDFGraphicsState __RPC_FAR * This,
            /* [retval][out] */ long __RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_CharSpacing )( 
            IPDFGraphicsState __RPC_FAR * This,
            /* [retval][out] */ float __RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_WordSpacing )( 
            IPDFGraphicsState __RPC_FAR * This,
            /* [retval][out] */ float __RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_HorizontalScaling )( 
            IPDFGraphicsState __RPC_FAR * This,
            /* [retval][out] */ float __RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_Leading )( 
            IPDFGraphicsState __RPC_FAR * This,
            /* [retval][out] */ float __RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_Font )( 
            IPDFGraphicsState __RPC_FAR * This,
            /* [retval][out] */ IPDFFont __RPC_FAR *__RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_FontSize )( 
            IPDFGraphicsState __RPC_FAR * This,
            /* [retval][out] */ float __RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_TextRenderingMode )( 
            IPDFGraphicsState __RPC_FAR * This,
            /* [retval][out] */ short __RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_TextRise )( 
            IPDFGraphicsState __RPC_FAR * This,
            /* [retval][out] */ float __RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_TextKnockout )( 
            IPDFGraphicsState __RPC_FAR * This,
            /* [retval][out] */ VARIANT_BOOL __RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_LineWidth )( 
            IPDFGraphicsState __RPC_FAR * This,
            /* [retval][out] */ float __RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_LineCap )( 
            IPDFGraphicsState __RPC_FAR * This,
            /* [retval][out] */ short __RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_LineJoin )( 
            IPDFGraphicsState __RPC_FAR * This,
            /* [retval][out] */ short __RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_MiterLimit )( 
            IPDFGraphicsState __RPC_FAR * This,
            /* [retval][out] */ float __RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_DashPhase )( 
            IPDFGraphicsState __RPC_FAR * This,
            /* [retval][out] */ float __RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_DashArray )( 
            IPDFGraphicsState __RPC_FAR * This,
            /* [retval][out] */ VARIANT __RPC_FAR *pVal);
        
        END_INTERFACE
    } IPDFGraphicsStateVtbl;

    interface IPDFGraphicsState
    {
        CONST_VTBL struct IPDFGraphicsStateVtbl __RPC_FAR *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IPDFGraphicsState_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IPDFGraphicsState_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IPDFGraphicsState_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IPDFGraphicsState_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IPDFGraphicsState_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IPDFGraphicsState_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IPDFGraphicsState_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define IPDFGraphicsState_get_CTM(This,pVal)	\
    (This)->lpVtbl -> get_CTM(This,pVal)

#define IPDFGraphicsState_get_StrokeColorSpace(This,pVal)	\
    (This)->lpVtbl -> get_StrokeColorSpace(This,pVal)

#define IPDFGraphicsState_get_FillColorSpace(This,pVal)	\
    (This)->lpVtbl -> get_FillColorSpace(This,pVal)

#define IPDFGraphicsState_get_StrokeColorRGB(This,pVal)	\
    (This)->lpVtbl -> get_StrokeColorRGB(This,pVal)

#define IPDFGraphicsState_get_FillColorRGB(This,pVal)	\
    (This)->lpVtbl -> get_FillColorRGB(This,pVal)

#define IPDFGraphicsState_get_StrokeColorCMYK(This,pVal)	\
    (This)->lpVtbl -> get_StrokeColorCMYK(This,pVal)

#define IPDFGraphicsState_get_FillColorCMYK(This,pVal)	\
    (This)->lpVtbl -> get_FillColorCMYK(This,pVal)

#define IPDFGraphicsState_get_CharSpacing(This,pVal)	\
    (This)->lpVtbl -> get_CharSpacing(This,pVal)

#define IPDFGraphicsState_get_WordSpacing(This,pVal)	\
    (This)->lpVtbl -> get_WordSpacing(This,pVal)

#define IPDFGraphicsState_get_HorizontalScaling(This,pVal)	\
    (This)->lpVtbl -> get_HorizontalScaling(This,pVal)

#define IPDFGraphicsState_get_Leading(This,pVal)	\
    (This)->lpVtbl -> get_Leading(This,pVal)

#define IPDFGraphicsState_get_Font(This,pVal)	\
    (This)->lpVtbl -> get_Font(This,pVal)

#define IPDFGraphicsState_get_FontSize(This,pVal)	\
    (This)->lpVtbl -> get_FontSize(This,pVal)

#define IPDFGraphicsState_get_TextRenderingMode(This,pVal)	\
    (This)->lpVtbl -> get_TextRenderingMode(This,pVal)

#define IPDFGraphicsState_get_TextRise(This,pVal)	\
    (This)->lpVtbl -> get_TextRise(This,pVal)

#define IPDFGraphicsState_get_TextKnockout(This,pVal)	\
    (This)->lpVtbl -> get_TextKnockout(This,pVal)

#define IPDFGraphicsState_get_LineWidth(This,pVal)	\
    (This)->lpVtbl -> get_LineWidth(This,pVal)

#define IPDFGraphicsState_get_LineCap(This,pVal)	\
    (This)->lpVtbl -> get_LineCap(This,pVal)

#define IPDFGraphicsState_get_LineJoin(This,pVal)	\
    (This)->lpVtbl -> get_LineJoin(This,pVal)

#define IPDFGraphicsState_get_MiterLimit(This,pVal)	\
    (This)->lpVtbl -> get_MiterLimit(This,pVal)

#define IPDFGraphicsState_get_DashPhase(This,pVal)	\
    (This)->lpVtbl -> get_DashPhase(This,pVal)

#define IPDFGraphicsState_get_DashArray(This,pVal)	\
    (This)->lpVtbl -> get_DashArray(This,pVal)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFGraphicsState_get_CTM_Proxy( 
    IPDFGraphicsState __RPC_FAR * This,
    /* [retval][out] */ IPDFTransformMatrix __RPC_FAR *__RPC_FAR *pVal);


void __RPC_STUB IPDFGraphicsState_get_CTM_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFGraphicsState_get_StrokeColorSpace_Proxy( 
    IPDFGraphicsState __RPC_FAR * This,
    /* [retval][out] */ IPDFColorSpace __RPC_FAR *__RPC_FAR *pVal);


void __RPC_STUB IPDFGraphicsState_get_StrokeColorSpace_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFGraphicsState_get_FillColorSpace_Proxy( 
    IPDFGraphicsState __RPC_FAR * This,
    /* [retval][out] */ IPDFColorSpace __RPC_FAR *__RPC_FAR *pVal);


void __RPC_STUB IPDFGraphicsState_get_FillColorSpace_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFGraphicsState_get_StrokeColorRGB_Proxy( 
    IPDFGraphicsState __RPC_FAR * This,
    /* [retval][out] */ long __RPC_FAR *pVal);


void __RPC_STUB IPDFGraphicsState_get_StrokeColorRGB_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFGraphicsState_get_FillColorRGB_Proxy( 
    IPDFGraphicsState __RPC_FAR * This,
    /* [retval][out] */ long __RPC_FAR *pVal);


void __RPC_STUB IPDFGraphicsState_get_FillColorRGB_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFGraphicsState_get_StrokeColorCMYK_Proxy( 
    IPDFGraphicsState __RPC_FAR * This,
    /* [retval][out] */ long __RPC_FAR *pVal);


void __RPC_STUB IPDFGraphicsState_get_StrokeColorCMYK_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFGraphicsState_get_FillColorCMYK_Proxy( 
    IPDFGraphicsState __RPC_FAR * This,
    /* [retval][out] */ long __RPC_FAR *pVal);


void __RPC_STUB IPDFGraphicsState_get_FillColorCMYK_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFGraphicsState_get_CharSpacing_Proxy( 
    IPDFGraphicsState __RPC_FAR * This,
    /* [retval][out] */ float __RPC_FAR *pVal);


void __RPC_STUB IPDFGraphicsState_get_CharSpacing_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFGraphicsState_get_WordSpacing_Proxy( 
    IPDFGraphicsState __RPC_FAR * This,
    /* [retval][out] */ float __RPC_FAR *pVal);


void __RPC_STUB IPDFGraphicsState_get_WordSpacing_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFGraphicsState_get_HorizontalScaling_Proxy( 
    IPDFGraphicsState __RPC_FAR * This,
    /* [retval][out] */ float __RPC_FAR *pVal);


void __RPC_STUB IPDFGraphicsState_get_HorizontalScaling_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFGraphicsState_get_Leading_Proxy( 
    IPDFGraphicsState __RPC_FAR * This,
    /* [retval][out] */ float __RPC_FAR *pVal);


void __RPC_STUB IPDFGraphicsState_get_Leading_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFGraphicsState_get_Font_Proxy( 
    IPDFGraphicsState __RPC_FAR * This,
    /* [retval][out] */ IPDFFont __RPC_FAR *__RPC_FAR *pVal);


void __RPC_STUB IPDFGraphicsState_get_Font_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFGraphicsState_get_FontSize_Proxy( 
    IPDFGraphicsState __RPC_FAR * This,
    /* [retval][out] */ float __RPC_FAR *pVal);


void __RPC_STUB IPDFGraphicsState_get_FontSize_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFGraphicsState_get_TextRenderingMode_Proxy( 
    IPDFGraphicsState __RPC_FAR * This,
    /* [retval][out] */ short __RPC_FAR *pVal);


void __RPC_STUB IPDFGraphicsState_get_TextRenderingMode_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFGraphicsState_get_TextRise_Proxy( 
    IPDFGraphicsState __RPC_FAR * This,
    /* [retval][out] */ float __RPC_FAR *pVal);


void __RPC_STUB IPDFGraphicsState_get_TextRise_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFGraphicsState_get_TextKnockout_Proxy( 
    IPDFGraphicsState __RPC_FAR * This,
    /* [retval][out] */ VARIANT_BOOL __RPC_FAR *pVal);


void __RPC_STUB IPDFGraphicsState_get_TextKnockout_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFGraphicsState_get_LineWidth_Proxy( 
    IPDFGraphicsState __RPC_FAR * This,
    /* [retval][out] */ float __RPC_FAR *pVal);


void __RPC_STUB IPDFGraphicsState_get_LineWidth_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFGraphicsState_get_LineCap_Proxy( 
    IPDFGraphicsState __RPC_FAR * This,
    /* [retval][out] */ short __RPC_FAR *pVal);


void __RPC_STUB IPDFGraphicsState_get_LineCap_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFGraphicsState_get_LineJoin_Proxy( 
    IPDFGraphicsState __RPC_FAR * This,
    /* [retval][out] */ short __RPC_FAR *pVal);


void __RPC_STUB IPDFGraphicsState_get_LineJoin_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFGraphicsState_get_MiterLimit_Proxy( 
    IPDFGraphicsState __RPC_FAR * This,
    /* [retval][out] */ float __RPC_FAR *pVal);


void __RPC_STUB IPDFGraphicsState_get_MiterLimit_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFGraphicsState_get_DashPhase_Proxy( 
    IPDFGraphicsState __RPC_FAR * This,
    /* [retval][out] */ float __RPC_FAR *pVal);


void __RPC_STUB IPDFGraphicsState_get_DashPhase_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFGraphicsState_get_DashArray_Proxy( 
    IPDFGraphicsState __RPC_FAR * This,
    /* [retval][out] */ VARIANT __RPC_FAR *pVal);


void __RPC_STUB IPDFGraphicsState_get_DashArray_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IPDFGraphicsState_INTERFACE_DEFINED__ */


#ifndef __IPDFText_INTERFACE_DEFINED__
#define __IPDFText_INTERFACE_DEFINED__

/* interface IPDFText */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_IPDFText;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("6679C1EC-128E-4EE2-9963-7D4F7047DCA3")
    IPDFText : public IDispatch
    {
    public:
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_UnicodeString( 
            /* [retval][out] */ BSTR __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_BoundingBox( 
            /* [retval][out] */ VARIANT __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_FontSize( 
            /* [retval][out] */ float __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Width( 
            /* [retval][out] */ float __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Rotation( 
            /* [retval][out] */ float __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Length( 
            /* [retval][out] */ short __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_XPos( 
            /* [retval][out] */ VARIANT __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_YPos( 
            /* [retval][out] */ VARIANT __RPC_FAR *pVal) = 0;
        
        virtual /* [hidden][id][propget] */ HRESULT STDMETHODCALLTYPE get_NextXPos( 
            /* [retval][out] */ VARIANT __RPC_FAR *pVal) = 0;
        
        virtual /* [hidden][id][propget] */ HRESULT STDMETHODCALLTYPE get_TextMatrix( 
            /* [retval][out] */ IPDFTransformMatrix __RPC_FAR *__RPC_FAR *pVal) = 0;
        
        virtual /* [hidden][id][propget] */ HRESULT STDMETHODCALLTYPE get_NextYPos( 
            /* [retval][out] */ VARIANT __RPC_FAR *pVal) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IPDFTextVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *QueryInterface )( 
            IPDFText __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void __RPC_FAR *__RPC_FAR *ppvObject);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *AddRef )( 
            IPDFText __RPC_FAR * This);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *Release )( 
            IPDFText __RPC_FAR * This);
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetTypeInfoCount )( 
            IPDFText __RPC_FAR * This,
            /* [out] */ UINT __RPC_FAR *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetTypeInfo )( 
            IPDFText __RPC_FAR * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo __RPC_FAR *__RPC_FAR *ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetIDsOfNames )( 
            IPDFText __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR __RPC_FAR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID __RPC_FAR *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Invoke )( 
            IPDFText __RPC_FAR * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS __RPC_FAR *pDispParams,
            /* [out] */ VARIANT __RPC_FAR *pVarResult,
            /* [out] */ EXCEPINFO __RPC_FAR *pExcepInfo,
            /* [out] */ UINT __RPC_FAR *puArgErr);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_UnicodeString )( 
            IPDFText __RPC_FAR * This,
            /* [retval][out] */ BSTR __RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_BoundingBox )( 
            IPDFText __RPC_FAR * This,
            /* [retval][out] */ VARIANT __RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_FontSize )( 
            IPDFText __RPC_FAR * This,
            /* [retval][out] */ float __RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_Width )( 
            IPDFText __RPC_FAR * This,
            /* [retval][out] */ float __RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_Rotation )( 
            IPDFText __RPC_FAR * This,
            /* [retval][out] */ float __RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_Length )( 
            IPDFText __RPC_FAR * This,
            /* [retval][out] */ short __RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_XPos )( 
            IPDFText __RPC_FAR * This,
            /* [retval][out] */ VARIANT __RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_YPos )( 
            IPDFText __RPC_FAR * This,
            /* [retval][out] */ VARIANT __RPC_FAR *pVal);
        
        /* [hidden][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_NextXPos )( 
            IPDFText __RPC_FAR * This,
            /* [retval][out] */ VARIANT __RPC_FAR *pVal);
        
        /* [hidden][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_TextMatrix )( 
            IPDFText __RPC_FAR * This,
            /* [retval][out] */ IPDFTransformMatrix __RPC_FAR *__RPC_FAR *pVal);
        
        /* [hidden][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_NextYPos )( 
            IPDFText __RPC_FAR * This,
            /* [retval][out] */ VARIANT __RPC_FAR *pVal);
        
        END_INTERFACE
    } IPDFTextVtbl;

    interface IPDFText
    {
        CONST_VTBL struct IPDFTextVtbl __RPC_FAR *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IPDFText_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IPDFText_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IPDFText_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IPDFText_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IPDFText_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IPDFText_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IPDFText_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define IPDFText_get_UnicodeString(This,pVal)	\
    (This)->lpVtbl -> get_UnicodeString(This,pVal)

#define IPDFText_get_BoundingBox(This,pVal)	\
    (This)->lpVtbl -> get_BoundingBox(This,pVal)

#define IPDFText_get_FontSize(This,pVal)	\
    (This)->lpVtbl -> get_FontSize(This,pVal)

#define IPDFText_get_Width(This,pVal)	\
    (This)->lpVtbl -> get_Width(This,pVal)

#define IPDFText_get_Rotation(This,pVal)	\
    (This)->lpVtbl -> get_Rotation(This,pVal)

#define IPDFText_get_Length(This,pVal)	\
    (This)->lpVtbl -> get_Length(This,pVal)

#define IPDFText_get_XPos(This,pVal)	\
    (This)->lpVtbl -> get_XPos(This,pVal)

#define IPDFText_get_YPos(This,pVal)	\
    (This)->lpVtbl -> get_YPos(This,pVal)

#define IPDFText_get_NextXPos(This,pVal)	\
    (This)->lpVtbl -> get_NextXPos(This,pVal)

#define IPDFText_get_TextMatrix(This,pVal)	\
    (This)->lpVtbl -> get_TextMatrix(This,pVal)

#define IPDFText_get_NextYPos(This,pVal)	\
    (This)->lpVtbl -> get_NextYPos(This,pVal)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFText_get_UnicodeString_Proxy( 
    IPDFText __RPC_FAR * This,
    /* [retval][out] */ BSTR __RPC_FAR *pVal);


void __RPC_STUB IPDFText_get_UnicodeString_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFText_get_BoundingBox_Proxy( 
    IPDFText __RPC_FAR * This,
    /* [retval][out] */ VARIANT __RPC_FAR *pVal);


void __RPC_STUB IPDFText_get_BoundingBox_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFText_get_FontSize_Proxy( 
    IPDFText __RPC_FAR * This,
    /* [retval][out] */ float __RPC_FAR *pVal);


void __RPC_STUB IPDFText_get_FontSize_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFText_get_Width_Proxy( 
    IPDFText __RPC_FAR * This,
    /* [retval][out] */ float __RPC_FAR *pVal);


void __RPC_STUB IPDFText_get_Width_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFText_get_Rotation_Proxy( 
    IPDFText __RPC_FAR * This,
    /* [retval][out] */ float __RPC_FAR *pVal);


void __RPC_STUB IPDFText_get_Rotation_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFText_get_Length_Proxy( 
    IPDFText __RPC_FAR * This,
    /* [retval][out] */ short __RPC_FAR *pVal);


void __RPC_STUB IPDFText_get_Length_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFText_get_XPos_Proxy( 
    IPDFText __RPC_FAR * This,
    /* [retval][out] */ VARIANT __RPC_FAR *pVal);


void __RPC_STUB IPDFText_get_XPos_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFText_get_YPos_Proxy( 
    IPDFText __RPC_FAR * This,
    /* [retval][out] */ VARIANT __RPC_FAR *pVal);


void __RPC_STUB IPDFText_get_YPos_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [hidden][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFText_get_NextXPos_Proxy( 
    IPDFText __RPC_FAR * This,
    /* [retval][out] */ VARIANT __RPC_FAR *pVal);


void __RPC_STUB IPDFText_get_NextXPos_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [hidden][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFText_get_TextMatrix_Proxy( 
    IPDFText __RPC_FAR * This,
    /* [retval][out] */ IPDFTransformMatrix __RPC_FAR *__RPC_FAR *pVal);


void __RPC_STUB IPDFText_get_TextMatrix_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [hidden][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFText_get_NextYPos_Proxy( 
    IPDFText __RPC_FAR * This,
    /* [retval][out] */ VARIANT __RPC_FAR *pVal);


void __RPC_STUB IPDFText_get_NextYPos_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IPDFText_INTERFACE_DEFINED__ */


#ifndef __IPDFImage_INTERFACE_DEFINED__
#define __IPDFImage_INTERFACE_DEFINED__

/* interface IPDFImage */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_IPDFImage;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("4BCEA86A-E1CD-41EF-B317-C652E39AA204")
    IPDFImage : public IDispatch
    {
    public:
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Width( 
            /* [retval][out] */ long __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Height( 
            /* [retval][out] */ long __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ColorSpace( 
            /* [retval][out] */ IPDFColorSpace __RPC_FAR *__RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_BitsPerComponent( 
            /* [retval][out] */ short __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Samples( 
            /* [retval][out] */ VARIANT __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Mask( 
            /* [retval][out] */ VARIANT __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Store( 
            BSTR bstrPath,
            /* [optional] */ TPDFCompression iCompression,
            /* [retval][out] */ VARIANT_BOOL __RPC_FAR *pDone) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE StoreInMemory( 
            BSTR bstrExtension,
            /* [optional] */ TPDFCompression iCompression,
            /* [retval][out] */ VARIANT_BOOL __RPC_FAR *pDone) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetImage( 
            /* [retval][out] */ VARIANT __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ConvertToRGB( 
            /* [retval][out] */ VARIANT_BOOL __RPC_FAR *pDone) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ChangeOrientation( 
            /* [in] */ TPDFOrientation iOrientation,
            /* [retval][out] */ VARIANT_BOOL __RPC_FAR *pDone) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Alternates( 
            /* [retval][out] */ VARIANT __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_IsBitonal( 
            /* [retval][out] */ VARIANT_BOOL __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_IsMonochrome( 
            /* [retval][out] */ VARIANT_BOOL __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_IsColor( 
            /* [retval][out] */ VARIANT_BOOL __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetResolution( 
            IPDFTransformMatrix __RPC_FAR *pMatrix,
            /* [retval][out] */ float __RPC_FAR *pVal) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IPDFImageVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *QueryInterface )( 
            IPDFImage __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void __RPC_FAR *__RPC_FAR *ppvObject);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *AddRef )( 
            IPDFImage __RPC_FAR * This);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *Release )( 
            IPDFImage __RPC_FAR * This);
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetTypeInfoCount )( 
            IPDFImage __RPC_FAR * This,
            /* [out] */ UINT __RPC_FAR *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetTypeInfo )( 
            IPDFImage __RPC_FAR * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo __RPC_FAR *__RPC_FAR *ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetIDsOfNames )( 
            IPDFImage __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR __RPC_FAR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID __RPC_FAR *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Invoke )( 
            IPDFImage __RPC_FAR * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS __RPC_FAR *pDispParams,
            /* [out] */ VARIANT __RPC_FAR *pVarResult,
            /* [out] */ EXCEPINFO __RPC_FAR *pExcepInfo,
            /* [out] */ UINT __RPC_FAR *puArgErr);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_Width )( 
            IPDFImage __RPC_FAR * This,
            /* [retval][out] */ long __RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_Height )( 
            IPDFImage __RPC_FAR * This,
            /* [retval][out] */ long __RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_ColorSpace )( 
            IPDFImage __RPC_FAR * This,
            /* [retval][out] */ IPDFColorSpace __RPC_FAR *__RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_BitsPerComponent )( 
            IPDFImage __RPC_FAR * This,
            /* [retval][out] */ short __RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_Samples )( 
            IPDFImage __RPC_FAR * This,
            /* [retval][out] */ VARIANT __RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_Mask )( 
            IPDFImage __RPC_FAR * This,
            /* [retval][out] */ VARIANT __RPC_FAR *pVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Store )( 
            IPDFImage __RPC_FAR * This,
            BSTR bstrPath,
            /* [optional] */ TPDFCompression iCompression,
            /* [retval][out] */ VARIANT_BOOL __RPC_FAR *pDone);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *StoreInMemory )( 
            IPDFImage __RPC_FAR * This,
            BSTR bstrExtension,
            /* [optional] */ TPDFCompression iCompression,
            /* [retval][out] */ VARIANT_BOOL __RPC_FAR *pDone);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetImage )( 
            IPDFImage __RPC_FAR * This,
            /* [retval][out] */ VARIANT __RPC_FAR *pVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *ConvertToRGB )( 
            IPDFImage __RPC_FAR * This,
            /* [retval][out] */ VARIANT_BOOL __RPC_FAR *pDone);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *ChangeOrientation )( 
            IPDFImage __RPC_FAR * This,
            /* [in] */ TPDFOrientation iOrientation,
            /* [retval][out] */ VARIANT_BOOL __RPC_FAR *pDone);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_Alternates )( 
            IPDFImage __RPC_FAR * This,
            /* [retval][out] */ VARIANT __RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_IsBitonal )( 
            IPDFImage __RPC_FAR * This,
            /* [retval][out] */ VARIANT_BOOL __RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_IsMonochrome )( 
            IPDFImage __RPC_FAR * This,
            /* [retval][out] */ VARIANT_BOOL __RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_IsColor )( 
            IPDFImage __RPC_FAR * This,
            /* [retval][out] */ VARIANT_BOOL __RPC_FAR *pVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetResolution )( 
            IPDFImage __RPC_FAR * This,
            IPDFTransformMatrix __RPC_FAR *pMatrix,
            /* [retval][out] */ float __RPC_FAR *pVal);
        
        END_INTERFACE
    } IPDFImageVtbl;

    interface IPDFImage
    {
        CONST_VTBL struct IPDFImageVtbl __RPC_FAR *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IPDFImage_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IPDFImage_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IPDFImage_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IPDFImage_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IPDFImage_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IPDFImage_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IPDFImage_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define IPDFImage_get_Width(This,pVal)	\
    (This)->lpVtbl -> get_Width(This,pVal)

#define IPDFImage_get_Height(This,pVal)	\
    (This)->lpVtbl -> get_Height(This,pVal)

#define IPDFImage_get_ColorSpace(This,pVal)	\
    (This)->lpVtbl -> get_ColorSpace(This,pVal)

#define IPDFImage_get_BitsPerComponent(This,pVal)	\
    (This)->lpVtbl -> get_BitsPerComponent(This,pVal)

#define IPDFImage_get_Samples(This,pVal)	\
    (This)->lpVtbl -> get_Samples(This,pVal)

#define IPDFImage_get_Mask(This,pVal)	\
    (This)->lpVtbl -> get_Mask(This,pVal)

#define IPDFImage_Store(This,bstrPath,iCompression,pDone)	\
    (This)->lpVtbl -> Store(This,bstrPath,iCompression,pDone)

#define IPDFImage_StoreInMemory(This,bstrExtension,iCompression,pDone)	\
    (This)->lpVtbl -> StoreInMemory(This,bstrExtension,iCompression,pDone)

#define IPDFImage_GetImage(This,pVal)	\
    (This)->lpVtbl -> GetImage(This,pVal)

#define IPDFImage_ConvertToRGB(This,pDone)	\
    (This)->lpVtbl -> ConvertToRGB(This,pDone)

#define IPDFImage_ChangeOrientation(This,iOrientation,pDone)	\
    (This)->lpVtbl -> ChangeOrientation(This,iOrientation,pDone)

#define IPDFImage_get_Alternates(This,pVal)	\
    (This)->lpVtbl -> get_Alternates(This,pVal)

#define IPDFImage_get_IsBitonal(This,pVal)	\
    (This)->lpVtbl -> get_IsBitonal(This,pVal)

#define IPDFImage_get_IsMonochrome(This,pVal)	\
    (This)->lpVtbl -> get_IsMonochrome(This,pVal)

#define IPDFImage_get_IsColor(This,pVal)	\
    (This)->lpVtbl -> get_IsColor(This,pVal)

#define IPDFImage_GetResolution(This,pMatrix,pVal)	\
    (This)->lpVtbl -> GetResolution(This,pMatrix,pVal)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFImage_get_Width_Proxy( 
    IPDFImage __RPC_FAR * This,
    /* [retval][out] */ long __RPC_FAR *pVal);


void __RPC_STUB IPDFImage_get_Width_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFImage_get_Height_Proxy( 
    IPDFImage __RPC_FAR * This,
    /* [retval][out] */ long __RPC_FAR *pVal);


void __RPC_STUB IPDFImage_get_Height_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFImage_get_ColorSpace_Proxy( 
    IPDFImage __RPC_FAR * This,
    /* [retval][out] */ IPDFColorSpace __RPC_FAR *__RPC_FAR *pVal);


void __RPC_STUB IPDFImage_get_ColorSpace_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFImage_get_BitsPerComponent_Proxy( 
    IPDFImage __RPC_FAR * This,
    /* [retval][out] */ short __RPC_FAR *pVal);


void __RPC_STUB IPDFImage_get_BitsPerComponent_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFImage_get_Samples_Proxy( 
    IPDFImage __RPC_FAR * This,
    /* [retval][out] */ VARIANT __RPC_FAR *pVal);


void __RPC_STUB IPDFImage_get_Samples_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFImage_get_Mask_Proxy( 
    IPDFImage __RPC_FAR * This,
    /* [retval][out] */ VARIANT __RPC_FAR *pVal);


void __RPC_STUB IPDFImage_get_Mask_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IPDFImage_Store_Proxy( 
    IPDFImage __RPC_FAR * This,
    BSTR bstrPath,
    /* [optional] */ TPDFCompression iCompression,
    /* [retval][out] */ VARIANT_BOOL __RPC_FAR *pDone);


void __RPC_STUB IPDFImage_Store_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IPDFImage_StoreInMemory_Proxy( 
    IPDFImage __RPC_FAR * This,
    BSTR bstrExtension,
    /* [optional] */ TPDFCompression iCompression,
    /* [retval][out] */ VARIANT_BOOL __RPC_FAR *pDone);


void __RPC_STUB IPDFImage_StoreInMemory_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IPDFImage_GetImage_Proxy( 
    IPDFImage __RPC_FAR * This,
    /* [retval][out] */ VARIANT __RPC_FAR *pVal);


void __RPC_STUB IPDFImage_GetImage_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IPDFImage_ConvertToRGB_Proxy( 
    IPDFImage __RPC_FAR * This,
    /* [retval][out] */ VARIANT_BOOL __RPC_FAR *pDone);


void __RPC_STUB IPDFImage_ConvertToRGB_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IPDFImage_ChangeOrientation_Proxy( 
    IPDFImage __RPC_FAR * This,
    /* [in] */ TPDFOrientation iOrientation,
    /* [retval][out] */ VARIANT_BOOL __RPC_FAR *pDone);


void __RPC_STUB IPDFImage_ChangeOrientation_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFImage_get_Alternates_Proxy( 
    IPDFImage __RPC_FAR * This,
    /* [retval][out] */ VARIANT __RPC_FAR *pVal);


void __RPC_STUB IPDFImage_get_Alternates_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFImage_get_IsBitonal_Proxy( 
    IPDFImage __RPC_FAR * This,
    /* [retval][out] */ VARIANT_BOOL __RPC_FAR *pVal);


void __RPC_STUB IPDFImage_get_IsBitonal_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFImage_get_IsMonochrome_Proxy( 
    IPDFImage __RPC_FAR * This,
    /* [retval][out] */ VARIANT_BOOL __RPC_FAR *pVal);


void __RPC_STUB IPDFImage_get_IsMonochrome_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFImage_get_IsColor_Proxy( 
    IPDFImage __RPC_FAR * This,
    /* [retval][out] */ VARIANT_BOOL __RPC_FAR *pVal);


void __RPC_STUB IPDFImage_get_IsColor_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IPDFImage_GetResolution_Proxy( 
    IPDFImage __RPC_FAR * This,
    IPDFTransformMatrix __RPC_FAR *pMatrix,
    /* [retval][out] */ float __RPC_FAR *pVal);


void __RPC_STUB IPDFImage_GetResolution_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IPDFImage_INTERFACE_DEFINED__ */


#ifndef __IPDFAlternateImage_INTERFACE_DEFINED__
#define __IPDFAlternateImage_INTERFACE_DEFINED__

/* interface IPDFAlternateImage */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_IPDFAlternateImage;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("62AE898E-B1C4-4DAE-828D-A0555BD90427")
    IPDFAlternateImage : public IDispatch
    {
    public:
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Image( 
            /* [retval][out] */ IPDFImage __RPC_FAR *__RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_DefaultForPrinting( 
            /* [retval][out] */ VARIANT_BOOL __RPC_FAR *pVal) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IPDFAlternateImageVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *QueryInterface )( 
            IPDFAlternateImage __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void __RPC_FAR *__RPC_FAR *ppvObject);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *AddRef )( 
            IPDFAlternateImage __RPC_FAR * This);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *Release )( 
            IPDFAlternateImage __RPC_FAR * This);
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetTypeInfoCount )( 
            IPDFAlternateImage __RPC_FAR * This,
            /* [out] */ UINT __RPC_FAR *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetTypeInfo )( 
            IPDFAlternateImage __RPC_FAR * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo __RPC_FAR *__RPC_FAR *ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetIDsOfNames )( 
            IPDFAlternateImage __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR __RPC_FAR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID __RPC_FAR *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Invoke )( 
            IPDFAlternateImage __RPC_FAR * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS __RPC_FAR *pDispParams,
            /* [out] */ VARIANT __RPC_FAR *pVarResult,
            /* [out] */ EXCEPINFO __RPC_FAR *pExcepInfo,
            /* [out] */ UINT __RPC_FAR *puArgErr);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_Image )( 
            IPDFAlternateImage __RPC_FAR * This,
            /* [retval][out] */ IPDFImage __RPC_FAR *__RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_DefaultForPrinting )( 
            IPDFAlternateImage __RPC_FAR * This,
            /* [retval][out] */ VARIANT_BOOL __RPC_FAR *pVal);
        
        END_INTERFACE
    } IPDFAlternateImageVtbl;

    interface IPDFAlternateImage
    {
        CONST_VTBL struct IPDFAlternateImageVtbl __RPC_FAR *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IPDFAlternateImage_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IPDFAlternateImage_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IPDFAlternateImage_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IPDFAlternateImage_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IPDFAlternateImage_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IPDFAlternateImage_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IPDFAlternateImage_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define IPDFAlternateImage_get_Image(This,pVal)	\
    (This)->lpVtbl -> get_Image(This,pVal)

#define IPDFAlternateImage_get_DefaultForPrinting(This,pVal)	\
    (This)->lpVtbl -> get_DefaultForPrinting(This,pVal)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFAlternateImage_get_Image_Proxy( 
    IPDFAlternateImage __RPC_FAR * This,
    /* [retval][out] */ IPDFImage __RPC_FAR *__RPC_FAR *pVal);


void __RPC_STUB IPDFAlternateImage_get_Image_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFAlternateImage_get_DefaultForPrinting_Proxy( 
    IPDFAlternateImage __RPC_FAR * This,
    /* [retval][out] */ VARIANT_BOOL __RPC_FAR *pVal);


void __RPC_STUB IPDFAlternateImage_get_DefaultForPrinting_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IPDFAlternateImage_INTERFACE_DEFINED__ */


#ifndef __IPDFDocument_INTERFACE_DEFINED__
#define __IPDFDocument_INTERFACE_DEFINED__

/* interface IPDFDocument */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_IPDFDocument;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("E46A1E80-977C-42DB-A770-65EF9D314B13")
    IPDFDocument : public IDispatch
    {
    public:
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_PageCount( 
            /* [retval][out] */ long __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_PageNo( 
            /* [retval][out] */ long __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_PageNo( 
            /* [in] */ long newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Page( 
            /* [retval][out] */ IPDFPage __RPC_FAR *__RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Open( 
            BSTR bstrPath,
            /* [optional] */ BSTR bstrPassword,
            /* [retval][out] */ VARIANT_BOOL __RPC_FAR *bDone) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Close( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE OpenMem( 
            VARIANT __RPC_FAR *varMem,
            /* [optional] */ BSTR bstrPassword,
            /* [retval][out] */ VARIANT_BOOL __RPC_FAR *bDone) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ErrorCode( 
            /* [retval][out] */ TPDFErrorCode __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_IsEncrypted( 
            /* [retval][out] */ VARIANT_BOOL __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_MajorVersion( 
            /* [retval][out] */ short __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_MinorVersion( 
            /* [retval][out] */ short __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Title( 
            /* [retval][out] */ BSTR __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Author( 
            /* [retval][out] */ BSTR __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Subject( 
            /* [retval][out] */ BSTR __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Keywords( 
            /* [retval][out] */ BSTR __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetInfoEntry( 
            /* [in] */ BSTR szKey,
            /* [retval][out] */ BSTR __RPC_FAR *szValue) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Creator( 
            /* [retval][out] */ BSTR __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Producer( 
            /* [retval][out] */ BSTR __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_CreationDate( 
            /* [retval][out] */ DATE __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ModDate( 
            /* [retval][out] */ DATE __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetFirstFontResource( 
            /* [retval][out] */ IPDFFont __RPC_FAR *__RPC_FAR *ppFont) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetNextFontResource( 
            /* [retval][out] */ IPDFFont __RPC_FAR *__RPC_FAR *ppFont) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetFirstImageResource( 
            /* [retval][out] */ IPDFImage __RPC_FAR *__RPC_FAR *ppImage) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetNextImageResource( 
            /* [retval][out] */ IPDFImage __RPC_FAR *__RPC_FAR *ppImage) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetFirstColorSpaceResource( 
            /* [retval][out] */ IPDFColorSpace __RPC_FAR *__RPC_FAR *ppColorSpace) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetNextColorSpaceResource( 
            /* [retval][out] */ IPDFColorSpace __RPC_FAR *__RPC_FAR *ppColorSpace) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetFirstOutlineItem( 
            /* [retval][out] */ IPDFOutlineItem __RPC_FAR *__RPC_FAR *ppOutlineItem) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetNextOutlineItem( 
            /* [defaultvalue][in] */ long nMaxLevel,
            /* [defaultvalue][in] */ VARIANT_BOOL bReturnOpenOnly,
            /* [retval][out] */ IPDFOutlineItem __RPC_FAR *__RPC_FAR *ppOutlineItem) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetCurrentOutlineLevel( 
            /* [retval][out] */ long __RPC_FAR *pVal) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IPDFDocumentVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *QueryInterface )( 
            IPDFDocument __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void __RPC_FAR *__RPC_FAR *ppvObject);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *AddRef )( 
            IPDFDocument __RPC_FAR * This);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *Release )( 
            IPDFDocument __RPC_FAR * This);
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetTypeInfoCount )( 
            IPDFDocument __RPC_FAR * This,
            /* [out] */ UINT __RPC_FAR *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetTypeInfo )( 
            IPDFDocument __RPC_FAR * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo __RPC_FAR *__RPC_FAR *ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetIDsOfNames )( 
            IPDFDocument __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR __RPC_FAR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID __RPC_FAR *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Invoke )( 
            IPDFDocument __RPC_FAR * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS __RPC_FAR *pDispParams,
            /* [out] */ VARIANT __RPC_FAR *pVarResult,
            /* [out] */ EXCEPINFO __RPC_FAR *pExcepInfo,
            /* [out] */ UINT __RPC_FAR *puArgErr);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_PageCount )( 
            IPDFDocument __RPC_FAR * This,
            /* [retval][out] */ long __RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_PageNo )( 
            IPDFDocument __RPC_FAR * This,
            /* [retval][out] */ long __RPC_FAR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *put_PageNo )( 
            IPDFDocument __RPC_FAR * This,
            /* [in] */ long newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_Page )( 
            IPDFDocument __RPC_FAR * This,
            /* [retval][out] */ IPDFPage __RPC_FAR *__RPC_FAR *pVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Open )( 
            IPDFDocument __RPC_FAR * This,
            BSTR bstrPath,
            /* [optional] */ BSTR bstrPassword,
            /* [retval][out] */ VARIANT_BOOL __RPC_FAR *bDone);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Close )( 
            IPDFDocument __RPC_FAR * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *OpenMem )( 
            IPDFDocument __RPC_FAR * This,
            VARIANT __RPC_FAR *varMem,
            /* [optional] */ BSTR bstrPassword,
            /* [retval][out] */ VARIANT_BOOL __RPC_FAR *bDone);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_ErrorCode )( 
            IPDFDocument __RPC_FAR * This,
            /* [retval][out] */ TPDFErrorCode __RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_IsEncrypted )( 
            IPDFDocument __RPC_FAR * This,
            /* [retval][out] */ VARIANT_BOOL __RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_MajorVersion )( 
            IPDFDocument __RPC_FAR * This,
            /* [retval][out] */ short __RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_MinorVersion )( 
            IPDFDocument __RPC_FAR * This,
            /* [retval][out] */ short __RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_Title )( 
            IPDFDocument __RPC_FAR * This,
            /* [retval][out] */ BSTR __RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_Author )( 
            IPDFDocument __RPC_FAR * This,
            /* [retval][out] */ BSTR __RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_Subject )( 
            IPDFDocument __RPC_FAR * This,
            /* [retval][out] */ BSTR __RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_Keywords )( 
            IPDFDocument __RPC_FAR * This,
            /* [retval][out] */ BSTR __RPC_FAR *pVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetInfoEntry )( 
            IPDFDocument __RPC_FAR * This,
            /* [in] */ BSTR szKey,
            /* [retval][out] */ BSTR __RPC_FAR *szValue);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_Creator )( 
            IPDFDocument __RPC_FAR * This,
            /* [retval][out] */ BSTR __RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_Producer )( 
            IPDFDocument __RPC_FAR * This,
            /* [retval][out] */ BSTR __RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_CreationDate )( 
            IPDFDocument __RPC_FAR * This,
            /* [retval][out] */ DATE __RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_ModDate )( 
            IPDFDocument __RPC_FAR * This,
            /* [retval][out] */ DATE __RPC_FAR *pVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetFirstFontResource )( 
            IPDFDocument __RPC_FAR * This,
            /* [retval][out] */ IPDFFont __RPC_FAR *__RPC_FAR *ppFont);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetNextFontResource )( 
            IPDFDocument __RPC_FAR * This,
            /* [retval][out] */ IPDFFont __RPC_FAR *__RPC_FAR *ppFont);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetFirstImageResource )( 
            IPDFDocument __RPC_FAR * This,
            /* [retval][out] */ IPDFImage __RPC_FAR *__RPC_FAR *ppImage);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetNextImageResource )( 
            IPDFDocument __RPC_FAR * This,
            /* [retval][out] */ IPDFImage __RPC_FAR *__RPC_FAR *ppImage);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetFirstColorSpaceResource )( 
            IPDFDocument __RPC_FAR * This,
            /* [retval][out] */ IPDFColorSpace __RPC_FAR *__RPC_FAR *ppColorSpace);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetNextColorSpaceResource )( 
            IPDFDocument __RPC_FAR * This,
            /* [retval][out] */ IPDFColorSpace __RPC_FAR *__RPC_FAR *ppColorSpace);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetFirstOutlineItem )( 
            IPDFDocument __RPC_FAR * This,
            /* [retval][out] */ IPDFOutlineItem __RPC_FAR *__RPC_FAR *ppOutlineItem);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetNextOutlineItem )( 
            IPDFDocument __RPC_FAR * This,
            /* [defaultvalue][in] */ long nMaxLevel,
            /* [defaultvalue][in] */ VARIANT_BOOL bReturnOpenOnly,
            /* [retval][out] */ IPDFOutlineItem __RPC_FAR *__RPC_FAR *ppOutlineItem);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetCurrentOutlineLevel )( 
            IPDFDocument __RPC_FAR * This,
            /* [retval][out] */ long __RPC_FAR *pVal);
        
        END_INTERFACE
    } IPDFDocumentVtbl;

    interface IPDFDocument
    {
        CONST_VTBL struct IPDFDocumentVtbl __RPC_FAR *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IPDFDocument_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IPDFDocument_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IPDFDocument_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IPDFDocument_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IPDFDocument_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IPDFDocument_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IPDFDocument_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define IPDFDocument_get_PageCount(This,pVal)	\
    (This)->lpVtbl -> get_PageCount(This,pVal)

#define IPDFDocument_get_PageNo(This,pVal)	\
    (This)->lpVtbl -> get_PageNo(This,pVal)

#define IPDFDocument_put_PageNo(This,newVal)	\
    (This)->lpVtbl -> put_PageNo(This,newVal)

#define IPDFDocument_get_Page(This,pVal)	\
    (This)->lpVtbl -> get_Page(This,pVal)

#define IPDFDocument_Open(This,bstrPath,bstrPassword,bDone)	\
    (This)->lpVtbl -> Open(This,bstrPath,bstrPassword,bDone)

#define IPDFDocument_Close(This)	\
    (This)->lpVtbl -> Close(This)

#define IPDFDocument_OpenMem(This,varMem,bstrPassword,bDone)	\
    (This)->lpVtbl -> OpenMem(This,varMem,bstrPassword,bDone)

#define IPDFDocument_get_ErrorCode(This,pVal)	\
    (This)->lpVtbl -> get_ErrorCode(This,pVal)

#define IPDFDocument_get_IsEncrypted(This,pVal)	\
    (This)->lpVtbl -> get_IsEncrypted(This,pVal)

#define IPDFDocument_get_MajorVersion(This,pVal)	\
    (This)->lpVtbl -> get_MajorVersion(This,pVal)

#define IPDFDocument_get_MinorVersion(This,pVal)	\
    (This)->lpVtbl -> get_MinorVersion(This,pVal)

#define IPDFDocument_get_Title(This,pVal)	\
    (This)->lpVtbl -> get_Title(This,pVal)

#define IPDFDocument_get_Author(This,pVal)	\
    (This)->lpVtbl -> get_Author(This,pVal)

#define IPDFDocument_get_Subject(This,pVal)	\
    (This)->lpVtbl -> get_Subject(This,pVal)

#define IPDFDocument_get_Keywords(This,pVal)	\
    (This)->lpVtbl -> get_Keywords(This,pVal)

#define IPDFDocument_GetInfoEntry(This,szKey,szValue)	\
    (This)->lpVtbl -> GetInfoEntry(This,szKey,szValue)

#define IPDFDocument_get_Creator(This,pVal)	\
    (This)->lpVtbl -> get_Creator(This,pVal)

#define IPDFDocument_get_Producer(This,pVal)	\
    (This)->lpVtbl -> get_Producer(This,pVal)

#define IPDFDocument_get_CreationDate(This,pVal)	\
    (This)->lpVtbl -> get_CreationDate(This,pVal)

#define IPDFDocument_get_ModDate(This,pVal)	\
    (This)->lpVtbl -> get_ModDate(This,pVal)

#define IPDFDocument_GetFirstFontResource(This,ppFont)	\
    (This)->lpVtbl -> GetFirstFontResource(This,ppFont)

#define IPDFDocument_GetNextFontResource(This,ppFont)	\
    (This)->lpVtbl -> GetNextFontResource(This,ppFont)

#define IPDFDocument_GetFirstImageResource(This,ppImage)	\
    (This)->lpVtbl -> GetFirstImageResource(This,ppImage)

#define IPDFDocument_GetNextImageResource(This,ppImage)	\
    (This)->lpVtbl -> GetNextImageResource(This,ppImage)

#define IPDFDocument_GetFirstColorSpaceResource(This,ppColorSpace)	\
    (This)->lpVtbl -> GetFirstColorSpaceResource(This,ppColorSpace)

#define IPDFDocument_GetNextColorSpaceResource(This,ppColorSpace)	\
    (This)->lpVtbl -> GetNextColorSpaceResource(This,ppColorSpace)

#define IPDFDocument_GetFirstOutlineItem(This,ppOutlineItem)	\
    (This)->lpVtbl -> GetFirstOutlineItem(This,ppOutlineItem)

#define IPDFDocument_GetNextOutlineItem(This,nMaxLevel,bReturnOpenOnly,ppOutlineItem)	\
    (This)->lpVtbl -> GetNextOutlineItem(This,nMaxLevel,bReturnOpenOnly,ppOutlineItem)

#define IPDFDocument_GetCurrentOutlineLevel(This,pVal)	\
    (This)->lpVtbl -> GetCurrentOutlineLevel(This,pVal)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFDocument_get_PageCount_Proxy( 
    IPDFDocument __RPC_FAR * This,
    /* [retval][out] */ long __RPC_FAR *pVal);


void __RPC_STUB IPDFDocument_get_PageCount_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFDocument_get_PageNo_Proxy( 
    IPDFDocument __RPC_FAR * This,
    /* [retval][out] */ long __RPC_FAR *pVal);


void __RPC_STUB IPDFDocument_get_PageNo_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IPDFDocument_put_PageNo_Proxy( 
    IPDFDocument __RPC_FAR * This,
    /* [in] */ long newVal);


void __RPC_STUB IPDFDocument_put_PageNo_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFDocument_get_Page_Proxy( 
    IPDFDocument __RPC_FAR * This,
    /* [retval][out] */ IPDFPage __RPC_FAR *__RPC_FAR *pVal);


void __RPC_STUB IPDFDocument_get_Page_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IPDFDocument_Open_Proxy( 
    IPDFDocument __RPC_FAR * This,
    BSTR bstrPath,
    /* [optional] */ BSTR bstrPassword,
    /* [retval][out] */ VARIANT_BOOL __RPC_FAR *bDone);


void __RPC_STUB IPDFDocument_Open_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IPDFDocument_Close_Proxy( 
    IPDFDocument __RPC_FAR * This);


void __RPC_STUB IPDFDocument_Close_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IPDFDocument_OpenMem_Proxy( 
    IPDFDocument __RPC_FAR * This,
    VARIANT __RPC_FAR *varMem,
    /* [optional] */ BSTR bstrPassword,
    /* [retval][out] */ VARIANT_BOOL __RPC_FAR *bDone);


void __RPC_STUB IPDFDocument_OpenMem_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFDocument_get_ErrorCode_Proxy( 
    IPDFDocument __RPC_FAR * This,
    /* [retval][out] */ TPDFErrorCode __RPC_FAR *pVal);


void __RPC_STUB IPDFDocument_get_ErrorCode_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFDocument_get_IsEncrypted_Proxy( 
    IPDFDocument __RPC_FAR * This,
    /* [retval][out] */ VARIANT_BOOL __RPC_FAR *pVal);


void __RPC_STUB IPDFDocument_get_IsEncrypted_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFDocument_get_MajorVersion_Proxy( 
    IPDFDocument __RPC_FAR * This,
    /* [retval][out] */ short __RPC_FAR *pVal);


void __RPC_STUB IPDFDocument_get_MajorVersion_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFDocument_get_MinorVersion_Proxy( 
    IPDFDocument __RPC_FAR * This,
    /* [retval][out] */ short __RPC_FAR *pVal);


void __RPC_STUB IPDFDocument_get_MinorVersion_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFDocument_get_Title_Proxy( 
    IPDFDocument __RPC_FAR * This,
    /* [retval][out] */ BSTR __RPC_FAR *pVal);


void __RPC_STUB IPDFDocument_get_Title_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFDocument_get_Author_Proxy( 
    IPDFDocument __RPC_FAR * This,
    /* [retval][out] */ BSTR __RPC_FAR *pVal);


void __RPC_STUB IPDFDocument_get_Author_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFDocument_get_Subject_Proxy( 
    IPDFDocument __RPC_FAR * This,
    /* [retval][out] */ BSTR __RPC_FAR *pVal);


void __RPC_STUB IPDFDocument_get_Subject_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFDocument_get_Keywords_Proxy( 
    IPDFDocument __RPC_FAR * This,
    /* [retval][out] */ BSTR __RPC_FAR *pVal);


void __RPC_STUB IPDFDocument_get_Keywords_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IPDFDocument_GetInfoEntry_Proxy( 
    IPDFDocument __RPC_FAR * This,
    /* [in] */ BSTR szKey,
    /* [retval][out] */ BSTR __RPC_FAR *szValue);


void __RPC_STUB IPDFDocument_GetInfoEntry_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFDocument_get_Creator_Proxy( 
    IPDFDocument __RPC_FAR * This,
    /* [retval][out] */ BSTR __RPC_FAR *pVal);


void __RPC_STUB IPDFDocument_get_Creator_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFDocument_get_Producer_Proxy( 
    IPDFDocument __RPC_FAR * This,
    /* [retval][out] */ BSTR __RPC_FAR *pVal);


void __RPC_STUB IPDFDocument_get_Producer_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFDocument_get_CreationDate_Proxy( 
    IPDFDocument __RPC_FAR * This,
    /* [retval][out] */ DATE __RPC_FAR *pVal);


void __RPC_STUB IPDFDocument_get_CreationDate_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFDocument_get_ModDate_Proxy( 
    IPDFDocument __RPC_FAR * This,
    /* [retval][out] */ DATE __RPC_FAR *pVal);


void __RPC_STUB IPDFDocument_get_ModDate_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IPDFDocument_GetFirstFontResource_Proxy( 
    IPDFDocument __RPC_FAR * This,
    /* [retval][out] */ IPDFFont __RPC_FAR *__RPC_FAR *ppFont);


void __RPC_STUB IPDFDocument_GetFirstFontResource_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IPDFDocument_GetNextFontResource_Proxy( 
    IPDFDocument __RPC_FAR * This,
    /* [retval][out] */ IPDFFont __RPC_FAR *__RPC_FAR *ppFont);


void __RPC_STUB IPDFDocument_GetNextFontResource_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IPDFDocument_GetFirstImageResource_Proxy( 
    IPDFDocument __RPC_FAR * This,
    /* [retval][out] */ IPDFImage __RPC_FAR *__RPC_FAR *ppImage);


void __RPC_STUB IPDFDocument_GetFirstImageResource_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IPDFDocument_GetNextImageResource_Proxy( 
    IPDFDocument __RPC_FAR * This,
    /* [retval][out] */ IPDFImage __RPC_FAR *__RPC_FAR *ppImage);


void __RPC_STUB IPDFDocument_GetNextImageResource_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IPDFDocument_GetFirstColorSpaceResource_Proxy( 
    IPDFDocument __RPC_FAR * This,
    /* [retval][out] */ IPDFColorSpace __RPC_FAR *__RPC_FAR *ppColorSpace);


void __RPC_STUB IPDFDocument_GetFirstColorSpaceResource_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IPDFDocument_GetNextColorSpaceResource_Proxy( 
    IPDFDocument __RPC_FAR * This,
    /* [retval][out] */ IPDFColorSpace __RPC_FAR *__RPC_FAR *ppColorSpace);


void __RPC_STUB IPDFDocument_GetNextColorSpaceResource_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IPDFDocument_GetFirstOutlineItem_Proxy( 
    IPDFDocument __RPC_FAR * This,
    /* [retval][out] */ IPDFOutlineItem __RPC_FAR *__RPC_FAR *ppOutlineItem);


void __RPC_STUB IPDFDocument_GetFirstOutlineItem_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IPDFDocument_GetNextOutlineItem_Proxy( 
    IPDFDocument __RPC_FAR * This,
    /* [defaultvalue][in] */ long nMaxLevel,
    /* [defaultvalue][in] */ VARIANT_BOOL bReturnOpenOnly,
    /* [retval][out] */ IPDFOutlineItem __RPC_FAR *__RPC_FAR *ppOutlineItem);


void __RPC_STUB IPDFDocument_GetNextOutlineItem_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IPDFDocument_GetCurrentOutlineLevel_Proxy( 
    IPDFDocument __RPC_FAR * This,
    /* [retval][out] */ long __RPC_FAR *pVal);


void __RPC_STUB IPDFDocument_GetCurrentOutlineLevel_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IPDFDocument_INTERFACE_DEFINED__ */


#ifndef __IPDFContent_INTERFACE_DEFINED__
#define __IPDFContent_INTERFACE_DEFINED__

/* interface IPDFContent */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_IPDFContent;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("A4D73C27-0670-462B-A91D-00A5FAD884E0")
    IPDFContent : public IDispatch
    {
    public:
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_GraphicsState( 
            /* [retval][out] */ IPDFGraphicsState __RPC_FAR *__RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Text( 
            /* [retval][out] */ IPDFText __RPC_FAR *__RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Image( 
            /* [retval][out] */ IPDFImage __RPC_FAR *__RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Path( 
            /* [retval][out] */ BSTR __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_BreakWords( 
            /* [retval][out] */ VARIANT_BOOL __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_BreakWords( 
            /* [in] */ VARIANT_BOOL newVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Reset( 
            /* [defaultvalue][in] */ VARIANT_BOOL bIncludeRotate = FALSE) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetNextText( 
            /* [retval][out] */ IPDFText __RPC_FAR *__RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetNextImage( 
            /* [retval][out] */ IPDFImage __RPC_FAR *__RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetNextPath( 
            /* [retval][out] */ BSTR __RPC_FAR *bstrPath) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetNextObject( 
            /* [retval][out] */ TPDFContentObject __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Flags( 
            /* [retval][out] */ long __RPC_FAR *pVal) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IPDFContentVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *QueryInterface )( 
            IPDFContent __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void __RPC_FAR *__RPC_FAR *ppvObject);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *AddRef )( 
            IPDFContent __RPC_FAR * This);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *Release )( 
            IPDFContent __RPC_FAR * This);
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetTypeInfoCount )( 
            IPDFContent __RPC_FAR * This,
            /* [out] */ UINT __RPC_FAR *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetTypeInfo )( 
            IPDFContent __RPC_FAR * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo __RPC_FAR *__RPC_FAR *ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetIDsOfNames )( 
            IPDFContent __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR __RPC_FAR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID __RPC_FAR *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Invoke )( 
            IPDFContent __RPC_FAR * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS __RPC_FAR *pDispParams,
            /* [out] */ VARIANT __RPC_FAR *pVarResult,
            /* [out] */ EXCEPINFO __RPC_FAR *pExcepInfo,
            /* [out] */ UINT __RPC_FAR *puArgErr);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_GraphicsState )( 
            IPDFContent __RPC_FAR * This,
            /* [retval][out] */ IPDFGraphicsState __RPC_FAR *__RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_Text )( 
            IPDFContent __RPC_FAR * This,
            /* [retval][out] */ IPDFText __RPC_FAR *__RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_Image )( 
            IPDFContent __RPC_FAR * This,
            /* [retval][out] */ IPDFImage __RPC_FAR *__RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_Path )( 
            IPDFContent __RPC_FAR * This,
            /* [retval][out] */ BSTR __RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_BreakWords )( 
            IPDFContent __RPC_FAR * This,
            /* [retval][out] */ VARIANT_BOOL __RPC_FAR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *put_BreakWords )( 
            IPDFContent __RPC_FAR * This,
            /* [in] */ VARIANT_BOOL newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Reset )( 
            IPDFContent __RPC_FAR * This,
            /* [defaultvalue][in] */ VARIANT_BOOL bIncludeRotate);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetNextText )( 
            IPDFContent __RPC_FAR * This,
            /* [retval][out] */ IPDFText __RPC_FAR *__RPC_FAR *pVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetNextImage )( 
            IPDFContent __RPC_FAR * This,
            /* [retval][out] */ IPDFImage __RPC_FAR *__RPC_FAR *pVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetNextPath )( 
            IPDFContent __RPC_FAR * This,
            /* [retval][out] */ BSTR __RPC_FAR *bstrPath);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetNextObject )( 
            IPDFContent __RPC_FAR * This,
            /* [retval][out] */ TPDFContentObject __RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_Flags )( 
            IPDFContent __RPC_FAR * This,
            /* [retval][out] */ long __RPC_FAR *pVal);
        
        END_INTERFACE
    } IPDFContentVtbl;

    interface IPDFContent
    {
        CONST_VTBL struct IPDFContentVtbl __RPC_FAR *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IPDFContent_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IPDFContent_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IPDFContent_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IPDFContent_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IPDFContent_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IPDFContent_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IPDFContent_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define IPDFContent_get_GraphicsState(This,pVal)	\
    (This)->lpVtbl -> get_GraphicsState(This,pVal)

#define IPDFContent_get_Text(This,pVal)	\
    (This)->lpVtbl -> get_Text(This,pVal)

#define IPDFContent_get_Image(This,pVal)	\
    (This)->lpVtbl -> get_Image(This,pVal)

#define IPDFContent_get_Path(This,pVal)	\
    (This)->lpVtbl -> get_Path(This,pVal)

#define IPDFContent_get_BreakWords(This,pVal)	\
    (This)->lpVtbl -> get_BreakWords(This,pVal)

#define IPDFContent_put_BreakWords(This,newVal)	\
    (This)->lpVtbl -> put_BreakWords(This,newVal)

#define IPDFContent_Reset(This,bIncludeRotate)	\
    (This)->lpVtbl -> Reset(This,bIncludeRotate)

#define IPDFContent_GetNextText(This,pVal)	\
    (This)->lpVtbl -> GetNextText(This,pVal)

#define IPDFContent_GetNextImage(This,pVal)	\
    (This)->lpVtbl -> GetNextImage(This,pVal)

#define IPDFContent_GetNextPath(This,bstrPath)	\
    (This)->lpVtbl -> GetNextPath(This,bstrPath)

#define IPDFContent_GetNextObject(This,pVal)	\
    (This)->lpVtbl -> GetNextObject(This,pVal)

#define IPDFContent_get_Flags(This,pVal)	\
    (This)->lpVtbl -> get_Flags(This,pVal)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFContent_get_GraphicsState_Proxy( 
    IPDFContent __RPC_FAR * This,
    /* [retval][out] */ IPDFGraphicsState __RPC_FAR *__RPC_FAR *pVal);


void __RPC_STUB IPDFContent_get_GraphicsState_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFContent_get_Text_Proxy( 
    IPDFContent __RPC_FAR * This,
    /* [retval][out] */ IPDFText __RPC_FAR *__RPC_FAR *pVal);


void __RPC_STUB IPDFContent_get_Text_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFContent_get_Image_Proxy( 
    IPDFContent __RPC_FAR * This,
    /* [retval][out] */ IPDFImage __RPC_FAR *__RPC_FAR *pVal);


void __RPC_STUB IPDFContent_get_Image_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFContent_get_Path_Proxy( 
    IPDFContent __RPC_FAR * This,
    /* [retval][out] */ BSTR __RPC_FAR *pVal);


void __RPC_STUB IPDFContent_get_Path_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFContent_get_BreakWords_Proxy( 
    IPDFContent __RPC_FAR * This,
    /* [retval][out] */ VARIANT_BOOL __RPC_FAR *pVal);


void __RPC_STUB IPDFContent_get_BreakWords_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IPDFContent_put_BreakWords_Proxy( 
    IPDFContent __RPC_FAR * This,
    /* [in] */ VARIANT_BOOL newVal);


void __RPC_STUB IPDFContent_put_BreakWords_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IPDFContent_Reset_Proxy( 
    IPDFContent __RPC_FAR * This,
    /* [defaultvalue][in] */ VARIANT_BOOL bIncludeRotate);


void __RPC_STUB IPDFContent_Reset_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IPDFContent_GetNextText_Proxy( 
    IPDFContent __RPC_FAR * This,
    /* [retval][out] */ IPDFText __RPC_FAR *__RPC_FAR *pVal);


void __RPC_STUB IPDFContent_GetNextText_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IPDFContent_GetNextImage_Proxy( 
    IPDFContent __RPC_FAR * This,
    /* [retval][out] */ IPDFImage __RPC_FAR *__RPC_FAR *pVal);


void __RPC_STUB IPDFContent_GetNextImage_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IPDFContent_GetNextPath_Proxy( 
    IPDFContent __RPC_FAR * This,
    /* [retval][out] */ BSTR __RPC_FAR *bstrPath);


void __RPC_STUB IPDFContent_GetNextPath_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IPDFContent_GetNextObject_Proxy( 
    IPDFContent __RPC_FAR * This,
    /* [retval][out] */ TPDFContentObject __RPC_FAR *pVal);


void __RPC_STUB IPDFContent_GetNextObject_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFContent_get_Flags_Proxy( 
    IPDFContent __RPC_FAR * This,
    /* [retval][out] */ long __RPC_FAR *pVal);


void __RPC_STUB IPDFContent_get_Flags_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IPDFContent_INTERFACE_DEFINED__ */


#ifndef __IPDFAnnotation_INTERFACE_DEFINED__
#define __IPDFAnnotation_INTERFACE_DEFINED__

/* interface IPDFAnnotation */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_IPDFAnnotation;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("5FBE061F-1929-4213-8350-0A73FE885353")
    IPDFAnnotation : public IDispatch
    {
    public:
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Subtype( 
            /* [retval][out] */ BSTR __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Rect( 
            /* [retval][out] */ VARIANT __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Contents( 
            /* [retval][out] */ BSTR __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Name( 
            /* [retval][out] */ BSTR __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Date( 
            /* [retval][out] */ DATE __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Flags( 
            /* [retval][out] */ long __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Color( 
            /* [retval][out] */ long __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_TextLabel( 
            /* [retval][out] */ BSTR __RPC_FAR *pVal) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IPDFAnnotationVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *QueryInterface )( 
            IPDFAnnotation __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void __RPC_FAR *__RPC_FAR *ppvObject);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *AddRef )( 
            IPDFAnnotation __RPC_FAR * This);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *Release )( 
            IPDFAnnotation __RPC_FAR * This);
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetTypeInfoCount )( 
            IPDFAnnotation __RPC_FAR * This,
            /* [out] */ UINT __RPC_FAR *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetTypeInfo )( 
            IPDFAnnotation __RPC_FAR * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo __RPC_FAR *__RPC_FAR *ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetIDsOfNames )( 
            IPDFAnnotation __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR __RPC_FAR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID __RPC_FAR *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Invoke )( 
            IPDFAnnotation __RPC_FAR * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS __RPC_FAR *pDispParams,
            /* [out] */ VARIANT __RPC_FAR *pVarResult,
            /* [out] */ EXCEPINFO __RPC_FAR *pExcepInfo,
            /* [out] */ UINT __RPC_FAR *puArgErr);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_Subtype )( 
            IPDFAnnotation __RPC_FAR * This,
            /* [retval][out] */ BSTR __RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_Rect )( 
            IPDFAnnotation __RPC_FAR * This,
            /* [retval][out] */ VARIANT __RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_Contents )( 
            IPDFAnnotation __RPC_FAR * This,
            /* [retval][out] */ BSTR __RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_Name )( 
            IPDFAnnotation __RPC_FAR * This,
            /* [retval][out] */ BSTR __RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_Date )( 
            IPDFAnnotation __RPC_FAR * This,
            /* [retval][out] */ DATE __RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_Flags )( 
            IPDFAnnotation __RPC_FAR * This,
            /* [retval][out] */ long __RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_Color )( 
            IPDFAnnotation __RPC_FAR * This,
            /* [retval][out] */ long __RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_TextLabel )( 
            IPDFAnnotation __RPC_FAR * This,
            /* [retval][out] */ BSTR __RPC_FAR *pVal);
        
        END_INTERFACE
    } IPDFAnnotationVtbl;

    interface IPDFAnnotation
    {
        CONST_VTBL struct IPDFAnnotationVtbl __RPC_FAR *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IPDFAnnotation_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IPDFAnnotation_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IPDFAnnotation_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IPDFAnnotation_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IPDFAnnotation_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IPDFAnnotation_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IPDFAnnotation_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define IPDFAnnotation_get_Subtype(This,pVal)	\
    (This)->lpVtbl -> get_Subtype(This,pVal)

#define IPDFAnnotation_get_Rect(This,pVal)	\
    (This)->lpVtbl -> get_Rect(This,pVal)

#define IPDFAnnotation_get_Contents(This,pVal)	\
    (This)->lpVtbl -> get_Contents(This,pVal)

#define IPDFAnnotation_get_Name(This,pVal)	\
    (This)->lpVtbl -> get_Name(This,pVal)

#define IPDFAnnotation_get_Date(This,pVal)	\
    (This)->lpVtbl -> get_Date(This,pVal)

#define IPDFAnnotation_get_Flags(This,pVal)	\
    (This)->lpVtbl -> get_Flags(This,pVal)

#define IPDFAnnotation_get_Color(This,pVal)	\
    (This)->lpVtbl -> get_Color(This,pVal)

#define IPDFAnnotation_get_TextLabel(This,pVal)	\
    (This)->lpVtbl -> get_TextLabel(This,pVal)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFAnnotation_get_Subtype_Proxy( 
    IPDFAnnotation __RPC_FAR * This,
    /* [retval][out] */ BSTR __RPC_FAR *pVal);


void __RPC_STUB IPDFAnnotation_get_Subtype_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFAnnotation_get_Rect_Proxy( 
    IPDFAnnotation __RPC_FAR * This,
    /* [retval][out] */ VARIANT __RPC_FAR *pVal);


void __RPC_STUB IPDFAnnotation_get_Rect_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFAnnotation_get_Contents_Proxy( 
    IPDFAnnotation __RPC_FAR * This,
    /* [retval][out] */ BSTR __RPC_FAR *pVal);


void __RPC_STUB IPDFAnnotation_get_Contents_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFAnnotation_get_Name_Proxy( 
    IPDFAnnotation __RPC_FAR * This,
    /* [retval][out] */ BSTR __RPC_FAR *pVal);


void __RPC_STUB IPDFAnnotation_get_Name_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFAnnotation_get_Date_Proxy( 
    IPDFAnnotation __RPC_FAR * This,
    /* [retval][out] */ DATE __RPC_FAR *pVal);


void __RPC_STUB IPDFAnnotation_get_Date_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFAnnotation_get_Flags_Proxy( 
    IPDFAnnotation __RPC_FAR * This,
    /* [retval][out] */ long __RPC_FAR *pVal);


void __RPC_STUB IPDFAnnotation_get_Flags_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFAnnotation_get_Color_Proxy( 
    IPDFAnnotation __RPC_FAR * This,
    /* [retval][out] */ long __RPC_FAR *pVal);


void __RPC_STUB IPDFAnnotation_get_Color_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFAnnotation_get_TextLabel_Proxy( 
    IPDFAnnotation __RPC_FAR * This,
    /* [retval][out] */ BSTR __RPC_FAR *pVal);


void __RPC_STUB IPDFAnnotation_get_TextLabel_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IPDFAnnotation_INTERFACE_DEFINED__ */


#ifndef __IPDFPage_INTERFACE_DEFINED__
#define __IPDFPage_INTERFACE_DEFINED__

/* interface IPDFPage */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_IPDFPage;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("FABFF1BB-3852-4BE4-8344-BAB92105D5A0")
    IPDFPage : public IDispatch
    {
    public:
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Document( 
            /* [retval][out] */ IPDFDocument __RPC_FAR *__RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_MediaBox( 
            /* [retval][out] */ VARIANT __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_CropBox( 
            /* [retval][out] */ VARIANT __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Rotate( 
            /* [retval][out] */ short __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Content( 
            /* [retval][out] */ IPDFContent __RPC_FAR *__RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetFirstAnnotation( 
            /* [retval][out] */ IPDFAnnotation __RPC_FAR *__RPC_FAR *pAnnot) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetNextAnnotation( 
            /* [retval][out] */ IPDFAnnotation __RPC_FAR *__RPC_FAR *pAnnot) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_BleedBox( 
            /* [retval][out] */ VARIANT __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_TrimBox( 
            /* [retval][out] */ VARIANT __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ArtBox( 
            /* [retval][out] */ VARIANT __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_DeviceColorant( 
            /* [retval][out] */ BSTR __RPC_FAR *pVal) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IPDFPageVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *QueryInterface )( 
            IPDFPage __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void __RPC_FAR *__RPC_FAR *ppvObject);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *AddRef )( 
            IPDFPage __RPC_FAR * This);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *Release )( 
            IPDFPage __RPC_FAR * This);
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetTypeInfoCount )( 
            IPDFPage __RPC_FAR * This,
            /* [out] */ UINT __RPC_FAR *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetTypeInfo )( 
            IPDFPage __RPC_FAR * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo __RPC_FAR *__RPC_FAR *ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetIDsOfNames )( 
            IPDFPage __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR __RPC_FAR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID __RPC_FAR *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Invoke )( 
            IPDFPage __RPC_FAR * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS __RPC_FAR *pDispParams,
            /* [out] */ VARIANT __RPC_FAR *pVarResult,
            /* [out] */ EXCEPINFO __RPC_FAR *pExcepInfo,
            /* [out] */ UINT __RPC_FAR *puArgErr);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_Document )( 
            IPDFPage __RPC_FAR * This,
            /* [retval][out] */ IPDFDocument __RPC_FAR *__RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_MediaBox )( 
            IPDFPage __RPC_FAR * This,
            /* [retval][out] */ VARIANT __RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_CropBox )( 
            IPDFPage __RPC_FAR * This,
            /* [retval][out] */ VARIANT __RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_Rotate )( 
            IPDFPage __RPC_FAR * This,
            /* [retval][out] */ short __RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_Content )( 
            IPDFPage __RPC_FAR * This,
            /* [retval][out] */ IPDFContent __RPC_FAR *__RPC_FAR *pVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetFirstAnnotation )( 
            IPDFPage __RPC_FAR * This,
            /* [retval][out] */ IPDFAnnotation __RPC_FAR *__RPC_FAR *pAnnot);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetNextAnnotation )( 
            IPDFPage __RPC_FAR * This,
            /* [retval][out] */ IPDFAnnotation __RPC_FAR *__RPC_FAR *pAnnot);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_BleedBox )( 
            IPDFPage __RPC_FAR * This,
            /* [retval][out] */ VARIANT __RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_TrimBox )( 
            IPDFPage __RPC_FAR * This,
            /* [retval][out] */ VARIANT __RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_ArtBox )( 
            IPDFPage __RPC_FAR * This,
            /* [retval][out] */ VARIANT __RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_DeviceColorant )( 
            IPDFPage __RPC_FAR * This,
            /* [retval][out] */ BSTR __RPC_FAR *pVal);
        
        END_INTERFACE
    } IPDFPageVtbl;

    interface IPDFPage
    {
        CONST_VTBL struct IPDFPageVtbl __RPC_FAR *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IPDFPage_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IPDFPage_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IPDFPage_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IPDFPage_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IPDFPage_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IPDFPage_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IPDFPage_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define IPDFPage_get_Document(This,pVal)	\
    (This)->lpVtbl -> get_Document(This,pVal)

#define IPDFPage_get_MediaBox(This,pVal)	\
    (This)->lpVtbl -> get_MediaBox(This,pVal)

#define IPDFPage_get_CropBox(This,pVal)	\
    (This)->lpVtbl -> get_CropBox(This,pVal)

#define IPDFPage_get_Rotate(This,pVal)	\
    (This)->lpVtbl -> get_Rotate(This,pVal)

#define IPDFPage_get_Content(This,pVal)	\
    (This)->lpVtbl -> get_Content(This,pVal)

#define IPDFPage_GetFirstAnnotation(This,pAnnot)	\
    (This)->lpVtbl -> GetFirstAnnotation(This,pAnnot)

#define IPDFPage_GetNextAnnotation(This,pAnnot)	\
    (This)->lpVtbl -> GetNextAnnotation(This,pAnnot)

#define IPDFPage_get_BleedBox(This,pVal)	\
    (This)->lpVtbl -> get_BleedBox(This,pVal)

#define IPDFPage_get_TrimBox(This,pVal)	\
    (This)->lpVtbl -> get_TrimBox(This,pVal)

#define IPDFPage_get_ArtBox(This,pVal)	\
    (This)->lpVtbl -> get_ArtBox(This,pVal)

#define IPDFPage_get_DeviceColorant(This,pVal)	\
    (This)->lpVtbl -> get_DeviceColorant(This,pVal)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFPage_get_Document_Proxy( 
    IPDFPage __RPC_FAR * This,
    /* [retval][out] */ IPDFDocument __RPC_FAR *__RPC_FAR *pVal);


void __RPC_STUB IPDFPage_get_Document_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFPage_get_MediaBox_Proxy( 
    IPDFPage __RPC_FAR * This,
    /* [retval][out] */ VARIANT __RPC_FAR *pVal);


void __RPC_STUB IPDFPage_get_MediaBox_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFPage_get_CropBox_Proxy( 
    IPDFPage __RPC_FAR * This,
    /* [retval][out] */ VARIANT __RPC_FAR *pVal);


void __RPC_STUB IPDFPage_get_CropBox_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFPage_get_Rotate_Proxy( 
    IPDFPage __RPC_FAR * This,
    /* [retval][out] */ short __RPC_FAR *pVal);


void __RPC_STUB IPDFPage_get_Rotate_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFPage_get_Content_Proxy( 
    IPDFPage __RPC_FAR * This,
    /* [retval][out] */ IPDFContent __RPC_FAR *__RPC_FAR *pVal);


void __RPC_STUB IPDFPage_get_Content_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IPDFPage_GetFirstAnnotation_Proxy( 
    IPDFPage __RPC_FAR * This,
    /* [retval][out] */ IPDFAnnotation __RPC_FAR *__RPC_FAR *pAnnot);


void __RPC_STUB IPDFPage_GetFirstAnnotation_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IPDFPage_GetNextAnnotation_Proxy( 
    IPDFPage __RPC_FAR * This,
    /* [retval][out] */ IPDFAnnotation __RPC_FAR *__RPC_FAR *pAnnot);


void __RPC_STUB IPDFPage_GetNextAnnotation_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFPage_get_BleedBox_Proxy( 
    IPDFPage __RPC_FAR * This,
    /* [retval][out] */ VARIANT __RPC_FAR *pVal);


void __RPC_STUB IPDFPage_get_BleedBox_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFPage_get_TrimBox_Proxy( 
    IPDFPage __RPC_FAR * This,
    /* [retval][out] */ VARIANT __RPC_FAR *pVal);


void __RPC_STUB IPDFPage_get_TrimBox_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFPage_get_ArtBox_Proxy( 
    IPDFPage __RPC_FAR * This,
    /* [retval][out] */ VARIANT __RPC_FAR *pVal);


void __RPC_STUB IPDFPage_get_ArtBox_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFPage_get_DeviceColorant_Proxy( 
    IPDFPage __RPC_FAR * This,
    /* [retval][out] */ BSTR __RPC_FAR *pVal);


void __RPC_STUB IPDFPage_get_DeviceColorant_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IPDFPage_INTERFACE_DEFINED__ */


#ifndef __IPDFDestination_INTERFACE_DEFINED__
#define __IPDFDestination_INTERFACE_DEFINED__

/* interface IPDFDestination */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_IPDFDestination;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("405CA7A9-10C7-40CD-AAA1-22EFF01E2CE5")
    IPDFDestination : public IDispatch
    {
    public:
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Type( 
            /* [retval][out] */ BSTR __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_PageNo( 
            /* [retval][out] */ long __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Left( 
            /* [retval][out] */ float __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Bottom( 
            /* [retval][out] */ float __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Right( 
            /* [retval][out] */ float __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Top( 
            /* [retval][out] */ float __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Zoom( 
            /* [retval][out] */ float __RPC_FAR *pVal) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IPDFDestinationVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *QueryInterface )( 
            IPDFDestination __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void __RPC_FAR *__RPC_FAR *ppvObject);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *AddRef )( 
            IPDFDestination __RPC_FAR * This);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *Release )( 
            IPDFDestination __RPC_FAR * This);
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetTypeInfoCount )( 
            IPDFDestination __RPC_FAR * This,
            /* [out] */ UINT __RPC_FAR *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetTypeInfo )( 
            IPDFDestination __RPC_FAR * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo __RPC_FAR *__RPC_FAR *ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetIDsOfNames )( 
            IPDFDestination __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR __RPC_FAR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID __RPC_FAR *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Invoke )( 
            IPDFDestination __RPC_FAR * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS __RPC_FAR *pDispParams,
            /* [out] */ VARIANT __RPC_FAR *pVarResult,
            /* [out] */ EXCEPINFO __RPC_FAR *pExcepInfo,
            /* [out] */ UINT __RPC_FAR *puArgErr);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_Type )( 
            IPDFDestination __RPC_FAR * This,
            /* [retval][out] */ BSTR __RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_PageNo )( 
            IPDFDestination __RPC_FAR * This,
            /* [retval][out] */ long __RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_Left )( 
            IPDFDestination __RPC_FAR * This,
            /* [retval][out] */ float __RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_Bottom )( 
            IPDFDestination __RPC_FAR * This,
            /* [retval][out] */ float __RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_Right )( 
            IPDFDestination __RPC_FAR * This,
            /* [retval][out] */ float __RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_Top )( 
            IPDFDestination __RPC_FAR * This,
            /* [retval][out] */ float __RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_Zoom )( 
            IPDFDestination __RPC_FAR * This,
            /* [retval][out] */ float __RPC_FAR *pVal);
        
        END_INTERFACE
    } IPDFDestinationVtbl;

    interface IPDFDestination
    {
        CONST_VTBL struct IPDFDestinationVtbl __RPC_FAR *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IPDFDestination_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IPDFDestination_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IPDFDestination_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IPDFDestination_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IPDFDestination_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IPDFDestination_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IPDFDestination_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define IPDFDestination_get_Type(This,pVal)	\
    (This)->lpVtbl -> get_Type(This,pVal)

#define IPDFDestination_get_PageNo(This,pVal)	\
    (This)->lpVtbl -> get_PageNo(This,pVal)

#define IPDFDestination_get_Left(This,pVal)	\
    (This)->lpVtbl -> get_Left(This,pVal)

#define IPDFDestination_get_Bottom(This,pVal)	\
    (This)->lpVtbl -> get_Bottom(This,pVal)

#define IPDFDestination_get_Right(This,pVal)	\
    (This)->lpVtbl -> get_Right(This,pVal)

#define IPDFDestination_get_Top(This,pVal)	\
    (This)->lpVtbl -> get_Top(This,pVal)

#define IPDFDestination_get_Zoom(This,pVal)	\
    (This)->lpVtbl -> get_Zoom(This,pVal)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFDestination_get_Type_Proxy( 
    IPDFDestination __RPC_FAR * This,
    /* [retval][out] */ BSTR __RPC_FAR *pVal);


void __RPC_STUB IPDFDestination_get_Type_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFDestination_get_PageNo_Proxy( 
    IPDFDestination __RPC_FAR * This,
    /* [retval][out] */ long __RPC_FAR *pVal);


void __RPC_STUB IPDFDestination_get_PageNo_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFDestination_get_Left_Proxy( 
    IPDFDestination __RPC_FAR * This,
    /* [retval][out] */ float __RPC_FAR *pVal);


void __RPC_STUB IPDFDestination_get_Left_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFDestination_get_Bottom_Proxy( 
    IPDFDestination __RPC_FAR * This,
    /* [retval][out] */ float __RPC_FAR *pVal);


void __RPC_STUB IPDFDestination_get_Bottom_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFDestination_get_Right_Proxy( 
    IPDFDestination __RPC_FAR * This,
    /* [retval][out] */ float __RPC_FAR *pVal);


void __RPC_STUB IPDFDestination_get_Right_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFDestination_get_Top_Proxy( 
    IPDFDestination __RPC_FAR * This,
    /* [retval][out] */ float __RPC_FAR *pVal);


void __RPC_STUB IPDFDestination_get_Top_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFDestination_get_Zoom_Proxy( 
    IPDFDestination __RPC_FAR * This,
    /* [retval][out] */ float __RPC_FAR *pVal);


void __RPC_STUB IPDFDestination_get_Zoom_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IPDFDestination_INTERFACE_DEFINED__ */


#ifndef __IPDFOutlineItem_INTERFACE_DEFINED__
#define __IPDFOutlineItem_INTERFACE_DEFINED__

/* interface IPDFOutlineItem */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_IPDFOutlineItem;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("E1AFC937-B6AC-4E19-9BC1-F46E8A125E6F")
    IPDFOutlineItem : public IDispatch
    {
    public:
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Title( 
            /* [retval][out] */ BSTR __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Count( 
            /* [retval][out] */ long __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Dest( 
            /* [retval][out] */ IPDFDestination __RPC_FAR *__RPC_FAR *pVal) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IPDFOutlineItemVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *QueryInterface )( 
            IPDFOutlineItem __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void __RPC_FAR *__RPC_FAR *ppvObject);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *AddRef )( 
            IPDFOutlineItem __RPC_FAR * This);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *Release )( 
            IPDFOutlineItem __RPC_FAR * This);
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetTypeInfoCount )( 
            IPDFOutlineItem __RPC_FAR * This,
            /* [out] */ UINT __RPC_FAR *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetTypeInfo )( 
            IPDFOutlineItem __RPC_FAR * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo __RPC_FAR *__RPC_FAR *ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetIDsOfNames )( 
            IPDFOutlineItem __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR __RPC_FAR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID __RPC_FAR *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Invoke )( 
            IPDFOutlineItem __RPC_FAR * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS __RPC_FAR *pDispParams,
            /* [out] */ VARIANT __RPC_FAR *pVarResult,
            /* [out] */ EXCEPINFO __RPC_FAR *pExcepInfo,
            /* [out] */ UINT __RPC_FAR *puArgErr);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_Title )( 
            IPDFOutlineItem __RPC_FAR * This,
            /* [retval][out] */ BSTR __RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_Count )( 
            IPDFOutlineItem __RPC_FAR * This,
            /* [retval][out] */ long __RPC_FAR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_Dest )( 
            IPDFOutlineItem __RPC_FAR * This,
            /* [retval][out] */ IPDFDestination __RPC_FAR *__RPC_FAR *pVal);
        
        END_INTERFACE
    } IPDFOutlineItemVtbl;

    interface IPDFOutlineItem
    {
        CONST_VTBL struct IPDFOutlineItemVtbl __RPC_FAR *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IPDFOutlineItem_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IPDFOutlineItem_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IPDFOutlineItem_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IPDFOutlineItem_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IPDFOutlineItem_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IPDFOutlineItem_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IPDFOutlineItem_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define IPDFOutlineItem_get_Title(This,pVal)	\
    (This)->lpVtbl -> get_Title(This,pVal)

#define IPDFOutlineItem_get_Count(This,pVal)	\
    (This)->lpVtbl -> get_Count(This,pVal)

#define IPDFOutlineItem_get_Dest(This,pVal)	\
    (This)->lpVtbl -> get_Dest(This,pVal)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFOutlineItem_get_Title_Proxy( 
    IPDFOutlineItem __RPC_FAR * This,
    /* [retval][out] */ BSTR __RPC_FAR *pVal);


void __RPC_STUB IPDFOutlineItem_get_Title_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFOutlineItem_get_Count_Proxy( 
    IPDFOutlineItem __RPC_FAR * This,
    /* [retval][out] */ long __RPC_FAR *pVal);


void __RPC_STUB IPDFOutlineItem_get_Count_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPDFOutlineItem_get_Dest_Proxy( 
    IPDFOutlineItem __RPC_FAR * This,
    /* [retval][out] */ IPDFDestination __RPC_FAR *__RPC_FAR *pVal);


void __RPC_STUB IPDFOutlineItem_get_Dest_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IPDFOutlineItem_INTERFACE_DEFINED__ */


EXTERN_C const CLSID CLSID_ColorSpace;

#ifdef __cplusplus

class DECLSPEC_UUID("8360A99C-F604-4DFD-8B26-A9E735E414AA")
ColorSpace;
#endif

EXTERN_C const CLSID CLSID_TransformMatrix;

#ifdef __cplusplus

class DECLSPEC_UUID("A52F80A5-6BBE-460C-9249-E8DAB46688B5")
TransformMatrix;
#endif

EXTERN_C const CLSID CLSID_Font;

#ifdef __cplusplus

class DECLSPEC_UUID("E46EBEE0-0E50-4A5E-88AC-519335F8714D")
Font;
#endif

EXTERN_C const CLSID CLSID_Text;

#ifdef __cplusplus

class DECLSPEC_UUID("BC62F899-1C67-4D7A-A5CC-F35819051552")
Text;
#endif

EXTERN_C const CLSID CLSID_Image;

#ifdef __cplusplus

class DECLSPEC_UUID("ACC2258C-D7ED-468C-BF99-118795A997E0")
Image;
#endif

EXTERN_C const CLSID CLSID_GraphicsState;

#ifdef __cplusplus

class DECLSPEC_UUID("39D04D8D-6AA7-42EC-B3F8-6CE5E44DD363")
GraphicsState;
#endif

EXTERN_C const CLSID CLSID_Content;

#ifdef __cplusplus

class DECLSPEC_UUID("F282DB6E-18C9-4E6A-A3C6-A505FE799BA6")
Content;
#endif

EXTERN_C const CLSID CLSID_Page;

#ifdef __cplusplus

class DECLSPEC_UUID("2D5A39C5-6BAC-419e-8964-9199C92F2DA0")
Page;
#endif

EXTERN_C const CLSID CLSID_Document;

#ifdef __cplusplus

class DECLSPEC_UUID("2C02B78D-B66A-4ED4-ADF7-DA2491972621")
Document;
#endif

EXTERN_C const CLSID CLSID_AlternateImage;

#ifdef __cplusplus

class DECLSPEC_UUID("AA7CE8E6-5B44-49CA-A9E1-391F39695207")
AlternateImage;
#endif

EXTERN_C const CLSID CLSID_Annotation;

#ifdef __cplusplus

class DECLSPEC_UUID("69F55E13-7315-4403-AA77-562225172C64")
Annotation;
#endif

EXTERN_C const CLSID CLSID_OutlineItem;

#ifdef __cplusplus

class DECLSPEC_UUID("F236D5D4-21EA-4FE1-A230-D4F48391A284")
OutlineItem;
#endif

EXTERN_C const CLSID CLSID_Destination;

#ifdef __cplusplus

class DECLSPEC_UUID("C4817A5F-DABE-4751-BF36-4643F3E38600")
Destination;
#endif
#endif /* __PDFPARSERLib_LIBRARY_DEFINED__ */

/* Additional Prototypes for ALL interfaces */

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif
