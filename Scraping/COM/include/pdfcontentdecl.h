
/****************************************************************************
 *
 * File:            pdfcontentdecl.h
 *
 * Description:     The include file for native C interfaces using page and form contents.
 *
 * Author:          Dr. Hans B�rfuss, PDF Tools AG   
 * 
 * Copyright:       Copyright (C) 2001 - 2005 PDF Tools AG, Switzerland
 *                  All rights reserved.
 *                  
 ***************************************************************************/

#ifndef _PDFCONTENTDECL_INCLUDED
#define _PDFCONTENTDECL_INCLUDED

typedef enum TPDFContentObject 
{
	eNone, 
	eText, 
	eImage, 
	ePath 
} TPDFContentObject;

#endif  //_PDFCONTENTDECL_INCLUDED
