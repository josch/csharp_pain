
/****************************************************************************
 *
 * File:            pdfcodecdecl.h
 *
 * Description:     The include file for native C interfaces using codecs.
 *
 * Author:          Dr. Hans B�rfuss, PDF Tools AG   
 * 
 * Copyright:       Copyright (C) 2001 - 2005 PDF Tools AG, Switzerland
 *                  All rights reserved.
 *                  
 ***************************************************************************/

#ifndef _PDFCODECDECL_INCLUDED
#define _PDFCODECDECL_INCLUDED

typedef enum TPDFCompression 
{
    eComprRaw,                  // No compression (raw).
    eComprJPEG,                 // Lossless DCT (Discrete Cosine Transform, JPEG) compression. 
    eComprFlate,                // Lossless flate (ZIP)compression.
    eComprLZW,                  // Lossless LZW (Lempel-Ziff-Welch) compression.
    eComprGroup3,               // Lossless CCITT Fax Group3 compression.
    eComprGroup3_2D,            // Lossless CCITT Fax Group3 (2D) compression. 
    eComprGroup4,               // Lossless CCITT Fax Group4 compression.
    eComprJBIG2,                // Lossless JBGI2 compression.
    eComprJPEG2000,             // Lossy JPEG2000 compression.
    eComprUnknown
} TPDFCompression;

typedef enum TPDFColorSpace 
{
    eColorGray,                 // One channel gray.
    eColorGrayA,                // Two channels gray and alpha.
    eColorRGB,                  // Three channels red, green, blue.
    eColorRGBA,                 // Four channels red, green, blue, alpha.
    eColorCMYK,                 // Four channels cyan, magenta, yellow, black.
    eColorYCbCr,                // Three channels Luminance (Y) and Chroma (Cb, Cr)
    eColorYCbCrK,               // Four channels Luminance (Y), Chroma (Cb, Cr) and black.
    eColorPalette,              // One channel palette indices (into an RGB color table).
    eColorLAB,                  // Three channels IEC LAB.
    eColorOther
} TPDFColorSpace;

#endif // _PDFCODECDECL_INCLUDED
