
/****************************************************************************
 *
 * File:            pdferror.h
 *
 * Description:     The PDF error codes.
 *
 * Author:          Dr. Hans B�rfuss, PDF Tools AG   
 * 
 * Copyright:       Copyright (C) 2001 - 2005 PDF Tools AG, Switzerland
 *                  All rights reserved.
 *                  
 ***************************************************************************/

#ifndef _PDFERROR_INCLUDED
#define _PDFERROR_INCLUDED

#ifndef LIBPDFAPI
#define LIBPDFAPI
#endif

#define PDF_IS_ERROR(code)      (((unsigned int) (code) & 0x80000000) != 0)
#define PDF_IS_WARNING(code)    (((unsigned int) (code) & 0x00008000) != 0)

typedef enum TPDFErrorCode
{
// PDF general codes.
PDF_S_SUCCESS       =   0x00000000,     // The operation was completed successfully.
PDF_E_FATAL         =   0x8041ffff,     // Unknown fatal error.

// PDF stream module.
PDF_E_FILEOPEN      =   0x80410001,     // The file couldn't be opened.
PDF_E_FILECREATE    =   0x80410002,     // The file couldn't be created.
PDF_E_SETPOS        =   0x80410003,     // The read / write position pointer couldn't be set.

// PDF file module.
PDF_E_HEADER        =   0x80410010,     // The PDF header was not found.
PDF_E_EOF           =   0x80410011,     // The PDF end of file mark was not found.
PDF_E_XREF          =   0x80410012,     // The xref table contains an error.
PDF_E_TRAILER       =   0x80410013,     // The trailer dictionary is missing or invalid.
PDF_E_ROOT          =   0x80410014,     // The root object was not found.
PDF_E_PASSWORD      =   0x80410015,     // The authentication failed due to a wrong password.
PDF_E_CORRUPT       =   0x80410016,     // The file is corrupt and needs to be repaired. 
PDF_W_CORRUPT       =   0x00418016,     // The file is corrupt and needs to be repaired. 
PDF_W_OBJNUMBER     =   0x00418017,     // The object with this number doesn't exist.
PDF_W_GENNUMBER     =   0x00418018,     // The generation numbers of the object and the objects reference do not match.

// PDF filter module.
PDF_E_FILTER        =   0x80410060,     // The filter is unknown.
PDF_E_ASCIIHEX      =   0x80410061,     // A character in an ASCIIHex stream is invalid.
PDF_E_ASCII85_G5    =   0x80410062,     // The value represented by a group of five characters in an ASCII85 stream is invalid.
PDF_E_ASCII85_Z     =   0x80410063,     // The position of a z character in an ASCII85 stream is invalid.
PDF_E_ASCII85_G1    =   0x80410064,     // A final partial group in an ASCII85 stream contains only one character.
PDF_E_ASCII85_CH    =   0x80410065,     // An invalid character was encountered in an ASCII85 stream.
PDF_E_FLATE_DINI    =   0x80410066,     // The deflate initialization failed with %d.
PDF_E_FLATE_DFAIL   =   0x80410067,     // The deflate operation failed with %d.
PDF_E_FLATE_IINI    =   0x80410068,     // The inflate initialization failed with %d.
PDF_E_FLATE_IFAIL   =   0x80410069,     // The inflate operation failed with %d.
PDF_E_LZW_EOI       =   0x80410070,     // An EOI code was missing in an LZW stream.
PDF_E_LZW_CODE      =   0x80410071,     // An invalid code was encountered in an LZW stream.
PDF_E_CCITTFAX_1DEXT=   0x80410072,     // The CCITTFax stream contains 1D extensions. 
PDF_E_CCITTFAX_HUFF1D=  0x80410073,     // The CCITTFax stream contains an invalid 1D huffman code.
PDF_E_CCITTFAX_LINE =   0x80410074,     // The CCITTFax stream contains a line which is too long.
PDF_E_CCITTFAX_RUN  =   0x80410075,     // The CCITTFax stream contains too few runs.
PDF_E_CCITTFAX_2DEXT=   0x80410076,     // The CCITTFax stream contains 2D extensions.
PDF_E_CCITTFAX_HUFF2D=  0x80410077,     // The CCITTFax stream contains an invalid 2D huffman code.
PDF_E_JPX_FEATURE   =   0x80410078,     // The JPX stream contains unsupported features.
PDF_E_JPX_INTERNAL  =   0x80410079,     // The JPX stream caused an internal library error with code %d.
PDF_I_JB2           =   0x00410080,     // The JB2 stream caused the information '%s'.
PDF_W_JB2           =   0x00418080,     // The JB2 stream caused the warning '%s'.
PDF_E_JB2           =   0x80410080,     // The JB2 stream caused the error '%s'.
PDF_E_IMAGEDATA     =   0x80410081,     // The image data stream is too short.
 
// PDF parser module.
PDF_E_OBJNO         =   0x80410021,     // The object number is missing.
PDF_E_GENNO         =   0x80410022,     // The generation number is missing.
PDF_E_IDENTITY      =   0x80410023,     // The object's identity doesn't match with the reference's.
PDF_E_OBJ           =   0x80410024,     // The "obj" keyword is missing.
PDF_E_NULL          =   0x80410025,     // The object is empty (null).
PDF_E_LENGTH        =   0x80410026,     // The "Length" attribute of the stream object is wrong.
PDF_E_ENDSTREAM     =   0x80410027,     // The "endstream" keyword is missing.
PDF_E_ENDOBJ        =   0x80410028,     // The "endobj" keyword is missing.

// PDF document module.
PDF_E_PAGE          =   0x80410031,     // The page doesn't exist (null).
PDF_E_PAGETYPE      =   0x80410032,     // The page has a missing or invalid "Type" attribute.
PDF_E_PAGEKIDS      =   0x80410033,     // The page has a missing or invalid "Kids" attribute.
PDF_E_PAGECOUNT     =   0x80410034,     // The page has a missing or invalid "Count" attribute.
PDF_E_PAGEPARENT    =   0x80410035,     // The page has a missing or invalid "Parent" attribute.
PDF_E_PAGERES       =   0x80410036,     // The page has a missing or invalid "Resource" attribute.
PDF_E_PAGEMEDIABOX  =   0x80410037,     // The page has a missing or invalid "MediaBox" attribute.
PDF_I_OPEN          =   0x00410001,     // The document is going to be opened.
PDF_I_PAGE          =   0x00410002,     // The page is going to be read.

// PDF content module.
PDF_E_TOKEN         =   0x80410041,     // An unexpected token was found.
PDF_E_OPERATOR      =   0x80410042,     // The content stream contains an invalid operator.
PDF_E_OPERANDS      =   0x80410043,     // The operator has an invalid number of operands.
PDF_E_OPNDSTACK     =   0x80410044,     // An operand stack over- or underflow occured.
PDF_E_OPNDNUMBER    =   0x80410045,     // The operand must be a number.
PDF_E_OPNDSTRING    =   0x80410046,     // The operand must be a string.
PDF_E_OPNDNAME      =   0x80410047,     // The operand must be a name.
PDF_E_OPNDARRAY     =   0x80410048,     // The operand must be an array.
PDF_E_OPNDDICT      =   0x80410049,     // The operand must be a dictionary.
PDF_E_OPNDVALUE     =   0x8041004A,     // The value of an operand is out of range.
PDF_E_UNKCOLORSPACE =   0x8041004B,     // The name of the color space is unknown.
PDF_E_UNKGS         =   0x8041004C,     // The name of a graphics state dictionary is unknown.
PDF_E_UNKPAT        =   0x8041004D,     // The name of a pattern dictionary is unknown.
PDF_E_UNKXOBJ       =   0x8041004E,     // The name of a xobject is unknown.
PDF_E_GSSTACK       =   0x80410051,     // A graphics state stack over- or underflow occurred.
PDF_E_BEGINTEXT     =   0x80410052,     // A begin text operator is missing.
PDF_E_ENDTEXT       =   0x80410053,     // An end text operator is missing.
PDF_E_BEGINPATH     =   0x80410054,     // A path start operator was missing.
PDF_E_EMPTYXOBJ     =   0x80410055,     // The form xobject has an empty or unreadable content stream.
PDF_I_UNUSEDPATH    =   0x00410056,     // An path was constructed but not painted.
PDF_I_GSSTATE       =   0x00410057,     // An unknown graphics state parameter was encountered.
PDF_W_EMPTYPATH     =   0x00418058,     // A path painting operator was used on an empty path.

// PDF font module.
PDF_E_FONTPROG      =   0x80410060,     // The font program cannot be read.

// PDF rendering module.
PDF_E_PATTERNTYPE   =   0x81410001,     // The type of pattern is unknown or not yet implemented.
PDF_E_CHARPROC      =   0x81410002,     // The glyph's charproc was not found.
PDF_E_SHADINGTYPE   =   0x81410003,     // The type of shading is unknown or not yet implemented.
PDF_E_FONTINSTALL   =   0x81410004,     // The font program couldn't be installed successfully.
PDF_E_STANDARDFONT  =   0x81410005,     // The font program of a standard font was not found.
PDF_W_SAMPLELENGTH  =   0x01418006,     // The length of the image's sample stream is not correct.
PDF_W_PAINTMISSING  =   0x01418007,     // The path is constructed but the painting operator is missing.
PDF_W_SYSTEMFONT    =   0x00418008,     // The font uses a pre-installed system font.
PDF_W_FONTREPLACE   =   0x00418009,     // The font was replaced by an other font.

} TPDFErrorCode;

#ifndef __midl

typedef void (CDECL *TPDFErrorHandler)(TPDFErrorCode iErrorCode, va_list args);
LIBPDFAPI const char* CDECL GetPDFErrorMessage(TPDFErrorCode iErrorCode);

#endif

#endif // _PDFERROR_INCLUDED

