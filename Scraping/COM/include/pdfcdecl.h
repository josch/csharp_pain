
/****************************************************************************
 *
 * File:            pdfdecl.h
 *
 * Description:     The basic header file for native C interfaces.
 *
 * Author:          Dr. Hans B�rfuss, PDF Tools AG   
 * 
 * Copyright:       Copyright (C) 2001 - 2005 PDF Tools AG, Switzerland
 *                  All rights reserved.
 *                  
 ***************************************************************************/

#ifndef _PDFCDECL_INCLUDED
#define _PDFCDECL_INCLUDED

#if defined(UNICODE) && !defined(_UNICODE)
#define _UNICODE
#endif

// Character strings on Windows can be either WinAnsi (CP1252) or Unicode (UTF16).
// On Unix only char strings (8-bit, ISO encoded) are used for OS interfaces. 
// WCHAR strings are always UTF16 and may be different to wchar_t.
// This simplifies interoperability with Java.
#ifdef WIN32
#include <tchar.h>
typedef wchar_t WCHAR;
#ifndef CDECL
#define CDECL __cdecl
#endif
#else
#ifndef WCHAR
typedef unsigned short WCHAR;           // UTF-16
#endif
#ifndef CDECL
#define CDECL
#endif
#endif // !defined(WIN32)

typedef struct TPDFByteArray
{
    unsigned char*  m_pData;
    long            m_nLength;
} TPDFByteArray;

typedef struct TPDFFloatArray
{
    float*			m_pData;
    long            m_nLength;
} TPDFFloatArray;

#endif // _PDFCDECL_INCLUDED
