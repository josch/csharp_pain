
/****************************************************************************
 *
 * File:            pdfcommondecl.h
 *
 * Description:     The include file for native C interfaces using PDF common objects.
 *
 * Author:          Dr. Hans B�rfuss, PDF Tools AG   
 * 
 * Copyright:       Copyright (C) 2001 - 2005 PDF Tools AG, Switzerland
 *                  All rights reserved.
 *                  
 ***************************************************************************/

#ifndef _PDFCOMMONDECL_INCLUDED
#define _PDFCOMMONDECL_INCLUDED

typedef struct TPDFRect
{
    float   m_fRect[4];
} TPDFRect;

/** orientation enumeration type */
typedef enum TPDFOrientation 
{
	eOrientationTopLeft = 1, 
	eOrientationTopRight = 2, 
	eOrientationBottomRight = 3, 
	eOrientationBottomLeft = 4,
    eOrientationLeftTop = 5, 
	eOrientationRightTop = 6, 
	eOrientationRightBottom = 7, 
	eOrientationLeftBottom = 8
} TPDFOrientation;

/** structure representing a date and time of day (e.g. CreationDate) */
typedef struct TPDFDate
{
    short iYear;   /**< year 1900..2100 */
    short iMonth ; /**< 1..12 */
    short iDay;    /**< 1..31 */
    short iHour;   /**< 0..23 */
    short iMinute; /**< 0..59 */
    short iSecond; /**< 0..59 */
    signed short iUtcOffset; /**< time zone information, offset to UTC in minutes */
} TPDFDate;

#endif //_PDFCOMMONDECL_INCLUDED
