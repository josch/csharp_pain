object TextExtForm: TTextExtForm
  Left = 233
  Top = 149
  Width = 274
  Height = 533
  Caption = 'PDF Export API Text-Extraction'
  Color = clSilver
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 1
    Top = 0
    Width = 264
    Height = 33
    Alignment = taRightJustify
    AutoSize = False
    Caption = 'pdf-tools.com '
    Color = clWhite
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -21
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
  end
  object GetText: TButton
    Left = 64
    Top = 472
    Width = 145
    Height = 25
    Caption = 'Get Text'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 0
    OnClick = GetTextClick
  end
  object Memo1: TMemo
    Left = 8
    Top = 72
    Width = 249
    Height = 393
    TabOrder = 1
  end
  object InFileName: TEdit
    Left = 8
    Top = 40
    Width = 177
    Height = 21
    TabOrder = 2
  end
  object Browse: TButton
    Left = 200
    Top = 40
    Width = 57
    Height = 25
    Caption = 'Browse'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 3
    OnClick = BrowseClick
  end
  object OpenDialog: TOpenDialog
    Left = 224
    Top = 472
  end
end
