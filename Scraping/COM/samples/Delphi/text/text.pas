unit text;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, OleServer, PDFPARSERLib_TLB;

type
  TTextExtForm = class(TForm)
    GetText: TButton;
    Memo1: TMemo;
    Label1: TLabel;
    InFileName: TEdit;
    Browse: TButton;
    OpenDialog: TOpenDialog;

    procedure GetTextClick(Sender: TObject);
    procedure BrowseClick(Sender: TObject);
  private
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
  end;

var
  TextExtForm: TTextExtForm;

implementation

{$R *.dfm}

procedure TTextExtForm.GetTextClick(Sender: TObject);
var
    pdf : IPDFDocument;
    content : IPDFContent;
    i : integer;
    Text : IPDFText;
    Str : String;

begin
    pdf := CoDocument.Create;
    if not pdf.open(InFileName.Text , '') then
    with Application do
    begin
       NormalizeTopMosts;
       MessageBox('Could not create PDF file.', 'Look');
       RestoreTopMosts;
       Exit;
    end;
    Str := '';
    for i := 0 to pdf.pagecount-1 do
      begin
        pdf.pageNo := i + 1;
        content := pdf.Page.content;
        if content <> nil then
        begin
           content.breakWords := true;
           Content.reset(true);
           While (content.GetNextText <> nil) do
           begin
             Text := content.text;
             Str := Str + content.text.UnicodeString + ' ';
           end;
        end;
      end;
   Memo1.Text := Str;
   end;

procedure TTextExtForm.BrowseClick(Sender: TObject);
begin
  OpenDialog.FileName := InFileName.Text;
  OpenDialog.Execute;
  InFileName.Text := OpenDialog.FileName;
end;

end.
