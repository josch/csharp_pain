VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form Form1 
   BackColor       =   &H00FFFFFF&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Outline Extraction"
   ClientHeight    =   8340
   ClientLeft      =   3990
   ClientTop       =   2430
   ClientWidth     =   9120
   FillColor       =   &H00FFFFFF&
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8340
   ScaleWidth      =   9120
   Begin VB.TextBox TextLogo 
      BackColor       =   &H006699FF&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   1440
      TabIndex        =   14
      Text            =   " 3-Heights PDF Extract Tool - Outline"
      Top             =   120
      Width           =   7575
   End
   Begin VB.Frame Frame2 
      BackColor       =   &H00E2D1AE&
      BorderStyle     =   0  'None
      Height          =   7770
      Left            =   0
      TabIndex        =   11
      Top             =   0
      Width           =   1335
      Begin VB.Label Label1 
         BackColor       =   &H00D7B67A&
         Caption         =   " OUTLINE"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00996600&
         Height          =   315
         Left            =   0
         TabIndex        =   13
         Top             =   1440
         Width           =   1335
      End
      Begin VB.Label Label10 
         BackColor       =   &H00D7B67A&
         Caption         =   " PDF FILE"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00996600&
         Height          =   315
         Left            =   0
         TabIndex        =   12
         Top             =   840
         Width           =   1335
      End
   End
   Begin VB.Frame Frame16 
      BackColor       =   &H00E2D1AE&
      BorderStyle     =   0  'None
      Height          =   255
      Left            =   0
      TabIndex        =   9
      Top             =   7800
      Width           =   9135
      Begin VB.Label Label22 
         BackColor       =   &H00D7B67A&
         BackStyle       =   0  'Transparent
         Caption         =   "Last update: November, 2005   -   Copyright 2001-2005 PDF Tools AG"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00996600&
         Height          =   315
         Left            =   720
         TabIndex        =   10
         Top             =   0
         Width           =   7215
      End
   End
   Begin VB.Frame Frame17 
      BackColor       =   &H00996600&
      BorderStyle     =   0  'None
      Height          =   45
      Left            =   0
      TabIndex        =   8
      Top             =   8040
      Width           =   9135
   End
   Begin VB.Frame Frame14 
      Appearance      =   0  'Flat
      BackColor       =   &H00996600&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   540
      Left            =   1440
      TabIndex        =   4
      Top             =   720
      Width           =   7500
      Begin VB.Frame Frame3 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   500
         Left            =   20
         TabIndex        =   5
         Top             =   20
         Width           =   7460
         Begin VB.TextBox txtInput 
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   120
            TabIndex        =   7
            Top             =   120
            Width           =   6135
         End
         Begin VB.CommandButton OpenPDF 
            Caption         =   "Browse"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   6360
            TabIndex        =   6
            Top             =   80
            Width           =   975
         End
      End
   End
   Begin VB.Frame Frame1 
      Appearance      =   0  'Flat
      BackColor       =   &H00996600&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   6180
      Left            =   1440
      TabIndex        =   0
      Top             =   1320
      Width           =   7500
      Begin VB.Frame Frame4 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   6140
         Left            =   20
         TabIndex        =   1
         Top             =   20
         Width           =   7460
         Begin VB.TextBox txtList 
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   5925
            Left            =   120
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   3
            Top             =   120
            Width           =   6135
         End
         Begin VB.CommandButton Extract 
            Caption         =   "List Outline"
            Height          =   495
            Left            =   6360
            TabIndex        =   2
            Top             =   120
            Width           =   975
         End
      End
   End
   Begin MSComDlg.CommonDialog fileDialog 
      Left            =   8400
      Top             =   7440
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' List Outlines (Bookmarks)
' -------------------------
'
' Visual Basic 6 sample for the 3-Heights PDF Extract Tool API
' http://www.pdf-tools.com
'
' Copyright (C) 2005 PDF Tools AG, Switzerland
' Permission to use, copy, modify, and distribute this
' software and its documentation for any purpose and without
' fee is hereby granted, provided that the above copyright
' notice appear in all copies and that both that copyright
' notice and this permission notice appear in supporting
' documentation.  This software is provided "as is" without
' express or implied warranty.

Private Sub Extract_Click()

    Dim doc As New PDFPARSERLib.Document
    Dim outline As PDFPARSERLib.OutlineItem
    Dim blanks As String
    
    If Not doc.Open(txtInput, "") Then
        MsgBox "Could not open PDF file."
        Exit Sub
    End If
    
    txtList.Text = ""
    
    Set outline = doc.GetFirstOutlineItem
    While Not outline Is Nothing
        blanks = ""
        For blank = 1 To doc.GetCurrentOutlineLevel
            blanks = blanks & " "
        Next blank
        txtList.Text = txtList.Text & blanks & outline.Title & Chr(13) & Chr(10)
        Set outline = doc.GetNextOutlineItem
    Wend
    
End Sub

Private Sub OpenPDF_Click()
    ' Open File Dialog
    fileDialog.FileName = txtInput
    fileDialog.ShowOpen
    txtInput = fileDialog.FileName
End Sub
