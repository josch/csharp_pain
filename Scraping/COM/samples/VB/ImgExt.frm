VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form Form1 
   BackColor       =   &H00FFFFFF&
   Caption         =   "Image Extraction"
   ClientHeight    =   3585
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   9120
   LinkTopic       =   "Form1"
   ScaleHeight     =   3585
   ScaleWidth      =   9120
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame Frame1 
      Appearance      =   0  'Flat
      BackColor       =   &H00996600&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   1260
      Left            =   1440
      TabIndex        =   11
      Top             =   1320
      Width           =   7500
      Begin VB.Frame Frame4 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   1220
         Left            =   20
         TabIndex        =   12
         Top             =   20
         Width           =   7460
         Begin VB.CommandButton Extract 
            Caption         =   "Extract Images"
            Height          =   495
            Left            =   6360
            TabIndex        =   14
            Top             =   120
            Width           =   975
         End
         Begin VB.TextBox txtList 
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   1005
            Left            =   120
            MultiLine       =   -1  'True
            TabIndex        =   13
            Top             =   120
            Width           =   6135
         End
      End
   End
   Begin VB.Frame Frame14 
      Appearance      =   0  'Flat
      BackColor       =   &H00996600&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   540
      Left            =   1440
      TabIndex        =   7
      Top             =   720
      Width           =   7500
      Begin VB.Frame Frame3 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   500
         Left            =   20
         TabIndex        =   8
         Top             =   20
         Width           =   7460
         Begin VB.CommandButton OpenPDF 
            Caption         =   "Browse"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   6360
            TabIndex        =   10
            Top             =   80
            Width           =   975
         End
         Begin VB.TextBox txtInput 
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   120
            TabIndex        =   9
            Top             =   120
            Width           =   6135
         End
      End
   End
   Begin VB.Frame Frame17 
      BackColor       =   &H00996600&
      BorderStyle     =   0  'None
      Height          =   45
      Left            =   0
      TabIndex        =   4
      Top             =   3240
      Width           =   9135
   End
   Begin VB.Frame Frame16 
      BackColor       =   &H00E2D1AE&
      BorderStyle     =   0  'None
      Height          =   255
      Left            =   0
      TabIndex        =   5
      Top             =   3000
      Width           =   9135
      Begin VB.Label Label22 
         BackColor       =   &H00D7B67A&
         BackStyle       =   0  'Transparent
         Caption         =   "Last update: November 22, 2005   -   Copyright 2001-2005 PDF Tools AG"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00996600&
         Height          =   315
         Left            =   720
         TabIndex        =   6
         Top             =   0
         Width           =   6495
      End
   End
   Begin VB.Frame Frame2 
      BackColor       =   &H00E2D1AE&
      BorderStyle     =   0  'None
      Height          =   2970
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   1335
      Begin VB.Label Label10 
         BackColor       =   &H00D7B67A&
         Caption         =   " PDF FILE"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00996600&
         Height          =   315
         Left            =   0
         TabIndex        =   3
         Top             =   720
         Width           =   1335
      End
      Begin VB.Label Label1 
         BackColor       =   &H00D7B67A&
         Caption         =   " Image List"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00996600&
         Height          =   315
         Left            =   0
         TabIndex        =   2
         Top             =   1440
         Width           =   1335
      End
   End
   Begin VB.TextBox TextLogo 
      BackColor       =   &H006699FF&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   1440
      TabIndex        =   0
      Text            =   " 3-Heights PDF Extract Tool - Images"
      Top             =   120
      Width           =   7575
   End
   Begin MSComDlg.CommonDialog fileDialog 
      Left            =   8160
      Top             =   2520
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' Extract and list images and their properties
' --------------------------------------------
'
' Visual Basic 6 sample for the 3-Heights PDF Extract Tool API
' http://www.pdf-tools.com
'
' Copyright (C) 2005 PDF Tools AG, Switzerland
' Permission to use, copy, modify, and distribute this
' software and its documentation for any purpose and without
' fee is hereby granted, provided that the above copyright
' notice appear in all copies and that both that copyright
' notice and this permission notice appear in supporting
' documentation.  This software is provided "as is" without
' express or implied warranty.

Private Sub Extract_Click()

    Dim pdf As New PDFPARSERLib.Document
    Dim content As PDFPARSERLib.content
    Dim image As PDFPARSERLib.image
    Dim ctm As PDFPARSERLib.TransformMatrix
    Dim Width As Single, Height As Single, X As Single, Y As Single
    Dim sColor As String
    
    ' Open the input file
    If pdf.Open(txtInput.Text) Then
        
        ' Clear Text field and reset counter
        txtList.Text = ""
        imagecount = 0
        
        ' Loop through all pages
        For Page = 1 To pdf.PageCount
            
            ' Set current page number
            pdf.PageNo = Page
                 
            ' Get the page content
            Set content = pdf.Page.content
            If Not (content Is Nothing) Then
            
                ' Loop through all images
                Do
                    ' Get the next image from the page
                    Set image = content.GetNextImage
                    If Not (image Is Nothing) Then
                        imagecount = imagecount + 1
                        
                        ' Get the Transformation Matrix
                        Set ctm = content.GraphicsState.ctm
                        res = image.GetResolution(ctm)
                                               
                        ' Get the coordinates and dimensions of the image
                        ' Calcualte Pythagoras in case of rotated images
                        Width = Round(Sqr(ctm.a * ctm.a + ctm.b * ctm.b), 1)
                        Height = Round(Sqr(ctm.d * ctm.d + ctm.c * ctm.c), 1)
                        X = Round(ctm.e, 1)
                        Y = Round(ctm.f, 1)
                        
                        ' Check if it's color
                        If image.ColorSpace.IsColor Then
                            sColor = "color"
                        ElseIf image.ColorSpace.IsMonochrome Then
                            If image.BitsPerComponent > 1 Then
                                sColor = "greyscale"
                            Else
                                sColor = "bitonal"
                            End If
                        End If
                        
                        ' Update the Text List
                        txtList.Text = txtList.Text & "img: " & imagecount & ",  w=" & _
                            Width & ",  h=" & Height & ",  x=" & X & ",  y=" & Y & ",  page=" & _
                            Page & ", " & sColor & ", res=" & Round(res, 0) & Chr(13) & Chr(10)
                            
                        ' Store the image on a file
                        image.ChangeOrientation (ctm.Orientation)
                        image.Store App.Path & "\out" & imagecount & ".tif", eLZW

                    Else
                        Exit Do
                    End If
                Loop
            End If
        Next Page
        pdf.Close
    Else
        MsgBox "Couldn't open input file"
    End If
End Sub

Private Sub OpenPDF_Click()
    ' Open File Dialog
    fileDialog.FileName = txtInput
    fileDialog.ShowOpen
    txtInput = fileDialog.FileName
End Sub
