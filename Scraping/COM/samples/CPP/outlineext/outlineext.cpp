/****************************************************************************
 *
 * File:            outlineext.cpp
 *
 * Usage:           outlineext PDF-input output.txt
 *
 * Description:     Extracts the outlines (bookmarks) of a PDF document and  
 *                  prints them to an Ascii file
 *
 * Version:         1.00  (25-July-2005)
 *
 * Author:          Philip Renggli, PDF Tools AG   
 * 
 * Copyright:       Copyright (C) 2005 PDF Tools AG, Switzerland
 *                  Permission to use, copy, modify, and distribute this
 *                  software and its documentation for any purpose and without
 *                  fee is hereby granted, provided that the above copyright
 *                  notice appear in all copies and that both that copyright
 *                  notice and this permission notice appear in supporting
 *                  documentation.  This software is provided "as is" without
 *                  express or implied warranty.
 *
 ***************************************************************************/

#include <stdio.h>
#include <windows.h>
#include "expa_c.h"

int main(int argc, char* argv[])
{
    TExpaDocument         pDocument;
	TPdfExpaOutlineItem*  pOutline;
    FILE*                 out;

    if (argc < 3)
    {
        printf("Usage: outlineext input.pdf output.txt");
        return 3;
    } 

    ExpaInitialize();

    /* Create the object */
    pDocument = ExpaCreateObject();

    /* Open the document */
    if (!ExpaDocOpen(pDocument, argv[1], "")) 
    {
        printf("error opening PDF file %s...\n",argv[1]);
        return 1;
    }

    /* Create an Ascii Output file */
    out = fopen(argv[2], "w");
    if (!out){
        fprintf(stderr, "Couldn't create file %s\n", argv[2]);
        return 2;
    }

	/* get root node */
	pOutline = ExpaDocGetFirstOutlineItem(pDocument);

    /* walk through outline tree and print the titles */
    while(pOutline)
    {
		for (int iLevel=1; iLevel < ExpaDocGetCurrentOutlineLevel(pDocument); iLevel++)
			fprintf(out, " ");
		fprintf(out, "%s  (%s", ExpaOutlineItemGetTitleA(pOutline), ExpaDestGetType(ExpaOutlineItemGetDest(pOutline)));
		fprintf(out, " %i)", ExpaDestGetPageNo(ExpaOutlineItemGetDest(pOutline)));
		fprintf(out, "count = %i \n", ExpaOutlineItemGetCount(pOutline));
		pOutline = ExpaDocGetNextOutlineItem(pDocument, 1000, false);
    }

    /* Close the text file */
    fclose(out);

    /* Close and Destroy the object */
    ExpaDocClose(pDocument);
    ExpaDestroyObject(pDocument);
    ExpaUnInitialize();
    return 0;
}

