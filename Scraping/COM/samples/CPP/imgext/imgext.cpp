/****************************************************************************
 *
 * File:            imgext.cpp
 *
 * Usage:           imgext PDF-input
 *
 * Description:     Extracts Images of a PDF document 
 *
 * Version:         1.00  (15-Nov-2005)
 *
 * Author:          Philip Renggli, PDF Tools AG   
 * 
 * Copyright:       Copyright (C) 2005 PDF Tools AG, Switzerland
 *                  Permission to use, copy, modify, and distribute this
 *                  software and its documentation for any purpose and without
 *                  fee is hereby granted, provided that the above copyright
 *                  notice appear in all copies and that both that copyright
 *                  notice and this permission notice appear in supporting
 *                  documentation.  This software is provided "as is" without
 *                  express or implied warranty.
 *
 ***************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "expa_c.h"


int main(int argc, char* argv[])
{
    TExpaDocument         pDocument;
    TExpaContent          pContent;
    TExpaGraphicsState    pGs;
    TExpaImage            pImage;
    double                dWidth, dHeight;
    int                   iImageNumber;
    char                  cImageNumber[33];

    if (argc < 2)
    {
        printf("Usage: imgext input.pdf");
        return 3;
    } 

    /* Create the object */
    ExpaInitialize();
    pDocument = ExpaCreateObject();

    /* Open the document */
    if (!ExpaDocOpen(pDocument, argv[1], ""))
    {
        printf("error opening PDF file %s...\n",argv[1]);
        return 1;
    }

    /* Loop through all page and render them */
    iImageNumber = 1;
    for(int iPage = 1; iPage <= ExpaDocGetPageCount(pDocument); iPage++)
    {
        /* Set the page number */
        ExpaDocSetPageNo(pDocument, iPage);

        /* Get the content */
        pContent = ExpaPageGetContent(ExpaDocGetPage(pDocument));
        
        while (ExpaContentGetNextImage(pContent))
        {
            pImage  = ExpaContentGetImage(pContent);
            dWidth  = ExpaImageGetWidth(pImage);
            dHeight = ExpaImageGetHeight(pImage);
            pGs     = ExpaContentGetGraphicsState(pContent);
            ExpaImageChangeOrientation(pImage, ExpaGSGetCTM(pGs)->Orientation);
            sprintf(cImageNumber, "%d", iImageNumber++);
            /* The name of the extention decides which image type is used. i.e. tif, jpg) */
            ExpaImageStore(pImage, strcat(cImageNumber, "-image.jpg"), eComprRaw);
        }
    }

    /* Close and destroy the object */
    ExpaDocClose(pDocument);
    ExpaDestroyObject(pDocument);
    ExpaUnInitialize();
    return 0;
}
