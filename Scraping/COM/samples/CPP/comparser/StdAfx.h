#if !defined(AFX_STDAFX_H__26DD6F9B_989C_4AE1_814D_BBD9DC3B3A67__INCLUDED_)
#define AFX_STDAFX_H__26DD6F9B_989C_4AE1_814D_BBD9DC3B3A67__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define WIN32_LEAN_AND_MEAN

#include <stdio.h>
#include <windows.h>
#include <atlbase.h>

#endif // !defined(AFX_STDAFX_H__26DD6F9B_989C_4AE1_814D_BBD9DC3B3A67__INCLUDED_)
