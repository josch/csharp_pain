/****************************************************************************
 *
 * File:            outlineextu.cpp
 *
 * Usage:           outlineextu PDF-input output.txt
 *
 * Description:     Extracts the outlines (bookmarks) of a PDF document and  
 *                  writes them to an Unicode file
 *
 * Version:         1.00  (9-August-2005)
 *
 * Author:          Philip Renggli, PDF Tools AG   
 * 
 * Copyright:       Copyright (C) 2005 PDF Tools AG, Switzerland
 *                  Permission to use, copy, modify, and distribute this
 *                  software and its documentation for any purpose and without
 *                  fee is hereby granted, provided that the above copyright
 *                  notice appear in all copies and that both that copyright
 *                  notice and this permission notice appear in supporting
 *                  documentation.  This software is provided "as is" without
 *                  express or implied warranty.
 *
 ***************************************************************************/

#include <stdio.h>
#include <windows.h>
#include "expa_c.h"
#include <fcntl.h>
#include <io.h>

void print(const WCHAR* wzStr, FILE* out)
{
    while(*wzStr) 
    {
      if (*wzStr == '\n') 
      {
        fputc('\r', out);
        fputc(0, out);
      }
      fputc(*wzStr % 256, out);
      fputc(*wzStr / 256, out);
      wzStr++;
    }
}

int main(int argc, char* argv[])
{
    TExpaDocument         pDocument;
	TPdfExpaOutlineItem*  pOutline;
    FILE*                 out;

    if (argc < 3)
    {
        printf("Usage: outlineext input.pdf output.txt");
        return 3;
    } 

    ExpaInitialize();

    /* Create the object */
    pDocument = ExpaCreateObject();

    /* Open the document */
    if (!ExpaDocOpen(pDocument, argv[1], "")) 
    {
        printf("error opening PDF file %s...\n",argv[1]);
        return 1;
    }

    /* Create an Ascii Output file */
    out = fopen(argv[2], "wb");
    if (!out){
        fprintf(stderr, "Couldn't create file %s\n", argv[2]);
        return 2;
    }

    /* Create a little endian Unicode file */
//#ifdef WIN32
    _setmode(_fileno(stdout), _O_BINARY);
//#endif
    fprintf(out, "\377\376");

	/* get root node */
	pOutline = ExpaDocGetFirstOutlineItem(pDocument);

    /* walk through outline tree and print the titles */
    while(pOutline)
    {
		for (int iLevel=1; iLevel < ExpaDocGetCurrentOutlineLevel(pDocument); iLevel++)
            print(L" ", out);
        print(ExpaOutlineItemGetTitleW(pOutline), out);
        print(L"\n", out);
        pOutline = ExpaDocGetNextOutlineItem(pDocument, 100, false);
    }

    /* Close the text file */
    fclose(out);

    /* Close and Destroy the object */
    ExpaDocClose(pDocument);
    ExpaDestroyObject(pDocument);
    ExpaUnInitialize();
    return 0;
}

