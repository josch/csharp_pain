Option Strict Off
Option Explicit On
Friend Class Form1
	Inherits System.Windows.Forms.Form
#Region "Windows Form Designer generated code "
	Public Sub New()
		MyBase.New()
		If m_vb6FormDefInstance Is Nothing Then
			If m_InitializingDefInstance Then
				m_vb6FormDefInstance = Me
			Else
				Try 
					'For the start-up form, the first instance created is the default instance.
					If System.Reflection.Assembly.GetExecutingAssembly.EntryPoint.DeclaringType Is Me.GetType Then
						m_vb6FormDefInstance = Me
					End If
				Catch
				End Try
			End If
		End If
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents Extract As System.Windows.Forms.Button
	Public WithEvents txtList As System.Windows.Forms.TextBox
	Public WithEvents Frame4 As System.Windows.Forms.Panel
	Public WithEvents Frame1 As System.Windows.Forms.Panel
	Public WithEvents OpenPDF As System.Windows.Forms.Button
	Public WithEvents txtInput As System.Windows.Forms.TextBox
	Public WithEvents Frame3 As System.Windows.Forms.Panel
	Public WithEvents Frame14 As System.Windows.Forms.Panel
	Public WithEvents Frame17 As System.Windows.Forms.Panel
	Public WithEvents Label22 As System.Windows.Forms.Label
	Public WithEvents Frame16 As System.Windows.Forms.Panel
	Public WithEvents Label10 As System.Windows.Forms.Label
	Public WithEvents Label1 As System.Windows.Forms.Label
	Public WithEvents Frame2 As System.Windows.Forms.Panel
	Public WithEvents TextLogo As System.Windows.Forms.TextBox
	Public WithEvents fileDialog As AxMSComDlg.AxCommonDialog
	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(Form1))
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.Frame1 = New System.Windows.Forms.Panel
        Me.Frame4 = New System.Windows.Forms.Panel
        Me.Extract = New System.Windows.Forms.Button
        Me.txtList = New System.Windows.Forms.TextBox
        Me.Frame14 = New System.Windows.Forms.Panel
        Me.Frame3 = New System.Windows.Forms.Panel
        Me.OpenPDF = New System.Windows.Forms.Button
        Me.txtInput = New System.Windows.Forms.TextBox
        Me.Frame17 = New System.Windows.Forms.Panel
        Me.Frame16 = New System.Windows.Forms.Panel
        Me.Label22 = New System.Windows.Forms.Label
        Me.Frame2 = New System.Windows.Forms.Panel
        Me.Label10 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.TextLogo = New System.Windows.Forms.TextBox
        Me.fileDialog = New AxMSComDlg.AxCommonDialog
        Me.Frame1.SuspendLayout()
        Me.Frame4.SuspendLayout()
        Me.Frame14.SuspendLayout()
        Me.Frame3.SuspendLayout()
        Me.Frame16.SuspendLayout()
        Me.Frame2.SuspendLayout()
        CType(Me.fileDialog, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Frame1
        '
        Me.Frame1.BackColor = System.Drawing.Color.FromArgb(CType(0, Byte), CType(102, Byte), CType(153, Byte))
        Me.Frame1.Controls.Add(Me.Frame4)
        Me.Frame1.Cursor = System.Windows.Forms.Cursors.Default
        Me.Frame1.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Frame1.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Frame1.Location = New System.Drawing.Point(96, 88)
        Me.Frame1.Name = "Frame1"
        Me.Frame1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Frame1.Size = New System.Drawing.Size(500, 84)
        Me.Frame1.TabIndex = 11
        '
        'Frame4
        '
        Me.Frame4.BackColor = System.Drawing.Color.White
        Me.Frame4.Controls.Add(Me.Extract)
        Me.Frame4.Controls.Add(Me.txtList)
        Me.Frame4.Cursor = System.Windows.Forms.Cursors.Default
        Me.Frame4.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Frame4.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Frame4.Location = New System.Drawing.Point(2, 2)
        Me.Frame4.Name = "Frame4"
        Me.Frame4.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Frame4.Size = New System.Drawing.Size(498, 82)
        Me.Frame4.TabIndex = 12
        '
        'Extract
        '
        Me.Extract.BackColor = System.Drawing.SystemColors.Control
        Me.Extract.Cursor = System.Windows.Forms.Cursors.Default
        Me.Extract.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Extract.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Extract.Location = New System.Drawing.Point(424, 8)
        Me.Extract.Name = "Extract"
        Me.Extract.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Extract.Size = New System.Drawing.Size(65, 33)
        Me.Extract.TabIndex = 14
        Me.Extract.Text = "Extract Images"
        '
        'txtList
        '
        Me.txtList.AcceptsReturn = True
        Me.txtList.AutoSize = False
        Me.txtList.BackColor = System.Drawing.SystemColors.Window
        Me.txtList.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtList.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtList.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtList.Location = New System.Drawing.Point(8, 8)
        Me.txtList.MaxLength = 0
        Me.txtList.Multiline = True
        Me.txtList.Name = "txtList"
        Me.txtList.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtList.Size = New System.Drawing.Size(409, 67)
        Me.txtList.TabIndex = 13
        Me.txtList.Text = ""
        '
        'Frame14
        '
        Me.Frame14.BackColor = System.Drawing.Color.FromArgb(CType(0, Byte), CType(102, Byte), CType(153, Byte))
        Me.Frame14.Controls.Add(Me.Frame3)
        Me.Frame14.Cursor = System.Windows.Forms.Cursors.Default
        Me.Frame14.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Frame14.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Frame14.Location = New System.Drawing.Point(96, 48)
        Me.Frame14.Name = "Frame14"
        Me.Frame14.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Frame14.Size = New System.Drawing.Size(500, 36)
        Me.Frame14.TabIndex = 7
        '
        'Frame3
        '
        Me.Frame3.BackColor = System.Drawing.Color.White
        Me.Frame3.Controls.Add(Me.OpenPDF)
        Me.Frame3.Controls.Add(Me.txtInput)
        Me.Frame3.Cursor = System.Windows.Forms.Cursors.Default
        Me.Frame3.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Frame3.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Frame3.Location = New System.Drawing.Point(2, 2)
        Me.Frame3.Name = "Frame3"
        Me.Frame3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Frame3.Size = New System.Drawing.Size(498, 34)
        Me.Frame3.TabIndex = 8
        '
        'OpenPDF
        '
        Me.OpenPDF.BackColor = System.Drawing.SystemColors.Control
        Me.OpenPDF.Cursor = System.Windows.Forms.Cursors.Default
        Me.OpenPDF.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OpenPDF.ForeColor = System.Drawing.SystemColors.ControlText
        Me.OpenPDF.Location = New System.Drawing.Point(424, 6)
        Me.OpenPDF.Name = "OpenPDF"
        Me.OpenPDF.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.OpenPDF.Size = New System.Drawing.Size(65, 25)
        Me.OpenPDF.TabIndex = 10
        Me.OpenPDF.Text = "Browse"
        '
        'txtInput
        '
        Me.txtInput.AcceptsReturn = True
        Me.txtInput.AutoSize = False
        Me.txtInput.BackColor = System.Drawing.SystemColors.Window
        Me.txtInput.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtInput.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtInput.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtInput.Location = New System.Drawing.Point(8, 8)
        Me.txtInput.MaxLength = 0
        Me.txtInput.Name = "txtInput"
        Me.txtInput.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtInput.Size = New System.Drawing.Size(409, 19)
        Me.txtInput.TabIndex = 9
        Me.txtInput.Text = ""
        '
        'Frame17
        '
        Me.Frame17.BackColor = System.Drawing.Color.FromArgb(CType(0, Byte), CType(102, Byte), CType(153, Byte))
        Me.Frame17.Cursor = System.Windows.Forms.Cursors.Default
        Me.Frame17.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Frame17.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Frame17.Location = New System.Drawing.Point(0, 216)
        Me.Frame17.Name = "Frame17"
        Me.Frame17.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Frame17.Size = New System.Drawing.Size(609, 3)
        Me.Frame17.TabIndex = 4
        '
        'Frame16
        '
        Me.Frame16.BackColor = System.Drawing.Color.FromArgb(CType(174, Byte), CType(209, Byte), CType(226, Byte))
        Me.Frame16.Controls.Add(Me.Label22)
        Me.Frame16.Cursor = System.Windows.Forms.Cursors.Default
        Me.Frame16.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Frame16.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Frame16.Location = New System.Drawing.Point(0, 200)
        Me.Frame16.Name = "Frame16"
        Me.Frame16.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Frame16.Size = New System.Drawing.Size(609, 17)
        Me.Frame16.TabIndex = 5
        '
        'Label22
        '
        Me.Label22.BackColor = System.Drawing.Color.Transparent
        Me.Label22.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label22.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.ForeColor = System.Drawing.Color.FromArgb(CType(0, Byte), CType(102, Byte), CType(153, Byte))
        Me.Label22.Location = New System.Drawing.Point(48, 0)
        Me.Label22.Name = "Label22"
        Me.Label22.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label22.Size = New System.Drawing.Size(433, 21)
        Me.Label22.TabIndex = 6
        Me.Label22.Text = "Last update: November 18, 2005   -   Copyright 2001-2005 PDF Tools AG"
        '
        'Frame2
        '
        Me.Frame2.BackColor = System.Drawing.Color.FromArgb(CType(174, Byte), CType(209, Byte), CType(226, Byte))
        Me.Frame2.Controls.Add(Me.Label10)
        Me.Frame2.Controls.Add(Me.Label1)
        Me.Frame2.Cursor = System.Windows.Forms.Cursors.Default
        Me.Frame2.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Frame2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Frame2.Location = New System.Drawing.Point(0, 0)
        Me.Frame2.Name = "Frame2"
        Me.Frame2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Frame2.Size = New System.Drawing.Size(89, 198)
        Me.Frame2.TabIndex = 1
        '
        'Label10
        '
        Me.Label10.BackColor = System.Drawing.Color.FromArgb(CType(122, Byte), CType(182, Byte), CType(215, Byte))
        Me.Label10.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label10.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.Color.FromArgb(CType(0, Byte), CType(102, Byte), CType(153, Byte))
        Me.Label10.Location = New System.Drawing.Point(0, 48)
        Me.Label10.Name = "Label10"
        Me.Label10.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label10.Size = New System.Drawing.Size(89, 21)
        Me.Label10.TabIndex = 3
        Me.Label10.Text = " PDF FILE"
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.Color.FromArgb(CType(122, Byte), CType(182, Byte), CType(215, Byte))
        Me.Label1.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label1.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.FromArgb(CType(0, Byte), CType(102, Byte), CType(153, Byte))
        Me.Label1.Location = New System.Drawing.Point(0, 96)
        Me.Label1.Name = "Label1"
        Me.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label1.Size = New System.Drawing.Size(89, 21)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = " Image List"
        '
        'TextLogo
        '
        Me.TextLogo.AcceptsReturn = True
        Me.TextLogo.AutoSize = False
        Me.TextLogo.BackColor = System.Drawing.Color.FromArgb(CType(255, Byte), CType(153, Byte), CType(102, Byte))
        Me.TextLogo.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextLogo.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.TextLogo.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextLogo.ForeColor = System.Drawing.Color.White
        Me.TextLogo.Location = New System.Drawing.Point(96, 8)
        Me.TextLogo.MaxLength = 0
        Me.TextLogo.Name = "TextLogo"
        Me.TextLogo.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.TextLogo.Size = New System.Drawing.Size(505, 25)
        Me.TextLogo.TabIndex = 0
        Me.TextLogo.Text = " 3-Heights PDF Extract Tool - Images"
        '
        'fileDialog
        '
        Me.fileDialog.Enabled = True
        Me.fileDialog.Location = New System.Drawing.Point(544, 168)
        Me.fileDialog.Name = "fileDialog"
        Me.fileDialog.OcxState = CType(resources.GetObject("fileDialog.OcxState"), System.Windows.Forms.AxHost.State)
        Me.fileDialog.Size = New System.Drawing.Size(32, 32)
        Me.fileDialog.TabIndex = 12
        '
        'Form1
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(608, 239)
        Me.Controls.Add(Me.Frame1)
        Me.Controls.Add(Me.Frame14)
        Me.Controls.Add(Me.Frame17)
        Me.Controls.Add(Me.Frame16)
        Me.Controls.Add(Me.Frame2)
        Me.Controls.Add(Me.TextLogo)
        Me.Controls.Add(Me.fileDialog)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Location = New System.Drawing.Point(4, 23)
        Me.Name = "Form1"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text = "Image Extraction"
        Me.Frame1.ResumeLayout(False)
        Me.Frame4.ResumeLayout(False)
        Me.Frame14.ResumeLayout(False)
        Me.Frame3.ResumeLayout(False)
        Me.Frame16.ResumeLayout(False)
        Me.Frame2.ResumeLayout(False)
        CType(Me.fileDialog, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
#End Region 
#Region "Upgrade Support "
	Private Shared m_vb6FormDefInstance As Form1
	Private Shared m_InitializingDefInstance As Boolean
	Public Shared Property DefInstance() As Form1
		Get
			If m_vb6FormDefInstance Is Nothing OrElse m_vb6FormDefInstance.IsDisposed Then
				m_InitializingDefInstance = True
				m_vb6FormDefInstance = New Form1()
				m_InitializingDefInstance = False
			End If
			DefInstance = m_vb6FormDefInstance
		End Get
		Set
			m_vb6FormDefInstance = Value
		End Set
	End Property
#End Region 
	' Extract and list images and their properties
	' --------------------------------------------
	'
    ' Visual Basic .NET sample for the 3-Heights PDF Extract Tool API
	' http://www.pdf-tools.com
	'
	' Copyright (C) 2005 PDF Tools AG, Switzerland
	' Permission to use, copy, modify, and distribute this
	' software and its documentation for any purpose and without
	' fee is hereby granted, provided that the above copyright
	' notice appear in all copies and that both that copyright
	' notice and this permission notice appear in supporting
	' documentation.  This software is provided "as is" without
	' express or implied warranty.
	
	Private Sub Extract_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles Extract.Click
		Dim eLZW As Object
		Dim res As Object
		Dim Page As Object
		Dim imagecount As Object
		
		Dim pdf As New PDFPARSERLib.Document
		Dim content As PDFPARSERLib.Content
		Dim image As PDFPARSERLib.Image
		Dim ctm As PDFPARSERLib.TransformMatrix
        Dim X, Width_Renamed, Height_Renamed, Y As Single
		Dim sColor As String
		
		' Open the input file
		If pdf.Open(txtInput.Text) Then
			
			' Clear Text field and reset counter
			txtList.Text = ""
			imagecount = 0
			
			' Loop through all pages
			For Page = 1 To pdf.PageCount
				
				' Set current page number
				pdf.PageNo = Page
				
				' Get the page content
				content = pdf.Page.content
				If Not (content Is Nothing) Then
					
					' Loop through all images
					Do 
						' Get the next image from the page
						image = content.GetNextImage
						If Not (image Is Nothing) Then
							imagecount = imagecount + 1
							
							' Get the Transformation Matrix
							ctm = content.GraphicsState.ctm
							res = image.GetResolution(ctm)
							
							' Get the coordinates and dimensions of the image
							' Calcualte Pythagoras in case of rotated images
							Width_Renamed = System.Math.Round(System.Math.Sqrt(ctm.a * ctm.a + ctm.b * ctm.b), 1)
							Height_Renamed = System.Math.Round(System.Math.Sqrt(ctm.d * ctm.d + ctm.c * ctm.c), 1)
							X = System.Math.Round(ctm.e, 1)
							Y = System.Math.Round(ctm.f, 1)
							
							' Check if it's color
							If image.ColorSpace.IsColor Then
								sColor = "color"
							ElseIf image.ColorSpace.IsMonochrome Then 
								If image.BitsPerComponent > 1 Then
									sColor = "greyscale"
								Else
									sColor = "bitonal"
								End If
							End If
							
							' Update the Text List
							txtList.Text = txtList.Text & "img: " & imagecount & ",  w=" & Width_Renamed & ",  h=" & Height_Renamed & ",  x=" & X & ",  y=" & Y & ",  page=" & Page & ", " & sColor & ", res=" & System.Math.Round(res, 0) & Chr(13) & Chr(10)
							
							' Store the image on a file
							image.ChangeOrientation(ctm.Orientation)
							image.Store(VB6.GetPath & "\out" & imagecount & ".tif", eLZW)
							
						Else
							Exit Do
						End If
					Loop 
				End If
			Next Page
			pdf.Close()
		Else
			MsgBox("Couldn't open input file")
		End If
	End Sub
	
	Private Sub OpenPDF_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles OpenPDF.Click
		' Open File Dialog
		fileDialog.FileName = txtInput.Text
		fileDialog.ShowOpen()
		txtInput.Text = fileDialog.FileName
	End Sub
End Class