/****************************************************************************
 *
 * File:            ImgExt.java
 *
 * Usage:           java ImgExt PDF-InputFile
 *
 * Description:     Extract Images and Image data of PDF-InputFile.
 *
 * Version:         1.02  (6-September-2004)
 *
 * Author:          Philip Renggli, PDF Tools AG   
 * 
 * Copyright:       Copyright (C) 2004 PDF Tools AG, Switzerland
 *                  Permission to use, copy, modify, and distribute this
 *                  software and its documentation for any purpose and without
 *                  fee is hereby granted, provided that the above copyright
 *                  notice appear in all copies and that both that copyright
 *                  notice and this permission notice appear in supporting
 *                  documentation.  This software is provided "as is" without
 *                  express or implied warranty.
 *
 ***************************************************************************/

import com.pdftools.expa.*;
import com.pdftools.*;

public class ImgExt {

public static void main(String[] args) {
    try {

        // open input file
        Document thePDF = new Document(args[0], "");
        
        // select 1st page
        thePDF.setPageNo(3);

        // get the content
        Content theContent = thePDF.getPage().getContent();
        while(theContent.getNextImage()!= null)
        {
            Image theImage = theContent.getImage();
            System.out.println("Image width = " + theImage.getWidth());
            System.out.println("Image height = " + theImage.getHeight());
            theImage.store("image.tif", NativeLibrary.COMPRESSION.eComprRaw);
        }
        
    } catch (Throwable e) {
        e.printStackTrace();
    }
}
}
