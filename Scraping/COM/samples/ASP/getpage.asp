<%@ Language=VBScript %>

<% 

    option explicit
	
    dim pdfDoc
    
    set pdfDoc = Server.CreateObject("PDFParser.Document")
    
    if not pdfDoc.Open(Server.Mappath("in_sample.pdf")) then
    	Response.Write "<p>"
    	Response.Write "Could not open  file." & "<br>"
    end if

    Response.Write "<p>"
    Response.Write "Number of pages: " & pdfDoc.PageCount & "<br>"
    Response.Write "</p>"

%>
