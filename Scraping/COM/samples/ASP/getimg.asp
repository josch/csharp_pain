<%@ Language=VBScript %>

<% 
    Dim pdfDoc
    Dim content
    Dim text
    set pdfDoc  = Server.CreateObject("PDFParser.Document")
    
    If not pdfDoc.Open(Server.Mappath("in_sample.pdf")) then
        Response.Write "<p>"
        Response.Write "Could not open  file." & "<br>"
    End if

    pdfDoc.PageNo = 1
    set content = pdfDoc.Page.content		
    If Not (content Is Nothing) Then
        for iImg = 1 to 10
            Set image = content.GetNextImage    
            If Not (image Is Nothing) Then
                Height  = image.Height
                imgName = "img" & iImg & ".jpg"
                image.ConvertToRGB
                image.Store Server.Mappath(imgName)
            Else
                Response.Write "<p>No more images" & "<br></p>"
                Exit For
            End If
            Set ctm = content.GraphicsState.ctm
            Width   = ctm.a
            Height  = ctm.d
            X       = ctm.e
            y       = ctm.f
            Response.Write "<p>Image " & iImg & ": (" & round(Height, 1) & " x " & round(Width, 1) & ") at (" & round(X, 1) & ", " & round(Y, 1) & ")<br>"
            Response.Write "<img border=0 src=" & imgName & " alt=" & imgName & "></p>"
        next
    Else
        Response.Write "<p>There is no content on this page" & "<br></p>"
    End If
%>
