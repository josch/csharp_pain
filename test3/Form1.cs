/*
 * Created by SharpDevelop.
 * User: windows
 * Date: 27.10.2004
 * Time: 17:19
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */

using System;
using System.Drawing;
using System.Windows.Forms;

namespace test3
{
	/// <summary>
	/// Description of Form1.
	/// </summary>
	public class Form1 : System.Windows.Forms.Form
	{
		private System.Windows.Forms.StatusBarPanel statusBarPanel1;
		private System.Windows.Forms.PictureBox pictureBox1;
		private System.Windows.Forms.Button ButtonCancel;
		private System.Windows.Forms.Button ButtonAbort;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Button ButtonOK;
		private System.Windows.Forms.ProgressBar progressBar1;
		private System.Windows.Forms.StatusBar statusBar1;
		public Form1()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
		}
		
		#region Windows Forms Designer generated code
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent() {
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(Form1));
			this.statusBar1 = new System.Windows.Forms.StatusBar();
			this.progressBar1 = new System.Windows.Forms.ProgressBar();
			this.ButtonOK = new System.Windows.Forms.Button();
			this.button1 = new System.Windows.Forms.Button();
			this.ButtonAbort = new System.Windows.Forms.Button();
			this.ButtonCancel = new System.Windows.Forms.Button();
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.statusBarPanel1 = new System.Windows.Forms.StatusBarPanel();
			((System.ComponentModel.ISupportInitialize)(this.statusBarPanel1)).BeginInit();
			this.SuspendLayout();
			// 
			// statusBar1
			// 
			this.statusBar1.Location = new System.Drawing.Point(0, 441);
			this.statusBar1.Name = "statusBar1";
			this.statusBar1.Panels.AddRange(new System.Windows.Forms.StatusBarPanel[] {
						this.statusBarPanel1});
			this.statusBar1.ShowPanels = true;
			this.statusBar1.Size = new System.Drawing.Size(682, 22);
			this.statusBar1.TabIndex = 6;
			this.statusBar1.Text = "statusBar1";
			// 
			// progressBar1
			// 
			this.progressBar1.Location = new System.Drawing.Point(96, 232);
			this.progressBar1.Name = "progressBar1";
			this.progressBar1.Size = new System.Drawing.Size(400, 32);
			this.progressBar1.Step = 1;
			this.progressBar1.TabIndex = 4;
			this.progressBar1.Value = 20;
			// 
			// ButtonOK
			// 
			this.ButtonOK.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.ButtonOK.Location = new System.Drawing.Point(496, 408);
			this.ButtonOK.Name = "ButtonOK";
			this.ButtonOK.Size = new System.Drawing.Size(80, 24);
			this.ButtonOK.TabIndex = 0;
			this.ButtonOK.Text = "Weiter >";
			this.ButtonOK.Click += new System.EventHandler(this.ButtonOKClick);
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(80, 168);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(144, 24);
			this.button1.TabIndex = 5;
			this.button1.Text = "Start";
			this.button1.Click += new System.EventHandler(this.Button1Click);
			// 
			// ButtonAbort
			// 
			this.ButtonAbort.DialogResult = System.Windows.Forms.DialogResult.Abort;
			this.ButtonAbort.Location = new System.Drawing.Point(592, 408);
			this.ButtonAbort.Name = "ButtonAbort";
			this.ButtonAbort.Size = new System.Drawing.Size(80, 24);
			this.ButtonAbort.TabIndex = 1;
			this.ButtonAbort.Text = "Abbrechen";
			this.ButtonAbort.Click += new System.EventHandler(this.ButtonAbortClick);
			// 
			// ButtonCancel
			// 
			this.ButtonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.ButtonCancel.Enabled = false;
			this.ButtonCancel.Location = new System.Drawing.Point(416, 408);
			this.ButtonCancel.Name = "ButtonCancel";
			this.ButtonCancel.Size = new System.Drawing.Size(80, 24);
			this.ButtonCancel.TabIndex = 2;
			this.ButtonCancel.Text = "< Zur�ck";
			// 
			// pictureBox1
			// 
			this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
			this.pictureBox1.Location = new System.Drawing.Point(0, 0);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(688, 80);
			this.pictureBox1.TabIndex = 3;
			this.pictureBox1.TabStop = false;
			// 
			// statusBarPanel1
			// 
			this.statusBarPanel1.Text = "statusBarPanel1";
			this.statusBarPanel1.ToolTipText = "StatusBar";
			this.statusBarPanel1.Width = 500;
			// 
			// Form1
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(682, 463);
			this.ControlBox = false;
			this.Controls.Add(this.statusBar1);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.progressBar1);
			this.Controls.Add(this.pictureBox1);
			this.Controls.Add(this.ButtonCancel);
			this.Controls.Add(this.ButtonAbort);
			this.Controls.Add(this.ButtonOK);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.Name = "Form1";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Schritt Eins";
			((System.ComponentModel.ISupportInitialize)(this.statusBarPanel1)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion
		

		void ButtonAbortClick(object sender, System.EventArgs e)
		{
		}
		
		void ButtonOKClick(object sender, System.EventArgs e)
		{
			Close();
		}
		
		void Button1Click(object sender, System.EventArgs e)
		{
			for(int i=progressBar1.Minimum; i<progressBar1.Maximum; i++)
			{
				progressBar1.Value = i;
				statusBarPanel1.Text = "Text: " + i;
				for(int j=1; j<10000000; j++);
			}
		}
		
	}
}
