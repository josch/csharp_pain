/*
 * Created by SharpDevelop.
 * User: windows
 * Date: 27.10.2004
 * Time: 17:12
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Drawing;
using System.Windows.Forms;

namespace test3
{
	/// <summary>
	/// Description of MainForm.	
	/// </summary>
	public class MainForm : System.Windows.Forms.Form
	{
		public MainForm()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			
			//
			// TODO: Add constructor code after the InitializeComponent() call.
			//
		}
		

		public static void openForm1()
		{
			Form1 myForm = new Form1();
			if(myForm.ShowDialog()==DialogResult.OK)
			{openForm2();}
		}
		
		public static void openForm2()
		{
			Form2 myForm = new Form2();
			
			DialogResult Result = myForm.ShowDialog();
			
			if(Result==DialogResult.OK)
			{openForm3();}
			else if(Result==DialogResult.Cancel)
			{openForm1();}
		}
		
		public static void openForm3()
		{
			Form3 myForm = new Form3();
			
			DialogResult Result = myForm.ShowDialog();
			
			if(Result==DialogResult.Cancel)
			{openForm2();}
		}
		
		[STAThread]
		public static void Main(string[] args)
		{
//			Application.Run(new MainForm());
//			Form1 myForm = new Form1();
//			if(myForm.ShowDialog()==DialogResult.OK)
//			{
//				Form2 myForm2 = new Form2();
//				myForm2.ShowDialog();
//			}
			
			openForm1();
			
		}
		
		#region Windows Forms Designer generated code
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent() {
			// 
			// MainForm
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(292, 266);
			this.Name = "MainForm";
			this.Text = "MainForm";
		}
		#endregion
	}
}
