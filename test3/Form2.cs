/*
 * Created by SharpDevelop.
 * User: windows
 * Date: 03.11.2004
 * Time: 13:01
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */

using System;
using System.Drawing;
using System.Windows.Forms;

namespace test3
{
	/// <summary>
	/// Description of Form2.
	/// </summary>
	public class Form2 : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Button ButtonOK;
		private System.Windows.Forms.Button ButtonCancel;
		private System.Windows.Forms.PictureBox pictureBox1;
		private System.Windows.Forms.Button ButtonAbort;
		public Form2()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			
			//
			// TODO: Add constructor code after the InitializeComponent() call.
			//
		}
		
		#region Windows Forms Designer generated code
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent() {
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(Form2));
			this.ButtonAbort = new System.Windows.Forms.Button();
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.ButtonCancel = new System.Windows.Forms.Button();
			this.ButtonOK = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// ButtonAbort
			// 
			this.ButtonAbort.DialogResult = System.Windows.Forms.DialogResult.Abort;
			this.ButtonAbort.Location = new System.Drawing.Point(592, 424);
			this.ButtonAbort.Name = "ButtonAbort";
			this.ButtonAbort.Size = new System.Drawing.Size(80, 24);
			this.ButtonAbort.TabIndex = 0;
			this.ButtonAbort.Text = "Abbrechen";
			this.ButtonAbort.Click += new System.EventHandler(this.ButtonAbortClick);
			// 
			// pictureBox1
			// 
			this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
			this.pictureBox1.Location = new System.Drawing.Point(0, 0);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(680, 80);
			this.pictureBox1.TabIndex = 1;
			this.pictureBox1.TabStop = false;
			// 
			// ButtonCancel
			// 
			this.ButtonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.ButtonCancel.Location = new System.Drawing.Point(416, 424);
			this.ButtonCancel.Name = "ButtonCancel";
			this.ButtonCancel.Size = new System.Drawing.Size(80, 24);
			this.ButtonCancel.TabIndex = 3;
			this.ButtonCancel.Text = "< Zur�ck";
			this.ButtonCancel.Click += new System.EventHandler(this.ButtonCancelClick);
			// 
			// ButtonOK
			// 
			this.ButtonOK.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.ButtonOK.Location = new System.Drawing.Point(496, 424);
			this.ButtonOK.Name = "ButtonOK";
			this.ButtonOK.Size = new System.Drawing.Size(80, 24);
			this.ButtonOK.TabIndex = 2;
			this.ButtonOK.Text = "Weiter >";
			this.ButtonOK.Click += new System.EventHandler(this.ButtonAbortClick);
			// 
			// Form2
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(682, 463);
			this.ControlBox = false;
			this.Controls.Add(this.ButtonCancel);
			this.Controls.Add(this.ButtonOK);
			this.Controls.Add(this.pictureBox1);
			this.Controls.Add(this.ButtonAbort);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.Name = "Form2";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Schritt Zwei";
			this.ResumeLayout(false);
		}
		#endregion
		void ButtonAbortClick(object sender, System.EventArgs e)
		{
		}
		
		void ButtonCancelClick(object sender, System.EventArgs e)
		{
			Close();
		}
		
		void TreeView1AfterSelect(object sender, System.Windows.Forms.TreeViewEventArgs e)
		{
			
		}
		
	}
}
