/*
 * Created by SharpDevelop.
 * User: Johannes
 * Date: 01.06.2005
 * Time: 19:10
 */
using System;
using System.Drawing;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace Chat_3
{
	/// <summary>
	/// Description of MainForm.
	/// </summary>
	public class MainForm : System.Windows.Forms.Form
	{
		private System.ComponentModel.IContainer components;
		private System.Windows.Forms.MenuItem menuItem5;
		private System.Windows.Forms.MenuItem menuItem4;
		private System.Windows.Forms.Button btnSend;
		private System.Windows.Forms.Button btnMenü;
		private System.Windows.Forms.NotifyIcon notifyIcon1;
		private System.Windows.Forms.ImageList Bilder;
		private System.Windows.Forms.ColumnHeader columnHeader1;
		private System.Windows.Forms.ColumnHeader columnHeader2;
		private System.Windows.Forms.RichTextBox txtSend;
		private System.Windows.Forms.ListView lstUser;
		private System.Windows.Forms.RichTextBox txtmsg;
		private System.Windows.Forms.ContextMenu Taskleiste;
		private System.Windows.Forms.MenuItem menuItem3;
		private System.Windows.Forms.MenuItem menuItem2;
		private System.Windows.Forms.MenuItem menuItem1;
		
		private Thread t1 = null;
		private Thread t2 = null;
		private Socket s = null;
		
		private Font font;
		private Color color;
		private string benutzer;
		private bool vordergrund;
		private string status;
		
		public MainForm()
		{
			InitializeComponent();
			
			font = new Font("Tahoma", 10, FontStyle.Bold);
			color = System.Drawing.Color.FromArgb(255, 0, 0);
			benutzer = "JoSch";
			vordergrund = false;
			status = "Online";
			
			t1 = new Thread(new ThreadStart(Identifizieren));
			t1.Start();
			t2 = new Thread(new ThreadStart(Empfangen));
			t2.Start();
		}
		
		[STAThread]
		public static void Main(string[] args)
		{
			Application.Run(new MainForm());
		}
		
		#region Windows Forms Designer generated code
		private void InitializeComponent() {
			this.components = new System.ComponentModel.Container();
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(MainForm));
			this.menuItem1 = new System.Windows.Forms.MenuItem();
			this.menuItem2 = new System.Windows.Forms.MenuItem();
			this.menuItem3 = new System.Windows.Forms.MenuItem();
			this.Taskleiste = new System.Windows.Forms.ContextMenu();
			this.txtmsg = new System.Windows.Forms.RichTextBox();
			this.lstUser = new System.Windows.Forms.ListView();
			this.txtSend = new System.Windows.Forms.RichTextBox();
			this.columnHeader2 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
			this.Bilder = new System.Windows.Forms.ImageList(this.components);
			this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
			this.btnMenü = new System.Windows.Forms.Button();
			this.btnSend = new System.Windows.Forms.Button();
			this.menuItem4 = new System.Windows.Forms.MenuItem();
			this.menuItem5 = new System.Windows.Forms.MenuItem();
			this.SuspendLayout();
			// 
			// menuItem1
			// 
			this.menuItem1.Index = 0;
			this.menuItem1.Text = "Chat 3 minimieren";
			this.menuItem1.Click += new System.EventHandler(this.Minimieren);
			// 
			// menuItem2
			// 
			this.menuItem2.Index = 1;
			this.menuItem2.Text = "Einstellungen";
			this.menuItem2.Click += new System.EventHandler(this.EinstellungenÖffnen);
			// 
			// menuItem3
			// 
			this.menuItem3.Index = 2;
			this.menuItem3.Text = "Chat 3 beenden";
			this.menuItem3.Click += new System.EventHandler(this.Beenden);
			// 
			// Taskleiste
			// 
			this.Taskleiste.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
						this.menuItem1,
						this.menuItem2,
						this.menuItem3,
						this.menuItem4,
						this.menuItem5});
			// 
			// txtmsg
			// 
			this.txtmsg.Location = new System.Drawing.Point(8, 8);
			this.txtmsg.Name = "txtmsg";
			this.txtmsg.ReadOnly = true;
			this.txtmsg.Size = new System.Drawing.Size(384, 352);
			this.txtmsg.TabIndex = 4;
			this.txtmsg.Text = "";
			// 
			// lstUser
			// 
			this.lstUser.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
						this.columnHeader1,
						this.columnHeader2});
			this.lstUser.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
			this.lstUser.Location = new System.Drawing.Point(400, 8);
			this.lstUser.Name = "lstUser";
			this.lstUser.Size = new System.Drawing.Size(144, 416);
			this.lstUser.SmallImageList = this.Bilder;
			this.lstUser.TabIndex = 7;
			this.lstUser.View = System.Windows.Forms.View.Details;
			// 
			// txtSend
			// 
			this.txtSend.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.txtSend.Location = new System.Drawing.Point(8, 368);
			this.txtSend.Name = "txtSend";
			this.txtSend.Size = new System.Drawing.Size(296, 64);
			this.txtSend.TabIndex = 0;
			this.txtSend.Text = "";
			this.txtSend.TextChanged += new System.EventHandler(this.TxtSendTextChanged);
			// 
			// columnHeader2
			// 
			this.columnHeader2.Text = "Status";
			// 
			// columnHeader1
			// 
			this.columnHeader1.Text = "Benutzer";
			this.columnHeader1.Width = 80;
			// 
			// Bilder
			// 
			this.Bilder.ImageSize = new System.Drawing.Size(16, 16);
			this.Bilder.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("Bilder.ImageStream")));
			this.Bilder.TransparentColor = System.Drawing.Color.Transparent;
			// 
			// notifyIcon1
			// 
			this.notifyIcon1.ContextMenu = this.Taskleiste;
			this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
			this.notifyIcon1.Text = "Chat 3";
			this.notifyIcon1.Visible = true;
			// 
			// btnMenü
			// 
			this.btnMenü.Location = new System.Drawing.Point(312, 408);
			this.btnMenü.Name = "btnMenü";
			this.btnMenü.Size = new System.Drawing.Size(80, 24);
			this.btnMenü.TabIndex = 5;
			this.btnMenü.Text = "Menü öffnen";
			this.btnMenü.Click += new System.EventHandler(this.BtnMenüClick);
			// 
			// btnSend
			// 
			this.btnSend.Enabled = false;
			this.btnSend.Location = new System.Drawing.Point(312, 368);
			this.btnSend.Name = "btnSend";
			this.btnSend.Size = new System.Drawing.Size(80, 24);
			this.btnSend.TabIndex = 1;
			this.btnSend.Text = "Senden";
			this.btnSend.Click += new System.EventHandler(this.BtnSendClick);
			// 
			// menuItem4
			// 
			this.menuItem4.Index = 3;
			this.menuItem4.Text = "In den Vordergrund";
			this.menuItem4.Click += new System.EventHandler(this.Vordergrund);
			// 
			// menuItem5
			// 
			this.menuItem5.Index = 4;
			this.menuItem5.Text = "Über...";
			this.menuItem5.Click += new System.EventHandler(this.ÜberÖffnen);
			// 
			// MainForm
			// 
			this.AcceptButton = this.btnSend;
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(552, 437);
			this.Controls.Add(this.lstUser);
			this.Controls.Add(this.txtSend);
			this.Controls.Add(this.btnMenü);
			this.Controls.Add(this.txtmsg);
			this.Controls.Add(this.btnSend);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "MainForm";
			this.ShowInTaskbar = false;
			this.Text = "Chat 3";
			this.Closed += new System.EventHandler(this.MainFormClosed);
			this.ResumeLayout(false);
		}
		#endregion
		
		#region Network Functions
		void Empfangen()
		{
			s = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
			s.Bind(new IPEndPoint(IPAddress.Any, 4378));
			
			string temp = "";
			
			while(true)
			{
				byte[] buffer = new byte[256];
				s.Receive(buffer);
				string received = Encoding.Unicode.GetString(buffer);
				buffer = null;
				
				string[] token = received.Split(new char[] {'‼'});
				if(token[0] == "Nachricht")
				{
					if(token[2]!=temp)
					{
						txtmsg.SelectionFont = font;
						txtmsg.SelectionColor = System.Drawing.Color.FromArgb(int.Parse(token[4]),
						                                                      int.Parse(token[5]),
						                                                      int.Parse(token[6]));
						
						txtmsg.SelectedText = token[1];
						txtmsg.ScrollToCaret();
						
						txtmsg.SelectionFont = font;
						txtmsg.SelectionColor = System.Drawing.Color.FromArgb(0, 0, 0);
						txtmsg.SelectedText = " ("+token[2]+"): "+token[3];
						txtmsg.SelectedText = "\r\n";
						txtmsg.ScrollToCaret();
						temp = token[2];
					}
				}
				else if(token[0] == "Status")
				{
					bool isfound = false;
					foreach(ListViewItem i in lstUser.Items)
					{
						TimeSpan timeout = DateTime.Now - DateTime.Parse(i.SubItems[2].Text);
						
						if(timeout.TotalMilliseconds < 10000)
						{
							if(token[1]==i.Text)
							{
								i.SubItems[1].Text = token[3];
								i.SubItems[2].Text = token[2];
								isfound = true;
							}
						}
						else
						{
							i.Remove();
						}
					}
					
					if(!isfound)
					{
						int count = lstUser.Items.Add(token[1]).Index;
						lstUser.Items[count].SubItems.Add(token[3]);
						lstUser.Items[count].SubItems.Add(token[2]);
						lstUser.Items[count].ImageIndex = 0;
					}
				}
			}
		}
		
		void Senden()
		{
			UdpClient client = new UdpClient();
			IPEndPoint ipepbc = new IPEndPoint(IPAddress.Broadcast, 4378);
			byte[] msg = Encoding.Unicode.GetBytes("Nachricht"+"‼"+benutzer+"‼"+DateTime.Now.ToLongTimeString()
			                                       +"‼"+txtSend.Text+"‼"+color.R.ToString()
			                                       +"‼"+color.G.ToString()+"‼"+color.B.ToString());
			if(msg.Length < 256)
			{
				client.Send(msg, msg.Length, ipepbc);
				txtSend.Text = "";
			}
			else
			{
				MessageBox.Show("Nachricht zu lang!");
			}
			client.Close();
		}
		
		void Identifizieren()
		{
			UdpClient client = new UdpClient();
			IPEndPoint ipepbc = new IPEndPoint(IPAddress.Broadcast, 4378);
			
			while(true)
			{
				byte[] msg = Encoding.Unicode.GetBytes("Status"+"‼"+benutzer+"‼"
				                                       +DateTime.Now.ToLongTimeString()
				                                       +"‼"+status);
				client.Send(msg, msg.Length, ipepbc);
				Thread.Sleep(2000);
			}
		}
		#endregion
		
		#region Eventhandler Functions
		void MainFormClosed(object sender, System.EventArgs e)
		{
			if(s != null)
			{
				s.Close();
			}
			if(t1!=null)
			{
				t1.Abort();
			}
			if(t2!=null)
			{
				t2.Abort();
			}
			notifyIcon1.Dispose();
		}
		
		void TxtSendTextChanged(object sender, System.EventArgs e)
		{
			btnSend.Enabled = (txtSend.Text.Length > 0);
			status = "Schreibt";
		}
		
		void BtnSendClick(object sender, System.EventArgs e)
		{
			Senden();
			status = "Online";
		}
		
		void BtnMenüClick(object sender, System.EventArgs e)
		{
			Taskleiste.Show(btnMenü, new System.Drawing.Point(0, btnMenü.Height));
		}
		#endregion
		
		#region Menu Functions
		void Minimieren(object sender, System.EventArgs e)
		{
			if(this.WindowState == FormWindowState.Minimized)
			{
				this.Visible = true;
				this.WindowState = FormWindowState.Normal;
				this.menuItem1.Text = "Chat 3 minimieren";
			}
			else if(this.WindowState == FormWindowState.Normal)
			{
				this.WindowState = FormWindowState.Minimized;
				this.Visible = false;
				this.menuItem1.Text = "Chat 3 wiederherstellen";
			}
			else
			{
				MessageBox.Show(this.WindowState.ToString());
			}
		}
		
		void ÜberÖffnen(object sender, System.EventArgs e)
		{
			ÜberForm über = new ÜberForm();
			über.TopMost = this.TopMost;
			über.ShowDialog();
			über.Dispose();
		}
		
		void EinstellungenÖffnen(object sender, System.EventArgs e)
		{
			EinstellungenForm einst = new EinstellungenForm(font, color, benutzer, vordergrund);
			einst.TopMost = this.TopMost;
			if(einst.ShowDialog()==DialogResult.OK)
			{
				font = einst.font;
				color = einst.color;
				benutzer = einst.benutzer;
				vordergrund = einst.vordergrund;
			}
			einst.Dispose();
			
			this.TopMost = vordergrund;
		}
		
		void Vordergrund(object sender, System.EventArgs e)
		{
			this.BringToFront();
		}
		
		void Beenden(object sender, System.EventArgs e)
		{
			this.Close();
		}
		#endregion
		
		
	}
}
