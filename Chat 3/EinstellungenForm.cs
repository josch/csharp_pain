/*
 * Created by SharpDevelop.
 * User: Johannes
 * Date: 02.06.2005
 * Time: 21:07
 */

using System;
using System.Drawing;
using System.Windows.Forms;

namespace Chat_3
{
	/// <summary>
	/// Description of EinstellungenForm.
	/// </summary>
	public class EinstellungenForm : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Button btnOK;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button btnSchriftart;
		private System.Windows.Forms.FontDialog fontDialog1;
		private System.Windows.Forms.CheckBox cbVordergrund;
		private System.Windows.Forms.RichTextBox txtTest;
		private System.Windows.Forms.Button btnAbbrechen;
		private System.Windows.Forms.TextBox txtBenutzer;
		
		public Font font;
		public Color color;
		public string benutzer;
		public bool vordergrund;
		
		public EinstellungenForm(Font f, Color c, string b, bool v)
		{
			InitializeComponent();
			fontDialog1.Font = f;
			fontDialog1.Color = c;
			txtBenutzer.Text = b;
			cbVordergrund.Checked = v;
			Aktualisiere();
			
		}
		
		#region Windows Forms Designer generated code
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent() {
			this.txtBenutzer = new System.Windows.Forms.TextBox();
			this.btnAbbrechen = new System.Windows.Forms.Button();
			this.txtTest = new System.Windows.Forms.RichTextBox();
			this.cbVordergrund = new System.Windows.Forms.CheckBox();
			this.fontDialog1 = new System.Windows.Forms.FontDialog();
			this.btnSchriftart = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.btnOK = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// txtBenutzer
			// 
			this.txtBenutzer.Location = new System.Drawing.Point(120, 16);
			this.txtBenutzer.Name = "txtBenutzer";
			this.txtBenutzer.Size = new System.Drawing.Size(120, 20);
			this.txtBenutzer.TabIndex = 2;
			this.txtBenutzer.Text = "JoSch";
			this.txtBenutzer.TextChanged += new System.EventHandler(this.TxtBenutzerTextChanged);
			// 
			// btnAbbrechen
			// 
			this.btnAbbrechen.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnAbbrechen.Location = new System.Drawing.Point(136, 192);
			this.btnAbbrechen.Name = "btnAbbrechen";
			this.btnAbbrechen.TabIndex = 1;
			this.btnAbbrechen.Text = "A&bbrechen";
			// 
			// txtTest
			// 
			this.txtTest.Location = new System.Drawing.Point(16, 88);
			this.txtTest.Name = "txtTest";
			this.txtTest.ReadOnly = true;
			this.txtTest.Size = new System.Drawing.Size(192, 40);
			this.txtTest.TabIndex = 7;
			this.txtTest.Text = "";
			// 
			// cbVordergrund
			// 
			this.cbVordergrund.Location = new System.Drawing.Point(40, 136);
			this.cbVordergrund.Name = "cbVordergrund";
			this.cbVordergrund.Size = new System.Drawing.Size(168, 24);
			this.cbVordergrund.TabIndex = 10;
			this.cbVordergrund.Text = "Immer im Vordergrund";
			// 
			// fontDialog1
			// 
			this.fontDialog1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.fontDialog1.ShowColor = true;
			// 
			// btnSchriftart
			// 
			this.btnSchriftart.Location = new System.Drawing.Point(224, 96);
			this.btnSchriftart.Name = "btnSchriftart";
			this.btnSchriftart.TabIndex = 8;
			this.btnSchriftart.Text = "Schriftart";
			this.btnSchriftart.Click += new System.EventHandler(this.BtnSchriftartClick);
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(24, 16);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(88, 24);
			this.label1.TabIndex = 3;
			this.label1.Text = "Benutzername:";
			this.label1.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(32, 48);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(272, 40);
			this.label2.TabIndex = 9;
			this.label2.Text = "Die eingestellte Farbe ist für alle sichtbar, die ausgewählte Schriftart erschein" +
"t nur bei dir.";
			// 
			// btnOK
			// 
			this.btnOK.Location = new System.Drawing.Point(224, 192);
			this.btnOK.Name = "btnOK";
			this.btnOK.TabIndex = 0;
			this.btnOK.Text = "&OK";
			this.btnOK.Click += new System.EventHandler(this.BtnOKClick);
			// 
			// EinstellungenForm
			// 
			this.AcceptButton = this.btnOK;
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.CancelButton = this.btnAbbrechen;
			this.ClientSize = new System.Drawing.Size(314, 231);
			this.Controls.Add(this.cbVordergrund);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.btnSchriftart);
			this.Controls.Add(this.txtTest);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.txtBenutzer);
			this.Controls.Add(this.btnAbbrechen);
			this.Controls.Add(this.btnOK);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.KeyPreview = true;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "EinstellungenForm";
			this.Text = "Einstellungen";
			this.ResumeLayout(false);
		}
		#endregion
		
		void BtnOKClick(object sender, System.EventArgs e)
		{
			this.DialogResult = DialogResult.OK;
			font = fontDialog1.Font;
			color = fontDialog1.Color;
			benutzer = txtBenutzer.Text;
			vordergrund = cbVordergrund.Checked;
			this.Close();
		}
		
		void TxtBenutzerTextChanged(object sender, System.EventArgs e)
		{
			Aktualisiere();
		}
		
		void Aktualisiere()
		{
			txtTest.Text = "";
			txtTest.SelectionFont = fontDialog1.Font;
			txtTest.SelectionColor = fontDialog1.Color;
			txtTest.SelectedText = txtBenutzer.Text;
			txtTest.SelectionFont = new Font("Tahoma", 10, FontStyle.Bold);
			txtTest.SelectionColor = System.Drawing.Color.FromArgb(0, 0, 0);
			txtTest.SelectedText = " (13:24:06): hiho!";
		}
		
		void BtnSchriftartClick(object sender, System.EventArgs e)
		{
			if(fontDialog1.ShowDialog()==DialogResult.OK)
			{
				Aktualisiere();
			}
		}
	}
}
