/*
 * Created by SharpDevelop.
 * User: windows
 * Date: 24.01.2005
 * Time: 14:01
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Windows.Forms;

namespace fraktal
{
	/// <summary>
	/// Description of MainForm.	
	/// </summary>
	public class MainForm : System.Windows.Forms.Form
	{
		public MainForm()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			
		}
		
		[STAThread]
		public static void Main(string[] args)
		{
			Application.Run(new MainForm());
		}
		
		#region Windows Forms Designer generated code
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent() {
			// 
			// MainForm
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(512, 517);
			this.Name = "MainForm";
			this.Text = "MainForm";
			this.Paint += new System.Windows.Forms.PaintEventHandler(this.MainFormPaint);
		}
		#endregion
		
		void MainFormPaint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			for(int k=1;k<=64;k++)
			{
			string filename = "c:\\Bit"+k+".bmp";
			Bitmap bit = new Bitmap(filename);
			Bitmap frakt = new Bitmap("c:\\Frakt.bmp");
			
			int[] Drehung = SetType(bit);
			
			int width = 4;
			
			while(width<512)
			{
				for(int y = 0; y<width; y++)
				{
					for(int x=0; x<width; x++)
					{
						if(Drehung[0]==0)
						{
							frakt.SetPixel(x, y+width, bit.GetPixel(x, y)); //unten links |__
						}
						else if(Drehung[0]==1)
						{
							frakt.SetPixel(width-y-1, x+width, bit.GetPixel(x, y)); //unten links |**
						}
						else if(Drehung[0]==2)
						{
							frakt.SetPixel(width-x-1, ((2*width)-y-1), bit.GetPixel(x, y)); //unten links **|
						}
						else if(Drehung[0]==3)
						{
							frakt.SetPixel(y, ((2*width)-x-1) ,bit.GetPixel(x, y)); //unten links __|
						}
						
						if(Drehung[1]==0)
						{
							frakt.SetPixel(x+width, y+width, bit.GetPixel(x, y)); //unten rechts |__
						}
						else if(Drehung[1]==1)
						{
							frakt.SetPixel((2*width-y-1), x+width, bit.GetPixel(x, y)); //unten rechts |**
						}
						else if(Drehung[1]==2)
						{
							frakt.SetPixel((2*width-x-1), ((2*width)-y-1), bit.GetPixel(x, y)); //unten rechts **|
						}
						else if(Drehung[1]==3)
						{
							frakt.SetPixel(y+width, ((2*width)-x-1) ,bit.GetPixel(x, y)); //unten rechts __|
						}
						
						if(Drehung[2]==0)
						{
							frakt.SetPixel(x, y, bit.GetPixel(x, y)); //oben links |__
						}
						else if(Drehung[2]==1)
						{
							frakt.SetPixel(width-y-1, x, bit.GetPixel(x, y)); //oben links |**
						}
						else if(Drehung[2]==2)
						{
							frakt.SetPixel(width-x-1, width-y-1, bit.GetPixel(x, y)); //oben links **|
						}
						else if(Drehung[2]==3)
						{
							frakt.SetPixel(y, width-x-1, bit.GetPixel(x, y)); //oben links __|
						}
					}
				}
				
				for(int y=0; y<(width*2);y++)
				{
					for(int x=0; x<(width*2);x++)
					{
						bit.SetPixel(x, y, frakt.GetPixel(x, y));
					}
				}
				
				width *= 2;
			}
			
			e.Graphics.DrawImage(frakt, 0, 0, frakt.Width, frakt.Height);
			
			string filename2 = "c:\\foo"+k+".bmp";
			
			bit.Save(filename2);
			}
			
		}
		
		int[] SetType(Bitmap bit)
		{
			int[] Drehung = {0,0,0};
			
			Color white = bit.GetPixel(2,0);
			
			if(bit.GetPixel(0,2)==white)
			{
				Drehung[0]=3;
			}
			else if(bit.GetPixel(1,2)==white)
			{
				Drehung[0]=0;
			}
			else if(bit.GetPixel(0,3)==white)
			{
				Drehung[0]=2;
			}
			else if(bit.GetPixel(1,3)==white)
			{
				Drehung[0]=1;
			}
			
			if(bit.GetPixel(2,2)==white)
			{
				Drehung[1]=3;
			}
			else if(bit.GetPixel(3,2)==white)
			{
				Drehung[1]=0;
			}
			else if(bit.GetPixel(2,3)==white)
			{
				Drehung[1]=2;
			}
			else if(bit.GetPixel(3,3)==white)
			{
				Drehung[1]=1;
			}
			
			if(bit.GetPixel(0,0)==white)
			{
				Drehung[2]=3;
			}
			else if(bit.GetPixel(1,0)==white)
			{
				Drehung[2]=0;
			}
			else if(bit.GetPixel(0,1)==white)
			{
				Drehung[2]=2;
			}
			else if(bit.GetPixel(1,1)==white)
			{
				Drehung[2]=1;
			}
			
			return Drehung;
		}
	}
}
