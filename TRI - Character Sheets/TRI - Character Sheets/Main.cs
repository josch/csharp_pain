using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Xml;

namespace TRI___Character_Sheets
{
    public partial class Main : Form
    {
        public Main()
        {
            InitializeComponent();

            FertigkeitenbaumLaden();
        }

        private void FertigkeitenbaumLaden()
        {
            XmlTextReader reader = new XmlTextReader("Fertigkeiten.xml");
            reader.ReadStartElement();
            cbFertigkeiten.Items.Add(reader.ReadElementString("Gruppen"));
        }
    }
}