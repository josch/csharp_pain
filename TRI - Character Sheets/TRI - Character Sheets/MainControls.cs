using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace TRI___Character_Sheets
{
    partial class Main
    {
        private void BerechneVerbleibendeNebelpunkte(object sender, EventArgs e)
        {
            int iVerbleibendePunkte = 10 - (int)nUDSuni.Value - (int)nUDSihir.Value - (int)nUDSans.Value;
            lVerbleibendeNebelpunkte.ForeColor = (iVerbleibendePunkte < 0) ? Color.Red : Color.Black;
            lVerbleibendeNebelpunkte.Text = "Verbleibende Punkte: " + iVerbleibendePunkte.ToString();
        }
    }
}