namespace TRI___Character_Sheets
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.panel7 = new System.Windows.Forms.Panel();
            this.numericUpDown2 = new System.Windows.Forms.NumericUpDown();
            this.label34 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.numericUpDown9 = new System.Windows.Forms.NumericUpDown();
            this.label13 = new System.Windows.Forms.Label();
            this.numericUpDown8 = new System.Windows.Forms.NumericUpDown();
            this.label14 = new System.Windows.Forms.Label();
            this.numericUpDown7 = new System.Windows.Forms.NumericUpDown();
            this.label15 = new System.Windows.Forms.Label();
            this.numericUpDown6 = new System.Windows.Forms.NumericUpDown();
            this.label16 = new System.Windows.Forms.Label();
            this.numericUpDown5 = new System.Windows.Forms.NumericUpDown();
            this.label17 = new System.Windows.Forms.Label();
            this.numericUpDown4 = new System.Windows.Forms.NumericUpDown();
            this.label18 = new System.Windows.Forms.Label();
            this.numericUpDown3 = new System.Windows.Forms.NumericUpDown();
            this.label19 = new System.Windows.Forms.Label();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.panel9 = new System.Windows.Forms.Panel();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.numericUpDown22 = new System.Windows.Forms.NumericUpDown();
            this.label43 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.numericUpDown23 = new System.Windows.Forms.NumericUpDown();
            this.label45 = new System.Windows.Forms.Label();
            this.numericUpDown24 = new System.Windows.Forms.NumericUpDown();
            this.label46 = new System.Windows.Forms.Label();
            this.numericUpDown25 = new System.Windows.Forms.NumericUpDown();
            this.label47 = new System.Windows.Forms.Label();
            this.numericUpDown26 = new System.Windows.Forms.NumericUpDown();
            this.label48 = new System.Windows.Forms.Label();
            this.numericUpDown27 = new System.Windows.Forms.NumericUpDown();
            this.label49 = new System.Windows.Forms.Label();
            this.numericUpDown28 = new System.Windows.Forms.NumericUpDown();
            this.label50 = new System.Windows.Forms.Label();
            this.numericUpDown29 = new System.Windows.Forms.NumericUpDown();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.numericUpDown38 = new System.Windows.Forms.NumericUpDown();
            this.label59 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.numericUpDown39 = new System.Windows.Forms.NumericUpDown();
            this.label61 = new System.Windows.Forms.Label();
            this.numericUpDown40 = new System.Windows.Forms.NumericUpDown();
            this.label62 = new System.Windows.Forms.Label();
            this.numericUpDown41 = new System.Windows.Forms.NumericUpDown();
            this.label63 = new System.Windows.Forms.Label();
            this.numericUpDown42 = new System.Windows.Forms.NumericUpDown();
            this.label64 = new System.Windows.Forms.Label();
            this.numericUpDown43 = new System.Windows.Forms.NumericUpDown();
            this.label65 = new System.Windows.Forms.Label();
            this.numericUpDown44 = new System.Windows.Forms.NumericUpDown();
            this.label66 = new System.Windows.Forms.Label();
            this.numericUpDown45 = new System.Windows.Forms.NumericUpDown();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.numericUpDown30 = new System.Windows.Forms.NumericUpDown();
            this.label51 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.numericUpDown31 = new System.Windows.Forms.NumericUpDown();
            this.label53 = new System.Windows.Forms.Label();
            this.numericUpDown32 = new System.Windows.Forms.NumericUpDown();
            this.label54 = new System.Windows.Forms.Label();
            this.numericUpDown33 = new System.Windows.Forms.NumericUpDown();
            this.label55 = new System.Windows.Forms.Label();
            this.numericUpDown34 = new System.Windows.Forms.NumericUpDown();
            this.label56 = new System.Windows.Forms.Label();
            this.numericUpDown35 = new System.Windows.Forms.NumericUpDown();
            this.label57 = new System.Windows.Forms.Label();
            this.numericUpDown36 = new System.Windows.Forms.NumericUpDown();
            this.label58 = new System.Windows.Forms.Label();
            this.numericUpDown37 = new System.Windows.Forms.NumericUpDown();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.numericUpDown14 = new System.Windows.Forms.NumericUpDown();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.numericUpDown15 = new System.Windows.Forms.NumericUpDown();
            this.label37 = new System.Windows.Forms.Label();
            this.numericUpDown16 = new System.Windows.Forms.NumericUpDown();
            this.label38 = new System.Windows.Forms.Label();
            this.numericUpDown17 = new System.Windows.Forms.NumericUpDown();
            this.label39 = new System.Windows.Forms.Label();
            this.numericUpDown18 = new System.Windows.Forms.NumericUpDown();
            this.label40 = new System.Windows.Forms.Label();
            this.numericUpDown19 = new System.Windows.Forms.NumericUpDown();
            this.label41 = new System.Windows.Forms.Label();
            this.numericUpDown20 = new System.Windows.Forms.NumericUpDown();
            this.label42 = new System.Windows.Forms.Label();
            this.numericUpDown21 = new System.Windows.Forms.NumericUpDown();
            this.label24 = new System.Windows.Forms.Label();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.panel5 = new System.Windows.Forms.Panel();
            this.numericUpDown10 = new System.Windows.Forms.NumericUpDown();
            this.comboBox5 = new System.Windows.Forms.ComboBox();
            this.comboBox4 = new System.Windows.Forms.ComboBox();
            this.cbFertigkeiten = new System.Windows.Forms.ComboBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.label21 = new System.Windows.Forms.Label();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.label20 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.listBox2 = new System.Windows.Forms.ListBox();
            this.panel6 = new System.Windows.Forms.Panel();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.label23 = new System.Windows.Forms.Label();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this.label68 = new System.Windows.Forms.Label();
            this.label69 = new System.Windows.Forms.Label();
            this.label70 = new System.Windows.Forms.Label();
            this.label71 = new System.Windows.Forms.Label();
            this.label72 = new System.Windows.Forms.Label();
            this.label73 = new System.Windows.Forms.Label();
            this.label74 = new System.Windows.Forms.Label();
            this.label75 = new System.Windows.Forms.Label();
            this.label76 = new System.Windows.Forms.Label();
            this.label77 = new System.Windows.Forms.Label();
            this.label78 = new System.Windows.Forms.Label();
            this.label79 = new System.Windows.Forms.Label();
            this.label80 = new System.Windows.Forms.Label();
            this.label81 = new System.Windows.Forms.Label();
            this.label82 = new System.Windows.Forms.Label();
            this.label83 = new System.Windows.Forms.Label();
            this.label84 = new System.Windows.Forms.Label();
            this.label85 = new System.Windows.Forms.Label();
            this.label86 = new System.Windows.Forms.Label();
            this.label87 = new System.Windows.Forms.Label();
            this.label88 = new System.Windows.Forms.Label();
            this.label89 = new System.Windows.Forms.Label();
            this.label90 = new System.Windows.Forms.Label();
            this.label91 = new System.Windows.Forms.Label();
            this.label92 = new System.Windows.Forms.Label();
            this.label93 = new System.Windows.Forms.Label();
            this.panel8 = new System.Windows.Forms.Panel();
            this.nUDSans = new System.Windows.Forms.NumericUpDown();
            this.nUDSuni = new System.Windows.Forms.NumericUpDown();
            this.nUDSihir = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.lVerbleibendeNebelpunkte = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown3)).BeginInit();
            this.tabPage4.SuspendLayout();
            this.panel9.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown29)).BeginInit();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown43)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown44)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown45)).BeginInit();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown37)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown21)).BeginInit();
            this.tabPage5.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown10)).BeginInit();
            this.panel3.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nUDSans)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nUDSuni)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nUDSihir)).BeginInit();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(612, 25);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // tabControl1
            // 
            this.tabControl1.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tabPage6);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 25);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(612, 434);
            this.tabControl1.TabIndex = 1;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.panel1);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(604, 405);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Allgemein";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.Controls.Add(this.textBox6);
            this.panel1.Controls.Add(this.label30);
            this.panel1.Controls.Add(this.textBox5);
            this.panel1.Controls.Add(this.label29);
            this.panel1.Controls.Add(this.textBox4);
            this.panel1.Controls.Add(this.label22);
            this.panel1.Controls.Add(this.textBox3);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.textBox2);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(598, 399);
            this.panel1.TabIndex = 8;
            // 
            // textBox6
            // 
            this.textBox6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox6.Dock = System.Windows.Forms.DockStyle.Top;
            this.textBox6.Location = new System.Drawing.Point(0, 466);
            this.textBox6.Multiline = true;
            this.textBox6.Name = "textBox6";
            this.textBox6.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox6.Size = new System.Drawing.Size(582, 49);
            this.textBox6.TabIndex = 18;
            // 
            // label30
            // 
            this.label30.Dock = System.Windows.Forms.DockStyle.Top;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(0, 450);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(582, 16);
            this.label30.TabIndex = 17;
            this.label30.Text = "Erscheinung Ion";
            this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox5
            // 
            this.textBox5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox5.Dock = System.Windows.Forms.DockStyle.Top;
            this.textBox5.Location = new System.Drawing.Point(0, 401);
            this.textBox5.Multiline = true;
            this.textBox5.Name = "textBox5";
            this.textBox5.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox5.Size = new System.Drawing.Size(582, 49);
            this.textBox5.TabIndex = 16;
            // 
            // label29
            // 
            this.label29.Dock = System.Windows.Forms.DockStyle.Top;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(0, 385);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(582, 16);
            this.label29.TabIndex = 15;
            this.label29.Text = "Erscheinung Rulegard";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox4
            // 
            this.textBox4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox4.Dock = System.Windows.Forms.DockStyle.Top;
            this.textBox4.Location = new System.Drawing.Point(0, 336);
            this.textBox4.Multiline = true;
            this.textBox4.Name = "textBox4";
            this.textBox4.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox4.Size = new System.Drawing.Size(582, 49);
            this.textBox4.TabIndex = 14;
            // 
            // label22
            // 
            this.label22.Dock = System.Windows.Forms.DockStyle.Top;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(0, 320);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(582, 16);
            this.label22.TabIndex = 13;
            this.label22.Text = "Erscheinung Terra";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox3
            // 
            this.textBox3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox3.Dock = System.Windows.Forms.DockStyle.Top;
            this.textBox3.Location = new System.Drawing.Point(0, 271);
            this.textBox3.Multiline = true;
            this.textBox3.Name = "textBox3";
            this.textBox3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox3.Size = new System.Drawing.Size(582, 49);
            this.textBox3.TabIndex = 12;
            // 
            // label6
            // 
            this.label6.Dock = System.Windows.Forms.DockStyle.Top;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(0, 255);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(582, 16);
            this.label6.TabIndex = 11;
            this.label6.Text = "Erscheinung Allgemein";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox2
            // 
            this.textBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.textBox2.Location = new System.Drawing.Point(0, 144);
            this.textBox2.Multiline = true;
            this.textBox2.Name = "textBox2";
            this.textBox2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox2.Size = new System.Drawing.Size(582, 111);
            this.textBox2.TabIndex = 10;
            // 
            // label5
            // 
            this.label5.Dock = System.Windows.Forms.DockStyle.Top;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(0, 128);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(582, 16);
            this.label5.TabIndex = 9;
            this.label5.Text = "Hintergrund";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel2
            // 
            this.panel2.AutoScroll = true;
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.textBox1);
            this.panel2.Controls.Add(this.numericUpDown1);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.comboBox1);
            this.panel2.Controls.Add(this.comboBox2);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(582, 128);
            this.panel2.TabIndex = 8;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(5, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Name:";
            // 
            // textBox1
            // 
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox1.Location = new System.Drawing.Point(86, 7);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(182, 20);
            this.textBox1.TabIndex = 4;
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numericUpDown1.Location = new System.Drawing.Point(86, 87);
            this.numericUpDown1.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.numericUpDown1.Minimum = new decimal(new int[] {
            16,
            0,
            0,
            0});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(40, 20);
            this.numericUpDown1.TabIndex = 7;
            this.numericUpDown1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.numericUpDown1.Value = new decimal(new int[] {
            16,
            0,
            0,
            0});
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(5, 89);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(37, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Alter:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(5, 36);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Heimatwelt:";
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Terra",
            "Rulegard",
            "Ion"});
            this.comboBox1.Location = new System.Drawing.Point(86, 33);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(86, 21);
            this.comboBox1.TabIndex = 5;
            // 
            // comboBox2
            // 
            this.comboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Items.AddRange(new object[] {
            "M�nnlich",
            "Weiblich"});
            this.comboBox2.Location = new System.Drawing.Point(86, 60);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(86, 21);
            this.comboBox2.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(5, 63);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(75, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Geschlecht:";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.panel8);
            this.tabPage2.Controls.Add(this.lVerbleibendeNebelpunkte);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Size = new System.Drawing.Size(604, 405);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Nebelattribute";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.panel7);
            this.tabPage3.Controls.Add(this.label19);
            this.tabPage3.Location = new System.Drawing.Point(4, 25);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(604, 405);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Grundattribute";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // panel7
            // 
            this.panel7.AutoScroll = true;
            this.panel7.Controls.Add(this.label90);
            this.panel7.Controls.Add(this.label91);
            this.panel7.Controls.Add(this.label92);
            this.panel7.Controls.Add(this.label93);
            this.panel7.Controls.Add(this.label86);
            this.panel7.Controls.Add(this.label87);
            this.panel7.Controls.Add(this.label88);
            this.panel7.Controls.Add(this.label89);
            this.panel7.Controls.Add(this.label82);
            this.panel7.Controls.Add(this.label83);
            this.panel7.Controls.Add(this.label84);
            this.panel7.Controls.Add(this.label85);
            this.panel7.Controls.Add(this.label78);
            this.panel7.Controls.Add(this.label79);
            this.panel7.Controls.Add(this.label80);
            this.panel7.Controls.Add(this.label81);
            this.panel7.Controls.Add(this.label74);
            this.panel7.Controls.Add(this.label75);
            this.panel7.Controls.Add(this.label76);
            this.panel7.Controls.Add(this.label77);
            this.panel7.Controls.Add(this.label70);
            this.panel7.Controls.Add(this.label71);
            this.panel7.Controls.Add(this.label72);
            this.panel7.Controls.Add(this.label73);
            this.panel7.Controls.Add(this.label7);
            this.panel7.Controls.Add(this.label67);
            this.panel7.Controls.Add(this.label68);
            this.panel7.Controls.Add(this.label69);
            this.panel7.Controls.Add(this.numericUpDown2);
            this.panel7.Controls.Add(this.label34);
            this.panel7.Controls.Add(this.label32);
            this.panel7.Controls.Add(this.label33);
            this.panel7.Controls.Add(this.label31);
            this.panel7.Controls.Add(this.label28);
            this.panel7.Controls.Add(this.label27);
            this.panel7.Controls.Add(this.label26);
            this.panel7.Controls.Add(this.label25);
            this.panel7.Controls.Add(this.label11);
            this.panel7.Controls.Add(this.label12);
            this.panel7.Controls.Add(this.numericUpDown9);
            this.panel7.Controls.Add(this.label13);
            this.panel7.Controls.Add(this.numericUpDown8);
            this.panel7.Controls.Add(this.label14);
            this.panel7.Controls.Add(this.numericUpDown7);
            this.panel7.Controls.Add(this.label15);
            this.panel7.Controls.Add(this.numericUpDown6);
            this.panel7.Controls.Add(this.label16);
            this.panel7.Controls.Add(this.numericUpDown5);
            this.panel7.Controls.Add(this.label17);
            this.panel7.Controls.Add(this.numericUpDown4);
            this.panel7.Controls.Add(this.label18);
            this.panel7.Controls.Add(this.numericUpDown3);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel7.Location = new System.Drawing.Point(0, 24);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(604, 381);
            this.panel7.TabIndex = 18;
            // 
            // numericUpDown2
            // 
            this.numericUpDown2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numericUpDown2.Location = new System.Drawing.Point(94, 25);
            this.numericUpDown2.Maximum = new decimal(new int[] {
            6,
            0,
            0,
            0});
            this.numericUpDown2.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown2.Name = "numericUpDown2";
            this.numericUpDown2.Size = new System.Drawing.Size(40, 20);
            this.numericUpDown2.TabIndex = 26;
            this.numericUpDown2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.numericUpDown2.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label34
            // 
            this.label34.Location = new System.Drawing.Point(258, 25);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(40, 16);
            this.label34.TabIndex = 25;
            this.label34.Text = "8";
            this.label34.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label32
            // 
            this.label32.Location = new System.Drawing.Point(212, 25);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(40, 16);
            this.label32.TabIndex = 24;
            this.label32.Text = "8";
            this.label32.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label33
            // 
            this.label33.Location = new System.Drawing.Point(166, 25);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(40, 16);
            this.label33.TabIndex = 23;
            this.label33.Text = "8";
            this.label33.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label31
            // 
            this.label31.Location = new System.Drawing.Point(140, 25);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(20, 16);
            this.label31.TabIndex = 21;
            this.label31.Text = "8";
            this.label31.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label28
            // 
            this.label28.Location = new System.Drawing.Point(258, 6);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(40, 16);
            this.label28.TabIndex = 20;
            this.label28.Text = "I";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label27
            // 
            this.label27.Location = new System.Drawing.Point(212, 6);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(40, 16);
            this.label27.TabIndex = 19;
            this.label27.Text = "R";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label26
            // 
            this.label26.Location = new System.Drawing.Point(166, 6);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(40, 16);
            this.label26.TabIndex = 18;
            this.label26.Text = "T";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label25
            // 
            this.label25.Location = new System.Drawing.Point(91, 6);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(69, 16);
            this.label25.TabIndex = 17;
            this.label25.Text = "Basis";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(8, 27);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(41, 13);
            this.label11.TabIndex = 0;
            this.label11.Text = "St�rke:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(8, 53);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(67, 13);
            this.label12.TabIndex = 1;
            this.label12.Text = "Gewandheit:";
            // 
            // numericUpDown9
            // 
            this.numericUpDown9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numericUpDown9.Location = new System.Drawing.Point(94, 207);
            this.numericUpDown9.Maximum = new decimal(new int[] {
            6,
            0,
            0,
            0});
            this.numericUpDown9.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown9.Name = "numericUpDown9";
            this.numericUpDown9.Size = new System.Drawing.Size(40, 20);
            this.numericUpDown9.TabIndex = 16;
            this.numericUpDown9.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.numericUpDown9.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(8, 79);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(70, 13);
            this.label13.TabIndex = 2;
            this.label13.Text = "Schnelligkeit:";
            // 
            // numericUpDown8
            // 
            this.numericUpDown8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numericUpDown8.Location = new System.Drawing.Point(94, 181);
            this.numericUpDown8.Maximum = new decimal(new int[] {
            6,
            0,
            0,
            0});
            this.numericUpDown8.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown8.Name = "numericUpDown8";
            this.numericUpDown8.Size = new System.Drawing.Size(40, 20);
            this.numericUpDown8.TabIndex = 15;
            this.numericUpDown8.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.numericUpDown8.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(8, 103);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(65, 13);
            this.label14.TabIndex = 3;
            this.label14.Text = "Konstitution:";
            // 
            // numericUpDown7
            // 
            this.numericUpDown7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numericUpDown7.Location = new System.Drawing.Point(94, 155);
            this.numericUpDown7.Maximum = new decimal(new int[] {
            6,
            0,
            0,
            0});
            this.numericUpDown7.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown7.Name = "numericUpDown7";
            this.numericUpDown7.Size = new System.Drawing.Size(40, 20);
            this.numericUpDown7.TabIndex = 14;
            this.numericUpDown7.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.numericUpDown7.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(8, 131);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(57, 13);
            this.label15.TabIndex = 4;
            this.label15.Text = "Intelligenz:";
            // 
            // numericUpDown6
            // 
            this.numericUpDown6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numericUpDown6.Location = new System.Drawing.Point(94, 129);
            this.numericUpDown6.Maximum = new decimal(new int[] {
            6,
            0,
            0,
            0});
            this.numericUpDown6.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown6.Name = "numericUpDown6";
            this.numericUpDown6.Size = new System.Drawing.Size(40, 20);
            this.numericUpDown6.TabIndex = 13;
            this.numericUpDown6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.numericUpDown6.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(8, 157);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(65, 13);
            this.label16.TabIndex = 5;
            this.label16.Text = "Willenskraft:";
            // 
            // numericUpDown5
            // 
            this.numericUpDown5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numericUpDown5.Location = new System.Drawing.Point(94, 103);
            this.numericUpDown5.Maximum = new decimal(new int[] {
            6,
            0,
            0,
            0});
            this.numericUpDown5.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown5.Name = "numericUpDown5";
            this.numericUpDown5.Size = new System.Drawing.Size(40, 20);
            this.numericUpDown5.TabIndex = 12;
            this.numericUpDown5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.numericUpDown5.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(8, 183);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(53, 13);
            this.label17.TabIndex = 6;
            this.label17.Text = "Charisma:";
            // 
            // numericUpDown4
            // 
            this.numericUpDown4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numericUpDown4.Location = new System.Drawing.Point(94, 77);
            this.numericUpDown4.Maximum = new decimal(new int[] {
            6,
            0,
            0,
            0});
            this.numericUpDown4.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown4.Name = "numericUpDown4";
            this.numericUpDown4.Size = new System.Drawing.Size(40, 20);
            this.numericUpDown4.TabIndex = 11;
            this.numericUpDown4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.numericUpDown4.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(8, 209);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(80, 13);
            this.label18.TabIndex = 7;
            this.label18.Text = "Wahrnehmung:";
            // 
            // numericUpDown3
            // 
            this.numericUpDown3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numericUpDown3.Location = new System.Drawing.Point(94, 51);
            this.numericUpDown3.Maximum = new decimal(new int[] {
            6,
            0,
            0,
            0});
            this.numericUpDown3.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown3.Name = "numericUpDown3";
            this.numericUpDown3.Size = new System.Drawing.Size(40, 20);
            this.numericUpDown3.TabIndex = 10;
            this.numericUpDown3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.numericUpDown3.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label19
            // 
            this.label19.Dock = System.Windows.Forms.DockStyle.Top;
            this.label19.Location = new System.Drawing.Point(0, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(604, 24);
            this.label19.TabIndex = 17;
            this.label19.Text = "Verbleibende Punkte: 22";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.panel9);
            this.tabPage4.Controls.Add(this.label24);
            this.tabPage4.Location = new System.Drawing.Point(4, 25);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(604, 405);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Suni-Punkte";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // panel9
            // 
            this.panel9.AutoScroll = true;
            this.panel9.Controls.Add(this.groupBox5);
            this.panel9.Controls.Add(this.groupBox4);
            this.panel9.Controls.Add(this.groupBox7);
            this.panel9.Controls.Add(this.groupBox3);
            this.panel9.Controls.Add(this.groupBox6);
            this.panel9.Controls.Add(this.groupBox2);
            this.panel9.Controls.Add(this.groupBox1);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel9.Location = new System.Drawing.Point(0, 24);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(604, 381);
            this.panel9.TabIndex = 9;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.numericUpDown22);
            this.groupBox5.Controls.Add(this.label43);
            this.groupBox5.Controls.Add(this.label44);
            this.groupBox5.Controls.Add(this.numericUpDown23);
            this.groupBox5.Controls.Add(this.label45);
            this.groupBox5.Controls.Add(this.numericUpDown24);
            this.groupBox5.Controls.Add(this.label46);
            this.groupBox5.Controls.Add(this.numericUpDown25);
            this.groupBox5.Controls.Add(this.label47);
            this.groupBox5.Controls.Add(this.numericUpDown26);
            this.groupBox5.Controls.Add(this.label48);
            this.groupBox5.Controls.Add(this.numericUpDown27);
            this.groupBox5.Controls.Add(this.label49);
            this.groupBox5.Controls.Add(this.numericUpDown28);
            this.groupBox5.Controls.Add(this.label50);
            this.groupBox5.Controls.Add(this.numericUpDown29);
            this.groupBox5.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox5.Location = new System.Drawing.Point(0, 510);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(588, 70);
            this.groupBox5.TabIndex = 19;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Ion - Attribute";
            // 
            // numericUpDown22
            // 
            this.numericUpDown22.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numericUpDown22.Location = new System.Drawing.Point(94, 14);
            this.numericUpDown22.Maximum = new decimal(new int[] {
            6,
            0,
            0,
            0});
            this.numericUpDown22.Name = "numericUpDown22";
            this.numericUpDown22.Size = new System.Drawing.Size(40, 20);
            this.numericUpDown22.TabIndex = 42;
            this.numericUpDown22.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(8, 16);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(41, 13);
            this.label43.TabIndex = 27;
            this.label43.Text = "St�rke:";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(8, 42);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(67, 13);
            this.label44.TabIndex = 28;
            this.label44.Text = "Gewandheit:";
            // 
            // numericUpDown23
            // 
            this.numericUpDown23.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numericUpDown23.Location = new System.Drawing.Point(490, 40);
            this.numericUpDown23.Maximum = new decimal(new int[] {
            6,
            0,
            0,
            0});
            this.numericUpDown23.Name = "numericUpDown23";
            this.numericUpDown23.Size = new System.Drawing.Size(40, 20);
            this.numericUpDown23.TabIndex = 41;
            this.numericUpDown23.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(140, 16);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(70, 13);
            this.label45.TabIndex = 29;
            this.label45.Text = "Schnelligkeit:";
            // 
            // numericUpDown24
            // 
            this.numericUpDown24.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numericUpDown24.Location = new System.Drawing.Point(490, 14);
            this.numericUpDown24.Maximum = new decimal(new int[] {
            6,
            0,
            0,
            0});
            this.numericUpDown24.Name = "numericUpDown24";
            this.numericUpDown24.Size = new System.Drawing.Size(40, 20);
            this.numericUpDown24.TabIndex = 40;
            this.numericUpDown24.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(140, 42);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(65, 13);
            this.label46.TabIndex = 30;
            this.label46.Text = "Konstitution:";
            // 
            // numericUpDown25
            // 
            this.numericUpDown25.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numericUpDown25.Location = new System.Drawing.Point(358, 40);
            this.numericUpDown25.Maximum = new decimal(new int[] {
            6,
            0,
            0,
            0});
            this.numericUpDown25.Name = "numericUpDown25";
            this.numericUpDown25.Size = new System.Drawing.Size(40, 20);
            this.numericUpDown25.TabIndex = 39;
            this.numericUpDown25.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(272, 16);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(57, 13);
            this.label47.TabIndex = 31;
            this.label47.Text = "Intelligenz:";
            // 
            // numericUpDown26
            // 
            this.numericUpDown26.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numericUpDown26.Location = new System.Drawing.Point(358, 14);
            this.numericUpDown26.Maximum = new decimal(new int[] {
            6,
            0,
            0,
            0});
            this.numericUpDown26.Name = "numericUpDown26";
            this.numericUpDown26.Size = new System.Drawing.Size(40, 20);
            this.numericUpDown26.TabIndex = 38;
            this.numericUpDown26.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(272, 42);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(65, 13);
            this.label48.TabIndex = 32;
            this.label48.Text = "Willenskraft:";
            // 
            // numericUpDown27
            // 
            this.numericUpDown27.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numericUpDown27.Location = new System.Drawing.Point(226, 40);
            this.numericUpDown27.Maximum = new decimal(new int[] {
            6,
            0,
            0,
            0});
            this.numericUpDown27.Name = "numericUpDown27";
            this.numericUpDown27.Size = new System.Drawing.Size(40, 20);
            this.numericUpDown27.TabIndex = 37;
            this.numericUpDown27.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(404, 16);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(53, 13);
            this.label49.TabIndex = 33;
            this.label49.Text = "Charisma:";
            // 
            // numericUpDown28
            // 
            this.numericUpDown28.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numericUpDown28.Location = new System.Drawing.Point(226, 14);
            this.numericUpDown28.Maximum = new decimal(new int[] {
            6,
            0,
            0,
            0});
            this.numericUpDown28.Name = "numericUpDown28";
            this.numericUpDown28.Size = new System.Drawing.Size(40, 20);
            this.numericUpDown28.TabIndex = 36;
            this.numericUpDown28.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(404, 42);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(80, 13);
            this.label50.TabIndex = 34;
            this.label50.Text = "Wahrnehmung:";
            // 
            // numericUpDown29
            // 
            this.numericUpDown29.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numericUpDown29.Location = new System.Drawing.Point(94, 40);
            this.numericUpDown29.Maximum = new decimal(new int[] {
            6,
            0,
            0,
            0});
            this.numericUpDown29.Name = "numericUpDown29";
            this.numericUpDown29.Size = new System.Drawing.Size(40, 20);
            this.numericUpDown29.TabIndex = 35;
            this.numericUpDown29.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // groupBox4
            // 
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox4.Location = new System.Drawing.Point(0, 410);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(588, 100);
            this.groupBox4.TabIndex = 18;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Ion - Cyber- und Bioware";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.numericUpDown38);
            this.groupBox7.Controls.Add(this.label59);
            this.groupBox7.Controls.Add(this.label60);
            this.groupBox7.Controls.Add(this.numericUpDown39);
            this.groupBox7.Controls.Add(this.label61);
            this.groupBox7.Controls.Add(this.numericUpDown40);
            this.groupBox7.Controls.Add(this.label62);
            this.groupBox7.Controls.Add(this.numericUpDown41);
            this.groupBox7.Controls.Add(this.label63);
            this.groupBox7.Controls.Add(this.numericUpDown42);
            this.groupBox7.Controls.Add(this.label64);
            this.groupBox7.Controls.Add(this.numericUpDown43);
            this.groupBox7.Controls.Add(this.label65);
            this.groupBox7.Controls.Add(this.numericUpDown44);
            this.groupBox7.Controls.Add(this.label66);
            this.groupBox7.Controls.Add(this.numericUpDown45);
            this.groupBox7.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox7.Location = new System.Drawing.Point(0, 340);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(588, 70);
            this.groupBox7.TabIndex = 17;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Rulegard - Attribute";
            // 
            // numericUpDown38
            // 
            this.numericUpDown38.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numericUpDown38.Location = new System.Drawing.Point(94, 14);
            this.numericUpDown38.Maximum = new decimal(new int[] {
            6,
            0,
            0,
            0});
            this.numericUpDown38.Name = "numericUpDown38";
            this.numericUpDown38.Size = new System.Drawing.Size(40, 20);
            this.numericUpDown38.TabIndex = 42;
            this.numericUpDown38.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(8, 16);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(41, 13);
            this.label59.TabIndex = 27;
            this.label59.Text = "St�rke:";
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Location = new System.Drawing.Point(8, 42);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(67, 13);
            this.label60.TabIndex = 28;
            this.label60.Text = "Gewandheit:";
            // 
            // numericUpDown39
            // 
            this.numericUpDown39.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numericUpDown39.Location = new System.Drawing.Point(490, 40);
            this.numericUpDown39.Maximum = new decimal(new int[] {
            6,
            0,
            0,
            0});
            this.numericUpDown39.Name = "numericUpDown39";
            this.numericUpDown39.Size = new System.Drawing.Size(40, 20);
            this.numericUpDown39.TabIndex = 41;
            this.numericUpDown39.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Location = new System.Drawing.Point(140, 16);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(70, 13);
            this.label61.TabIndex = 29;
            this.label61.Text = "Schnelligkeit:";
            // 
            // numericUpDown40
            // 
            this.numericUpDown40.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numericUpDown40.Location = new System.Drawing.Point(490, 14);
            this.numericUpDown40.Maximum = new decimal(new int[] {
            6,
            0,
            0,
            0});
            this.numericUpDown40.Name = "numericUpDown40";
            this.numericUpDown40.Size = new System.Drawing.Size(40, 20);
            this.numericUpDown40.TabIndex = 40;
            this.numericUpDown40.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Location = new System.Drawing.Point(140, 42);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(65, 13);
            this.label62.TabIndex = 30;
            this.label62.Text = "Konstitution:";
            // 
            // numericUpDown41
            // 
            this.numericUpDown41.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numericUpDown41.Location = new System.Drawing.Point(358, 40);
            this.numericUpDown41.Maximum = new decimal(new int[] {
            6,
            0,
            0,
            0});
            this.numericUpDown41.Name = "numericUpDown41";
            this.numericUpDown41.Size = new System.Drawing.Size(40, 20);
            this.numericUpDown41.TabIndex = 39;
            this.numericUpDown41.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Location = new System.Drawing.Point(272, 16);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(57, 13);
            this.label63.TabIndex = 31;
            this.label63.Text = "Intelligenz:";
            // 
            // numericUpDown42
            // 
            this.numericUpDown42.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numericUpDown42.Location = new System.Drawing.Point(358, 14);
            this.numericUpDown42.Maximum = new decimal(new int[] {
            6,
            0,
            0,
            0});
            this.numericUpDown42.Name = "numericUpDown42";
            this.numericUpDown42.Size = new System.Drawing.Size(40, 20);
            this.numericUpDown42.TabIndex = 38;
            this.numericUpDown42.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Location = new System.Drawing.Point(272, 42);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(65, 13);
            this.label64.TabIndex = 32;
            this.label64.Text = "Willenskraft:";
            // 
            // numericUpDown43
            // 
            this.numericUpDown43.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numericUpDown43.Location = new System.Drawing.Point(226, 40);
            this.numericUpDown43.Maximum = new decimal(new int[] {
            6,
            0,
            0,
            0});
            this.numericUpDown43.Name = "numericUpDown43";
            this.numericUpDown43.Size = new System.Drawing.Size(40, 20);
            this.numericUpDown43.TabIndex = 37;
            this.numericUpDown43.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Location = new System.Drawing.Point(404, 16);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(53, 13);
            this.label65.TabIndex = 33;
            this.label65.Text = "Charisma:";
            // 
            // numericUpDown44
            // 
            this.numericUpDown44.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numericUpDown44.Location = new System.Drawing.Point(226, 14);
            this.numericUpDown44.Maximum = new decimal(new int[] {
            6,
            0,
            0,
            0});
            this.numericUpDown44.Name = "numericUpDown44";
            this.numericUpDown44.Size = new System.Drawing.Size(40, 20);
            this.numericUpDown44.TabIndex = 36;
            this.numericUpDown44.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Location = new System.Drawing.Point(404, 42);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(80, 13);
            this.label66.TabIndex = 34;
            this.label66.Text = "Wahrnehmung:";
            // 
            // numericUpDown45
            // 
            this.numericUpDown45.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numericUpDown45.Location = new System.Drawing.Point(94, 40);
            this.numericUpDown45.Maximum = new decimal(new int[] {
            6,
            0,
            0,
            0});
            this.numericUpDown45.Name = "numericUpDown45";
            this.numericUpDown45.Size = new System.Drawing.Size(40, 20);
            this.numericUpDown45.TabIndex = 35;
            this.numericUpDown45.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // groupBox3
            // 
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox3.Location = new System.Drawing.Point(0, 240);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(588, 100);
            this.groupBox3.TabIndex = 14;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Rulegard - Rasse/Klasse";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.numericUpDown30);
            this.groupBox6.Controls.Add(this.label51);
            this.groupBox6.Controls.Add(this.label52);
            this.groupBox6.Controls.Add(this.numericUpDown31);
            this.groupBox6.Controls.Add(this.label53);
            this.groupBox6.Controls.Add(this.numericUpDown32);
            this.groupBox6.Controls.Add(this.label54);
            this.groupBox6.Controls.Add(this.numericUpDown33);
            this.groupBox6.Controls.Add(this.label55);
            this.groupBox6.Controls.Add(this.numericUpDown34);
            this.groupBox6.Controls.Add(this.label56);
            this.groupBox6.Controls.Add(this.numericUpDown35);
            this.groupBox6.Controls.Add(this.label57);
            this.groupBox6.Controls.Add(this.numericUpDown36);
            this.groupBox6.Controls.Add(this.label58);
            this.groupBox6.Controls.Add(this.numericUpDown37);
            this.groupBox6.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox6.Location = new System.Drawing.Point(0, 170);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(588, 70);
            this.groupBox6.TabIndex = 13;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Terra - Attribute";
            // 
            // numericUpDown30
            // 
            this.numericUpDown30.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numericUpDown30.Location = new System.Drawing.Point(94, 14);
            this.numericUpDown30.Maximum = new decimal(new int[] {
            6,
            0,
            0,
            0});
            this.numericUpDown30.Name = "numericUpDown30";
            this.numericUpDown30.Size = new System.Drawing.Size(40, 20);
            this.numericUpDown30.TabIndex = 42;
            this.numericUpDown30.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(8, 16);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(41, 13);
            this.label51.TabIndex = 27;
            this.label51.Text = "St�rke:";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(8, 42);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(67, 13);
            this.label52.TabIndex = 28;
            this.label52.Text = "Gewandheit:";
            // 
            // numericUpDown31
            // 
            this.numericUpDown31.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numericUpDown31.Location = new System.Drawing.Point(490, 40);
            this.numericUpDown31.Maximum = new decimal(new int[] {
            6,
            0,
            0,
            0});
            this.numericUpDown31.Name = "numericUpDown31";
            this.numericUpDown31.Size = new System.Drawing.Size(40, 20);
            this.numericUpDown31.TabIndex = 41;
            this.numericUpDown31.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(140, 16);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(70, 13);
            this.label53.TabIndex = 29;
            this.label53.Text = "Schnelligkeit:";
            // 
            // numericUpDown32
            // 
            this.numericUpDown32.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numericUpDown32.Location = new System.Drawing.Point(490, 14);
            this.numericUpDown32.Maximum = new decimal(new int[] {
            6,
            0,
            0,
            0});
            this.numericUpDown32.Name = "numericUpDown32";
            this.numericUpDown32.Size = new System.Drawing.Size(40, 20);
            this.numericUpDown32.TabIndex = 40;
            this.numericUpDown32.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(140, 42);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(65, 13);
            this.label54.TabIndex = 30;
            this.label54.Text = "Konstitution:";
            // 
            // numericUpDown33
            // 
            this.numericUpDown33.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numericUpDown33.Location = new System.Drawing.Point(358, 40);
            this.numericUpDown33.Maximum = new decimal(new int[] {
            6,
            0,
            0,
            0});
            this.numericUpDown33.Name = "numericUpDown33";
            this.numericUpDown33.Size = new System.Drawing.Size(40, 20);
            this.numericUpDown33.TabIndex = 39;
            this.numericUpDown33.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(272, 16);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(57, 13);
            this.label55.TabIndex = 31;
            this.label55.Text = "Intelligenz:";
            // 
            // numericUpDown34
            // 
            this.numericUpDown34.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numericUpDown34.Location = new System.Drawing.Point(358, 14);
            this.numericUpDown34.Maximum = new decimal(new int[] {
            6,
            0,
            0,
            0});
            this.numericUpDown34.Name = "numericUpDown34";
            this.numericUpDown34.Size = new System.Drawing.Size(40, 20);
            this.numericUpDown34.TabIndex = 38;
            this.numericUpDown34.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(272, 42);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(65, 13);
            this.label56.TabIndex = 32;
            this.label56.Text = "Willenskraft:";
            // 
            // numericUpDown35
            // 
            this.numericUpDown35.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numericUpDown35.Location = new System.Drawing.Point(226, 40);
            this.numericUpDown35.Maximum = new decimal(new int[] {
            6,
            0,
            0,
            0});
            this.numericUpDown35.Name = "numericUpDown35";
            this.numericUpDown35.Size = new System.Drawing.Size(40, 20);
            this.numericUpDown35.TabIndex = 37;
            this.numericUpDown35.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(404, 16);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(53, 13);
            this.label57.TabIndex = 33;
            this.label57.Text = "Charisma:";
            // 
            // numericUpDown36
            // 
            this.numericUpDown36.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numericUpDown36.Location = new System.Drawing.Point(226, 14);
            this.numericUpDown36.Maximum = new decimal(new int[] {
            6,
            0,
            0,
            0});
            this.numericUpDown36.Name = "numericUpDown36";
            this.numericUpDown36.Size = new System.Drawing.Size(40, 20);
            this.numericUpDown36.TabIndex = 36;
            this.numericUpDown36.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Location = new System.Drawing.Point(404, 42);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(80, 13);
            this.label58.TabIndex = 34;
            this.label58.Text = "Wahrnehmung:";
            // 
            // numericUpDown37
            // 
            this.numericUpDown37.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numericUpDown37.Location = new System.Drawing.Point(94, 40);
            this.numericUpDown37.Maximum = new decimal(new int[] {
            6,
            0,
            0,
            0});
            this.numericUpDown37.Name = "numericUpDown37";
            this.numericUpDown37.Size = new System.Drawing.Size(40, 20);
            this.numericUpDown37.TabIndex = 35;
            this.numericUpDown37.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // groupBox2
            // 
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox2.Location = new System.Drawing.Point(0, 70);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(588, 100);
            this.groupBox2.TabIndex = 9;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Terra - Talent";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.numericUpDown14);
            this.groupBox1.Controls.Add(this.label35);
            this.groupBox1.Controls.Add(this.label36);
            this.groupBox1.Controls.Add(this.numericUpDown15);
            this.groupBox1.Controls.Add(this.label37);
            this.groupBox1.Controls.Add(this.numericUpDown16);
            this.groupBox1.Controls.Add(this.label38);
            this.groupBox1.Controls.Add(this.numericUpDown17);
            this.groupBox1.Controls.Add(this.label39);
            this.groupBox1.Controls.Add(this.numericUpDown18);
            this.groupBox1.Controls.Add(this.label40);
            this.groupBox1.Controls.Add(this.numericUpDown19);
            this.groupBox1.Controls.Add(this.label41);
            this.groupBox1.Controls.Add(this.numericUpDown20);
            this.groupBox1.Controls.Add(this.label42);
            this.groupBox1.Controls.Add(this.numericUpDown21);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(588, 70);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Auf Grundattribute";
            // 
            // numericUpDown14
            // 
            this.numericUpDown14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numericUpDown14.Location = new System.Drawing.Point(94, 14);
            this.numericUpDown14.Maximum = new decimal(new int[] {
            6,
            0,
            0,
            0});
            this.numericUpDown14.Name = "numericUpDown14";
            this.numericUpDown14.Size = new System.Drawing.Size(40, 20);
            this.numericUpDown14.TabIndex = 42;
            this.numericUpDown14.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(8, 16);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(41, 13);
            this.label35.TabIndex = 27;
            this.label35.Text = "St�rke:";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(8, 42);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(67, 13);
            this.label36.TabIndex = 28;
            this.label36.Text = "Gewandheit:";
            // 
            // numericUpDown15
            // 
            this.numericUpDown15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numericUpDown15.Location = new System.Drawing.Point(490, 40);
            this.numericUpDown15.Maximum = new decimal(new int[] {
            6,
            0,
            0,
            0});
            this.numericUpDown15.Name = "numericUpDown15";
            this.numericUpDown15.Size = new System.Drawing.Size(40, 20);
            this.numericUpDown15.TabIndex = 41;
            this.numericUpDown15.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(140, 16);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(70, 13);
            this.label37.TabIndex = 29;
            this.label37.Text = "Schnelligkeit:";
            // 
            // numericUpDown16
            // 
            this.numericUpDown16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numericUpDown16.Location = new System.Drawing.Point(490, 14);
            this.numericUpDown16.Maximum = new decimal(new int[] {
            6,
            0,
            0,
            0});
            this.numericUpDown16.Name = "numericUpDown16";
            this.numericUpDown16.Size = new System.Drawing.Size(40, 20);
            this.numericUpDown16.TabIndex = 40;
            this.numericUpDown16.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(140, 42);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(65, 13);
            this.label38.TabIndex = 30;
            this.label38.Text = "Konstitution:";
            // 
            // numericUpDown17
            // 
            this.numericUpDown17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numericUpDown17.Location = new System.Drawing.Point(358, 40);
            this.numericUpDown17.Maximum = new decimal(new int[] {
            6,
            0,
            0,
            0});
            this.numericUpDown17.Name = "numericUpDown17";
            this.numericUpDown17.Size = new System.Drawing.Size(40, 20);
            this.numericUpDown17.TabIndex = 39;
            this.numericUpDown17.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(272, 16);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(57, 13);
            this.label39.TabIndex = 31;
            this.label39.Text = "Intelligenz:";
            // 
            // numericUpDown18
            // 
            this.numericUpDown18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numericUpDown18.Location = new System.Drawing.Point(358, 14);
            this.numericUpDown18.Maximum = new decimal(new int[] {
            6,
            0,
            0,
            0});
            this.numericUpDown18.Name = "numericUpDown18";
            this.numericUpDown18.Size = new System.Drawing.Size(40, 20);
            this.numericUpDown18.TabIndex = 38;
            this.numericUpDown18.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(272, 42);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(65, 13);
            this.label40.TabIndex = 32;
            this.label40.Text = "Willenskraft:";
            // 
            // numericUpDown19
            // 
            this.numericUpDown19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numericUpDown19.Location = new System.Drawing.Point(226, 40);
            this.numericUpDown19.Maximum = new decimal(new int[] {
            6,
            0,
            0,
            0});
            this.numericUpDown19.Name = "numericUpDown19";
            this.numericUpDown19.Size = new System.Drawing.Size(40, 20);
            this.numericUpDown19.TabIndex = 37;
            this.numericUpDown19.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(404, 16);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(53, 13);
            this.label41.TabIndex = 33;
            this.label41.Text = "Charisma:";
            // 
            // numericUpDown20
            // 
            this.numericUpDown20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numericUpDown20.Location = new System.Drawing.Point(226, 14);
            this.numericUpDown20.Maximum = new decimal(new int[] {
            6,
            0,
            0,
            0});
            this.numericUpDown20.Name = "numericUpDown20";
            this.numericUpDown20.Size = new System.Drawing.Size(40, 20);
            this.numericUpDown20.TabIndex = 36;
            this.numericUpDown20.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(404, 42);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(80, 13);
            this.label42.TabIndex = 34;
            this.label42.Text = "Wahrnehmung:";
            // 
            // numericUpDown21
            // 
            this.numericUpDown21.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numericUpDown21.Location = new System.Drawing.Point(94, 40);
            this.numericUpDown21.Maximum = new decimal(new int[] {
            6,
            0,
            0,
            0});
            this.numericUpDown21.Name = "numericUpDown21";
            this.numericUpDown21.Size = new System.Drawing.Size(40, 20);
            this.numericUpDown21.TabIndex = 35;
            this.numericUpDown21.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label24
            // 
            this.label24.Dock = System.Windows.Forms.DockStyle.Top;
            this.label24.Location = new System.Drawing.Point(0, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(604, 24);
            this.label24.TabIndex = 7;
            this.label24.Text = "Verbleibende Punkte: X";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.panel5);
            this.tabPage5.Controls.Add(this.panel3);
            this.tabPage5.Controls.Add(this.label23);
            this.tabPage5.Location = new System.Drawing.Point(4, 25);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(604, 405);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Fertigkeiten";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // panel5
            // 
            this.panel5.AutoScroll = true;
            this.panel5.Controls.Add(this.numericUpDown10);
            this.panel5.Controls.Add(this.comboBox5);
            this.panel5.Controls.Add(this.comboBox4);
            this.panel5.Controls.Add(this.cbFertigkeiten);
            this.panel5.Controls.Add(this.checkBox2);
            this.panel5.Controls.Add(this.label21);
            this.panel5.Controls.Add(this.checkBox1);
            this.panel5.Controls.Add(this.label20);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(171, 24);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(433, 381);
            this.panel5.TabIndex = 8;
            // 
            // numericUpDown10
            // 
            this.numericUpDown10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numericUpDown10.Location = new System.Drawing.Point(130, 61);
            this.numericUpDown10.Maximum = new decimal(new int[] {
            99,
            0,
            0,
            0});
            this.numericUpDown10.Name = "numericUpDown10";
            this.numericUpDown10.Size = new System.Drawing.Size(40, 20);
            this.numericUpDown10.TabIndex = 10;
            this.numericUpDown10.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // comboBox5
            // 
            this.comboBox5.FormattingEnabled = true;
            this.comboBox5.Location = new System.Drawing.Point(130, 34);
            this.comboBox5.Name = "comboBox5";
            this.comboBox5.Size = new System.Drawing.Size(121, 21);
            this.comboBox5.TabIndex = 9;
            // 
            // comboBox4
            // 
            this.comboBox4.FormattingEnabled = true;
            this.comboBox4.Location = new System.Drawing.Point(130, 87);
            this.comboBox4.Name = "comboBox4";
            this.comboBox4.Size = new System.Drawing.Size(121, 21);
            this.comboBox4.TabIndex = 8;
            // 
            // cbFertigkeiten
            // 
            this.cbFertigkeiten.FormattingEnabled = true;
            this.cbFertigkeiten.Location = new System.Drawing.Point(130, 7);
            this.cbFertigkeiten.Name = "cbFertigkeiten";
            this.cbFertigkeiten.Size = new System.Drawing.Size(121, 21);
            this.cbFertigkeiten.TabIndex = 5;
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBox2.Location = new System.Drawing.Point(9, 88);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(107, 17);
            this.checkBox2.TabIndex = 3;
            this.checkBox2.Text = "Gruppenfertigkeit:";
            this.checkBox2.UseVisualStyleBackColor = true;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(6, 62);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(35, 13);
            this.label21.TabIndex = 2;
            this.label21.Text = "Stufe:";
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBox1.Location = new System.Drawing.Point(9, 35);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(115, 17);
            this.checkBox1.TabIndex = 1;
            this.checkBox1.Text = "Auspr�gung lernen:";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(6, 9);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(53, 13);
            this.label20.TabIndex = 0;
            this.label20.Text = "Fertigkeit:";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.listBox2);
            this.panel3.Controls.Add(this.panel6);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel3.Location = new System.Drawing.Point(0, 24);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(171, 381);
            this.panel3.TabIndex = 7;
            // 
            // listBox2
            // 
            this.listBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.listBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBox2.FormattingEnabled = true;
            this.listBox2.Location = new System.Drawing.Point(0, 0);
            this.listBox2.Name = "listBox2";
            this.listBox2.Size = new System.Drawing.Size(171, 340);
            this.listBox2.TabIndex = 2;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.button3);
            this.panel6.Controls.Add(this.button4);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel6.Location = new System.Drawing.Point(0, 350);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(171, 31);
            this.panel6.TabIndex = 1;
            // 
            // button3
            // 
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Location = new System.Drawing.Point(89, 5);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 1;
            this.button3.Text = "L�schen";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Location = new System.Drawing.Point(8, 5);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 0;
            this.button4.Text = "Neu";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // label23
            // 
            this.label23.Dock = System.Windows.Forms.DockStyle.Top;
            this.label23.Location = new System.Drawing.Point(0, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(604, 24);
            this.label23.TabIndex = 6;
            this.label23.Text = "Verbleibende Punkte: 66";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tabPage6
            // 
            this.tabPage6.Location = new System.Drawing.Point(4, 25);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Size = new System.Drawing.Size(604, 405);
            this.tabPage6.TabIndex = 5;
            this.tabPage6.Text = "Ausr�stung";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // listBox1
            // 
            this.listBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(0, 0);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(171, 342);
            this.listBox1.TabIndex = 2;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.button2);
            this.panel4.Controls.Add(this.button1);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel4.Location = new System.Drawing.Point(0, 346);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(171, 31);
            this.panel4.TabIndex = 1;
            // 
            // button2
            // 
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Location = new System.Drawing.Point(89, 5);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 1;
            this.button2.Text = "L�schen";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Location = new System.Drawing.Point(8, 5);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Neu";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(258, 51);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(40, 16);
            this.label7.TabIndex = 30;
            this.label7.Text = "8";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label67
            // 
            this.label67.Location = new System.Drawing.Point(212, 51);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(40, 16);
            this.label67.TabIndex = 29;
            this.label67.Text = "8";
            this.label67.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label68
            // 
            this.label68.Location = new System.Drawing.Point(166, 51);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(40, 16);
            this.label68.TabIndex = 28;
            this.label68.Text = "8";
            this.label68.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label69
            // 
            this.label69.Location = new System.Drawing.Point(140, 51);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(20, 16);
            this.label69.TabIndex = 27;
            this.label69.Text = "8";
            this.label69.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label70
            // 
            this.label70.Location = new System.Drawing.Point(258, 77);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(40, 16);
            this.label70.TabIndex = 34;
            this.label70.Text = "8";
            this.label70.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label71
            // 
            this.label71.Location = new System.Drawing.Point(212, 77);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(40, 16);
            this.label71.TabIndex = 33;
            this.label71.Text = "8";
            this.label71.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label72
            // 
            this.label72.Location = new System.Drawing.Point(166, 77);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(40, 16);
            this.label72.TabIndex = 32;
            this.label72.Text = "8";
            this.label72.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label73
            // 
            this.label73.Location = new System.Drawing.Point(140, 77);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(20, 16);
            this.label73.TabIndex = 31;
            this.label73.Text = "8";
            this.label73.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label74
            // 
            this.label74.Location = new System.Drawing.Point(258, 103);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(40, 16);
            this.label74.TabIndex = 38;
            this.label74.Text = "8";
            this.label74.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label75
            // 
            this.label75.Location = new System.Drawing.Point(212, 103);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(40, 16);
            this.label75.TabIndex = 37;
            this.label75.Text = "8";
            this.label75.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label76
            // 
            this.label76.Location = new System.Drawing.Point(166, 103);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(40, 16);
            this.label76.TabIndex = 36;
            this.label76.Text = "8";
            this.label76.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label77
            // 
            this.label77.Location = new System.Drawing.Point(140, 103);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(20, 16);
            this.label77.TabIndex = 35;
            this.label77.Text = "8";
            this.label77.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label78
            // 
            this.label78.Location = new System.Drawing.Point(258, 129);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(40, 16);
            this.label78.TabIndex = 42;
            this.label78.Text = "8";
            this.label78.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label79
            // 
            this.label79.Location = new System.Drawing.Point(212, 129);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(40, 16);
            this.label79.TabIndex = 41;
            this.label79.Text = "8";
            this.label79.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label80
            // 
            this.label80.Location = new System.Drawing.Point(166, 129);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(40, 16);
            this.label80.TabIndex = 40;
            this.label80.Text = "8";
            this.label80.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label81
            // 
            this.label81.Location = new System.Drawing.Point(140, 129);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(20, 16);
            this.label81.TabIndex = 39;
            this.label81.Text = "8";
            this.label81.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label82
            // 
            this.label82.Location = new System.Drawing.Point(258, 155);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(40, 16);
            this.label82.TabIndex = 46;
            this.label82.Text = "8";
            this.label82.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label83
            // 
            this.label83.Location = new System.Drawing.Point(212, 155);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(40, 16);
            this.label83.TabIndex = 45;
            this.label83.Text = "8";
            this.label83.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label84
            // 
            this.label84.Location = new System.Drawing.Point(166, 155);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(40, 16);
            this.label84.TabIndex = 44;
            this.label84.Text = "8";
            this.label84.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label85
            // 
            this.label85.Location = new System.Drawing.Point(140, 155);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(20, 16);
            this.label85.TabIndex = 43;
            this.label85.Text = "8";
            this.label85.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label86
            // 
            this.label86.Location = new System.Drawing.Point(258, 181);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(40, 16);
            this.label86.TabIndex = 50;
            this.label86.Text = "8";
            this.label86.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label87
            // 
            this.label87.Location = new System.Drawing.Point(212, 181);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(40, 16);
            this.label87.TabIndex = 49;
            this.label87.Text = "8";
            this.label87.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label88
            // 
            this.label88.Location = new System.Drawing.Point(166, 181);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(40, 16);
            this.label88.TabIndex = 48;
            this.label88.Text = "8";
            this.label88.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label89
            // 
            this.label89.Location = new System.Drawing.Point(140, 181);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(20, 16);
            this.label89.TabIndex = 47;
            this.label89.Text = "8";
            this.label89.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label90
            // 
            this.label90.Location = new System.Drawing.Point(258, 207);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(40, 16);
            this.label90.TabIndex = 54;
            this.label90.Text = "8";
            this.label90.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label91
            // 
            this.label91.Location = new System.Drawing.Point(212, 207);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(40, 16);
            this.label91.TabIndex = 53;
            this.label91.Text = "8";
            this.label91.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label92
            // 
            this.label92.Location = new System.Drawing.Point(166, 207);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(40, 16);
            this.label92.TabIndex = 52;
            this.label92.Text = "8";
            this.label92.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label93
            // 
            this.label93.Location = new System.Drawing.Point(140, 207);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(20, 16);
            this.label93.TabIndex = 51;
            this.label93.Text = "8";
            this.label93.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel8
            // 
            this.panel8.AutoScroll = true;
            this.panel8.Controls.Add(this.nUDSans);
            this.panel8.Controls.Add(this.nUDSuni);
            this.panel8.Controls.Add(this.nUDSihir);
            this.panel8.Controls.Add(this.label8);
            this.panel8.Controls.Add(this.label9);
            this.panel8.Controls.Add(this.label10);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel8.Location = new System.Drawing.Point(0, 24);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(604, 381);
            this.panel8.TabIndex = 10;
            // 
            // nUDSans
            // 
            this.nUDSans.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nUDSans.Location = new System.Drawing.Point(45, 60);
            this.nUDSans.Maximum = new decimal(new int[] {
            6,
            0,
            0,
            0});
            this.nUDSans.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nUDSans.Name = "nUDSans";
            this.nUDSans.Size = new System.Drawing.Size(40, 20);
            this.nUDSans.TabIndex = 12;
            this.nUDSans.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.nUDSans.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // nUDSuni
            // 
            this.nUDSuni.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nUDSuni.Location = new System.Drawing.Point(45, 34);
            this.nUDSuni.Maximum = new decimal(new int[] {
            6,
            0,
            0,
            0});
            this.nUDSuni.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nUDSuni.Name = "nUDSuni";
            this.nUDSuni.Size = new System.Drawing.Size(40, 20);
            this.nUDSuni.TabIndex = 11;
            this.nUDSuni.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.nUDSuni.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // nUDSihir
            // 
            this.nUDSihir.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nUDSihir.Location = new System.Drawing.Point(45, 8);
            this.nUDSihir.Maximum = new decimal(new int[] {
            6,
            0,
            0,
            0});
            this.nUDSihir.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nUDSihir.Name = "nUDSihir";
            this.nUDSihir.Size = new System.Drawing.Size(40, 20);
            this.nUDSihir.TabIndex = 10;
            this.nUDSihir.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.nUDSihir.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(5, 10);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(30, 13);
            this.label8.TabIndex = 1;
            this.label8.Text = "Sihir:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(5, 36);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(31, 13);
            this.label9.TabIndex = 2;
            this.label9.Text = "Suni:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(5, 62);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(34, 13);
            this.label10.TabIndex = 3;
            this.label10.Text = "Sans:";
            // 
            // lVerbleibendeNebelpunkte
            // 
            this.lVerbleibendeNebelpunkte.Dock = System.Windows.Forms.DockStyle.Top;
            this.lVerbleibendeNebelpunkte.Location = new System.Drawing.Point(0, 0);
            this.lVerbleibendeNebelpunkte.Name = "lVerbleibendeNebelpunkte";
            this.lVerbleibendeNebelpunkte.Size = new System.Drawing.Size(604, 24);
            this.lVerbleibendeNebelpunkte.TabIndex = 9;
            this.lVerbleibendeNebelpunkte.Text = "Verbleibende Punkte: 7";
            this.lVerbleibendeNebelpunkte.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(612, 459);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.toolStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Main";
            this.Text = "TRI - Character Sheets";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown3)).EndInit();
            this.tabPage4.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown29)).EndInit();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown43)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown44)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown45)).EndInit();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown37)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown21)).EndInit();
            this.tabPage5.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown10)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nUDSans)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nUDSuni)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nUDSihir)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.NumericUpDown numericUpDown9;
        private System.Windows.Forms.NumericUpDown numericUpDown8;
        private System.Windows.Forms.NumericUpDown numericUpDown7;
        private System.Windows.Forms.NumericUpDown numericUpDown6;
        private System.Windows.Forms.NumericUpDown numericUpDown5;
        private System.Windows.Forms.NumericUpDown numericUpDown4;
        private System.Windows.Forms.NumericUpDown numericUpDown3;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.ComboBox comboBox5;
        private System.Windows.Forms.ComboBox comboBox4;
        private System.Windows.Forms.ComboBox cbFertigkeiten;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.ListBox listBox2;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.NumericUpDown numericUpDown10;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.NumericUpDown numericUpDown2;
        private System.Windows.Forms.NumericUpDown numericUpDown14;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.NumericUpDown numericUpDown15;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.NumericUpDown numericUpDown16;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.NumericUpDown numericUpDown17;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.NumericUpDown numericUpDown18;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.NumericUpDown numericUpDown19;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.NumericUpDown numericUpDown20;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.NumericUpDown numericUpDown21;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.NumericUpDown numericUpDown22;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.NumericUpDown numericUpDown23;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.NumericUpDown numericUpDown24;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.NumericUpDown numericUpDown25;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.NumericUpDown numericUpDown26;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.NumericUpDown numericUpDown27;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.NumericUpDown numericUpDown28;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.NumericUpDown numericUpDown29;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.NumericUpDown numericUpDown38;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.NumericUpDown numericUpDown39;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.NumericUpDown numericUpDown40;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.NumericUpDown numericUpDown41;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.NumericUpDown numericUpDown42;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.NumericUpDown numericUpDown43;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.NumericUpDown numericUpDown44;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.NumericUpDown numericUpDown45;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.NumericUpDown numericUpDown30;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.NumericUpDown numericUpDown31;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.NumericUpDown numericUpDown32;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.NumericUpDown numericUpDown33;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.NumericUpDown numericUpDown34;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.NumericUpDown numericUpDown35;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.NumericUpDown numericUpDown36;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.NumericUpDown numericUpDown37;
        private System.Windows.Forms.Label label90;
        private System.Windows.Forms.Label label91;
        private System.Windows.Forms.Label label92;
        private System.Windows.Forms.Label label93;
        private System.Windows.Forms.Label label86;
        private System.Windows.Forms.Label label87;
        private System.Windows.Forms.Label label88;
        private System.Windows.Forms.Label label89;
        private System.Windows.Forms.Label label82;
        private System.Windows.Forms.Label label83;
        private System.Windows.Forms.Label label84;
        private System.Windows.Forms.Label label85;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.Label label81;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.NumericUpDown nUDSans;
        private System.Windows.Forms.NumericUpDown nUDSuni;
        private System.Windows.Forms.NumericUpDown nUDSihir;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label lVerbleibendeNebelpunkte;
    }
}

