using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace Wheel_of_Time___Character_Sheets
{
    public partial class Points : UserControl
    {
        private int givenpoints = 0;
        private bool aktiv = true;
        private bool backgroundskill = false;
        private bool ogierbackgroundskill = false;

        public event EventHandler Blub;
        public event EventHandler OnDisposing;

        [CategoryAttribute("Attribute"),
        DescriptionAttribute("Das Blub")]
        public int Multiplier
        {
            get { return int.Parse(lMultiplier.Text); }
            set { lMultiplier.Text = value.ToString(); }
        }

        [CategoryAttribute("Attribute"),
        DescriptionAttribute("Das Blub")]
        public bool BackgroundSkill
        {
            get { return backgroundskill; }
            set
            {
                if (value)
                {
                    nudSkillLevel.Minimum = 4;
                    lGesamt.Text = ((nudSkillLevel.Value - 4) * int.Parse(lMultiplier.Text)).ToString();
                }
                else
                {
                    nudSkillLevel.Minimum = 0;
                    lGesamt.Text = (nudSkillLevel.Value * int.Parse(lMultiplier.Text)).ToString();
                }
                cbSkillName.AutoCheck = !value;
                backgroundskill = value;
            }
        }

        [CategoryAttribute("Attribute"),
        DescriptionAttribute("Das Blub")]
        public bool OgierBackgroundSkill
        {
            get { return ogierbackgroundskill; }
            set
            {
                if (value)
                {
                    nudSkillLevel.Minimum = 2;
                    lGesamt.Text = ((nudSkillLevel.Value - 2) * int.Parse(lMultiplier.Text)).ToString();
                }
                else
                {
                    nudSkillLevel.Minimum = 0;
                    lGesamt.Text = (nudSkillLevel.Value * int.Parse(lMultiplier.Text)).ToString();
                }
                cbSkillName.AutoCheck = !value;
                ogierbackgroundskill = value;
            }
        }

        [CategoryAttribute("Attribute"),
        DescriptionAttribute("Das Blub")]
        public string SkillName
        {
            get { return cbSkillName.Text; }
            set { cbSkillName.Text = value; }
        }

        [CategoryAttribute("Attribute"),
        DescriptionAttribute("Das Blub")]
        public int GivenPoints
        {
            get { return givenpoints; }
            set { CheckButtons(value); givenpoints = value; }
        }

        [CategoryAttribute("Attribute"),
        DescriptionAttribute("Das Blub")]
        public int UsedPoints
        {
            get { return int.Parse(lGesamt.Text); }
        }

        [CategoryAttribute("Attribute"),
        DescriptionAttribute("Das Blub")]
        public bool Aktiv
        {
            get { return aktiv; }
            set { SetActive(value); aktiv = value; }
        }

        public Points()
        {
            InitializeComponent();
        }

        public Points(string Text, int LocationY, EventHandler Blub, EventHandler OnDisposing)
        {
            InitializeComponent();
            SkillName = Text;
            this.Location = new Point(0, LocationY);
            this.Blub += Blub;
            this.OnDisposing += OnDisposing;
            bDelete.Enabled = true;
        }

        private void cbSkillName_CheckedChanged(object sender, EventArgs e)
        {
            if (cbSkillName.Checked == false)
            {
                nudSkillLevel.Value = 0;
            }
            else if (nudSkillLevel.Value == 0)
            {
                nudSkillLevel.Value = 1;
            }
        }

        private void SetActive(bool active)
        {
            if (!active)
            {
                nudSkillLevel.Value = 0;
            }
            cbSkillName.Enabled = active;
            nudSkillLevel.Enabled = active;
            radioButton1.Enabled = active;
            radioButton2.Enabled = active;
            radioButton3.Enabled = active;
            radioButton4.Enabled = active;
            radioButton5.Enabled = active;
            radioButton6.Enabled = active;
            radioButton7.Enabled = active;
            radioButton8.Enabled = active;
        }

        private void radioButton1_Click(object sender, EventArgs e)
        {
            if (!backgroundskill && !ogierbackgroundskill)
            {
                nudSkillLevel.Value = 1;
            }
        }

        private void radioButton2_Click(object sender, EventArgs e)
        {
            if (!backgroundskill)
            {
                nudSkillLevel.Value = 2;
            }
        }

        private void radioButton3_Click(object sender, EventArgs e)
        {
            if (!backgroundskill)
            {
                nudSkillLevel.Value = 3;
            }
        }

        private void radioButton4_Click(object sender, EventArgs e)
        {
            nudSkillLevel.Value = 4;
        }

        private void radioButton5_Click(object sender, EventArgs e)
        {
            nudSkillLevel.Value = 5;
        }

        private void radioButton6_Click(object sender, EventArgs e)
        {
            nudSkillLevel.Value = 6;
        }

        private void radioButton7_Click(object sender, EventArgs e)
        {
            nudSkillLevel.Value = 7;
        }

        private void radioButton8_Click(object sender, EventArgs e)
        {
            nudSkillLevel.Value = 8;
        }

        private void CheckButtons(int num)
        {
            switch (num)
            {
                case 0:
                    cbSkillName.Checked = false;
                    radioButton1.Checked = false;
                    radioButton2.Checked = false;
                    radioButton3.Checked = false;
                    radioButton4.Checked = false;
                    radioButton5.Checked = false;
                    radioButton6.Checked = false;
                    radioButton7.Checked = false;
                    radioButton8.Checked = false;
                    break;
                case 1:
                    cbSkillName.Checked = true;
                    radioButton1.Checked = true;
                    radioButton2.Checked = false;
                    radioButton3.Checked = false;
                    radioButton4.Checked = false;
                    radioButton5.Checked = false;
                    radioButton6.Checked = false;
                    radioButton7.Checked = false;
                    radioButton8.Checked = false;
                    break;
                case 2:
                    cbSkillName.Checked = true;
                    radioButton1.Checked = true;
                    radioButton2.Checked = true;
                    radioButton3.Checked = false;
                    radioButton4.Checked = false;
                    radioButton5.Checked = false;
                    radioButton6.Checked = false;
                    radioButton7.Checked = false;
                    radioButton8.Checked = false;
                    break;
                case 3:
                    cbSkillName.Checked = true;
                    radioButton1.Checked = true;
                    radioButton2.Checked = true;
                    radioButton3.Checked = true;
                    radioButton4.Checked = false;
                    radioButton5.Checked = false;
                    radioButton6.Checked = false;
                    radioButton7.Checked = false;
                    radioButton8.Checked = false;
                    break;
                case 4:
                    cbSkillName.Checked = true;
                    radioButton1.Checked = true;
                    radioButton2.Checked = true;
                    radioButton3.Checked = true;
                    radioButton4.Checked = true;
                    radioButton5.Checked = false;
                    radioButton6.Checked = false;
                    radioButton7.Checked = false;
                    radioButton8.Checked = false;
                    break;
                case 5:
                    cbSkillName.Checked = true;
                    radioButton1.Checked = true;
                    radioButton2.Checked = true;
                    radioButton3.Checked = true;
                    radioButton4.Checked = true;
                    radioButton5.Checked = true;
                    radioButton6.Checked = false;
                    radioButton7.Checked = false;
                    radioButton8.Checked = false;
                    break;
                case 6:
                    cbSkillName.Checked = true;
                    radioButton1.Checked = true;
                    radioButton2.Checked = true;
                    radioButton3.Checked = true;
                    radioButton4.Checked = true;
                    radioButton5.Checked = true;
                    radioButton6.Checked = true;
                    radioButton7.Checked = false;
                    radioButton8.Checked = false;
                    break;
                case 7:
                    cbSkillName.Checked = true;
                    radioButton1.Checked = true;
                    radioButton2.Checked = true;
                    radioButton3.Checked = true;
                    radioButton4.Checked = true;
                    radioButton5.Checked = true;
                    radioButton6.Checked = true;
                    radioButton7.Checked = true;
                    radioButton8.Checked = false;
                    break;
                case 8:
                    cbSkillName.Checked = true;
                    radioButton1.Checked = true;
                    radioButton2.Checked = true;
                    radioButton3.Checked = true;
                    radioButton4.Checked = true;
                    radioButton5.Checked = true;
                    radioButton6.Checked = true;
                    radioButton7.Checked = true;
                    radioButton8.Checked = true;
                    break;
                default:
                    break;
            }
        }

        private void nudSkillLevel_ValueChanged(object sender, EventArgs e)
        {
            GivenPoints = Convert.ToInt32(nudSkillLevel.Value);
            if (backgroundskill)
            { lGesamt.Text = ((nudSkillLevel.Value - 4) * int.Parse(lMultiplier.Text)).ToString(); }
            else if (ogierbackgroundskill)
            { lGesamt.Text = ((nudSkillLevel.Value - 2) * int.Parse(lMultiplier.Text)).ToString(); }
            else
            { lGesamt.Text = (nudSkillLevel.Value * int.Parse(lMultiplier.Text)).ToString(); }
            Blub(null, null);
        }

        private void bDelete_Click(object sender, EventArgs e)
        {
            OnDisposing(SkillName, null);
            this.Dispose();
        }
    }
}
