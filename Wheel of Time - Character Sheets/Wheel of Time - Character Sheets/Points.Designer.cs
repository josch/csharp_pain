namespace Wheel_of_Time___Character_Sheets
{
    partial class Points
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.radioButton4 = new System.Windows.Forms.RadioButton();
            this.radioButton5 = new System.Windows.Forms.RadioButton();
            this.radioButton6 = new System.Windows.Forms.RadioButton();
            this.radioButton7 = new System.Windows.Forms.RadioButton();
            this.radioButton8 = new System.Windows.Forms.RadioButton();
            this.lSkillName = new System.Windows.Forms.Label();
            this.cbSkillName = new System.Windows.Forms.CheckBox();
            this.nudSkillLevel = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.lMultiplier = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lGesamt = new System.Windows.Forms.Label();
            this.bDelete = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.nudSkillLevel)).BeginInit();
            this.SuspendLayout();
            // 
            // radioButton1
            // 
            this.radioButton1.AutoCheck = false;
            this.radioButton1.AutoSize = true;
            this.radioButton1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButton1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.radioButton1.Location = new System.Drawing.Point(142, 3);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(13, 12);
            this.radioButton1.TabIndex = 0;
            this.radioButton1.Click += new System.EventHandler(this.radioButton1_Click);
            // 
            // radioButton2
            // 
            this.radioButton2.AutoCheck = false;
            this.radioButton2.AutoSize = true;
            this.radioButton2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButton2.Location = new System.Drawing.Point(155, 3);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(13, 12);
            this.radioButton2.TabIndex = 1;
            this.radioButton2.Click += new System.EventHandler(this.radioButton2_Click);
            // 
            // radioButton3
            // 
            this.radioButton3.AutoCheck = false;
            this.radioButton3.AutoSize = true;
            this.radioButton3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButton3.Location = new System.Drawing.Point(168, 3);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(13, 12);
            this.radioButton3.TabIndex = 2;
            this.radioButton3.Click += new System.EventHandler(this.radioButton3_Click);
            // 
            // radioButton4
            // 
            this.radioButton4.AutoCheck = false;
            this.radioButton4.AutoSize = true;
            this.radioButton4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButton4.Location = new System.Drawing.Point(181, 3);
            this.radioButton4.Name = "radioButton4";
            this.radioButton4.Size = new System.Drawing.Size(13, 12);
            this.radioButton4.TabIndex = 3;
            this.radioButton4.Click += new System.EventHandler(this.radioButton4_Click);
            // 
            // radioButton5
            // 
            this.radioButton5.AutoCheck = false;
            this.radioButton5.AutoSize = true;
            this.radioButton5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButton5.Location = new System.Drawing.Point(194, 3);
            this.radioButton5.Name = "radioButton5";
            this.radioButton5.Size = new System.Drawing.Size(13, 12);
            this.radioButton5.TabIndex = 4;
            this.radioButton5.Click += new System.EventHandler(this.radioButton5_Click);
            // 
            // radioButton6
            // 
            this.radioButton6.AutoCheck = false;
            this.radioButton6.AutoSize = true;
            this.radioButton6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButton6.Location = new System.Drawing.Point(207, 3);
            this.radioButton6.Name = "radioButton6";
            this.radioButton6.Size = new System.Drawing.Size(13, 12);
            this.radioButton6.TabIndex = 5;
            this.radioButton6.Click += new System.EventHandler(this.radioButton6_Click);
            // 
            // radioButton7
            // 
            this.radioButton7.AutoCheck = false;
            this.radioButton7.AutoSize = true;
            this.radioButton7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButton7.Location = new System.Drawing.Point(220, 3);
            this.radioButton7.Name = "radioButton7";
            this.radioButton7.Size = new System.Drawing.Size(13, 12);
            this.radioButton7.TabIndex = 6;
            this.radioButton7.Click += new System.EventHandler(this.radioButton7_Click);
            // 
            // radioButton8
            // 
            this.radioButton8.AutoCheck = false;
            this.radioButton8.AutoSize = true;
            this.radioButton8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButton8.Location = new System.Drawing.Point(233, 3);
            this.radioButton8.Name = "radioButton8";
            this.radioButton8.Size = new System.Drawing.Size(13, 12);
            this.radioButton8.TabIndex = 7;
            this.radioButton8.Click += new System.EventHandler(this.radioButton8_Click);
            // 
            // lSkillName
            // 
            this.lSkillName.AutoSize = true;
            this.lSkillName.Location = new System.Drawing.Point(3, 3);
            this.lSkillName.Name = "lSkillName";
            this.lSkillName.Size = new System.Drawing.Size(0, 0);
            this.lSkillName.TabIndex = 8;
            // 
            // cbSkillName
            // 
            this.cbSkillName.AutoSize = true;
            this.cbSkillName.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbSkillName.Location = new System.Drawing.Point(4, 0);
            this.cbSkillName.Name = "cbSkillName";
            this.cbSkillName.Size = new System.Drawing.Size(12, 11);
            this.cbSkillName.TabIndex = 9;
            this.cbSkillName.CheckedChanged += new System.EventHandler(this.cbSkillName_CheckedChanged);
            // 
            // nudSkillLevel
            // 
            this.nudSkillLevel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nudSkillLevel.Location = new System.Drawing.Point(252, 0);
            this.nudSkillLevel.Maximum = new decimal(new int[] {
            8,
            0,
            0,
            0});
            this.nudSkillLevel.Name = "nudSkillLevel";
            this.nudSkillLevel.Size = new System.Drawing.Size(39, 20);
            this.nudSkillLevel.TabIndex = 10;
            this.nudSkillLevel.ValueChanged += new System.EventHandler(this.nudSkillLevel_ValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(297, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(8, 13);
            this.label1.TabIndex = 11;
            this.label1.Text = "x";
            // 
            // lMultiplier
            // 
            this.lMultiplier.AutoSize = true;
            this.lMultiplier.Location = new System.Drawing.Point(311, 3);
            this.lMultiplier.Name = "lMultiplier";
            this.lMultiplier.Size = new System.Drawing.Size(9, 13);
            this.lMultiplier.TabIndex = 12;
            this.lMultiplier.Text = "1";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(326, 3);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(9, 13);
            this.label3.TabIndex = 13;
            this.label3.Text = "=";
            // 
            // lGesamt
            // 
            this.lGesamt.AutoSize = true;
            this.lGesamt.Location = new System.Drawing.Point(341, 3);
            this.lGesamt.Name = "lGesamt";
            this.lGesamt.Size = new System.Drawing.Size(9, 13);
            this.lGesamt.TabIndex = 14;
            this.lGesamt.Text = "0";
            // 
            // bDelete
            // 
            this.bDelete.Enabled = false;
            this.bDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bDelete.Location = new System.Drawing.Point(362, 0);
            this.bDelete.Name = "bDelete";
            this.bDelete.Size = new System.Drawing.Size(35, 20);
            this.bDelete.TabIndex = 15;
            this.bDelete.Text = "DEL";
            this.bDelete.Click += new System.EventHandler(this.bDelete_Click);
            // 
            // Points
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.bDelete);
            this.Controls.Add(this.lGesamt);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lMultiplier);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.nudSkillLevel);
            this.Controls.Add(this.cbSkillName);
            this.Controls.Add(this.lSkillName);
            this.Controls.Add(this.radioButton8);
            this.Controls.Add(this.radioButton7);
            this.Controls.Add(this.radioButton6);
            this.Controls.Add(this.radioButton5);
            this.Controls.Add(this.radioButton4);
            this.Controls.Add(this.radioButton3);
            this.Controls.Add(this.radioButton2);
            this.Controls.Add(this.radioButton1);
            this.Name = "Points";
            this.Size = new System.Drawing.Size(397, 20);
            ((System.ComponentModel.ISupportInitialize)(this.nudSkillLevel)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.RadioButton radioButton4;
        private System.Windows.Forms.RadioButton radioButton5;
        private System.Windows.Forms.RadioButton radioButton6;
        private System.Windows.Forms.RadioButton radioButton7;
        private System.Windows.Forms.RadioButton radioButton8;
        private System.Windows.Forms.Label lSkillName;
        private System.Windows.Forms.CheckBox cbSkillName;
        private System.Windows.Forms.NumericUpDown nudSkillLevel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lMultiplier;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lGesamt;
        private System.Windows.Forms.Button bDelete;
    }
}
