using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Drawing.Imaging;
using System.Text;
using System.Windows.Forms;
using System.Xml;

namespace Wheel_of_Time___Character_Sheets
{
    public partial class ChildWnd : Form
    {
        public string filename;
        Char character = new Char();

        public ChildWnd(string file)
        {
            InitializeComponent();

            filename = file;

            if (filename != "")
            {
                XmlTextReader xmlreader = new XmlTextReader(filename);
                xmlreader.ReadStartElement("Wheel_of_Time_Character_Sheet");
                character.Name = xmlreader.ReadElementString("Name");
                character.MaxSkillPoints = int.Parse(xmlreader.ReadElementString("MaxSkillPoints"));
                character.Alter = int.Parse(xmlreader.ReadElementString("Alter"));
                string sAugen = xmlreader.ReadElementString("Augen");
                int iAugen = 0;
                if (int.TryParse(sAugen, out iAugen))
                { character.Augen = Color.FromArgb(iAugen); }
                else
                { character.Augen = Color.FromName(sAugen); }
                character.Charisma = int.Parse(xmlreader.ReadElementString("Charisma"));
                character.Chronik = xmlreader.ReadElementString("Chronik");
                character.Geschicklichkeit = int.Parse(xmlreader.ReadElementString("Geschicklichkeit"));
                character.Gewicht = int.Parse(xmlreader.ReadElementString("Gewicht"));
                character.Geschlecht = xmlreader.ReadElementString("Geschlecht");
                character.Größe = int.Parse(xmlreader.ReadElementString("Größe"));
                string sHaare = xmlreader.ReadElementString("Haare");
                int iHaare = 0;
                if (int.TryParse(sHaare, out iHaare))
                { character.Haare = Color.FromArgb(iHaare); }
                else
                { character.Haare = Color.FromName(sHaare); }
                string sHaut = xmlreader.ReadElementString("Haut");
                int iHaut = 0;
                if (int.TryParse(sHaut, out iHaut))
                { character.Haut = Color.FromArgb(iHaut); }
                else
                { character.Haut = Color.FromName(sHaut); }
                character.Intelligenz = int.Parse(xmlreader.ReadElementString("Intelligenz"));
                character.Background = xmlreader.ReadElementString("Background");
                character.Algai_d_Siswai = bool.Parse(xmlreader.ReadElementString("Algai_d_Siswai"));
                character.Armsman = bool.Parse(xmlreader.ReadElementString("Armsman"));
                character.Initiate = bool.Parse(xmlreader.ReadElementString("Initiate"));
                character.Noble = bool.Parse(xmlreader.ReadElementString("Noble"));
                character.Wanderer = bool.Parse(xmlreader.ReadElementString("Wanderer"));
                character.Wilder = bool.Parse(xmlreader.ReadElementString("Wilder"));
                character.Woodsman = bool.Parse(xmlreader.ReadElementString("Woodsman"));
                //character.Klasse = xmlreader.ReadElementString("Klasse");
                character.Konstitution = int.Parse(xmlreader.ReadElementString("Konstitution"));
                character.Level = xmlreader.ReadElementString("Level");
                character.Spieler = xmlreader.ReadElementString("Spieler");
                character.Stärke = int.Parse(xmlreader.ReadElementString("Stärke"));
                character.Weisheit = int.Parse(xmlreader.ReadElementString("Weisheit"));
                tbSpruch.Text = xmlreader.ReadElementString("Spruch");
                tbGeschichte.Text = xmlreader.ReadElementString("Geschichte");
                cbAir.Checked = bool.Parse(xmlreader.ReadElementString("Air"));
                cbBalefire.Checked = bool.Parse(xmlreader.ReadElementString("Balefire"));
                cbCloudDancing.Checked = bool.Parse(xmlreader.ReadElementString("CloudDancing"));
                cbConjunction.Checked = bool.Parse(xmlreader.ReadElementString("Conjunction"));
                cbEarth.Checked = bool.Parse(xmlreader.ReadElementString("Earth"));
                cbEarthSinging.Checked = bool.Parse(xmlreader.ReadElementString("EarthSinging"));
                cbElementalism.Checked = bool.Parse(xmlreader.ReadElementString("Elementalism"));
                cbFire.Checked = bool.Parse(xmlreader.ReadElementString("Fire"));
                cbHealing.Checked = bool.Parse(xmlreader.ReadElementString("Healing"));
                cbIllusion.Checked = bool.Parse(xmlreader.ReadElementString("Illusion"));
                cbSpirit.Checked = bool.Parse(xmlreader.ReadElementString("Spirit"));
                cbTravling.Checked = bool.Parse(xmlreader.ReadElementString("Traveling"));
                cbWarding.Checked = bool.Parse(xmlreader.ReadElementString("Warding"));
                cbWater.Checked = bool.Parse(xmlreader.ReadElementString("Water"));

                string SkillPoints = xmlreader.ReadElementString("SkillPoints");
                string[] SkillPointsArr = SkillPoints.Split('‼');
                int i = 0;
                foreach (Points p in SkillsPanel.Controls)
                {
                    p.GivenPoints = int.Parse(SkillPointsArr[i]);
                    i++;
                }
                UpdateSkillsPanel();
                PopulateBackgroundFeatsTab();
                PopulateBackgroundSkillsTab();

                string BackgroundSkills = xmlreader.ReadElementString("BackgroundSkills");
                string[] BackgroundSkillsArr = BackgroundSkills.Split('‼');
                if (BackgroundSkillsArr.Length > 1)
                {
                    foreach (CheckBox cb in tpBackgroundSkills.Controls)
                    {
                        if (cb.Text == BackgroundSkillsArr[0] || cb.Text == BackgroundSkillsArr[1])
                        {
                            cb.Checked = true;
                        }
                    }
                }

                string General_Feats = xmlreader.ReadElementString("General_Feats");
                string[] General_FeatsArr = General_Feats.Split('‼');
                int j = 0;
                foreach (Control c in gbGeneral_Feats.Controls)
                {
                    try
                    {
                        CheckBox cb = (CheckBox)c;
                        cb.Checked = bool.Parse(General_FeatsArr[j]);
                        j++;
                    }
                    catch { }
                }

                string Special_Feats = xmlreader.ReadElementString("Special_Feats");
                string[] Special_FeatsArr = Special_Feats.Split('‼');
                int k = 0;
                foreach (Control c in gbSpecial_Feats.Controls)
                {
                    try
                    {
                        CheckBox cb = (CheckBox)c;
                        cb.Checked = bool.Parse(Special_FeatsArr[k]);
                        k++;
                    }
                    catch { }
                }

                string Channeling_Feats = xmlreader.ReadElementString("Channeling_Feats");
                string[] Channeling_FeatsArr = Channeling_Feats.Split('‼');
                int l = 0;
                foreach (Control c in gbChanneling_Feats.Controls)
                {
                    try
                    {
                        CheckBox cb = (CheckBox)c;
                        cb.Checked = bool.Parse(Channeling_FeatsArr[l]);
                        l++;
                    }
                    catch { }
                }

                string Lost_Ability_Feats = xmlreader.ReadElementString("Lost_Ability_Feats");
                string[] Lost_Ability_FeatsArr = Lost_Ability_Feats.Split('‼');
                int m = 0;
                foreach (Control c in gbLost_Ability_Feats.Controls)
                {
                    try
                    {
                        CheckBox cb = (CheckBox)c;
                        cb.Checked = bool.Parse(Lost_Ability_FeatsArr[m]);
                        m++;
                    }
                    catch { }
                }

                string Background_Feats = xmlreader.ReadElementString("Background_Feats");
                string[] Background_FeatsArr = Background_Feats.Split('‼');
                foreach (CheckBox cb in tpBackground_Feats.Controls)
                {
                    if (cb.Text == Background_FeatsArr[0] || cb.Text == Background_FeatsArr[1])
                    {
                        cb.Checked = true;
                    }
                }
                tbExotic_Weapon_Proficiency.Text = xmlreader.ReadElementString("Exotic_Weapon_Proficiency");
                tbMartial_Weapon_Proficiency.Text = xmlreader.ReadElementString("Martial_Weapon_Proficiency");
                tbWeapon_Finesse.Text = xmlreader.ReadElementString("Weapon_Finesse");
                tbWeapon_Focus.Text = xmlreader.ReadElementString("Weapon_Focus");
                tbWeapon_Specializiation.Text = xmlreader.ReadElementString("Weapon_Specializiation");

                character.SkillsPath = xmlreader.ReadElementString("SkillsPath");

                if (character.SkillsPath != "")
                {
                    XmlTextReader xmlreaderskills = new XmlTextReader(character.SkillsPath);
                    xmlreaderskills.ReadStartElement("Wheel_of_Time_Time_Skills");
                    string Skills = xmlreaderskills.ReadElementString("Skills");
                    string[] SkillsArr = Skills.Split('‼');
                    while (SkillsPanel.Controls.Count > 44)
                    {
                        SkillsPanel.Controls.RemoveAt(44);
                    }
                    for (int n = 0; n < (SkillsArr.Length - 1); n++)
                    {
                        SkillsPanel.Controls.Add(new Points(SkillsArr[n], SkillsPanel.Controls.Count * 22 + 6, new EventHandler(pBlub_Blub), new EventHandler(pOnDisposing)));
                    }
                    xmlreaderskills.Close();
                }
                xmlreader.Close();

                this.Text = filename;
            }
            else
            {
                character.Charisma = 8;
                character.Geschicklichkeit = 8;
                character.Intelligenz = 8;
                character.Konstitution = 8;
                character.Stärke = 8;
                character.Weisheit = 8;
            }
            propertyGrid1.SelectedObject = character;
            UpdateSkillsPanel();
            printPreviewControl1.InvalidatePreview();
        }

        private void toolStripPrintButton_Click(object sender, EventArgs e)
        {
            if (printDialog1.ShowDialog() == DialogResult.OK)
            {
                printDocument1.Print();
            }
        }

        private void toolStripSaveButton1_Click(object sender, EventArgs e)
        {
            if (filename == "")
            {
                if (saveWOTFileDialog.ShowDialog() == DialogResult.OK)
                {
                    WriteWOT(saveWOTFileDialog.FileName);
                    filename = saveWOTFileDialog.FileName;
                    this.Text = filename;
                }
            }
            else
            {
                WriteWOT(filename);
            }
        }

        void WriteWOT(string file)
        {
            string SkillPoints = "";
            foreach (Points p in SkillsPanel.Controls)
            {
                SkillPoints += p.GivenPoints.ToString();
                SkillPoints += "‼";
            }
            string BackgroundSkills = "";
            foreach (CheckBox cb in tpBackgroundSkills.Controls)
            {
                if (cb.Checked)
                {
                    BackgroundSkills += cb.Text;
                    BackgroundSkills += "‼";
                }
            }

            string Background_Feats = "";
            foreach (CheckBox cb in tpBackground_Feats.Controls)
            {
                if (cb.Checked)
                {
                    Background_Feats += cb.Text;
                    Background_Feats += "‼";
                }
            }
            
            string General_Feats = "";
            foreach (Control c in gbGeneral_Feats.Controls)
            {
                try
                {
                    CheckBox cb = (CheckBox)c;
                    General_Feats += cb.Checked.ToString();
                    General_Feats += "‼";
                }
                catch {}
            }
            string Special_Feats = "";
            foreach (Control c in gbSpecial_Feats.Controls)
            {
                try
                {
                    CheckBox cb = (CheckBox)c;
                    Special_Feats += cb.Checked.ToString();
                    Special_Feats += "‼";
                }
                catch {}
            }
            string Channeling_Feats = "";
            foreach (Control c in gbChanneling_Feats.Controls)
            {
                try
                {
                    CheckBox cb = (CheckBox)c;
                    Channeling_Feats += cb.Checked.ToString();
                    Channeling_Feats += "‼";
                }
                catch {}
            }
            string Lost_Ability_Feats = "";
            foreach (Control c in gbLost_Ability_Feats.Controls)
            {
                try
                {
                    CheckBox cb = (CheckBox)c;
                    Lost_Ability_Feats += cb.Checked.ToString();
                    Lost_Ability_Feats += "‼";
                }
                catch { }
            }

            XmlTextWriter xmlwriter = new XmlTextWriter(file, null);
            xmlwriter.WriteStartDocument();
            xmlwriter.WriteStartElement("Wheel_of_Time_Character_Sheet");
            xmlwriter.WriteElementString("Name", character.Name);
            xmlwriter.WriteElementString("MaxSkillPoints", character.MaxSkillPoints.ToString());
            xmlwriter.WriteElementString("Alter", character.Alter.ToString());
            if (character.Augen.IsKnownColor)
            { xmlwriter.WriteElementString("Augen", character.Augen.ToKnownColor().ToString()); }
            else
            { xmlwriter.WriteElementString("Augen", character.Augen.ToArgb().ToString()); }
            xmlwriter.WriteElementString("Charisma", character.Charisma.ToString());
            xmlwriter.WriteElementString("Chronik", character.Chronik);
            xmlwriter.WriteElementString("Geschicklichkeit", character.Geschicklichkeit.ToString());
            xmlwriter.WriteElementString("Gewicht", character.Gewicht.ToString());
            xmlwriter.WriteElementString("Geschlecht", character.Geschlecht);
            xmlwriter.WriteElementString("Größe", character.Größe.ToString());
            if (character.Haare.IsKnownColor)
            { xmlwriter.WriteElementString("Haare", character.Haare.ToKnownColor().ToString()); }
            else
            { xmlwriter.WriteElementString("Haare", character.Haare.ToArgb().ToString()); }
            if (character.Haut.IsKnownColor)
            { xmlwriter.WriteElementString("Haut", character.Haut.ToKnownColor().ToString()); }
            else
            { xmlwriter.WriteElementString("Haut", character.Haut.ToArgb().ToString()); }
            xmlwriter.WriteElementString("Intelligenz", character.Intelligenz.ToString());
            xmlwriter.WriteElementString("Background", character.Background);
            xmlwriter.WriteElementString("Algai_d_Siswai", character.Algai_d_Siswai.ToString());
            xmlwriter.WriteElementString("Armsman", character.Armsman.ToString());
            xmlwriter.WriteElementString("Initiate", character.Initiate.ToString());
            xmlwriter.WriteElementString("Noble", character.Noble.ToString());
            xmlwriter.WriteElementString("Wanderer", character.Wanderer.ToString());
            xmlwriter.WriteElementString("Wilder", character.Wilder.ToString());
            xmlwriter.WriteElementString("Woodsman", character.Woodsman.ToString());
            //xmlwriter.WriteElementString("Klasse", character.Klasse);
            xmlwriter.WriteElementString("Konstitution", character.Konstitution.ToString());
            xmlwriter.WriteElementString("Level", character.Level);
            xmlwriter.WriteElementString("Spieler", character.Spieler);
            xmlwriter.WriteElementString("Stärke", character.Stärke.ToString());
            xmlwriter.WriteElementString("Weisheit", character.Weisheit.ToString());
            xmlwriter.WriteElementString("Spruch", tbSpruch.Text);
            xmlwriter.WriteElementString("Geschichte", tbGeschichte.Text);
            xmlwriter.WriteElementString("Air", cbAir.Checked.ToString());
            xmlwriter.WriteElementString("Balefire", cbBalefire.Checked.ToString());
            xmlwriter.WriteElementString("CloudDancing", cbCloudDancing.Checked.ToString());
            xmlwriter.WriteElementString("Conjunction", cbConjunction.Checked.ToString());
            xmlwriter.WriteElementString("Earth", cbEarth.Checked.ToString());
            xmlwriter.WriteElementString("EarthSinging", cbEarthSinging.Checked.ToString());
            xmlwriter.WriteElementString("Elementalism", cbElementalism.Checked.ToString());
            xmlwriter.WriteElementString("Fire", cbFire.Checked.ToString());
            xmlwriter.WriteElementString("Healing", cbHealing.Checked.ToString());
            xmlwriter.WriteElementString("Illusion", cbIllusion.Checked.ToString());
            xmlwriter.WriteElementString("Spirit", cbSpirit.Checked.ToString());
            xmlwriter.WriteElementString("Traveling", cbTravling.Checked.ToString());
            xmlwriter.WriteElementString("Warding", cbWarding.Checked.ToString());
            xmlwriter.WriteElementString("Water", cbWater.Checked.ToString());
            xmlwriter.WriteElementString("SkillPoints", SkillPoints);
            xmlwriter.WriteElementString("BackgroundSkills", BackgroundSkills);
            xmlwriter.WriteElementString("General_Feats", General_Feats);
            xmlwriter.WriteElementString("Special_Feats", Special_Feats);
            xmlwriter.WriteElementString("Channeling_Feats", Channeling_Feats);
            xmlwriter.WriteElementString("Lost_Ability_Feats", Lost_Ability_Feats);
            xmlwriter.WriteElementString("Background_Feats", Background_Feats);
            xmlwriter.WriteElementString("Exotic_Weapon_Proficiency", tbExotic_Weapon_Proficiency.Text);
            xmlwriter.WriteElementString("Martial_Weapon_Proficiency", tbMartial_Weapon_Proficiency.Text);
            xmlwriter.WriteElementString("Weapon_Finesse", tbWeapon_Finesse.Text);
            xmlwriter.WriteElementString("Weapon_Focus", tbWeapon_Focus.Text);
            xmlwriter.WriteElementString("Weapon_Specializiation", tbWeapon_Specializiation.Text);
            xmlwriter.WriteElementString("SkillsPath", character.SkillsPath);
            xmlwriter.WriteEndElement();
            xmlwriter.WriteEndDocument();
            xmlwriter.Close();
        }

        private void propertyGrid1_PropertyValueChanged(object s, PropertyValueChangedEventArgs e)
        {
            if (e.ChangedItem.Label == "Background")
            {
                if (character.Background == "Ogier")
                {
                    character.Charisma = 12;
                    character.Stärke = 12;
                    character.Konstitution = 12;
                    character.Geschicklichkeit = 4;
                    character.Algai_d_Siswai = false;
                    character.Armsman = false;
                    character.Initiate = false;
                    character.Noble = false;
                    character.Wanderer = false;
                    character.Wilder = false;
                    character.Woodsman = false;
                }
                else
                {
                    character.Charisma = 8;
                    character.Stärke = 8;
                    character.Konstitution = 8;
                    character.Geschicklichkeit = 8;
                }
            }
            if (e.ChangedItem.Label == "MaxSkillPoints")
            {
                lSkillGesamt.Text = character.MaxSkillPoints.ToString();
                CalculatePoints();
            }
            if (character.Background == "Ogier" && (e.ChangedItem.Label == "Algai_d_Siswai" || e.ChangedItem.Label == "Armsman" || e.ChangedItem.Label == "Initiate" || e.ChangedItem.Label == "Noble" || e.ChangedItem.Label == "Wanderer" || e.ChangedItem.Label == "Wilder" || e.ChangedItem.Label == "Woodsman"))
            {
                character.Algai_d_Siswai = false;
                character.Armsman = false;
                character.Initiate = false;
                character.Noble = false;
                character.Wanderer = false;
                character.Wilder = false;
                character.Woodsman = false;
            }
            if (character.Initiate || character.Wilder)
            {
                gbAffinities.Enabled = true;
                gbTalents.Enabled = true;
            }
            else
            {
                cbAir.Checked = false;
                cbBalefire.Checked = false;
                cbCloudDancing.Checked = false;
                cbConjunction.Checked = false;
                cbEarth.Checked = false;
                cbEarthSinging.Checked = false;
                cbElementalism.Checked = false;
                cbFire.Checked = false;
                cbHealing.Checked = false;
                cbIllusion.Checked = false;
                cbSpirit.Checked = false;
                cbTravling.Checked = false;
                cbWarding.Checked = false;
                cbWater.Checked = false;
                gbAffinities.Enabled = false;
                gbTalents.Enabled = false;
            }
            UpdateSkillsPanel();
            tpBackground_Feats.Controls.Clear();
            PopulateBackgroundFeatsTab();
            tpBackgroundSkills.Controls.Clear();
            PopulateBackgroundSkillsTab();
            printPreviewControl1.InvalidatePreview();
        }

        private void pBlub_Blub(object sender, EventArgs e)
        {
            CalculatePoints();
        }

        private void pOnDisposing(object sender, EventArgs e)
        {
            MessageBox.Show((string)sender);
        }

        void CalculatePoints()
        {
            int gesamt = 0;
            foreach(Points p in SkillsPanel.Controls)
            {
                gesamt += p.UsedPoints;
            }
            lSkillUsed.Text = gesamt.ToString();
            int rest = int.Parse(lSkillGesamt.Text) - gesamt;
            lSkillRest.Text = rest.ToString();
            if (rest < 0)
            {
                lSkillRest.ForeColor = Color.Red;
            }
            else
            {
                lSkillRest.ForeColor = Color.Black;
            }
        }

        void FeatsClick(object sender, EventArgs e)
        {
            CheckBox Sender = (CheckBox)sender;
            if (!Sender.Checked)
            {
                int numberofchecked = 0;
                foreach (CheckBox cb in tpBackground_Feats.Controls)
                {
                    if (cb.Checked)
                    {
                        numberofchecked++;
                    }
                }
                if (numberofchecked < 2)
                {
                    Sender.Checked = true;
                }
            }
            else
            {
                Sender.Checked = false;
            }
        }

        private void bSaveSkills_Click(object sender, EventArgs e)
        {
            if (saveSKLFileDialog.ShowDialog() == DialogResult.OK)
            {
                XmlTextWriter xmlwriter = new XmlTextWriter(saveSKLFileDialog.FileName, null);
                xmlwriter.WriteStartDocument();
                xmlwriter.WriteStartElement("Wheel_of_Time_Time_Skills");
                string Skills = "";
                for (int i = 44; i < SkillsPanel.Controls.Count; i++)
                {
                    Points p = (Points)SkillsPanel.Controls[i];
                    Skills += p.SkillName;
                    Skills += "‼";
                }
                xmlwriter.WriteElementString("Skills", Skills);
                xmlwriter.WriteEndElement();
                xmlwriter.WriteEndDocument();
                xmlwriter.Close();
                character.SkillsPath = saveSKLFileDialog.FileName;
            }
        }

        private void bLoadSkills_Click(object sender, EventArgs e)
        {
            if (openSKLFileDialog.ShowDialog() == DialogResult.OK)
            {
                XmlTextReader xmlreader = new XmlTextReader(openSKLFileDialog.FileName);
                xmlreader.ReadStartElement("Wheel_of_Time_Time_Skills");
                string Skills = xmlreader.ReadElementString("Skills");
                string[] SkillsArr = Skills.Split('‼');
                while (SkillsPanel.Controls.Count > 44)
                {
                    SkillsPanel.Controls.RemoveAt(44);
                }
                for (int i = 0; i < (SkillsArr.Length - 1); i++)
                {
                    SkillsPanel.Controls.Add(new Points(SkillsArr[i], SkillsPanel.Controls.Count * 22 + 6, new EventHandler(pBlub_Blub), new EventHandler(pOnDisposing)));
                }
            }
        }

        private void bClear_Click(object sender, EventArgs e)
        {
            while (SkillsPanel.Controls.Count > 44)
            {
                SkillsPanel.Controls.RemoveAt(44);
            }
        }

        private void bCreateSkill_Click(object sender, EventArgs e)
        {
            SkillsPanel.Controls.Add(new Points(tbSkillName.Text, SkillsPanel.Controls.Count * 22 + 6, new EventHandler(pBlub_Blub), new EventHandler(pOnDisposing)));
        }

        private void SkillsPanel_Leave(object sender, EventArgs e)
        {
            printPreviewControl1.InvalidatePreview();
        }

        private void tpBackgroundSkills_Leave(object sender, EventArgs e)
        {
            UpdateSkillsPanel();
            printPreviewControl1.InvalidatePreview();
        }

        private void tabPage2_Leave(object sender, EventArgs e)
        {
            printPreviewControl1.InvalidatePreview();
        }

        private void tpFeats_Leave(object sender, EventArgs e)
        {
            printPreviewControl1.InvalidatePreview();
        }

        private void bBackground_Click(object sender, EventArgs e)
        {
            printPreviewControl1.InvalidatePreview();
        }

        private void bPrintPreview_Click(object sender, EventArgs e)
        {
            printPreviewDialog1.TopLevel = true;
            printPreviewDialog1.ShowDialog();
        }

        private void bAktualisieren_Click(object sender, EventArgs e)
        {
            printPreviewControl1.InvalidatePreview();
        }

        private void bKopieren_Click(object sender, EventArgs e)
        {
            tbSpruch.Text = tbSprüche.SelectedText.Trim();
        }

        private void cbArmor_Proficiency_light_CheckedChanged(object sender, EventArgs e)
        {
            cbArmor_Proficiency_medium.AutoCheck = cbArmor_Proficiency_light.Checked;
            cbArmor_Proficiency_medium.Checked = false;
            cbImproved_Critical.AutoCheck = cbArmor_Proficiency_light.Checked;
            cbImproved_Critical.Checked = false;
        }

        private void cbArmor_Proficiency_medium_CheckedChanged(object sender, EventArgs e)
        {
            cbArmor_Proficiency_heavy.AutoCheck = cbArmor_Proficiency_medium.Checked;
            cbArmor_Proficiency_heavy.Checked = false;
        }

        private void cbCombat_Expertise_CheckedChanged(object sender, EventArgs e)
        {
            cbImproved_Disarm.AutoCheck = cbCombat_Expertise.Checked;
            cbImproved_Disarm.Checked = false;
            cbImproved_Trip.AutoCheck = cbCombat_Expertise.Checked;
            cbImproved_Trip.Checked = false;
            cbWhirlwind_Attack.AutoCheck = cbCombat_Expertise.Checked;
            cbWhirlwind_Attack.Checked = false;
        }

        private void cbDodge_CheckedChanged(object sender, EventArgs e)
        {
            cbMobility.AutoCheck = cbDodge.Checked;
            cbMobility.Checked = false;
        }

        private void cbMobility_CheckedChanged(object sender, EventArgs e)
        {
            cbSpring_Attack.AutoCheck = cbMobility.Checked;
            cbSpring_Attack.Checked = false;
        }

        private void cbExotic_Weapon_Proficiency_CheckedChanged(object sender, EventArgs e)
        {
            tbExotic_Weapon_Proficiency.Enabled = cbExotic_Weapon_Proficiency.Checked;
            tbExotic_Weapon_Proficiency.Text = "";
        }

        private void cbMartial_Weapon_Proficiency_CheckedChanged(object sender, EventArgs e)
        {
            tbMartial_Weapon_Proficiency.Enabled = cbMartial_Weapon_Proficiency.Checked;
            tbMartial_Weapon_Proficiency.Text = "";
        }

        private void cbMounted_Combat_CheckedChanged(object sender, EventArgs e)
        {
            cbMounted_Archery.AutoCheck = cbMounted_Combat.Checked;
            cbMounted_Archery.Checked = false;
            cbTrample.AutoCheck = cbMounted_Combat.Checked;
            cbTrample.Checked = false;
            cbRide_By_Attack.AutoCheck = cbMounted_Combat.Checked;
            cbRide_By_Attack.Checked = false;
        }

        private void cbPoint_Blank_Shot_CheckedChanged(object sender, EventArgs e)
        {
            cbFar_Shot.AutoCheck = cbPoint_Blank_Shot.Checked;
            cbFar_Shot.Checked = false;
            cbPrecise_Shot.AutoCheck = cbPoint_Blank_Shot.Checked;
            cbPrecise_Shot.Checked = false;
            cbRapid_Shot.AutoCheck = cbPoint_Blank_Shot.Checked;
            cbRapid_Shot.Checked = false;
            cbShot_on_the_Run.AutoCheck = cbPoint_Blank_Shot.Checked;
            cbShot_on_the_Run.Checked = false;
        }

        private void cbPower_Attack_CheckedChanged(object sender, EventArgs e)
        {
            cbCleave.AutoCheck = cbPower_Attack.Checked;
            cbCleave.Checked = false;
            cbImproved_Bull_Rush.AutoCheck = cbPower_Attack.Checked;
            cbImproved_Bull_Rush.Checked = false;
        }

        private void cbCleave_CheckedChanged(object sender, EventArgs e)
        {
            cbGreat_Cleave.AutoCheck = cbCleave.Checked;
            cbGreat_Cleave.Checked = false;
        }

        private void cbTwo_Weapon_Fighting_CheckedChanged(object sender, EventArgs e)
        {
            cbImproved_Two_Weapon_Fighting.AutoCheck = cbTwo_Weapon_Fighting.Checked;
            cbImproved_Two_Weapon_Fighting.Checked = false;
        }

        private void cbWeapon_Finesse_CheckedChanged(object sender, EventArgs e)
        {
            tbWeapon_Finesse.Enabled = cbWeapon_Finesse.Checked;
            tbWeapon_Finesse.Text = "";
        }

        private void cbWeapon_Focus_CheckedChanged(object sender, EventArgs e)
        {
            tbWeapon_Focus.Enabled = cbWeapon_Focus.Checked;
            tbWeapon_Focus.Text = "";
        }

        private void cbWeapon_Specializiation_CheckedChanged(object sender, EventArgs e)
        {
            tbWeapon_Specializiation.Enabled = cbWeapon_Specializiation.Checked;
            tbWeapon_Specializiation.Text = "";
        }

        private void cbLatent_Dreamer_CheckedChanged(object sender, EventArgs e)
        {
            cbDreamwalk.AutoCheck = cbLatent_Dreamer.Checked;
            cbDreamwalk.Checked = false;
            cbBend_Dream.AutoCheck = cbLatent_Dreamer.Checked;
            cbBend_Dream.Checked = false;
            cbDreamjump.AutoCheck = cbLatent_Dreamer.Checked;
            cbDreamjump.Checked = false;
            cbWaken_Dream.AutoCheck = cbLatent_Dreamer.Checked;
            cbWaken_Dream.Checked = false;
            cbDreamwatch.AutoCheck = cbLatent_Dreamer.Checked;
            cbDreamwatch.Checked = false;
        }

        private void cbLatent_Foreteller_CheckedChanged(object sender, EventArgs e)
        {
            cbForeteller.AutoCheck = cbLatent_Foreteller.Checked;
            cbForeteller.Checked = false;
        }

        private void cbLatent_Old_Blood_CheckedChanged(object sender, EventArgs e)
        {
            cbOld_Blood.AutoCheck = cbLatent_Old_Blood.Checked;
            cbOld_Blood.Checked = false;
        }

        private void cbLatent_Sniffer_CheckedChanged(object sender, EventArgs e)
        {
            cbSniffer.AutoCheck = cbLatent_Sniffer.Checked;
            cbSniffer.Checked = false;
        }

        private void cbLatent_Treesinger_CheckedChanged(object sender, EventArgs e)
        {
            cbTreesinger.AutoCheck = cbLatent_Treesinger.Checked;
            cbTreesinger.Checked = false;
        }

        private void tsbSaveAs_Click(object sender, EventArgs e)
        {
            if (saveWOTFileDialog.ShowDialog() == DialogResult.OK)
            {
                WriteWOT(saveWOTFileDialog.FileName);
                filename = saveWOTFileDialog.FileName;
                this.Text = filename;
            }
        }
    }
}