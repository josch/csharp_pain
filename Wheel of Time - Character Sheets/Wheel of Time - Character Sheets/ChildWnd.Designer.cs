namespace Wheel_of_Time___Character_Sheets
{
    partial class ChildWnd
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ChildWnd));
            this.printDocument1 = new System.Drawing.Printing.PrintDocument();
            this.printDialog1 = new System.Windows.Forms.PrintDialog();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripSaveButton1 = new System.Windows.Forms.ToolStripButton();
            this.tsbSaveAs = new System.Windows.Forms.ToolStripButton();
            this.toolStripPrintButton = new System.Windows.Forms.ToolStripButton();
            this.bBackground = new System.Windows.Forms.ToolStripButton();
            this.bPrintPreview = new System.Windows.Forms.ToolStripButton();
            this.bAktualisieren = new System.Windows.Forms.ToolStripButton();
            this.saveWOTFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.printPreviewControl1 = new System.Windows.Forms.PrintPreviewControl();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.propertyGrid1 = new System.Windows.Forms.PropertyGrid();
            this.tpBackgroundSkills = new System.Windows.Forms.TabPage();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.SkillsPanel = new System.Windows.Forms.Panel();
            this.pAnimal_Empathy = new Wheel_of_Time___Character_Sheets.Points();
            this.pAppraise = new Wheel_of_Time___Character_Sheets.Points();
            this.pBalance = new Wheel_of_Time___Character_Sheets.Points();
            this.pBluff = new Wheel_of_Time___Character_Sheets.Points();
            this.pClimb = new Wheel_of_Time___Character_Sheets.Points();
            this.pConcentration = new Wheel_of_Time___Character_Sheets.Points();
            this.pCraft = new Wheel_of_Time___Character_Sheets.Points();
            this.pDecipher_Script = new Wheel_of_Time___Character_Sheets.Points();
            this.pDiplomacy = new Wheel_of_Time___Character_Sheets.Points();
            this.pDisable_Device = new Wheel_of_Time___Character_Sheets.Points();
            this.pDisguise = new Wheel_of_Time___Character_Sheets.Points();
            this.pEscape_Artist = new Wheel_of_Time___Character_Sheets.Points();
            this.pForgery = new Wheel_of_Time___Character_Sheets.Points();
            this.pGather_Information = new Wheel_of_Time___Character_Sheets.Points();
            this.pHandle_Animal = new Wheel_of_Time___Character_Sheets.Points();
            this.pHeal = new Wheel_of_Time___Character_Sheets.Points();
            this.pHide = new Wheel_of_Time___Character_Sheets.Points();
            this.pInnuendo = new Wheel_of_Time___Character_Sheets.Points();
            this.pIntimidate = new Wheel_of_Time___Character_Sheets.Points();
            this.pIntuit_Direction = new Wheel_of_Time___Character_Sheets.Points();
            this.pJump = new Wheel_of_Time___Character_Sheets.Points();
            this.pKnowledge_Legends = new Wheel_of_Time___Character_Sheets.Points();
            this.pKnowledge_Arcana = new Wheel_of_Time___Character_Sheets.Points();
            this.pKnowledge_Arch = new Wheel_of_Time___Character_Sheets.Points();
            this.pKnowledge_Blight = new Wheel_of_Time___Character_Sheets.Points();
            this.pKnowledge_Geography = new Wheel_of_Time___Character_Sheets.Points();
            this.pKnowledge_History = new Wheel_of_Time___Character_Sheets.Points();
            this.pKnowledge_Local = new Wheel_of_Time___Character_Sheets.Points();
            this.pKnowledge_Nature = new Wheel_of_Time___Character_Sheets.Points();
            this.pKnowledge_Nobility = new Wheel_of_Time___Character_Sheets.Points();
            this.pListen = new Wheel_of_Time___Character_Sheets.Points();
            this.pMove_Silently = new Wheel_of_Time___Character_Sheets.Points();
            this.pOpen_Lock = new Wheel_of_Time___Character_Sheets.Points();
            this.pPick_Pocket = new Wheel_of_Time___Character_Sheets.Points();
            this.pRead_Lips = new Wheel_of_Time___Character_Sheets.Points();
            this.pRide = new Wheel_of_Time___Character_Sheets.Points();
            this.pSearch = new Wheel_of_Time___Character_Sheets.Points();
            this.pSense_Motive = new Wheel_of_Time___Character_Sheets.Points();
            this.pSpot = new Wheel_of_Time___Character_Sheets.Points();
            this.pSwim = new Wheel_of_Time___Character_Sheets.Points();
            this.pTumble = new Wheel_of_Time___Character_Sheets.Points();
            this.pUse_Rope = new Wheel_of_Time___Character_Sheets.Points();
            this.pWeavesight = new Wheel_of_Time___Character_Sheets.Points();
            this.pWilderness_Lore = new Wheel_of_Time___Character_Sheets.Points();
            this.panel2 = new System.Windows.Forms.Panel();
            this.bClear = new System.Windows.Forms.Button();
            this.bLoadSkills = new System.Windows.Forms.Button();
            this.bSaveSkills = new System.Windows.Forms.Button();
            this.bCreateSkill = new System.Windows.Forms.Button();
            this.tbSkillName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.lSkillRest = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lSkillUsed = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lSkillGesamt = new System.Windows.Forms.Label();
            this.tpBackground_Feats = new System.Windows.Forms.TabPage();
            this.tpFeats = new System.Windows.Forms.TabPage();
            this.gbLost_Ability_Feats = new System.Windows.Forms.GroupBox();
            this.cbLatent_Dreamer = new System.Windows.Forms.CheckBox();
            this.cbDreamwalk = new System.Windows.Forms.CheckBox();
            this.cbBend_Dream = new System.Windows.Forms.CheckBox();
            this.cbDreamjump = new System.Windows.Forms.CheckBox();
            this.cbWaken_Dream = new System.Windows.Forms.CheckBox();
            this.cbDreamwatch = new System.Windows.Forms.CheckBox();
            this.cbLatent_Foreteller = new System.Windows.Forms.CheckBox();
            this.cbForeteller = new System.Windows.Forms.CheckBox();
            this.cbLatent_Old_Blood = new System.Windows.Forms.CheckBox();
            this.cbOld_Blood = new System.Windows.Forms.CheckBox();
            this.cbLatent_Sniffer = new System.Windows.Forms.CheckBox();
            this.cbSniffer = new System.Windows.Forms.CheckBox();
            this.cbLatent_Treesinger = new System.Windows.Forms.CheckBox();
            this.cbTreesinger = new System.Windows.Forms.CheckBox();
            this.cbLatent_Viewer = new System.Windows.Forms.CheckBox();
            this.cbViewing = new System.Windows.Forms.CheckBox();
            this.gbChanneling_Feats = new System.Windows.Forms.GroupBox();
            this.cbCombat_Casting = new System.Windows.Forms.CheckBox();
            this.cbExtra_Affinity = new System.Windows.Forms.CheckBox();
            this.cbExtra_Talent = new System.Windows.Forms.CheckBox();
            this.cbMultiweave = new System.Windows.Forms.CheckBox();
            this.cbPower_Heightened_Senses = new System.Windows.Forms.CheckBox();
            this.cbSense_Residue = new System.Windows.Forms.CheckBox();
            this.cbTie_Off_Weave = new System.Windows.Forms.CheckBox();
            this.gbSpecial_Feats = new System.Windows.Forms.GroupBox();
            this.cbEliminate_Block = new System.Windows.Forms.CheckBox();
            this.cbWeapon_Specializiation = new System.Windows.Forms.CheckBox();
            this.tbWeapon_Specializiation = new System.Windows.Forms.TextBox();
            this.gbGeneral_Feats = new System.Windows.Forms.GroupBox();
            this.cbAlertness = new System.Windows.Forms.CheckBox();
            this.cbAmbidextrous = new System.Windows.Forms.CheckBox();
            this.cbAnimal_Affinity = new System.Windows.Forms.CheckBox();
            this.cbArmor_Proficiency_light = new System.Windows.Forms.CheckBox();
            this.cbArmor_Proficiency_medium = new System.Windows.Forms.CheckBox();
            this.cbArmor_Proficiency_heavy = new System.Windows.Forms.CheckBox();
            this.cbImproved_Critical = new System.Windows.Forms.CheckBox();
            this.cbAthletic = new System.Windows.Forms.CheckBox();
            this.cbBlind_Fight = new System.Windows.Forms.CheckBox();
            this.cbCombat_Expertise = new System.Windows.Forms.CheckBox();
            this.cbImproved_Disarm = new System.Windows.Forms.CheckBox();
            this.cbImproved_Trip = new System.Windows.Forms.CheckBox();
            this.cbWhirlwind_Attack = new System.Windows.Forms.CheckBox();
            this.cbCombat_Reflexes = new System.Windows.Forms.CheckBox();
            this.cbThe_Dark_One_s_own_Luck = new System.Windows.Forms.CheckBox();
            this.cbDodge = new System.Windows.Forms.CheckBox();
            this.cbMobility = new System.Windows.Forms.CheckBox();
            this.cbSpring_Attack = new System.Windows.Forms.CheckBox();
            this.cbEndurance = new System.Windows.Forms.CheckBox();
            this.cbExotic_Weapon_Proficiency = new System.Windows.Forms.CheckBox();
            this.cbFame = new System.Windows.Forms.CheckBox();
            this.cbGreat_Fortitude = new System.Windows.Forms.CheckBox();
            this.cbImproved_Initiative = new System.Windows.Forms.CheckBox();
            this.cbImproved_Unarmed_Strike = new System.Windows.Forms.CheckBox();
            this.cbIron_Will = new System.Windows.Forms.CheckBox();
            this.cbLightning_Reflexes = new System.Windows.Forms.CheckBox();
            this.cbMartial_Weapon_Proficiency = new System.Windows.Forms.CheckBox();
            this.cbMental_Stability = new System.Windows.Forms.CheckBox();
            this.cbMimic = new System.Windows.Forms.CheckBox();
            this.cbMounted_Combat = new System.Windows.Forms.CheckBox();
            this.cbMounted_Archery = new System.Windows.Forms.CheckBox();
            this.cbTrample = new System.Windows.Forms.CheckBox();
            this.cbRide_By_Attack = new System.Windows.Forms.CheckBox();
            this.cbNimble = new System.Windows.Forms.CheckBox();
            this.cbPersuasive = new System.Windows.Forms.CheckBox();
            this.cbPoint_Blank_Shot = new System.Windows.Forms.CheckBox();
            this.cbFar_Shot = new System.Windows.Forms.CheckBox();
            this.cbPrecise_Shot = new System.Windows.Forms.CheckBox();
            this.cbRapid_Shot = new System.Windows.Forms.CheckBox();
            this.cbShot_on_the_Run = new System.Windows.Forms.CheckBox();
            this.cbPower_Attack = new System.Windows.Forms.CheckBox();
            this.cbCleave = new System.Windows.Forms.CheckBox();
            this.cbGreat_Cleave = new System.Windows.Forms.CheckBox();
            this.cbImproved_Bull_Rush = new System.Windows.Forms.CheckBox();
            this.cbQuick_Draw = new System.Windows.Forms.CheckBox();
            this.cbQuickness = new System.Windows.Forms.CheckBox();
            this.cbRun = new System.Windows.Forms.CheckBox();
            this.cbSharp_eyed = new System.Windows.Forms.CheckBox();
            this.cbShield_Proficiency = new System.Windows.Forms.CheckBox();
            this.cbSimple_Weapon_Proficiency = new System.Windows.Forms.CheckBox();
            this.cbSkill_Emphasis = new System.Windows.Forms.CheckBox();
            this.cbStealthy = new System.Windows.Forms.CheckBox();
            this.cbToughness = new System.Windows.Forms.CheckBox();
            this.cbTrack = new System.Windows.Forms.CheckBox();
            this.cbTrustworthy = new System.Windows.Forms.CheckBox();
            this.cbTwo_Weapon_Fighting = new System.Windows.Forms.CheckBox();
            this.cbImproved_Two_Weapon_Fighting = new System.Windows.Forms.CheckBox();
            this.cbWeapon_Finesse = new System.Windows.Forms.CheckBox();
            this.cbWeapon_Focus = new System.Windows.Forms.CheckBox();
            this.tbWeapon_Focus = new System.Windows.Forms.TextBox();
            this.tbWeapon_Finesse = new System.Windows.Forms.TextBox();
            this.tbMartial_Weapon_Proficiency = new System.Windows.Forms.TextBox();
            this.tbExotic_Weapon_Proficiency = new System.Windows.Forms.TextBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.gbAffinities = new System.Windows.Forms.GroupBox();
            this.cbAir = new System.Windows.Forms.CheckBox();
            this.cbEarth = new System.Windows.Forms.CheckBox();
            this.cbWater = new System.Windows.Forms.CheckBox();
            this.cbFire = new System.Windows.Forms.CheckBox();
            this.cbSpirit = new System.Windows.Forms.CheckBox();
            this.gbTalents = new System.Windows.Forms.GroupBox();
            this.cbBalefire = new System.Windows.Forms.CheckBox();
            this.cbTravling = new System.Windows.Forms.CheckBox();
            this.cbIllusion = new System.Windows.Forms.CheckBox();
            this.cbWarding = new System.Windows.Forms.CheckBox();
            this.cbHealing = new System.Windows.Forms.CheckBox();
            this.cbCloudDancing = new System.Windows.Forms.CheckBox();
            this.cbElementalism = new System.Windows.Forms.CheckBox();
            this.cbEarthSinging = new System.Windows.Forms.CheckBox();
            this.cbConjunction = new System.Windows.Forms.CheckBox();
            this.tpHistory = new System.Windows.Forms.TabPage();
            this.tbGeschichte = new System.Windows.Forms.TextBox();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.tbSpr�che = new System.Windows.Forms.TextBox();
            this.bKopieren = new System.Windows.Forms.Button();
            this.tbSpruch = new System.Windows.Forms.TextBox();
            this.printPreviewDialog1 = new System.Windows.Forms.PrintPreviewDialog();
            this.saveSKLFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.openSKLFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.toolStrip1.SuspendLayout();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.SkillsPanel.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tpFeats.SuspendLayout();
            this.gbLost_Ability_Feats.SuspendLayout();
            this.gbChanneling_Feats.SuspendLayout();
            this.gbSpecial_Feats.SuspendLayout();
            this.gbGeneral_Feats.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.gbAffinities.SuspendLayout();
            this.gbTalents.SuspendLayout();
            this.tpHistory.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.SuspendLayout();
            // 
            // printDocument1
            // 
            this.printDocument1.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.PrintPreview);
            // 
            // printDialog1
            // 
            this.printDialog1.AllowSelection = true;
            this.printDialog1.AllowSomePages = true;
            this.printDialog1.Document = this.printDocument1;
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSaveButton1,
            this.tsbSaveAs,
            this.toolStripSeparator1,
            this.toolStripPrintButton,
            this.bBackground,
            this.bPrintPreview,
            this.bAktualisieren});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1170, 25);
            this.toolStrip1.TabIndex = 3;
            this.toolStrip1.Text = "toolStrip1";
            this.toolStrip1.Visible = false;
            // 
            // toolStripSaveButton1
            // 
            this.toolStripSaveButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripSaveButton1.Image")));
            this.toolStripSaveButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripSaveButton1.Name = "toolStripSaveButton1";
            this.toolStripSaveButton1.Text = "Speichern";
            this.toolStripSaveButton1.Click += new System.EventHandler(this.toolStripSaveButton1_Click);
            // 
            // tsbSaveAs
            // 
            this.tsbSaveAs.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsbSaveAs.Image = ((System.Drawing.Image)(resources.GetObject("tsbSaveAs.Image")));
            this.tsbSaveAs.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbSaveAs.Name = "tsbSaveAs";
            this.tsbSaveAs.Text = "Speichern unter";
            this.tsbSaveAs.Click += new System.EventHandler(this.tsbSaveAs_Click);
            // 
            // toolStripPrintButton
            // 
            this.toolStripPrintButton.Image = ((System.Drawing.Image)(resources.GetObject("toolStripPrintButton.Image")));
            this.toolStripPrintButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripPrintButton.Name = "toolStripPrintButton";
            this.toolStripPrintButton.Text = "Drucken";
            this.toolStripPrintButton.Click += new System.EventHandler(this.toolStripPrintButton_Click);
            // 
            // bBackground
            // 
            this.bBackground.CheckOnClick = true;
            this.bBackground.Image = ((System.Drawing.Image)(resources.GetObject("bBackground.Image")));
            this.bBackground.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bBackground.Name = "bBackground";
            this.bBackground.Text = "Background on/off";
            this.bBackground.Click += new System.EventHandler(this.bBackground_Click);
            // 
            // bPrintPreview
            // 
            this.bPrintPreview.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bPrintPreview.Image = ((System.Drawing.Image)(resources.GetObject("bPrintPreview.Image")));
            this.bPrintPreview.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bPrintPreview.Name = "bPrintPreview";
            this.bPrintPreview.Text = "Druckvorschau";
            this.bPrintPreview.Click += new System.EventHandler(this.bPrintPreview_Click);
            // 
            // bAktualisieren
            // 
            this.bAktualisieren.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bAktualisieren.Image = ((System.Drawing.Image)(resources.GetObject("bAktualisieren.Image")));
            this.bAktualisieren.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bAktualisieren.Name = "bAktualisieren";
            this.bAktualisieren.Text = "Aktualisieren";
            this.bAktualisieren.Click += new System.EventHandler(this.bAktualisieren_Click);
            // 
            // saveWOTFileDialog
            // 
            this.saveWOTFileDialog.DefaultExt = "wot";
            this.saveWOTFileDialog.Filter = "Wheel of Time Character Sheets (*.wot)|*.wot|Alle Dateien|*.*";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.printPreviewControl1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.tabControl1);
            this.splitContainer1.Size = new System.Drawing.Size(1170, 575);
            this.splitContainer1.SplitterDistance = 716;
            this.splitContainer1.TabIndex = 5;
            this.splitContainer1.Text = "splitContainer1";
            // 
            // printPreviewControl1
            // 
            this.printPreviewControl1.AutoZoom = false;
            this.printPreviewControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.printPreviewControl1.Document = this.printDocument1;
            this.printPreviewControl1.Location = new System.Drawing.Point(0, 0);
            this.printPreviewControl1.Name = "printPreviewControl1";
            this.printPreviewControl1.Rows = 2;
            this.printPreviewControl1.Size = new System.Drawing.Size(716, 575);
            this.printPreviewControl1.TabIndex = 0;
            this.printPreviewControl1.Zoom = 1;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tpFeats);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tpHistory);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tpBackgroundSkills);
            this.tabControl1.Controls.Add(this.tpBackground_Feats);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(450, 575);
            this.tabControl1.TabIndex = 16;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.propertyGrid1);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(442, 549);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Properties";
            // 
            // propertyGrid1
            // 
            this.propertyGrid1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.propertyGrid1.Location = new System.Drawing.Point(3, 3);
            this.propertyGrid1.Name = "propertyGrid1";
            this.propertyGrid1.Size = new System.Drawing.Size(436, 543);
            this.propertyGrid1.TabIndex = 2;
            this.propertyGrid1.ToolbarVisible = false;
            this.propertyGrid1.PropertyValueChanged += new System.Windows.Forms.PropertyValueChangedEventHandler(this.propertyGrid1_PropertyValueChanged);
            // 
            // tpBackgroundSkills
            // 
            this.tpBackgroundSkills.Location = new System.Drawing.Point(4, 22);
            this.tpBackgroundSkills.Name = "tpBackgroundSkills";
            this.tpBackgroundSkills.Size = new System.Drawing.Size(442, 549);
            this.tpBackgroundSkills.TabIndex = 4;
            this.tpBackgroundSkills.Text = "Background Skills";
            this.tpBackgroundSkills.Leave += new System.EventHandler(this.tpBackgroundSkills_Leave);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.SkillsPanel);
            this.tabPage1.Controls.Add(this.panel2);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(442, 549);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Skills";
            // 
            // SkillsPanel
            // 
            this.SkillsPanel.AutoScroll = true;
            this.SkillsPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.SkillsPanel.Controls.Add(this.pAnimal_Empathy);
            this.SkillsPanel.Controls.Add(this.pAppraise);
            this.SkillsPanel.Controls.Add(this.pBalance);
            this.SkillsPanel.Controls.Add(this.pBluff);
            this.SkillsPanel.Controls.Add(this.pClimb);
            this.SkillsPanel.Controls.Add(this.pConcentration);
            this.SkillsPanel.Controls.Add(this.pCraft);
            this.SkillsPanel.Controls.Add(this.pDecipher_Script);
            this.SkillsPanel.Controls.Add(this.pDiplomacy);
            this.SkillsPanel.Controls.Add(this.pDisable_Device);
            this.SkillsPanel.Controls.Add(this.pDisguise);
            this.SkillsPanel.Controls.Add(this.pEscape_Artist);
            this.SkillsPanel.Controls.Add(this.pForgery);
            this.SkillsPanel.Controls.Add(this.pGather_Information);
            this.SkillsPanel.Controls.Add(this.pHandle_Animal);
            this.SkillsPanel.Controls.Add(this.pHeal);
            this.SkillsPanel.Controls.Add(this.pHide);
            this.SkillsPanel.Controls.Add(this.pInnuendo);
            this.SkillsPanel.Controls.Add(this.pIntimidate);
            this.SkillsPanel.Controls.Add(this.pIntuit_Direction);
            this.SkillsPanel.Controls.Add(this.pJump);
            this.SkillsPanel.Controls.Add(this.pKnowledge_Legends);
            this.SkillsPanel.Controls.Add(this.pKnowledge_Arcana);
            this.SkillsPanel.Controls.Add(this.pKnowledge_Arch);
            this.SkillsPanel.Controls.Add(this.pKnowledge_Blight);
            this.SkillsPanel.Controls.Add(this.pKnowledge_Geography);
            this.SkillsPanel.Controls.Add(this.pKnowledge_History);
            this.SkillsPanel.Controls.Add(this.pKnowledge_Local);
            this.SkillsPanel.Controls.Add(this.pKnowledge_Nature);
            this.SkillsPanel.Controls.Add(this.pKnowledge_Nobility);
            this.SkillsPanel.Controls.Add(this.pListen);
            this.SkillsPanel.Controls.Add(this.pMove_Silently);
            this.SkillsPanel.Controls.Add(this.pOpen_Lock);
            this.SkillsPanel.Controls.Add(this.pPick_Pocket);
            this.SkillsPanel.Controls.Add(this.pRead_Lips);
            this.SkillsPanel.Controls.Add(this.pRide);
            this.SkillsPanel.Controls.Add(this.pSearch);
            this.SkillsPanel.Controls.Add(this.pSense_Motive);
            this.SkillsPanel.Controls.Add(this.pSpot);
            this.SkillsPanel.Controls.Add(this.pSwim);
            this.SkillsPanel.Controls.Add(this.pTumble);
            this.SkillsPanel.Controls.Add(this.pUse_Rope);
            this.SkillsPanel.Controls.Add(this.pWeavesight);
            this.SkillsPanel.Controls.Add(this.pWilderness_Lore);
            this.SkillsPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SkillsPanel.Location = new System.Drawing.Point(3, 57);
            this.SkillsPanel.Name = "SkillsPanel";
            this.SkillsPanel.Size = new System.Drawing.Size(436, 489);
            this.SkillsPanel.TabIndex = 16;
            this.SkillsPanel.Leave += new System.EventHandler(this.SkillsPanel_Leave);
            // 
            // pAnimal_Empathy
            // 
            this.pAnimal_Empathy.Aktiv = true;
            this.pAnimal_Empathy.BackgroundSkill = false;
            this.pAnimal_Empathy.GivenPoints = 0;
            this.pAnimal_Empathy.Location = new System.Drawing.Point(0, 6);
            this.pAnimal_Empathy.Multiplier = 1;
            this.pAnimal_Empathy.Name = "pAnimal_Empathy";
            this.pAnimal_Empathy.OgierBackgroundSkill = false;
            this.pAnimal_Empathy.Size = new System.Drawing.Size(397, 20);
            this.pAnimal_Empathy.SkillName = "Animal Empathy";
            this.pAnimal_Empathy.TabIndex = 0;
            this.pAnimal_Empathy.Blub += new System.EventHandler(this.pBlub_Blub);
            // 
            // pAppraise
            // 
            this.pAppraise.Aktiv = true;
            this.pAppraise.BackgroundSkill = false;
            this.pAppraise.GivenPoints = 0;
            this.pAppraise.Location = new System.Drawing.Point(0, 28);
            this.pAppraise.Multiplier = 1;
            this.pAppraise.Name = "pAppraise";
            this.pAppraise.OgierBackgroundSkill = false;
            this.pAppraise.Size = new System.Drawing.Size(397, 20);
            this.pAppraise.SkillName = "Appraise";
            this.pAppraise.TabIndex = 1;
            this.pAppraise.Blub += new System.EventHandler(this.pBlub_Blub);
            // 
            // pBalance
            // 
            this.pBalance.Aktiv = true;
            this.pBalance.BackgroundSkill = false;
            this.pBalance.GivenPoints = 0;
            this.pBalance.Location = new System.Drawing.Point(0, 50);
            this.pBalance.Multiplier = 1;
            this.pBalance.Name = "pBalance";
            this.pBalance.OgierBackgroundSkill = false;
            this.pBalance.Size = new System.Drawing.Size(397, 20);
            this.pBalance.SkillName = "Balance";
            this.pBalance.TabIndex = 2;
            this.pBalance.Blub += new System.EventHandler(this.pBlub_Blub);
            // 
            // pBluff
            // 
            this.pBluff.Aktiv = true;
            this.pBluff.BackgroundSkill = false;
            this.pBluff.GivenPoints = 0;
            this.pBluff.Location = new System.Drawing.Point(0, 72);
            this.pBluff.Multiplier = 1;
            this.pBluff.Name = "pBluff";
            this.pBluff.OgierBackgroundSkill = false;
            this.pBluff.Size = new System.Drawing.Size(397, 20);
            this.pBluff.SkillName = "Bluff";
            this.pBluff.TabIndex = 3;
            this.pBluff.Blub += new System.EventHandler(this.pBlub_Blub);
            // 
            // pClimb
            // 
            this.pClimb.Aktiv = true;
            this.pClimb.BackgroundSkill = false;
            this.pClimb.GivenPoints = 0;
            this.pClimb.Location = new System.Drawing.Point(0, 94);
            this.pClimb.Multiplier = 1;
            this.pClimb.Name = "pClimb";
            this.pClimb.OgierBackgroundSkill = false;
            this.pClimb.Size = new System.Drawing.Size(397, 20);
            this.pClimb.SkillName = "Climb";
            this.pClimb.TabIndex = 4;
            this.pClimb.Blub += new System.EventHandler(this.pBlub_Blub);
            // 
            // pConcentration
            // 
            this.pConcentration.Aktiv = true;
            this.pConcentration.BackgroundSkill = false;
            this.pConcentration.GivenPoints = 0;
            this.pConcentration.Location = new System.Drawing.Point(0, 116);
            this.pConcentration.Multiplier = 1;
            this.pConcentration.Name = "pConcentration";
            this.pConcentration.OgierBackgroundSkill = false;
            this.pConcentration.Size = new System.Drawing.Size(397, 20);
            this.pConcentration.SkillName = "Concentration";
            this.pConcentration.TabIndex = 6;
            this.pConcentration.Blub += new System.EventHandler(this.pBlub_Blub);
            // 
            // pCraft
            // 
            this.pCraft.Aktiv = true;
            this.pCraft.BackgroundSkill = false;
            this.pCraft.GivenPoints = 0;
            this.pCraft.Location = new System.Drawing.Point(0, 138);
            this.pCraft.Multiplier = 1;
            this.pCraft.Name = "pCraft";
            this.pCraft.OgierBackgroundSkill = false;
            this.pCraft.Size = new System.Drawing.Size(397, 20);
            this.pCraft.SkillName = "Craft";
            this.pCraft.TabIndex = 7;
            this.pCraft.Blub += new System.EventHandler(this.pBlub_Blub);
            // 
            // pDecipher_Script
            // 
            this.pDecipher_Script.Aktiv = true;
            this.pDecipher_Script.BackgroundSkill = false;
            this.pDecipher_Script.GivenPoints = 0;
            this.pDecipher_Script.Location = new System.Drawing.Point(0, 160);
            this.pDecipher_Script.Multiplier = 1;
            this.pDecipher_Script.Name = "pDecipher_Script";
            this.pDecipher_Script.OgierBackgroundSkill = false;
            this.pDecipher_Script.Size = new System.Drawing.Size(397, 20);
            this.pDecipher_Script.SkillName = "Decipher Script";
            this.pDecipher_Script.TabIndex = 8;
            this.pDecipher_Script.Blub += new System.EventHandler(this.pBlub_Blub);
            // 
            // pDiplomacy
            // 
            this.pDiplomacy.Aktiv = true;
            this.pDiplomacy.BackgroundSkill = false;
            this.pDiplomacy.GivenPoints = 0;
            this.pDiplomacy.Location = new System.Drawing.Point(0, 182);
            this.pDiplomacy.Multiplier = 1;
            this.pDiplomacy.Name = "pDiplomacy";
            this.pDiplomacy.OgierBackgroundSkill = false;
            this.pDiplomacy.Size = new System.Drawing.Size(397, 20);
            this.pDiplomacy.SkillName = "Diplomacy";
            this.pDiplomacy.TabIndex = 9;
            this.pDiplomacy.Blub += new System.EventHandler(this.pBlub_Blub);
            // 
            // pDisable_Device
            // 
            this.pDisable_Device.Aktiv = true;
            this.pDisable_Device.BackgroundSkill = false;
            this.pDisable_Device.GivenPoints = 0;
            this.pDisable_Device.Location = new System.Drawing.Point(0, 204);
            this.pDisable_Device.Multiplier = 1;
            this.pDisable_Device.Name = "pDisable_Device";
            this.pDisable_Device.OgierBackgroundSkill = false;
            this.pDisable_Device.Size = new System.Drawing.Size(397, 20);
            this.pDisable_Device.SkillName = "Disable Device";
            this.pDisable_Device.TabIndex = 10;
            this.pDisable_Device.Blub += new System.EventHandler(this.pBlub_Blub);
            // 
            // pDisguise
            // 
            this.pDisguise.Aktiv = true;
            this.pDisguise.BackgroundSkill = false;
            this.pDisguise.GivenPoints = 0;
            this.pDisguise.Location = new System.Drawing.Point(0, 226);
            this.pDisguise.Multiplier = 1;
            this.pDisguise.Name = "pDisguise";
            this.pDisguise.OgierBackgroundSkill = false;
            this.pDisguise.Size = new System.Drawing.Size(397, 20);
            this.pDisguise.SkillName = "Disguise";
            this.pDisguise.TabIndex = 11;
            this.pDisguise.Blub += new System.EventHandler(this.pBlub_Blub);
            // 
            // pEscape_Artist
            // 
            this.pEscape_Artist.Aktiv = true;
            this.pEscape_Artist.BackgroundSkill = false;
            this.pEscape_Artist.GivenPoints = 0;
            this.pEscape_Artist.Location = new System.Drawing.Point(0, 248);
            this.pEscape_Artist.Multiplier = 1;
            this.pEscape_Artist.Name = "pEscape_Artist";
            this.pEscape_Artist.OgierBackgroundSkill = false;
            this.pEscape_Artist.Size = new System.Drawing.Size(397, 20);
            this.pEscape_Artist.SkillName = "Escape Artist";
            this.pEscape_Artist.TabIndex = 12;
            this.pEscape_Artist.Blub += new System.EventHandler(this.pBlub_Blub);
            // 
            // pForgery
            // 
            this.pForgery.Aktiv = true;
            this.pForgery.BackgroundSkill = false;
            this.pForgery.GivenPoints = 0;
            this.pForgery.Location = new System.Drawing.Point(0, 270);
            this.pForgery.Multiplier = 1;
            this.pForgery.Name = "pForgery";
            this.pForgery.OgierBackgroundSkill = false;
            this.pForgery.Size = new System.Drawing.Size(397, 20);
            this.pForgery.SkillName = "Forgery";
            this.pForgery.TabIndex = 14;
            this.pForgery.Blub += new System.EventHandler(this.pBlub_Blub);
            // 
            // pGather_Information
            // 
            this.pGather_Information.Aktiv = true;
            this.pGather_Information.BackgroundSkill = false;
            this.pGather_Information.GivenPoints = 0;
            this.pGather_Information.Location = new System.Drawing.Point(0, 292);
            this.pGather_Information.Multiplier = 1;
            this.pGather_Information.Name = "pGather_Information";
            this.pGather_Information.OgierBackgroundSkill = false;
            this.pGather_Information.Size = new System.Drawing.Size(397, 20);
            this.pGather_Information.SkillName = "Gather Information";
            this.pGather_Information.TabIndex = 15;
            this.pGather_Information.Blub += new System.EventHandler(this.pBlub_Blub);
            // 
            // pHandle_Animal
            // 
            this.pHandle_Animal.Aktiv = true;
            this.pHandle_Animal.BackgroundSkill = false;
            this.pHandle_Animal.GivenPoints = 0;
            this.pHandle_Animal.Location = new System.Drawing.Point(0, 314);
            this.pHandle_Animal.Multiplier = 1;
            this.pHandle_Animal.Name = "pHandle_Animal";
            this.pHandle_Animal.OgierBackgroundSkill = false;
            this.pHandle_Animal.Size = new System.Drawing.Size(397, 20);
            this.pHandle_Animal.SkillName = "Handle Animal";
            this.pHandle_Animal.TabIndex = 16;
            this.pHandle_Animal.Blub += new System.EventHandler(this.pBlub_Blub);
            // 
            // pHeal
            // 
            this.pHeal.Aktiv = true;
            this.pHeal.BackgroundSkill = false;
            this.pHeal.GivenPoints = 0;
            this.pHeal.Location = new System.Drawing.Point(0, 336);
            this.pHeal.Multiplier = 1;
            this.pHeal.Name = "pHeal";
            this.pHeal.OgierBackgroundSkill = false;
            this.pHeal.Size = new System.Drawing.Size(397, 20);
            this.pHeal.SkillName = "Heal";
            this.pHeal.TabIndex = 17;
            this.pHeal.Blub += new System.EventHandler(this.pBlub_Blub);
            // 
            // pHide
            // 
            this.pHide.Aktiv = true;
            this.pHide.BackgroundSkill = false;
            this.pHide.GivenPoints = 0;
            this.pHide.Location = new System.Drawing.Point(0, 358);
            this.pHide.Multiplier = 1;
            this.pHide.Name = "pHide";
            this.pHide.OgierBackgroundSkill = false;
            this.pHide.Size = new System.Drawing.Size(397, 20);
            this.pHide.SkillName = "Hide";
            this.pHide.TabIndex = 18;
            this.pHide.Blub += new System.EventHandler(this.pBlub_Blub);
            // 
            // pInnuendo
            // 
            this.pInnuendo.Aktiv = true;
            this.pInnuendo.BackgroundSkill = false;
            this.pInnuendo.GivenPoints = 0;
            this.pInnuendo.Location = new System.Drawing.Point(0, 380);
            this.pInnuendo.Multiplier = 1;
            this.pInnuendo.Name = "pInnuendo";
            this.pInnuendo.OgierBackgroundSkill = false;
            this.pInnuendo.Size = new System.Drawing.Size(397, 20);
            this.pInnuendo.SkillName = "Innuendo";
            this.pInnuendo.TabIndex = 19;
            this.pInnuendo.Blub += new System.EventHandler(this.pBlub_Blub);
            // 
            // pIntimidate
            // 
            this.pIntimidate.Aktiv = true;
            this.pIntimidate.BackgroundSkill = false;
            this.pIntimidate.GivenPoints = 0;
            this.pIntimidate.Location = new System.Drawing.Point(0, 402);
            this.pIntimidate.Multiplier = 1;
            this.pIntimidate.Name = "pIntimidate";
            this.pIntimidate.OgierBackgroundSkill = false;
            this.pIntimidate.Size = new System.Drawing.Size(397, 20);
            this.pIntimidate.SkillName = "Intimidate";
            this.pIntimidate.TabIndex = 20;
            this.pIntimidate.Blub += new System.EventHandler(this.pBlub_Blub);
            // 
            // pIntuit_Direction
            // 
            this.pIntuit_Direction.Aktiv = true;
            this.pIntuit_Direction.BackgroundSkill = false;
            this.pIntuit_Direction.GivenPoints = 0;
            this.pIntuit_Direction.Location = new System.Drawing.Point(0, 424);
            this.pIntuit_Direction.Multiplier = 1;
            this.pIntuit_Direction.Name = "pIntuit_Direction";
            this.pIntuit_Direction.OgierBackgroundSkill = false;
            this.pIntuit_Direction.Size = new System.Drawing.Size(397, 20);
            this.pIntuit_Direction.SkillName = "Intuit Direction";
            this.pIntuit_Direction.TabIndex = 21;
            this.pIntuit_Direction.Blub += new System.EventHandler(this.pBlub_Blub);
            // 
            // pJump
            // 
            this.pJump.Aktiv = true;
            this.pJump.BackgroundSkill = false;
            this.pJump.GivenPoints = 0;
            this.pJump.Location = new System.Drawing.Point(0, 446);
            this.pJump.Multiplier = 1;
            this.pJump.Name = "pJump";
            this.pJump.OgierBackgroundSkill = false;
            this.pJump.Size = new System.Drawing.Size(397, 20);
            this.pJump.SkillName = "Jump";
            this.pJump.TabIndex = 22;
            this.pJump.Blub += new System.EventHandler(this.pBlub_Blub);
            // 
            // pKnowledge_Legends
            // 
            this.pKnowledge_Legends.Aktiv = true;
            this.pKnowledge_Legends.BackgroundSkill = false;
            this.pKnowledge_Legends.GivenPoints = 0;
            this.pKnowledge_Legends.Location = new System.Drawing.Point(0, 468);
            this.pKnowledge_Legends.Multiplier = 1;
            this.pKnowledge_Legends.Name = "pKnowledge_Legends";
            this.pKnowledge_Legends.OgierBackgroundSkill = false;
            this.pKnowledge_Legends.Size = new System.Drawing.Size(397, 20);
            this.pKnowledge_Legends.SkillName = "Knowledge (Legends)";
            this.pKnowledge_Legends.TabIndex = 23;
            this.pKnowledge_Legends.Blub += new System.EventHandler(this.pBlub_Blub);
            // 
            // pKnowledge_Arcana
            // 
            this.pKnowledge_Arcana.Aktiv = true;
            this.pKnowledge_Arcana.BackgroundSkill = false;
            this.pKnowledge_Arcana.GivenPoints = 0;
            this.pKnowledge_Arcana.Location = new System.Drawing.Point(0, 490);
            this.pKnowledge_Arcana.Multiplier = 1;
            this.pKnowledge_Arcana.Name = "pKnowledge_Arcana";
            this.pKnowledge_Arcana.OgierBackgroundSkill = false;
            this.pKnowledge_Arcana.Size = new System.Drawing.Size(397, 20);
            this.pKnowledge_Arcana.SkillName = "Knowledge (Arcana)";
            this.pKnowledge_Arcana.TabIndex = 24;
            this.pKnowledge_Arcana.Blub += new System.EventHandler(this.pBlub_Blub);
            // 
            // pKnowledge_Arch
            // 
            this.pKnowledge_Arch.Aktiv = true;
            this.pKnowledge_Arch.BackgroundSkill = false;
            this.pKnowledge_Arch.GivenPoints = 0;
            this.pKnowledge_Arch.Location = new System.Drawing.Point(0, 512);
            this.pKnowledge_Arch.Multiplier = 1;
            this.pKnowledge_Arch.Name = "pKnowledge_Arch";
            this.pKnowledge_Arch.OgierBackgroundSkill = false;
            this.pKnowledge_Arch.Size = new System.Drawing.Size(397, 20);
            this.pKnowledge_Arch.SkillName = "Knowledge (Arch)";
            this.pKnowledge_Arch.TabIndex = 25;
            this.pKnowledge_Arch.Blub += new System.EventHandler(this.pBlub_Blub);
            // 
            // pKnowledge_Blight
            // 
            this.pKnowledge_Blight.Aktiv = true;
            this.pKnowledge_Blight.BackgroundSkill = false;
            this.pKnowledge_Blight.GivenPoints = 0;
            this.pKnowledge_Blight.Location = new System.Drawing.Point(0, 534);
            this.pKnowledge_Blight.Multiplier = 1;
            this.pKnowledge_Blight.Name = "pKnowledge_Blight";
            this.pKnowledge_Blight.OgierBackgroundSkill = false;
            this.pKnowledge_Blight.Size = new System.Drawing.Size(397, 20);
            this.pKnowledge_Blight.SkillName = "Knowledge (Blight)";
            this.pKnowledge_Blight.TabIndex = 26;
            this.pKnowledge_Blight.Blub += new System.EventHandler(this.pBlub_Blub);
            // 
            // pKnowledge_Geography
            // 
            this.pKnowledge_Geography.Aktiv = true;
            this.pKnowledge_Geography.BackgroundSkill = false;
            this.pKnowledge_Geography.GivenPoints = 0;
            this.pKnowledge_Geography.Location = new System.Drawing.Point(0, 556);
            this.pKnowledge_Geography.Multiplier = 1;
            this.pKnowledge_Geography.Name = "pKnowledge_Geography";
            this.pKnowledge_Geography.OgierBackgroundSkill = false;
            this.pKnowledge_Geography.Size = new System.Drawing.Size(397, 20);
            this.pKnowledge_Geography.SkillName = "Knowledge (Geography)";
            this.pKnowledge_Geography.TabIndex = 27;
            this.pKnowledge_Geography.Blub += new System.EventHandler(this.pBlub_Blub);
            // 
            // pKnowledge_History
            // 
            this.pKnowledge_History.Aktiv = true;
            this.pKnowledge_History.BackgroundSkill = false;
            this.pKnowledge_History.GivenPoints = 0;
            this.pKnowledge_History.Location = new System.Drawing.Point(0, 578);
            this.pKnowledge_History.Multiplier = 1;
            this.pKnowledge_History.Name = "pKnowledge_History";
            this.pKnowledge_History.OgierBackgroundSkill = false;
            this.pKnowledge_History.Size = new System.Drawing.Size(397, 20);
            this.pKnowledge_History.SkillName = "Knowledge (History)";
            this.pKnowledge_History.TabIndex = 28;
            this.pKnowledge_History.Blub += new System.EventHandler(this.pBlub_Blub);
            // 
            // pKnowledge_Local
            // 
            this.pKnowledge_Local.Aktiv = true;
            this.pKnowledge_Local.BackgroundSkill = false;
            this.pKnowledge_Local.GivenPoints = 0;
            this.pKnowledge_Local.Location = new System.Drawing.Point(0, 600);
            this.pKnowledge_Local.Multiplier = 1;
            this.pKnowledge_Local.Name = "pKnowledge_Local";
            this.pKnowledge_Local.OgierBackgroundSkill = false;
            this.pKnowledge_Local.Size = new System.Drawing.Size(397, 20);
            this.pKnowledge_Local.SkillName = "Knowledge (Local)";
            this.pKnowledge_Local.TabIndex = 29;
            this.pKnowledge_Local.Blub += new System.EventHandler(this.pBlub_Blub);
            // 
            // pKnowledge_Nature
            // 
            this.pKnowledge_Nature.Aktiv = true;
            this.pKnowledge_Nature.BackgroundSkill = false;
            this.pKnowledge_Nature.GivenPoints = 0;
            this.pKnowledge_Nature.Location = new System.Drawing.Point(0, 622);
            this.pKnowledge_Nature.Multiplier = 1;
            this.pKnowledge_Nature.Name = "pKnowledge_Nature";
            this.pKnowledge_Nature.OgierBackgroundSkill = false;
            this.pKnowledge_Nature.Size = new System.Drawing.Size(397, 20);
            this.pKnowledge_Nature.SkillName = "Knowledge (Nature)";
            this.pKnowledge_Nature.TabIndex = 30;
            this.pKnowledge_Nature.Blub += new System.EventHandler(this.pBlub_Blub);
            // 
            // pKnowledge_Nobility
            // 
            this.pKnowledge_Nobility.Aktiv = true;
            this.pKnowledge_Nobility.BackgroundSkill = false;
            this.pKnowledge_Nobility.GivenPoints = 0;
            this.pKnowledge_Nobility.Location = new System.Drawing.Point(0, 644);
            this.pKnowledge_Nobility.Multiplier = 1;
            this.pKnowledge_Nobility.Name = "pKnowledge_Nobility";
            this.pKnowledge_Nobility.OgierBackgroundSkill = false;
            this.pKnowledge_Nobility.Size = new System.Drawing.Size(397, 20);
            this.pKnowledge_Nobility.SkillName = "Knowledge (Nobility)";
            this.pKnowledge_Nobility.TabIndex = 31;
            this.pKnowledge_Nobility.Blub += new System.EventHandler(this.pBlub_Blub);
            // 
            // pListen
            // 
            this.pListen.Aktiv = true;
            this.pListen.BackgroundSkill = false;
            this.pListen.GivenPoints = 0;
            this.pListen.Location = new System.Drawing.Point(0, 666);
            this.pListen.Multiplier = 1;
            this.pListen.Name = "pListen";
            this.pListen.OgierBackgroundSkill = false;
            this.pListen.Size = new System.Drawing.Size(397, 20);
            this.pListen.SkillName = "Listen";
            this.pListen.TabIndex = 32;
            this.pListen.Blub += new System.EventHandler(this.pBlub_Blub);
            // 
            // pMove_Silently
            // 
            this.pMove_Silently.Aktiv = true;
            this.pMove_Silently.BackgroundSkill = false;
            this.pMove_Silently.GivenPoints = 0;
            this.pMove_Silently.Location = new System.Drawing.Point(0, 688);
            this.pMove_Silently.Multiplier = 1;
            this.pMove_Silently.Name = "pMove_Silently";
            this.pMove_Silently.OgierBackgroundSkill = false;
            this.pMove_Silently.Size = new System.Drawing.Size(397, 20);
            this.pMove_Silently.SkillName = "Move Silently";
            this.pMove_Silently.TabIndex = 33;
            this.pMove_Silently.Blub += new System.EventHandler(this.pBlub_Blub);
            // 
            // pOpen_Lock
            // 
            this.pOpen_Lock.Aktiv = true;
            this.pOpen_Lock.BackgroundSkill = false;
            this.pOpen_Lock.GivenPoints = 0;
            this.pOpen_Lock.Location = new System.Drawing.Point(0, 710);
            this.pOpen_Lock.Multiplier = 1;
            this.pOpen_Lock.Name = "pOpen_Lock";
            this.pOpen_Lock.OgierBackgroundSkill = false;
            this.pOpen_Lock.Size = new System.Drawing.Size(397, 20);
            this.pOpen_Lock.SkillName = "Open Lock";
            this.pOpen_Lock.TabIndex = 34;
            this.pOpen_Lock.Blub += new System.EventHandler(this.pBlub_Blub);
            // 
            // pPick_Pocket
            // 
            this.pPick_Pocket.Aktiv = true;
            this.pPick_Pocket.BackgroundSkill = false;
            this.pPick_Pocket.GivenPoints = 0;
            this.pPick_Pocket.Location = new System.Drawing.Point(0, 732);
            this.pPick_Pocket.Multiplier = 1;
            this.pPick_Pocket.Name = "pPick_Pocket";
            this.pPick_Pocket.OgierBackgroundSkill = false;
            this.pPick_Pocket.Size = new System.Drawing.Size(397, 20);
            this.pPick_Pocket.SkillName = "Pick Pocket";
            this.pPick_Pocket.TabIndex = 35;
            this.pPick_Pocket.Blub += new System.EventHandler(this.pBlub_Blub);
            // 
            // pRead_Lips
            // 
            this.pRead_Lips.Aktiv = true;
            this.pRead_Lips.BackgroundSkill = false;
            this.pRead_Lips.GivenPoints = 0;
            this.pRead_Lips.Location = new System.Drawing.Point(0, 754);
            this.pRead_Lips.Multiplier = 1;
            this.pRead_Lips.Name = "pRead_Lips";
            this.pRead_Lips.OgierBackgroundSkill = false;
            this.pRead_Lips.Size = new System.Drawing.Size(397, 20);
            this.pRead_Lips.SkillName = "Read Lips";
            this.pRead_Lips.TabIndex = 36;
            this.pRead_Lips.Blub += new System.EventHandler(this.pBlub_Blub);
            // 
            // pRide
            // 
            this.pRide.Aktiv = true;
            this.pRide.BackgroundSkill = false;
            this.pRide.GivenPoints = 0;
            this.pRide.Location = new System.Drawing.Point(0, 776);
            this.pRide.Multiplier = 1;
            this.pRide.Name = "pRide";
            this.pRide.OgierBackgroundSkill = false;
            this.pRide.Size = new System.Drawing.Size(397, 20);
            this.pRide.SkillName = "Ride";
            this.pRide.TabIndex = 37;
            this.pRide.Blub += new System.EventHandler(this.pBlub_Blub);
            // 
            // pSearch
            // 
            this.pSearch.Aktiv = true;
            this.pSearch.BackgroundSkill = false;
            this.pSearch.GivenPoints = 0;
            this.pSearch.Location = new System.Drawing.Point(0, 798);
            this.pSearch.Multiplier = 1;
            this.pSearch.Name = "pSearch";
            this.pSearch.OgierBackgroundSkill = false;
            this.pSearch.Size = new System.Drawing.Size(397, 20);
            this.pSearch.SkillName = "Search";
            this.pSearch.TabIndex = 38;
            this.pSearch.Blub += new System.EventHandler(this.pBlub_Blub);
            // 
            // pSense_Motive
            // 
            this.pSense_Motive.Aktiv = true;
            this.pSense_Motive.BackgroundSkill = false;
            this.pSense_Motive.GivenPoints = 0;
            this.pSense_Motive.Location = new System.Drawing.Point(0, 820);
            this.pSense_Motive.Multiplier = 1;
            this.pSense_Motive.Name = "pSense_Motive";
            this.pSense_Motive.OgierBackgroundSkill = false;
            this.pSense_Motive.Size = new System.Drawing.Size(397, 20);
            this.pSense_Motive.SkillName = "Sense Motive";
            this.pSense_Motive.TabIndex = 39;
            this.pSense_Motive.Blub += new System.EventHandler(this.pBlub_Blub);
            // 
            // pSpot
            // 
            this.pSpot.Aktiv = true;
            this.pSpot.BackgroundSkill = false;
            this.pSpot.GivenPoints = 0;
            this.pSpot.Location = new System.Drawing.Point(0, 842);
            this.pSpot.Multiplier = 1;
            this.pSpot.Name = "pSpot";
            this.pSpot.OgierBackgroundSkill = false;
            this.pSpot.Size = new System.Drawing.Size(397, 20);
            this.pSpot.SkillName = "Spot";
            this.pSpot.TabIndex = 40;
            this.pSpot.Blub += new System.EventHandler(this.pBlub_Blub);
            // 
            // pSwim
            // 
            this.pSwim.Aktiv = true;
            this.pSwim.BackgroundSkill = false;
            this.pSwim.GivenPoints = 0;
            this.pSwim.Location = new System.Drawing.Point(0, 864);
            this.pSwim.Multiplier = 1;
            this.pSwim.Name = "pSwim";
            this.pSwim.OgierBackgroundSkill = false;
            this.pSwim.Size = new System.Drawing.Size(397, 20);
            this.pSwim.SkillName = "Swim";
            this.pSwim.TabIndex = 41;
            this.pSwim.Blub += new System.EventHandler(this.pBlub_Blub);
            // 
            // pTumble
            // 
            this.pTumble.Aktiv = true;
            this.pTumble.BackgroundSkill = false;
            this.pTumble.GivenPoints = 0;
            this.pTumble.Location = new System.Drawing.Point(0, 886);
            this.pTumble.Multiplier = 1;
            this.pTumble.Name = "pTumble";
            this.pTumble.OgierBackgroundSkill = false;
            this.pTumble.Size = new System.Drawing.Size(397, 20);
            this.pTumble.SkillName = "Tumble";
            this.pTumble.TabIndex = 42;
            this.pTumble.Blub += new System.EventHandler(this.pBlub_Blub);
            // 
            // pUse_Rope
            // 
            this.pUse_Rope.Aktiv = true;
            this.pUse_Rope.BackgroundSkill = false;
            this.pUse_Rope.GivenPoints = 0;
            this.pUse_Rope.Location = new System.Drawing.Point(0, 908);
            this.pUse_Rope.Multiplier = 1;
            this.pUse_Rope.Name = "pUse_Rope";
            this.pUse_Rope.OgierBackgroundSkill = false;
            this.pUse_Rope.Size = new System.Drawing.Size(397, 20);
            this.pUse_Rope.SkillName = "Use Rope";
            this.pUse_Rope.TabIndex = 44;
            this.pUse_Rope.Blub += new System.EventHandler(this.pBlub_Blub);
            // 
            // pWeavesight
            // 
            this.pWeavesight.Aktiv = true;
            this.pWeavesight.BackgroundSkill = false;
            this.pWeavesight.GivenPoints = 0;
            this.pWeavesight.Location = new System.Drawing.Point(0, 930);
            this.pWeavesight.Multiplier = 1;
            this.pWeavesight.Name = "pWeavesight";
            this.pWeavesight.OgierBackgroundSkill = false;
            this.pWeavesight.Size = new System.Drawing.Size(397, 20);
            this.pWeavesight.SkillName = "Weavesight";
            this.pWeavesight.TabIndex = 45;
            this.pWeavesight.Blub += new System.EventHandler(this.pBlub_Blub);
            // 
            // pWilderness_Lore
            // 
            this.pWilderness_Lore.Aktiv = true;
            this.pWilderness_Lore.BackgroundSkill = false;
            this.pWilderness_Lore.GivenPoints = 0;
            this.pWilderness_Lore.Location = new System.Drawing.Point(0, 952);
            this.pWilderness_Lore.Multiplier = 1;
            this.pWilderness_Lore.Name = "pWilderness_Lore";
            this.pWilderness_Lore.OgierBackgroundSkill = false;
            this.pWilderness_Lore.Size = new System.Drawing.Size(397, 20);
            this.pWilderness_Lore.SkillName = "Wilderness Lore";
            this.pWilderness_Lore.TabIndex = 46;
            this.pWilderness_Lore.Blub += new System.EventHandler(this.pBlub_Blub);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.bClear);
            this.panel2.Controls.Add(this.bLoadSkills);
            this.panel2.Controls.Add(this.bSaveSkills);
            this.panel2.Controls.Add(this.bCreateSkill);
            this.panel2.Controls.Add(this.tbSkillName);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.lSkillRest);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.lSkillUsed);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.lSkillGesamt);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(3, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(436, 54);
            this.panel2.TabIndex = 14;
            // 
            // bClear
            // 
            this.bClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bClear.Location = new System.Drawing.Point(194, 28);
            this.bClear.Name = "bClear";
            this.bClear.Size = new System.Drawing.Size(75, 20);
            this.bClear.TabIndex = 18;
            this.bClear.Text = "Clear Custom";
            this.bClear.Click += new System.EventHandler(this.bClear_Click);
            // 
            // bLoadSkills
            // 
            this.bLoadSkills.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bLoadSkills.Location = new System.Drawing.Point(356, 28);
            this.bLoadSkills.Name = "bLoadSkills";
            this.bLoadSkills.Size = new System.Drawing.Size(75, 20);
            this.bLoadSkills.TabIndex = 17;
            this.bLoadSkills.Text = "Load";
            this.bLoadSkills.Click += new System.EventHandler(this.bLoadSkills_Click);
            // 
            // bSaveSkills
            // 
            this.bSaveSkills.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bSaveSkills.Location = new System.Drawing.Point(275, 28);
            this.bSaveSkills.Name = "bSaveSkills";
            this.bSaveSkills.Size = new System.Drawing.Size(75, 20);
            this.bSaveSkills.TabIndex = 16;
            this.bSaveSkills.Text = "Save";
            this.bSaveSkills.Click += new System.EventHandler(this.bSaveSkills_Click);
            // 
            // bCreateSkill
            // 
            this.bCreateSkill.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bCreateSkill.Location = new System.Drawing.Point(343, 6);
            this.bCreateSkill.Name = "bCreateSkill";
            this.bCreateSkill.Size = new System.Drawing.Size(42, 20);
            this.bCreateSkill.TabIndex = 15;
            this.bCreateSkill.Text = "Neu";
            this.bCreateSkill.Click += new System.EventHandler(this.bCreateSkill_Click);
            // 
            // tbSkillName
            // 
            this.tbSkillName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbSkillName.Location = new System.Drawing.Point(209, 6);
            this.tbSkillName.Name = "tbSkillName";
            this.tbSkillName.Size = new System.Drawing.Size(128, 20);
            this.tbSkillName.TabIndex = 14;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 32);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(123, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Verbleibende Skillpunkte:";
            // 
            // lSkillRest
            // 
            this.lSkillRest.AutoSize = true;
            this.lSkillRest.Location = new System.Drawing.Point(166, 32);
            this.lSkillRest.Name = "lSkillRest";
            this.lSkillRest.Size = new System.Drawing.Size(9, 13);
            this.lSkillRest.TabIndex = 13;
            this.lSkillRest.Text = "0";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(157, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Gesamte verf�gabre Skillpunkte:";
            // 
            // lSkillUsed
            // 
            this.lSkillUsed.AutoSize = true;
            this.lSkillUsed.Location = new System.Drawing.Point(166, 19);
            this.lSkillUsed.Name = "lSkillUsed";
            this.lSkillUsed.Size = new System.Drawing.Size(9, 13);
            this.lSkillUsed.TabIndex = 12;
            this.lSkillUsed.Text = "0";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(119, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Verbrauchte Skillpunkte:";
            // 
            // lSkillGesamt
            // 
            this.lSkillGesamt.AutoSize = true;
            this.lSkillGesamt.Location = new System.Drawing.Point(166, 6);
            this.lSkillGesamt.Name = "lSkillGesamt";
            this.lSkillGesamt.Size = new System.Drawing.Size(15, 13);
            this.lSkillGesamt.TabIndex = 11;
            this.lSkillGesamt.Text = "40";
            // 
            // tpBackground_Feats
            // 
            this.tpBackground_Feats.Location = new System.Drawing.Point(4, 22);
            this.tpBackground_Feats.Name = "tpBackground_Feats";
            this.tpBackground_Feats.Padding = new System.Windows.Forms.Padding(3);
            this.tpBackground_Feats.Size = new System.Drawing.Size(442, 549);
            this.tpBackground_Feats.TabIndex = 3;
            this.tpBackground_Feats.Text = "Background Feats";
            this.tpBackground_Feats.Leave += new System.EventHandler(this.tpFeats_Leave);
            // 
            // tpFeats
            // 
            this.tpFeats.AutoScroll = true;
            this.tpFeats.Controls.Add(this.gbLost_Ability_Feats);
            this.tpFeats.Controls.Add(this.gbChanneling_Feats);
            this.tpFeats.Controls.Add(this.gbSpecial_Feats);
            this.tpFeats.Controls.Add(this.gbGeneral_Feats);
            this.tpFeats.Location = new System.Drawing.Point(4, 22);
            this.tpFeats.Name = "tpFeats";
            this.tpFeats.Padding = new System.Windows.Forms.Padding(3);
            this.tpFeats.Size = new System.Drawing.Size(442, 549);
            this.tpFeats.TabIndex = 7;
            this.tpFeats.Text = "Feats";
            // 
            // gbLost_Ability_Feats
            // 
            this.gbLost_Ability_Feats.Controls.Add(this.cbLatent_Dreamer);
            this.gbLost_Ability_Feats.Controls.Add(this.cbDreamwalk);
            this.gbLost_Ability_Feats.Controls.Add(this.cbBend_Dream);
            this.gbLost_Ability_Feats.Controls.Add(this.cbDreamjump);
            this.gbLost_Ability_Feats.Controls.Add(this.cbWaken_Dream);
            this.gbLost_Ability_Feats.Controls.Add(this.cbDreamwatch);
            this.gbLost_Ability_Feats.Controls.Add(this.cbLatent_Foreteller);
            this.gbLost_Ability_Feats.Controls.Add(this.cbForeteller);
            this.gbLost_Ability_Feats.Controls.Add(this.cbLatent_Old_Blood);
            this.gbLost_Ability_Feats.Controls.Add(this.cbOld_Blood);
            this.gbLost_Ability_Feats.Controls.Add(this.cbLatent_Sniffer);
            this.gbLost_Ability_Feats.Controls.Add(this.cbSniffer);
            this.gbLost_Ability_Feats.Controls.Add(this.cbLatent_Treesinger);
            this.gbLost_Ability_Feats.Controls.Add(this.cbTreesinger);
            this.gbLost_Ability_Feats.Controls.Add(this.cbLatent_Viewer);
            this.gbLost_Ability_Feats.Controls.Add(this.cbViewing);
            this.gbLost_Ability_Feats.Dock = System.Windows.Forms.DockStyle.Top;
            this.gbLost_Ability_Feats.Location = new System.Drawing.Point(3, 1626);
            this.gbLost_Ability_Feats.Name = "gbLost_Ability_Feats";
            this.gbLost_Ability_Feats.Size = new System.Drawing.Size(420, 384);
            this.gbLost_Ability_Feats.TabIndex = 3;
            this.gbLost_Ability_Feats.TabStop = false;
            this.gbLost_Ability_Feats.Text = "Lost Ability Feats";
            // 
            // cbLatent_Dreamer
            // 
            this.cbLatent_Dreamer.AutoSize = true;
            this.cbLatent_Dreamer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbLatent_Dreamer.Location = new System.Drawing.Point(6, 19);
            this.cbLatent_Dreamer.Name = "cbLatent_Dreamer";
            this.cbLatent_Dreamer.Size = new System.Drawing.Size(92, 17);
            this.cbLatent_Dreamer.TabIndex = 0;
            this.cbLatent_Dreamer.Text = "Latent Dreamer";
            this.cbLatent_Dreamer.CheckedChanged += new System.EventHandler(this.cbLatent_Dreamer_CheckedChanged);
            // 
            // cbDreamwalk
            // 
            this.cbDreamwalk.AutoCheck = false;
            this.cbDreamwalk.AutoSize = true;
            this.cbDreamwalk.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbDreamwalk.Location = new System.Drawing.Point(26, 42);
            this.cbDreamwalk.Name = "cbDreamwalk";
            this.cbDreamwalk.Size = new System.Drawing.Size(72, 17);
            this.cbDreamwalk.TabIndex = 1;
            this.cbDreamwalk.Text = "Dreamwalk";
            // 
            // cbBend_Dream
            // 
            this.cbBend_Dream.AutoCheck = false;
            this.cbBend_Dream.AutoSize = true;
            this.cbBend_Dream.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbBend_Dream.Location = new System.Drawing.Point(26, 65);
            this.cbBend_Dream.Name = "cbBend_Dream";
            this.cbBend_Dream.Size = new System.Drawing.Size(78, 17);
            this.cbBend_Dream.TabIndex = 2;
            this.cbBend_Dream.Text = "Bend Dream";
            // 
            // cbDreamjump
            // 
            this.cbDreamjump.AutoCheck = false;
            this.cbDreamjump.AutoSize = true;
            this.cbDreamjump.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbDreamjump.Location = new System.Drawing.Point(26, 88);
            this.cbDreamjump.Name = "cbDreamjump";
            this.cbDreamjump.Size = new System.Drawing.Size(72, 17);
            this.cbDreamjump.TabIndex = 3;
            this.cbDreamjump.Text = "Dreamjump";
            // 
            // cbWaken_Dream
            // 
            this.cbWaken_Dream.AutoCheck = false;
            this.cbWaken_Dream.AutoSize = true;
            this.cbWaken_Dream.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbWaken_Dream.Location = new System.Drawing.Point(26, 111);
            this.cbWaken_Dream.Name = "cbWaken_Dream";
            this.cbWaken_Dream.Size = new System.Drawing.Size(88, 17);
            this.cbWaken_Dream.TabIndex = 4;
            this.cbWaken_Dream.Text = "Waken Dream";
            // 
            // cbDreamwatch
            // 
            this.cbDreamwatch.AutoCheck = false;
            this.cbDreamwatch.AutoSize = true;
            this.cbDreamwatch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbDreamwatch.Location = new System.Drawing.Point(26, 134);
            this.cbDreamwatch.Name = "cbDreamwatch";
            this.cbDreamwatch.Size = new System.Drawing.Size(79, 17);
            this.cbDreamwatch.TabIndex = 5;
            this.cbDreamwatch.Text = "Dreamwatch";
            // 
            // cbLatent_Foreteller
            // 
            this.cbLatent_Foreteller.AutoSize = true;
            this.cbLatent_Foreteller.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbLatent_Foreteller.Location = new System.Drawing.Point(6, 157);
            this.cbLatent_Foreteller.Name = "cbLatent_Foreteller";
            this.cbLatent_Foreteller.Size = new System.Drawing.Size(95, 17);
            this.cbLatent_Foreteller.TabIndex = 6;
            this.cbLatent_Foreteller.Text = "Latent Foreteller";
            this.cbLatent_Foreteller.CheckedChanged += new System.EventHandler(this.cbLatent_Foreteller_CheckedChanged);
            // 
            // cbForeteller
            // 
            this.cbForeteller.AutoCheck = false;
            this.cbForeteller.AutoSize = true;
            this.cbForeteller.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbForeteller.Location = new System.Drawing.Point(26, 180);
            this.cbForeteller.Name = "cbForeteller";
            this.cbForeteller.Size = new System.Drawing.Size(62, 17);
            this.cbForeteller.TabIndex = 7;
            this.cbForeteller.Text = "Foreteller";
            // 
            // cbLatent_Old_Blood
            // 
            this.cbLatent_Old_Blood.AutoSize = true;
            this.cbLatent_Old_Blood.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbLatent_Old_Blood.Location = new System.Drawing.Point(6, 203);
            this.cbLatent_Old_Blood.Name = "cbLatent_Old_Blood";
            this.cbLatent_Old_Blood.Size = new System.Drawing.Size(98, 17);
            this.cbLatent_Old_Blood.TabIndex = 8;
            this.cbLatent_Old_Blood.Text = "Latent Old Blood";
            this.cbLatent_Old_Blood.CheckedChanged += new System.EventHandler(this.cbLatent_Old_Blood_CheckedChanged);
            // 
            // cbOld_Blood
            // 
            this.cbOld_Blood.AutoCheck = false;
            this.cbOld_Blood.AutoSize = true;
            this.cbOld_Blood.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbOld_Blood.Location = new System.Drawing.Point(26, 226);
            this.cbOld_Blood.Name = "cbOld_Blood";
            this.cbOld_Blood.Size = new System.Drawing.Size(65, 17);
            this.cbOld_Blood.TabIndex = 9;
            this.cbOld_Blood.Text = "Old Blood";
            // 
            // cbLatent_Sniffer
            // 
            this.cbLatent_Sniffer.AutoSize = true;
            this.cbLatent_Sniffer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbLatent_Sniffer.Location = new System.Drawing.Point(6, 249);
            this.cbLatent_Sniffer.Name = "cbLatent_Sniffer";
            this.cbLatent_Sniffer.Size = new System.Drawing.Size(82, 17);
            this.cbLatent_Sniffer.TabIndex = 10;
            this.cbLatent_Sniffer.Text = "Latent Sniffer";
            this.cbLatent_Sniffer.CheckedChanged += new System.EventHandler(this.cbLatent_Sniffer_CheckedChanged);
            // 
            // cbSniffer
            // 
            this.cbSniffer.AutoCheck = false;
            this.cbSniffer.AutoSize = true;
            this.cbSniffer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbSniffer.Location = new System.Drawing.Point(26, 272);
            this.cbSniffer.Name = "cbSniffer";
            this.cbSniffer.Size = new System.Drawing.Size(49, 17);
            this.cbSniffer.TabIndex = 11;
            this.cbSniffer.Text = "Sniffer";
            // 
            // cbLatent_Treesinger
            // 
            this.cbLatent_Treesinger.AutoSize = true;
            this.cbLatent_Treesinger.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbLatent_Treesinger.Location = new System.Drawing.Point(6, 295);
            this.cbLatent_Treesinger.Name = "cbLatent_Treesinger";
            this.cbLatent_Treesinger.Size = new System.Drawing.Size(102, 17);
            this.cbLatent_Treesinger.TabIndex = 12;
            this.cbLatent_Treesinger.Text = "Latent Treesinger";
            this.cbLatent_Treesinger.CheckedChanged += new System.EventHandler(this.cbLatent_Treesinger_CheckedChanged);
            // 
            // cbTreesinger
            // 
            this.cbTreesinger.AutoCheck = false;
            this.cbTreesinger.AutoSize = true;
            this.cbTreesinger.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbTreesinger.Location = new System.Drawing.Point(26, 318);
            this.cbTreesinger.Name = "cbTreesinger";
            this.cbTreesinger.Size = new System.Drawing.Size(69, 17);
            this.cbTreesinger.TabIndex = 13;
            this.cbTreesinger.Text = "Treesinger";
            // 
            // cbLatent_Viewer
            // 
            this.cbLatent_Viewer.AutoSize = true;
            this.cbLatent_Viewer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbLatent_Viewer.Location = new System.Drawing.Point(6, 341);
            this.cbLatent_Viewer.Name = "cbLatent_Viewer";
            this.cbLatent_Viewer.Size = new System.Drawing.Size(84, 17);
            this.cbLatent_Viewer.TabIndex = 14;
            this.cbLatent_Viewer.Text = "Latent Viewer";
            // 
            // cbViewing
            // 
            this.cbViewing.AutoCheck = false;
            this.cbViewing.AutoSize = true;
            this.cbViewing.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbViewing.Location = new System.Drawing.Point(26, 364);
            this.cbViewing.Name = "cbViewing";
            this.cbViewing.Size = new System.Drawing.Size(59, 17);
            this.cbViewing.TabIndex = 15;
            this.cbViewing.Text = "Viewing ";
            // 
            // gbChanneling_Feats
            // 
            this.gbChanneling_Feats.Controls.Add(this.cbCombat_Casting);
            this.gbChanneling_Feats.Controls.Add(this.cbExtra_Affinity);
            this.gbChanneling_Feats.Controls.Add(this.cbExtra_Talent);
            this.gbChanneling_Feats.Controls.Add(this.cbMultiweave);
            this.gbChanneling_Feats.Controls.Add(this.cbPower_Heightened_Senses);
            this.gbChanneling_Feats.Controls.Add(this.cbSense_Residue);
            this.gbChanneling_Feats.Controls.Add(this.cbTie_Off_Weave);
            this.gbChanneling_Feats.Dock = System.Windows.Forms.DockStyle.Top;
            this.gbChanneling_Feats.Location = new System.Drawing.Point(3, 1447);
            this.gbChanneling_Feats.Name = "gbChanneling_Feats";
            this.gbChanneling_Feats.Size = new System.Drawing.Size(420, 179);
            this.gbChanneling_Feats.TabIndex = 2;
            this.gbChanneling_Feats.TabStop = false;
            this.gbChanneling_Feats.Text = "Channeling Feats";
            // 
            // cbCombat_Casting
            // 
            this.cbCombat_Casting.AutoSize = true;
            this.cbCombat_Casting.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbCombat_Casting.Location = new System.Drawing.Point(6, 19);
            this.cbCombat_Casting.Name = "cbCombat_Casting";
            this.cbCombat_Casting.Size = new System.Drawing.Size(93, 17);
            this.cbCombat_Casting.TabIndex = 0;
            this.cbCombat_Casting.Text = "Combat Casting";
            // 
            // cbExtra_Affinity
            // 
            this.cbExtra_Affinity.AutoSize = true;
            this.cbExtra_Affinity.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbExtra_Affinity.Location = new System.Drawing.Point(6, 42);
            this.cbExtra_Affinity.Name = "cbExtra_Affinity";
            this.cbExtra_Affinity.Size = new System.Drawing.Size(77, 17);
            this.cbExtra_Affinity.TabIndex = 1;
            this.cbExtra_Affinity.Text = "Extra Affinity";
            // 
            // cbExtra_Talent
            // 
            this.cbExtra_Talent.AutoSize = true;
            this.cbExtra_Talent.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbExtra_Talent.Location = new System.Drawing.Point(6, 65);
            this.cbExtra_Talent.Name = "cbExtra_Talent";
            this.cbExtra_Talent.Size = new System.Drawing.Size(76, 17);
            this.cbExtra_Talent.TabIndex = 2;
            this.cbExtra_Talent.Text = "Extra Talent";
            // 
            // cbMultiweave
            // 
            this.cbMultiweave.AutoSize = true;
            this.cbMultiweave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbMultiweave.Location = new System.Drawing.Point(6, 88);
            this.cbMultiweave.Name = "cbMultiweave";
            this.cbMultiweave.Size = new System.Drawing.Size(73, 17);
            this.cbMultiweave.TabIndex = 3;
            this.cbMultiweave.Text = "Multiweave";
            // 
            // cbPower_Heightened_Senses
            // 
            this.cbPower_Heightened_Senses.AutoSize = true;
            this.cbPower_Heightened_Senses.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbPower_Heightened_Senses.Location = new System.Drawing.Point(6, 111);
            this.cbPower_Heightened_Senses.Name = "cbPower_Heightened_Senses";
            this.cbPower_Heightened_Senses.Size = new System.Drawing.Size(145, 17);
            this.cbPower_Heightened_Senses.TabIndex = 4;
            this.cbPower_Heightened_Senses.Text = "Power-Heightened Senses";
            // 
            // cbSense_Residue
            // 
            this.cbSense_Residue.AutoSize = true;
            this.cbSense_Residue.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbSense_Residue.Location = new System.Drawing.Point(6, 134);
            this.cbSense_Residue.Name = "cbSense_Residue";
            this.cbSense_Residue.Size = new System.Drawing.Size(91, 17);
            this.cbSense_Residue.TabIndex = 5;
            this.cbSense_Residue.Text = "Sense Residue";
            // 
            // cbTie_Off_Weave
            // 
            this.cbTie_Off_Weave.AutoSize = true;
            this.cbTie_Off_Weave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbTie_Off_Weave.Location = new System.Drawing.Point(6, 157);
            this.cbTie_Off_Weave.Name = "cbTie_Off_Weave";
            this.cbTie_Off_Weave.Size = new System.Drawing.Size(89, 17);
            this.cbTie_Off_Weave.TabIndex = 6;
            this.cbTie_Off_Weave.Text = "Tie Off Weave";
            // 
            // gbSpecial_Feats
            // 
            this.gbSpecial_Feats.Controls.Add(this.cbEliminate_Block);
            this.gbSpecial_Feats.Controls.Add(this.cbWeapon_Specializiation);
            this.gbSpecial_Feats.Controls.Add(this.tbWeapon_Specializiation);
            this.gbSpecial_Feats.Dock = System.Windows.Forms.DockStyle.Top;
            this.gbSpecial_Feats.Location = new System.Drawing.Point(3, 1380);
            this.gbSpecial_Feats.Name = "gbSpecial_Feats";
            this.gbSpecial_Feats.Size = new System.Drawing.Size(420, 67);
            this.gbSpecial_Feats.TabIndex = 1;
            this.gbSpecial_Feats.TabStop = false;
            this.gbSpecial_Feats.Text = "Special Feats";
            // 
            // cbEliminate_Block
            // 
            this.cbEliminate_Block.AutoSize = true;
            this.cbEliminate_Block.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbEliminate_Block.Location = new System.Drawing.Point(6, 19);
            this.cbEliminate_Block.Name = "cbEliminate_Block";
            this.cbEliminate_Block.Size = new System.Drawing.Size(91, 17);
            this.cbEliminate_Block.TabIndex = 0;
            this.cbEliminate_Block.Text = "Eliminate Block";
            // 
            // cbWeapon_Specializiation
            // 
            this.cbWeapon_Specializiation.AutoSize = true;
            this.cbWeapon_Specializiation.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbWeapon_Specializiation.Location = new System.Drawing.Point(6, 42);
            this.cbWeapon_Specializiation.Name = "cbWeapon_Specializiation";
            this.cbWeapon_Specializiation.Size = new System.Drawing.Size(130, 17);
            this.cbWeapon_Specializiation.TabIndex = 1;
            this.cbWeapon_Specializiation.Text = "Weapon Specializiation";
            this.cbWeapon_Specializiation.CheckedChanged += new System.EventHandler(this.cbWeapon_Specializiation_CheckedChanged);
            // 
            // tbWeapon_Specializiation
            // 
            this.tbWeapon_Specializiation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbWeapon_Specializiation.Enabled = false;
            this.tbWeapon_Specializiation.Location = new System.Drawing.Point(142, 41);
            this.tbWeapon_Specializiation.Name = "tbWeapon_Specializiation";
            this.tbWeapon_Specializiation.Size = new System.Drawing.Size(254, 20);
            this.tbWeapon_Specializiation.TabIndex = 2;
            // 
            // gbGeneral_Feats
            // 
            this.gbGeneral_Feats.Controls.Add(this.cbAlertness);
            this.gbGeneral_Feats.Controls.Add(this.cbAmbidextrous);
            this.gbGeneral_Feats.Controls.Add(this.cbAnimal_Affinity);
            this.gbGeneral_Feats.Controls.Add(this.cbArmor_Proficiency_light);
            this.gbGeneral_Feats.Controls.Add(this.cbArmor_Proficiency_medium);
            this.gbGeneral_Feats.Controls.Add(this.cbArmor_Proficiency_heavy);
            this.gbGeneral_Feats.Controls.Add(this.cbImproved_Critical);
            this.gbGeneral_Feats.Controls.Add(this.cbAthletic);
            this.gbGeneral_Feats.Controls.Add(this.cbBlind_Fight);
            this.gbGeneral_Feats.Controls.Add(this.cbCombat_Expertise);
            this.gbGeneral_Feats.Controls.Add(this.cbImproved_Disarm);
            this.gbGeneral_Feats.Controls.Add(this.cbImproved_Trip);
            this.gbGeneral_Feats.Controls.Add(this.cbWhirlwind_Attack);
            this.gbGeneral_Feats.Controls.Add(this.cbCombat_Reflexes);
            this.gbGeneral_Feats.Controls.Add(this.cbThe_Dark_One_s_own_Luck);
            this.gbGeneral_Feats.Controls.Add(this.cbDodge);
            this.gbGeneral_Feats.Controls.Add(this.cbMobility);
            this.gbGeneral_Feats.Controls.Add(this.cbSpring_Attack);
            this.gbGeneral_Feats.Controls.Add(this.cbEndurance);
            this.gbGeneral_Feats.Controls.Add(this.cbExotic_Weapon_Proficiency);
            this.gbGeneral_Feats.Controls.Add(this.cbFame);
            this.gbGeneral_Feats.Controls.Add(this.cbGreat_Fortitude);
            this.gbGeneral_Feats.Controls.Add(this.cbImproved_Initiative);
            this.gbGeneral_Feats.Controls.Add(this.cbImproved_Unarmed_Strike);
            this.gbGeneral_Feats.Controls.Add(this.cbIron_Will);
            this.gbGeneral_Feats.Controls.Add(this.cbLightning_Reflexes);
            this.gbGeneral_Feats.Controls.Add(this.cbMartial_Weapon_Proficiency);
            this.gbGeneral_Feats.Controls.Add(this.cbMental_Stability);
            this.gbGeneral_Feats.Controls.Add(this.cbMimic);
            this.gbGeneral_Feats.Controls.Add(this.cbMounted_Combat);
            this.gbGeneral_Feats.Controls.Add(this.cbMounted_Archery);
            this.gbGeneral_Feats.Controls.Add(this.cbTrample);
            this.gbGeneral_Feats.Controls.Add(this.cbRide_By_Attack);
            this.gbGeneral_Feats.Controls.Add(this.cbNimble);
            this.gbGeneral_Feats.Controls.Add(this.cbPersuasive);
            this.gbGeneral_Feats.Controls.Add(this.cbPoint_Blank_Shot);
            this.gbGeneral_Feats.Controls.Add(this.cbFar_Shot);
            this.gbGeneral_Feats.Controls.Add(this.cbPrecise_Shot);
            this.gbGeneral_Feats.Controls.Add(this.cbRapid_Shot);
            this.gbGeneral_Feats.Controls.Add(this.cbShot_on_the_Run);
            this.gbGeneral_Feats.Controls.Add(this.cbPower_Attack);
            this.gbGeneral_Feats.Controls.Add(this.cbCleave);
            this.gbGeneral_Feats.Controls.Add(this.cbGreat_Cleave);
            this.gbGeneral_Feats.Controls.Add(this.cbImproved_Bull_Rush);
            this.gbGeneral_Feats.Controls.Add(this.cbQuick_Draw);
            this.gbGeneral_Feats.Controls.Add(this.cbQuickness);
            this.gbGeneral_Feats.Controls.Add(this.cbRun);
            this.gbGeneral_Feats.Controls.Add(this.cbSharp_eyed);
            this.gbGeneral_Feats.Controls.Add(this.cbShield_Proficiency);
            this.gbGeneral_Feats.Controls.Add(this.cbSimple_Weapon_Proficiency);
            this.gbGeneral_Feats.Controls.Add(this.cbSkill_Emphasis);
            this.gbGeneral_Feats.Controls.Add(this.cbStealthy);
            this.gbGeneral_Feats.Controls.Add(this.cbToughness);
            this.gbGeneral_Feats.Controls.Add(this.cbTrack);
            this.gbGeneral_Feats.Controls.Add(this.cbTrustworthy);
            this.gbGeneral_Feats.Controls.Add(this.cbTwo_Weapon_Fighting);
            this.gbGeneral_Feats.Controls.Add(this.cbImproved_Two_Weapon_Fighting);
            this.gbGeneral_Feats.Controls.Add(this.cbWeapon_Finesse);
            this.gbGeneral_Feats.Controls.Add(this.cbWeapon_Focus);
            this.gbGeneral_Feats.Controls.Add(this.tbWeapon_Focus);
            this.gbGeneral_Feats.Controls.Add(this.tbWeapon_Finesse);
            this.gbGeneral_Feats.Controls.Add(this.tbMartial_Weapon_Proficiency);
            this.gbGeneral_Feats.Controls.Add(this.tbExotic_Weapon_Proficiency);
            this.gbGeneral_Feats.Dock = System.Windows.Forms.DockStyle.Top;
            this.gbGeneral_Feats.Location = new System.Drawing.Point(3, 3);
            this.gbGeneral_Feats.Name = "gbGeneral_Feats";
            this.gbGeneral_Feats.Size = new System.Drawing.Size(420, 1377);
            this.gbGeneral_Feats.TabIndex = 0;
            this.gbGeneral_Feats.TabStop = false;
            this.gbGeneral_Feats.Text = "General Feats";
            // 
            // cbAlertness
            // 
            this.cbAlertness.AutoSize = true;
            this.cbAlertness.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbAlertness.Location = new System.Drawing.Point(6, 19);
            this.cbAlertness.Name = "cbAlertness";
            this.cbAlertness.Size = new System.Drawing.Size(62, 17);
            this.cbAlertness.TabIndex = 0;
            this.cbAlertness.Text = "Alertness";
            // 
            // cbAmbidextrous
            // 
            this.cbAmbidextrous.AutoSize = true;
            this.cbAmbidextrous.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbAmbidextrous.Location = new System.Drawing.Point(6, 42);
            this.cbAmbidextrous.Name = "cbAmbidextrous";
            this.cbAmbidextrous.Size = new System.Drawing.Size(82, 17);
            this.cbAmbidextrous.TabIndex = 1;
            this.cbAmbidextrous.Text = "Ambidextrous";
            // 
            // cbAnimal_Affinity
            // 
            this.cbAnimal_Affinity.AutoSize = true;
            this.cbAnimal_Affinity.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbAnimal_Affinity.Location = new System.Drawing.Point(6, 65);
            this.cbAnimal_Affinity.Name = "cbAnimal_Affinity";
            this.cbAnimal_Affinity.Size = new System.Drawing.Size(84, 17);
            this.cbAnimal_Affinity.TabIndex = 2;
            this.cbAnimal_Affinity.Text = "Animal Affinity";
            // 
            // cbArmor_Proficiency_light
            // 
            this.cbArmor_Proficiency_light.AutoSize = true;
            this.cbArmor_Proficiency_light.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbArmor_Proficiency_light.Location = new System.Drawing.Point(6, 88);
            this.cbArmor_Proficiency_light.Name = "cbArmor_Proficiency_light";
            this.cbArmor_Proficiency_light.Size = new System.Drawing.Size(129, 17);
            this.cbArmor_Proficiency_light.TabIndex = 3;
            this.cbArmor_Proficiency_light.Text = "Armor Proficiency (light)";
            this.cbArmor_Proficiency_light.CheckedChanged += new System.EventHandler(this.cbArmor_Proficiency_light_CheckedChanged);
            // 
            // cbArmor_Proficiency_medium
            // 
            this.cbArmor_Proficiency_medium.AutoCheck = false;
            this.cbArmor_Proficiency_medium.AutoSize = true;
            this.cbArmor_Proficiency_medium.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbArmor_Proficiency_medium.Location = new System.Drawing.Point(26, 111);
            this.cbArmor_Proficiency_medium.Name = "cbArmor_Proficiency_medium";
            this.cbArmor_Proficiency_medium.Size = new System.Drawing.Size(146, 17);
            this.cbArmor_Proficiency_medium.TabIndex = 4;
            this.cbArmor_Proficiency_medium.Text = "Armor Proficiency (medium)";
            this.cbArmor_Proficiency_medium.CheckedChanged += new System.EventHandler(this.cbArmor_Proficiency_medium_CheckedChanged);
            // 
            // cbArmor_Proficiency_heavy
            // 
            this.cbArmor_Proficiency_heavy.AutoCheck = false;
            this.cbArmor_Proficiency_heavy.AutoSize = true;
            this.cbArmor_Proficiency_heavy.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbArmor_Proficiency_heavy.Location = new System.Drawing.Point(46, 134);
            this.cbArmor_Proficiency_heavy.Name = "cbArmor_Proficiency_heavy";
            this.cbArmor_Proficiency_heavy.Size = new System.Drawing.Size(139, 17);
            this.cbArmor_Proficiency_heavy.TabIndex = 5;
            this.cbArmor_Proficiency_heavy.Text = "Armor Proficiency (heavy)";
            // 
            // cbImproved_Critical
            // 
            this.cbImproved_Critical.AutoCheck = false;
            this.cbImproved_Critical.AutoSize = true;
            this.cbImproved_Critical.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbImproved_Critical.Location = new System.Drawing.Point(26, 157);
            this.cbImproved_Critical.Name = "cbImproved_Critical";
            this.cbImproved_Critical.Size = new System.Drawing.Size(97, 17);
            this.cbImproved_Critical.TabIndex = 23;
            this.cbImproved_Critical.Text = "Improved Critical";
            // 
            // cbAthletic
            // 
            this.cbAthletic.AutoSize = true;
            this.cbAthletic.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbAthletic.Location = new System.Drawing.Point(6, 180);
            this.cbAthletic.Name = "cbAthletic";
            this.cbAthletic.Size = new System.Drawing.Size(54, 17);
            this.cbAthletic.TabIndex = 6;
            this.cbAthletic.Text = "Athletic";
            // 
            // cbBlind_Fight
            // 
            this.cbBlind_Fight.AutoSize = true;
            this.cbBlind_Fight.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbBlind_Fight.Location = new System.Drawing.Point(6, 203);
            this.cbBlind_Fight.Name = "cbBlind_Fight";
            this.cbBlind_Fight.Size = new System.Drawing.Size(68, 17);
            this.cbBlind_Fight.TabIndex = 7;
            this.cbBlind_Fight.Text = "Blind-Fight";
            // 
            // cbCombat_Expertise
            // 
            this.cbCombat_Expertise.AutoSize = true;
            this.cbCombat_Expertise.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbCombat_Expertise.Location = new System.Drawing.Point(6, 226);
            this.cbCombat_Expertise.Name = "cbCombat_Expertise";
            this.cbCombat_Expertise.Size = new System.Drawing.Size(101, 17);
            this.cbCombat_Expertise.TabIndex = 8;
            this.cbCombat_Expertise.Text = "Combat Expertise";
            this.cbCombat_Expertise.CheckedChanged += new System.EventHandler(this.cbCombat_Expertise_CheckedChanged);
            // 
            // cbImproved_Disarm
            // 
            this.cbImproved_Disarm.AutoCheck = false;
            this.cbImproved_Disarm.AutoSize = true;
            this.cbImproved_Disarm.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbImproved_Disarm.Location = new System.Drawing.Point(26, 249);
            this.cbImproved_Disarm.Name = "cbImproved_Disarm";
            this.cbImproved_Disarm.Size = new System.Drawing.Size(98, 17);
            this.cbImproved_Disarm.TabIndex = 9;
            this.cbImproved_Disarm.Text = "Improved Disarm";
            // 
            // cbImproved_Trip
            // 
            this.cbImproved_Trip.AutoCheck = false;
            this.cbImproved_Trip.AutoSize = true;
            this.cbImproved_Trip.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbImproved_Trip.Location = new System.Drawing.Point(26, 272);
            this.cbImproved_Trip.Name = "cbImproved_Trip";
            this.cbImproved_Trip.Size = new System.Drawing.Size(84, 17);
            this.cbImproved_Trip.TabIndex = 10;
            this.cbImproved_Trip.Text = "Improved Trip";
            // 
            // cbWhirlwind_Attack
            // 
            this.cbWhirlwind_Attack.AutoCheck = false;
            this.cbWhirlwind_Attack.AutoSize = true;
            this.cbWhirlwind_Attack.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbWhirlwind_Attack.Location = new System.Drawing.Point(26, 295);
            this.cbWhirlwind_Attack.Name = "cbWhirlwind_Attack";
            this.cbWhirlwind_Attack.Size = new System.Drawing.Size(99, 17);
            this.cbWhirlwind_Attack.TabIndex = 11;
            this.cbWhirlwind_Attack.Text = "Whirlwind Attack";
            // 
            // cbCombat_Reflexes
            // 
            this.cbCombat_Reflexes.AutoSize = true;
            this.cbCombat_Reflexes.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbCombat_Reflexes.Location = new System.Drawing.Point(6, 318);
            this.cbCombat_Reflexes.Name = "cbCombat_Reflexes";
            this.cbCombat_Reflexes.Size = new System.Drawing.Size(99, 17);
            this.cbCombat_Reflexes.TabIndex = 12;
            this.cbCombat_Reflexes.Text = "Combat Reflexes";
            // 
            // cbThe_Dark_One_s_own_Luck
            // 
            this.cbThe_Dark_One_s_own_Luck.AutoSize = true;
            this.cbThe_Dark_One_s_own_Luck.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbThe_Dark_One_s_own_Luck.Location = new System.Drawing.Point(6, 341);
            this.cbThe_Dark_One_s_own_Luck.Name = "cbThe_Dark_One_s_own_Luck";
            this.cbThe_Dark_One_s_own_Luck.Size = new System.Drawing.Size(144, 17);
            this.cbThe_Dark_One_s_own_Luck.TabIndex = 13;
            this.cbThe_Dark_One_s_own_Luck.Text = "The Dark One\'s own Luck";
            // 
            // cbDodge
            // 
            this.cbDodge.AutoSize = true;
            this.cbDodge.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbDodge.Location = new System.Drawing.Point(6, 364);
            this.cbDodge.Name = "cbDodge";
            this.cbDodge.Size = new System.Drawing.Size(51, 17);
            this.cbDodge.TabIndex = 14;
            this.cbDodge.Text = "Dodge";
            this.cbDodge.CheckedChanged += new System.EventHandler(this.cbDodge_CheckedChanged);
            // 
            // cbMobility
            // 
            this.cbMobility.AutoCheck = false;
            this.cbMobility.AutoSize = true;
            this.cbMobility.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbMobility.Location = new System.Drawing.Point(26, 387);
            this.cbMobility.Name = "cbMobility";
            this.cbMobility.Size = new System.Drawing.Size(54, 17);
            this.cbMobility.TabIndex = 15;
            this.cbMobility.Text = "Mobility";
            this.cbMobility.CheckedChanged += new System.EventHandler(this.cbMobility_CheckedChanged);
            // 
            // cbSpring_Attack
            // 
            this.cbSpring_Attack.AutoCheck = false;
            this.cbSpring_Attack.AutoSize = true;
            this.cbSpring_Attack.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbSpring_Attack.Location = new System.Drawing.Point(46, 410);
            this.cbSpring_Attack.Name = "cbSpring_Attack";
            this.cbSpring_Attack.Size = new System.Drawing.Size(83, 17);
            this.cbSpring_Attack.TabIndex = 16;
            this.cbSpring_Attack.Text = "Spring Attack";
            // 
            // cbEndurance
            // 
            this.cbEndurance.AutoSize = true;
            this.cbEndurance.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbEndurance.Location = new System.Drawing.Point(6, 433);
            this.cbEndurance.Name = "cbEndurance";
            this.cbEndurance.Size = new System.Drawing.Size(71, 17);
            this.cbEndurance.TabIndex = 17;
            this.cbEndurance.Text = "Endurance";
            // 
            // cbExotic_Weapon_Proficiency
            // 
            this.cbExotic_Weapon_Proficiency.AutoSize = true;
            this.cbExotic_Weapon_Proficiency.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbExotic_Weapon_Proficiency.Location = new System.Drawing.Point(6, 456);
            this.cbExotic_Weapon_Proficiency.Name = "cbExotic_Weapon_Proficiency";
            this.cbExotic_Weapon_Proficiency.Size = new System.Drawing.Size(147, 17);
            this.cbExotic_Weapon_Proficiency.TabIndex = 18;
            this.cbExotic_Weapon_Proficiency.Text = "Exotic Weapon Proficiency";
            this.cbExotic_Weapon_Proficiency.CheckedChanged += new System.EventHandler(this.cbExotic_Weapon_Proficiency_CheckedChanged);
            // 
            // cbFame
            // 
            this.cbFame.AutoSize = true;
            this.cbFame.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbFame.Location = new System.Drawing.Point(6, 479);
            this.cbFame.Name = "cbFame";
            this.cbFame.Size = new System.Drawing.Size(45, 17);
            this.cbFame.TabIndex = 20;
            this.cbFame.Text = "Fame";
            // 
            // cbGreat_Fortitude
            // 
            this.cbGreat_Fortitude.AutoSize = true;
            this.cbGreat_Fortitude.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbGreat_Fortitude.Location = new System.Drawing.Point(6, 502);
            this.cbGreat_Fortitude.Name = "cbGreat_Fortitude";
            this.cbGreat_Fortitude.Size = new System.Drawing.Size(89, 17);
            this.cbGreat_Fortitude.TabIndex = 21;
            this.cbGreat_Fortitude.Text = "Great Fortitude";
            // 
            // cbImproved_Initiative
            // 
            this.cbImproved_Initiative.AutoSize = true;
            this.cbImproved_Initiative.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbImproved_Initiative.Location = new System.Drawing.Point(6, 525);
            this.cbImproved_Initiative.Name = "cbImproved_Initiative";
            this.cbImproved_Initiative.Size = new System.Drawing.Size(105, 17);
            this.cbImproved_Initiative.TabIndex = 24;
            this.cbImproved_Initiative.Text = "Improved Initiative";
            // 
            // cbImproved_Unarmed_Strike
            // 
            this.cbImproved_Unarmed_Strike.AutoSize = true;
            this.cbImproved_Unarmed_Strike.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbImproved_Unarmed_Strike.Location = new System.Drawing.Point(6, 548);
            this.cbImproved_Unarmed_Strike.Name = "cbImproved_Unarmed_Strike";
            this.cbImproved_Unarmed_Strike.Size = new System.Drawing.Size(139, 17);
            this.cbImproved_Unarmed_Strike.TabIndex = 25;
            this.cbImproved_Unarmed_Strike.Text = "Improved Unarmed Strike";
            // 
            // cbIron_Will
            // 
            this.cbIron_Will.AutoSize = true;
            this.cbIron_Will.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbIron_Will.Location = new System.Drawing.Point(6, 571);
            this.cbIron_Will.Name = "cbIron_Will";
            this.cbIron_Will.Size = new System.Drawing.Size(57, 17);
            this.cbIron_Will.TabIndex = 26;
            this.cbIron_Will.Text = "Iron Will";
            // 
            // cbLightning_Reflexes
            // 
            this.cbLightning_Reflexes.AutoSize = true;
            this.cbLightning_Reflexes.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbLightning_Reflexes.Location = new System.Drawing.Point(6, 594);
            this.cbLightning_Reflexes.Name = "cbLightning_Reflexes";
            this.cbLightning_Reflexes.Size = new System.Drawing.Size(106, 17);
            this.cbLightning_Reflexes.TabIndex = 27;
            this.cbLightning_Reflexes.Text = "Lightning Reflexes";
            // 
            // cbMartial_Weapon_Proficiency
            // 
            this.cbMartial_Weapon_Proficiency.AutoSize = true;
            this.cbMartial_Weapon_Proficiency.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbMartial_Weapon_Proficiency.Location = new System.Drawing.Point(6, 617);
            this.cbMartial_Weapon_Proficiency.Name = "cbMartial_Weapon_Proficiency";
            this.cbMartial_Weapon_Proficiency.Size = new System.Drawing.Size(149, 17);
            this.cbMartial_Weapon_Proficiency.TabIndex = 28;
            this.cbMartial_Weapon_Proficiency.Text = "Martial Weapon Proficiency";
            this.cbMartial_Weapon_Proficiency.CheckedChanged += new System.EventHandler(this.cbMartial_Weapon_Proficiency_CheckedChanged);
            // 
            // cbMental_Stability
            // 
            this.cbMental_Stability.AutoSize = true;
            this.cbMental_Stability.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbMental_Stability.Location = new System.Drawing.Point(6, 640);
            this.cbMental_Stability.Name = "cbMental_Stability";
            this.cbMental_Stability.Size = new System.Drawing.Size(90, 17);
            this.cbMental_Stability.TabIndex = 29;
            this.cbMental_Stability.Text = "Mental Stability";
            // 
            // cbMimic
            // 
            this.cbMimic.AutoSize = true;
            this.cbMimic.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbMimic.Location = new System.Drawing.Point(6, 663);
            this.cbMimic.Name = "cbMimic";
            this.cbMimic.Size = new System.Drawing.Size(46, 17);
            this.cbMimic.TabIndex = 30;
            this.cbMimic.Text = "Mimic";
            // 
            // cbMounted_Combat
            // 
            this.cbMounted_Combat.AutoSize = true;
            this.cbMounted_Combat.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbMounted_Combat.Location = new System.Drawing.Point(6, 686);
            this.cbMounted_Combat.Name = "cbMounted_Combat";
            this.cbMounted_Combat.Size = new System.Drawing.Size(100, 17);
            this.cbMounted_Combat.TabIndex = 31;
            this.cbMounted_Combat.Text = "Mounted Combat";
            this.cbMounted_Combat.CheckedChanged += new System.EventHandler(this.cbMounted_Combat_CheckedChanged);
            // 
            // cbMounted_Archery
            // 
            this.cbMounted_Archery.AutoCheck = false;
            this.cbMounted_Archery.AutoSize = true;
            this.cbMounted_Archery.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbMounted_Archery.Location = new System.Drawing.Point(26, 709);
            this.cbMounted_Archery.Name = "cbMounted_Archery";
            this.cbMounted_Archery.Size = new System.Drawing.Size(100, 17);
            this.cbMounted_Archery.TabIndex = 32;
            this.cbMounted_Archery.Text = "Mounted Archery";
            // 
            // cbTrample
            // 
            this.cbTrample.AutoCheck = false;
            this.cbTrample.AutoSize = true;
            this.cbTrample.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbTrample.Location = new System.Drawing.Point(26, 732);
            this.cbTrample.Name = "cbTrample";
            this.cbTrample.Size = new System.Drawing.Size(57, 17);
            this.cbTrample.TabIndex = 34;
            this.cbTrample.Text = "Trample";
            // 
            // cbRide_By_Attack
            // 
            this.cbRide_By_Attack.AutoCheck = false;
            this.cbRide_By_Attack.AutoSize = true;
            this.cbRide_By_Attack.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbRide_By_Attack.Location = new System.Drawing.Point(26, 755);
            this.cbRide_By_Attack.Name = "cbRide_By_Attack";
            this.cbRide_By_Attack.Size = new System.Drawing.Size(90, 17);
            this.cbRide_By_Attack.TabIndex = 35;
            this.cbRide_By_Attack.Text = "Ride-By Attack";
            // 
            // cbNimble
            // 
            this.cbNimble.AutoSize = true;
            this.cbNimble.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbNimble.Location = new System.Drawing.Point(6, 778);
            this.cbNimble.Name = "cbNimble";
            this.cbNimble.Size = new System.Drawing.Size(51, 17);
            this.cbNimble.TabIndex = 36;
            this.cbNimble.Text = "Nimble";
            // 
            // cbPersuasive
            // 
            this.cbPersuasive.AutoSize = true;
            this.cbPersuasive.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbPersuasive.Location = new System.Drawing.Point(6, 801);
            this.cbPersuasive.Name = "cbPersuasive";
            this.cbPersuasive.Size = new System.Drawing.Size(71, 17);
            this.cbPersuasive.TabIndex = 37;
            this.cbPersuasive.Text = "Persuasive";
            // 
            // cbPoint_Blank_Shot
            // 
            this.cbPoint_Blank_Shot.AutoSize = true;
            this.cbPoint_Blank_Shot.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbPoint_Blank_Shot.Location = new System.Drawing.Point(6, 824);
            this.cbPoint_Blank_Shot.Name = "cbPoint_Blank_Shot";
            this.cbPoint_Blank_Shot.Size = new System.Drawing.Size(98, 17);
            this.cbPoint_Blank_Shot.TabIndex = 38;
            this.cbPoint_Blank_Shot.Text = "Point Blank Shot";
            this.cbPoint_Blank_Shot.CheckedChanged += new System.EventHandler(this.cbPoint_Blank_Shot_CheckedChanged);
            // 
            // cbFar_Shot
            // 
            this.cbFar_Shot.AutoCheck = false;
            this.cbFar_Shot.AutoSize = true;
            this.cbFar_Shot.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbFar_Shot.Location = new System.Drawing.Point(26, 847);
            this.cbFar_Shot.Name = "cbFar_Shot";
            this.cbFar_Shot.Size = new System.Drawing.Size(59, 17);
            this.cbFar_Shot.TabIndex = 39;
            this.cbFar_Shot.Text = "Far Shot";
            // 
            // cbPrecise_Shot
            // 
            this.cbPrecise_Shot.AutoCheck = false;
            this.cbPrecise_Shot.AutoSize = true;
            this.cbPrecise_Shot.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbPrecise_Shot.Location = new System.Drawing.Point(26, 870);
            this.cbPrecise_Shot.Name = "cbPrecise_Shot";
            this.cbPrecise_Shot.Size = new System.Drawing.Size(79, 17);
            this.cbPrecise_Shot.TabIndex = 40;
            this.cbPrecise_Shot.Text = "Precise Shot";
            // 
            // cbRapid_Shot
            // 
            this.cbRapid_Shot.AutoCheck = false;
            this.cbRapid_Shot.AutoSize = true;
            this.cbRapid_Shot.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbRapid_Shot.Location = new System.Drawing.Point(26, 893);
            this.cbRapid_Shot.Name = "cbRapid_Shot";
            this.cbRapid_Shot.Size = new System.Drawing.Size(72, 17);
            this.cbRapid_Shot.TabIndex = 41;
            this.cbRapid_Shot.Text = "Rapid Shot";
            // 
            // cbShot_on_the_Run
            // 
            this.cbShot_on_the_Run.AutoCheck = false;
            this.cbShot_on_the_Run.AutoSize = true;
            this.cbShot_on_the_Run.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbShot_on_the_Run.Location = new System.Drawing.Point(26, 916);
            this.cbShot_on_the_Run.Name = "cbShot_on_the_Run";
            this.cbShot_on_the_Run.Size = new System.Drawing.Size(97, 17);
            this.cbShot_on_the_Run.TabIndex = 42;
            this.cbShot_on_the_Run.Text = "Shot on the Run";
            // 
            // cbPower_Attack
            // 
            this.cbPower_Attack.AutoSize = true;
            this.cbPower_Attack.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbPower_Attack.Location = new System.Drawing.Point(6, 939);
            this.cbPower_Attack.Name = "cbPower_Attack";
            this.cbPower_Attack.Size = new System.Drawing.Size(83, 17);
            this.cbPower_Attack.TabIndex = 43;
            this.cbPower_Attack.Text = "Power Attack";
            this.cbPower_Attack.CheckedChanged += new System.EventHandler(this.cbPower_Attack_CheckedChanged);
            // 
            // cbCleave
            // 
            this.cbCleave.AutoCheck = false;
            this.cbCleave.AutoSize = true;
            this.cbCleave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbCleave.Location = new System.Drawing.Point(26, 962);
            this.cbCleave.Name = "cbCleave";
            this.cbCleave.Size = new System.Drawing.Size(52, 17);
            this.cbCleave.TabIndex = 44;
            this.cbCleave.Text = "Cleave";
            this.cbCleave.CheckedChanged += new System.EventHandler(this.cbCleave_CheckedChanged);
            // 
            // cbGreat_Cleave
            // 
            this.cbGreat_Cleave.AutoCheck = false;
            this.cbGreat_Cleave.AutoSize = true;
            this.cbGreat_Cleave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbGreat_Cleave.Location = new System.Drawing.Point(46, 985);
            this.cbGreat_Cleave.Name = "cbGreat_Cleave";
            this.cbGreat_Cleave.Size = new System.Drawing.Size(81, 17);
            this.cbGreat_Cleave.TabIndex = 45;
            this.cbGreat_Cleave.Text = "Great Cleave";
            // 
            // cbImproved_Bull_Rush
            // 
            this.cbImproved_Bull_Rush.AutoCheck = false;
            this.cbImproved_Bull_Rush.AutoSize = true;
            this.cbImproved_Bull_Rush.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbImproved_Bull_Rush.Location = new System.Drawing.Point(26, 1008);
            this.cbImproved_Bull_Rush.Name = "cbImproved_Bull_Rush";
            this.cbImproved_Bull_Rush.Size = new System.Drawing.Size(111, 17);
            this.cbImproved_Bull_Rush.TabIndex = 46;
            this.cbImproved_Bull_Rush.Text = "Improved Bull Rush";
            // 
            // cbQuick_Draw
            // 
            this.cbQuick_Draw.AutoSize = true;
            this.cbQuick_Draw.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbQuick_Draw.Location = new System.Drawing.Point(6, 1031);
            this.cbQuick_Draw.Name = "cbQuick_Draw";
            this.cbQuick_Draw.Size = new System.Drawing.Size(75, 17);
            this.cbQuick_Draw.TabIndex = 47;
            this.cbQuick_Draw.Text = "Quick Draw";
            // 
            // cbQuickness
            // 
            this.cbQuickness.AutoSize = true;
            this.cbQuickness.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbQuickness.Location = new System.Drawing.Point(6, 1054);
            this.cbQuickness.Name = "cbQuickness";
            this.cbQuickness.Size = new System.Drawing.Size(69, 17);
            this.cbQuickness.TabIndex = 48;
            this.cbQuickness.Text = "Quickness";
            // 
            // cbRun
            // 
            this.cbRun.AutoSize = true;
            this.cbRun.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbRun.Location = new System.Drawing.Point(6, 1077);
            this.cbRun.Name = "cbRun";
            this.cbRun.Size = new System.Drawing.Size(39, 17);
            this.cbRun.TabIndex = 49;
            this.cbRun.Text = "Run";
            // 
            // cbSharp_eyed
            // 
            this.cbSharp_eyed.AutoSize = true;
            this.cbSharp_eyed.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbSharp_eyed.Location = new System.Drawing.Point(6, 1100);
            this.cbSharp_eyed.Name = "cbSharp_eyed";
            this.cbSharp_eyed.Size = new System.Drawing.Size(73, 17);
            this.cbSharp_eyed.TabIndex = 50;
            this.cbSharp_eyed.Text = "Sharp-eyed";
            // 
            // cbShield_Proficiency
            // 
            this.cbShield_Proficiency.AutoSize = true;
            this.cbShield_Proficiency.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbShield_Proficiency.Location = new System.Drawing.Point(6, 1123);
            this.cbShield_Proficiency.Name = "cbShield_Proficiency";
            this.cbShield_Proficiency.Size = new System.Drawing.Size(103, 17);
            this.cbShield_Proficiency.TabIndex = 51;
            this.cbShield_Proficiency.Text = "Shield Proficiency";
            // 
            // cbSimple_Weapon_Proficiency
            // 
            this.cbSimple_Weapon_Proficiency.AutoSize = true;
            this.cbSimple_Weapon_Proficiency.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbSimple_Weapon_Proficiency.Location = new System.Drawing.Point(6, 1146);
            this.cbSimple_Weapon_Proficiency.Name = "cbSimple_Weapon_Proficiency";
            this.cbSimple_Weapon_Proficiency.Size = new System.Drawing.Size(149, 17);
            this.cbSimple_Weapon_Proficiency.TabIndex = 52;
            this.cbSimple_Weapon_Proficiency.Text = "Simple Weapon Proficiency";
            // 
            // cbSkill_Emphasis
            // 
            this.cbSkill_Emphasis.AutoSize = true;
            this.cbSkill_Emphasis.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbSkill_Emphasis.Location = new System.Drawing.Point(6, 1169);
            this.cbSkill_Emphasis.Name = "cbSkill_Emphasis";
            this.cbSkill_Emphasis.Size = new System.Drawing.Size(86, 17);
            this.cbSkill_Emphasis.TabIndex = 53;
            this.cbSkill_Emphasis.Text = "Skill Emphasis";
            // 
            // cbStealthy
            // 
            this.cbStealthy.AutoSize = true;
            this.cbStealthy.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbStealthy.Location = new System.Drawing.Point(6, 1192);
            this.cbStealthy.Name = "cbStealthy";
            this.cbStealthy.Size = new System.Drawing.Size(57, 17);
            this.cbStealthy.TabIndex = 54;
            this.cbStealthy.Text = "Stealthy";
            // 
            // cbToughness
            // 
            this.cbToughness.AutoSize = true;
            this.cbToughness.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbToughness.Location = new System.Drawing.Point(6, 1215);
            this.cbToughness.Name = "cbToughness";
            this.cbToughness.Size = new System.Drawing.Size(72, 17);
            this.cbToughness.TabIndex = 55;
            this.cbToughness.Text = "Toughness";
            // 
            // cbTrack
            // 
            this.cbTrack.AutoSize = true;
            this.cbTrack.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbTrack.Location = new System.Drawing.Point(6, 1238);
            this.cbTrack.Name = "cbTrack";
            this.cbTrack.Size = new System.Drawing.Size(47, 17);
            this.cbTrack.TabIndex = 56;
            this.cbTrack.Text = "Track";
            // 
            // cbTrustworthy
            // 
            this.cbTrustworthy.AutoSize = true;
            this.cbTrustworthy.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbTrustworthy.Location = new System.Drawing.Point(6, 1261);
            this.cbTrustworthy.Name = "cbTrustworthy";
            this.cbTrustworthy.Size = new System.Drawing.Size(74, 17);
            this.cbTrustworthy.TabIndex = 57;
            this.cbTrustworthy.Text = "Trustworthy";
            // 
            // cbTwo_Weapon_Fighting
            // 
            this.cbTwo_Weapon_Fighting.AutoSize = true;
            this.cbTwo_Weapon_Fighting.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbTwo_Weapon_Fighting.Location = new System.Drawing.Point(6, 1284);
            this.cbTwo_Weapon_Fighting.Name = "cbTwo_Weapon_Fighting";
            this.cbTwo_Weapon_Fighting.Size = new System.Drawing.Size(124, 17);
            this.cbTwo_Weapon_Fighting.TabIndex = 58;
            this.cbTwo_Weapon_Fighting.Text = "Two-Weapon Fighting";
            this.cbTwo_Weapon_Fighting.CheckedChanged += new System.EventHandler(this.cbTwo_Weapon_Fighting_CheckedChanged);
            // 
            // cbImproved_Two_Weapon_Fighting
            // 
            this.cbImproved_Two_Weapon_Fighting.AutoCheck = false;
            this.cbImproved_Two_Weapon_Fighting.AutoSize = true;
            this.cbImproved_Two_Weapon_Fighting.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbImproved_Two_Weapon_Fighting.Location = new System.Drawing.Point(26, 1307);
            this.cbImproved_Two_Weapon_Fighting.Name = "cbImproved_Two_Weapon_Fighting";
            this.cbImproved_Two_Weapon_Fighting.Size = new System.Drawing.Size(171, 17);
            this.cbImproved_Two_Weapon_Fighting.TabIndex = 59;
            this.cbImproved_Two_Weapon_Fighting.Text = "Improved Two-Weapon Fighting";
            // 
            // cbWeapon_Finesse
            // 
            this.cbWeapon_Finesse.AutoSize = true;
            this.cbWeapon_Finesse.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbWeapon_Finesse.Location = new System.Drawing.Point(6, 1330);
            this.cbWeapon_Finesse.Name = "cbWeapon_Finesse";
            this.cbWeapon_Finesse.Size = new System.Drawing.Size(99, 17);
            this.cbWeapon_Finesse.TabIndex = 60;
            this.cbWeapon_Finesse.Text = "Weapon Finesse";
            this.cbWeapon_Finesse.CheckedChanged += new System.EventHandler(this.cbWeapon_Finesse_CheckedChanged);
            // 
            // cbWeapon_Focus
            // 
            this.cbWeapon_Focus.AutoSize = true;
            this.cbWeapon_Focus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbWeapon_Focus.Location = new System.Drawing.Point(6, 1353);
            this.cbWeapon_Focus.Name = "cbWeapon_Focus";
            this.cbWeapon_Focus.Size = new System.Drawing.Size(92, 17);
            this.cbWeapon_Focus.TabIndex = 61;
            this.cbWeapon_Focus.Text = "Weapon Focus";
            this.cbWeapon_Focus.CheckedChanged += new System.EventHandler(this.cbWeapon_Focus_CheckedChanged);
            // 
            // tbWeapon_Focus
            // 
            this.tbWeapon_Focus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbWeapon_Focus.Enabled = false;
            this.tbWeapon_Focus.Location = new System.Drawing.Point(104, 1351);
            this.tbWeapon_Focus.Name = "tbWeapon_Focus";
            this.tbWeapon_Focus.Size = new System.Drawing.Size(292, 20);
            this.tbWeapon_Focus.TabIndex = 63;
            // 
            // tbWeapon_Finesse
            // 
            this.tbWeapon_Finesse.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbWeapon_Finesse.Enabled = false;
            this.tbWeapon_Finesse.Location = new System.Drawing.Point(111, 1330);
            this.tbWeapon_Finesse.Name = "tbWeapon_Finesse";
            this.tbWeapon_Finesse.Size = new System.Drawing.Size(285, 20);
            this.tbWeapon_Finesse.TabIndex = 62;
            // 
            // tbMartial_Weapon_Proficiency
            // 
            this.tbMartial_Weapon_Proficiency.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbMartial_Weapon_Proficiency.Enabled = false;
            this.tbMartial_Weapon_Proficiency.Location = new System.Drawing.Point(161, 617);
            this.tbMartial_Weapon_Proficiency.Name = "tbMartial_Weapon_Proficiency";
            this.tbMartial_Weapon_Proficiency.Size = new System.Drawing.Size(235, 20);
            this.tbMartial_Weapon_Proficiency.TabIndex = 33;
            // 
            // tbExotic_Weapon_Proficiency
            // 
            this.tbExotic_Weapon_Proficiency.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbExotic_Weapon_Proficiency.Enabled = false;
            this.tbExotic_Weapon_Proficiency.Location = new System.Drawing.Point(159, 456);
            this.tbExotic_Weapon_Proficiency.Name = "tbExotic_Weapon_Proficiency";
            this.tbExotic_Weapon_Proficiency.Size = new System.Drawing.Size(237, 20);
            this.tbExotic_Weapon_Proficiency.TabIndex = 19;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.gbAffinities);
            this.tabPage2.Controls.Add(this.gbTalents);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(442, 549);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Talents";
            this.tabPage2.Leave += new System.EventHandler(this.tabPage2_Leave);
            // 
            // gbAffinities
            // 
            this.gbAffinities.Controls.Add(this.cbAir);
            this.gbAffinities.Controls.Add(this.cbEarth);
            this.gbAffinities.Controls.Add(this.cbWater);
            this.gbAffinities.Controls.Add(this.cbFire);
            this.gbAffinities.Controls.Add(this.cbSpirit);
            this.gbAffinities.Dock = System.Windows.Forms.DockStyle.Top;
            this.gbAffinities.Location = new System.Drawing.Point(3, 227);
            this.gbAffinities.Name = "gbAffinities";
            this.gbAffinities.Size = new System.Drawing.Size(436, 134);
            this.gbAffinities.TabIndex = 17;
            this.gbAffinities.TabStop = false;
            this.gbAffinities.Text = "One Power Affinities";
            // 
            // cbAir
            // 
            this.cbAir.AutoSize = true;
            this.cbAir.BackColor = System.Drawing.SystemColors.Control;
            this.cbAir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbAir.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbAir.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.cbAir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cbAir.Location = new System.Drawing.Point(6, 19);
            this.cbAir.Name = "cbAir";
            this.cbAir.Size = new System.Drawing.Size(34, 17);
            this.cbAir.TabIndex = 11;
            this.cbAir.Text = "Air";
            this.cbAir.UseVisualStyleBackColor = false;
            // 
            // cbEarth
            // 
            this.cbEarth.AutoSize = true;
            this.cbEarth.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbEarth.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbEarth.ForeColor = System.Drawing.Color.Brown;
            this.cbEarth.Location = new System.Drawing.Point(6, 42);
            this.cbEarth.Name = "cbEarth";
            this.cbEarth.Size = new System.Drawing.Size(49, 17);
            this.cbEarth.TabIndex = 12;
            this.cbEarth.Text = "Earth";
            // 
            // cbWater
            // 
            this.cbWater.AutoSize = true;
            this.cbWater.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbWater.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbWater.ForeColor = System.Drawing.Color.LightSeaGreen;
            this.cbWater.Location = new System.Drawing.Point(6, 111);
            this.cbWater.Name = "cbWater";
            this.cbWater.Size = new System.Drawing.Size(53, 17);
            this.cbWater.TabIndex = 15;
            this.cbWater.Text = "Water";
            // 
            // cbFire
            // 
            this.cbFire.AutoSize = true;
            this.cbFire.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbFire.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbFire.ForeColor = System.Drawing.Color.OrangeRed;
            this.cbFire.Location = new System.Drawing.Point(6, 65);
            this.cbFire.Name = "cbFire";
            this.cbFire.Size = new System.Drawing.Size(40, 17);
            this.cbFire.TabIndex = 13;
            this.cbFire.Text = "Fire";
            // 
            // cbSpirit
            // 
            this.cbSpirit.AutoSize = true;
            this.cbSpirit.BackColor = System.Drawing.SystemColors.Control;
            this.cbSpirit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbSpirit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbSpirit.ForeColor = System.Drawing.Color.Yellow;
            this.cbSpirit.Location = new System.Drawing.Point(6, 88);
            this.cbSpirit.Name = "cbSpirit";
            this.cbSpirit.Size = new System.Drawing.Size(48, 17);
            this.cbSpirit.TabIndex = 14;
            this.cbSpirit.Text = "Spirit";
            this.cbSpirit.UseVisualStyleBackColor = false;
            // 
            // gbTalents
            // 
            this.gbTalents.Controls.Add(this.cbBalefire);
            this.gbTalents.Controls.Add(this.cbTravling);
            this.gbTalents.Controls.Add(this.cbIllusion);
            this.gbTalents.Controls.Add(this.cbWarding);
            this.gbTalents.Controls.Add(this.cbHealing);
            this.gbTalents.Controls.Add(this.cbCloudDancing);
            this.gbTalents.Controls.Add(this.cbElementalism);
            this.gbTalents.Controls.Add(this.cbEarthSinging);
            this.gbTalents.Controls.Add(this.cbConjunction);
            this.gbTalents.Dock = System.Windows.Forms.DockStyle.Top;
            this.gbTalents.Location = new System.Drawing.Point(3, 3);
            this.gbTalents.Name = "gbTalents";
            this.gbTalents.Size = new System.Drawing.Size(436, 224);
            this.gbTalents.TabIndex = 16;
            this.gbTalents.TabStop = false;
            this.gbTalents.Text = "Talents";
            // 
            // cbBalefire
            // 
            this.cbBalefire.AutoSize = true;
            this.cbBalefire.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbBalefire.Location = new System.Drawing.Point(6, 19);
            this.cbBalefire.Name = "cbBalefire";
            this.cbBalefire.Size = new System.Drawing.Size(54, 17);
            this.cbBalefire.TabIndex = 0;
            this.cbBalefire.Text = "Balefire";
            // 
            // cbTravling
            // 
            this.cbTravling.AutoSize = true;
            this.cbTravling.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbTravling.Location = new System.Drawing.Point(6, 180);
            this.cbTravling.Name = "cbTravling";
            this.cbTravling.Size = new System.Drawing.Size(63, 17);
            this.cbTravling.TabIndex = 7;
            this.cbTravling.Text = "Traveling";
            // 
            // cbIllusion
            // 
            this.cbIllusion.AutoSize = true;
            this.cbIllusion.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbIllusion.Location = new System.Drawing.Point(6, 157);
            this.cbIllusion.Name = "cbIllusion";
            this.cbIllusion.Size = new System.Drawing.Size(51, 17);
            this.cbIllusion.TabIndex = 6;
            this.cbIllusion.Text = "Illusion";
            // 
            // cbWarding
            // 
            this.cbWarding.AutoSize = true;
            this.cbWarding.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbWarding.Location = new System.Drawing.Point(6, 203);
            this.cbWarding.Name = "cbWarding";
            this.cbWarding.Size = new System.Drawing.Size(59, 17);
            this.cbWarding.TabIndex = 8;
            this.cbWarding.Text = "Warding";
            // 
            // cbHealing
            // 
            this.cbHealing.AutoSize = true;
            this.cbHealing.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbHealing.Location = new System.Drawing.Point(6, 134);
            this.cbHealing.Name = "cbHealing";
            this.cbHealing.Size = new System.Drawing.Size(55, 17);
            this.cbHealing.TabIndex = 5;
            this.cbHealing.Text = "Healing";
            // 
            // cbCloudDancing
            // 
            this.cbCloudDancing.AutoSize = true;
            this.cbCloudDancing.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbCloudDancing.Location = new System.Drawing.Point(6, 42);
            this.cbCloudDancing.Name = "cbCloudDancing";
            this.cbCloudDancing.Size = new System.Drawing.Size(89, 17);
            this.cbCloudDancing.TabIndex = 1;
            this.cbCloudDancing.Text = "Cloud Dancing";
            // 
            // cbElementalism
            // 
            this.cbElementalism.AutoSize = true;
            this.cbElementalism.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbElementalism.Location = new System.Drawing.Point(6, 111);
            this.cbElementalism.Name = "cbElementalism";
            this.cbElementalism.Size = new System.Drawing.Size(80, 17);
            this.cbElementalism.TabIndex = 4;
            this.cbElementalism.Text = "Elementalism";
            // 
            // cbEarthSinging
            // 
            this.cbEarthSinging.AutoSize = true;
            this.cbEarthSinging.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbEarthSinging.Location = new System.Drawing.Point(6, 88);
            this.cbEarthSinging.Name = "cbEarthSinging";
            this.cbEarthSinging.Size = new System.Drawing.Size(82, 17);
            this.cbEarthSinging.TabIndex = 3;
            this.cbEarthSinging.Text = "Earth Singing";
            // 
            // cbConjunction
            // 
            this.cbConjunction.AutoSize = true;
            this.cbConjunction.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbConjunction.Location = new System.Drawing.Point(6, 65);
            this.cbConjunction.Name = "cbConjunction";
            this.cbConjunction.Size = new System.Drawing.Size(75, 17);
            this.cbConjunction.TabIndex = 2;
            this.cbConjunction.Text = "Conjunction";
            // 
            // tpHistory
            // 
            this.tpHistory.Controls.Add(this.tbGeschichte);
            this.tpHistory.Location = new System.Drawing.Point(4, 22);
            this.tpHistory.Name = "tpHistory";
            this.tpHistory.Padding = new System.Windows.Forms.Padding(3);
            this.tpHistory.Size = new System.Drawing.Size(442, 549);
            this.tpHistory.TabIndex = 5;
            this.tpHistory.Text = "Geschichte";
            // 
            // tbGeschichte
            // 
            this.tbGeschichte.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbGeschichte.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbGeschichte.Location = new System.Drawing.Point(3, 3);
            this.tbGeschichte.Multiline = true;
            this.tbGeschichte.Name = "tbGeschichte";
            this.tbGeschichte.Size = new System.Drawing.Size(436, 543);
            this.tbGeschichte.TabIndex = 0;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.tbSpr�che);
            this.tabPage4.Controls.Add(this.bKopieren);
            this.tabPage4.Controls.Add(this.tbSpruch);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(442, 549);
            this.tabPage4.TabIndex = 6;
            this.tabPage4.Text = "Spruch";
            // 
            // tbSpr�che
            // 
            this.tbSpr�che.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbSpr�che.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbSpr�che.Location = new System.Drawing.Point(3, 85);
            this.tbSpr�che.Multiline = true;
            this.tbSpr�che.Name = "tbSpr�che";
            this.tbSpr�che.ReadOnly = true;
            this.tbSpr�che.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbSpr�che.Size = new System.Drawing.Size(436, 461);
            this.tbSpr�che.TabIndex = 3;
            this.tbSpr�che.Text = resources.GetString("tbSpr�che.Text");
            // 
            // bKopieren
            // 
            this.bKopieren.Dock = System.Windows.Forms.DockStyle.Top;
            this.bKopieren.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bKopieren.Location = new System.Drawing.Point(3, 62);
            this.bKopieren.Name = "bKopieren";
            this.bKopieren.Size = new System.Drawing.Size(436, 23);
            this.bKopieren.TabIndex = 2;
            this.bKopieren.Text = "Kopieren";
            this.bKopieren.Click += new System.EventHandler(this.bKopieren_Click);
            // 
            // tbSpruch
            // 
            this.tbSpruch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbSpruch.Dock = System.Windows.Forms.DockStyle.Top;
            this.tbSpruch.Location = new System.Drawing.Point(3, 3);
            this.tbSpruch.Multiline = true;
            this.tbSpruch.Name = "tbSpruch";
            this.tbSpruch.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbSpruch.Size = new System.Drawing.Size(436, 59);
            this.tbSpruch.TabIndex = 0;
            // 
            // printPreviewDialog1
            // 
            this.printPreviewDialog1.AutoScrollMargin = new System.Drawing.Size(0, 0);
            this.printPreviewDialog1.AutoScrollMinSize = new System.Drawing.Size(0, 0);
            this.printPreviewDialog1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.printPreviewDialog1.ClientSize = new System.Drawing.Size(400, 300);
            this.printPreviewDialog1.Document = this.printDocument1;
            this.printPreviewDialog1.Enabled = true;
            this.printPreviewDialog1.Icon = ((System.Drawing.Icon)(resources.GetObject("printPreviewDialog1.Icon")));
            this.printPreviewDialog1.Name = "printPreviewDialog1";
            this.printPreviewDialog1.Visible = false;
            // 
            // saveSKLFileDialog
            // 
            this.saveSKLFileDialog.DefaultExt = "skl";
            this.saveSKLFileDialog.Filter = "Wheel of Time Skills (*.skl)|*.skl|Alle Dateien|*.*";
            // 
            // openSKLFileDialog
            // 
            this.openSKLFileDialog.Filter = "Wheel of Time Skills (*.skl)|*.skl|Alle Dateien|*.*";
            // 
            // ChildWnd
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1170, 575);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.toolStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ChildWnd";
            this.Text = "ChildWnd";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.toolStrip1.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.SkillsPanel.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.tpFeats.ResumeLayout(false);
            this.gbLost_Ability_Feats.ResumeLayout(false);
            this.gbLost_Ability_Feats.PerformLayout();
            this.gbChanneling_Feats.ResumeLayout(false);
            this.gbChanneling_Feats.PerformLayout();
            this.gbSpecial_Feats.ResumeLayout(false);
            this.gbSpecial_Feats.PerformLayout();
            this.gbGeneral_Feats.ResumeLayout(false);
            this.gbGeneral_Feats.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.gbAffinities.ResumeLayout(false);
            this.gbAffinities.PerformLayout();
            this.gbTalents.ResumeLayout(false);
            this.gbTalents.PerformLayout();
            this.tpHistory.ResumeLayout(false);
            this.tpHistory.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PrintDialog printDialog1;
        private System.Drawing.Printing.PrintDocument printDocument1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.SaveFileDialog saveWOTFileDialog;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.PrintPreviewControl printPreviewControl1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.PropertyGrid propertyGrid1;
        private System.Windows.Forms.TabPage tpBackground_Feats;
        private System.Windows.Forms.TabPage tpBackgroundSkills;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Panel SkillsPanel;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button bLoadSkills;
        private System.Windows.Forms.Button bSaveSkills;
        private System.Windows.Forms.Button bCreateSkill;
        private System.Windows.Forms.TextBox tbSkillName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lSkillRest;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lSkillUsed;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lSkillGesamt;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.GroupBox gbAffinities;
        private System.Windows.Forms.CheckBox cbAir;
        private System.Windows.Forms.CheckBox cbEarth;
        private System.Windows.Forms.CheckBox cbWater;
        private System.Windows.Forms.CheckBox cbFire;
        private System.Windows.Forms.CheckBox cbSpirit;
        private System.Windows.Forms.GroupBox gbTalents;
        private System.Windows.Forms.CheckBox cbBalefire;
        private System.Windows.Forms.CheckBox cbTravling;
        private System.Windows.Forms.CheckBox cbIllusion;
        private System.Windows.Forms.CheckBox cbWarding;
        private System.Windows.Forms.CheckBox cbHealing;
        private System.Windows.Forms.CheckBox cbCloudDancing;
        private System.Windows.Forms.CheckBox cbElementalism;
        private System.Windows.Forms.CheckBox cbEarthSinging;
        private System.Windows.Forms.CheckBox cbConjunction;
        private System.Windows.Forms.ToolStripButton toolStripSaveButton1;
        private System.Windows.Forms.ToolStripButton tsbSaveAs;
        private System.Windows.Forms.ToolStripButton toolStripPrintButton;
        private System.Windows.Forms.TabPage tpHistory;
        private System.Windows.Forms.TextBox tbGeschichte;
        private System.Windows.Forms.ToolStripButton bBackground;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TextBox tbSpruch;
        private System.Windows.Forms.ToolStripButton bPrintPreview;
        private System.Windows.Forms.PrintPreviewDialog printPreviewDialog1;
        private System.Windows.Forms.ToolStripButton bAktualisieren;
        private System.Windows.Forms.Button bKopieren;
        private System.Windows.Forms.TextBox tbSpr�che;
        private System.Windows.Forms.TabPage tpFeats;
        private System.Windows.Forms.GroupBox gbSpecial_Feats;
        private System.Windows.Forms.GroupBox gbGeneral_Feats;
        private System.Windows.Forms.GroupBox gbLost_Ability_Feats;
        private System.Windows.Forms.GroupBox gbChanneling_Feats;
        private System.Windows.Forms.CheckBox cbAlertness;
        private System.Windows.Forms.CheckBox cbAmbidextrous;
        private System.Windows.Forms.CheckBox cbAnimal_Affinity;
        private System.Windows.Forms.CheckBox cbArmor_Proficiency_light;
        private System.Windows.Forms.CheckBox cbArmor_Proficiency_medium;
        private System.Windows.Forms.CheckBox cbArmor_Proficiency_heavy;
        private System.Windows.Forms.CheckBox cbAthletic;
        private System.Windows.Forms.CheckBox cbBlind_Fight;
        private System.Windows.Forms.CheckBox cbCombat_Expertise;
        private System.Windows.Forms.CheckBox cbImproved_Disarm;
        private System.Windows.Forms.CheckBox cbImproved_Trip;
        private System.Windows.Forms.CheckBox cbWhirlwind_Attack;
        private System.Windows.Forms.CheckBox cbCombat_Reflexes;
        private System.Windows.Forms.CheckBox cbThe_Dark_One_s_own_Luck;
        private System.Windows.Forms.CheckBox cbDodge;
        private System.Windows.Forms.CheckBox cbMobility;
        private System.Windows.Forms.CheckBox cbSpring_Attack;
        private System.Windows.Forms.CheckBox cbEndurance;
        private System.Windows.Forms.CheckBox cbExotic_Weapon_Proficiency;
        private System.Windows.Forms.CheckBox cbFame;
        private System.Windows.Forms.TextBox tbExotic_Weapon_Proficiency;
        private System.Windows.Forms.CheckBox cbGreat_Fortitude;
        private System.Windows.Forms.CheckBox cbImproved_Critical;
        private System.Windows.Forms.CheckBox cbImproved_Initiative;
        private System.Windows.Forms.CheckBox cbImproved_Unarmed_Strike;
        private System.Windows.Forms.CheckBox cbIron_Will;
        private System.Windows.Forms.CheckBox cbLightning_Reflexes;
        private System.Windows.Forms.CheckBox cbMental_Stability;
        private System.Windows.Forms.CheckBox cbMartial_Weapon_Proficiency;
        private System.Windows.Forms.CheckBox cbMimic;
        private System.Windows.Forms.CheckBox cbMounted_Combat;
        private System.Windows.Forms.CheckBox cbMounted_Archery;
        private System.Windows.Forms.TextBox tbMartial_Weapon_Proficiency;
        private System.Windows.Forms.CheckBox cbTrample;
        private System.Windows.Forms.CheckBox cbRide_By_Attack;
        private System.Windows.Forms.CheckBox cbNimble;
        private System.Windows.Forms.CheckBox cbPersuasive;
        private System.Windows.Forms.CheckBox cbPoint_Blank_Shot;
        private System.Windows.Forms.CheckBox cbFar_Shot;
        private System.Windows.Forms.CheckBox cbPrecise_Shot;
        private System.Windows.Forms.CheckBox cbRapid_Shot;
        private System.Windows.Forms.CheckBox cbShot_on_the_Run;
        private System.Windows.Forms.CheckBox cbPower_Attack;
        private System.Windows.Forms.CheckBox cbCleave;
        private System.Windows.Forms.CheckBox cbGreat_Cleave;
        private System.Windows.Forms.CheckBox cbImproved_Bull_Rush;
        private System.Windows.Forms.CheckBox cbQuick_Draw;
        private System.Windows.Forms.CheckBox cbQuickness;
        private System.Windows.Forms.CheckBox cbRun;
        private System.Windows.Forms.CheckBox cbSharp_eyed;
        private System.Windows.Forms.CheckBox cbShield_Proficiency;
        private System.Windows.Forms.CheckBox cbSimple_Weapon_Proficiency;
        private System.Windows.Forms.CheckBox cbSkill_Emphasis;
        private System.Windows.Forms.CheckBox cbStealthy;
        private System.Windows.Forms.CheckBox cbToughness;
        private System.Windows.Forms.CheckBox cbTrack;
        private System.Windows.Forms.CheckBox cbTrustworthy;
        private System.Windows.Forms.CheckBox cbTwo_Weapon_Fighting;
        private System.Windows.Forms.CheckBox cbImproved_Two_Weapon_Fighting;
        private System.Windows.Forms.CheckBox cbWeapon_Finesse;
        private System.Windows.Forms.CheckBox cbWeapon_Focus;
        private System.Windows.Forms.TextBox tbWeapon_Focus;
        private System.Windows.Forms.TextBox tbWeapon_Finesse;
        private System.Windows.Forms.CheckBox cbEliminate_Block;
        private System.Windows.Forms.CheckBox cbWeapon_Specializiation;
        private System.Windows.Forms.CheckBox cbCombat_Casting;
        private System.Windows.Forms.TextBox tbWeapon_Specializiation;
        private System.Windows.Forms.CheckBox cbExtra_Affinity;
        private System.Windows.Forms.CheckBox cbExtra_Talent;
        private System.Windows.Forms.CheckBox cbMultiweave;
        private System.Windows.Forms.CheckBox cbPower_Heightened_Senses;
        private System.Windows.Forms.CheckBox cbSense_Residue;
        private System.Windows.Forms.CheckBox cbTie_Off_Weave;
        private System.Windows.Forms.CheckBox cbLatent_Dreamer;
        private System.Windows.Forms.CheckBox cbDreamwalk;
        private System.Windows.Forms.CheckBox cbBend_Dream;
        private System.Windows.Forms.CheckBox cbDreamjump;
        private System.Windows.Forms.CheckBox cbWaken_Dream;
        private System.Windows.Forms.CheckBox cbDreamwatch;
        private System.Windows.Forms.CheckBox cbLatent_Foreteller;
        private System.Windows.Forms.CheckBox cbForeteller;
        private System.Windows.Forms.CheckBox cbLatent_Old_Blood;
        private System.Windows.Forms.CheckBox cbOld_Blood;
        private System.Windows.Forms.CheckBox cbLatent_Sniffer;
        private System.Windows.Forms.CheckBox cbSniffer;
        private System.Windows.Forms.CheckBox cbLatent_Treesinger;
        private System.Windows.Forms.CheckBox cbTreesinger;
        private System.Windows.Forms.CheckBox cbLatent_Viewer;
        private System.Windows.Forms.CheckBox cbViewing;
        private System.Windows.Forms.SaveFileDialog saveSKLFileDialog;
        private System.Windows.Forms.OpenFileDialog openSKLFileDialog;
        private System.Windows.Forms.Button bClear;
        private Points pAnimal_Empathy;
        private Points pAppraise;
        private Points pBalance;
        private Points pBluff;
        private Points pClimb;
        private Points pConcentration;
        private Points pCraft;
        private Points pDecipher_Script;
        private Points pDiplomacy;
        private Points pDisable_Device;
        private Points pDisguise;
        private Points pEscape_Artist;
        private Points pForgery;
        private Points pGather_Information;
        private Points pHandle_Animal;
        private Points pHeal;
        private Points pHide;
        private Points pInnuendo;
        private Points pIntimidate;
        private Points pIntuit_Direction;
        private Points pJump;
        private Points pKnowledge_Legends;
        private Points pKnowledge_Arcana;
        private Points pKnowledge_Arch;
        private Points pKnowledge_Blight;
        private Points pKnowledge_Geography;
        private Points pKnowledge_History;
        private Points pKnowledge_Local;
        private Points pKnowledge_Nature;
        private Points pKnowledge_Nobility;
        private Points pListen;
        private Points pMove_Silently;
        private Points pOpen_Lock;
        private Points pPick_Pocket;
        private Points pRead_Lips;
        private Points pRide;
        private Points pSearch;
        private Points pSense_Motive;
        private Points pSpot;
        private Points pSwim;
        private Points pTumble;
        private Points pUse_Rope;
        private Points pWeavesight;
        private Points pWilderness_Lore;
    }
}