using System;
using System.Drawing;
using System.Drawing.Printing;
using System.Windows.Forms;

namespace Wheel_of_Time___Character_Sheets
{
    partial class ChildWnd
    {
        int currentpage = 1;
        private void PrintPreview(object sender, PrintPageEventArgs e)
        {
            //pagewidth = 826



            Font ParchmentBig = new Font("Parchment", 65, FontStyle.Bold);
            Font Corsiva = new Font("Monotype Corsiva", 15, FontStyle.Italic);
            Font CorsivaSmall = new Font("Monotype Corsiva", 10, FontStyle.Italic);
            Font MicrosoftSansSerif = new Font("Microsoft Sans Serif", 11, FontStyle.Bold);

            //Hintergrund
            if (bBackground.Checked)
            {
                e.Graphics.DrawImage(Wheel_of_Time___Character_Sheets.Properties.Resources.Hintergrund, new Rectangle(0, 0, e.PageBounds.Width, e.PageBounds.Height));
            }

            //Seitenzahl
            //e.Graphics.DrawString("("+currentpage+"/2)", MicrosoftSansSerif, Brushes.Black, 400, 4);

            //�berschrift
            e.Graphics.DrawString("the Wheel of Time", ParchmentBig, Brushes.Black, 200, 10);

            //Allgemein
            //Zeile y=100
            e.Graphics.DrawString("Chronik:", Corsiva, Brushes.Black, 100, 100);
            e.Graphics.DrawString(character.Chronik, Corsiva, Brushes.Black, 210, 100);
            //Zeile y=125
            e.Graphics.DrawString("Spieler:", Corsiva, Brushes.Black, 100, 125);
            e.Graphics.DrawString(character.Spieler, Corsiva, Brushes.Black, 210, 125);
            e.Graphics.DrawString("Name:", Corsiva, Brushes.Black, 360, 125);
            e.Graphics.DrawString(character.Name, Corsiva, Brushes.Black, 470, 125);

            if (currentpage == 1)
            {
                string Klasse = "";
                Klasse += character.Algai_d_Siswai ? "Algei'd'Siswai " : "";
                Klasse += character.Armsman ? "Armsman " : "";
                Klasse += character.Initiate ? "Initiate " : "";
                Klasse += character.Noble ? "Noble " : "";
                Klasse += character.Wanderer ? "Wanderer " : "";
                Klasse += character.Wilder ? "Wilder " : "";
                Klasse += character.Woodsman ? "Woodsman" : "";

                string Augen = "";
                if (character.Augen.IsKnownColor)
                { Augen = character.Augen.ToKnownColor().ToString(); }
                else
                { Augen = character.Augen.R.ToString() + " " + character.Augen.G.ToString() + " " + character.Augen.B.ToString(); }

                string Haare = "";
                if (character.Haare.IsKnownColor)
                { Haare = character.Haare.ToKnownColor().ToString(); }
                else
                { Haare = character.Haare.R.ToString() + " " + character.Haare.G.ToString() + " " + character.Haare.B.ToString(); }

                string Haut = "";
                if (character.Haut.IsKnownColor)
                { Haut = character.Haut.ToKnownColor().ToString(); }
                else
                { Haut = character.Haut.R.ToString() + " " + character.Haut.G.ToString() + " " + character.Haut.B.ToString(); }

                string BackgroundFeats = "";
                bool firstBackgroundFeat = true;
                foreach (CheckBox cb in tpBackground_Feats.Controls)
                {
                    if (cb.Checked)
                    {
                        BackgroundFeats += cb.Text;
                        if (firstBackgroundFeat)
                        {
                            BackgroundFeats += ", ";
                            firstBackgroundFeat = false;
                        }
                    }
                }

                //Zeile y=175
                e.Graphics.DrawString("Background:", Corsiva, Brushes.Black, 100, 175);
                e.Graphics.DrawString(character.Background, Corsiva, Brushes.Black, 210, 175);
                e.Graphics.DrawString("Geschlecht:", Corsiva, Brushes.Black, 360, 175);
                e.Graphics.DrawString(character.Geschlecht, Corsiva, Brushes.Black, 470, 175);
                e.Graphics.DrawString("Level:", Corsiva, Brushes.Black, 600, 175);
                e.Graphics.DrawString(character.Level, Corsiva, Brushes.Black, 660, 175);
                //Zeile y=200
                e.Graphics.DrawString("Klasse:", Corsiva, Brushes.Black, 100, 200);
                e.Graphics.DrawString(Klasse, Corsiva, Brushes.Black, 210, 200);
                e.Graphics.DrawString("Alter:", Corsiva, Brushes.Black, 600, 200);
                e.Graphics.DrawString(character.Alter.ToString(), Corsiva, Brushes.Black, 660, 200);
                //Linie y=225
                e.Graphics.DrawLine(new Pen(Brushes.Black), 100, 225, e.PageBounds.Width - 100, 225);
                //Zeile y=225
                e.Graphics.DrawString("Gr��e:", Corsiva, Brushes.Black, 100, 225);
                e.Graphics.DrawString(character.Gr��e.ToString() + " cm", Corsiva, Brushes.Black, 210, 225);
                e.Graphics.DrawString("Gewicht:", Corsiva, Brushes.Black, 340, 225);
                e.Graphics.DrawString(character.Gewicht.ToString() + " kg", Corsiva, Brushes.Black, 450, 225);
                //Zeile y=250
                e.Graphics.DrawString("Augenfarbe:", Corsiva, Brushes.Black, 100, 250);
                e.Graphics.DrawString(Augen, Corsiva, Brushes.Black, 210, 250);
                e.Graphics.DrawString("Haarfarbe:", Corsiva, Brushes.Black, 340, 250);
                e.Graphics.DrawString(Haare, Corsiva, Brushes.Black, 450, 250);
                e.Graphics.DrawString("Hautfarbe:", Corsiva, Brushes.Black, 580, 250);
                e.Graphics.DrawString(Haut, Corsiva, Brushes.Black, 690, 250);
                //Linie y=275
                e.Graphics.DrawLine(new Pen(Brushes.Black), 100, 275, e.PageBounds.Width - 100, 275);
                //Zeile y=275
                e.Graphics.DrawString("Charisma:", Corsiva, Brushes.Black, 100, 275);
                e.Graphics.DrawString(character.Charisma.ToString(), Corsiva, Brushes.Black, 210, 275);
                e.Graphics.DrawString("Geschick:", Corsiva, Brushes.Black, 340, 275);
                e.Graphics.DrawString(character.Geschicklichkeit.ToString(), Corsiva, Brushes.Black, 450, 275);
                e.Graphics.DrawString("Intelligenz:", Corsiva, Brushes.Black, 580, 275);
                e.Graphics.DrawString(character.Intelligenz.ToString(), Corsiva, Brushes.Black, 690, 275);
                //Zeile y=300
                e.Graphics.DrawString("Konstitution:", Corsiva, Brushes.Black, 100, 300);
                e.Graphics.DrawString(character.Konstitution.ToString(), Corsiva, Brushes.Black, 210, 300);
                e.Graphics.DrawString("St�rke:", Corsiva, Brushes.Black, 340, 300);
                e.Graphics.DrawString(character.St�rke.ToString(), Corsiva, Brushes.Black, 450, 300);
                e.Graphics.DrawString("Weisheit:", Corsiva, Brushes.Black, 580, 300);
                e.Graphics.DrawString(character.Weisheit.ToString(), Corsiva, Brushes.Black, 690, 300);
                //Linie y=325
                //e.Graphics.DrawLine(new Pen(Brushes.Black), 100, 325, e.PageBounds.Width - 100, 325);
                //Linie Mitte
                //e.Graphics.DrawLine(new Pen(Brushes.Black), e.PageBounds.Width / 2, 345, e.PageBounds.Width / 2, 840);

                int LineHeight = 0;
                int Half = 0;
                int Count = 0;
                if (SkillsPanel.Controls.Count != 0)
                {
                    for (int i = 0; i < SkillsPanel.Controls.Count; i++)
                    {
                        Points p = (Points)SkillsPanel.Controls[i];
                        if (p.Aktiv)
                        {
                            Count++;
                        }
                    }

                    Points[] AktiveSkills = new Points[Count];
                    Count = 0;
                    for (int i = 0; i < SkillsPanel.Controls.Count; i++)
                    {
                        Points p = (Points)SkillsPanel.Controls[i];
                        if (p.Aktiv)
                        {
                            AktiveSkills[Count] = p;
                            Count++;
                        }
                    }

                    Half = Convert.ToInt32(Math.Ceiling(AktiveSkills.Length / 2.0));
                    LineHeight = Convert.ToInt32(Math.Floor(500.0 / Half));

                    for (int i = 0; i < Half; i++)
                    {
                        e.Graphics.DrawString(AktiveSkills[i].SkillName, Corsiva, Brushes.Black, 100, 350 + (i) * LineHeight);
                        for (int k = 0; k < 8; k++)
                        {
                            e.Graphics.DrawEllipse(new Pen(Brushes.Black), 290 + k * 15, 358 + (i) * LineHeight, 10, 10);
                        }
                        for (int j = 0; j < AktiveSkills[i].GivenPoints; j++)
                        {
                            e.Graphics.FillEllipse(Brushes.Black, 290 + j * 15, 358 + (i) * LineHeight, 10, 10);
                        }
                    }

                    for (int i = Half; i < AktiveSkills.Length; i++)
                    {
                        e.Graphics.DrawString(AktiveSkills[i].SkillName, Corsiva, Brushes.Black, 410, 350 + (i - Half) * LineHeight);
                        for (int k = 0; k < 8; k++)
                        {
                            e.Graphics.DrawEllipse(new Pen(Brushes.Black), 604 + k * 15, 358 + (i - Half) * LineHeight, 10, 10);
                        }
                        for (int j = 0; j < AktiveSkills[i].GivenPoints; j++)
                        {
                            e.Graphics.FillEllipse(Brushes.Black, 604 + j * 15, 358 + (i - Half) * LineHeight, 10, 10);
                        }
                    }
                }

                //Linie y=875
                //e.Graphics.DrawLine(new Pen(Brushes.Black), 100, 875, e.PageBounds.Width - 100, 875);
                //Zeile y=875
                e.Graphics.DrawString("Background Feats:", Corsiva, Brushes.Black, 100, 875);
                e.Graphics.DrawString(BackgroundFeats, Corsiva, Brushes.Black, 260, 875);

                //Zeile y=925
                e.Graphics.DrawString(tbSpruch.Text, Corsiva, Brushes.Black, new Rectangle(100, 925, 626, 300));
            }
            if (currentpage == 2)
            {

                int Line = 175;
                if (character.Initiate || character.Wilder)
                {
                    string BalefireTalents = "";
                    string CloudDancingTalents = "";
                    string ConjunctionTalents = "";
                    string EarthSingingTalents = "";
                    string ElementalismTalents = "";
                    string HealingTalents = "";
                    string IllusionTalents = "";
                    string TravelingTalents = "";
                    string WardingTalents = "";
                    if (cbBalefire.Checked)
                    {
                        BalefireTalents += "Balefire: Balefire (Lost)\r\n";
                    }
                    if (cbCloudDancing.Checked)
                    {
                        CloudDancingTalents += cbAir.Checked || cbWater.Checked ? "Foretell Weather, " : "";
                        CloudDancingTalents += cbAir.Checked || cbWater.Checked ? "Harness the Wind, " : "";
                        CloudDancingTalents += cbAir.Checked || cbFire.Checked ? "Lightning, " : "";
                        CloudDancingTalents += cbAir.Checked || cbWater.Checked ? "Raise Fog, " : "";
                        CloudDancingTalents += cbAir.Checked || cbFire.Checked ? "Warmth" : "";
                    }
                    if (cbConjunction.Checked)
                    {
                        ConjunctionTalents += cbSpirit.Checked ? "Bond Warder, " : "";
                        ConjunctionTalents += "Compulsion (Lost), ";
                        ConjunctionTalents += cbAir.Checked || cbEarth.Checked || cbSpirit.Checked ? "False Trail, " : "";
                        ConjunctionTalents += cbSpirit.Checked ? "Pass Bond, " : "";
                        ConjunctionTalents += cbSpirit.Checked ? "Sense Shadowspawn, " : "";
                        ConjunctionTalents += cbSpirit.Checked ? "Trace" : "";
                    }
                    if (cbEarthSinging.Checked)
                    {
                        EarthSingingTalents += cbEarth.Checked ? "Earth Delving, " : "";
                        EarthSingingTalents += cbEarth.Checked ? "Earthquake, " : "";
                        EarthSingingTalents += cbEarth.Checked || cbFire.Checked ? "Grenade, " : "";
                        EarthSingingTalents += cbEarth.Checked ? "Polish, " : "";
                        EarthSingingTalents += cbEarth.Checked || cbFire.Checked ? "Riven Earth" : "";
                    }
                    if (cbElementalism.Checked)
                    {
                        ElementalismTalents += cbAir.Checked ? "Arms of Air, " : "";
                        ElementalismTalents += cbAir.Checked || cbFire.Checked ? "Blade of Fire, " : "";
                        ElementalismTalents += cbFire.Checked ? "Create Fire, " : "";
                        ElementalismTalents += cbSpirit.Checked || cbWater.Checked ? "Current, " : "";
                        ElementalismTalents += cbAir.Checked || cbFire.Checked ? "Cutting Lines of Fire (Lost), " : "";
                        ElementalismTalents += cbWater.Checked ? "Dry, " : "";
                        ElementalismTalents += cbAir.Checked || cbEarth.Checked ? "False Wall, " : "";
                        ElementalismTalents += cbAir.Checked || cbFire.Checked || cbSpirit.Checked ? "Fiery Sword, " : "";
                        ElementalismTalents += cbAir.Checked || cbFire.Checked ? "Fireball, " : "";
                        ElementalismTalents += cbAir.Checked || cbSpirit.Checked ? "Fly (Lost), " : "";
                        ElementalismTalents += cbAir.Checked ? "Harden Air, " : "";
                        ElementalismTalents += cbFire.Checked || cbSpirit.Checked ? "Immolate, " : "";
                        ElementalismTalents += cbAir.Checked || cbFire.Checked ? "Light, " : "";
                        ElementalismTalents += cbWater.Checked ? "Move Water, " : "";
                        ElementalismTalents += cbAir.Checked ? "Tool of Air, " : "";
                        ElementalismTalents += cbEarth.Checked || cbFire.Checked ? "Wand of Fire, " : "";
                        ElementalismTalents += cbSpirit.Checked || cbWater.Checked ? "Whirlpool" : "";
                    }
                    if (cbHealing.Checked)
                    {
                        HealingTalents += cbSpirit.Checked ? "Delive, " : "";
                        HealingTalents += cbAir.Checked || cbSpirit.Checked || cbWater.Checked ? "Heal, " : "";
                        HealingTalents += cbAir.Checked || cbSpirit.Checked || cbWater.Checked ? "Heal the Mind, " : "";
                        HealingTalents += cbAir.Checked || cbSpirit.Checked || cbWater.Checked ? "Rend (Rare), " : "";
                        HealingTalents += cbAir.Checked || cbSpirit.Checked || cbWater.Checked ? "Renew, " : "";
                        HealingTalents += "Restore the Power (Lost), ";
                        HealingTalents += cbSpirit.Checked ? "Sever, " : "";
                        HealingTalents += cbEarth.Checked || cbFire.Checked || cbSpirit.Checked || cbWater.Checked ? "Touch of Death (Lost)" : "";
                    }
                    if (cbIllusion.Checked)
                    {
                        IllusionTalents += cbAir.Checked || cbFire.Checked || cbSpirit.Checked ? "Disguise, " : "";
                        IllusionTalents += cbAir.Checked || cbSpirit.Checked ? "Distant Eve (Lost), " : "";
                        IllusionTalents += cbAir.Checked || cbSpirit.Checked ? "Eavesdrop, " : "";
                        IllusionTalents += cbAir.Checked || cbFire.Checked ? "Folded Light, " : "";
                        IllusionTalents += cbAir.Checked || cbFire.Checked || cbSpirit.Checked ? "Mirror of Mists, " : "";
                        IllusionTalents += cbAir.Checked || cbFire.Checked ? "Voice of Power" : "";
                    }
                    if (cbTravling.Checked)
                    {
                        TravelingTalents += cbEarth.Checked || cbSpirit.Checked ? "Bridge Between Worlds (Lost), " : "";
                        TravelingTalents += cbSpirit.Checked ? "Create Gateway (Lost), " : "";
                        TravelingTalents += cbAir.Checked || cbEarth.Checked || cbSpirit.Checked ? "Skimming (Lost), " : "";
                        TravelingTalents += cbSpirit.Checked ? "Use Portal Stone (Rare)" : "";
                    }
                    if (cbWarding.Checked)
                    {
                        WardingTalents += "Warding: ";
                        WardingTalents += cbAir.Checked || cbFire.Checked || cbSpirit.Checked ? "Barrier to Sight, " : "";
                        WardingTalents += cbAir.Checked || cbFire.Checked || cbWater.Checked ? "Circle of Silence, " : "";
                        WardingTalents += cbSpirit.Checked ? "Dream Shielding, " : "";
                        WardingTalents += cbAir.Checked || cbFire.Checked || cbSpirit.Checked ? "Fire Trap (Rare), " : "";
                        WardingTalents += "Master Ward, ";
                        WardingTalents += cbAir.Checked || cbFire.Checked || cbSpirit.Checked ? "Seal, " : "";
                        WardingTalents += cbSpirit.Checked ? "Shield, " : "";
                        WardingTalents += cbAir.Checked || cbFire.Checked || cbSpirit.Checked ? "Strike of Death, " : "";
                        WardingTalents += cbAir.Checked || cbFire.Checked || cbSpirit.Checked ? "Ward against People, " : "";
                        WardingTalents += "Ward against the One Power, ";
                        WardingTalents += cbAir.Checked || cbFire.Checked || cbSpirit.Checked ? "Ward against Shadowspawn, " : "";
                        WardingTalents += "Ward Bore (Lost)";
                        WardingTalents += "\r\n";
                    }

                    if (BalefireTalents != "")
                    {
                        e.Graphics.DrawString("Balefire:", Corsiva, Brushes.Black, 100, Line);
                        e.Graphics.DrawString(BalefireTalents, CorsivaSmall, Brushes.Black, new Rectangle(220, Line + 6, 556, 300));
                        Line += 20;
                    }
                    if (CloudDancingTalents != "")
                    {
                        e.Graphics.DrawString("Cloud Dancing:", Corsiva, Brushes.Black, 100, Line);
                        e.Graphics.DrawString(CloudDancingTalents, CorsivaSmall, Brushes.Black, new Rectangle(220, Line + 6, 556, 300));
                        Line += 20;
                    }
                    if (ConjunctionTalents != "")
                    {
                        e.Graphics.DrawString("Conjunction:", Corsiva, Brushes.Black, 100, Line);
                        e.Graphics.DrawString(ConjunctionTalents, CorsivaSmall, Brushes.Black, new Rectangle(220, Line + 6, 556, 300));
                        Line += 20;
                    }
                    if (EarthSingingTalents != "")
                    {
                        e.Graphics.DrawString("Earth Singing:", Corsiva, Brushes.Black, 100, Line);
                        e.Graphics.DrawString(EarthSingingTalents, CorsivaSmall, Brushes.Black, new Rectangle(220, Line + 6, 556, 300));
                        Line += 20;
                    }
                    if (ElementalismTalents != "")
                    {
                        e.Graphics.DrawString("Elementalism:", Corsiva, Brushes.Black, 100, Line);
                        e.Graphics.DrawString(ElementalismTalents, CorsivaSmall, Brushes.Black, new Rectangle(220, Line + 6, 556, 300));
                        Line += 40;
                    }
                    if (HealingTalents != "")
                    {
                        e.Graphics.DrawString("Healing:", Corsiva, Brushes.Black, 100, Line);
                        e.Graphics.DrawString(HealingTalents, CorsivaSmall, Brushes.Black, new Rectangle(220, Line + 6, 556, 300));
                        Line += 20;
                    }
                    if (IllusionTalents != "")
                    {
                        e.Graphics.DrawString("Illusion:", Corsiva, Brushes.Black, 100, Line);
                        e.Graphics.DrawString(IllusionTalents, CorsivaSmall, Brushes.Black, new Rectangle(220, Line + 6, 556, 300));
                        Line += 20;
                    }
                    if (TravelingTalents != "")
                    {
                        e.Graphics.DrawString("Traveling:", Corsiva, Brushes.Black, 100, Line);
                        e.Graphics.DrawString(TravelingTalents, CorsivaSmall, Brushes.Black, new Rectangle(220, Line + 6, 556, 300));
                        Line += 20;
                    }
                    if (WardingTalents != "")
                    {
                        e.Graphics.DrawString("Warding:", Corsiva, Brushes.Black, 100, Line);
                        e.Graphics.DrawString(WardingTalents, CorsivaSmall, Brushes.Black, new Rectangle(220, Line + 6, 556, 300));
                        Line += 20;
                    }
                    Line += 25;
                }

                //Zeile Line

                e.Graphics.DrawString("Geschichte:", Corsiva, Brushes.Black, 100, Line);

                //Zeile Line+25
                e.Graphics.DrawString(tbGeschichte.Text, Corsiva, Brushes.Black, new Rectangle(100, Line+25, 626, 300));

                string General_Feats = "General Feats: ";
                foreach (Control c in gbGeneral_Feats.Controls)
                {
                    try
                    {
                        CheckBox cb = (CheckBox)c;
                        if (cb.Checked)
                        {
                            General_Feats += cb.Text;
                            if (cb.Text == "Exotic Weapon Proficiency")
                            {
                                General_Feats += " (" + tbExotic_Weapon_Proficiency.Text + ")";
                            }
                            if (cb.Text == "Martial Weapon Proficiency")
                            {
                                General_Feats += " (" + tbMartial_Weapon_Proficiency.Text + ")";
                            }
                            if (cb.Text == "Weapon Finesse")
                            {
                                General_Feats += " (" + tbWeapon_Finesse.Text + ")";
                            }
                            if (cb.Text == "Weapon Focus")
                            {
                                General_Feats += " (" + tbWeapon_Focus.Text + ")";
                            }
                            General_Feats += ", ";
                        }
                    }
                    catch { }
                }
                string Special_Feats = "Special Feats: ";
                foreach (Control c in gbSpecial_Feats.Controls)
                {
                    try
                    {
                        CheckBox cb = (CheckBox)c;
                        if (cb.Checked)
                        {
                            Special_Feats += cb.Text;
                            Special_Feats += ", ";
                            if (cb.Text == "Weapon Specializiation")
                            {
                                General_Feats += " (" + tbWeapon_Specializiation.Text + ")";
                            }
                        }
                    }
                    catch { }
                }
                string Channeling_Feats = "Channeling Feats: ";
                foreach (Control c in gbChanneling_Feats.Controls)
                {
                    try
                    {
                        CheckBox cb = (CheckBox)c;
                        if (cb.Checked)
                        {
                            Channeling_Feats += cb.Text;
                            Channeling_Feats += ", ";
                        }
                    }
                    catch { }
                }
                string Lost_Ability_Feats = "Lost Ability Feats: ";
                foreach (Control c in gbLost_Ability_Feats.Controls)
                {
                    try
                    {
                        CheckBox cb = (CheckBox)c;
                        if (cb.Checked)
                        {
                            Lost_Ability_Feats += cb.Text;
                            Lost_Ability_Feats += ", ";
                        }
                    }
                    catch { }
                }

                //Zeile Line+350
                e.Graphics.DrawLine(new Pen(Brushes.Black), 100, Line + 350, 626, Line + 350);

                //Zeile Line+375
                e.Graphics.DrawString(General_Feats, Corsiva, Brushes.Black, new Rectangle(100, Line + 375, 626, 325));

                //Zeile Line+700
                e.Graphics.DrawString(Special_Feats, Corsiva, Brushes.Black, new Rectangle(100, Line + 700, 626, 50));

                //Zeile Line+750
                e.Graphics.DrawString(Channeling_Feats, Corsiva, Brushes.Black, new Rectangle(100, Line + 750, 626, 75));

                //Zeile Line+825
                e.Graphics.DrawString(Lost_Ability_Feats, Corsiva, Brushes.Black, new Rectangle(100, Line + 825, 626, 200));
            }

            if (currentpage != 2)
            {
                e.HasMorePages = true;
                currentpage++;
            }
            else
            {
                e.HasMorePages = false;
                currentpage = 1;
            }
        }
    }
}