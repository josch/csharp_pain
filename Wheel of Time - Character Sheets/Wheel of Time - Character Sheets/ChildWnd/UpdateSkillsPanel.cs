using System.Windows.Forms;

namespace Wheel_of_Time___Character_Sheets
{
    partial class ChildWnd
    {
        private void UpdateSkillsPanel()
        {
            //Animal Empathy
            pAnimal_Empathy.Aktiv = character.Woodsman || (character.Background == "Ogier");
            if (character.Woodsman)
            { pAnimal_Empathy.Multiplier = 1; }
            else
            { pAnimal_Empathy.Multiplier = 2; }

            //Appraise
            pAppraise.Aktiv = true;
            if (character.Noble || character.Wanderer)
            { pAppraise.Multiplier = 1; }
            else
            { pAppraise.Multiplier = 2; }

            //Balance
            pBalance.Aktiv = true;
            if (character.Algai_d_Siswai || character.Wanderer)
            { pBalance.Multiplier = 1; }
            else
            { pBalance.Multiplier = 2; }

            //Bluff
            pBluff.Aktiv = !(character.Background == "Ogier");
            if (character.Noble || character.Wanderer)
            { pBluff.Multiplier = 1; }
            else
            { pBluff.Multiplier = 2; }

            //Climb
            pClimb.Aktiv = true;
            if (character.Algai_d_Siswai || character.Armsman || character.Wanderer)
            { pClimb.Multiplier = 1; }
            else
            { pClimb.Multiplier = 2; }

            //Concentration
            pConcentration.Aktiv = true;
            if (character.Initiate || character.Wilder || (character.Background == "Ogier"))
            { pConcentration.Multiplier = 1; }
            else
            { pConcentration.Multiplier = 2; }

            //Disable Device
            pDisable_Device.Aktiv = true;
            if (character.Background == "Ogier")
            { pDisable_Device.Multiplier = 2; }
            else
            { pDisable_Device.Multiplier = 1; }

            //Disguise
            pDisguise.Aktiv = !(character.Background == "Ogier");

            //Escape Artist
            pEscape_Artist.Aktiv = true;
            if (character.Background == "Ogier")
            { pEscape_Artist.Multiplier = 2; }
            else
            { pEscape_Artist.Multiplier = 1; }

            //Forgery
            pForgery.Aktiv = !(character.Background == "Ogier");

            //Gather Information
            pGather_Information.Aktiv = true;
            if (character.Background == "Ogier")
            { pGather_Information.Multiplier = 1; }
            else
            { pGather_Information.Multiplier = 2; }

            //Handle Animal
            pHandle_Animal.Aktiv = true;
            if (character.Background == "Ogier")
            { pHandle_Animal.Multiplier = 1; }
            else
            { pHandle_Animal.Multiplier = 2; }

            //Heal
            pHeal.Aktiv = true;
            if (character.Background == "Ogier")
            { pHeal.Multiplier = 1; }
            else
            { pHeal.Multiplier = 2; }

            //Hide
            pHide.Aktiv = !(character.Background == "Ogier");
            if (character.Algai_d_Siswai || character.Wanderer || character.Woodsman)
            { pHide.Multiplier = 1; }
            else
            { pHide.Multiplier = 2; }

            //Innuendo
            pInnuendo.Aktiv = !(character.Background == "Ogier"); ;
            if (character.Noble || character.Wanderer)
            { pInnuendo.Multiplier = 1; }
            else
            { pInnuendo.Multiplier = 2; }

            //Intimidate
            pIntimidate.Aktiv =  !(character.Background == "Ogier");
            pInnuendo.Multiplier = 1;

            //Intuit Direction
            pIntuit_Direction.Aktiv = true;
            if (character.Algai_d_Siswai || character.Wanderer || character.Woodsman || (character.Background == "Ogier"))
            { pIntuit_Direction.Multiplier = 1; }
            else
            { pIntuit_Direction.Multiplier = 2; }

            //Jump
            pJump.Aktiv = true;
            if (character.Algai_d_Siswai || character.Armsman || character.Wanderer || character.Woodsman)
            { pJump.Multiplier = 1; }
            else
            { pJump.Multiplier = 2; }

            //Knowledge
            if (character.Initiate || character.Noble || character.Wanderer || character.Wilder || character.Woodsman || (character.Background == "Ogier"))
            {
                pKnowledge_Arcana.Multiplier = 1;
                pKnowledge_Arch.Multiplier = 1;
                pKnowledge_Blight.Multiplier = 1;
                pKnowledge_Geography.Multiplier = 1;
                pKnowledge_History.Multiplier = 1;
                pKnowledge_Legends.Multiplier = 1;
                pKnowledge_Local.Multiplier = 1;
                pKnowledge_Nature.Multiplier = 1;
                pKnowledge_Nobility.Multiplier = 1;
            }
            else
            {
                pKnowledge_Arcana.Multiplier = 2;
                pKnowledge_Arch.Multiplier = 2;
                pKnowledge_Blight.Multiplier = 2;
                pKnowledge_Geography.Multiplier = 2;
                pKnowledge_History.Multiplier = 2;
                pKnowledge_Legends.Multiplier = 2;
                pKnowledge_Local.Multiplier = 2;
                pKnowledge_Nature.Multiplier = 2;
                pKnowledge_Nobility.Multiplier = 2;
            }

            //Listen
            pListen.Aktiv = true;
            if (character.Algai_d_Siswai || character.Noble || character.Wanderer || character.Woodsman || (character.Background == "Ogier"))
            { pListen.Multiplier = 1; }
            else
            { pListen.Multiplier = 2; }

            //Move Silently
            pMove_Silently.Aktiv =  !(character.Background == "Ogier");
            if (character.Algai_d_Siswai || character.Wanderer || character.Woodsman)
            { pMove_Silently.Multiplier = 1; }
            else
            { pMove_Silently.Multiplier = 2; }

            //Open Lock
            pOpen_Lock.Aktiv =  !(character.Background == "Ogier");
            if (character.Wanderer)
            { pOpen_Lock.Multiplier = 1; }
            else
            { pOpen_Lock.Multiplier = 2; }

            //Pick Pocket
            pPick_Pocket.Aktiv =  !(character.Background == "Ogier");
            if (character.Wanderer)
            { pPick_Pocket.Multiplier = 1; }
            else
            { pPick_Pocket.Multiplier = 2; }

            //Read Lips
            pRead_Lips.Aktiv = character.Wanderer;
            if (character.Wanderer)
            { pRead_Lips.Multiplier = 1; }
            else
            { pRead_Lips.Multiplier = 2; }

            //Ride
            pRide.Aktiv =  !(character.Background == "Ogier");
            if (character.Armsman || character.Noble || character.Woodsman)
            { pRide.Multiplier = 1; }
            else
            { pRide.Multiplier = 2; }

            //Search
            pSearch.Aktiv = true;
            if (character.Wanderer || character.Woodsman || (character.Background == "Ogier"))
            { pSearch.Multiplier = 1; }
            else
            { pSearch.Multiplier = 2; }

            //Sense Motive
            pSense_Motive.Aktiv = true;
            if (character.Initiate || character.Noble || character.Wanderer || character.Wilder)
            { pSense_Motive.Multiplier = 1; }
            else
            { pSense_Motive.Multiplier = 2; }

            //Spot
            pSpot.Aktiv = true;
            if (character.Wanderer || character.Woodsman || (character.Background == "Ogier"))
            { pSpot.Multiplier = 1; }
            else
            { pSpot.Multiplier = 2; }
            
            //Swim
            pSwim.Aktiv = true;
            if (character.Armsman || character.Wanderer || character.Woodsman)
            { pSwim.Multiplier = 1; }
            else
            { pSwim.Multiplier = 2; }

            //Tumble
            pTumble.Aktiv = true;
            if (character.Wanderer)
            { pTumble.Multiplier = 1; }
            else
            { pTumble.Multiplier = 2; }

            //Use Rope
            pUse_Rope.Aktiv = true;
            if (character.Wanderer || character.Woodsman)
            { pUse_Rope.Multiplier = 1; }
            else
            { pUse_Rope.Multiplier = 2; }

            //Weavesight
            pWeavesight.Aktiv = character.Initiate || character.Wilder;
            if (character.Initiate || character.Wilder)
            { pWeavesight.Multiplier = 1; }
            else
            { pWeavesight.Multiplier = 2; }

            //Wilderness Lore
            pWilderness_Lore.Aktiv = true;
            if (character.Algai_d_Siswai || character.Woodsman)
            { pWilderness_Lore.Multiplier = 1; }
            else
            { pWilderness_Lore.Multiplier = 2; }

            foreach(Points p in SkillsPanel.Controls)
            {
                bool isbackgroundskill = false;
                foreach (CheckBox c in tpBackgroundSkills.Controls)
                {
                    if (character.Background == "Ogier")
                    {
                        if (c.Text == p.SkillName && c.Checked)
                        {
                            if (p.GivenPoints < 4)
                            { p.GivenPoints = 4; }
                            p.OgierBackgroundSkill = true;
                            isbackgroundskill = true;
                            break;
                        }
                    }
                    else
                    {
                        if (c.Text == p.SkillName && c.Checked)
                        {
                            if (p.GivenPoints < 4)
                            { p.GivenPoints = 4; }
                            p.BackgroundSkill = true;
                            isbackgroundskill = true;
                            break;
                        }
                    }
                }
                if (!isbackgroundskill)
                { p.BackgroundSkill = false; }
            }

            if (character.Background == "Ogier")
            {
                if (pDecipher_Script.GivenPoints < 2) {pDecipher_Script.GivenPoints = 2; }
                pDecipher_Script.OgierBackgroundSkill = true;
                if (pHandle_Animal.GivenPoints < 2) { pHandle_Animal.GivenPoints = 2; }
                pHandle_Animal.OgierBackgroundSkill = true;
                if (pIntuit_Direction.GivenPoints < 2) { pIntuit_Direction.GivenPoints = 2; }
                pIntuit_Direction.OgierBackgroundSkill = true;
                if (pListen.GivenPoints < 2) { pListen.GivenPoints = 2; }
                pListen.OgierBackgroundSkill = true;
                if (pSpot.GivenPoints < 2) { pSpot.GivenPoints = 2; }
                pSpot.OgierBackgroundSkill = true;
                if (pWilderness_Lore.GivenPoints < 2) { pWilderness_Lore.GivenPoints = 2; }
                pWilderness_Lore.OgierBackgroundSkill = true;
            }

            CalculatePoints();
        }
    }
}