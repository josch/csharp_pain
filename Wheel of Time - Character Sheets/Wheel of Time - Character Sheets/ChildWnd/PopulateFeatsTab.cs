using System;
using System.Windows.Forms;
using System.Drawing;

namespace Wheel_of_Time___Character_Sheets
{
    partial class ChildWnd
    {
        private void PopulateBackgroundFeatsTab()
        {
            string[] Aiel = { "Blooded", "Bullheaded", "Disciplined", "Stealthy", "Survivor" };
            string[] Atha_an_Miere = { "Disciplined", "Mercantile Background", "Sea Legs", "Silver Palm", "Smooth Talk" };
            string[] Borderlander = { "Blooded", "Saddleback", "Shadowspawn Hunter", "Stealthy", "Strong Soul" };
            string[] Cairhienin = { "Cosmopolitan", "Militia", "Silver Palm", "Smooth Talk", "Street Smart" };
            string[] Domani = { "Cosmopolitan", "Militia", "Seductive", "Street Smart" };
            string[] Ebou_Dari = { "Cosmopolitan", "Duelist", "Gambler", "Mercantile Background", "Street Smart" };
            string[] Illianer = { "Cosmopolitan", "Mecantile Background", "Militia", "Silver Palm", "Street Smart" };
            string[] Midlander = { "Bullheaded", "Luck of Heroes", "Militia", "Strong Soul" };
            string[] Ogier = { "Artist", "Education", "Smooth Talk", "Bullheaded", "Luck of Heroes" };
            string[] Tairen = { "Gambler", "Mercantile Background", "Militia", "Saddleback" };
            string[] Tar_Valoner = { "Cosmopolitan", "Education", "Militia", "Smooth Talk" };
            string[] Taraboner = { "Artist", "Living History", "Mercantile Background" };

            switch (character.Background)
            {
                case "Aiel":
                    for (int i = 0; i < Aiel.Length; i++)
                    {
                        CheckBox cb = new CheckBox();
                        cb.Text = Aiel[i];
                        cb.Location = new Point(6, 6 + i * cb.Height);
                        cb.AutoSize = true;
                        cb.FlatStyle = FlatStyle.Flat;
                        cb.AutoCheck = false;
                        cb.Click += new EventHandler(FeatsClick);
                        tpBackground_Feats.Controls.Add(cb);
                    }
                    break;
                case "Atha'an Miere":
                    for (int i = 0; i < Atha_an_Miere.Length; i++)
                    {
                        CheckBox cb = new CheckBox();
                        cb.Text = Atha_an_Miere[i];
                        cb.Location = new Point(6, 6 + i * cb.Height);
                        cb.AutoSize = true;
                        cb.FlatStyle = FlatStyle.Flat;
                        cb.AutoCheck = false;
                        cb.Click += new EventHandler(FeatsClick);
                        tpBackground_Feats.Controls.Add(cb);
                    }
                    break;
                case "Borderlander":
                    for (int i = 0; i < Borderlander.Length; i++)
                    {
                        CheckBox cb = new CheckBox();
                        cb.Text = Borderlander[i];
                        cb.Location = new Point(6, 6 + i * cb.Height);
                        cb.AutoSize = true;
                        cb.FlatStyle = FlatStyle.Flat;
                        cb.AutoCheck = false;
                        cb.Click += new EventHandler(FeatsClick);
                        tpBackground_Feats.Controls.Add(cb);
                    }
                    break;
                case "Cairhienin":
                    for (int i = 0; i < Cairhienin.Length; i++)
                    {
                        CheckBox cb = new CheckBox();
                        cb.Text = Cairhienin[i];
                        cb.Location = new Point(6, 6 + i * cb.Height);
                        cb.AutoSize = true;
                        cb.FlatStyle = FlatStyle.Flat;
                        cb.AutoCheck = false;
                        cb.Click += new EventHandler(FeatsClick);
                        tpBackground_Feats.Controls.Add(cb);
                    }
                    break;
                case "Domani":
                    for (int i = 0; i < Domani.Length; i++)
                    {
                        CheckBox cb = new CheckBox();
                        cb.Text = Domani[i];
                        cb.Location = new Point(6, 6 + i * cb.Height);
                        cb.AutoSize = true;
                        cb.FlatStyle = FlatStyle.Flat;
                        cb.AutoCheck = false;
                        cb.Click += new EventHandler(FeatsClick);
                        tpBackground_Feats.Controls.Add(cb);
                    }
                    break;
                case "Ebou Dari":
                    for (int i = 0; i < Ebou_Dari.Length; i++)
                    {
                        CheckBox cb = new CheckBox();
                        cb.Text = Ebou_Dari[i];
                        cb.Location = new Point(6, 6 + i * cb.Height);
                        cb.AutoSize = true;
                        cb.FlatStyle = FlatStyle.Flat;
                        cb.AutoCheck = false;
                        cb.Click += new EventHandler(FeatsClick);
                        tpBackground_Feats.Controls.Add(cb);
                    }
                    break;
                case "Illianer":
                    for (int i = 0; i < Illianer.Length; i++)
                    {
                        CheckBox cb = new CheckBox();
                        cb.Text = Illianer[i];
                        cb.Location = new Point(6, 6 + i * cb.Height);
                        cb.AutoSize = true;
                        cb.FlatStyle = FlatStyle.Flat;
                        cb.AutoCheck = false;
                        cb.Click += new EventHandler(FeatsClick);
                        tpBackground_Feats.Controls.Add(cb);
                    }
                    break;
                case "Midlander":
                    for (int i = 0; i < Midlander.Length; i++)
                    {
                        CheckBox cb = new CheckBox();
                        cb.Text = Midlander[i];
                        cb.Location = new Point(6, 6 + i * cb.Height);
                        cb.AutoSize = true;
                        cb.FlatStyle = FlatStyle.Flat;
                        cb.AutoCheck = false;
                        cb.Click += new EventHandler(FeatsClick);
                        tpBackground_Feats.Controls.Add(cb);
                    }
                    break;
                case "Ogier":
                    for (int i = 0; i < Ogier.Length; i++)
                    {
                        CheckBox cb = new CheckBox();
                        cb.Text = Ogier[i];
                        cb.Location = new Point(6, 6 + i * cb.Height);
                        cb.AutoSize = true;
                        cb.FlatStyle = FlatStyle.Flat;
                        cb.AutoCheck = false;
                        cb.Click += new EventHandler(FeatsClick);
                        tpBackground_Feats.Controls.Add(cb);
                    }
                    break;
                case "Tairen":
                    for (int i = 0; i < Tairen.Length; i++)
                    {
                        CheckBox cb = new CheckBox();
                        cb.Text = Tairen[i];
                        cb.Location = new Point(6, 6 + i * cb.Height);
                        cb.AutoSize = true;
                        cb.FlatStyle = FlatStyle.Flat;
                        cb.AutoCheck = false;
                        cb.Click += new EventHandler(FeatsClick);
                        tpBackground_Feats.Controls.Add(cb);
                    }
                    break;
                case "Tar Valoner":
                    for (int i = 0; i < Tar_Valoner.Length; i++)
                    {
                        CheckBox cb = new CheckBox();
                        cb.Text = Tar_Valoner[i];
                        cb.Location = new Point(6, 6 + i * cb.Height);
                        cb.AutoSize = true;
                        cb.FlatStyle = FlatStyle.Flat;
                        cb.AutoCheck = false;
                        cb.Click += new EventHandler(FeatsClick);
                        tpBackground_Feats.Controls.Add(cb);
                    }
                    break;
                case "Taraboner":
                    for (int i = 0; i < Taraboner.Length; i++)
                    {
                        CheckBox cb = new CheckBox();
                        cb.Text = Taraboner[i];
                        cb.Location = new Point(6, 6 + i * cb.Height);
                        cb.AutoSize = true;
                        cb.FlatStyle = FlatStyle.Flat;
                        cb.AutoCheck = false;
                        cb.Click += new EventHandler(FeatsClick);
                        tpBackground_Feats.Controls.Add(cb);
                    }
                    break;
            }
        }
    }
}