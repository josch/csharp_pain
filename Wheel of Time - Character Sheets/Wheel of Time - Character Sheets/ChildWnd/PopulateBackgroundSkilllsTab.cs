using System;
using System.Windows.Forms;
using System.Drawing;

namespace Wheel_of_Time___Character_Sheets
{
    partial class ChildWnd
    {
        private void PopulateBackgroundSkillsTab()
        {
            string[] Aiel = { "Hide", "Move Silently", "Spot", "Wilderness Lore" };
            string[] Atha_an_Miere = { "Intuit Direction", "Profession (Sailor)", "Swim", "Use Rope" };
            string[] Borderlander = { "Knowledge (Blight)", "Listen", "Move Silently", "Ride" };
            string[] Cairhienin = { "Diplomacy", "Forgery", "Innuendo", "Sense Motive" };
            string[] Domani = { "Bluff", "Diplomacy", "Gather Information", "Perform" };
            string[] Ebou_Dari = { "Appraise", "Balance", "Hide", "Open Lock" };
            string[] Illianer = { "Intimidate", "Search" };
            string[] Midlander = { "Handle Animal", "Heal", "Ride", "Wilderness Lore" };
            string[] Ogier = { "Craft" };
            string[] Tairen = { "Ride", "Search", "Sense Motive" };
            string[] Tar_Valoner = { "Concentration" };
            string[] Taraboner = { "Appraise", "Hide", "Move Silently", "Open Lock" };

            switch (character.Background)
            {
                case "Aiel":
                    for (int i = 0; i < Aiel.Length; i++)
                    {
                        CheckBox cb = new CheckBox();
                        cb.Text = Aiel[i];
                        cb.Location = new Point(6, 6 + i * cb.Height);
                        cb.AutoSize = true;
                        cb.FlatStyle = FlatStyle.Flat;
                        cb.AutoCheck = false;
                        cb.Click += new EventHandler(BackgroundSkillClick);
                        tpBackgroundSkills.Controls.Add(cb);
                    }
                    break;
                case "Atha'an Miere":
                    for (int i = 0; i < Atha_an_Miere.Length; i++)
                    {
                        CheckBox cb = new CheckBox();
                        cb.Text = Atha_an_Miere[i];
                        cb.Location = new Point(6, 6 + i * cb.Height);
                        cb.AutoSize = true;
                        cb.FlatStyle = FlatStyle.Flat;
                        cb.AutoCheck = false;
                        cb.Click += new EventHandler(BackgroundSkillClick);
                        tpBackgroundSkills.Controls.Add(cb);
                    }
                    break;
                case "Borderlander":
                    for (int i = 0; i < Borderlander.Length; i++)
                    {
                        CheckBox cb = new CheckBox();
                        cb.Text = Borderlander[i];
                        cb.Location = new Point(6, 6 + i * cb.Height);
                        cb.AutoSize = true;
                        cb.FlatStyle = FlatStyle.Flat;
                        cb.AutoCheck = false;
                        cb.Click += new EventHandler(BackgroundSkillClick);
                        tpBackgroundSkills.Controls.Add(cb);
                    }
                    break;
                case "Cairhienin":
                    for (int i = 0; i < Cairhienin.Length; i++)
                    {
                        CheckBox cb = new CheckBox();
                        cb.Text = Cairhienin[i];
                        cb.Location = new Point(6, 6 + i * cb.Height);
                        cb.AutoSize = true;
                        cb.FlatStyle = FlatStyle.Flat;
                        cb.AutoCheck = false;
                        cb.Click += new EventHandler(BackgroundSkillClick);
                        tpBackgroundSkills.Controls.Add(cb);
                    }
                    break;
                case "Domani":
                    for (int i = 0; i < Domani.Length; i++)
                    {
                        CheckBox cb = new CheckBox();
                        cb.Text = Domani[i];
                        cb.Location = new Point(6, 6 + i * cb.Height);
                        cb.AutoSize = true;
                        cb.FlatStyle = FlatStyle.Flat;
                        cb.AutoCheck = false;
                        cb.Click += new EventHandler(BackgroundSkillClick);
                        tpBackgroundSkills.Controls.Add(cb);
                    }
                    break;
                case "Ebou Dari":
                    for (int i = 0; i < Ebou_Dari.Length; i++)
                    {
                        CheckBox cb = new CheckBox();
                        cb.Text = Ebou_Dari[i];
                        cb.Location = new Point(6, 6 + i * cb.Height);
                        cb.AutoSize = true;
                        cb.FlatStyle = FlatStyle.Flat;
                        cb.AutoCheck = false;
                        cb.Click += new EventHandler(BackgroundSkillClick);
                        tpBackgroundSkills.Controls.Add(cb);
                    }
                    break;
                case "Illianer":
                    ListAllCraftSkills();
                    ListAllKnowledgeSkills();
                    for (int i = 0; i < Illianer.Length; i++)
                    {
                        CheckBox cb = new CheckBox();
                        cb.Text = Illianer[i];
                        cb.Location = new Point(6, 6 + tpBackgroundSkills.Controls.Count * cb.Height);
                        cb.AutoSize = true;
                        cb.FlatStyle = FlatStyle.Flat;
                        cb.AutoCheck = false;
                        cb.Click += new EventHandler(BackgroundSkillClick);
                        tpBackgroundSkills.Controls.Add(cb);
                    }
                    break;
                case "Midlander":
                    for (int i = 0; i < Midlander.Length; i++)
                    {
                        CheckBox cb = new CheckBox();
                        cb.Text = Midlander[i];
                        cb.Location = new Point(6, 6 + i * cb.Height);
                        cb.AutoSize = true;
                        cb.FlatStyle = FlatStyle.Flat;
                        cb.AutoCheck = false;
                        cb.Click += new EventHandler(BackgroundSkillClick);
                        tpBackgroundSkills.Controls.Add(cb);
                    }
                    break;
                case "Ogier":
                    ListAllKnowledgeSkills();
                    for (int i = 0; i < Ogier.Length; i++)
                    {
                        CheckBox cb = new CheckBox();
                        cb.Text = Ogier[i];
                        cb.Location = new Point(6, 6 + tpBackgroundSkills.Controls.Count * cb.Height);
                        cb.AutoSize = true;
                        cb.FlatStyle = FlatStyle.Flat;
                        cb.AutoCheck = false;
                        cb.Click += new EventHandler(BackgroundSkillClick);
                        tpBackgroundSkills.Controls.Add(cb);
                    }
                    break;
                case "Tairen":
                    ListAllProfessionSkills();
                    for (int i = 0; i < Tairen.Length; i++)
                    {
                        CheckBox cb = new CheckBox();
                        cb.Text = Tairen[i];
                        cb.Location = new Point(6, 6 + tpBackgroundSkills.Controls.Count * cb.Height);
                        cb.AutoSize = true;
                        cb.FlatStyle = FlatStyle.Flat;
                        cb.AutoCheck = false;
                        cb.Click += new EventHandler(BackgroundSkillClick);
                        tpBackgroundSkills.Controls.Add(cb);
                    }
                    break;
                case "Tar Valoner":
                    ListAllCraftSkills();
                    ListAllKnowledgeSkills();
                    ListAllProfessionSkills();
                    for (int i = 0; i < Tar_Valoner.Length; i++)
                    {
                        CheckBox cb = new CheckBox();
                        cb.Text = Tar_Valoner[i];
                        cb.Location = new Point(6, 6 + tpBackgroundSkills.Controls.Count * cb.Height);
                        cb.AutoSize = true;
                        cb.FlatStyle = FlatStyle.Flat;
                        cb.AutoCheck = false;
                        cb.Click += new EventHandler(BackgroundSkillClick);
                        tpBackgroundSkills.Controls.Add(cb);
                    }
                    break;
                case "Taraboner":
                    for (int i = 0; i < Taraboner.Length; i++)
                    {
                        CheckBox cb = new CheckBox();
                        cb.Text = Taraboner[i];
                        cb.Location = new Point(6, 6 + i * cb.Height);
                        cb.AutoSize = true;
                        cb.FlatStyle = FlatStyle.Flat;
                        cb.AutoCheck = false;
                        cb.Click += new EventHandler(BackgroundSkillClick);
                        tpBackgroundSkills.Controls.Add(cb);
                    }
                    break;
            }
        }

        void BackgroundSkillClick(object sender, EventArgs e)
        {
            CheckBox Sender = (CheckBox)sender;
            if (!Sender.Checked)
            {
                int numberofchecked = 0;
                foreach (CheckBox cb in tpBackgroundSkills.Controls)
                {
                    if (cb.Checked)
                    {
                        numberofchecked++;
                    }
                }
                if (numberofchecked < 2)
                {
                    Sender.Checked = true;
                }
            }
            else
            {
                Sender.Checked = false;
            }
        }

        private void ListAllCraftSkills()
        {
            foreach (Points p in SkillsPanel.Controls)
            {
                if (p.SkillName.Length >= 5)
                {
                    if (p.SkillName.Substring(0, 5) == "Craft")
                    {
                        CheckBox cb = new CheckBox();
                        cb.Text = p.SkillName;
                        cb.Location = new Point(6, 6 + tpBackgroundSkills.Controls.Count * cb.Height);
                        cb.AutoSize = true;
                        cb.FlatStyle = FlatStyle.Flat;
                        cb.AutoCheck = false;
                        cb.Click += new EventHandler(BackgroundSkillClick);
                        tpBackgroundSkills.Controls.Add(cb);
                    }
                }
            }
        }

        private void ListAllKnowledgeSkills()
        {
            foreach (Points p in SkillsPanel.Controls)
            {
                if (p.SkillName.Length >= 9)
                {
                    if (p.SkillName.Substring(0, 9) == "Knowledge")
                    {
                        CheckBox cb = new CheckBox();
                        cb.Text = p.SkillName;
                        cb.Location = new Point(6, 6 + tpBackgroundSkills.Controls.Count * cb.Height);
                        cb.AutoSize = true;
                        cb.FlatStyle = FlatStyle.Flat;
                        cb.AutoCheck = false;
                        cb.Click += new EventHandler(BackgroundSkillClick);
                        tpBackgroundSkills.Controls.Add(cb);
                    }
                }
            }
        }

        private void ListAllProfessionSkills()
        {
            foreach (Points p in SkillsPanel.Controls)
            {
                if (p.SkillName.Length >= 10)
                {
                    if (p.SkillName.Substring(0, 10) == "Profession")
                    {
                        CheckBox cb = new CheckBox();
                        cb.Text = p.SkillName;
                        cb.Location = new Point(6, 6 + tpBackgroundSkills.Controls.Count * cb.Height);
                        cb.AutoSize = true;
                        cb.FlatStyle = FlatStyle.Flat;
                        cb.AutoCheck = false;
                        cb.Click += new EventHandler(BackgroundSkillClick);
                        tpBackgroundSkills.Controls.Add(cb);
                    }
                }
            }
        }
    }
}

