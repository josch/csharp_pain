using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Printing;

namespace Wheel_of_Time___Character_Sheets
{
    public partial class Main : Form
    {
        private int childFormNumber = 0;

        public Main(string[] args)
        {
            InitializeComponent();

            if (args.Length == 0)
            {
                ChildWnd childwnd = new ChildWnd("");
                childwnd.MdiParent = this;
                childwnd.Text = "Dokument " + childFormNumber++;
                childwnd.Show();
            }
            else
            {
                ChildWnd childwnd = new ChildWnd(args[0]);
                childwnd.MdiParent = this;
                childwnd.Text = args[0];
                childwnd.Show();
            }
        }

        private void ShowNewForm(object sender, EventArgs e)
        {
            ChildWnd childwnd = new ChildWnd("");
            childwnd.MdiParent = this;
            childwnd.Text = "Dokument " + childFormNumber++;
            childwnd.Show();
        }

        private void OpenFile(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            openFileDialog.Filter = "Wheel of Time Character Files (*.wot)|*.wot";
            openFileDialog.ShowDialog(this);

            ChildWnd childwnd = new ChildWnd(openFileDialog.FileName);
            childwnd.MdiParent = this;
            childwnd.Text = openFileDialog.FileName;
            childwnd.Show();
        }

        private void SaveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            saveFileDialog.Filter = "Text Files (*.txt)|*.txt";
            saveFileDialog.ShowDialog(this);

            string FileName = saveFileDialog.FileName;
            // TODO: Add code here to save the current contents of the form to a file.
        }

        private void ExitToolsStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void CascadeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.Cascade);
        }

        private void TileVerticleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileVertical);
        }

        private void TileHorizontalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileHorizontal);
        }

        private void ArrangeIconsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.ArrangeIcons);
        }

        private void CloseAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Form childForm in MdiChildren)
            {
                childForm.Close();
            }
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AboutBox1 ab = new AboutBox1();
            ab.ShowDialog();
        }
    }
}