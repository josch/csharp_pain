using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Wheel_of_Time___Character_Sheets
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            Splash splash = new Splash();
            splash.Show();
            Application.EnableVisualStyles();
            Application.Run(new Main(args));
        }
    }
}