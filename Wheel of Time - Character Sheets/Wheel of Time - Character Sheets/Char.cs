using System;
using System.Collections;
using System.Text;
using System.ComponentModel;
using System.Drawing;

namespace Wheel_of_Time___Character_Sheets
{
    [DefaultPropertyAttribute("Name")]
    public class Char
    {
        private string name;
        private string spieler;
        internal class c_background
        { internal static string[] background = { "Aiel", "Atha'an Miere", "Borderlander",
        "Cairhienin", "Domani", "Ebou Dari", "Illianer", "Midlander", "Ogier", "Tairen", "Tar Valoner", "Taraboner"};}
        private string background;
        internal class c_sex
        { internal static string[] sex = { "m�nnlich", "weiblich" }; }
        private string sex;
        internal class c_level
        { internal static string[] level = { "Bauer", "Springer", "L�ufer", "Turm", "K�nigin", "K�nig" }; }
        private string level;
        private string chronik;

        private bool algai_d_siswai;
        private bool armsman;
        private bool initiate;
        private bool noble;
        private bool wanderer;
        private bool wilder;
        private bool woodsman;

        private Color haare;
        private int gr��e;
        private int alter;
        private int gewicht;
        private Color augen;
        private Color haut;

        private int st�rke;
        private int geschicklichkeit;
        private int konstitution;
        private int intelligenz;
        private int weisheit;
        private int charisma;
        private int maxskillpoints;

        private string skillspath;

        [CategoryAttribute("Misc"),
                DescriptionAttribute("Der Pfad zu den zus�tzlichen Skills")]
        public string SkillsPath
        {
            get { return skillspath; }
            set { skillspath = value; }
        }
        [CategoryAttribute("Misc"),
        DescriptionAttribute("Maximale Skill Punkte")]
        public int MaxSkillPoints
        {
            get { return maxskillpoints; }
            set { maxskillpoints = value; }
        }

        [CategoryAttribute("Attribute"),
        DescriptionAttribute("Das Charisma")]
        public int Charisma
        {
            get { return charisma; }
            set { charisma = value; }
        }

        [CategoryAttribute("Attribute"),
        DescriptionAttribute("Die Weisheit")]
        public int Weisheit
        {
            get { return weisheit; }
            set { weisheit = value; }
        }

        [CategoryAttribute("Attribute"),
        DescriptionAttribute("Die Intelligenz")]
        public int Intelligenz
        {
            get { return intelligenz; }
            set { intelligenz = value; }
        }

        [CategoryAttribute("Attribute"),
        DescriptionAttribute("Die Konstitution")]
        public int Konstitution
        {
            get { return konstitution; }
            set { konstitution = value; }
        }

        [CategoryAttribute("Attribute"),
        DescriptionAttribute("Die Gesundheit")]
        public int Geschicklichkeit
        {
            get { return geschicklichkeit; }
            set { geschicklichkeit = value; }
        }

        [CategoryAttribute("Attribute"),
        DescriptionAttribute("Die St�rke")]
        public int St�rke
        {
            get { return st�rke; }
            set { st�rke = value; }
        }

        [CategoryAttribute("Allgemein"),
        DescriptionAttribute("Die Hautfarbe")]
        public Color Haut
        {
            get { return haut; }
            set { haut = value; }
        }

        [CategoryAttribute("Allgemein"),
        DescriptionAttribute("Die Augenfarbe")]
        public Color Augen
        {
            get { return augen; }
            set { augen = value; }
        }

        [CategoryAttribute("Allgemein"),
        DescriptionAttribute("Das Gewicht in Kilogramm")]
        public int Gewicht
        {
            get { return gewicht; }
            set { gewicht = value; }
        }

        [CategoryAttribute("Allgemein"),
        DescriptionAttribute("Das Alter")]
        public int Alter
        {
            get { return alter; }
            set { alter = value; }
        }

        [CategoryAttribute("Allgemein"),
        DescriptionAttribute("Die K�rpergr��e in Centimetern")]
        public int Gr��e
        {
            get { return gr��e; }
            set { gr��e = value; }
        }

        [CategoryAttribute("Allgemein"),
        DescriptionAttribute("Der Name")]
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        [CategoryAttribute("Allgemein"),
        DescriptionAttribute("Der Spielername")]
        public string Spieler
        {
            get { return spieler; }
            set { spieler = value; }
        }

        [CategoryAttribute("Allgemein"),
        DescriptionAttribute("Die Haarfarbe")]
        public Color Haare
        {
            get { return haare; }
            set { haare = value; }
        }

        [CategoryAttribute("Allgemein"),
        DescriptionAttribute("Die Chronik")]
        public string Chronik
        {
            get { return chronik; }
            set { chronik = value; }
        }

        [CategoryAttribute("Klasse"),
        DescriptionAttribute("Woodsman")]
        public bool Woodsman
        {
            get { return woodsman; }
            set { woodsman = value; }
        }

        [CategoryAttribute("Klasse"),
        DescriptionAttribute("Wilder")]
        public bool Wilder
        {
            get { return wilder; }
            set { wilder = value; }
        }

        [CategoryAttribute("Klasse"),
        DescriptionAttribute("Wanderer")]
        public bool Wanderer
        {
            get { return wanderer; }
            set { wanderer = value; }
        }

        [CategoryAttribute("Klasse"),
        DescriptionAttribute("Noble")]
        public bool Noble
        {
            get { return noble; }
            set { noble = value; }
        }

        [CategoryAttribute("Klasse"),
        DescriptionAttribute("Initiate")]
        public bool Initiate
        {
            get { return initiate; }
            set { initiate = value; }
        }

        [CategoryAttribute("Klasse"),
        DescriptionAttribute("Armsman")]
        public bool Armsman
        {
            get { return armsman; }
            set { armsman = value; }
        }

        [CategoryAttribute("Klasse"),
        DescriptionAttribute("Algai'd'Siswai")]
        public bool Algai_d_Siswai
        {
            get { return algai_d_siswai; }
            set { algai_d_siswai = value; }
        }

        [Browsable(true),
        TypeConverter(typeof(BackgroundRuleConverter)),
        CategoryAttribute("Allgemein"),
        DescriptionAttribute("Der Background")]
        public string Background
        {
            get { return background; }
            set { background = value; }
        }

        [Browsable(true),
        TypeConverter(typeof(SexRuleConverter)),
        CategoryAttribute("Allgemein"),
        DescriptionAttribute("Das Geschlecht")]
        public string Geschlecht
        {
            get { return sex; }
            set { sex = value; }
        }

        [Browsable(true),
        TypeConverter(typeof(LevelRuleConverter)),
        CategoryAttribute("Allgemein"),
        DescriptionAttribute("Der Level")]
        public string Level
        {
            get { return level; }
            set { level = value; }
        }

        public class SexRuleConverter : StringConverter
        {
            public override TypeConverter.StandardValuesCollection GetStandardValues(ITypeDescriptorContext context)
            {
                return new StandardValuesCollection(c_sex.sex);
            }

            public override bool GetStandardValuesSupported(ITypeDescriptorContext context)
            {
                return true;
            }

            public override bool GetStandardValuesExclusive(ITypeDescriptorContext context)
            {
                return true;
            }
        }
        
        public class BackgroundRuleConverter : StringConverter
        {
            public override TypeConverter.StandardValuesCollection GetStandardValues(ITypeDescriptorContext context)
            {
                return new StandardValuesCollection(c_background.background);
            }

            public override bool GetStandardValuesSupported(ITypeDescriptorContext context)
            {
                return true;
            }

            public override bool GetStandardValuesExclusive(ITypeDescriptorContext context)
            {
                return true;
            }
        }

        public class LevelRuleConverter : StringConverter
        {
            public override TypeConverter.StandardValuesCollection GetStandardValues(ITypeDescriptorContext context)
            {
                return new StandardValuesCollection(c_level.level);
            }

            public override bool GetStandardValuesSupported(ITypeDescriptorContext context)
            {
                return true;
            }

            public override bool GetStandardValuesExclusive(ITypeDescriptorContext context)
            {
                return true;
            }
        }
    }
}
