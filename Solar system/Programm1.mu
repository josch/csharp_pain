funcdisc:=proc(func) begin
f:=eval(func);  //Zu analysierende Funktion
fd1:=diff(f, x); //erste Ableitung
fd2:=diff(fd1, x); //zweite Ableitug

x0:=solve(f, x); //Nullstellen
x0anz:=card(x0); //Anzahl der Nullstellen

xe:=solve(fd1, x); //Extremstellen
xeanz:=card(xe); //Anzahl der Extremstellen

xw:=solve(fd2, x); //Wendestellen
xwanz:=card(xw); //Anzahl Wendestellen

print(Unquoted, "Folgende Funktion wird nun analysiert: f(x) = ".expr2text(f));

//plotfunc2d(f);

//Nullstellen
print(Unquoted, "Nullstellen liegen bei:");
for i from 1 to x0anz do

	//Funktionswerte auf und nahe bei jeder Nullstelle
	testdavor:=float(subs(f, x=(x0[i]-0.0000001)));
	test:=float(subs(f, x=x0[i]));
	testdanach:=float(subs(f, x=(x0[i]+0.0000001)));

	//Bestimmung der Art des Extremums
	if testdavor < test and testdanach < test then
		art:="Ber�hrung: Maximum";
	end_if;
	if testdavor > test and testdanach > test then
		art:="Ber�hrung: Miniumum";
	end_if;
	if testdavor > test and testdanach < test then
		art:="Von plus zu minus";
	end_if;
	if testdavor < test and testdanach > test then
		art:="Von minus zu plus";
	end_if;

	print(Unquoted, "x0".i." = ".expr2text(x0[i])." Art: ".art);
end_for;

//Erste Ableitung
print(Unquoted, "Die erste Ableitung ist gleich:");
print(Unquoted, "f'(x) = ".expr2text(fd1));

//Extrempunkte
print(Unquoted, "Extrempunkte liegen bei:");
for i from 1 to xeanz do
	
	//Funktionswerte auf und nahe bei jedem Extremum
	testdavor:=float(subs(f, x=(xe[i]-0.0000001)));
	test:=float(subs(f, x=xe[i]));
	testdanach:=float(subs(f, x=(xe[i]+0.0000001)));

	//Bestimmung der Art des Extremums
	if testdavor < test and testdanach < test then
		art:="Maximum";
	end_if;
	if testdavor > test and testdanach > test then
		art:="Miniumum";
	end_if;
	if testdavor > test and testdanach < test then
		art:="Sattelpunkt";
	end_if;
	if testdavor < test and testdanach > test then
		art:="Sattelpunkt";
	end_if;

	print(Unquoted, "E".i."(".expr2text(xe[i])." | ".expr2text(subs(f, x=xe[i])).") Art: ".art);
end_for;

//Zweite Ableitung
print(Unquoted, "Die zweite Ableitung ist gleich:");
print(Unquoted, "f''(x) = ".expr2text(fd2));

//Wendepunkte
print(Unquoted, "Wendepunkte liegen bei:");
for i from 1 to xwanz do
	print(Unquoted, "W".i."(".expr2text(xw[i])." | ".expr2text(subs(f, x=xw[i])).")");
end_for;
end_proc:
