
using System;
using System.Drawing;
using System.Windows.Forms;
using Microsoft.DirectX;
using Microsoft.DirectX.Direct3D;
using Direct3D=Microsoft.DirectX.Direct3D;

namespace Solar_System
{
	public class MainForm : System.Windows.Forms.Form
	{
		
		private Device device = null;
		private VertexBuffer vertexBuffer = null;
		private PresentParameters presentParams = new PresentParameters();
		private Mesh mesh = null;
		private Direct3D.Material[] material;
		private Texture texture;
		private bool pause = false;
		
		public bool InitializeGraphics()
		{
			try
			{
				presentParams.Windowed=true;
				presentParams.SwapEffect = SwapEffect.Discard;
				presentParams.EnableAutoDepthStencil = true;
				presentParams.AutoDepthStencilFormat = DepthFormat.D16;
				device = new Device(0, DeviceType.Hardware, this, CreateFlags.SoftwareVertexProcessing, presentParams);
				device.DeviceReset += new System.EventHandler(this.OnResetDevice);
				this.OnCreateDevice(device, null);
				this.OnResetDevice(device, null);
				pause = false;
				return true;
			}
			catch (DirectXException)
			{
				return false;
			}
		}
		
		public void OnCreateDevice(object sender, EventArgs e)
		{
			Device dev = (Device)sender;
			vertexBuffer = new VertexBuffer(typeof(CustomVertex.PositionNormal), 100, dev, Usage.WriteOnly, CustomVertex.PositionNormal.Format, Pool.Default);
			vertexBuffer.Created += new System.EventHandler(this.OnCreateVertexBuffer);
			this.OnCreateVertexBuffer(vertexBuffer, null);
		}
		
		public void OnResetDevice(object sender, EventArgs e)
		{
			Device dev = (Device)sender;
			device.RenderState.CullMode = Cull.None;
			device.RenderState.ZBufferEnable = true;
			device.RenderState.Lighting = true;
			
			mesh = Mesh.Teapot(device);
		}
		
		public void OnCreateVertexBuffer(object sender, EventArgs e)
		{
			VertexBuffer vb = (VertexBuffer)sender;
			CustomVertex.PositionNormal[] verts = (CustomVertex.PositionNormal[])vb.Lock(0,0);
			for (int i = 0; i < 50; i++)
			{
				float theta = (float)(2 * Math.PI * i) / 49;
				verts[2 * i].SetPosition(new Vector3((float)Math.Sin(theta), -1, (float)Math.Cos(theta)));
				verts[2 * i].SetNormal(new Vector3((float)Math.Sin(theta), 0, (float)Math.Cos(theta)));
				verts[2 * i + 1].SetPosition(new Vector3((float)Math.Sin(theta), 1, (float)Math.Cos(theta)));
				verts[2 * i + 1].SetNormal(new Vector3((float)Math.Sin(theta), 0, (float)Math.Cos(theta)));
			}
			vb.Unlock();
		}
		
		private void SetupMatrices()
		{
			device.Transform.World = Matrix.RotationAxis(new Vector3((float)Math.Cos(Environment.TickCount / 250.0f),1,(float)Math.Sin(Environment.TickCount / 250.0f)), Environment.TickCount / 3000.0f );
			device.Transform.View = Matrix.LookAtLH( new Vector3( 0.0f, 3.0f,-5.0f ), new Vector3( 0.0f, 0.0f, 0.0f ), new Vector3( 0.0f, 1.0f, 0.0f ) );
			device.Transform.Projection = Matrix.PerspectiveFovLH( (float)Math.PI / 4.0f, this.Width/this.Height, 1.0f, 100.0f );
		}
		
		private void SetupLights()
		{
			System.Drawing.Color col = System.Drawing.Color.White;
			Direct3D.Material mtrl = new Direct3D.Material();
			mtrl.Diffuse = col;
			mtrl.Ambient = col;
			device.Material = mtrl;
    
			device.Lights[0].Type = LightType.Directional;
			device.Lights[0].Diffuse = System.Drawing.Color.DarkTurquoise;
			device.Lights[0].Direction = new Vector3((float)Math.Cos(Environment.TickCount / 250.0f), 1.0f, (float)Math.Sin(Environment.TickCount / 250.0f));

			device.Lights[0].Commit();
			device.Lights[0].Enabled = true;

			device.RenderState.Ambient = System.Drawing.Color.FromArgb(0x202020);
		}
		
		private void Render()
		{
			if (pause)
				return;
			
			device.Clear(ClearFlags.Target | ClearFlags.ZBuffer, System.Drawing.Color.Blue, 1.0f, 0);
			device.BeginScene();
			SetupLights();
			SetupMatrices();
			
			device.SetStreamSource(0, vertexBuffer, 0);
			device.VertexFormat = CustomVertex.PositionNormal.Format;
			device.DrawPrimitives(PrimitiveType.TriangleStrip, 0, (4*25)-2);
			device.EndScene();
			device.Present();
		}
		
		protected override void OnPaint(System.Windows.Forms.PaintEventArgs e)
		{
			this.Render();
		}
		protected override void OnKeyPress(System.Windows.Forms.KeyPressEventArgs e)
		{
			if ((int)(byte)e.KeyChar == (int)System.Windows.Forms.Keys.Escape)
				this.Close();
		}
		protected override void OnResize(System.EventArgs e)
		{
			pause = ((this.WindowState == FormWindowState.Minimized) || !this.Visible);
		}
		
		public MainForm()
		{
			InitializeComponent();
		}
		
		[STAThread]
		public static void Main(string[] args)
		{
			using (MainForm frm = new MainForm())
			{
				if (!frm.InitializeGraphics())
				{
					return;
				}
				frm.Show();
				
				while(frm.Created)
				{
					frm.Render();
					Application.DoEvents();
				}
			}
		}
		private void InitializeComponent() {
			// 
			// MainForm
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(752, 533);
			this.Name = "MainForm";
			this.Text = "MainForm";
		}
	}
}
