@echo off
echo Compilation script for the J3D Solar System Simulator
if "%1"=="" goto error

echo Please be patient, compiling will take some time...
javac -d %1 Joystick.java
javac -d %1 sss3d\SolarSystemSimulator.java

echo creating directory %1\images
mkdir %1\images
echo copying image files to %1\images
copy images %1\images
echo creating directory %1\xml
mkdir %1\xml
echo copying xml files to %1\xml
copy xml %1\xml
echo copying default.xml to %1
copy default.xml %1
echo copying joystick.dll to %1
copy joystick.dll %1
echo copying SpaceShuttle.lwo to %1
copy SpaceShuttle.lwo %1
echo copying spaceshuttle.lws to %1
copy spaceshuttle.lws %1
goto end

:error
echo 
echo Usage: compile.bat output_path
echo               to recompile the project J3D Solar System Simulator
echo               and write the all class files to output_path
echo               give an *existing* output_path with *write permission*
goto end

:end
