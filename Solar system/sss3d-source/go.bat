@echo off
echo Starting script for the J3D Solar System Simulator
if "%1"=="" goto error

echo THIS IS A BATCH SCRIPT TO START
echo *** J3D Solar System Simulator ***
echo Diploma thesis of Bernhard Hari and Marcel Portner, 2000
echo Biel school of engineering and architecture, Biel, Switzerland

cd %1
javaw -Xmx128m sss3d.SolarSystemSimulator

goto end

:error
echo 
echo Usage: go.bat execute_path
echo               to run the project J3D Solar System Simulator 
echo               give the right execute path where you had compiled it
goto end

:end

