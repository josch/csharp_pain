/*
  File: Planets.java

  University of Applied Science Berne,HTA-Biel/Bienne,
  Computer Science Department.

  Diploma thesis J3D Solar System Simulator
  Originally written by Marcel Portner & Bernhard Hari (c) 2000
  
  CVS - Information :
  
  $Header: /var/cvsreps/projects/c450/2000/sss3d/source_diploma/sss3d/utils/astronomy/Planets.java,v 1.4 2000/12/13 13:42:49 portm Exp $
  $Author: portm $
  $Date: 2000/12/13 13:42:49 $
  $State: Exp $
  
*/
package sss3d.utils.astronomy;

import sss3d.calculations.constants.AstronomicalConstants;
import sss3d.calculations.*;
import sss3d.utils.SSS3dConstants;

import javax.vecmath.Matrix3d;

/**
 * This class calculate the position of all planets.
 *
 * @author Marcel Portner & Bernhard Hari
 * @version $Revision: 1.4 $
 */
public class Planets {

   private static final double P = 1.3970;        // Praezession in einem Jahrhundert [deg]
   
   private PlanetPert pert;
   private SunPert s_pert;
 
   /**
    * Constructor
    */
   public Planets() {
      // referenz zur Stoerungs Klasse
      pert  = new PlanetPert();
      s_pert = new SunPert();
   }

   /**
    * Calculate the place- and velocity vector for the kepler ways.
    *
    * @param planet  the current planet number for identification.
    * @param time  time in julianish century.
    *
    * @return an array with the place- and velocity vector.<br>
    *         Vec3D[0] = heliocentrically place vector in [AE], ecliptic and
    *                    equinox of the date.<br>
    *         Vec3D[1] = heliocentrically velocity vector in [AE / d], ecliptic and
    *                    equinox of the date.
    */
   public Vec3D[] state(int planet, double time) {

      double a, e, m0, o, i, w, n, t0;
 
       // Bahnelemente; Ekliptik und Aequinoktium J2000
      switch(planet) {
        case SSS3dConstants.SUN:
          Vec3D[] vec = new Vec3D[2];
          vec[0] = new Vec3D();    // Nullvektor
          vec[1] = new Vec3D();    // Nullvektor
          return vec;
        case SSS3dConstants.MERCURY:
          a =  0.387099; e = 0.205634; m0 = 174.7947; n = 149472.6738;
          o =  48.331;   i = 7.0048;   w  =  77.4552; t0 = 0.0;
          break;
        case SSS3dConstants.VENUS:
          a =  0.723332; e = 0.006773; m0 =  50.4071; n  = 58517.8149;
          o =  76.680;   i = 3.3946;   w  = 131.5718; t0 = 0.0;
          break;
        case SSS3dConstants.EARTH:
          a =  1.000000; e = 0.016709; m0 = 357.5256; n  = 35999.3720;
          o = 174.876;   i = 0.0000;   w  = 102.9400; t0 = 0.0;
          break;
        case SSS3dConstants.MARS:
          a =  1.523692; e = 0.093405; m0 =  19.3879; n  = 19140.3023;
          o =  49.557;   i = 1.8496;   w  = 336.0590; t0 = 0.0;
          break;
        case SSS3dConstants.JUPITER:
          a =  5.204267; e = 0.048775; m0 =  18.8185; n  =  3033.6272;
          o = 100.4908;  i = 1.3046;   w  =  15.5576; t0 = 0.0;
          break;
        case SSS3dConstants.SATURN:
          a =  9.582018; e = 0.055723; m0 = 320.3477; n  =  1213.8664;
          o = 113.6427;  i = 2.4852;   w  =  89.6567; t0 = 0.0;
          break;
        case SSS3dConstants.URANUS:
          a = 19.229412; e = 0.044406; m0 = 142.9559; n  =   426.9282;
          o =  73.9893;  i = 0.7726;   w  = 170.5310; t0 = 0.0;
          break;
        case SSS3dConstants.NEPTUNE:
          a = 30.103658; e = 0.011214; m0 = 267.7649; n  =   217.9599;
          o = 131.7942;  i = 1.7680;   w  =  37.4435; t0 = 0.0;
          break;
        case SSS3dConstants.PLUTO:
          a = 39.264230; e = 0.244672; m0 =  15.0233; n  =   146.3183;
          o = 110.2867;  i = 17.1514;  w  = 224.0499; t0 = 0.0;
          break;
        default:
          a = 0.0;       e = 0.0;      m0 = 0.0;      n  = 0.0;
          o = 0.0;       i = 0.0;      w  = 0.0;      t0 = 0.0;
      }
      
      Kepler kepler = new Kepler();

      // Zustandsvektor bezogen auf Ekliptik und Aequinoktium des Datums
      Vec3D[] vec = kepler.ellip(AstronomicalConstants.GM_SUN,
                                 MoreMath.RAD * (m0 + n * (time - t0)),
                                 a,
                                 e);                           // bezogen auf Bahnebene
 
      Matrix3d pqr = kepler.gaussVec(MoreMath.RAD * (o + P * time),
                                     MoreMath.RAD * i,
                                     MoreMath.RAD * (w - o));  // Transform. -> Ekliptik
 
      vec[0].mul(pqr);                 // Ekliptikale
      vec[1].mul(pqr);                 // Koordinaten

      return vec;
   }

   /**
    * Calculate the place vector of the kepler elements.
    *
    * @param planet  the current planet number for identification.
    * @param time  time in julianish century.
    *
    * @return heliocentrically place vector in [AE], ecliptic and
    *         equinox of the date
    */
   public Vec3D kepPosition(int planet, double time) {
      Vec3D[] vec = state(planet, time);
      return vec[0];
   }

   /**
    * Calculate the velocity vector of the kepler elements.
    *
    * @param planet  the current planet number for identification.
    * @param time  time in julianish century.
    *
    * @return heliocentrically velocity vector in [AE / d], ecliptic and
    *         equinox of the date
    */
   public Vec3D kepVelocity(int planet, double time) {
      Vec3D[] vec = state(planet, time);
      return vec[1];
   }

   /**
    * Calculate the position of mercury with analytic series expansion.
    *
    * @param time  time in julianish century.
    *
    * @return heliocentrically place vector in [AE], ecliptic and
    *         equinox of the date
    */
   public Vec3D mercuryPos(double time) {

      double m1, m2, m3, m5, m6;         // Mittlere Anomalien
      double dl, dr, db;                 // Korrekturen in Laenge ["],
                                         // Entfernung [AE] und Breite ["]
      double l, b, r;                    // Ekliptikale Koordinaten
 
      // Mittlere Anomalien der Planeten in [rad]
      m1 = MoreMath.PI2 * MoreMath.frac(0.4855407 + 415.2014314 * time);
      m2 = MoreMath.PI2 * MoreMath.frac(0.1394222 + 162.5490444 * time);
      m3 = MoreMath.PI2 * MoreMath.frac(0.9937861 +  99.9978139 * time);
      m5 = MoreMath.PI2 * MoreMath.frac(0.0558417 +   8.4298417 * time);
      m6 = MoreMath.PI2 * MoreMath.frac(0.8823333 +   3.3943333 * time);
 
      // Keplerbewegung und Stoerungen durch Venus
      pert.init(time, m1, -1, 9, m2, -5, 0);
 
      pert.term( 1, 0,0, 259.74,84547.39,-78342.34, 0.01,11683.22,21203.79);
      pert.term( 1, 0,1,   2.30,    5.04,    -7.52, 0.02,  138.55,  -71.01);
      pert.term( 1, 0,2,   0.01,   -0.01,     0.01, 0.01,   -0.19,   -0.54);
      pert.term( 2, 0,0,-549.71,10394.44, -7955.45, 0.00, 2390.29, 4306.79);
      pert.term( 2, 0,1,  -4.77,    8.97,    -1.53, 0.00,   28.49,  -14.18);
      pert.term( 2, 0,2,   0.00,    0.00,     0.00, 0.00,   -0.04,   -0.11);
      pert.term( 3, 0,0,-234.04, 1748.74, -1212.86, 0.00,  535.41,  984.33);
      pert.term( 3, 0,1,  -2.03,    3.48,    -0.35, 0.00,    6.56,   -2.91);
      pert.term( 4, 0,0, -77.64,  332.63,  -219.23, 0.00,  124.40,  237.03);
      pert.term( 4, 0,1,  -0.70,    1.10,    -0.08, 0.00,    1.59,   -0.59);
      pert.term( 5, 0,0, -23.59,   67.28,   -43.54, 0.00,   29.44,   58.77);
      pert.term( 5, 0,1,  -0.23,    0.32,    -0.02, 0.00,    0.39,   -0.11);
      pert.term( 6, 0,0,  -6.86,   14.06,    -9.18, 0.00,    7.03,   14.84);
      pert.term( 6, 0,1,  -0.07,    0.09,    -0.01, 0.00,    0.10,   -0.02);
      pert.term( 7, 0,0,  -1.94,    2.98,    -2.02, 0.00,    1.69,    3.80);
      pert.term( 8, 0,0,  -0.54,    0.63,    -0.46, 0.00,    0.41,    0.98);
      pert.term( 9, 0,0,  -0.15,    0.13,    -0.11, 0.00,    0.10,    0.25);
      pert.term(-1,-2,0,  -0.17,   -0.06,    -0.05, 0.14,   -0.06,   -0.07);
      pert.term( 0,-1,0,   0.24,   -0.16,    -0.11,-0.16,    0.04,   -0.01);
      pert.term( 0,-2,0,  -0.68,   -0.25,    -0.26, 0.73,   -0.16,   -0.18);
      pert.term( 0,-5,0,   0.37,    0.08,     0.06,-0.28,    0.13,    0.12);
      pert.term( 1,-1,0,   0.58,   -0.41,     0.26, 0.36,    0.01,   -0.01);
      pert.term( 1,-2,0,  -3.51,   -1.23,     0.23,-0.63,   -0.05,   -0.06);
      pert.term( 1,-3,0,   0.08,    0.53,    -0.11, 0.04,    0.02,   -0.09);
      pert.term( 1,-5,0,   1.44,    0.31,     0.30,-1.39,    0.34,    0.29);
      pert.term( 2,-1,0,   0.15,   -0.11,     0.09, 0.12,    0.02,   -0.04);
      pert.term( 2,-2,0,  -1.99,   -0.68,     0.65,-1.91,   -0.20,    0.03);
      pert.term( 2,-3,0,  -0.34,   -1.28,     0.97,-0.26,    0.03,    0.03);
      pert.term( 2,-4,0,  -0.33,    0.35,    -0.13,-0.13,   -0.01,    0.00);
      pert.term( 2,-5,0,   7.19,    1.56,    -0.05, 0.12,    0.06,    0.05);
      pert.term( 3,-2,0,  -0.52,   -0.18,     0.13,-0.39,   -0.16,    0.03);
      pert.term( 3,-3,0,  -0.11,   -0.42,     0.36,-0.10,   -0.05,   -0.05);
      pert.term( 3,-4,0,  -0.19,    0.22,    -0.23,-0.20,   -0.01,    0.02);
      pert.term( 3,-5,0,   2.77,    0.49,    -0.45, 2.56,    0.40,   -0.12);
      pert.term( 4,-5,0,   0.67,    0.12,    -0.09, 0.47,    0.24,   -0.08);
      pert.term( 5,-5,0,   0.18,    0.03,    -0.02, 0.12,    0.09,   -0.03);
 
      dl = pert.dl();  dr = pert.dr();  db = pert.db();
 
      // Stoerungen durch die Erde
      pert.init(time, m1, 0 ,2, m3, -4, -1);
 
      pert.term( 0,-4,0,  -0.11,   -0.07,    -0.08, 0.11,   -0.02,   -0.04);
      pert.term( 1,-1,0,   0.10,   -0.20,     0.15, 0.07,    0.00,    0.00);
      pert.term( 1,-2,0,  -0.35,    0.28,    -0.13,-0.17,   -0.01,    0.00);
      pert.term( 1,-4,0,  -0.67,   -0.45,     0.00, 0.01,   -0.01,   -0.01);
      pert.term( 2,-2,0,  -0.20,    0.16,    -0.16,-0.20,   -0.01,    0.02);
      pert.term( 2,-3,0,   0.13,   -0.02,     0.02, 0.14,    0.01,    0.00);
      pert.term( 2,-4,0,  -0.33,   -0.18,     0.17,-0.31,   -0.04,    0.00);
 
      dl += pert.dl();  dr += pert.dr();  db += pert.db();
 
      // Stoerungen durch Jupiter
      pert.init(time, m1, -1, 3, m5, -3, -1);
 
      pert.term(-1,-1,0,  -0.08,    0.16,     0.15, 0.08,   -0.04,    0.01);
      pert.term(-1,-2,0,   0.10,   -0.06,    -0.07,-0.12,    0.07,   -0.01);
      pert.term( 0,-1,0,  -0.31,    0.48,    -0.02, 0.13,   -0.03,   -0.02);
      pert.term( 0,-2,0,   0.42,   -0.26,    -0.38,-0.50,    0.20,   -0.03);
      pert.term( 1,-1,0,  -0.70,    0.01,    -0.02,-0.63,    0.00,    0.03);
      pert.term( 1,-2,0,   2.61,   -1.97,     1.74, 2.32,    0.01,    0.01);
      pert.term( 1,-3,0,   0.32,   -0.15,     0.13, 0.28,    0.00,    0.00);
      pert.term( 2,-1,0,  -0.18,    0.01,     0.00,-0.13,   -0.03,    0.03);
      pert.term( 2,-2,0,   0.75,   -0.56,     0.45, 0.60,    0.08,   -0.17);
      pert.term( 3,-2,0,   0.20,   -0.15,     0.10, 0.14,    0.04,   -0.08);
 
      dl += pert.dl();  dr += pert.dr();  db += pert.db();
 
 
      // Stoerungen durch Saturn
      pert.init(time, m1, 1, 1, m6, -2, -2);
 
      pert.term( 1,-2,0,  -0.19,    0.33,     0.00, 0.00,    0.00,    0.00);
 
      dl += pert.dl();  dr += pert.dr();  db += pert.db();
 
 
      // Ekliptikale Koordinaten ([rad],[AE])
      dl += 2.8 + 3.2 * time;
 
      l = MoreMath.PI2 * MoreMath.frac(0.2151379 + m1 / MoreMath.PI2 +
                                       ((5601.7 + 1.1 * time) * time + dl) / 1296.0e3);
      r = 0.3952829 + 0.0000016 * time + dr * 1.0e-6;
      b = (-2522.15 + (-30.18 + 0.04 * time) * time  +  db ) / MoreMath.ARCS;
 
      return new Vec3D(new Polar(l, b, r));  // Ort
   }

   /**
    * Calculate the position of venus with analytic series expansion.
    *
    * @param time  time in julianish century.
    *
    * @return heliocentrically place vector in [AE], ecliptic and
    *         equinox of the date
    */
   public Vec3D venusPos(double time) {

      double  m1,m2,m3,m4,m5,m6;       // Mittlere Anomalien
      double  dl, dr, db;              // Korrekturen in Laenge ["],
                                       // Entfernung [AE] und Breite ["]
      double  l,b,r;                   // Ekliptikale Koordinaten
    
      // Mittlere Anomalien der Planeten in [rad]
      m1 = MoreMath.PI2 * MoreMath.frac(0.4861431 + 415.2018375 * time);
      m2 = MoreMath.PI2 * MoreMath.frac(0.1400197 + 162.5494552 * time);
      m3 = MoreMath.PI2 * MoreMath.frac(0.9944153 +  99.9982208 * time);
      m4 = MoreMath.PI2 * MoreMath.frac(0.0556297 +  53.1674631 * time);
      m5 = MoreMath.PI2 * MoreMath.frac(0.0567028 +   8.4305083 * time);
      m6 = MoreMath.PI2 * MoreMath.frac(0.8830539 +   3.3947206 * time);
    
      // Stoerungen durch Merkur
      pert.init(time, m2, 1, 5, m1, -2, -1);
    
      pert.term(1,-1,0,   0.00,   0.00,    0.06, -0.09,   0.01,   0.00);
      pert.term(2,-1,0,   0.25,  -0.09,   -0.09, -0.27,   0.00,   0.00);
      pert.term(4,-2,0,  -0.07,  -0.08,   -0.14,  0.14,  -0.01,  -0.01);
      pert.term(5,-2,0,  -0.35,   0.08,    0.02,  0.09,   0.00,   0.00);
    
      dl = pert.dl();  dr = pert.dr();  db = pert.db();
    
      // Keplerbewegung und Stoerungen durch die Erde
      pert.init(time, m2, 1, 8, m3, -8, 0);
    
      pert.term(1, 0,0,   2.37,2793.23,-4899.07,  0.11,9995.27,7027.22);
      pert.term(1, 0,1,   0.10, -19.65,   34.40,  0.22,  64.95, -86.10);
      pert.term(1, 0,2,   0.06,   0.04,   -0.07,  0.11,  -0.55,  -0.07);
      pert.term(2, 0,0,-170.42,  73.13,  -16.59,  0.00,  67.71,  47.56);
      pert.term(2, 0,1,   0.93,   2.91,    0.23,  0.00,  -0.03,  -0.92);
      pert.term(3, 0,0,  -2.31,   0.90,   -0.08,  0.00,   0.04,   2.09);
      pert.term(1,-1,0,  -2.38,  -4.27,    3.27, -1.82,   0.00,   0.00);
      pert.term(1,-2,0,   0.09,   0.00,   -0.08,  0.05,  -0.02,  -0.25);
      pert.term(2,-2,0,  -9.57,  -5.93,    8.57,-13.83,  -0.01,  -0.01);
      pert.term(2,-3,0,  -2.47,  -2.40,    0.83, -0.95,   0.16,   0.24);
      pert.term(3,-2,0,  -0.09,  -0.05,    0.08, -0.13,  -0.28,   0.12);
      pert.term(3,-3,0,   7.12,   0.32,   -0.62, 13.76,  -0.07,   0.01);
      pert.term(3,-4,0,  -0.65,  -0.17,    0.18, -0.73,   0.10,   0.05);
      pert.term(3,-5,0,  -1.08,  -0.95,   -0.17,  0.22,  -0.03,  -0.03);
      pert.term(4,-3,0,   0.06,   0.00,   -0.01,  0.08,   0.14,  -0.18);
      pert.term(4,-4,0,   0.93,  -0.46,    1.06,  2.13,  -0.01,   0.01);
      pert.term(4,-5,0,  -1.53,   0.38,   -0.64, -2.54,   0.27,   0.00);
      pert.term(4,-6,0,  -0.17,  -0.05,    0.03, -0.11,   0.02,   0.00);
      pert.term(5,-5,0,   0.18,  -0.28,    0.71,  0.47,  -0.02,   0.04);
      pert.term(5,-6,0,   0.15,  -0.14,    0.30,  0.31,  -0.04,   0.03);
      pert.term(5,-7,0,  -0.08,   0.02,   -0.03, -0.11,   0.01,   0.00);
      pert.term(5,-8,0,  -0.23,   0.00,    0.01, -0.04,   0.00,   0.00);
      pert.term(6,-6,0,   0.01,  -0.14,    0.39,  0.04,   0.00,  -0.01);
      pert.term(6,-7,0,   0.02,  -0.05,    0.12,  0.04,  -0.01,   0.01);
      pert.term(6,-8,0,   0.10,  -0.10,    0.19,  0.19,  -0.02,   0.02);
      pert.term(7,-7,0,  -0.03,  -0.06,    0.18, -0.08,   0.00,   0.00);
      pert.term(8,-8,0,  -0.03,  -0.02,    0.06, -0.08,   0.00,   0.00);
    
      dl += pert.dl();  dr += pert.dr();  db += pert.db();
    
    
      // Stoerungen durch Mars
      pert.init(time, m2, 1, 2, m4, -3, -2);
    
      pert.term(1,-3,0,  -0.65,   1.02,   -0.04, -0.02,  -0.02,   0.00);
      pert.term(2,-2,0,  -0.05,   0.04,   -0.09, -0.10,   0.00,   0.00);
      pert.term(2,-3,0,  -0.50,   0.45,   -0.79, -0.89,   0.01,   0.03);
    
      dl += pert.dl();  dr += pert.dr();  db += pert.db();
    
    
      // Stoerungen durch Jupiter
      pert.init(time, m2, 0, 3, m5, -3, -1);
    
      pert.term(0,-1,0,  -0.05,   1.56,    0.16,  0.04,  -0.08,  -0.04);
      pert.term(1,-1,0,  -2.62,   1.40,   -2.35, -4.40,   0.02,   0.03);
      pert.term(1,-2,0,  -0.47,  -0.08,    0.12, -0.76,   0.04,  -0.18);
      pert.term(2,-2,0,  -0.73,  -0.51,    1.27, -1.82,  -0.01,   0.01);
      pert.term(2,-3,0,  -0.14,  -0.10,    0.25, -0.34,   0.00,   0.00);
      pert.term(3,-3,0,  -0.01,   0.04,   -0.11, -0.02,   0.00,   0.00);
    
      dl += pert.dl(); dr += pert.dr();  db += pert.db();
    
    
      // Stoerungen durch Saturn
      pert.init(time, m2, 0, 1, m6, -1, -1);
    
      pert.term(0,-1,0,   0.00,   0.21,    0.00,  0.00,   0.00,  -0.01);
      pert.term(1,-1,0,  -0.11,  -0.14,    0.24, -0.20,   0.01,   0.00);
    
      dl += pert.dl(); dr += pert.dr();  db += pert.db();
    
    
      // Ekliptikale Koordinaten ([rad],[AE])
      dl += 2.74 * StrictMath.sin(MoreMath.PI2 * (0.0764 + 0.4174 * time)) +
            0.27 * StrictMath.sin(MoreMath.PI2 * (0.9201 + 0.3307 * time));
      dl += 1.9 + 1.8 * time;
    
      l = MoreMath.PI2 * MoreMath.frac(0.3654783 + m2 / MoreMath.PI2 +
                                       ((5071.2 + 1.1 * time) * time + dl) / 1296.0e3);
      r = 0.7233482 - 0.0000002 * time +  dr * 1.0e-6;
      b = (-67.70 + (0.04 + 0.01 * time) * time + db ) / MoreMath.ARCS;
    
      return new Vec3D(new Polar(l, b, r));  // Ort
   }

   /**
    * Calculate the position of earth with analytic series expansion.
    *
    * @param time  time in julianish century.
    *
    * @return heliocentrically place vector in [AE], ecliptic and
    *         equinox of the date
    */
   public Vec3D earthPos(double time) {

      double  m2,m3,m4,m5,m6;          // Mittlere Anomalien
      double  d, a, u;                 // Mittlere Argumente der Mondbahn
      double  dl, dr, db;              // Korrekturen in Laenge ["],
                                       // Entfernung [AE] und Breite ["]
      double  l,b,r;                   // Ekliptikale Koordinaten
      Vec3D rvec;                  // Ort
    
      // Mittlere Anomalien der Planeten und mittlere Argumente der Mondbahn in [rad]
      m2 = MoreMath.PI2 * MoreMath.frac( 0.1387306 + 162.5485917 * time);
      m3 = MoreMath.PI2 * MoreMath.frac( 0.9931266 +  99.9973604 * time);
      m4 = MoreMath.PI2 * MoreMath.frac( 0.0543250 +  53.1666028 * time); 
      m5 = MoreMath.PI2 * MoreMath.frac( 0.0551750 +   8.4293972 * time);
      m6 = MoreMath.PI2 * MoreMath.frac( 0.8816500 +   3.3938722 * time); 
    
      d  = MoreMath.PI2 * MoreMath.frac( 0.8274 + 1236.8531 * time);
      a  = MoreMath.PI2 * MoreMath.frac( 0.3749 + 1325.5524 * time);      
      u  = MoreMath.PI2 * MoreMath.frac( 0.2591 + 1342.2278 * time);
    
      // Keplerbewegung und Stoerungen durch Venus
      s_pert.init(time, m3, 0, 7, m2, -6, 0);
    
      s_pert.term( 1, 0,0,-0.22,6892.76,-16707.37, -0.54, 0.00, 0.00);
      s_pert.term( 1, 0,1,-0.06, -17.35,    42.04, -0.15, 0.00, 0.00);
      s_pert.term( 1, 0,2,-0.01,  -0.05,     0.13, -0.02, 0.00, 0.00);
      s_pert.term( 2, 0,0, 0.00,  71.98,  -139.57,  0.00, 0.00, 0.00);
      s_pert.term( 2, 0,1, 0.00,  -0.36,     0.70,  0.00, 0.00, 0.00);
      s_pert.term( 3, 0,0, 0.00,   1.04,    -1.75,  0.00, 0.00, 0.00);
      s_pert.term( 0,-1,0, 0.03,  -0.07,    -0.16, -0.07, 0.02,-0.02);
      s_pert.term( 1,-1,0, 2.35,  -4.23,    -4.75, -2.64, 0.00, 0.00);
      s_pert.term( 1,-2,0,-0.10,   0.06,     0.12,  0.20, 0.02, 0.00);
      s_pert.term( 2,-1,0,-0.06,  -0.03,     0.20, -0.01, 0.01,-0.09);
      s_pert.term( 2,-2,0,-4.70,   2.90,     8.28, 13.42, 0.01,-0.01);
      s_pert.term( 3,-2,0, 1.80,  -1.74,    -1.44, -1.57, 0.04,-0.06);
      s_pert.term( 3,-3,0,-0.67,   0.03,     0.11,  2.43, 0.01, 0.00);
      s_pert.term( 4,-2,0, 0.03,  -0.03,     0.10,  0.09, 0.01,-0.01);
      s_pert.term( 4,-3,0, 1.51,  -0.40,    -0.88, -3.36, 0.18,-0.10);
      s_pert.term( 4,-4,0,-0.19,  -0.09,    -0.38,  0.77, 0.00, 0.00);
      s_pert.term( 5,-3,0, 0.76,  -0.68,     0.30,  0.37, 0.01, 0.00);
      s_pert.term( 5,-4,0,-0.14,  -0.04,    -0.11,  0.43,-0.03, 0.00);
      s_pert.term( 5,-5,0,-0.05,  -0.07,    -0.31,  0.21, 0.00, 0.00);
      s_pert.term( 6,-4,0, 0.15,  -0.04,    -0.06, -0.21, 0.01, 0.00);
      s_pert.term( 6,-5,0,-0.03,  -0.03,    -0.09,  0.09,-0.01, 0.00);
      s_pert.term( 6,-6,0, 0.00,  -0.04,    -0.18,  0.02, 0.00, 0.00);
      s_pert.term( 7,-5,0,-0.12,  -0.03,    -0.08,  0.31,-0.02,-0.01);
    
      dl = s_pert.dl(); dr = s_pert.dr();  db = s_pert.db();
    
    
      // Stoerungen durch Mars
      s_pert.init(time, m3, 1, 5, m4, -8, -1);
    
      s_pert.term( 1,-1,0,-0.22,   0.17,    -0.21, -0.27, 0.00, 0.00);
      s_pert.term( 1,-2,0,-1.66,   0.62,     0.16,  0.28, 0.00, 0.00);
      s_pert.term( 2,-2,0, 1.96,   0.57,    -1.32,  4.55, 0.00, 0.01);
      s_pert.term( 2,-3,0, 0.40,   0.15,    -0.17,  0.46, 0.00, 0.00);
      s_pert.term( 2,-4,0, 0.53,   0.26,     0.09, -0.22, 0.00, 0.00);
      s_pert.term( 3,-3,0, 0.05,   0.12,    -0.35,  0.15, 0.00, 0.00);
      s_pert.term( 3,-4,0,-0.13,  -0.48,     1.06, -0.29, 0.01, 0.00);
      s_pert.term( 3,-5,0,-0.04,  -0.20,     0.20, -0.04, 0.00, 0.00);
      s_pert.term( 4,-4,0, 0.00,  -0.03,     0.10,  0.04, 0.00, 0.00);
      s_pert.term( 4,-5,0, 0.05,  -0.07,     0.20,  0.14, 0.00, 0.00);
      s_pert.term( 4,-6,0,-0.10,   0.11,    -0.23, -0.22, 0.00, 0.00);
      s_pert.term( 5,-7,0,-0.05,   0.00,     0.01, -0.14, 0.00, 0.00);
      s_pert.term( 5,-8,0, 0.05,   0.01,    -0.02,  0.10, 0.00, 0.00);
    
      dl += s_pert.dl(); dr += s_pert.dr();  db += s_pert.db();
    
      
      // Stoerungen durch Jupiter 
      s_pert.init(time, m3, -1, 3, m5, -4, -1);
    
      s_pert.term(-1,-1,0, 0.01,   0.07,     0.18, -0.02, 0.00,-0.02);
      s_pert.term( 0,-1,0,-0.31,   2.58,     0.52,  0.34, 0.02, 0.00);
      s_pert.term( 1,-1,0,-7.21,  -0.06,     0.13,-16.27, 0.00,-0.02);
      s_pert.term( 1,-2,0,-0.54,  -1.52,     3.09, -1.12, 0.01,-0.17);
      s_pert.term( 1,-3,0,-0.03,  -0.21,     0.38, -0.06, 0.00,-0.02);
      s_pert.term( 2,-1,0,-0.16,   0.05,    -0.18, -0.31, 0.01, 0.00);
      s_pert.term( 2,-2,0, 0.14,  -2.73,     9.23,  0.48, 0.00, 0.00);
      s_pert.term( 2,-3,0, 0.07,  -0.55,     1.83,  0.25, 0.01, 0.00);
      s_pert.term( 2,-4,0, 0.02,  -0.08,     0.25,  0.06, 0.00, 0.00);
      s_pert.term( 3,-2,0, 0.01,  -0.07,     0.16,  0.04, 0.00, 0.00);
      s_pert.term( 3,-3,0,-0.16,  -0.03,     0.08, -0.64, 0.00, 0.00);
      s_pert.term( 3,-4,0,-0.04,  -0.01,     0.03, -0.17, 0.00, 0.00);
    
      dl += s_pert.dl(); dr += s_pert.dr();  db += s_pert.db();
    
      
      // Stoerungen durch Saturn 
      s_pert.init(time, m3, 0, 2, m6, -2, -1);
    
      s_pert.term( 0,-1,0, 0.00,   0.32,     0.01,  0.00, 0.00, 0.00);
      s_pert.term( 1,-1,0,-0.08,  -0.41,     0.97, -0.18, 0.00,-0.01);
      s_pert.term( 1,-2,0, 0.04,   0.10,    -0.23,  0.10, 0.00, 0.00);
      s_pert.term( 2,-2,0, 0.04,   0.10,    -0.35,  0.13, 0.00, 0.00);
    
      dl += s_pert.dl(); dr += s_pert.dr();  db += s_pert.db();
    
    
      // Abstand des Schwerpunkts des Erde-Mond-Systems vom Erdmittelpunkt
      dl += 6.45 * StrictMath.sin(d) -
            0.42 * StrictMath.sin(d - a) +
            0.18 * StrictMath.sin(d + a) +
            0.17 * StrictMath.sin(d - m3) -
            0.06 * StrictMath.sin(d + m3);
    
      dr += 30.76 * StrictMath.cos(d) -
             3.06 * StrictMath.cos(d - a) +
             0.85 * StrictMath.cos(d + a) -
             0.58 * StrictMath.cos(d + m3) +
             0.57 * StrictMath.cos(d - m3);
    
      db += 0.576 * StrictMath.sin(u);
    
    
      // Langperiodische Stoerungen
      dl += 6.40 * StrictMath.sin(MoreMath.PI2 * (0.6983 + 0.0561 * time)) +
            1.87 * StrictMath.sin(MoreMath.PI2 * (0.5764 + 0.4174 * time)) +
            0.27 * StrictMath.sin(MoreMath.PI2 * (0.4189 + 0.3306 * time)) +
            0.20 * StrictMath.sin(MoreMath.PI2 * (0.3581 + 2.4814 * time));
    
    
      // Ekliptikale Koordinaten ([rad],[AE])
      l = MoreMath.PI2 * MoreMath.frac(0.7859453 + m3 / MoreMath.PI2 +
                                       ((6191.2 + 1.1 * time) * time + dl ) / 1296.0e3);
      r = 1.0001398 - 0.0000007 * time + dr * 1.0e-6;
      b = db / MoreMath.ARCS;
      
      rvec = new Vec3D(new Polar(l, b, r));
      // die Koordinate wurde (die Erde als Zentrum!)
      // f�r die Sonnen berechnet. Darum muss beim Vektor die
      // Vorzeichen ge�ndert werden!
      rvec.negate();
    
      return rvec;   // Ort
   }

   /**
    * Calculate the position of mars with analytic series expansion.
    *
    * @param time  time in julianish century.
    *
    * @return heliocentrically place vector in [AE], ecliptic and
    *         equinox of the date
    */
   public Vec3D marsPos(double time) {

      double  m2,m3,m4,m5,m6;          // Mittlere Anomalien
      double  dl, dr, db;              // Korrekturen in Laenge ["],
                                       // Entfernung [AE] und Breite ["]
      double  l,b,r;                   // Ekliptikale Koordinaten
    
      // Mittlere Anomalien der Planeten in [rad]
      m2 = MoreMath.PI2 * MoreMath.frac( 0.1382208 + 162.5482542 * time);
      m3 = MoreMath.PI2 * MoreMath.frac( 0.9926208 +  99.9970236 * time);
      m4 = MoreMath.PI2 * MoreMath.frac( 0.0538553 +  53.1662736 * time);
      m5 = MoreMath.PI2 * MoreMath.frac( 0.0548944 +   8.4290611 * time);
      m6 = MoreMath.PI2 * MoreMath.frac( 0.8811167 +   3.3935250 * time);
    
      // Stoerungen durch Venus
      pert.init(time, m4, 0, 7, m2, -2, -1);
    
      pert.term( 0,-1,0, -0.01,   -0.03,      0.10, -0.04,    0.00,   0.00);
      pert.term( 1,-1,0,  0.05,    0.10,     -2.08,  0.75,    0.00,   0.00);
      pert.term( 2,-1,0, -0.25,   -0.57,     -2.58,  1.18,    0.05,  -0.04);
      pert.term( 2,-2,0,  0.02,    0.02,      0.13, -0.14,    0.00,   0.00);
      pert.term( 3,-1,0,  3.41,    5.38,      1.87, -1.15,    0.01,  -0.01);
      pert.term( 3,-2,0,  0.02,    0.02,      0.11, -0.13,    0.00,   0.00);
      pert.term( 4,-1,0,  0.32,    0.49,     -1.88,  1.21,   -0.07,   0.07);
      pert.term( 4,-2,0,  0.03,    0.03,      0.12, -0.14,    0.00,   0.00);
      pert.term( 5,-1,0,  0.04,    0.06,     -0.17,  0.11,   -0.01,   0.01);
      pert.term( 5,-2,0,  0.11,    0.09,      0.35, -0.43,   -0.01,   0.01);
      pert.term( 6,-2,0, -0.36,   -0.28,     -0.20,  0.25,    0.00,   0.00);
      pert.term( 7,-2,0, -0.03,   -0.03,      0.11, -0.13,    0.00,  -0.01);
    
      dl = pert.dl();  dr = pert.dr();  db = pert.db();
    
      // Keplerbewegung und Stoerungen durch die Erde
      pert.init(time, m4, -1, 16, m3, -9, 0);
    
      pert.term( 1, 0,0, -5.32,38481.97,-141856.04,  0.40,-6321.67,1876.89);
      pert.term( 1, 0,1, -1.12,   37.98,   -138.67, -2.93,   37.28, 117.48);
      pert.term( 1, 0,2, -0.32,   -0.03,      0.12, -1.19,    1.04,  -0.40);
      pert.term( 2, 0,0, 28.28, 2285.80,  -6608.37,  0.00, -589.35, 174.81);
      pert.term( 2, 0,1,  1.64,    3.37,    -12.93,  0.00,    2.89,  11.10);
      pert.term( 2, 0,2,  0.00,    0.00,      0.00,  0.00,    0.10,  -0.03);
      pert.term( 3, 0,0,  5.31,  189.29,   -461.81,  0.00,  -61.98,  18.53);
      pert.term( 3, 0,1,  0.31,    0.35,     -1.36,  0.00,    0.25,   1.19);
      pert.term( 4, 0,0,  0.81,   17.96,    -38.26,  0.00,   -6.88,   2.08);
      pert.term( 4, 0,1,  0.05,    0.04,     -0.15,  0.00,    0.02,   0.14);
      pert.term( 5, 0,0,  0.11,    1.83,     -3.48,  0.00,   -0.79,   0.24);
      pert.term( 6, 0,0,  0.02,    0.20,     -0.34,  0.00,   -0.09,   0.03);
      pert.term(-1,-1,0,  0.09,    0.06,      0.14, -0.22,    0.02,  -0.02);
      pert.term( 0,-1,0,  0.72,    0.49,      1.55, -2.31,    0.12,  -0.10);
      pert.term( 1,-1,0,  7.00,    4.92,     13.93,-20.48,    0.08,  -0.13);
      pert.term( 2,-1,0, 13.08,    4.89,     -4.53, 10.01,   -0.05,   0.13);
      pert.term( 2,-2,0,  0.14,    0.05,     -0.48, -2.66,    0.01,   0.14);
      pert.term( 3,-1,0,  1.38,    0.56,     -2.00,  4.85,   -0.01,   0.19);
      pert.term( 3,-2,0, -6.85,    2.68,      8.38, 21.42,    0.00,   0.03);
      pert.term( 3,-3,0, -0.08,    0.20,      1.20,  0.46,    0.00,   0.00);
      pert.term( 4,-1,0,  0.16,    0.07,     -0.19,  0.47,   -0.01,   0.05);
      pert.term( 4,-2,0, -4.41,    2.14,     -3.33, -7.21,   -0.07,  -0.09);
      pert.term( 4,-3,0, -0.12,    0.33,      2.22,  0.72,   -0.03,  -0.02);
      pert.term( 4,-4,0, -0.04,   -0.06,     -0.36,  0.23,    0.00,   0.00);
      pert.term( 5,-2,0, -0.44,    0.21,     -0.70, -1.46,   -0.06,  -0.07);
      pert.term( 5,-3,0,  0.48,   -2.60,     -7.25, -1.37,    0.00,   0.00);
      pert.term( 5,-4,0, -0.09,   -0.12,     -0.66,  0.50,    0.00,   0.00);
      pert.term( 5,-5,0,  0.03,    0.00,      0.01, -0.17,    0.00,   0.00);
      pert.term( 6,-2,0, -0.05,    0.03,     -0.07, -0.15,   -0.01,  -0.01);
      pert.term( 6,-3,0,  0.10,   -0.96,      2.36,  0.30,    0.04,   0.00);
      pert.term( 6,-4,0, -0.17,   -0.20,     -1.09,  0.94,    0.02,  -0.02);
      pert.term( 6,-5,0,  0.05,    0.00,      0.00, -0.30,    0.00,   0.00);
      pert.term( 7,-3,0,  0.01,   -0.10,      0.32,  0.04,    0.02,   0.00);
      pert.term( 7,-4,0,  0.86,    0.77,      1.86, -2.01,    0.01,  -0.01);
      pert.term( 7,-5,0,  0.09,   -0.01,     -0.05, -0.44,    0.00,   0.00);
      pert.term( 7,-6,0, -0.01,    0.02,      0.10,  0.08,    0.00,   0.00);
      pert.term( 8,-4,0,  0.20,    0.16,     -0.53,  0.64,   -0.01,   0.02);
      pert.term( 8,-5,0,  0.17,   -0.03,     -0.14, -0.84,    0.00,   0.01);
      pert.term( 8,-6,0, -0.02,    0.03,      0.16,  0.09,    0.00,   0.00);
      pert.term( 9,-5,0, -0.55,    0.15,      0.30,  1.10,    0.00,   0.00);
      pert.term( 9,-6,0, -0.02,    0.04,      0.20,  0.10,    0.00,   0.00);
      pert.term(10,-5,0, -0.09,    0.03,     -0.10, -0.33,    0.00,  -0.01);
      pert.term(10,-6,0, -0.05,    0.11,      0.48,  0.21,   -0.01,   0.00);
      pert.term(11,-6,0,  0.10,   -0.35,     -0.52, -0.15,    0.00,   0.00);
      pert.term(11,-7,0, -0.01,   -0.02,     -0.10,  0.07,    0.00,   0.00);
      pert.term(12,-6,0,  0.01,   -0.04,      0.18,  0.04,    0.01,   0.00);
      pert.term(12,-7,0, -0.05,   -0.07,     -0.29,  0.20,    0.01,   0.00);
      pert.term(13,-7,0,  0.23,    0.27,      0.25, -0.21,    0.00,   0.00);
      pert.term(14,-7,0,  0.02,    0.03,     -0.10,  0.09,    0.00,   0.00);
      pert.term(14,-8,0,  0.05,    0.01,      0.03, -0.23,    0.00,   0.03);
      pert.term(15,-8,0, -1.53,    0.27,      0.06,  0.42,    0.00,   0.00);
      pert.term(16,-8,0, -0.14,    0.02,     -0.10, -0.55,   -0.01,  -0.02);
      pert.term(16,-9,0,  0.03,   -0.06,     -0.25, -0.11,    0.00,   0.00);
    
      dl += pert.dl();  dr += pert.dr();  db += pert.db();
    
      // Stoerungen durch Jupiter
      pert.init(time, m4, -2, 5, m5, -5, -1);
    
      pert.term(-2,-1,0,  0.05,    0.03,      0.08, -0.14,    0.01,  -0.01);
      pert.term(-1,-1,0,  0.39,    0.27,      0.92, -1.50,   -0.03,  -0.06);
      pert.term(-1,-2,0, -0.16,    0.03,      0.13,  0.67,   -0.01,   0.06);
      pert.term(-1,-3,0, -0.02,    0.01,      0.05,  0.09,    0.00,   0.01);
      pert.term( 0,-1,0,  3.56,    1.13,     -5.41, -7.18,   -0.25,  -0.24);
      pert.term( 0,-2,0, -1.44,    0.25,      1.24,  7.96,    0.02,   0.31);
      pert.term( 0,-3,0, -0.21,    0.11,      0.55,  1.04,    0.01,   0.05);
      pert.term( 0,-4,0, -0.02,    0.02,      0.11,  0.11,    0.00,   0.01);
      pert.term( 1,-1,0, 16.67,  -19.15,     61.00, 53.36,   -0.06,  -0.07);
      pert.term( 1,-2,0,-21.64,    3.18,     -7.77,-54.64,   -0.31,   0.50);
      pert.term( 1,-3,0, -2.82,    1.45,     -2.53, -5.73,    0.01,   0.07);
      pert.term( 1,-4,0, -0.31,    0.28,     -0.34, -0.51,    0.00,   0.00);
      pert.term( 2,-1,0,  2.15,   -2.29,      7.04,  6.94,    0.33,   0.19);
      pert.term( 2,-2,0,-15.69,    3.31,    -15.70,-73.17,   -0.17,  -0.25);
      pert.term( 2,-3,0, -1.73,    1.95,     -9.19, -7.20,    0.02,  -0.03);
      pert.term( 2,-4,0, -0.01,    0.33,     -1.42,  0.08,    0.01,  -0.01);
      pert.term( 2,-5,0,  0.03,    0.03,     -0.13,  0.12,    0.00,   0.00);
      pert.term( 3,-1,0,  0.26,   -0.28,      0.73,  0.71,    0.08,   0.04);
      pert.term( 3,-2,0, -2.06,    0.46,     -1.61, -6.72,   -0.13,  -0.25);
      pert.term( 3,-3,0, -1.28,   -0.27,      2.21, -6.90,   -0.04,  -0.02);
      pert.term( 3,-4,0, -0.22,    0.08,     -0.44, -1.25,    0.00,   0.01);
      pert.term( 3,-5,0, -0.02,    0.03,     -0.15, -0.08,    0.00,   0.00);
      pert.term( 4,-1,0,  0.03,   -0.03,      0.08,  0.08,    0.01,   0.01);
      pert.term( 4,-2,0, -0.26,    0.06,     -0.17, -0.70,   -0.03,  -0.05);
      pert.term( 4,-3,0, -0.20,   -0.05,      0.22, -0.79,   -0.01,  -0.02);
      pert.term( 4,-4,0, -0.11,   -0.14,      0.93, -0.60,    0.00,   0.00);
      pert.term( 4,-5,0, -0.04,   -0.02,      0.09, -0.23,    0.00,   0.00);
      pert.term( 5,-4,0, -0.02,   -0.03,      0.13, -0.09,    0.00,   0.00);
      pert.term( 5,-5,0,  0.00,   -0.03,      0.21,  0.01,    0.00,   0.00);
    
      dl += pert.dl();  dr += pert.dr();  db += pert.db();
    
      // Stoerungen durch Saturn
      pert.init(time, m4, -1, 3, m6, -4, -1);
    
      pert.term(-1,-1,0,  0.03,    0.13,      0.48, -0.13,    0.02,   0.00);
      pert.term( 0,-1,0,  0.27,    0.84,      0.40, -0.43,    0.01,  -0.01);
      pert.term( 0,-2,0,  0.12,   -0.04,     -0.33, -0.55,   -0.01,  -0.02);
      pert.term( 0,-3,0,  0.02,   -0.01,     -0.07, -0.08,    0.00,   0.00);
      pert.term( 1,-1,0,  1.12,    0.76,     -2.66,  3.91,   -0.01,   0.01);
      pert.term( 1,-2,0,  1.49,   -0.95,      3.07,  4.83,    0.04,  -0.05);
      pert.term( 1,-3,0,  0.21,   -0.18,      0.55,  0.64,    0.00,   0.00);
      pert.term( 2,-1,0,  0.12,    0.10,     -0.29,  0.34,   -0.01,   0.02);
      pert.term( 2,-2,0,  0.51,   -0.36,      1.61,  2.25,    0.03,   0.01);
      pert.term( 2,-3,0,  0.10,   -0.10,      0.50,  0.43,    0.00,   0.00);
      pert.term( 2,-4,0,  0.01,   -0.02,      0.11,  0.05,    0.00,   0.00);
      pert.term( 3,-2,0,  0.07,   -0.05,      0.16,  0.22,    0.01,   0.01);
    
      dl += pert.dl();  dr += pert.dr();  db += pert.db();
    
      // Ekliptikale Koordinaten ([rad],[AE])
      dl += 52.49 * StrictMath.sin(MoreMath.PI2 * (0.1868 + 0.0549 * time)) +
             0.61 * StrictMath.sin(MoreMath.PI2 * (0.9220 + 0.3307 * time)) +
             0.32 * StrictMath.sin(MoreMath.PI2 * (0.4731 + 2.1485 * time)) +
             0.28 * StrictMath.sin(MoreMath.PI2 * (0.9467 + 0.1133 * time));
      dl += 0.14 + 0.87 * time - 0.11 * time * time;
    
      l = MoreMath.PI2 * MoreMath.frac(0.9334591 + m4 / MoreMath.PI2 +
                                       ((6615.5 + 1.1 * time) * time + dl ) / 1296.0e3);
      r = 1.5303352 + 0.0000131 * time + dr * 1.0E-6;
      b = (596.32 + (-2.92 - 0.10 * time) * time + db) / MoreMath.ARCS;
    
      return new Vec3D(new Polar(l, b, r));  // Ort   
   }

   /**
    * Calculate the position of jupiter with analytic series expansion.
    *
    * @param time  time in julianish century.
    *
    * @return heliocentrically place vector in [AE], ecliptic and
    *         equinox of the date
    */
   public Vec3D jupiterPos(double time) {

      double  m5,m6,m7;                // Mittlere Anomalien
      double  phi, c, s;
      double  dl, dr, db;              // Korrekturen in Laenge ["],
                                       // Entfernung [AE] und Breite ["]
      double  l,b,r;                   // Ekliptikale Koordinaten
    
      // Mittlere Anomalien der Planeten in [rad]
      m5 = MoreMath.PI2 * MoreMath.frac( 0.0565314 + 8.4302963 * time);
      m6 = MoreMath.PI2 * MoreMath.frac( 0.8829867 + 3.3947688 * time);
      m7 = MoreMath.PI2 * MoreMath.frac( 0.3969537 + 1.1902586 * time);
    
      // Keplerbewegung und Stoerungen durch Saturn
      pert.init(time, m5, -1, 5, m6, -10, 0);
    
      pert.term(-1, -1,0,  -0.2,    1.4,     2.0,   0.6,    0.1, -0.2);
      pert.term( 0, -1,0,   9.4,    8.9,     3.9,  -8.3,   -0.4, -1.4);
      pert.term( 0, -2,0,   5.6,   -3.0,    -5.4,  -5.7,   -2.0,  0.0);
      pert.term( 0, -3,0,  -4.0,   -0.1,     0.0,   5.5,    0.0,  0.0);
      pert.term( 0, -5,0,   3.3,   -1.6,    -1.6,  -3.1,   -0.5, -1.2);
      pert.term( 1,  0,0,-113.1,19998.6,-25208.2,-142.2,-4670.7,288.9);
      pert.term( 1,  0,1, -76.1,   66.9,   -84.2, -95.8,   21.6, 29.4);
      pert.term( 1,  0,2,  -0.5,   -0.3,     0.4,  -0.7,    0.1, -0.1);
      pert.term( 1, -1,0,  78.8,  -14.5,    11.5,  64.4,   -0.2,  0.2);
      pert.term( 1, -2,0,  -2.0, -132.4,    28.8,   4.3,   -1.7,  0.4);
      pert.term( 1, -2,1,  -1.1,   -0.7,     0.2,  -0.3,    0.0,  0.0);
      pert.term( 1, -3,0,  -7.5,   -6.8,    -0.4,  -1.1,    0.6, -0.9);
      pert.term( 1, -4,0,   0.7,    0.7,     0.6,  -1.1,    0.0, -0.2);
      pert.term( 1, -5,0,  51.5,  -26.0,   -32.5, -64.4,   -4.9,-12.4);
      pert.term( 1, -5,1,  -1.2,   -2.2,    -2.7,   1.5,   -0.4,  0.3);
      pert.term( 2,  0,0,  -3.4,  632.0,  -610.6,  -6.5, -226.8, 12.7);
      pert.term( 2,  0,1,  -4.2,    3.8,    -4.1,  -4.5,    0.2,  0.6);
      pert.term( 2, -1,0,   5.3,   -0.7,     0.7,   6.1,    0.2,  1.1);
      pert.term( 2, -2,0, -76.4, -185.1,   260.2,-108.0,    1.6,  0.0);
      pert.term( 2, -3,0,  66.7,   47.8,   -51.4,  69.8,    0.9,  0.3);
      pert.term( 2, -3,1,   0.6,   -1.0,     1.0,   0.6,    0.0,  0.0);
      pert.term( 2, -4,0,  17.0,    1.4,    -1.8,   9.6,    0.0, -0.1);
      pert.term( 2, -5,0,1066.2, -518.3,    -1.3, -23.9,    1.8, -0.3);
      pert.term( 2, -5,1, -25.4,  -40.3,    -0.9,   0.3,    0.0,  0.0);
      pert.term( 2, -5,2,  -0.7,    0.5,     0.0,   0.0,    0.0,  0.0);
      pert.term( 3,  0,0,  -0.1,   28.0,   -22.1,  -0.2,  -12.5,  0.7);
      pert.term( 3, -2,0,  -5.0,  -11.5,    11.7,  -5.4,    2.1, -1.0);
      pert.term( 3, -3,0,  16.9,   -6.4,    13.4,  26.9,   -0.5,  0.8);
      pert.term( 3, -4,0,   7.2,  -13.3,    20.9,  10.5,    0.1, -0.1);
      pert.term( 3, -5,0,  68.5,  134.3,  -166.9,  86.5,    7.1, 15.2);
      pert.term( 3, -5,1,   3.5,   -2.7,     3.4,   4.3,    0.5, -0.4);
      pert.term( 3, -6,0,   0.6,    1.0,    -0.9,   0.5,    0.0,  0.0);
      pert.term( 3, -7,0,  -1.1,    1.7,    -0.4,  -0.2,    0.0,  0.0);
      pert.term( 4,  0,0,   0.0,    1.4,    -1.0,   0.0,   -0.6,  0.0);
      pert.term( 4, -2,0,  -0.3,   -0.7,     0.4,  -0.2,    0.2, -0.1);
      pert.term( 4, -3,0,   1.1,   -0.6,     0.9,   1.2,    0.1,  0.2);
      pert.term( 4, -4,0,   3.2,    1.7,    -4.1,   5.8,    0.2,  0.1);
      pert.term( 4, -5,0,   6.7,    8.7,    -9.3,   8.7,   -1.1,  1.6);
      pert.term( 4, -6,0,   1.5,   -0.3,     0.6,   2.4,    0.0,  0.0);
      pert.term( 4, -7,0,  -1.9,    2.3,    -3.2,  -2.7,    0.0, -0.1);
      pert.term( 4, -8,0,   0.4,   -1.8,     1.9,   0.5,    0.0,  0.0);
      pert.term( 4, -9,0,  -0.2,   -0.5,     0.3,  -0.1,    0.0,  0.0);
      pert.term( 4,-10,0,  -8.6,   -6.8,    -0.4,   0.1,    0.0,  0.0);
      pert.term( 4,-10,1,  -0.5,    0.6,     0.0,   0.0,    0.0,  0.0);
      pert.term( 5, -5,0,  -0.1,    1.5,    -2.5,  -0.8,   -0.1,  0.1);
      pert.term( 5, -6,0,   0.1,    0.8,    -1.6,   0.1,    0.0,  0.0);
      pert.term( 5, -9,0,  -0.5,   -0.1,     0.1,  -0.8,    0.0,  0.0);
      pert.term( 5,-10,0,   2.5,   -2.2,     2.8,   3.1,    0.1, -0.2);
    
      dl = pert.dl();  dr = pert.dr();  db = pert.db();
    
      // Stoerungen durch Uranus
      pert.init(time, m5, 1, 1, m7, -2, -1);
    
      pert.term( 1, -1,0,   0.4,    0.9,     0.0,   0.0,    0.0,  0.0);
      pert.term( 1, -2,0,   0.4,    0.4,    -0.4,   0.3,    0.0,  0.0);
    
      dl += pert.dl();  dr += pert.dr();  db += pert.db();
    
      // Stoerungen durch Saturn und Uranus
      phi = (2 * m5 - 6 * m6 + 3 * m7);
      c   = StrictMath.cos(phi);
      s   = StrictMath.sin(phi);
      dl += -0.8 * c + 8.5 * s;
      dr += -0.1 * c;
    
      phi = (3 * m5 - 6 * m6 + 3 * m7);
      c   = StrictMath.cos(phi);
      s   = StrictMath.sin(phi);
      dl += +0.4 * c + 0.5 * s;
      dr += -0.7 * c + 0.5 * s;
      db += -0.1 * c;
    
      // Ekliptikale Koordinaten ([rad],[AE])
      l = MoreMath.PI2 * MoreMath.frac(0.0388910 + m5 / MoreMath.PI2 +
                                       ((5025.2 + 0.8 * time) * time + dl) / 1296.0e3);
      r = 5.208873 + 0.000041 * time + dr * 1.0E-5;
      b = (227.3 - 0.3 * time + db) / MoreMath.ARCS;
    
      return new Vec3D(new Polar(l, b, r));  // Ort   
   }

   /**
    * Calculate the position of saturn with analytic series expansion.
    *
    * @param time  time in julianish century.
    *
    * @return heliocentrically place vector in [AE], ecliptic and
    *         equinox of the date
    */
   public Vec3D saturnPos(double time) {

      double  m5,m6,m7,m8;             // Mittlere Anomalien
      double  phi, c, s;
      double  dl, dr, db;              // Korrekturen in Laenge ["],
                                       // Entfernung [AE] und Breite ["]
      double  l,b,r;                   // Ekliptikale Koordinaten
    
      // Mittlere Anomalien der Planeten in [rad]
      m5 = MoreMath.PI2 * MoreMath.frac( 0.0565314 + 8.4302963 * time);
      m6 = MoreMath.PI2 * MoreMath.frac( 0.8829867 + 3.3947688 * time);
      m7 = MoreMath.PI2 * MoreMath.frac( 0.3969537 + 1.1902586 * time);
      m8 = MoreMath.PI2 * MoreMath.frac( 0.7208473 + 0.6068623 * time);
    
      // Keplerbewegung und Stoerungen durch Jupiter
      pert.init(time, m6, 0, 11, m5, -6, 0);
    
      pert.term( 0,-1,0,   12.0,   -1.4,   -13.9,    6.4,    1.2,  -1.8);
      pert.term( 0,-2,0,    0.0,   -0.2,    -0.9,    1.0,    0.0,  -0.1);
      pert.term( 1, 1,0,    0.9,    0.4,    -1.8,    1.9,    0.2,   0.2);
      pert.term( 1, 0,0, -348.3,22907.7,-52915.5, -752.2,-3266.5,8314.4);
      pert.term( 1, 0,1, -225.2, -146.2,   337.7, -521.3,   79.6,  17.4);
      pert.term( 1, 0,2,    1.3,   -1.4,     3.2,    2.9,    0.1,  -0.4);
      pert.term( 1,-1,0,   -1.0,  -30.7,   108.6, -815.0,   -3.6,  -9.3);
      pert.term( 1,-2,0,   -2.0,   -2.7,    -2.1,  -11.9,   -0.1,  -0.4);
      pert.term( 2, 1,0,    0.1,    0.2,    -1.0,    0.3,    0.0,   0.0);
      pert.term( 2, 0,0,   44.2,  724.0, -1464.3,  -34.7, -188.7, 459.1);
      pert.term( 2, 0,1,  -17.0,  -11.3,    18.9,  -28.6,    1.0,  -3.7);
      pert.term( 2,-1,0,   -3.5, -426.6,  -546.5,  -26.5,   -1.6,  -2.7);
      pert.term( 2,-1,1,    3.5,   -2.2,    -2.6,   -4.3,    0.0,   0.0);
      pert.term( 2,-2,0,   10.5,  -30.9,  -130.5,  -52.3,   -1.9,   0.2);
      pert.term( 2,-3,0,   -0.2,   -0.4,    -1.2,   -0.1,   -0.1,   0.0);
      pert.term( 3, 0,0,    6.5,   30.5,   -61.1,    0.4,  -11.6,  28.1);
      pert.term( 3, 0,1,   -1.2,   -0.7,     1.1,   -1.8,   -0.2,  -0.6);
      pert.term( 3,-1,0,   29.0,  -40.2,    98.2,   45.3,    3.2,  -9.4);
      pert.term( 3,-1,1,    0.6,    0.6,    -1.0,    1.3,    0.0,   0.0);
      pert.term( 3,-2,0,  -27.0,  -21.1,   -68.5,    8.1,  -19.8,   5.4);
      pert.term( 3,-2,1,    0.9,   -0.5,    -0.4,   -2.0,   -0.1,  -0.8);
      pert.term( 3,-3,0,   -5.4,   -4.1,   -19.1,   26.2,   -0.1,  -0.1);
      pert.term( 4, 0,0,    0.6,    1.4,    -3.0,   -0.2,   -0.6,   1.6);
      pert.term( 4,-1,0,    1.5,   -2.5,    12.4,    4.7,    1.0,  -1.1);
      pert.term( 4,-2,0, -821.9,   -9.6,   -26.0, 1873.6,  -70.5,  -4.4);
      pert.term( 4,-2,1,    4.1,  -21.9,   -50.3,   -9.9,    0.7,  -3.0);
      pert.term( 4,-3,0,   -2.0,   -4.7,   -19.3,    8.2,   -0.1,  -0.3);
      pert.term( 4,-4,0,   -1.5,    1.3,     6.5,    7.3,    0.0,   0.0);
      pert.term( 5,-2,0,-2627.6,-1277.3,   117.4, -344.1,  -13.8,  -4.3);
      pert.term( 5,-2,1,   63.0,  -98.6,    12.7,    6.7,    0.1,  -0.2);
      pert.term( 5,-2,2,    1.7,    1.2,    -0.2,    0.3,    0.0,   0.0);
      pert.term( 5,-3,0,    0.4,   -3.6,   -11.3,   -1.6,    0.0,  -0.3);
      pert.term( 5,-4,0,   -1.4,    0.3,     1.5,    6.3,   -0.1,   0.0);
      pert.term( 5,-5,0,    0.3,    0.6,     3.0,   -1.7,    0.0,   0.0);
      pert.term( 6,-2,0, -146.7,  -73.7,   166.4, -334.3,  -43.6, -46.7);
      pert.term( 6,-2,1,    5.2,   -6.8,    15.1,   11.4,    1.7,  -1.0);
      pert.term( 6,-3,0,    1.5,   -2.9,    -2.2,   -1.3,    0.1,  -0.1);
      pert.term( 6,-4,0,   -0.7,   -0.2,    -0.7,    2.8,    0.0,   0.0);
      pert.term( 6,-5,0,    0.0,    0.5,     2.5,   -0.1,    0.0,   0.0);
      pert.term( 6,-6,0,    0.3,   -0.1,    -0.3,   -1.2,    0.0,   0.0);
      pert.term( 7,-2,0,   -9.6,   -3.9,     9.6,  -18.6,   -4.7,  -5.3);
      pert.term( 7,-2,1,    0.4,   -0.5,     1.0,    0.9,    0.3,  -0.1);
      pert.term( 7,-3,0,    3.0,    5.3,     7.5,   -3.5,    0.0,   0.0);
      pert.term( 7,-4,0,    0.2,    0.4,     1.6,   -1.3,    0.0,   0.0);
      pert.term( 7,-5,0,   -0.1,    0.2,     1.0,    0.5,    0.0,   0.0);
      pert.term( 7,-6,0,    0.2,    0.0,     0.2,   -1.0,    0.0,   0.0);
      pert.term( 8,-2,0,   -0.7,   -0.2,     0.6,   -1.2,   -0.4,  -0.4);
      pert.term( 8,-3,0,    0.5,    1.0,    -2.0,    1.5,    0.1,   0.2);
      pert.term( 8,-4,0,    0.4,    1.3,     3.6,   -0.9,    0.0,  -0.1);
      pert.term( 9,-4,0,    4.0,   -8.7,   -19.9,   -9.9,    0.2,  -0.4);
      pert.term( 9,-4,1,    0.5,    0.3,     0.8,   -1.8,    0.0,   0.0);
      pert.term(10,-4,0,   21.3,  -16.8,     3.3,    3.3,    0.2,  -0.2);
      pert.term(10,-4,1,    1.0,    1.7,    -0.4,    0.4,    0.0,   0.0);
      pert.term(11,-4,0,    1.6,   -1.3,     3.0,    3.7,    0.8,  -0.2);
    
      dl = pert.dl();  dr = pert.dr();  db = pert.db();
    
      // Stoerungen durch Uranus
      pert.init(time, m6, 0, 3, m7, -5, -1);
    
      pert.term( 0,-1,0,    1.0,    0.7,     0.4,   -1.5,    0.1,   0.0);
      pert.term( 0,-2,0,    0.0,   -0.4,    -1.1,    0.1,   -0.1,  -0.1);
      pert.term( 0,-3,0,   -0.9,   -1.2,    -2.7,    2.1,   -0.5,  -0.3);
      pert.term( 1,-1,0,    7.8,   -1.5,     2.3,   12.7,    0.0,   0.0);
      pert.term( 1,-2,0,   -1.1,   -8.1,     5.2,   -0.3,   -0.3,  -0.3);
      pert.term( 1,-3,0,  -16.4,  -21.0,    -2.1,    0.0,    0.4,   0.0);
      pert.term( 2,-1,0,    0.6,   -0.1,     0.1,    1.2,    0.1,   0.0);
      pert.term( 2,-2,0,   -4.9,  -11.7,    31.5,  -13.3,    0.0,  -0.2);
      pert.term( 2,-3,0,   19.1,   10.0,   -22.1,   42.1,    0.1,  -1.1);
      pert.term( 2,-4,0,    0.9,   -0.1,     0.1,    1.4,    0.0,   0.0);
      pert.term( 3,-2,0,   -0.4,   -0.9,     1.7,   -0.8,    0.0,  -0.3);
      pert.term( 3,-3,0,    2.3,    0.0,     1.0,    5.7,    0.3,   0.3);
      pert.term( 3,-4,0,    0.3,   -0.7,     2.0,    0.7,    0.0,   0.0);
      pert.term( 3,-5,0,   -0.1,   -0.4,     1.1,   -0.3,    0.0,   0.0);
    
      dl += pert.dl();  dr += pert.dr();  db += pert.db();
    
      // Stoerungen durch Neptun
      pert.init(time, m6, 1, 2, m8, -2, -1);
    
      pert.term( 1,-1,0,   -1.3,   -1.2,     2.3,   -2.5,    0.0,   0.0);
      pert.term( 1,-2,0,    1.0,   -0.1,     0.1,    1.4,    0.0,   0.0);
      pert.term( 2,-2,0,    1.1,   -0.1,     0.2,    3.3,    0.0,   0.0);
    
      dl += pert.dl();  dr += pert.dr();  db += pert.db();
    
      // Stoerungen durch Jupiter und Uranus
      phi = (-2 * m5 + 5 * m6 - 3 * m7);
      c   = StrictMath.cos(phi);
      s   = StrictMath.sin(phi);
    
      dl += -0.8 * c - 0.1 * s;
      dr += -0.2 * c + 1.8 * s;
      db += +0.3 * c + 0.5 * s;
    
      phi = (-2 * m5 + 6 * m6 - 3 * m7);
      c   = StrictMath.cos(phi);
      s   = StrictMath.sin(phi);
      dl += (2.4 - 0.7 * time) * c + (27.8 - 0.4 * time) * s;
      dr += 2.1 * c - 0.2 * s;
    
      phi = (-2 * m5 + 7 * m6 - 3 * m7);
      c   = StrictMath.cos(phi);
      s   = StrictMath.sin(phi);
      dl += +0.1 * c + 1.6 * s;
      dr += -3.6 * c + 0.3 * s;
      db += -0.2 * c + 0.6 * s;
    
      // Ekliptikale Koordinaten ([rad],[AE])
      l = MoreMath.PI2 * MoreMath.frac(0.2561136 + m6 / MoreMath.PI2 + 
                                       ((5018.6 + time * 1.9) * time + dl) / 1296.0e3);
      r = 9.557584 - 0.000186 * time + dr * 1.0e-5;
      b = (175.1 - 10.2 * time + db) / MoreMath.ARCS;
    
      return new Vec3D(new Polar(l, b, r));  // Ort   
   }

   /**
    * Calculate the position of uranus with analytic series expansion.
    *
    * @param time  time in julianish century.
    *
    * @return heliocentrically place vector in [AE], ecliptic and
    *         equinox of the date
    */
   public Vec3D uranusPos(double time) {

      double  m5,m6,m7,m8;             // Mittlere Anomalien
      double  dl, dr, db;              // Korrekturen in Laenge ["],
                                       // Entfernung [AE] und Breite ["]
      double  l,b,r;                   // Ekliptikale Koordinaten
    
      // Mittlere Anomalien der Planeten in [rad]
      m5 = MoreMath.PI2 * MoreMath.frac( 0.0564472 + 8.4302889 * time);
      m6 = MoreMath.PI2 * MoreMath.frac( 0.8829611 + 3.3947583 * time);
      m7 = MoreMath.PI2 * MoreMath.frac( 0.3967117 + 1.1902849 * time);
      m8 = MoreMath.PI2 * MoreMath.frac( 0.7216833 + 0.6068528 * time);
    
      // Stoerungen durch Jupiter
      pert.init(time, m7, -1, 3, m5, -2, -1);
    
      pert.term(-1,-1,0,  0.0,    0.0,    -0.1,   1.7,  -0.1,   0.0);
      pert.term( 0,-1,0,  0.5,   -1.2,    18.9,   9.1,  -0.9,   0.1);
      pert.term( 1,-1,0,-21.2,   48.7,  -455.5,-198.8,   0.0,   0.0);
      pert.term( 1,-2,0, -0.5,    1.2,   -10.9,  -4.8,   0.0,   0.0);
      pert.term( 2,-1,0, -1.3,    3.2,   -23.2, -11.1,   0.3,   0.1);
      pert.term( 2,-2,0, -0.2,    0.2,     1.1,   1.5,   0.0,   0.0);
      pert.term( 3,-1,0,  0.0,    0.2,    -1.8,   0.4,   0.0,   0.0);
    
      dl = pert.dl();  dr = pert.dr();  db = pert.db();
    
      // Stoerungen durch Saturn
      pert.init(time, m7, 0, 11, m6, -6, 0);
    
      pert.term( 0,-1,0,  1.4,   -0.5,    -6.4,   9.0,  -0.4,  -0.8);
      pert.term( 1,-1,0,-18.6,  -12.6,    36.7,-336.8,   1.0,   0.3);
      pert.term( 1,-2,0, -0.7,   -0.3,     0.5,  -7.5,   0.1,   0.0);
      pert.term( 2,-1,0, 20.0, -141.6,  -587.1,-107.0,   3.1,  -0.8);
      pert.term( 2,-1,1,  1.0,    1.4,     5.8,  -4.0,   0.0,   0.0);
      pert.term( 2,-2,0,  1.6,   -3.8,   -35.6, -16.0,   0.0,   0.0);
      pert.term( 3,-1,0, 75.3, -100.9,   128.9,  77.5,  -0.8,   0.1);
      pert.term( 3,-1,1,  0.2,    1.8,    -1.9,   0.3,   0.0,   0.0);
      pert.term( 3,-2,0,  2.3,   -1.3,    -9.5, -17.9,   0.0,   0.1);
      pert.term( 3,-3,0, -0.7,   -0.5,    -4.9,   6.8,   0.0,   0.0);
      pert.term( 4,-1,0,  3.4,   -5.0,    21.6,  14.3,  -0.8,  -0.5);
      pert.term( 4,-2,0,  1.9,    0.1,     1.2, -12.1,   0.0,   0.0);
      pert.term( 4,-3,0, -0.1,   -0.4,    -3.9,   1.2,   0.0,   0.0);
      pert.term( 4,-4,0, -0.2,    0.1,     1.6,   1.8,   0.0,   0.0);
      pert.term( 5,-1,0,  0.2,   -0.3,     1.0,   0.6,  -0.1,   0.0);
      pert.term( 5,-2,0, -2.2,   -2.2,    -7.7,   8.5,   0.0,   0.0);
      pert.term( 5,-3,0,  0.1,   -0.2,    -1.4,  -0.4,   0.0,   0.0);
      pert.term( 5,-4,0, -0.1,    0.0,     0.1,   1.2,   0.0,   0.0);
      pert.term( 6,-2,0, -0.2,   -0.6,     1.4,  -0.7,   0.0,   0.0);
    
      dl += pert.dl();  dr += pert.dr();  db += pert.db();
    
      // Keplerbewegung und Stoerungen durch Neptun
      pert.init(time, m7, -1, 7, m8, -8, 0);
    
      pert.term( 1, 0,0,-78.1,19518.1,-90718.2,-334.7,2759.5,-311.9);
      pert.term( 1, 0,1,-81.6,  107.7,  -497.4,-379.5,  -2.8, -43.7);
      pert.term( 1, 0,2, -6.6,   -3.1,    14.4, -30.6,  -0.4,  -0.5);
      pert.term( 1, 0,3,  0.0,   -0.5,     2.4,   0.0,   0.0,   0.0);
      pert.term( 2, 0,0, -2.4,  586.1, -2145.2, -15.3, 130.6, -14.3);
      pert.term( 2, 0,1, -4.5,    6.6,   -24.2, -17.8,   0.7,  -1.6);
      pert.term( 2, 0,2, -0.4,    0.0,     0.1,  -1.4,   0.0,   0.0);
      pert.term( 3, 0,0,  0.0,   24.5,   -76.2,  -0.6,   7.0,  -0.7);
      pert.term( 3, 0,1, -0.2,    0.4,    -1.4,  -0.8,   0.1,  -0.1);
      pert.term( 4, 0,0,  0.0,    1.1,    -3.0,   0.1,   0.4,   0.0);
      pert.term(-1,-1,0, -0.2,    0.2,     0.7,   0.7,  -0.1,   0.0);
      pert.term( 0,-1,0, -2.8,    2.5,     8.7,  10.5,  -0.4,  -0.1);
      pert.term( 1,-1,0,-28.4,   20.3,   -51.4, -72.0,   0.0,   0.0);
      pert.term( 1,-2,0, -0.6,   -0.1,     4.2, -14.6,   0.2,   0.4);
      pert.term( 1,-3,0,  0.2,    0.5,     3.4,  -1.6,  -0.1,   0.1);
      pert.term( 2,-1,0, -1.8,    1.3,    -5.5,  -7.7,   0.0,   0.3);
      pert.term( 2,-2,0, 29.4,   10.2,   -29.0,  83.2,   0.0,   0.0);
      pert.term( 2,-3,0,  8.8,   17.8,   -41.9,  21.5,  -0.1,  -0.3);
      pert.term( 2,-4,0,  0.0,    0.1,    -2.1,  -0.9,   0.1,   0.0);
      pert.term( 3,-2,0,  1.5,    0.5,    -1.7,   5.1,   0.1,  -0.2);
      pert.term( 3,-3,0,  4.4,   14.6,   -84.3,  25.2,   0.1,  -0.1);
      pert.term( 3,-4,0,  2.4,   -4.5,    12.0,   6.2,   0.0,   0.0);
      pert.term( 3,-5,0,  2.9,   -0.9,     2.1,   6.2,   0.0,   0.0);
      pert.term( 4,-3,0,  0.3,    1.0,    -4.0,   1.1,   0.1,  -0.1);
      pert.term( 4,-4,0,  2.1,   -2.7,    17.9,  14.0,   0.0,   0.0);
      pert.term( 4,-5,0,  3.0,   -0.4,     2.3,  17.6,  -0.1,  -0.1);
      pert.term( 4,-6,0, -0.6,   -0.5,     1.1,  -1.6,   0.0,   0.0);
      pert.term( 5,-4,0,  0.2,   -0.2,     1.0,   0.8,   0.0,   0.0);
      pert.term( 5,-5,0, -0.9,   -0.1,     0.6,  -7.1,   0.0,   0.0);
      pert.term( 5,-6,0, -0.5,   -0.6,     3.8,  -3.6,   0.0,   0.0);
      pert.term( 5,-7,0,  0.0,   -0.5,     3.0,   0.1,   0.0,   0.0);
      pert.term( 6,-6,0,  0.2,    0.3,    -2.7,   1.6,   0.0,   0.0);
      pert.term( 6,-7,0, -0.1,    0.2,    -2.0,  -0.4,   0.0,   0.0);
      pert.term( 7,-7,0,  0.1,   -0.2,     1.3,   0.5,   0.0,   0.0);
      pert.term( 7,-8,0,  0.1,    0.0,     0.4,   0.9,   0.0,   0.0);
    
      dl += pert.dl();  dr += pert.dr();  db += pert.db();
    
      // Stoerungen durch Jupiter und Uranus
      pert.init(time, m7, -2, 4, m6, -6, -4, 2*m5);
    
      pert.term(-2,-4,0, -0.7,    0.4,    -1.5,  -2.5,   0.0,   0.0);
      pert.term(-1,-4,0, -0.1,   -0.1,    -2.2,   1.0,   0.0,   0.0);
      pert.term( 1,-5,0,  0.1,   -0.4,     1.4,   0.2,   0.0,   0.0);
      pert.term( 1,-6,0,  0.4,    0.5,    -0.8,  -0.8,   0.0,   0.0);
      pert.term( 2,-6,0,  5.7,    6.3,    28.5, -25.5,   0.0,   0.0);
      pert.term( 2,-6,1,  0.1,   -0.2,    -1.1,  -0.6,   0.0,   0.0);
      pert.term( 3,-6,0, -1.4,   29.2,   -11.4,   1.1,   0.0,   0.0);
      pert.term( 3,-6,1,  0.8,   -0.4,     0.2,   0.3,   0.0,   0.0);
      pert.term( 4,-6,0,  0.0,    1.3,    -6.0,  -0.1,   0.0,   0.0);
    
      dl += pert.dl();  dr += pert.dr();  db += pert.db();
    
      // Ekliptikale Koordinaten ([rad],[AE])
      l = MoreMath.PI2 * MoreMath.frac(0.4734843 + m7 / MoreMath.PI2 +
                                       ((5082.3 + 34.2 * time) * time + dl) / 1296.0e3);
      r = 19.211991 + (-0.000333 - 0.000005 * time) * time + dr * 1.0e-5;
      b = (-130.61 + (-0.54 + 0.04 * time) * time + db) / MoreMath.ARCS;
    
      return new Vec3D(new Polar(l, b, r));  // Ort   
   }

   /**
    * Calculate the position of neptune with analytic series expansion.
    *
    * @param time  time in julianish century.
    *
    * @return heliocentrically place vector in [AE], ecliptic and
    *         equinox of the date
    */
   public Vec3D neptunePos(double time) {

      double  m5,m6,m7,m8;             // Mittlere Anomalien
      double  dl, dr, db;              // Korrekturen in Laenge ["],
                                       // Entfernung [AE] und Breite ["]
      double  l,b,r;                   // Ekliptikale Koordinaten
    
      // Mittlere Anomalien der Planeten in [rad]
      m5 = MoreMath.PI2 * MoreMath.frac( 0.0563867 + 8.4298907 * time);
      m6 = MoreMath.PI2 * MoreMath.frac( 0.8825086 + 3.3957748 * time);
      m7 = MoreMath.PI2 * MoreMath.frac( 0.3965358 + 1.1902851 * time);
      m8 = MoreMath.PI2 * MoreMath.frac( 0.7214906 + 0.6068526 * time);
    
      // Stoerungen durch Jupiter
      pert.init(time, m8, 0, 2, m5, -2, 0);
    
      pert.term( 0,-1,0,  0.1,   0.1,    -3.0,   1.8,   -0.3, -0.3);
      pert.term( 1, 0,0,  0.0,   0.0,   -15.9,   9.0,    0.0,  0.0);
      pert.term( 1,-1,0,-17.6, -29.3,   416.1,-250.0,    0.0,  0.0);
      pert.term( 1,-2,0, -0.4,  -0.7,    10.4,  -6.2,    0.0,  0.0);
      pert.term( 2,-1,0, -0.2,  -0.4,     2.4,  -1.4,    0.4, -0.3);
    
      dl = pert.dl();  dr = pert.dr();  db = pert.db();
    
      // Stoerungen durch Saturn
      pert.init(time, m8, 0, 2, m6, -2, 0);
    
      pert.term( 0,-1,0, -0.1,   0.0,     0.2,  -1.8,   -0.1, -0.5);
      pert.term( 1, 0,0,  0.0,   0.0,    -8.3, -10.4,    0.0,  0.0);
      pert.term( 1,-1,0, 13.6, -12.7,   187.5, 201.1,    0.0,  0.0);
      pert.term( 1,-2,0,  0.4,  -0.4,     4.5,   4.5,    0.0,  0.0);
      pert.term( 2,-1,0,  0.4,  -0.1,     1.7,  -3.2,    0.2,  0.2);
      pert.term( 2,-2,0, -0.1,   0.0,    -0.2,   2.7,    0.0,  0.0);
    
      dl += pert.dl();  dr += pert.dr();  db += pert.db();
    
      // Keplerbewegung und Stoerungen durch Uranus
      pert.init(time, m8, 1, 6, m7, -6, 0);
    
      pert.term( 1, 0,0, 32.3,3549.5,-25880.2, 235.8,-6360.5,374.0);
      pert.term( 1, 0,1, 31.2,  34.4,  -251.4, 227.4,   34.9, 29.3);
      pert.term( 1, 0,2, -1.4,   3.9,   -28.6, -10.1,    0.0, -0.9);
      pert.term( 2, 0,0,  6.1,  68.0,  -111.4,   2.0,  -54.7,  3.7);
      pert.term( 2, 0,1,  0.8,  -0.2,    -2.1,   2.0,   -0.2,  0.8);
      pert.term( 3, 0,0,  0.1,   1.0,    -0.7,   0.0,   -0.8,  0.1);
      pert.term( 0,-1,0, -0.1,  -0.3,    -3.6,   0.0,    0.0,  0.0);
      pert.term( 1, 0,0,  0.0,   0.0,     5.5,  -6.9,    0.1,  0.0);
      pert.term( 1,-1,0, -2.2,  -1.6,  -116.3, 163.6,    0.0, -0.1);
      pert.term( 1,-2,0,  0.2,   0.1,    -1.2,   0.4,    0.0, -0.1);
      pert.term( 2,-1,0,  4.2,  -1.1,    -4.4, -34.6,   -0.2,  0.1);
      pert.term( 2,-2,0,  8.6,  -2.9,   -33.4, -97.0,    0.2,  0.1);
      pert.term( 3,-1,0,  0.1,  -0.2,     2.1,  -1.2,    0.0,  0.1);
      pert.term( 3,-2,0, -4.6,   9.3,    38.2,  19.8,    0.1,  0.1);
      pert.term( 3,-3,0, -0.5,   1.7,    23.5,   7.0,    0.0,  0.0);
      pert.term( 4,-2,0,  0.2,   0.8,     3.3,  -1.5,   -0.2, -0.1);
      pert.term( 4,-3,0,  0.9,   1.7,    17.9,  -9.1,   -0.1,  0.0);
      pert.term( 4,-4,0, -0.4,  -0.4,    -6.2,   4.8,    0.0,  0.0);
      pert.term( 5,-3,0, -1.6,  -0.5,    -2.2,   7.0,    0.0,  0.0);
      pert.term( 5,-4,0, -0.4,  -0.1,    -0.7,   5.5,    0.0,  0.0);
      pert.term( 5,-5,0,  0.2,   0.0,     0.0,  -3.5,    0.0,  0.0);
      pert.term( 6,-4,0, -0.3,   0.2,     2.1,   2.7,    0.0,  0.0);
      pert.term( 6,-5,0,  0.1,  -0.1,    -1.4,  -1.4,    0.0,  0.0);
      pert.term( 6,-6,0, -0.1,   0.1,     1.4,   0.7,    0.0,  0.0);
    
      dl += pert.dl();  dr += pert.dr();  db += pert.db();
    
      // Ekliptikale Koordinaten ([rad],[AE])
      l = MoreMath.PI2 * MoreMath.frac(0.1254046 + m8 / MoreMath.PI2 +
                                       ((4982.8 - 21.3 * time) * time + dl) / 1296.0e3);
      r = 30.072984 + (0.001234 + 0.000003 * time) * time + dr * 1.0e-5;
      b = (54.77 + (0.26 + 0.06 * time) * time + db) / MoreMath.ARCS;
    
      return new Vec3D(new Polar(l, b, r));  // Ort   
   }

   /**
    * Calculate the position of pluto with analytic series expansion.
    *
    * @param time  time in julianish century.
    *
    * @return heliocentrically place vector in [AE], ecliptic and
    *         equinox of the date
    */
   public Vec3D plutoPos(double time) {

      double  m5,m6,m9;                // Mittlere Anomalien
      double  phi,c,s;
      double  dl, dr, db;              // Korrekturen in Laenge ["],
                                       // Entfernung [AE] und Breite ["]
      double  l,b,r;                   // Ekliptikale Koordinaten
    
      // Mittlere Anomalien der Planeten in [rad]
      m5 = MoreMath.PI2 * MoreMath.frac( 0.0565314 + 8.4302963 * time);
      m6 = MoreMath.PI2 * MoreMath.frac( 0.8829867 + 3.3947688 * time);
      m9 = MoreMath.PI2 * MoreMath.frac( 0.0385795 + 0.4026667 * time);
    
      // Stoerungen durch Jupiter
      pert.init(time, m9, 0, 6, m5, -2, 1);
    
      pert.term( 1, 0,0,   0.06,100924.08,-960396.0,15965.1,51987.68,-24288.76);
      pert.term( 2, 0,0,3274.74, 17835.12,-118252.2, 3632.4,12687.49, -6049.72);
      pert.term( 3, 0,0,1543.52,  4631.99, -21446.6, 1167.0, 3504.00, -1853.10);
      pert.term( 4, 0,0, 688.99,  1227.08,  -4823.4,  213.5, 1048.19,  -648.26);
      pert.term( 5, 0,0, 242.27,   415.93,  -1075.4,  140.6,  302.33,  -209.76);
      pert.term( 6, 0,0, 138.41,   110.91,   -308.8,  -55.3,  109.52,   -93.82);
      pert.term( 3,-1,0,  -0.99,     5.06,    -25.6,   19.8,    1.26,    -1.96);
      pert.term( 2,-1,0,   7.15,     5.61,    -96.7,   57.2,    1.64,    -2.16);
      pert.term( 1,-1,0,  10.79,    23.13,   -390.4,  236.4,   -0.33,     0.86);
      pert.term( 0, 1,0,  -0.23,     4.43,    102.8,   63.2,    3.15,     0.34);
      pert.term( 1, 1,0,  -1.10,    -0.92,     11.8,   -2.3,    0.43,     0.14);
      pert.term( 2, 1,0,   0.62,     0.84,      2.3,    0.7,    0.05,    -0.04);
      pert.term( 3, 1,0,  -0.38,    -0.45,      1.2,   -0.8,    0.04,     0.05);
      pert.term( 4, 1,0,   0.17,     0.25,      0.0,    0.2,   -0.01,    -0.01);
      pert.term( 3,-2,0,   0.06,     0.07,     -0.6,    0.3,    0.03,    -0.03);
      pert.term( 2,-2,0,   0.13,     0.20,     -2.2,    1.5,    0.03,    -0.07);
      pert.term( 1,-2,0,   0.32,     0.49,     -9.4,    5.7,   -0.01,     0.03);
      pert.term( 0,-2,0,  -0.04,    -0.07,      2.6,   -1.5,    0.07,    -0.02);
    
      dl = pert.dl();  dr = pert.dr();  db = pert.db();
    
      // Stoerungen durch Saturn
      pert.init(time, m9, 0, 3, m6, -2, 1);
    
      pert.term( 1,-1,0, -29.47,    75.97,   -106.4, -204.9,  -40.71,   -17.55);
      pert.term( 0, 1,0, -13.88,    18.20,     42.6,  -46.1,    1.13,     0.43);
      pert.term( 1, 1,0,   5.81,   -23.48,     15.0,   -6.8,   -7.48,     3.07);
      pert.term( 2, 1,0, -10.27,    14.16,     -7.9,    0.4,    2.43,    -0.09);
      pert.term( 3, 1,0,   6.86,   -10.66,      7.3,   -0.3,   -2.25,     0.69);
      pert.term( 2,-2,0,   4.32,     2.00,      0.0,   -2.2,   -0.24,     0.12);
      pert.term( 1,-2,0,  -5.04,    -0.83,     -9.2,   -3.1,    0.79,    -0.24);
      pert.term( 0,-2,0,   4.25,     2.48,     -5.9,   -3.3,    0.58,     0.02);
    
      dl += pert.dl();  dr += pert.dr();  db += pert.db();
    
      // Stoerungen durch Jupiter und Saturn
      phi = (m5 - m6);
      c   = StrictMath.cos(phi);
      s   = StrictMath.sin(phi);
      dl += -9.11 * c + 0.12 * s;
      dr += -3.40 * c - 3.30 * s;
      db += +0.81 * c + 0.78 * s;
    
      phi = (m5 - m6 + m9);
      c   = StrictMath.cos(phi);
      s   = StrictMath.sin(phi);
      dl += +5.92 * c + 0.25 * s;
      dr += +2.30 * c - 3.80 * s;
      db += -0.67 * c - 0.51 * s;
    
      // Ekliptikale Koordinaten ([rad],[AE])
      l = MoreMath.PI2 * MoreMath.frac(0.6232469 + m9 / MoreMath.PI2 + dl / 1296.0E3);
      r = 40.7247248 + dr * 1.0E-5;
      b = -3.909434 * MoreMath.RAD + db / MoreMath.ARCS;
    
      // Ort; Ekliptik und Aequinoktium B1950.0
      Vec3D rvec = new Vec3D(new Polar(l, b, r));
      
      PrecNut precNut = new PrecNut();
    
      // Ort; Ekliptik und Aequinoktium des Datums
      rvec.mul(precNut.precMatrix_Ecl(AstronomicalConstants.T_B1950, time));
      
      return rvec;
   }

   /**
    * Calculate the position of any planet with analytic series expansion.
    *
    * @param planet  the current planet number for identification.
    * @param time  time in julianish century.
    *
    * @return heliocentrically place vector in [AE], ecliptic and
    *         equinox of the date
    */
   public Vec3D pertPosition(int planet, double time) {

      Vec3D r;
    
      switch (planet) {
        case SSS3dConstants.SUN:      r = new Vec3D();       break;
        case SSS3dConstants.MERCURY:  r = mercuryPos(time);  break;
        case SSS3dConstants.VENUS:    r = venusPos(time);    break;
        case SSS3dConstants.EARTH:    r = earthPos(time);    break;
        case SSS3dConstants.MARS:     r = marsPos(time);     break;
        case SSS3dConstants.JUPITER:  r = jupiterPos(time);  break;
        case SSS3dConstants.SATURN:   r = saturnPos(time);   break;
        case SSS3dConstants.URANUS:   r = uranusPos(time);   break;
        case SSS3dConstants.NEPTUNE:  r = neptunePos(time);  break;
        case SSS3dConstants.PLUTO:    r = plutoPos(time);    break;
        default:                      r = null;
                                      System.out.println("Error in Planets");
      }
      return r;
   }
}
