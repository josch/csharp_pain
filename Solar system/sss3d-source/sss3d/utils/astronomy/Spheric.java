/*
  File: Spheric.java

  University of Applied Science Berne,HTA-Biel/Bienne,
  Computer Science Department.

  Diploma thesis J3D Solar System Simulator
  Originally written by Marcel Portner & Bernhard Hari (c) 2000
  
  CVS - Information :
  
  $Header: /var/cvsreps/projects/c450/2000/sss3d/source_diploma/sss3d/utils/astronomy/Spheric.java,v 1.3 2000/12/13 13:42:49 portm Exp $
  $Author: portm $
  $Date: 2000/12/13 13:42:49 $
  $State: Exp $
  
*/
package sss3d.utils.astronomy;

import sss3d.calculations.*;
import sss3d.calculations.constants.AstronomicalConstants;

import javax.vecmath.Matrix3d;

/**
 * This class calculate the transformation of the coordinate.
 *
 * @author Marcel Portner & Bernhard Hari
 * @version $Revision: 1.3 $
 */
public class Spheric {

   /**
    * Constructor
    */
   public Spheric() {
   
   }

   /**
    * Transformation the equatorial coordinates in ecliptical coordinates.
    *
    * @param time  time in julianish century.
    *
    * @return the transformation matrix.
    */
   public Matrix3d equ2EclMatrix(double time) {
      double eps = (23.43929111 - (46.8150 + (0.00059 - 0.001813 * time) * time) *
                    time / 3600.0) * MoreMath.RAD;
      Matrix3d mat = new Matrix3d();
      mat.setIdentity();
      mat.rotX(eps);
      return mat;
   }
   
   /**
    * Transformation the ecliptical coordinates in equatorial coordinates.
    *
    * @param time  time in julianish century.
    *
    * @return the transformation matrix.
    */
   public Matrix3d ecl2EquMatrix(double time) {
      Matrix3d matrix = equ2EclMatrix(time);
      matrix.transpose();
      return matrix;
   }
   
   /**
    * Transformation equatorial coordinates to horizon system.
    *
    * @param dec  the declension in [rad].
    * @param tau  the hour angle in [rad].
    * @param lat  the latitude of the observer in [rad].
    *
    * @return an array with the values:<br>
    *         double[0] = elevation in [rad].<br>
    *         double[1] = azimuth in [rad].
    */
   public double[] equ2Hor(double dec, double tau, double lat) {
      // Einheitsvektor im Horizontsystem
      Vec3D e_equ = new Vec3D(new Polar(tau, dec));
      Matrix3d mat = new Matrix3d();
      mat.setIdentity();
      mat.rotY(StrictMath.PI / 2.0 - lat);
      e_equ.mul(mat);     // Einheitsvektor im aequator. System

      double[] result = new double[2];

      result[0] = e_equ.getElevation();      
      result[1] = e_equ.getAzimut();
      return result;
   }
   
   /**
    * Transformation horizon system to equatorial coordinates.
    *
    * @param h  the elevation in [rad].
    * @param az  the azimuth in [rad].
    * @param lat  the latitude of the observer in [rad].
    *
    * @return an array with the values:<br>
    *         double[0] = declension in [rad].<br>
    *         double[1] = hour angle in [rad].
    */
   public double[] hor2Equ(double h, double az, double lat) {
      // Einheitsvektor im Horizontsystem
      Vec3D e_hor = new Vec3D(new Polar(az, h));
      Matrix3d mat = new Matrix3d();
      mat.setIdentity();
      mat.rotY(-(StrictMath.PI / 2.0 - lat));
     
      e_hor.mul(mat);      // Einheitsvektor im aequator. System
   
      double[] result = new double[2];

      result[0] = e_hor.getElevation();      
      result[1] = e_hor.getAzimut();
      return result;
   }
   
   /**
    * Calculate the geocentrically position of a place on the earth's surface.
    *
    * @param lambda  the geocentrically longitude (eastward positiv) in [rad].
    * @param phi  the geocentrically latitude in [rad].
    *
    * @return the geocentrically position in [km].
    */
   public Vec3D site(double lambda, double phi) {
      double f       = 1.0 / 298.257;        // Abplattung der Erde
      double e_sqr   = f * (2.0 - f);        // Quadrat der Exzentrizitaet
      double cos_phi = StrictMath.cos(phi);  // (Ko)sinus der geographischen Breite
      double sin_phi = StrictMath.sin(phi);
 
      double n = AstronomicalConstants.R_EARTH / StrictMath.sqrt(1.0 -
                                                                 e_sqr * (sin_phi * sin_phi));
    
      // Kartesischer Ortsvektor [km]
      return new Vec3D(n * cos_phi * StrictMath.cos(lambda),
                       n * cos_phi * StrictMath.sin(lambda),
                       (1.0 - e_sqr) * n * sin_phi);
   }
   
   /**
    * Calculation from equatorial coordinates to standart coordinates.
    *
    * @param ra0  the right ascension of the optic axis in [rad].
    * @param dec0  the declension of the optic axis in [rad].
    * @param x  the standart coordinate x.
    * @param y  the standart coordinate y.
    *
    * @return an array with the values:<br>
    *         double[0] = the right ascension in [rad].<br>
    *         double[1] = the declension in [rad].
    */
   public double[] stdEqu(double ra0, double dec0, double x, double y) {
      double[] result = new double[2];
      
      result[0] = ra0 + StrictMath.atan(-x / (StrictMath.cos(dec0) - y * StrictMath.sin(dec0)));
      result[1] = StrictMath.asin((StrictMath.sin(dec0) + y * StrictMath.cos(dec0)) /
                                  StrictMath.sqrt(1.0 + x * x + y * y));
      return result;
   }
   
   /**
    * Calculation from standart coordinates to equatorial coordinates.
    *
    * @param ra0  the right ascension of the optic axis in [rad].
    * @param dec0  the declension of the optic axis in [rad].
    * @param ra  the right ascension in [rad].
    * @param dec the declension in [rad].
    *
    * @return an array with the values:<br>
    *         double[0] = the standart coordinate x.<br>
    *         double[1] = the standart coordinate y.
    */
   public double[] equStd(double ra0, double dec0, double ra, double dec) {
      double[] result = new double[2];
      double c = StrictMath.cos(dec0) * StrictMath.cos(dec) * StrictMath.cos(ra - ra0) +
                 StrictMath.sin(dec0) * StrictMath.sin(dec);
   
      result[0] = -(StrictMath.cos(dec) * StrictMath.sin(ra - ra0)) / c;
      result[1] = -(StrictMath.sin(dec0) * StrictMath.cos(dec) * StrictMath.cos(ra - ra0) -
                    StrictMath.cos(dec0) * StrictMath.sin(dec)) / c;
      return result;
   }
}