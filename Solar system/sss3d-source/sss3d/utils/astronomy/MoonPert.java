/*
  File: MoonPert.java

  University of Applied Science Berne,HTA-Biel/Bienne,
  Computer Science Department.

  Diploma thesis J3D Solar System Simulator
  Originally written by Marcel Portner & Bernhard Hari (c) 2000
  
  CVS - Information :
  
  $Header: /var/cvsreps/projects/c450/2000/sss3d/source_diploma/sss3d/utils/astronomy/MoonPert.java,v 1.3 2000/12/13 13:42:48 portm Exp $
  $Author: portm $
  $Date: 2000/12/13 13:42:48 $
  $State: Exp $
  
*/
package sss3d.utils.astronomy;

import sss3d.calculations.*;
import sss3d.calculations.constants.AstronomicalConstants;

/**
 * This class add the permutation of the moon.
 *
 * @author Marcel Portner & Bernhard Hari
 * @version $Revision: 1.3 $
 */
public class MoonPert {
 
   private static final int O   = 6;          // Index-Versatz
   private static final int DIM = 2 * O + 1;  // Dimension eines Arbeits-Feldes

   private double dgam;                       // Langperiodische Stoerung
   private double dlam, ds, gam1C, sinPi, n;  // Periodische Stoerungen
   private double l0, l, ls, f, d;            // Mittlere Argumente der Mondbahn
   private double[][] mCos, mSin;             // cos und sin d. mittleren Argumente
   private double[] sincos;

   /**
    * Constructor
    */
   public MoonPert() {
      mCos = new double[DIM][4];
      mSin = new double[DIM][4];
      sincos = new double[2];
   }
   
   /**
    * Initialization with the current time.
    *
    * @param time  time in julianish century.
    */
   public void init(double time) {

      double dL0, dl, dls, dF, dD;             // Langperiodische Stoerungen
      double t2, arg, fac;                     // Hilfsgroessen
      int    max;
    
      t2 = time * time; // Zeit
  
      // Stoerungen auf Null setzen
      dlam = 0.0;
      ds = 0.0;
      gam1C = 0.0;
      sinPi = 3422.7000;
      n = 0.0;
  
      // Langperiodische Stoerungen
      double s1 = sine(0.19833 + 0.05611 * time);
      double s2 = sine(0.27869 + 0.04508 * time);
      double s3 = sine(0.16827 - 0.36903 * time);
      double s4 = sine(0.34734 - 5.37261 * time);
      double s5 = sine(0.10498 - 5.37899 * time);
      double s6 = sine(0.42681 - 0.41855 * time);
      double s7 = sine(0.14943 - 5.37511 * time); 
  
      dL0 = 0.84 * s1 + 0.31 * s2 + 14.27 * s3 + 7.26 * s4 + 0.28 * s5 + 0.24 * s6;
      dl  = 2.94 * s1 + 0.31 * s2 + 14.27 * s3 + 9.34 * s4 + 1.12 * s5 + 0.83 * s6;
      dls =-6.40 * s1                                                  - 1.89 * s6;
      dF  = 0.21 * s1 + 0.31 * s2 + 14.27 * s3 - 88.70 * s4 - 15.30 * s5 +
            0.24 * s6 - 1.86 * s7;
      dD  = dL0 - dls;
  
      dgam = -3332e-9 * sine(0.59734-5.37261 * time)
             - 539e-9 * sine(0.35498-5.37899 * time)
              - 64e-9 * sine(0.39943-5.37511 * time);
  
      // Mittlere Argumente der Mondbahn (inkl. langperiodische Korrekturen)
      // L0 mittlere Laenge des Mondes
      // l  mittlere Anomalie des Mondes  l' mittlere Anomalie der Sonne      
      // F  mittlerer Knotenabstand       D  mittlere Elongation von der Sonne
  
      l0 = MoreMath.PI2 * MoreMath.frac(0.60643382 + 1336.85522467 * time -
                                        0.00000313 * t2) + dL0 / MoreMath.ARCS;
      l  = MoreMath.PI2 * MoreMath.frac(0.37489701 + 1325.55240982 * time +
                                        0.00002565 * t2) + dl  / MoreMath.ARCS;
      ls = MoreMath.PI2 * MoreMath.frac(0.99312619 +   99.99735956 * time -
                                        0.00000044 * t2) + dls / MoreMath.ARCS;
      f  = MoreMath.PI2 * MoreMath.frac(0.25909118 + 1342.22782980 * time -
                                        0.00000892 * t2) + dF  / MoreMath.ARCS;
      d  = MoreMath.PI2 * MoreMath.frac(0.82736186 + 1236.85308708 * time -
                                        0.00000397 * t2) + dD  / MoreMath.ARCS;
  
    
      // Kosinus und Sinus von Vielfachen der mittleren Argumente 
      // einschliesslich saekularer Korrekturen
      for(int i = 0; i <= 3; i++) {
         switch(i) {      
           case 0:  arg=l;  max=4; fac=1.000002208;                      break;
           case 1:  arg=ls; max=3; fac=0.997504612 - 0.002495388 * time; break;
           case 2:  arg=f;  max=4; fac=1.000002708 + 139.978 * dgam;     break;
           case 3:  arg=d;  max=6; fac=1.0;                              break;
           default: arg=0.0;max=0; fac=0;
         }
        
         mCos[O][i] = 1.0;
         mSin[O][i] = 0.0;
         
         mCos[O+1][i] = StrictMath.cos(arg) * fac;
         mSin[O+1][i] = StrictMath.sin(arg) * fac;
         
         mCos[O-1][i] = +mCos[O+1][i];
         mSin[O-1][i] = -mSin[O+1][i];
         
         for(int j = 2; j <= max; j++) {
            sincos = MoreMath.addThe(mCos[O+j-1][i],
                                     mSin[O+j-1][i],
                                     mCos[O+1][i],
                                     mSin[O+1][i]);
            mCos[O+j][i] = sincos[0];
            mSin[O+j][i] = sincos[1];
            mCos[O-j][i] = +mCos[O+j][i];
            mSin[O-j][i] = -mSin[O+j][i];
         }
      }
   }
 
   /**
    * Calculate the sinus and cosinus.
    *
    * @param p  an int value.
    * @param q  an int value.
    * @param r  an int value.
    * @param s  an int value.
    *
    * @return a double array with the values:<br>
    *         double[0] = cos(p*l+q*ls+r*F+s*D).<br>
    *         double[1] = sin(p*l+q*ls+r*F+s*D).
    */
   public double[] term(int p, int q, int r, int s) {

      int[] i = new int[4];
  
      i[0] = p;
      i[1] = q;
      i[2] = r;
      i[3] = s;
      
      sincos[0] = 1.0;
      sincos[1] = 0.0;
  
      for(int k = 0; k < i.length; k++) {
         if(i[k] != 0) {
            sincos = MoreMath.addThe(sincos[0],
                                     sincos[1],
                                     mCos[O+i[k]][k],
                                     mSin[O+i[k]][k]);
         }
      }
      return sincos;
   }
 
   /**
    * Summation of solar interference term.
    *
    * @param coeffl  an double value.
    * @param coeffS  an double value.
    * @param coeffg  an double value.
    * @param coeffP  an double value.
    * @param p  an int value.
    * @param q  an int value.
    * @param r  an int value.
    * @param s  an int value.
    */
   public void addSol(double coeffl, double coeffS, double coeffg, double coeffP,
                      int p, int q, int r, int s) {
      sincos = term(p, q, r, s);
      dlam  += coeffl * sincos[1];
      ds    += coeffS * sincos[1];
      gam1C += coeffg * sincos[0];
      sinPi += coeffP * sincos[0];
   }
 
   /**
    * Summation of interference in the latitude.
    *
    * @param coeffN  an double value.
    * @param p  an int value.
    * @param q  an int value.
    * @param r  an int value.
    * @param s  an int value.
    */
   public void addN(double coeffN, int p, int q, int r, int s) {
      sincos = term(p, q, r, s); 
      n += coeffN * sincos[1];
   }
 
   /**
    * Interference in the ecliptical latitude by the venus and jupiter.
    *
    * @param time  time in julianish century.
    */
   public void planetary(double time) {
      dlam += 0.82 * sine(0.7736 -   62.5512 * time) +
              0.31 * sine(0.0466 -  125.1025 * time) +
              0.35 * sine(0.5785 -   25.1042 * time) +
              0.66 * sine(0.4591 + 1335.8075 * time) +
              0.64 * sine(0.3130 -   91.5680 * time) +
              1.14 * sine(0.1480 + 1331.2898 * time) +
              0.21 * sine(0.5918 + 1056.5859 * time) +
              0.44 * sine(0.5784 + 1322.8595 * time) +
              0.24 * sine(0.2275 -    5.7374 * time) +
              0.28 * sine(0.2965 +    2.6929 * time) +
              0.33 * sine(0.3132 +    6.3368 * time);
   }  
 
   /**
    * Give the current lambda angle of the moon positon.
    *
    * @return the lambda value.
    */
   public double lambda() {
      return MoreMath.modulo(l0 + dlam / MoreMath.ARCS, MoreMath.PI2); 
   }
 
   /**
    * Give the current beta angle of the moon position.
    *
    * @return the lambda value.
    */
   public double beta() {
      double s   = f + ds / MoreMath.ARCS;
      double fac = 1.000002708 + 139.978 * dgam;
      return (fac * (18518.511 + 1.189 + gam1C) * StrictMath.sin(s) -
              6.24 * StrictMath.sin(3 * s) + n) / MoreMath.ARCS;
   }
 
   /**
    * Give the current distance of the moon.
    *
    * @return the distance value.
    */
   public double dist() {
      return AstronomicalConstants.R_EARTH * MoreMath.ARCS / (sinPi * 0.999953253);
   }
   
   /**
    * Calculate the sinus of a modified x.
    *
    * @return the result of this arithmetic:<br>
    *         sine = sin(2 * PI * frac(x)).
    */
   private double sine(double x) {
      return StrictMath.sin(MoreMath.PI2 * MoreMath.frac(x));
   }
}