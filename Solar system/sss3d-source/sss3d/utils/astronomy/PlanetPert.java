/*
  File: PlanetPert.java

  University of Applied Science Berne,HTA-Biel/Bienne,
  Computer Science Department.

  Diploma thesis J3D Solar System Simulator
  Originally written by Marcel Portner & Bernhard Hari (c) 2000
  
  CVS - Information :
  
  $Header: /var/cvsreps/projects/c450/2000/sss3d/source_diploma/sss3d/utils/astronomy/PlanetPert.java,v 1.2 2000/12/12 16:00:25 portm Exp $
  $Author: portm $
  $Date: 2000/12/12 16:00:25 $
  $State: Exp $
  
*/
package sss3d.utils.astronomy;

import sss3d.calculations.*;

/**
 * This class add the permutation of all planets.
 *
 * @author Marcel Portner & Bernhard Hari
 * @version $Revision: 1.2 $
 */
public class PlanetPert {

   private static final int O   = 16;         // Index-Versatz
   private static final int DIM = 2 * O + 1;  // Dimension eines Arbeits-Feldes
 
   private double   m_T;
   private double   m_cosM, m_sinM;
   private double   m_dl, m_db, m_dr;
   private double   m_u, m_v;
   private double[] m_C, m_S, m_c, m_s;
 
   private double[] sincos;
 
   /**
    * Constructor
    */
   public PlanetPert() {
      m_C = new double[DIM];
      m_S = new double[DIM];
      m_c = new double[DIM];
      m_s = new double[DIM];
      sincos =  new double[2];
   }
 
   /**
    * Initialization with the current time, the middle anomaly
    * and the index zone.
    *
    * @param time  time in julianish century.
    * @param _M  a double value.
    * @param _I_min  a int value.
    * @param _I_max  a int value.
    * @param _m  a double value.
    * @param _i_min  a int value.
    * @param _i_max  a int value.
    */
   public void init(double time,
                    double _M, int _I_min, int _I_max,
                    double _m, int _i_min, int _i_max) {
      init(time, _M, _I_min, _I_max, _m, _i_min, _i_max, 0.0);
   }
      
   /**
    * Initialization with the current time, the middle anomaly
    * and the index zone.
    *
    * @param time  time in julianish century.
    * @param _M  a double value.
    * @param _I_min  a int value.
    * @param _I_max  a int value.
    * @param _m  a double value.
    * @param _i_min  a int value.
    * @param _i_max  a int value.
    * @param phi  a double value.
    */
   public void init(double time,
                    double _M, int _I_min, int _I_max,
                    double _m, int _i_min, int _i_max,
                    double phi) {
 
      m_dl = 0.0; m_dr = 0.0; m_db = 0.0; // Stoerungen auf Null setzen
      m_T = time;                         // Setze Zeit
 
      // Cosinus und Sinus von Vielfachen von _M
      m_cosM = StrictMath.cos(_M);
      m_sinM = StrictMath.sin(_M);
 
      m_C[O] = StrictMath.cos(phi);
      m_S[O] = StrictMath.sin(phi);
 
      for(int i = 0; i < _I_max; i++) {
         sincos = MoreMath.addThe(m_C[O+i], m_S[O+i], +m_cosM, +m_sinM);
         m_C[O+i+1] = sincos[0];
         m_S[O+i+1] = sincos[1];
      }
      for(int i = 0; i > _I_min; i--) {
         sincos = MoreMath.addThe(m_C[O+i], m_S[O+i], +m_cosM, -m_sinM);
         m_C[O+i-1] = sincos[0];
         m_S[O+i-1] = sincos[1];
      }
 
      // Cosinus und Sinus von Vielfachen von m
      m_c[O] = 1.0;
      m_s[O] = 0.0;
      
      m_c[O+1] = StrictMath.cos(_m);
      m_s[O+1] = StrictMath.sin(_m);
      
      m_c[O-1] = +m_c[O+1];
      m_s[O-1] = -m_s[O+1];
      
      for(int i = 1; i < _i_max; i++) {
         sincos = MoreMath.addThe(m_c[O+i], m_s[O+i], m_c[O+1], m_s[O+1]);
         m_c[O+i+1] = sincos[0];
         m_s[O+i+1] = sincos[1];
      }
      for(int i = -1; i > _i_min; i--) {
         sincos = MoreMath.addThe(m_c[O+i], m_s[O+i], m_c[O-1], m_s[O-1]);
         m_c[O+i-1] = sincos[0];
         m_s[O+i-1] = sincos[1];
      }
   }
 
   /**
    * Summation of the interference in longitude, distance and latitude.
    *
    * @param _I  a int value.
    * @param i  a int value.
    * @param iT  a int value.
    * @param dlc  a double value.
    * @param dls  a double value.
    * @param drc  a double value.
    * @param drs  a double value.
    * @param dbc  a double value.
    * @param dbs  a double value.
    */
   public void term(int _I, int i, int iT,
                    double dlc, double dls,
                    double drc, double drs,
                    double dbc, double dbs) {
 
      if(iT == 0) {
         sincos = MoreMath.addThe(m_C[O+_I], m_S[O+_I], m_c[O+i], m_s[O+i]);
         m_u = sincos[0];
         m_v = sincos[1];
      } else {
         m_u *= m_T;
         m_v *= m_T;
      }
 
      m_dl += dlc * m_u + dls * m_v;
      m_dr += drc * m_u + drs * m_v;
      m_db += dbc * m_u + dbs * m_v;
   }
 
   /**
    * Give the current interference longitude.
    *
    * @return the longitude value.
    */
   public double dl() {
      return m_dl;
   }
 
   /**
    * Give the current interference distance.
    *
    * @return the distance value.
    */
   public double dr() {
      return m_dr;
   }
 
   /**
    * Give the current interference latitude.
    *
    * @return the latitude value.
    */
   public double db() {
      return m_db;
   }
}
