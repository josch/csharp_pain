/*
  File: PrecNut.java

  University of Applied Science Berne,HTA-Biel/Bienne,
  Computer Science Department.

  Diploma thesis J3D Solar System Simulator
  Originally written by Marcel Portner & Bernhard Hari (c) 2000
  
  CVS - Information :
  
  $Header: /var/cvsreps/projects/c450/2000/sss3d/source_diploma/sss3d/utils/astronomy/PrecNut.java,v 1.3 2000/12/12 16:00:25 portm Exp $
  $Author: portm $
  $Date: 2000/12/12 16:00:25 $
  $State: Exp $
  
*/
package sss3d.utils.astronomy;

import sss3d.calculations.*;

import javax.vecmath.Matrix3d;

/**
 * This class calculate the precession and nutation.
 *
 * @author Marcel Portner & Bernhard Hari
 * @version $Revision: 1.3 $
 */
public class PrecNut {

   private Matrix3d mat1;
   private Matrix3d mat2;
   private Matrix3d mat3;

   /**
    * Constructor
    */
   public PrecNut() {
      mat1 = new Matrix3d();
      mat2 = new Matrix3d();
      mat3 = new Matrix3d();
   }

   /**
    * Calculate the precession of ecliptical coordinates.
    *
    * @param t1  given epoch in julianish century.
    * @param t2  epoch, to be calculated.
    *
    * @return a transform matrix for precession.
    */
   public Matrix3d precMatrix_Ecl(double t1, double t2) {

      double dT = t2 - t1;
      double _Pi, pi, p_a;
    
      _Pi = 174.876383889 * MoreMath.RAD + 
            (((3289.4789 + 0.60622 * t1) * t1) +
             ((-869.8089 - 0.50491 * t1) + 0.03536 * dT) * dT) / MoreMath.ARCS;
      pi  = ((47.0029 - (0.06603 - 0.000598 * t1) * t1) +
             ((-0.03302 + 0.000598 * t1) + 0.000060 * dT) * dT) * dT / MoreMath.ARCS;
      p_a = ((5029.0966 + (2.22226 - 0.000042 * t1) * t1) +
             ((1.11113 - 0.000042 * t1) - 0.000006 * dT) * dT) * dT / MoreMath.ARCS;
      
      mat1.setIdentity();
      mat2.setIdentity();
      mat3.setIdentity();
      
      // R_z(-(_Pi+p_a)) * R_x(pi) * R_z(_Pi)
      mat1.rotZ(-(_Pi + p_a));
      mat2.rotX(pi);
      mat3.rotZ(_Pi);
      
      mat3.mul(mat2);
      mat3.mul(mat1);
      
      return mat3;
   }    
   
   /**
    * Calculate the precession of equatorial coordinates.
    *
    * @param t1  given epoch in julianish century.
    * @param t2  epoch, to be calculated.
    *
    * @return a transform matrix for precession.
    */
   public Matrix3d precMatrix_Equ(double t1, double t2) {

      double dT = t2-t1;
      double zeta, z, theta;
 
      zeta  = ((2306.2181 + (1.39656 - 0.000139 * t1) * t1) +
               ((0.30188 - 0.000344 * t1) + 0.017998 * dT) * dT) * dT / MoreMath.ARCS;
      z     = zeta + ((0.79280 + 0.000411 * t1) + 0.000205 * dT) * dT * dT / MoreMath.ARCS;
      theta = ((2004.3109 - (0.85330 + 0.000217 * t1) * t1) -
               ((0.42665 + 0.000217 * t1) + 0.041833 * dT) * dT) * dT / MoreMath.ARCS;
 
      mat1.setIdentity();
      mat2.setIdentity();
      mat3.setIdentity();
      
      // R_z(-z) * R_y(theta) * R_z(-zeta)
      mat1.rotZ(-z);
      mat2.rotY(theta);
      mat3.rotZ(-zeta);
      
      mat3.mul(mat2);
      mat3.mul(mat1);
      
      return mat3;
   }    
   
   /**
    * Transformation from middle equator and spring point to the real equator and
    * spring point.
    *
    * @param time  time in julianish century.
    *
    * @return the nutation matrix.
    */
   public Matrix3d nutMatrix(double time) {

      double ls = MoreMath.PI2 *
                  MoreMath.frac(0.993133 +   99.997306 * time);   // Mittlere Anomalie der Sonne
      double d  = MoreMath.PI2 *
                  MoreMath.frac(0.827362 + 1236.853087 * time);   // Laengendifferenz Sonne-Mond
      double f  = MoreMath.PI2 *
                  MoreMath.frac(0.259089 + 1342.227826 * time);   // Mittleres Argument der Breite
      double n  = MoreMath.PI2 *
                  MoreMath.frac(0.347346 -    5.372447 * time);   // Laenge des aufsteigenden Knotens
    
      double dpsi = (-17.200 * StrictMath.sin(n)   - 1.319 * StrictMath.sin(2 * (f - d + n)) -
                     0.227 * StrictMath.sin(2 * (f + n)) + 0.206 * StrictMath.sin(2 * n) +
                     0.143 * StrictMath.sin(ls)) / MoreMath.ARCS;
      double deps = (9.203 * StrictMath.cos(n)    + 0.574 * StrictMath.cos(2 * (f - d + n)) +
                     0.098 * StrictMath.cos(2 * (f + n)) - 0.090 * StrictMath.cos(2 * n)) /
                    MoreMath.ARCS;
    
      double eps  = 0.4090928 - 2.2696E-4 * time;            // Mittlere Schiefe der Ekliptik
    
      mat1.setIdentity();
      mat2.setIdentity();
      mat3.setIdentity();
      
      // R_x(-eps-deps)*R_z(-dpsi)*R_x(+eps)
      mat1.rotX(-eps - deps);
      mat2.rotZ(-dpsi);
      mat3.rotX(+eps);
      
      mat3.mul(mat2);
      mat3.mul(mat1);
      
      return mat3;
   }
}