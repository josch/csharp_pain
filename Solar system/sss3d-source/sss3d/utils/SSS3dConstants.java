/*
  File: SSS3dConstants.java

  University of Applied Science Berne,HTA-Biel/Bienne,
  Computer Science Department.

  Diploma thesis J3D Solar System Simulator
  Originally written by Marcel Portner & Bernhard Hari (c) 2000
  
  CVS - Information :
  
  $Header: /var/cvsreps/projects/c450/2000/sss3d/source_diploma/sss3d/utils/SSS3dConstants.java,v 1.15 2000/12/12 13:01:38 harib Exp $
  $Author: harib $
  $Date: 2000/12/12 13:01:38 $
  $State: Exp $
  
*/
package sss3d.utils;


/**
 * This class contains constants for the 
 * J3D Solar System Simulator.
 *
 * <pre>
 *    use : 
 *    SSS3dConstants.TYPES[i];
 *    ...
 * </pre> 
 *
 * @author Marcel Portner & Bernhard Hari
 * @version $Revision: 1.15 $
 */
public final class SSS3dConstants {

//   public static final float VISIBILITY = 1e6f;
   public static final double BOUNDRADIUS = 1e6;
   public static final double SCALE = 1e4;
   
   // backclip / frontclip = not bigger than 3000 !!! (16 bit Z-buffer)
   // 1e9 differenz bei 32 bit Z-buffer ?
   public static final double FRONTCLIP            = 1e-1; // in meter
   public static final double BACKCLIP             = 1e5;  // in meter
   public static final double FRONTCLIP_COMPRESSED = 1e-3; // in meter
   public static final double BACKCLIP_COMPRESSED  = 1e3;  // in meter
      
   // used to call functions if no parameter is defined
   public static final int UNDEFINED_PARAMETER = -1;

   // To distinguish if solar system simulator is
   // in initialization or runtime mode.
   // This is important to check if an attribute
   // can be set in the current mode.
   // In class XMLConstants we have defined an
   // array MODIFIABLE_VALUES_AT_RUNTIME.
   public static final int INITIALIZATION_MODE = 0;
   public static final int RUNTIME_MODE        = 1;

   // reload whole scene - used if date changed
   public static final int RELOAD              = -2;

   // types of celestial objects or initialization 
   // files
   public static final int UNKNOWN_TYPE         = 0;
   public static final int INI_TYPE             = 1;
   public static final int CELESTIALOBJECT_TYPE = 2;
   public static final int STAR_TYPE            = 3;
   public static final int PLANET_TYPE          = 4;
   public static final int MOON_TYPE            = 5;
   public static final int COMET_TYPE           = 6;
   public static final int SATELLITE_TYPE       = 7;
   public static final int ROCKET_TYPE          = 8;
   
   /**
    * The strings representing the types
    */
   public static final String[] TYPES = {
                                          "unknown",
                                          "ini",
                                          "celestialobject",
                                          "star",  
                                          "planet",
                                          "moon",
                                          "comet",
                                          "satellite",
                                       	  "rocket"
                                        };

   public static final int SUN          =  0;
   public static final int MERCURY      =  1;
   public static final int VENUS        =  2;
   public static final int EARTH        =  3;
   public static final int MARS         =  4;
   public static final int JUPITER      =  5;
   public static final int SATURN       =  6;
   public static final int URANUS       =  7;
   public static final int NEPTUNE      =  8;
   public static final int PLUTO        =  9;
   public static final int MOON         = 10;
/*
   public static final int PHOBOS       = 11;
   public static final int DEIMOS       = 12;
   public static final int IO           = 13;
   public static final int EUROPA       = 14;
   public static final int GANYMEDE     = 15;
   public static final int CALLISTO     = 16;
   public static final int AMALTHEA     = 17;
   public static final int HIMALIA      = 18;
   public static final int ELARA        = 19;
   public static final int PASIPHAE     = 20;
   public static final int SINOPE       = 21;
   public static final int LYSITHEA     = 22;
   public static final int CARME        = 23;
   public static final int ANANKE       = 24;
   public static final int LEDA         = 25;
   public static final int THEBE        = 26;
   public static final int ADRASTEA     = 27;
   public static final int METIS        = 28;
   public static final int MIMAS        = 29;
   public static final int ENCELADUS    = 30;
   public static final int TETHYS       = 31;
   public static final int DIONE        = 32;
   public static final int RHEA         = 33;
   public static final int TITAN        = 34;
   public static final int HYPERION     = 35;
   public static final int IAPETUS      = 36;
   public static final int PHOEBE       = 37;
   public static final int JANUS        = 38;
   public static final int EPIMETHEUS   = 39;
   public static final int HELENE       = 40;
   public static final int TELESTO      = 41;
   public static final int CALYPSO      = 42;
   public static final int ATLAS        = 43;
   public static final int PROMETHEUS   = 44;
   public static final int PANDORA      = 45;
   public static final int PAN          = 46;
   public static final int ARIEL        = 47;
   public static final int UMBRIEL      = 48;
   public static final int TITANIA      = 49;
   public static final int OBERON       = 50;
   public static final int MIRANDA      = 51;
   public static final int CORDELIA     = 52;
   public static final int OPHELIA      = 53;
   public static final int BIANCA       = 54;
   public static final int CRESSIDA     = 55;
   public static final int DESDEMONA    = 56;
   public static final int JULIET       = 57;
   public static final int PORTIA       = 58;
   public static final int ROSALIND     = 59;
   public static final int BELINDA      = 60;
   public static final int PUCK         = 61;
   public static final int CALIBAN      = 62;
   public static final int SYCORAX      = 63;
   public static final int PROSPERO     = 64;
   public static final int SETEBOS      = 65;
   public static final int STEPHANO     = 66;
   public static final int TRITON       = 67;
   public static final int NEREID       = 68;
   public static final int NAIAD        = 69;
   public static final int THALASSA     = 70;
   public static final int DESPINA      = 71;
   public static final int GALATEA      = 72;
   public static final int LARISSA      = 73;
   public static final int PROTEUS      = 74;
   public static final int CHARON       = 75;
*/
   /**
    * The strings representing the celestial object names
    */
   public static final String[] CELESTIAL = {
                                              "sun",
                                              "mercury",
                                              "venus",
                                              "earth",
                                              "mars",
                                              "jupiter",
                                              "saturn",
                                              "uranus",
                                              "neptune",
                                              "pluto",
                                              "moon"/*,
                                              "phobos",
                                              "deimos",
                                              "io",
                                              "europa",
                                              "ganymede",
                                              "callisto",
                                              "amalthea",
                                              "himalia",
                                              "elara",
                                              "pasiphae",
                                              "sinope",
                                              "lysithea",
                                              "carme",
                                              "ananke",
                                              "leda",
                                              "thebe",
                                              "adrastea",
                                              "metis",
                                              "mimas",
                                              "enceladus",
                                              "tethys",
                                              "dione",
                                              "rhea",
                                              "titan",
                                              "hyperion",
                                              "iapetus",
                                              "phoebe",
                                              "janus",
                                              "epimetheus",
                                              "helene",
                                              "telesto",
                                              "calypso",
                                              "atlas",
                                              "prometheus",
                                              "pandora",
                                              "pan",
                                              "ariel",
                                              "umbriel",
                                              "titania",
                                              "oberon",
                                              "miranda",
                                              "cordelia",
                                              "ophelia",
                                              "bianca",
                                              "cressida",
                                              "desdemona",
                                              "juliet",
                                              "portia",
                                              "rosalind",
                                              "belinda",
                                              "puck",
                                              "caliban",
                                              "sycorax",
                                              "prospero",
                                              "setebos",
                                              "stephano",
                                              "triton",
                                              "nereid",
                                              "naiad",
                                              "thalassa",
                                              "despina",
                                              "galatea",
                                              "larissa",
                                              "proteus",
                                              "charon"*/
                                            };

  public static final int KEPLER   = 0;
  public static final int ELLIPSE  = 1;
  public static final int ANALYTIC = 2;
  
  /**
   * The strings representing the calculation methods.
   */
  public static final String[] CALCULATION_METHODS =  {
                                                        "Kepler",
                                                        "Ellipse",
                                                        "Analytic"
                                                      };  
  
  public static final int CAMERA_DEFAULT      =  0;
  public static final int CAMERA_SUN          =  1;
  public static final int CAMERA_MERCURY      =  2;
  public static final int CAMERA_VENUS        =  3;
  public static final int CAMERA_EARTH        =  4;
  public static final int CAMERA_MARS         =  5;
  public static final int CAMERA_JUPITER      =  6;
  public static final int CAMERA_SATURN       =  7;
  public static final int CAMERA_URANUS       =  8;
  public static final int CAMERA_NEPTUNE      =  9;
  public static final int CAMERA_PLUTO        = 10;
  public static final int CAMERA_MOON         = 11;
  public static final int CAMERA_SPACESHUTTLE = 12;
  
  /**
   * The strings representing the camera positons
   */
  public static final String[] CAMERA_POSITIONS = {
                                                    "Default",
                                                    "Sun",
                                                    "Mercury",
                                                    "Venus",
                                                    "Earth",
                                                    "Mars",
                                                    "Jupiter",
                                                    "Saturn",
                                                    "Uranus",
                                                    "Neptune",
                                                    "Pluto",
                                                    "Moon",
                                                    "SpaceShuttle"
                                                  };
}
