/*
  File: InfoObserver.java

  University of Applied Science Berne,HTA-Biel/Bienne,
  Computer Science Department.

  Diploma thesis J3D Solar System Simulator
  Originally written by Marcel Portner & Bernhard Hari (c) 2000
  
  CVS - Information :
  
  $Header: /var/cvsreps/projects/c450/2000/sss3d/source_diploma/sss3d/utils/observer/InfoObserver.java,v 1.4 2000/12/13 13:42:03 portm Exp $
  $Author: portm $
  $Date: 2000/12/13 13:42:03 $
  $State: Exp $
  
*/
package sss3d.utils.observer;

import sss3d.contentbranch.*;

/**
 * The interface InfoObserver is part of the information observer - defined
 * by the observer pattern. 
 * References : E.Gamma; Design Patterns ISBN 0-201-63361-2, Addison Wesley, 1995<br>
 * It defines an updating interface for objects that should be notified of changes
 * in a ObjectInfo.
 * @author Marcel Portner & Bernhard Hari
 * @version $Revision: 1.4 $
 * @see ObjectInfo
 * @see ObjectsInformation
 */
public interface InfoObserver {
    /**
     * The update methode is used to keep the state of the concrete observer
     * consistent with the state of the ObjectInfo.<br>
     *
     * @param     objInfo a reference to the ObjectInfo
     * @param     id the id of the concrete object that has to be updated
     */ 	
    void update(String id, int parameter);
    
    /**
     * Returns the id of the concrete observer.
     *
     * @return    String the id of the concrete observer
     */     
    String getId();
    
    /**
     * Returns the information object from the concrete observer.
     *
     * @return    CelestialObjectInfo the info object from the concrete observer
     */     
    CelestialObjectInfo getInfo();
}
