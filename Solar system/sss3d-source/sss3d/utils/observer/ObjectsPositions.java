/*
  File: ObjectsPositions.java

  University of Applied Science Berne,HTA-Biel/Bienne,
  Computer Science Department.

  Diploma thesis J3D Solar System Simulator
  Originally written by Marcel Portner & Bernhard Hari (c) 2000
  
  CVS - Information :
  
  $Header: /var/cvsreps/projects/c450/2000/sss3d/source_diploma/sss3d/utils/observer/ObjectsPositions.java,v 1.7 2000/12/13 13:42:03 portm Exp $
  $Author: portm $
  $Date: 2000/12/13 13:42:03 $
  $State: Exp $
  
*/
package sss3d.utils.observer;

import sss3d.calculations.*;
import sss3d.calculations.ellipseequation.*;
import sss3d.calculations.keplerequation.*;
import sss3d.contentbranch.*;
import sss3d.utils.xmlparser.XMLConstants;
import sss3d.utils.SSS3dConstants;

import java.util.*;
import javax.vecmath.Point3f;

/**
 * The class ObjectsPositions is part of the positions observer - defined
 * by the observer pattern. 
 * References : E.Gamma; Design Patterns ISBN 0-201-63361-2, Addison Wesley, 1995<br>
 * It stores the state (positions) of the concrete observers and sends a notification to 
 * its concrete observers when its state changes. 
 * @author Marcel Portner & Bernhard Hari
 * @version $Revision: 1.7 $
 * @see OrbitPositions
 * @see PositionObserver
 */
public class ObjectsPositions extends OrbitPositions {

    //private OrbitCalculator oc;
    private KeplerEquation kepler;
    private EllipseEquation ellipse;
    private Vector positions;
    private InitializationObject iniObject;
    private int calculationMethod = SSS3dConstants.KEPLER;
    
    /**
     * Constructor.<br>
     * It calls the constructor of its parent class OrbitPositions and
     * instanciates an object EllipseEquation or KeplerEquation. Kepler equation is the 
     * used method per default to calculate the positions of the objects.<br>
     * It also instanciates a new list to hold the objects positions.
     */     
    public ObjectsPositions( InitializationObject iniObject ) {
    	super();
      this.iniObject = iniObject;
        
    	// we can use KeplerEquation or EllipseEquation
    	ellipse = new EllipseEquation();
    	kepler  = new KeplerEquation();
    	
      positions = new Vector();
    }

    /**
     * This method can be used to modify positions of a given object.
     * It's currently not in use, it's to be defined later.
     *
     * @param cObjInfo a CelestialObjectInfo witch holds 
     *                 the information about the object
     * @param jDay the new julian date used to calculate the new positions
     * @param compressed the object positions in compressed mode
     */  
    public void setPositions(CelestialObjectInfo cObjInfo, double jDay, boolean compressed) {
    }

    /**
     * Returns the positions of a given object. If the object doesn't exists, then
     * it calculates the new positions and add those to the positions list.
     *
     * @param    cObjInfo a CelestialObjectInfo witch holds 
     *                     the information about the object
     * @param    jDay the new julian date used to calculate the new positions     
     * @param compressed the object positions in compressed mode
     * @return   Point3f[] the positins corresponding to the given object
     */  
    public Point3f[] getPositions(CelestialObjectInfo cObjInfo, double jDay, boolean compressed) {
    	  String id = cObjInfo.getName();
    	    	
    	  PositionsArray pa = null;
        boolean found = false;
        
        // Check if there already exists the positions for the given
        // object. If it doesn't exists, then we have to calculate them.    	
    	  Enumeration e = positions.elements();
    	  while(e.hasMoreElements()) {
    	  pa = (PositionsArray)e.nextElement();
    	  
    	  if( (pa.getId()).equals(id) ) {
            // remove element
            positions.removeElement(pa);
     	    break;
    	  }
   	    }
    	
        calculationMethod = ((Integer)iniObject.getParameter(XMLConstants.CALCULATION)).intValue(); 

     	  // calculate positions using Kepler or Ellipse equation
	      if( calculationMethod == SSS3dConstants.ELLIPSE ) {
             //System.out.println("EllipseEquation");
             
             // at the moment only valid for planets
             if (cObjInfo.getTypeNbr() == SSS3dConstants.PLANET_TYPE ) {
               PositionsArray newPa = new PositionsArray(
                                              cObjInfo.getName(),
                                              ellipse.getPositions(cObjInfo,jDay,compressed));    	
               positions.addElement(newPa);   
               pa = newPa;      
               
             } else  {
               PositionsArray newPa = new PositionsArray(
                                              cObjInfo.getName(),
                                              kepler.getPositions(cObjInfo,jDay,compressed));    	
               positions.addElement(newPa);   
               pa = newPa;      
             } 
          
        } else {
             //System.out.println("KeplerEquation");
             PositionsArray newPa = new PositionsArray(
                                   cObjInfo.getName(),
                                   kepler.getPositions(cObjInfo,jDay,compressed));    	
             positions.addElement(newPa);       
             pa = newPa;
        } 
        
    	
    	
        return pa.getPositions();

    	
    } // end method getPositon   
    
    
}// end class ObjectsPositions
