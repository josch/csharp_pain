/*
  File: PositionObserver.java

  University of Applied Science Berne,HTA-Biel/Bienne,
  Computer Science Department.

  Diploma thesis J3D Solar System Simulator
  Originally written by Marcel Portner & Bernhard Hari (c) 2000
  
  CVS - Information :
  
  $Header: /var/cvsreps/projects/c450/2000/sss3d/source_diploma/sss3d/utils/observer/PositionObserver.java,v 1.2 2000/12/13 13:42:04 portm Exp $
  $Author: portm $
  $Date: 2000/12/13 13:42:04 $
  $State: Exp $
  
*/
package sss3d.utils.observer;

/**
 * The interface PositionObserver is part of the position observer - defined
 * by the observer pattern. 
 * References : E.Gamma; Design Patterns ISBN 0-201-63361-2, Addison Wesley, 1995<br>
 * It defines an updating interface for objects that should be notified of changes
 * in a OrbitPositions.
 * @author Marcel Portner & Bernhard Hari
 * @version $Revision: 1.2 $
 * @see OrbitPositions
 * @see ObjectsPositions
 */
public interface PositionObserver {
    /**
     * The update methode is used to keep the state of the concrete observer
     * consistent with the state of the OrbitPositions.<br>
     *
     * @param     orbitPos a reference to the OrbitPositions
     */ 	
    void update(OrbitPositions orbitPos);
    
    /**
     * Returns the id of the concrete observer.
     *
     * @return    String the id of the concrete observer
     */      
    String getId();
}
