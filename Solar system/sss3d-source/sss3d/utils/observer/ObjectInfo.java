/*
  File: ObjectInfo.java

  University of Applied Science Berne,HTA-Biel/Bienne,
  Computer Science Department.

  Diploma thesis J3D Solar System Simulator
  Originally written by Marcel Portner & Bernhard Hari (c) 2000
  
  CVS - Information :
  
  $Header: /var/cvsreps/projects/c450/2000/sss3d/source_diploma/sss3d/utils/observer/ObjectInfo.java,v 1.5 2000/12/13 13:42:03 portm Exp $
  $Author: portm $
  $Date: 2000/12/13 13:42:03 $
  $State: Exp $
  
*/
package sss3d.utils.observer;

import java.util.*;

/**
 * Abstract class ObjectInfo is part of the information observer - defined
 * by the observer pattern. 
 * References : E.Gamma; Design Patterns ISBN 0-201-63361-2, Addison Wesley, 1995<br>
 * ObjectInfo knows its observers and provides an interface for attaching and 
 * detaching Observer objects.
 * @author Marcel Portner & Bernhard Hari
 * @version $Revision: 1.5 $
 * @see InfoObserver
 * @see ObjectsInformation
 */
abstract public class ObjectInfo {
  
   protected java.util.Vector observers; // contains references to every 
                                         // information observer
   /**
    * Constructor
    */
   public ObjectInfo() {
      observers = new Vector();
   }	

   /**
    * Attaches an observer to the vector observers.
    *
    * @param observer information observer to attach
    */	
   public void attach(InfoObserver observer) {
      observers.addElement(observer);
   }

   /**
    * Detaches an observer from the vector observers.
    *
    * @param observer removes the information observer from the list
    */	
   public void detach(InfoObserver observer) {
      observers.removeElement(observer);
   }

   /**
    * Notifys an observer given by the argument.
    *
    * @param id id from observer inside the list
    */
   public void notifyObservers(String id, int parameter) {
      InfoObserver io;
      Enumeration e = observers.elements();
      while(e.hasMoreElements()) {
         // get identification from every object
         // and update the information...
         io = (InfoObserver)e.nextElement();
   	  
         //if( (io.getId()).equals(id) ){}
         // defined at the moment for infobox only
         // if( (io.getId()).equals("infobox") ){
         io.update( id, parameter );
         //System.out.println("Class ObjectInfo : notifyObservers : "+io.getId());
   	     //}
      }
   }
}
