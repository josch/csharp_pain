/*
  File: OrbitPositions.java

  University of Applied Science Berne,HTA-Biel/Bienne,
  Computer Science Department.

  Diploma thesis J3D Solar System Simulator
  Originally written by Marcel Portner & Bernhard Hari (c) 2000
  
  CVS - Information :
  
  $Header: /var/cvsreps/projects/c450/2000/sss3d/source_diploma/sss3d/utils/observer/OrbitPositions.java,v 1.3 2000/12/13 13:42:04 portm Exp $
  $Author: portm $
  $Date: 2000/12/13 13:42:04 $
  $State: Exp $
  
*/
package sss3d.utils.observer;

import java.util.*;

/**
 * Abstract class OrbitPositions is part of the position observer - defined
 * by the observer pattern. 
 * References : E.Gamma; Design Patterns ISBN 0-201-63361-2, Addison Wesley, 1995<br>
 * OrbitPositions knows its observers and provides an interface for attaching and 
 * detaching Observer objects.
 *
 * @author Marcel Portner & Bernhard Hari
 * @version $Revision: 1.3 $
 * @see PositionObserver
 * @see ObjectsPositions
 */
abstract public class OrbitPositions {


    protected java.util.Vector observers; // list containing all observers
    
    /**
     * Creates an object of OrbitPositions witch holds a vector of
     * all attached observers.
     */    
    public OrbitPositions() {
    	observers = new Vector();
    }	

    /**
     * Attaches an observer to the vector observers.
     *
     * @param observer position observer to attach
     */		
    public void attach(PositionObserver observer) {
    	observers.addElement(observer);
    }

    /**
     * Detaches an observer from the vector observers.
     *
     * @param observer removes the position observer from the list
     */	
    public void detach(PositionObserver observer) {
    	observers.removeElement(observer);
    }

    /**
     * Notifys an observer given by the argument.
     *
     * @param id id from observer inside the list
     */
    public void notifyObservers(String id) {
    	PositionObserver po;
    	Enumeration e = observers.elements();
    	while(e.hasMoreElements()) {
    	  // get identification from every object
    	  // and update the positions...
    	  po = (PositionObserver)e.nextElement();
    	  
    	  if( (po.getId()).equals(id) ){
    	    po.update(this);
    	  }
    	}
    }

}
