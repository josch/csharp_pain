/*
  File: ObjectsInformation.java

  University of Applied Science Berne,HTA-Biel/Bienne,
  Computer Science Department.

  Diploma thesis J3D Solar System Simulator
  Originally written by Marcel Portner & Bernhard Hari (c) 2000
  
  CVS - Information :
  
  $Header: /var/cvsreps/projects/c450/2000/sss3d/source_diploma/sss3d/utils/observer/ObjectsInformation.java,v 1.6 2000/12/13 13:42:03 portm Exp $
  $Author: portm $
  $Date: 2000/12/13 13:42:03 $
  $State: Exp $
  
*/
package sss3d.utils.observer;

import sss3d.contentbranch.*;
import sss3d.utils.xmlparser.XMLConstants;
import sss3d.utils.SSS3dConstants;

import java.util.*;

/**
 * The class ObjectsInformation is part of the information observer - defined
 * by the observer pattern. 
 * References : E.Gamma; Design Patterns ISBN 0-201-63361-2, Addison Wesley, 1995<br>
 * It stores the state (info) of the concrete observers and sends a notification to 
 * its concrete observers when its state changes. 
 *
 * @author Marcel Portner & Bernhard Hari
 * @version $Revision: 1.6 $
 * @see ObjectInfo
 * @see InfoObserver
 */
public class ObjectsInformation extends ObjectInfo {

   /**
    * Initialization Object, keeps the state and configuration
    * of the solar system simulator 
    */
   private InitializationObject iniObject;

   /**
    * Constructor.<br>
    * It calls the constructor of its parent class ObjectInfo.
    */ 
   public ObjectsInformation( InitializationObject iniObject ) {
      super();
      this.iniObject = iniObject;
   }

   /**
    * Returns the information from the object given by the identification.
    *
    * @return    id the id of the concrete observer
    */  
   public CelestialObjectInfo getInfo(String id) {
    	
      boolean found = false;    	
      CelestialObjectInfo cObjInfo = null;
      InfoObserver io = null;
       
      // gets the object from the observer list    	
      Enumeration e = observers.elements();
      while(e.hasMoreElements()) {
         io = (InfoObserver)e.nextElement();

         if( (io.getId()).equals(id) ) {
            found = true;
            break;
         }
      }
   	
      if(!found) {
         // not specified at the moment
      } else {
         if( io != null){
            cObjInfo = io.getInfo();
            if(cObjInfo != null)return cObjInfo;
         }
         return null;	
      }
      return null;
   } // end method getInfo
   
   /**
    * Sets the object specified by the parameter and the
    * name of the celestial object.
    *
    * @param object the object associated to the parameter
    * @param parameter the desired parameter
    * @param name the name of the celestial object
    */  
   public void setParameter(Object object, int parameter ) {
     //System.out.println("setParameter/2");
     if ( iniObject.setParameter(object, parameter, null) )  {
        notifyObservers( "all", parameter );
        //System.out.println("set parameter = "+object.toString()+" "+parameter);
        if ( parameter == XMLConstants.JDAY ) {
             notifyObservers( "all", SSS3dConstants.RELOAD);
        }
     }
   }

   /**
    * Sets the object specified by the parameter and the
    * name of the celestial object.
    *
    * @param object the object associated to the parameter
    * @param parameter the desired parameter
    * @param name the name of the celestial object
    */  
   public void setParameter(Object object, int parameter , String name ) {
     //System.out.println("setParameter/3");
     if ( iniObject.setParameter(object, parameter, name) )  {
        notifyObservers( name , parameter );
        //System.out.println("set parameter = "+object.toString()+" "+parameter+" "+name);
     }
   }
   
   /**
    * Returns the object specified by the parameter.
    *
    * @param parameter the desired parameter
    * @return Object an object reference to the specified parameter
    */
   public Object getParameter( int parameter ) {
       //System.out.println("get parameter = "+parameter);
       return iniObject.getParameter( parameter, null );
   }

   /**
    * Returns the object specified by the parameter and
    * the object name.
    *
    * @param parameter the desired parameter
    * @param name the name of the celestial object
    * @return Object an object reference to the specified parameter
    */
    public Object getParameter( int parameter, String name ) {  
      //System.out.println("get parameter = "+parameter+" name = "+name);
      return iniObject.getParameter( parameter, name ); 
    }   
    
    /**
     * gets the whole state - initialization object
     */
    public InitializationObject getInitializationObject() {
      return iniObject;
    }
    
}// end class ObjectsInformation
