/*
  File: DomToTreeModelAdapter.java

  University of Applied Science Berne,HTA-Biel/Bienne,
  Computer Science Department.

  Diploma thesis J3D Solar System Simulator
  Originally written by Marcel Portner & Bernhard Hari (c) 2000

  CVS - Information :

  $Header: /var/cvsreps/projects/c450/2000/sss3d/source_diploma/sss3d/utils/xmlparser/DomToTreeModelAdapter.java,v 1.1 2000/11/07 14:28:11 harib Exp $
  $Author: harib $
  $Date: 2000/11/07 14:28:11 $
  $State: Exp $

*/
package sss3d.utils.xmlparser;

import sss3d.gui.infobox.AdapterNode;
import sss3d.contentbranch.CelestialObjectInfo;
import java.util.*;
import javax.swing.tree.*;
import javax.swing.event.*;
import org.w3c.dom.Document;

/**
 * This adapter converts the current Document (a DOM) into 
 * a JTree model. 
 *   
 * @author Marcel Portner & Bernhard Hari
 * @version $Revision: 1.1 $
 */
public class DomToTreeModelAdapter implements javax.swing.tree.TreeModel 
{
  private Document document;
  /**
   * Constructor
   *
   * @param document document object - DOM
   */
  public DomToTreeModelAdapter(Document document){
    this.document = document;  
  }
  
  /**
   * Basic TreeModel operations.
   * Returns the root object of this DOM
   *
   * @return Object root object of document
   */
  public Object  getRoot() {
    return new AdapterNode(document);
  }
  
  /**
   * Returns true if the given node is a leaf node.
   *
   * @param aNode a reference to an object
   * @return boolean true if given node is a leaf node
   */
  public boolean isLeaf(Object aNode) {
    // Determines whether the icon shows up to the left.
    // Return true for any node with no children
    AdapterNode node = (AdapterNode) aNode;
    if (node.childCount() > 0) return false;
    return true;
  }
  
  /**
   * Returns the number of childs related to a parent node
   *
   * @param parent object reference to the parent node
   * @returns int number of childs associated to the parent node
   */ 
  public int     getChildCount(Object parent) {
    AdapterNode node = (AdapterNode) parent;
    return node.childCount();
  }
  
  /**
   * Returns the child with the given index and parent node.
   *
   * @param index the index of the child node
   * @param parent an object reference to the parent node
   */
  public Object getChild(Object parent, int index) {
    AdapterNode node = (AdapterNode) parent;
    return node.child(index);
  }
  
  /**
   * Returns the index of the given child.
   *
   * @param parent object reference to its parent node
   * @param child object reference to the child node
   * @returns int the index of the child node
   */
  public int getIndexOfChild(Object parent, Object child) {
    AdapterNode node = (AdapterNode) parent;
    return node.index((AdapterNode) child);
  }
  
  /**
   * Not used at the moment. 
   */
  public void valueForPathChanged(TreePath path, Object newValue) {
    // Null. We won't be making changes in the GUI
    // If we did, we would ensure the new value was really new,
    // adjust the model, and then fire a TreeNodesChanged event.
  }

  /*
   * Use these methods to add and remove event listeners.
   * (Needed to satisfy TreeModel interface, but not used.)
   */
  private Vector listenerList = new Vector();

  public void addTreeModelListener(TreeModelListener listener) {
    if ( listener != null 
    && ! listenerList.contains( listener ) ) {
       listenerList.addElement( listener );
    }
  }
  public void removeTreeModelListener(TreeModelListener listener) {
    if ( listener != null ) {
       listenerList.removeElement( listener );
    }
  }

  // Note: Since XML works with 1.1, this example uses Vector.
  // If coding for 1.2 or later, though, I'd use this instead:
  //   private List listenerList = new LinkedList();
  // The operations on the List are then add(), remove() and
  // iteration, via:
  //  Iterator it = listenerList.iterator();
  //  while ( it.hasNext() ) {
  //    TreeModelListener listener = (TreeModelListener)it.next();
  //    ...
  //  }

  /*
   * Invoke these methods to inform listeners of changes.
   * (Not needed for this example.)
   * Methods taken from TreeModelSupport class described at 
   *   http://java.sun.com/products/jfc/tsc/articles/jtree/index.html
   * That architecture (produced by Tom Santos and Steve Wilson)
   * is more elegant. I just hacked 'em in here so they are
   * immediately at hand.
   */
  public void fireTreeNodesChanged( TreeModelEvent e ) {
    Enumeration listeners = listenerList.elements();
    while ( listeners.hasMoreElements() ) {
      TreeModelListener listener = 
        (TreeModelListener)listeners.nextElement();
      listener.treeNodesChanged( e );
    }
  } 
  public void fireTreeNodesInserted( TreeModelEvent e ) {
    Enumeration listeners = listenerList.elements();
    while ( listeners.hasMoreElements() ) {
       TreeModelListener listener =
         (TreeModelListener)listeners.nextElement();
       listener.treeNodesInserted( e );
    }
  }   
  public void fireTreeNodesRemoved( TreeModelEvent e ) {
    Enumeration listeners = listenerList.elements();
    while ( listeners.hasMoreElements() ) {
      TreeModelListener listener = 
        (TreeModelListener)listeners.nextElement();
      listener.treeNodesRemoved( e );
    }
  }   
  public void fireTreeStructureChanged( TreeModelEvent e ) {
    Enumeration listeners = listenerList.elements();
    while ( listeners.hasMoreElements() ) {
      TreeModelListener listener =
        (TreeModelListener)listeners.nextElement();
      listener.treeStructureChanged( e );
    }
  }
}
