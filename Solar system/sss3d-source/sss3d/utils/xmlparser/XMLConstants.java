/*
  File: XMLConstants.java

  University of Applied Science Berne,HTA-Biel/Bienne,
  Computer Science Department.

  Diploma thesis J3D Solar System Simulator
  Originally written by Marcel Portner & Bernhard Hari (c) 2000
  
  CVS - Information :
  
  $Header: /var/cvsreps/projects/c450/2000/sss3d/source_diploma/sss3d/utils/xmlparser/XMLConstants.java,v 1.11 2000/12/13 13:42:17 portm Exp $
  $Author: portm $
  $Date: 2000/12/13 13:42:17 $
  $State: Exp $
  
*/
package sss3d.utils.xmlparser;

import java.util.Hashtable;

/**
 * This class is used to parse an XML document.
 * It contains frequently used constants.
 *
 * <pre>
 *    use : 
 *    XMLConstants.treeElementNames[i];
 *    ...
 * </pre> 
 *
 * @author Marcel Portner & Bernhard Hari
 * @version $Revision: 1.11 $
 * @see XMLParser
 */
public class XMLConstants {

//constants
  // tree view
  // An array of names for DOM node-types
  public static final String[] typeName = {
        "none",
        "Element",
        "Attr",
        "Text",
        "CDATA",
        "EntityRef",
        "Entity",
        "ProcInstr",
        "Comment",
        "Document",
        "DocType",
        "DocFragment",
        "Notation",
  };
  
  public static final int ELEMENT_TYPE =   1;
  public static final int ATTR_TYPE =      2;
  public static final int TEXT_TYPE =      3;
  public static final int CDATA_TYPE =     4;
  public static final int ENTITYREF_TYPE = 5;
  public static final int ENTITY_TYPE =    6;
  public static final int PROCINSTR_TYPE = 7;
  public static final int COMMENT_TYPE =   8;
  public static final int DOCUMENT_TYPE =  9;
  public static final int DOCTYPE_TYPE =  10;
  public static final int DOCFRAG_TYPE =  11;
  public static final int NOTATION_TYPE = 12;
    
  // The list of elements to display in the tree
  // This array is also used for the setup of the 
  // hashtable treeElements
  
  // first set the limits accordingly to the array treeElementNames
  public static final int CELESTIALOBJECT_NAMES_MIN =  0;
  public static final int CELESTIALOBJECT_NAMES_MAX = 28;
  public static final int INITIALIZATION_NAMES_MIN  = 29;
  public static final int INITIALIZATION_NAMES_MAX  = 62;

  /**
   * String representations of the xml tree elements.
   */  
  public static final String[] treeElementNames = {
        // celestialobject parameter names
        "celestialobject",  
        "general",
        "parameter",
        "type",   
        "name",
        "parent",
        "scalingfactor",    
        "diameter",
        "mindistancefromsun",
        "maxdistancefromsun",
        "nbrofplanetpositions",
        "rotationperiod",
        "orbitperiod",
        "meanorbitvelocity",
        "orbiteccentricity",
        "orbitinclinationtoecliptic",
        "inclinationofequatortoorbit",
        "volume",
        "distance",
        "mass",
        "density",
        "surfacegravity",
        "escapevelocity",
        "meantempatsolidsurface",
        "majoratmosphericconstitutents",
        "epoch",
        "equinox",
        "longitudenode",
        "perihelion",
        // initialization object parameter names
        "ini",
        "info",
        "values",
        "filetype",
        "id",
        "filename",
        "path",
        "day",
        "month",
        "year",
        "hour",
        "minutes",
        "seconds",
        "jday",
        "calculation",
        "animationspeed",
        "value",
        "compressed",
        "glasses3d",
        "joystick",
        "cameraatorigin",
        "camera",
        "star",
        "planet",
        "moon",
        "comet",
        "satellite",
        "rocket",
        "objectname",
        "visible",
        "orbit",
        "colororbit",
        "coordinatesystem",
        "document"
        
  }; 

  /**
   * Modifiable values at runtime.
   * If set to true - the simulator can modify this value at runtime
   * else it's only possible at startup.
   */
  public static final boolean[] MODIFIABLE_VALUES_AT_RUNTIME = {
        // celestialobject parameter names
        
        false, // celestialobject  
        false, // general
        false, // parameter
        false, // type   
        false, // name
        false, // parent
    	  false, // scalingfactor    
        false, // diameter
        false, // mindistancefromsun
        false, // maxdistancefromsun
        false, // nbrofplanetpositions
        false, // rotationperiod
        false, // orbitperiod
        false, // meanorbitvelocity
        false, // orbiteccentricity
        false, // orbitinclinationtoecliptic
        false, // inclinationofequatortoorbit
        false, // volume
        false, // distance
    	  false, // mass
    	  false, // density
        false, // surfacegravity
        false, // escapevelocity
        false, // meantempatsolidsurface
        false, // majoratmosphericconstitutents
        false, // epoch
        false, // equinox
        false, // longitudenode
        false, // perihelion
        
        // initialization object parameter names
        false, // ini
        false, // info
        false, // values
        false, // filetype
        false, // id
        false, // filename
        false, // path
        true,  // day
        true,  // month
        true,  // year
        true,  // hour
        true,  // minutes
        true,  // seconds
        true,  // jday
        false, // calculation
        true,  // animationspeed
        true,  // value
        false, // compressed
        false, // glasses3d
        false, // joystick
        true,  // camera at origin
        true,  // camera
        true,  // star
        true,  // planet
        true,  // moon
        true,  // comet
        true,  // satellite
        true,  // rocket
        true,  // objectname
        true,  // visible
        true,  // orbit
        true,  // colororbit
        true,  // coordinatesystem
        false  // document
        
  }; 
    
  /**
   * Type of XML files xml file == initialization file
   */
  public static final int XML_INITIALIZATION   =  0;
  /**
   * Type of XML files xml file == celestial object info file
   */
  public static final int XML_CELESTIALOBJECT  =  1;
  
  /**
   * Values according to the array treeElementNames      
   * Celestial object constants
   */
  public static final int CELESTIALOBJECT               =  0;  
  public static final int GENERAL                       =  1;         
  public static final int PARAMETER                     =  2;
  public static final int TYPE                          =  3;
  public static final int NAME                          =  4;
  public static final int PARENT                        =  5;
  public static final int SCALINGFACTOR                 =  6;
  public static final int DIAMETER                      =  7;
  public static final int MINDISTANCEFROMSUN            =  8;
  public static final int MAXDISTANCEFROMSUN            =  9;
  public static final int NBROFPLANETPOSITIONS          = 10;
  public static final int ROTATIONPERIOD                = 11;
  public static final int ORBITPERIOD                   = 12;
  public static final int MEANORBITVELOCITY             = 13;
  public static final int ORBITECCENTRICITY             = 14;
  public static final int ORBITINCLINATIONTOECLIPTIC    = 15;
  public static final int INCLINATIONOFEQUATORTOORBIT   = 16;
  public static final int VOLUME                        = 17;
  public static final int DISTANCE                      = 18;
  public static final int MASS                          = 19;
  public static final int DENSITY                       = 20;
  public static final int SURFACEGRAVITY                = 21;
  public static final int ESCAPEVELOCITY                = 22;
  public static final int MEANTEMPATSOLIDSURFACE        = 23;
  public static final int MAJORATMOSPHERICCONSTITUTENTS = 24;
  public static final int EPOCH                         = 25;
  public static final int EQUINOX                       = 26;
  public static final int LONGITUDENODE                 = 27;
  public static final int PERIHELION                    = 28;
  // initialization constants
  public static final int INI                           = 29;
  public static final int INFO                          = 30;
  public static final int VALUES                        = 31;
  public static final int FILETYPE                      = 32;
  public static final int ID                            = 33;
  public static final int FILENAME                      = 34;
  public static final int PATH                          = 35;
  public static final int DAY                           = 36;
  public static final int MONTH                         = 37;
  public static final int YEAR                          = 38;
  public static final int HOUR                          = 39;
  public static final int MINUTES                       = 40;
  public static final int SECONDS                       = 41;
  public static final int JDAY                          = 42;
  public static final int CALCULATION                   = 43;
  public static final int ANIMATIONSPEED                = 44;
  public static final int VALUE                         = 45;
  public static final int COMPRESSED                    = 46;
  public static final int GLASSES3D                     = 47;
  public static final int JOYSTICK                      = 48;
  public static final int CAMERAATORIGIN                = 49;
  public static final int CAMERA                        = 50;
  public static final int STAR                          = 51;
  public static final int PLANET                        = 52;
  public static final int MOON                          = 53;
  public static final int COMET                         = 54;
  public static final int SATELLITE                     = 55;
  public static final int ROCKET                        = 56;
  public static final int OBJECTNAME                    = 57;
  public static final int VISIBLE                       = 58;
  public static final int ORBIT                         = 59;
  public static final int COLORORBIT                    = 60;  
  public static final int COORDINATESYSTEM              = 61;
  public static final int DOCUMENT                      = 62;

  
  private Hashtable names;
  
  /**
   * Constructs a hashtable for simple and fast access to
   * all values.
   * 
   */
  public XMLConstants() {
    names = new Hashtable();
    for (int i = 0; i< treeElementNames.length; i++) {
      names.put( treeElementNames[i] , new Integer(i) );
    }
  }
  
  /**
   * returns the index of a given parameter name
   *
   * @param  elementName the name of the parameter
   * @return int the index of a given parameter name
   */
  public int getElementIndex( String elementName ) {
    int index = -1;
    
    if ( names != null ) {
      Integer n = (Integer)names.get( elementName );
      if (n != null) {
          index = n.intValue();
      }
    }
    return index;
  }

}
