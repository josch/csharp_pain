/*
  File: XMLParser.java

  University of Applied Science Berne,HTA-Biel/Bienne,
  Computer Science Department.

  Diploma thesis J3D Solar System Simulator
  Originally written by Marcel Portner & Bernhard Hari (c) 2000
  
  CVS - Information :
  
  $Header: /var/cvsreps/projects/c450/2000/sss3d/source_diploma/sss3d/utils/xmlparser/XMLParser.java,v 1.11 2000/12/13 13:42:17 portm Exp $
  $Author: portm $
  $Date: 2000/12/13 13:42:17 $
  $State: Exp $
  
*/
package sss3d.utils.xmlparser;

import sss3d.contentbranch.*;
import sss3d.utils.SSS3dConstants;

import java.io.*;

import javax.xml.parsers.DocumentBuilderFactory;  
import javax.xml.parsers.FactoryConfigurationError;  
import javax.xml.parsers.ParserConfigurationException;
 
import javax.xml.parsers.DocumentBuilder;  
import org.xml.sax.SAXException;  
import org.xml.sax.SAXParseException; 
 
import java.io.File;
import java.io.IOException;

import org.w3c.dom.*;

import java.util.Hashtable;

/**
 * This class is used to parse an XML document. The XML file
 * holds important data about a celestial object or
 * the initialization state of the solar system simulator.
 *
 * <pre>
 *    use : 
 *    XMLParser xmlp = new XMLParser(CelestialObjectInfo, "fully qualified path");
 *    xmlp.read();
 * </pre> 
 *
 * @author Marcel Portner & Bernhard Hari
 * @version $Revision: 1.11 $
 * @see sss3d.contentbranch.CelestialObjectInfo
 * @see sss3d.contentbranch.InitializationObject
 * @see sss3d.utils.observer.ObjectsInformation
 */
public class XMLParser {
	
    private String path;
    private InitializationObject iniObject;    	
    private CelestialObjectInfo cObjInfo;	

    /**
     * Constructs an object of an XMLParser.
     *
     * @param     object  an object to store the information from the file.
     * @param     type the type of this xml file defined inside class SSS3dConstants
     * @param     path path to the file containing the information
     */ 
    public XMLParser(Object object,int type, String path) {
    	  if ( type == XMLConstants.XML_CELESTIALOBJECT ) {
               this.cObjInfo = (CelestialObjectInfo)object;
    	  } else if( type == XMLConstants.XML_INITIALIZATION ) {
    	       this.iniObject = (InitializationObject)object;
          }
    	  this.path = path.toLowerCase();
    }

    /**
     * Method parses an XML file. Depending on the parameters and
     * their values it stores the information in the InitializationObject.
     *
     * @exception IOExecption throws an IOException in case of a read error
     */     
    private void parseInitializationFile() throws IOException {
    
        // get current document from CelestialObjectInfo
        Document document    = (Document)iniObject.getParameter(XMLConstants.DOCUMENT);
        Hashtable parameters = new Hashtable();
	
        // initialize hashtable
	      for (int i = XMLConstants.INITIALIZATION_NAMES_MIN; 
                 i <= XMLConstants.INITIALIZATION_NAMES_MAX; i++) {
	        parameters.put(XMLConstants.treeElementNames[i], new Integer(i));
        }
                
        if ( document != null ) {
          // iterate over each possible element tag
          // defined in class XMLConstants
          for(int tag = XMLConstants.INITIALIZATION_NAMES_MIN;
                  tag <= XMLConstants.INITIALIZATION_NAMES_MAX; tag++ ) {
          
            // get node of specific tag
            // show only leaf nodes
            String nodeName = XMLConstants.treeElementNames[tag];
          
            if ( !nodeName.equals("ini") &&
                 !nodeName.equals("info") &&
                 !nodeName.equals("values") ) {
               
                 NodeList nodes = document.getElementsByTagName(nodeName);
                 // search for value of node == nodeName
                 for(int i = 0; i < nodes.getLength(); i++) {
                     
                     Node child = nodes.item(i);
                     if ( child.hasChildNodes() && !(
                          nodeName.equals(XMLConstants.treeElementNames[XMLConstants.STAR]) ||
                          nodeName.equals(XMLConstants.treeElementNames[XMLConstants.PLANET]) ||
                          nodeName.equals(XMLConstants.treeElementNames[XMLConstants.MOON]) ||
                          nodeName.equals(XMLConstants.treeElementNames[XMLConstants.COMET]) ||
                          nodeName.equals(XMLConstants.treeElementNames[XMLConstants.SATELLITE]) ||
                          nodeName.equals(XMLConstants.treeElementNames[XMLConstants.ROCKET]) ||
                          nodeName.equals(XMLConstants.treeElementNames[XMLConstants.ANIMATIONSPEED])
                        )) {

                         NodeList nl = child.getChildNodes();
                         for(int j = 0; j < nl.getLength(); j++) {
                             if ( nl.item(j).getNodeType() == XMLConstants.TEXT_TYPE ) {
                                  // get this value and set it correctly inside 
                                  // CelestialObjectInfo.....
                                  Integer n    = (Integer)parameters.get( child.getNodeName() );
                                  String value = nl.item(j).getNodeValue();
                                  //System.out.println("Name "+child.getNodeName()+" Value "+value);
                           
                                  /*System.out.println("Parameter = "+child.getNodeName()+
                                               " Value = "+nl.item(j).getNodeValue());
                                   */
                         
                                  if (n != null) {
                                    switch ( n.intValue() ){                                  
                                    case XMLConstants.FILETYPE :
                                         iniObject.setParameter(value,XMLConstants.FILETYPE);
                                         break;
                                    case XMLConstants.ID :
                                         iniObject.setParameter(value,XMLConstants.ID);
                                         break;
                                    case XMLConstants.FILENAME :
                                         iniObject.setParameter(value,XMLConstants.FILENAME);
                                         break;                                                                    
                                    case XMLConstants.PATH :
                                         iniObject.setParameter(value,XMLConstants.PATH);
                                         break;                                  
                                    case XMLConstants.DAY :
                                         iniObject.setParameter(new Integer(value),XMLConstants.DAY);
                                         break;                                  
                                    case XMLConstants.MONTH :
                                         iniObject.setParameter(new Integer(value),XMLConstants.MONTH);
                                         break;                                  
                                    case XMLConstants.YEAR :
                                         iniObject.setParameter(new Integer(value),XMLConstants.YEAR);
                                         break;                                  
                                    case XMLConstants.HOUR :
                                         iniObject.setParameter(new Integer(value),XMLConstants.HOUR);
                                         break;
                                    case XMLConstants.MINUTES :
                                         iniObject.setParameter(new Integer(value),XMLConstants.MINUTES);
                                         break;                                  
                                    case XMLConstants.SECONDS :
                                         iniObject.setParameter(new Integer(value),XMLConstants.SECONDS);
                                         break;                                  
                                    case XMLConstants.JDAY :
                                         iniObject.setParameter(new Double(value),XMLConstants.JDAY);
                                         break;                                  
                                    case XMLConstants.CALCULATION :
                                         if ( value.equals("kepler") ) {
                                           iniObject.setParameter(new Integer(SSS3dConstants.KEPLER),XMLConstants.CALCULATION);
                                         } else if ( value.equals("ellipse") ) {
                                           iniObject.setParameter(new Integer(SSS3dConstants.ELLIPSE),XMLConstants.CALCULATION);
                                         } else if ( value.equals("analytic") ) {
                                           iniObject.setParameter(new Integer(SSS3dConstants.ANALYTIC),XMLConstants.CALCULATION);
                                         } else {
                                           // in case of an unmatching value, use kepler as default calculation method 
                                           iniObject.setParameter(new Integer(SSS3dConstants.KEPLER),XMLConstants.CALCULATION);
                                         }      
                                         break;                                  
                                    /*case XMLConstants.ANIMATIONSPEED :
                                         iniObject.setParameter(new Integer(value),XMLConstants.ANIMATIONSPEED);
                                         break; */                                 
                                    case XMLConstants.COMPRESSED :
                                         iniObject.setParameter(new Boolean(value),XMLConstants.COMPRESSED);
                                         break;                                  
                                    case XMLConstants.GLASSES3D :
                                         iniObject.setParameter(new Boolean(value),XMLConstants.GLASSES3D);
                                         break;                  
                                    case XMLConstants.JOYSTICK :
                                         iniObject.setParameter(new Boolean(value),XMLConstants.JOYSTICK);
                                         break;
                                    case XMLConstants.CAMERAATORIGIN :
                                         iniObject.setParameter(new Boolean(value),XMLConstants.CAMERAATORIGIN);
                                         break;                                                                                                      
                                    case XMLConstants.CAMERA :
                                         boolean found = false;
                                         value = value.toLowerCase();
                                         for( int camera = 0; camera < SSS3dConstants.CAMERA_POSITIONS.length;camera++) {
                                            if ( value.equals( (SSS3dConstants.CAMERA_POSITIONS[camera]).toLowerCase() ) ) {
                                              iniObject.setParameter(new Integer(camera),XMLConstants.CAMERA);
                                              found = true;
                                              break;
                                            }
                                         }
                                         if ( !found ) {
                                         // in case of an unmatching value, use kepler as default calculation method 
                                           iniObject.setParameter(new Integer(SSS3dConstants.CAMERA_DEFAULT),XMLConstants.CAMERA);
                                         }
                                                                              
                                         break;                                  
                                    default : break;
                                    }
                                  }
                             }
                         }                     
                     
                     } else if(nodeName.equals(XMLConstants.treeElementNames[XMLConstants.ANIMATIONSPEED]) ){
                         AnimationSpeed animationSpeed;
                         
                         NamedNodeMap nnm = child.getAttributes();
                         Node type  = nnm.getNamedItem(XMLConstants.treeElementNames[XMLConstants.TYPE]);
                         Node value = nnm.getNamedItem(XMLConstants.treeElementNames[XMLConstants.VALUE]);
                         
                         String nodeValue = type.getNodeValue();
                         
                         int t = AnimationSpeed.DAYS_PER_SECOND;
                         
                         if ( nodeValue.equals(AnimationSpeed.NAMES[AnimationSpeed.HOURS_PER_SECOND]) ) {
                            t = AnimationSpeed.HOURS_PER_SECOND;
                         } else if( nodeValue.equals(AnimationSpeed.NAMES[AnimationSpeed.MINUTES_PER_SECOND]) ) {
                           t = AnimationSpeed.MINUTES_PER_SECOND;
                         } 
                         
                         try {
                           
                           int v = (new Integer(value.getNodeValue())).intValue();
                           animationSpeed = new AnimationSpeed( t, v );
                           System.out.println(animationSpeed.toString());
                         } catch (NumberFormatException e) {
                           // create animationspeed with default values
                           animationSpeed = new AnimationSpeed();
                         }
                         iniObject.setParameter(animationSpeed, XMLConstants.ANIMATIONSPEED);
                       
                     } else if(nodeName.equals(XMLConstants.treeElementNames[XMLConstants.STAR]) ||
                               nodeName.equals(XMLConstants.treeElementNames[XMLConstants.PLANET]) ||
                               nodeName.equals(XMLConstants.treeElementNames[XMLConstants.MOON]) ||
                               nodeName.equals(XMLConstants.treeElementNames[XMLConstants.COMET]) ||
                               nodeName.equals(XMLConstants.treeElementNames[XMLConstants.SATELLITE]) ||
                               nodeName.equals(XMLConstants.treeElementNames[XMLConstants.ROCKET]) ){

                       IniData data;
                       
                       // find out parent
                       //System.out.println("NODENAME ---->>>>>>>>>> "+nodeName);
                       Node parentNode  = child.getParentNode();
                       NamedNodeMap pnnm = parentNode.getAttributes();
                       Node attribute = pnnm.getNamedItem(XMLConstants.treeElementNames[XMLConstants.OBJECTNAME]);
                       
                       int type = SSS3dConstants.UNKNOWN_TYPE;
                       
                       if ( nodeName.equals(SSS3dConstants.TYPES[SSS3dConstants.STAR_TYPE]) ) {
                          type = SSS3dConstants.STAR_TYPE;
                       
                       } else if ( nodeName.equals(SSS3dConstants.TYPES[SSS3dConstants.PLANET_TYPE]) ) {
                          type = SSS3dConstants.PLANET_TYPE;
                       
                       } else if ( nodeName.equals(SSS3dConstants.TYPES[SSS3dConstants.MOON_TYPE]) ) {
                          type = SSS3dConstants.MOON_TYPE;
                       
                       } else if ( nodeName.equals(SSS3dConstants.TYPES[SSS3dConstants.COMET_TYPE]) ) {
                          type = SSS3dConstants.COMET_TYPE;
                       
                       } else if ( nodeName.equals(SSS3dConstants.TYPES[SSS3dConstants.SATELLITE_TYPE]) ) {
                          type = SSS3dConstants.SATELLITE_TYPE;
                      
                       } else if ( nodeName.equals(SSS3dConstants.TYPES[SSS3dConstants.ROCKET_TYPE]) ) {
                          type = SSS3dConstants.ROCKET_TYPE;
                      
                       } 
                       
                       if (attribute != null) {
                          data = new IniData(attribute.getNodeValue(), null,type, false, false, false, "red" );
                       } else  {
                         data = new IniData(null, null,type, false, false, false, "red" );
                       }
                       
                       NamedNodeMap nnm = child.getAttributes();
                       
                       for (int j = XMLConstants.OBJECTNAME; j <= XMLConstants.COORDINATESYSTEM; j++) {
                         Node node = nnm.getNamedItem(XMLConstants.treeElementNames[j]);
                         
                         if (node != null) {

                           // get this value and set it correctly inside 
                           Integer n    = (Integer)parameters.get( node.getNodeName() );

          
                           if (n != null) {
                             switch ( n.intValue() ){                                  
                               case XMLConstants.OBJECTNAME :
                                    data.setName(node.getNodeValue());
                                    break;
                               case XMLConstants.VISIBLE :
                                    data.setVisible((new Boolean(node.getNodeValue())).booleanValue());
                                    break;
                               case XMLConstants.ORBIT :
                                    data.setOrbit((new Boolean(node.getNodeValue())).booleanValue());
                                    break;                                                                    
                               case XMLConstants.COLORORBIT :
                                    data.setColorOrbit(node.getNodeValue());
                                    break; 
                               case XMLConstants.COORDINATESYSTEM :
                                    data.setCoordinateSystem((new Boolean(node.getNodeValue())).booleanValue());
                                    break;                                                                
                               default : break;
                             }
                           }
                         }
                       }
                       Integer n    = (Integer)parameters.get(nodeName);
                       //System.out.println("switch : "+n.toString());
                       if (n != null) {
                           switch ( n.intValue() ){                                  
                             case XMLConstants.STAR :
                                  iniObject.setParameter(data, XMLConstants.STAR);
                                  break;
                             case XMLConstants.PLANET :
                                  iniObject.setParameter(data, XMLConstants.PLANET);
                                  break;
                             case XMLConstants.MOON :
                                  iniObject.setParameter(data, XMLConstants.MOON);
                                  break;                                  
                             case XMLConstants.COMET :
                                  iniObject.setParameter(data, XMLConstants.COMET);
                                  break;   
                             case XMLConstants.SATELLITE :
                                  iniObject.setParameter(data, XMLConstants.SATELLITE);
                                  break;
                             case XMLConstants.ROCKET :
                                  iniObject.setParameter(data, XMLConstants.ROCKET);
                                  break; 
                             default :  break;                                 
                           }
                       }                           
                     }
                   
                }          
            }
          } 
        }    
    }
    
    /**
     * Returns a string representation of a given node type.
     *
     * @param node the node
     * @returns String a string representing this node type
     */
    private String nodeType(Node node) {
     int type = node.getNodeType();
     String str = "";
     switch ( type ) {

       case XMLConstants.ATTR_TYPE : str = "attribute_node";
            break;
       case XMLConstants.CDATA_TYPE : str = "cdata_section_node";
            break;              
       case XMLConstants.COMMENT_TYPE : str = "comment_node";
            break;       
       case XMLConstants.DOCFRAG_TYPE : str = "document_fragment_node";
            break;       
       case XMLConstants.DOCUMENT_TYPE : str = "document_node";
            break;       
       case XMLConstants.DOCTYPE_TYPE : str = "document_type_node";
            break;       
       case XMLConstants.ELEMENT_TYPE : str = "element_node";
            break;       
       case XMLConstants.ENTITY_TYPE : str = "entity_node";
            break;       
       case XMLConstants.ENTITYREF_TYPE : str = "entity_reference_node";
            break;       
       case XMLConstants.NOTATION_TYPE :  str = "notation_node";
            break;
       case XMLConstants.PROCINSTR_TYPE : str = "processsing_instruction_node";
            break;       
       case XMLConstants.TEXT_TYPE :  str = "text_node";
            break;       
       default : break;
     }
      return str;
    }

    
    /**
     * Method parses an XML file. Depending on the parameters and
     * their values it stores the information in the CelestialObjectInfo.
     *
     * @exception IOExecption throws an IOException in case of a read error
     */     
    private void parseCelestialObjectFile() throws IOException {
        
        // get current document from CelestialObjectInfo
        Document document    = cObjInfo.getDocument();
        Hashtable parameters = new Hashtable();
	
        // initialize hashtable
	      for (int i = XMLConstants.CELESTIALOBJECT_NAMES_MIN; 
                 i <= XMLConstants.CELESTIALOBJECT_NAMES_MAX; i++) {
	        parameters.put(XMLConstants.treeElementNames[i], new Integer(i));
        }
                
        if ( document != null ) {
        
          // iterate over each possible element tag
          // defined in class XMLConstants
          for(int tag = 0; tag < XMLConstants.treeElementNames.length; tag++ ) {
          
            // get node of specific tag
            // show only leaf nodes
            String nodeName = XMLConstants.treeElementNames[tag];
          
            if ( !nodeName.equals("celestialobject") &&
                 !nodeName.equals("general") &&
                 !nodeName.equals("parameter") ) {
               
                 NodeList childNodes = document.getElementsByTagName(nodeName);
                 
                 // search for value of node == nodeName
                 for(int i = 0; i < childNodes.getLength(); i++) {
                     
                     Node child = childNodes.item(i);
                     
                     NodeList nl = child.getChildNodes();
                     
                     for(int j = 0; j < childNodes.getLength(); j++) {
                       if ( nl.item(j).getNodeType() == XMLConstants.TEXT_TYPE ) {
                       
                         // get this value and set it correctly inside 
                         // CelestialObjectInfo.....
                         Integer n    = (Integer)parameters.get( child.getNodeName() );
                         String value = nl.item(j).getNodeValue();
                         
                         /*System.out.println("Parameter = "+child.getNodeName()+
                                            " Value = "+nl.item(j).getNodeValue());
                         */
                         
                         if (n != null) {
                           switch ( n.intValue() ){
                           
                             case  0 :         // celestialobject
                             case  1 :         // general
                             case  2 : break;  // parameter
                             
                             case  3 :         // type  
                                       cObjInfo.setType( value );
                                       break;
                             case  4 :         // name
                                       cObjInfo.setName( value );
                                       break;
                             case  5 :         // name
                                       cObjInfo.setParentName( value );
                                       break;                                       
                             case  6 :         // scalingfactor
                                       cObjInfo.setScalingFactor( (new Double( value )).doubleValue() );
                                       break;
                             case  7 :         // diameter
                                       cObjInfo.setDiameter( (new Double( value )).doubleValue() );
                                       break;
                             case  8 :         // mindistancefromsun
                                       cObjInfo.setMinDistanceFromSun( (new Double( value )).doubleValue() );
                                       break;
                             case  9 :         // maxdistancefromsun
                                       cObjInfo.setMaxDistanceFromSun( (new Double( value )).doubleValue() );
                                       break;
                             case 10 :         // nbrofplanetpositions
                                       cObjInfo.setNbrOfPositions( (new Integer( value )).intValue() );
                                       break;
                             case 11 :         // rotationperiod 
                                       cObjInfo.setRotationPeriod( (new Double( value )).doubleValue() );
                                       break;
                             case 12 :         // orbitperiod
                                       cObjInfo.setOrbitPeriod( (new Double( value )).doubleValue() );
                                       break;
                             case 13 :         // meanorbitvelocity
                                       cObjInfo.setMeanOrbitVelocity( (new Double( value )).doubleValue() );
                                       break;
                             case 14 :         // orbiteccentricity
                                       cObjInfo.setOrbitEccentricity( (new Double( value )).doubleValue() );
                                       break;
                             case 15 :         // orbitinclinationtoecliptic
                                       cObjInfo.setOrbitInclinationToEcliptic( (new Double( value )).doubleValue() );
                                       break;
                             case 16 :         // inclinationofequatortoorbit
                                       cObjInfo.setInclinationOfEquatorToOrbit( (new Double( value )).doubleValue() );
                                       break;
                             case 17 :         // volume
                                       cObjInfo.setVolume( (new Double( value )).doubleValue() );
                                       break;
                             case 18 :         // distance
                                       cObjInfo.setDistance( (new Double( value )).doubleValue() );
                                       break;
                             case 19 :         // mass
                                       cObjInfo.setMass( (new Double( value )).doubleValue() );
                                       break;
                             case 20 :         // density
                                       cObjInfo.setDensity( (new Double( value )).doubleValue() );
                                       break;
                             case 21 :         // surfacegravity
                                       cObjInfo.setSurfaceGravity( (new Double( value )).doubleValue() );
                                       break;
                             case 22 :         // escapevelocity
                                       cObjInfo.setEscapeVelocity( (new Double( value )).doubleValue() );
                                       break;
                             case 23 :         // meantempatsolidsurface
                                       cObjInfo.setTempAtSolidSurface( (new Double( value )).doubleValue() );
                                       break;
                             case 24 :         // majoratmosphericconstitutents
                                       cObjInfo.setAtmosphericConstitutents( value );
                                       break;
                                       
                             case 25 :         // epoch
                                       cObjInfo.setEpoch( (new Double( value )).doubleValue() );
                                       break;

                             case 26 :         // equinox
                                       cObjInfo.setEquinox( (new Double( value )).doubleValue() );
                                       break;

                             case 27 :         // longitudenode
                                       cObjInfo.setLongitudeNode( (new Double( value )).doubleValue() );
                                       break;

                             case 28 :         // perihelion
                                       cObjInfo.setPerihelion( (new Double( value )).doubleValue() );
                                       break;

                             default : break;
                           } 
                                   
                         }
                      }
                   }
                }          
             }
          } 
       }
    }

    
    /**
     * Method opens the XML file and calls the private method parse() to
     * parse the XML Document.
     *
     * @param type the type of the xml file
     */  
    public void read(int type) {

       DocumentBuilderFactory factory =
          DocumentBuilderFactory.newInstance();
       try {
          DocumentBuilder builder = factory.newDocumentBuilder();
          
          if ( type == XMLConstants.XML_CELESTIALOBJECT ) {
               System.out.println(path);
               File file = new File(path);
               if ( file != null ) {
                 cObjInfo.setDocument( builder.parse( file ) );
                 parseCelestialObjectFile();
               }
          } else if ( type == XMLConstants.XML_INITIALIZATION ) {
               //System.out.println("XML_INITIALIZATION "+path+" "+XMLConstants.DOCUMENT);

               iniObject.setParameter( builder.parse( new File(path)),
                                       XMLConstants.DOCUMENT );
               parseInitializationFile();
          }
          
       } catch (SAXParseException spe) {
          // Error generated by the parser
          System.out.println ("\n** Parsing error" 
             + ", line " + spe.getLineNumber ()
             + ", uri " + spe.getSystemId ());
          System.out.println("   " + spe.getMessage() );

          // Use the contained exception, if any
          Exception  x = spe;
          if (spe.getException() != null)
              x = spe.getException();
          x.printStackTrace();

       } catch (SAXException sxe) {
          // Error generated by this application
          // (or a parser-initialization error)
          Exception  x = sxe;
          if (sxe.getException() != null)
              x = sxe.getException();
          x.printStackTrace();

       } catch (ParserConfigurationException pce) {
           // Parser with specified options can't be built
           pce.printStackTrace();

       } catch (IOException ioe) {
          // I/O error
          ioe.printStackTrace();
       }	

    } // end function read()
    
}// end class XMLParser
