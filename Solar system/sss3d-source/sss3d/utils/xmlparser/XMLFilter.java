/*
  File: XMLFilter.java

  University of Applied Science Berne,HTA-Biel/Bienne,
  Computer Science Department.

  Diploma thesis J3D Solar System Simulator
  Originally written by Marcel Portner & Bernhard Hari (c) 2000

  CVS - Information :

  $Header: /var/cvsreps/projects/c450/2000/sss3d/source_diploma/sss3d/utils/xmlparser/XMLFilter.java,v 1.1 2000/11/17 15:53:58 harib Exp $
  $Author: harib $
  $Date: 2000/11/17 15:53:58 $
  $State: Exp $

*/
package sss3d.utils.xmlparser;

import java.io.File;
import javax.swing.*;
import javax.swing.filechooser.*;

/**
 * This filter is used to display only the xml files
 * inside the file chooser panel
 *   
 * @author Marcel Portner & Bernhard Hari
 * @version $Revision: 1.1 $
 */
public class XMLFilter extends FileFilter {
    
    /**
     * Accept all directories and all xml files.
     *
     * @return boolean true if this file is an xml file or directory
     */
    public boolean accept(File f) {
        if (f.isDirectory()) {
            return true;
        }

        String extension = null;
        String s = f.getName();
        
        int i = s.lastIndexOf('.');

        if (i > 0 &&  i < s.length() - 1) {
            extension = s.substring(i+1).toLowerCase();
        }

        if (extension != null) {
            if ( extension.equals("xml") ) {
                    return true;
            } else {
                return false;
            }
        }

        return false;
    }
    
    /**
     * The description of this filter
     *
     * @return String the description of this filter
     */
    public String getDescription() {
        return "XML Files";
    }
}
