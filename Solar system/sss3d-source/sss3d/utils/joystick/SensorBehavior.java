/*
  File: SensorBehavior.java

  University of Applied Science Berne,HTA-Biel/Bienne,
  Computer Science Department.

  Diploma thesis J3D Solar System Simulator
  Originally written by Marcel Portner & Bernhard Hari (c) 2000
  
  CVS - Information :
  
  $Header: /var/cvsreps/projects/c450/2000/sss3d/source_diploma/sss3d/utils/joystick/SensorBehavior.java,v 1.2 2000/12/12 13:03:28 harib Exp $
  $Author: harib $
  $Date: 2000/12/12 13:03:28 $
  $State: Exp $
  
*/
package sss3d.utils.joystick;

import javax.media.j3d.*;
import java.util.*;
import javax.media.j3d.*;
import java.util.*;
import javax.vecmath.*;

/**
 * This class is used to control a rocket with a joystick.
 * 
 * @author Marcel Portner & Bernhard Hari
 * @version $Revision: 1.2 $
 */
public class SensorBehavior extends Behavior {

	private WakeupOnElapsedFrames conditions = new WakeupOnElapsedFrames(0);
	private TransformGroup transformGroup;
	private Sensor sensor;
	private Transform3D transform = new Transform3D();

  /**
   * Creates a sesor behavior.
   * 
   * @param tg a modifiable transform group
   * @param sensor a sensor object 
   */    
	public SensorBehavior( TransformGroup tg, Sensor sensor ) {
		transformGroup = tg;
		this.sensor = sensor;
	}

  /**
   * initializing this behavior
   */
	public void initialize() {
		wakeupOn(conditions);
	}

  /**
   * Processes the Stimulus.
   * Currently called after each elapsed frame.
   *
   * @param criteria an enumeration
   */
	public void processStimulus(Enumeration criteria) {
        
    Transform3D old = new Transform3D();
	  transformGroup.getTransform(old);

    Vector3d offset = new Vector3d();
    old.get(offset);
  
     
    transform.setZero();    
	  sensor.getRead(transform);
    transform.setTranslation(offset);
    transformGroup.setTransform(transform);

		wakeupOn(conditions);
	}
}