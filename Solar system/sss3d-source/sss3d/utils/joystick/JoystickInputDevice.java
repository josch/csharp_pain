/*
  File: JoystickInputDevice.java

  University of Applied Science Berne,HTA-Biel/Bienne,
  Computer Science Department.

  Diploma thesis J3D Solar System Simulator
  Originally written by Marcel Portner & Bernhard Hari (c) 2000
  
  CVS - Information :
  
  $Header: /var/cvsreps/projects/c450/2000/sss3d/source_diploma/sss3d/utils/joystick/JoystickInputDevice.java,v 1.2 2000/12/12 13:03:29 harib Exp $
  $Author: harib $
  $Date: 2000/12/12 13:03:29 $
  $State: Exp $
  
*/
package sss3d.utils.joystick;

import Joystick;
import javax.media.j3d.*;
import javax.vecmath.*;

/**
 * JoystickInputDevice is the class through which Java 3D and Java 3D application programs communicate
 * with a joystick device driver. The joystick driver must be registered  
 * with Java 3D via a call toPhysicalEnvironment.addInputDevice(InputDevice).
 * An joystick input device transfers information to the Java 3D implementation and
 * Java 3D applications by writing transform information to sensors that the joystick device
 * driver has created and manages.The joystick driver can update its sensor information each
 * time the pollAndProcessInput method is called.
 * 
 * @author Marcel Portner & Bernhard Hari
 * @version $Revision: 1.2 $
 */
public class JoystickInputDevice implements InputDevice {
	/**
	 * If a Joystick is available and running, this Flag will be true.
	 */
	public static boolean jStickAvailable=true;
	private Joystick		joy				;
	private Sensor			joySensor		;
	private SensorRead	joySensorRead	= new SensorRead(4);//4 Buttons
	private Transform3D	joyTransform	= new Transform3D();
	private Transform3D	rotTransform	= new Transform3D();
	private Transform3D	rotTransformX	= new Transform3D();
	private float			  joyPos[]		  = new float[3];
	private float			  joyRot[]		  = new float[3];
	private Vector3f		joyTransVec		= new Vector3f();
	private float			  sensitivity		= 1.0f;
	private float			  angularRate		= 1.0f;
	private float			  minValue		  = 0.1f;
	private float			  x, y, z, firstX, firstY;
	private int				  buttons[]		  = new int[4];
	private int				  button			  = 0;
  private boolean     startup       = true;

  /**
   * Creates an JoystickInputDevice given by its id.
   *
   * @param device the device id
   */
	public JoystickInputDevice(int device) {
		try {
		  if (jStickAvailable)
			   joy = new Joystick(device);
		  else System.out.println("No Joystick!!");
		}
		catch (SecurityException se) {
			System.out.println("No Joystick!!");
			jStickAvailable=false;
		}
		catch (UnsatisfiedLinkError ule) {
			System.out.println("No Joystick!!");
			jStickAvailable=false;
		}

		joySensor = new Sensor(this, 1, 4);
	}
	
	/**
	 * Sets the Joystick Id.
	 * 
	 * @param device the id of the device
	 */
	public void setJoyDeviceId(int device){
		try {
		    if (jStickAvailable)
			    joy = new Joystick(device);
		    else System.out.println("No Joystick!!");
		}
		catch (SecurityException se) {System.out.println("No Joystick!!");}
		catch (UnsatisfiedLinkError ule) {System.out.println("No Joystick!!");}
	}
		

  /**
   * Initialize the joystick position and rotation.
   *
   * @returns true if an instance of a joystick exists
   */ 
	public boolean initialize() {
		for (int j=0; j<3; j++) {
				joyPos[j]	= 0.0f;
				joyRot[j]	= 0.0f;
		}
		return (joy!=null);
	}

  /**
   * Not used.
   */
	public void close() {
	}

  /**
   * Returns always DEMAND_DRIVEN;
   */
	public int getProcessingMode() {
		return DEMAND_DRIVEN;
	}

  /**
   * Returns always 1;
   */  
	public int getSensorCount() {
		return 1;
	}

  /**
   * Returns the Sensor object given by the id
   *
   * @returns Sensor the sensor object
   */
	public Sensor getSensor(int id)  {
		return joySensor;
	}

  /**
   * Not used. Mode constantly set to DEMAND_DRIVEN
   */  
	public void setProcessingMode(int mode) {
	}

  /**
   * Processes the input of the joystick
   * 
   * @param joy a reference to the joystick object
   * @param joySensor a reference to the sensor object
   * @param joyPos[] a three-dimensional position array
   * @param joyRot[] a three-dimensional rotation array
   */
	public final void pollAndProcessInput(Joystick joy, Sensor joySensor, float joyPos[], float joyRot[]) {
    
      if ( startup ) {
        firstX = joy.getXPos();
		    firstY = joy.getYPos(); 
        joySensorRead.setTime(System.currentTimeMillis());
        joyTransform.setIdentity();
		    joySensorRead.set( joyTransform );
		    joySensorRead.setButtons( buttons );
		    joySensor.setNextSensorRead( joySensorRead );
        startup = false;
      } else  {
        
        button = joy.getButtons();
    		buttons[0]=(button & Joystick.BUTTON1);
    		buttons[1]=(button & Joystick.BUTTON2);
    		buttons[2]=(button & Joystick.BUTTON3);
    		buttons[3]=(button & Joystick.BUTTON4);
    		
    		if ( (buttons[0] != 0) &&  (buttons[1] != 0)) {
    			 setNominalPositionAndOrientation(); //reset view...
           //System.out.println("Button 1 & 2 pressed");
    			return;
    		} 
        if ( buttons[2] != 0 )  {
           // increase speed
           // TODO
           //System.out.println("Button 3 pressed");
        }
        if ( buttons[3] != 0 )  {
           // reduce speed
           // TODO
           //System.out.println("Button 4 pressed");
        }
            
    		x = joy.getXPos();
    		y = joy.getYPos(); 
            
        //System.out.println("X Pos : "+x+" Y Pos"+y+" first time x"+firstX+" first time y"+firstY);
    
    		if (Math.abs(x) < minValue)
    			x = 0.0f;
    		if (Math.abs(y) < minValue)
    			y = 0.0f;
    
    		if (x>0.0f) {
     			joyRot[1] -= (x-minValue) * angularRate;
                System.out.println("x > 0");    
    		}
    		else if (x<0.0f) {
     			joyRot[1] -= (x+minValue) * angularRate;
    			System.out.println("x < 0");
    		}
    		if (y>0.0f) {
     			joyRot[0] += (y-minValue) * angularRate;
                System.out.println("y > 0");    
    		}
    		else if (y<0.0f) {
     			joyRot[0] += (y+minValue) * angularRate;
                System.out.println("y < 0");    
    		}
    
     		//joyPos[2] += y * sensitivity;
    		
    
     		//joyTransVec.x = joyPos[0];
     		//joyTransVec.y = joyPos[1];
     		//joyTransVec.z = joyPos[2];
    
     		rotTransform.setIdentity();
     		rotTransform.rotZ(joyRot[1]);

     		rotTransformX.setIdentity();
    		rotTransformX.rotX(joyRot[0]);      
     		joyTransform.setIdentity();
    		//joyTransform.set(joyTransVec);
        joyTransform.mul(rotTransformX);
    		joyTransform.mul(rotTransform);
            
        joySensorRead.setTime(System.currentTimeMillis());
    		joySensorRead.set( joyTransform );
    		joySensorRead.setButtons( buttons );
    		joySensor.setNextSensorRead( joySensorRead );
      }
	}

  /**
   * Calls method pollAndProcessInput(joy, joySensor, joyPos, joyRot).
   */
	public final void pollAndProcessInput() {
		pollAndProcessInput(joy, joySensor, joyPos, joyRot);
	}

  /**
   * Not used.
   */
	public void processStreamInput() {
	}

  /**
   * Sets the object to the initial position.
   */
	public final void setNominalPositionAndOrientation() {
		initialize();
		joySensorRead.setTime(System.currentTimeMillis());
		joyTransform.setIdentity();
		joySensorRead.set(joyTransform);
		joySensor.setNextSensorRead(joySensorRead);
	}

  /**
   * Sets the sensitivity of the joystick device.
   *
   * @param value the sensitivity
   */
	public void setSensitivity(float value) {
		sensitivity = value;
		//System.out.println("Sensitivity="+value);
	}

  /**
   * Returns the sensitivity of the joystick device.
   *
   * @returns float the sensitivity
   */
	public float getSensitivity() {
		return sensitivity;
	}

  /**
   * Sets the angular rate of the joystick device.
   *
   * @param value the angular rate
   */  
	public void setAngularRate(float value) {
		angularRate = value;
		//System.out.println("AngularRate="+value);
	}
	
  /**
   * Returns the angular rate of the joystick device.
   *
   * @returns float the angular rate
   */
	public float getAngularRate() {
		return angularRate;
	}
	
}
