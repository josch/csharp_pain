/*
  File: ButtonSensorBehavior.java

  University of Applied Science Berne,HTA-Biel/Bienne,
  Computer Science Department.

  Diploma thesis J3D Solar System Simulator
  Originally written by Marcel Portner & Bernhard Hari (c) 2000
  
  CVS - Information :
  
  $Header: /var/cvsreps/projects/c450/2000/sss3d/source_diploma/sss3d/utils/joystick/ButtonSensorBehavior.java,v 1.3 2000/12/13 13:41:49 portm Exp $
  $Author: portm $
  $Date: 2000/12/13 13:41:49 $
  $State: Exp $
  
*/
package sss3d.utils.joystick;

import javax.media.j3d.*;
import java.util.*;

/**
 * This Class can process the Input from the Joystick Buttons.
 * Up to now, it will only print out the Button number witch has been pressed.
 * For further functionality you may add some other Objects to the Constructor.
 * For the description of the Methods, see the Behavior Class!
 *
 * @version 1.1, June 1999
 * @author  M. Schlegel, HTA Biel
 * @see javax.media.j3d.Behavior
 */
public class ButtonSensorBehavior extends Behavior { 

	private WakeupCriterion conditions = new WakeupOnElapsedFrames(4);
	private Sensor 	sensor;
	private int		buttons[]= new int[4];

	/**
	 * The sensorobject must support the method "lastButtons(buttons[])" to make this class working
	 */
	public ButtonSensorBehavior(Sensor sensor ) {
		this.sensor = sensor;
	}

  /**
   * Initializes this behavior.
   */
	public void initialize() {
		wakeupOn(conditions);
	}
	
  /**
   * Processes the Stimulus.
   * Currently called after four elapsed frames.
   *
   * @param criteria an enumeration
   */
	public void processStimulus(Enumeration criteria) {
		sensor.lastButtons(buttons);
		for (int i=0; i<4;i++){
			if (buttons[i]!=0)
				System.out.println("Button "+(i+1)+" pressed!");
		}
		
		wakeupOn(conditions);
	}
}
