/*
  File: MyColor.java

  University of Applied Science Berne,HTA-Biel/Bienne,
  Computer Science Department.

  Diploma thesis J3D Solar System Simulator
  Originally written by Marcel Portner & Bernhard Hari (c) 2000

  CVS - Information :

  $Header: /var/cvsreps/projects/c450/2000/sss3d/source_diploma/sss3d/contentbranch/MyColor.java,v 1.3 2000/12/12 14:29:07 harib Exp $
  $Author: harib $
  $Date: 2000/12/12 14:29:07 $
  $State: Exp $

*/

package sss3d.contentbranch;

import java.awt.*;
import java.util.Hashtable;

/**
 * Because java.awt.Color doesn't support a constructor
 * witch works with a given color name, we had to 
 * implement an own class MyColor.
 *   
 * @author Marcel Portner & Bernhard Hari
 * @version $Revision: 1.3 $
 */
public class MyColor{

  private Color color =  null;

  private Hashtable hashtable = new Hashtable();


  /**
   * Currently supported color names.
   */
  public static final int BLACK     =  0;
  public static final int BLUE      =  1;
  public static final int CYAN      =  2;
  public static final int DARKGRAY  =  3;
  public static final int GRAY      =  4;
  public static final int GREEN     =  5;
  public static final int LIGHTGRAY =  6;
  public static final int MAGENTA   =  7;
  public static final int ORANGE    =  8;
  public static final int PINK      =  9;
  public static final int RED       = 10;
  public static final int WHITE     = 11;
  public static final int YELLOW    = 12;
  
  public static final String[] COLORNAMES  =  {
    "black",
    "blue",
    "cyan",
    "darkgray",
    "gray",
    "green",
    "lightgray",
    "magenta",
    "orange",
    "pink",
    "red",
    "white",
    "yellow"
  };

  /**
   * Constructs a new mycolor object with the given color
   *
   * @param color a color object
   */
  public MyColor( Color color ) { 
      this.color = color;
  }

  /**
   * Constructs a new color with the given name
   *
   * @param colorName the name of the new color
   */
  public MyColor( String colorName ) { 
      nameToColor( colorName );
  }
  
  /**
   * Returns the current color
   *
   * @return Color the current color
   */
  public Color getColor() {
    return color;
  }
  
  /**
   * Sets the new color
   *
   * @param color an object reference to the new color
   */
  public void setColor( Color color ) {
    this.color = color;
  }
  
  /**
   * Sets the new color
   *
   * @param colorName the name of the new color
   */
  public void setColor( String colorName ) {
    nameToColor( colorName );
  }
  
  /**
   * Converts a color given by its name into an 
   * color object.
   *
   * @param colorName the name of the new color
   */ 
  private void nameToColor( String colorName ) {
    //initialize hashtable
    for (int i = 0; i < COLORNAMES.length ; i++) {
            hashtable.put(COLORNAMES[i], new Integer(i));
    }
    colorName = colorName.toLowerCase();
  
    Integer n    = (Integer)hashtable.get( colorName );
    if (n != null) {
       switch ( n.intValue() ){
        case BLACK :
             color = new Color( Color.black.getRed(),
                                Color.black.getGreen(),
                                Color.black.getBlue() 
             );
             break;
        case BLUE :
             color = new Color( Color.blue.getRed(),
                                Color.blue.getGreen(),
                                Color.blue.getBlue() 
             );        
             break;     
        case CYAN :
             color = new Color( Color.cyan.getRed(),
                                Color.cyan.getGreen(),
                                Color.cyan.getBlue() 
             );        
             break;    
        case DARKGRAY :
             color = new Color( Color.darkGray.getRed(),
                                Color.darkGray.getGreen(),
                                Color.darkGray.getBlue() 
             );        
             break;  
        case GRAY :
             color = new Color( Color.gray.getRed(),
                                Color.gray.getGreen(),
                                Color.gray.getBlue() 
             );        
             break;
        case GREEN :
             color = new Color( Color.green.getRed(),
                                Color.green.getGreen(),
                                Color.green.getBlue() 
             );        
             break;    
        case LIGHTGRAY :
             color = new Color( Color.lightGray.getRed(),
                                Color.lightGray.getGreen(),
                                Color.lightGray.getBlue() 
             );        
             break;
        case MAGENTA :
             color = new Color( Color.magenta.getRed(),
                                Color.magenta.getGreen(),
                                Color.magenta.getBlue() 
             );        
             break;
        case ORANGE :
             color = new Color( Color.orange.getRed(),
                                Color.orange.getGreen(),
                                Color.orange.getBlue() 
             );        
             break; 
        case PINK :
             color = new Color( Color.pink.getRed(),
                                Color.pink.getGreen(),
                                Color.pink.getBlue() 
             );        
             break;  
        case RED : 
             color = new Color( Color.red.getRed(),
                                Color.red.getGreen(),
                                Color.red.getBlue() 
             );        
             break;   
        case WHITE :
             color = new Color( Color.white.getRed(),
                                Color.white.getGreen(),
                                Color.white.getBlue() 
             );        
             break;    
        case YELLOW :
             color = new Color( Color.yellow.getRed(),
                                Color.yellow.getGreen(),
                                Color.yellow.getBlue() 
             );        
             break;
        default : 
             color = new Color( 255, 0, 0 );        
             break; 
       }
    }

  }

}