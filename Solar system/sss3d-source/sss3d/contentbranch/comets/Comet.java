/*
  File: Comet.java

  University of Applied Science Berne,HTA-Biel/Bienne,
  Computer Science Department.

  Diploma thesis J3D Solar System Simulator
  Originally written by Marcel Portner & Bernhard Hari (c) 2000
  
  CVS - Information :
  
  $Header: /var/cvsreps/projects/c450/2000/sss3d/source_diploma/sss3d/contentbranch/comets/Comet.java,v 1.2 2000/12/12 16:23:43 harib Exp $
  $Author: harib $
  $Date: 2000/12/12 16:23:43 $
  $State: Exp $
  
*/
package sss3d.contentbranch.comets;

import sss3d.contentbranch.*;
import sss3d.utils.SSS3dConstants;
import sss3d.calculations.constants.AstronomicalConstants;

import javax.media.j3d.*;
import javax.vecmath.*;
import com.sun.j3d.utils.geometry.Sphere;

/**
 * This class is generate a planet in our solar system.
 *
 * @author Marcel Portner & Bernhard Hari
 * @version $Revision: 1.2 $
 */
public class Comet extends Sphere implements CelestialObject {

   // The name of the current comet
   private String comet;

   private CelestialObjectInfo lnkCelestialObjectInfo;   

   /**
    * Construct a new comet Sphere. The initradius of the Sphere is one meter.
    *
    * @param comet  the name of the current comet.
    */
   public Comet(String comet) {
      super(INITRADIUS, Sphere.GENERATE_NORMALS |
            Sphere.GENERATE_TEXTURE_COORDS, DIVISON);
      this.comet = comet;
      lnkCelestialObjectInfo = new CelestialObjectInfo(comet, SSS3dConstants.COMET_TYPE);

      // Set up the appearance to this Sphere object
      this.setAppearance(createAppearance());
   }

   /**
    * Returns a CelestialObjectInfo of the current comet.
    * This object has all specific information of the current comet.
    *
    * @return a CelestialObjectInfo of the current comet.
    */
   public CelestialObjectInfo getInfo() {
      return lnkCelestialObjectInfo;
   }

   /**
    * Gets the ID of the current comet.
    *
    * @return the ID of the current comet.
    */
   public String getId() {
      return comet;
   }

   /**
    * Gets the radius of the current comet.
    *
    * @return the radius as a scaled factor of the current comet.
    */
   public float getRadius() {
      return (float)(lnkCelestialObjectInfo.getDiameter() / SSS3dConstants.SCALE);
   }

   /**
    * Gets the radius of the current comet.
    *
    * @return the radius as a logarithm factor of the current comet.
    */
   public float getLogRadius() {
      return (float)(Math.log(lnkCelestialObjectInfo.getDiameter() / 10) / 40);
   }

   /**
    * Gets the degree of the current comet.
    *
    * @return the degree (�) of the current comet.
    */
   public double getDegree() {
      return (double)lnkCelestialObjectInfo.getInclinationOfEquatorToOrbit();
   }

   /**
    * Gets the rotation speed to the own axis of the current comet.
    *
    * @return the rotation speed to the own axis in day of the current comet.
    * One day correspond to one second in the animation.
    */
   public long getRotOwnAxis() {
      return (long)(lnkCelestialObjectInfo.getRotationPeriod() * SECOND);
   }

   /**
    * Gets the rotation speed around the sun of the current comet.
    *
    * @return the rotation speed around the sun in year of the current comet.
    * One year correspond to 365.242190 seconds (~6 minute) in the animation.
    */
   public long getRotOrbit() {
      return (long)(lnkCelestialObjectInfo.getOrbitPeriod() * SECOND * AstronomicalConstants.DAYS);
   }

   /**
    * Set the appearance of this comet (sphere) whith the right
    * texture.
    *
    * @return the Appearance of the comet with his right texture.
    */
   private Appearance createAppearance() {

      // Ambient-diffuse-reflection coefficient
      Color3f diffAmb = new Color3f(1.0f, 1.0f, 1.0f); // white
      // Diffuse-reflection coefficient
      Color3f reflDiff = new Color3f(1.0f, 1.0f, 1.0f); // white
      // Specular-reflection coefficient (reflectance function)
      Color3f reflSpec = new Color3f(1.0f, 1.0f, 1.0f); // white
      // c = shininess: cos^c in the specular reflection
      float c = 1;
      // Emitted light
      Color3f lumEmise = new Color3f(0.2f, 0.2f, 0.2f); // black

      Appearance appearance = new Appearance();

      // Set up the optical properties.
      appearance.setMaterial(new Material(diffAmb, lumEmise, reflDiff, reflSpec, c));

      Texture2D texture = getPlanetTexture();
      if(texture != null) {
         // Set up the texture
         appearance.setTexture(texture);

         // Application mode of the texture
         TextureAttributes texAttr = new TextureAttributes();
         texAttr.setTextureMode(TextureAttributes.MODULATE); // there still are: BLEND, DECAL,
                                                             //                  and REPLACE
         appearance.setTextureAttributes(texAttr);
      }

      return appearance;
   }

   /**
    * Returns the texture of the current comet.
    *
    * @return the texture of the current comet.
    */
   private Texture2D getPlanetTexture() {

      // Loading of the texture
      NewTextureLoader newTextureLoader = new NewTextureLoader("images/" + getId() + ".jpg",
                                                               NewTextureLoader.GENERATE_MIPMAP);

      ImageComponent2D image;
      try {
         image = newTextureLoader.getImage();
      } catch(NullPointerException e) {
         return null;
      }

      int imageWidth = image.getWidth();
      int imageHeight = image.getHeight();

      Texture2D texture = new Texture2D(Texture.MULTI_LEVEL_MIPMAP, Texture.RGBA,
                                        imageWidth, imageHeight);
      int imageLevel = 0;

      texture.setImage(imageLevel, image);

      while (imageWidth > SMALLEST || imageWidth > SMALLEST) {
         imageLevel++;
         if (imageWidth > SMALLEST) imageWidth /= 2;
         if (imageHeight > SMALLEST) imageHeight /= 2;
         image = newTextureLoader.getScaledImage(imageWidth, imageHeight);
         texture.setImage(imageLevel, image);
      }

      texture.setMagFilter(Texture.MULTI_LEVEL_POINT);
      texture.setMinFilter(Texture.MULTI_LEVEL_POINT);

      return texture;
   }
}
