/*
  File: SaturnRing.java

  University of Applied Science Berne,HTA-Biel/Bienne,
  Computer Science Department.

  Diploma thesis J3D Solar System Simulator
  Originally written by Marcel Portner & Bernhard Hari (c) 2000
  
  CVS - Information :
  
  $Header: /var/cvsreps/projects/c450/2000/sss3d/source_diploma/sss3d/contentbranch/planets/SaturnRing.java,v 1.5 2000/12/12 15:56:24 harib Exp $
  $Author: harib $
  $Date: 2000/12/12 15:56:24 $
  $State: Exp $
  
*/
package sss3d.contentbranch.planets;

import sss3d.contentbranch.NewTextureLoader;

import javax.media.j3d.*;
import javax.vecmath.*;
import java.awt.Color;

/**
 * This class constructs a ring system for the planet saturn. 
 *
 * @author Marcel Portner & Bernhard Hari
 * @version $Revision: 1.5 $
 */
public class SaturnRing extends Shape3D {

   private static final double  RMIN   = 0.5;
   private static final double  RMAX   = 1.0;
   private static final Color3f COLOR  = new Color3f(Color.yellow);
   private static final int     VERTEX = 80;

   private static final TexCoord2f[] texCoord = {
                                                  new TexCoord2f(0.0f, 0.0f),
                                                  new TexCoord2f(1.0f, 0.0f),
                                                  new TexCoord2f(1.0f, 1.0f),
                                                  new TexCoord2f(0.0f, 1.0f)
                                                };

   private double  rMin, rMax;
   private Color3f color;
   private int     vertex;
   
   /**
    * Constructs a saturn ringsystem with the default values
    * RMIN, RMAX, COLOR, VERTEX.
    */
   public SaturnRing() {
      this(RMIN, RMAX, COLOR, VERTEX);
   }
   
   /**
    * Constructs a saturn ringsystem with the values
    * rMin and rMax. Default values used: COLOR and VERTEX.
    *
    * @param rMin the smallest radius of the ring system
    * @param rMax the biggest radius of the ring system
    */   
   public SaturnRing(double rMin, double rMax) {
      this(rMin, rMax, COLOR, VERTEX);
   }

   /**
    * Constructs a saturn ringsystem with the values
    * rMin, rMax and color. Default value used: VERTEX.
    *
    * @param rMin  the smallest radius of the ring system
    * @param rMax  the biggest radius of the ring system
    * @param color the color of the ring system
    */  
   public SaturnRing(double rMin, double rMax, Color3f color) {
      this(rMin, rMax, color, VERTEX);
   }

   /**
    * Constructs a saturn ringsystem with the values
    * rMin, rMax, color and vertex.
    *
    * @param rMin   the smallest radius of the ring system
    * @param rMax   the biggest radius of the ring system
    * @param color  the color of the ring system
    * @param vertex the number of points to build a ring
    */  
   public SaturnRing(double rMin, double rMax, Color3f color, int vertex) {
      this.rMin = rMin;
      this.rMax = rMax;
      this.color = color;
      this.vertex = vertex;
      this.setGeometry(createGeometry());
      this.setAppearance(createAppearance());
   }

   /**
    * Creates the geometry of the saturn ring system.
    *
    * @returns Geometry the geometry
    */
   private Geometry createGeometry() {

      QuadArray ring = new QuadArray(vertex,
                                     /*QuadArray.NORMALS |*/
                                     QuadArray.COORDINATES |
                                     QuadArray.COLOR_3 |
                                     QuadArray.TEXTURE_COORDINATE_2);

      int v = 0;
      double a = 0.0;
      double fixAngle = 2.0 * StrictMath.PI / (vertex - 2);

      while(v < vertex) {
         ring.setCoordinate(v++,
                            new Point3d(rMin * StrictMath.cos(a),
                                        0.0,
                                        rMin * StrictMath.sin(a)));
         ring.setCoordinate(v++,
                            new Point3d(rMax * StrictMath.cos(a),
                                        0.0,
                                        rMax * StrictMath.sin(a)));

         a = v * fixAngle;

         ring.setCoordinate(v++,
                            new Point3d(rMax * StrictMath.cos(a),
                                        0.0,
                                        rMax * StrictMath.sin(a)));
         ring.setCoordinate(v++,
                            new Point3d(rMin * StrictMath.cos(a),
                                        0.0,
                                        rMin * StrictMath.sin(a)));
      }

      for(int i = 0; i < vertex; i++) {
         ring.setTextureCoordinate(0, i, texCoord[i % 4]);
         ring.setColor(i, color);
      }
      
      return ring;
   }
   
   /**
    * Set the appearance of the saturn ring system whith the texture.
    *
    * @return the Appearance of the ring system with his texture.
    */
   private Appearance createAppearance() {

      // Ambient-diffuse-reflection coefficient
      Color3f diffAmb = new Color3f(1.0f, 1.0f, 1.0f);
      // Diffuse-reflection coefficient
      Color3f reflDiff = new Color3f(1.0f, 1.0f, 1.0f);
      // Specular-reflection coefficient (reflectance function)
      Color3f reflSpec = new Color3f(1.0f, 1.0f, 1.0f);
      // c = shininess: cos^c in the specular reflection
      float c = 1;
      // Emitted light
      Color3f lumEmise = new Color3f(0.2f, 0.2f, 0.2f);

      Appearance appearance = new Appearance();

      // Set up the optical properties.
      appearance.setMaterial(new Material(diffAmb, lumEmise, reflDiff, reflSpec, c));

      // Loading of the texture
      NewTextureLoader newTextureLoader = new NewTextureLoader("images/rings.jpg");

      Texture texture;
      try {
         texture = newTextureLoader.getTexture();
         appearance.setTexture(texture);
   
         // Application mode of the texture
         TextureAttributes texAttr = new TextureAttributes();
         texAttr.setTextureMode(TextureAttributes.MODULATE); // there still are: BLEND, DECAL,
                                                             //                  and REPLACE
         appearance.setTextureAttributes(texAttr);
      } catch(NullPointerException e) { }

      return appearance;
   }
}
