/*
  File: CelestialObjectInfo.java

  University of Applied Science Berne,HTA-Biel/Bienne,
  Computer Science Department.

  Diploma thesis J3D Solar System Simulator
  Originally written by Marcel Portner & Bernhard Hari (c) 2000
  
  CVS - Information :
  
  $Header: /var/cvsreps/projects/c450/2000/sss3d/source_diploma/sss3d/contentbranch/CelestialObjectInfo.java,v 1.15 2000/12/13 13:37:07 portm Exp $
  $Author: portm $
  $Date: 2000/12/13 13:37:07 $
  $State: Exp $
  
*/
package sss3d.contentbranch;

import sss3d.utils.SSS3dConstants;
import sss3d.utils.xmlparser.*;

import org.w3c.dom.Document;


/**
 * An instanciated class of this object contains all the 
 * information about a celestial object. The class provides
 * methods to retrieve information from these objects.<br>
 * The filename given to the constructor contains the name of the
 * file without the type. We used the file extension .xml for
 * the information (XML) files.<br> 
 * The next example creates an instance of an object, containing
 * information about the sun. To do this the constructor calls
 * the class method read () from the XMLParser object 
 * with the arguments this and path.
 * <pre>
 *    filename : sun.xml
 *    use CelestialObjectInfo cObjInfo = new CelestialObjectInfo("sun");
 * </pre> 
 *
 * @author Marcel Portner & Bernhard Hari
 * @version $Revision: 1.15 $
 * @see sss3d.utils.xmlparser.XMLParser
 */
public class CelestialObjectInfo{

    private XMLParser ifp;                     // for parsing and reading the
                                               // information files.
    private Document document = null;          // holds the whole document for
                                               // easier creating and updating 
                                               // the JTree inside InfoBox
                                               // org.w3c.dom.Document
    
    private String name = null;                // filename
    private String parent = null;              // parentname
        
    // Possible types : STAR, PLANET, MOON, SATELLITE, ROCKET, COMET, UNKNOWN    
    private int type = SSS3dConstants.UNKNOWN_TYPE;  
    
    // parameters
    private double scalingFactor = 0.0d;       // scaling factor for representation
    private double volume = 0.0d;              // volume of the object
    private double distance = 0.0d;            // distance between object and sun
    private double diameter = 0.0d;            // mean diameter of the object
    private double minDistanceFromSun = 0.0d;  // used for ellipse equation
    private double maxDistanceFromSun = 0.0d;  // used for ellipse equation
    private int    nbrOfPositions = 0;         // number of positions to calculate
                                               // the flight path of the object

    private double mass = 0.0d;                        // mass
    private double density = 0.0d;                     // density
    private double surfaceGravity = 0.0d;              // surface gravity
    private double escapeVelocity = 0.0d;              // escape velocity
    private double rotationPeriod = 0.0d;              // mean rotation period 
                                                       // around the own axe
    private double orbitPeriod = 0.0d;                 // mean rotation period
                                                       // around the sun 
    private double meanOrbitVelocity = 0.0d;           // orbit velocity
    private double orbitEccentricity = 0.0d;           // orbit eccentrycity
    private double orbitInclinationToEcliptic = 0.0d;  // orbit inclination to ecliptic
    private double inclinationOfEquatorToOrbit = 0.0d; // inclination of equator to orbit
    private double tempAtSolidSurface = 0.0d;          // temperature on solid surface
    private String atmosphericConstitutents = null;    // atmospheric constitutents
    
    // parameters for the kepler (comet)
    private double epoch = 0.0;            // Zeitpunkt des Periheldurchgangs
    private double equinox = 0.0;          // Aequinoktium der Bahnelemente
    private double longitudeNode = 0.0;    // Laenge des aufsteigenden Knotens in [�]
    private double perihelion = 0.0;       // Argument des Perihels in [�]

    /**
     * Constructs a new object from the file defined by the argument filename. 
     * @param     filename  name of file without extension.
     */
    public CelestialObjectInfo(String name, int type) {
      this.name = name;
      this.type = type;
      
      String path = "xml/" + name + ".xml"; // temporary until we introduce a better solution
      
      ifp = new XMLParser(this, XMLConstants.XML_CELESTIALOBJECT, path);
      ifp.read(XMLConstants.XML_CELESTIALOBJECT);    // read and set all values coresponding to this object

    }


    /**
     * Sets the document of the object given by the argument. 
     * @param     document  org.w3c.dom.Document
     */    
    public void setDocument(org.w3c.dom.Document document) {
      this.document = document;
    }

    /**
     * Returns the DOM document of the object.
     * @return    org.w3c.dom.Document the DOM document
     */
    public org.w3c.dom.Document getDocument() {
      return document;
    }


    /**
     * Sets the type of the object given by the argument. 
     * @param     type  type of the object
     */    
    public void setType(String str) {
      int type = SSS3dConstants.UNKNOWN_TYPE;
      
      if( str.equals( SSS3dConstants.TYPES[SSS3dConstants.UNKNOWN_TYPE] )) {
        type = SSS3dConstants.UNKNOWN_TYPE; 
      } else if( str.equals( SSS3dConstants.TYPES[SSS3dConstants.INI_TYPE] )) {
        type = SSS3dConstants.INI_TYPE; 
      } else if( str.equals( SSS3dConstants.TYPES[SSS3dConstants.STAR_TYPE] )) {
        type = SSS3dConstants.STAR_TYPE; 
      } else if( str.equals( SSS3dConstants.TYPES[SSS3dConstants.PLANET_TYPE] )) {
        type = SSS3dConstants.PLANET_TYPE; 
      } else if( str.equals( SSS3dConstants.TYPES[SSS3dConstants.MOON_TYPE] )) {
        type = SSS3dConstants.MOON_TYPE; 
      } else if( str.equals( SSS3dConstants.TYPES[SSS3dConstants.COMET_TYPE] )) {
        type = SSS3dConstants.COMET_TYPE; 
      } else if( str.equals( SSS3dConstants.TYPES[SSS3dConstants.SATELLITE_TYPE] )) {
        type = SSS3dConstants.SATELLITE_TYPE; 
      } else if( str.equals( SSS3dConstants.TYPES[SSS3dConstants.ROCKET_TYPE] )) {
        type = SSS3dConstants.ROCKET_TYPE; 
      } else {
        System.out.println("Error in CelestialObjectInfo: " + str);
      }

      this.type = type;
    }
    /**
     * Returns the type of the object (integer).
     * @return    int the type of the object
     */
    public int getTypeNbr() {
      return type;
    }
    
    /**
     * Returns the type of the object. (string representation)
     * @return    String the type of the object
     */
    public String getType() {
      String str = "";
      switch( type ){
        case SSS3dConstants.UNKNOWN_TYPE : 
             str = SSS3dConstants.TYPES[SSS3dConstants.UNKNOWN_TYPE];
             break;
        case SSS3dConstants.INI_TYPE : 
             str = SSS3dConstants.TYPES[SSS3dConstants.INI_TYPE];
             break;
        case SSS3dConstants.STAR_TYPE : 
             str = SSS3dConstants.TYPES[SSS3dConstants.STAR_TYPE];
             break;
        case SSS3dConstants.PLANET_TYPE : 
             str = SSS3dConstants.TYPES[SSS3dConstants.PLANET_TYPE];
             break;
        case SSS3dConstants.MOON_TYPE : 
             str = SSS3dConstants.TYPES[SSS3dConstants.MOON_TYPE];
             break;
        case SSS3dConstants.COMET_TYPE : 
             str = SSS3dConstants.TYPES[SSS3dConstants.COMET_TYPE];
             break;
        case SSS3dConstants.SATELLITE_TYPE : 
             str = SSS3dConstants.TYPES[SSS3dConstants.SATELLITE_TYPE];
             break;
        case SSS3dConstants.ROCKET_TYPE : 
             str = SSS3dConstants.TYPES[SSS3dConstants.ROCKET_TYPE];
             break;
        default : 
             str = SSS3dConstants.TYPES[SSS3dConstants.UNKNOWN_TYPE];
      }
      return str;
    }


    /**
     * Sets the name/id of the object given by the argument. 
     * @param     name  name of the object
     */    
    public void setName(String name) {
      this.name = name;
    }

    /**
     * Returns the name/id of the object.
     * @return    String the name of the object
     */
    public String getName() {
      return name;
    }

    /**
     * Sets the name/id of the parent object given by the argument. 
     * @param     parent  name of the parent object
     */    
    public void setParentName(String parent) {
      this.parent = parent;
    }

    /**
     * Returns the name/id of the parent object.
     * @return    String the name of the parent object
     */
    public String getParentName() {
      return parent;
    }
    
    /**
     * Sets the scaling factor of the object given by the argument. 
     * @param     scalingFactor  scaling value
     */
    public void setScalingFactor(double scalingFactor){
      this.scalingFactor = scalingFactor;
    }

    /**
     * Returns the scaling factor of the object.
     * @return    double the scaling factor of the object
     */
    public double getScalingFactor(){
      return scalingFactor;
    }

    /**
     * Sets the diameter of the object given by the argument. 
     * @param     diameter  diameter of the object
     */
    public void setDiameter(double diameter) {
      this.diameter = diameter;
    }

    /**
     * Returns the diameter of the object.
     * @return    double the diameter of the object
     */
    public double getDiameter() {
      return diameter;
    }

    /**
     * Sets the minimum distance between the sun and the object given by the argument. 
     * @param     distance  minimum distance between the object and the sun
     */
    public void setMinDistanceFromSun(double distance) {
      this.minDistanceFromSun = distance;
    }

    /**
     * Returns the minimum distance between the sun and the object.
     * @return    double minimum distance between the object and the sun
     */
    public double getMinDistanceFromSun() {
      return minDistanceFromSun;
    }

    /**
     * Sets the maximum distance between the sun and the object given by the argument. 
     * @param     distance  maximum distance between the object and the sun
     */
    public void setMaxDistanceFromSun(double distance) {
      this.maxDistanceFromSun = distance;
    }

    /**
     * Returns the maximum distance between the sun and the object.
     * @return    double maximum distance between the object and the sun
     */
    public double getMaxDistanceFromSun() {
      return maxDistanceFromSun;
    }
    
    /**
     * Sets the number of positions for the flight path of the object. 
     * @param     pos  number of positons
     */    
    public void setNbrOfPositions(int pos) {
      this.nbrOfPositions = pos;	
    }
    
    /**
     * Returns the number of positions for the flight path of the object.
     * @return    int number of positons
     */    
    public int getNbrOfPositions() {
      return nbrOfPositions;	
    }

    /**
     * Sets the rotation period of the object around the own axe. 
     * @param     double rotation period
     */
    public void setRotationPeriod(double rotationPeriod) {
      this.rotationPeriod = rotationPeriod;
    }

    /**
     * Returns the rotation period of the object around the own axe.
     * @return    double rotation period
     */
    public double getRotationPeriod() {
      return rotationPeriod;
    }

    /**
     * Sets the period around the sun of the object. 
     * @param     orbitPeriod period around the sun
     */
    public void setOrbitPeriod(double orbitPeriod) {
      this.orbitPeriod = orbitPeriod;
    }

    /**
     * Returns the period around the sun of the object.
     * @return    double period around the sun
     */
    public double getOrbitPeriod() {
      return orbitPeriod;
    }

    /**
     * Sets the mean orbit velocity of the object. 
     * @param     meanOrbitVelocity mean orbit velocity
     */
    public void setMeanOrbitVelocity(double meanOrbitVelocity) {
      this.meanOrbitVelocity = meanOrbitVelocity;
    }

    /**
     * Returns the mean orbit velocity of the object.
     * @return    double mean orbit velocity
     */
    public double getMeanOrbitVelocity() {
      return meanOrbitVelocity;
    }

    /**
     * Sets the orbit eccentricity of the object. 
     * @param     orbitEccentricity
     */
    public void setOrbitEccentricity(double orbitEccentricity) {
      this.orbitEccentricity = orbitEccentricity;
    }

    /**
     * Returns the orbit eccentricity of the object.
     * @return    double orbit eccentricity
     */
    public double getOrbitEccentricity() {
      return orbitEccentricity;
    }
    
    /**
     * Sets the orbit inclination to ecliptic value of the object. 
     * @param     orbitInclinationToEcliptic orbit inclination to ecliptic
     */
    public void setOrbitInclinationToEcliptic(double orbitInclinationToEcliptic) {
      this.orbitInclinationToEcliptic = orbitInclinationToEcliptic;
    }

    /**
     * Returns the orbit inclination to ecliptic of the object.
     * @return    double orbit inclination to ecliptic
     */
    public double getOrbitInclinationToEcliptic() {
      return orbitInclinationToEcliptic;
    }

    /**
     * Sets the inclination of equator to orbit value of the object. 
     * @param     inclination of equator to orbit
     */
    public void setInclinationOfEquatorToOrbit(double inclinationOfEquatorToOrbit){
      this.inclinationOfEquatorToOrbit = inclinationOfEquatorToOrbit;
    }

    /**
     * Returns the inclination of equator to orbit of the object.
     * @return    double inclination of equator to orbit
     */
    public double getInclinationOfEquatorToOrbit() {
      return inclinationOfEquatorToOrbit;
    }

    /**
     * Sets the volume of the object. 
     * @param     volume volume of the object
     */
    public void setVolume(double volume) {
      this.volume = volume;
    }

    /**
     * Returns the volume of the object.
     * @return    double the volume of the object
     */
    public double getVolume() {
      return volume;
    }

    /**
     * Sets the distance between the sun and the object.
     * @param     distance distance between sun and the object
     */
    public void setDistance(double distance) {
      this.distance = distance;
    }

    /**
     * Returns the distance between the sun and the object.
     * @return    double distance between the sun and the object
     */
    public double getDistance() {
      return distance;
    }

    /**
     * Sets the mass of the object. 
     * @param     mass the mass of the object
     */
    public void setMass(double mass) {
      this.mass = mass;
    }

    /**
     * Returns the mass of the object.
     * @return    double the mass of the object
     */
    public double getMass() {
      return mass;
    }

    /**
     * Sets the density of the object. 
     * @param     density density of the object
     */
    public void setDensity(double density) {
      this.density = density;
    }

    /**
     * Returns the density of the object.
     * @return    double density of the object
     */
    public double getDensity() {
      return density;
    }

    /**
     * Sets the surface gravity of the object. 
     * @param     surfaceGravity surface gravity
     */
    public void setSurfaceGravity(double surfaceGravity) {
      this.surfaceGravity = surfaceGravity;
    }

    /**
     * Returns the surface gravity of the object.
     * @return    double surface gravity
     */
    public double getSurfaceGravity() {
      return surfaceGravity;
    }

    /**
     * Sets the escape velocity of the object. 
     * @param     escapeVelocity  escape velocity
     */
    public void setEscapeVelocity(double escapeVelocity) {
      this.escapeVelocity = escapeVelocity;
    }

    /**
     * Returns the escape velocity of the object.
     * @return    double escape velocity
     */
    public double getEscapeVelocity() {
      return escapeVelocity;
    }

    /**
     * Sets the temperature at the solid surface of the object. 
     * @param     temperature the temperature at the solid surface
     */
    public void setTempAtSolidSurface(double temperature) {
      this.tempAtSolidSurface = temperature;
    }

    /**
     * Returns the temperature at the solid surface of the object.
     * @return    double temperature at the solid surface
     */
    public double getTempAtSolidSurface() {
      return tempAtSolidSurface;
    }

    /**
     * Sets the atmospheric constitutents of the object. 
     * @param     constitutents atmospheric constitutents
     */
    public void setAtmosphericConstitutents(String constitutents) {
      this.atmosphericConstitutents = constitutents;
    }
    
    /**
     * Returns the atmospheric constitutents of the object.
     * @return    String atmospheric constitutents
     */
    public String getAtmosphericConstitutents() {
      return atmosphericConstitutents;
    }

    /**
     * Sets the epoch of the object. 
     * @param     epoch epoch of the object
     */
    public void setEpoch(double epoch) {
      this.epoch = epoch;
    }
    
    /**
     * Returns the epoch of the object.
     * @return    double the epoch of the object
     */
    public double getEpoch() {
      return epoch;
    }

    /**
     * Sets the equinox of the object. 
     * @param     equinox equinox of the object
     */
    public void setEquinox(double equinox) {
      this.equinox = equinox;
    }
    
    /**
     * Returns the equinox of the object.
     * @return    double the equinox of the object
     */
    public double getEquinox() {
      return equinox;
    }

    /**
     * Sets the longitudeNode of the object. 
     * @param     longitudeNode longitudeNode of the object
     */
    public void setLongitudeNode(double longitudeNode) {
      this.longitudeNode = longitudeNode;
    }
    
    /**
     * Returns the longitudeNode of the object.
     * @return    double the longitudeNode of the object
     */
    public double getLongitudeNode() {
      return longitudeNode;
    }

    /**
     * Sets the perihelion of the object. 
     * @param     perihelion perihelion of the object
     */
    public void setPerihelion(double perihelion) {
      this.perihelion = perihelion;
    }
    
    /**
     * Returns the perihelion of the object.
     * @return    double the perihelion of the object
     */
    public double getPerihelion() {
      return perihelion;
    }
}// end of class CelestialObjectInfo
