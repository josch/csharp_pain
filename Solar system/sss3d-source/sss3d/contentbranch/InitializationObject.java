/*
  File: InitializationObject.java

  University of Applied Science Berne,HTA-Biel/Bienne,
  Computer Science Department.

  Diploma thesis J3D Solar System Simulator
  Originally written by Marcel Portner & Bernhard Hari (c) 2000
  
  CVS - Information :
  
  $Header: /var/cvsreps/projects/c450/2000/sss3d/source_diploma/sss3d/contentbranch/InitializationObject.java,v 1.11 2000/12/12 14:29:07 harib Exp $
  $Author: harib $
  $Date: 2000/12/12 14:29:07 $
  $State: Exp $
  
*/

package sss3d.contentbranch;

import sss3d.utils.xmlparser.*;

import org.w3c.dom.Document;
import sss3d.utils.SSS3dConstants;

import java.util.Vector;
import java.util.Enumeration;

/**
 * An instanciated class of this object contains all the 
 * information about the J3D Solar System Simulator.
 *
 * @author Marcel Portner & Bernhard Hari
 * @version $Revision: 1.11 $
 */

public class InitializationObject{

    private XMLParser xmlp;                    // for parsing and reading the
                                               // information files.
    
    // Possible types : INI, STAR , PLANET, MOON, SATELLITE, ROCKET,COMET, UNKNOWN    
    private int id = SSS3dConstants.INI_TYPE;  


    
    // parameters
    private Document document = null;           // holds the whole document for
                                                // easier creating and updating 
                                                // the JTree inside InfoBox
                                                // org.w3c.dom.Document
    private String  type           = null;      // type of xml file                               
    private String  name           = null;      // name
    private String  filename       = null;      // filename
    private String  path           = null;      // current path
    private int     day            = 1;         // simulation start date & time
    private int     month          = 1;         
    private int     year           = 2000; 
    private int     hour           = 12;
    private int     minutes        = 0;
    private int     seconds        = 0;
    private double  jday           = 2451545.0d;
    private int     calculation    = SSS3dConstants.KEPLER;  // calculation method
    private AnimationSpeed animationspeed;      // animationspeed of simulation
    private boolean compressed     = true;
    private boolean glasses3d      = false;
    private boolean joystick       = false;
    private boolean cameraatorigin = false;
    private int     camera         = SSS3dConstants.CAMERA_DEFAULT; // initial camera position
    private Vector  objects        = new Vector(); 
    

                                               
    /**
     * Constructs a new object from the file defined by the argument filename. 
     * @param     filename  name of file without extension.
     */
    public InitializationObject(String path) {
      xmlp = new XMLParser(this,XMLConstants.XML_INITIALIZATION,path);
      // read and set all values coresponding to this object
      xmlp.read(XMLConstants.XML_INITIALIZATION); 
    }

    /**
     * Returns the data of a celestial object
     *
     * @param name the name of the celestial object
     * @return IniData the data of the celestial object
     */
    private IniData getData( String name ) {
      IniData data = null;
      
      if ( name != null ) {
      Enumeration enum = objects.elements();
        while ( enum.hasMoreElements() ) {
           IniData dataStructure = (IniData)enum.nextElement();
           if (dataStructure.getName().equals(name)) {
             data = dataStructure;
             break;
           }
        }
      }
    
      return data;
    }

    /**
     * Returns an enumeration of all currently loaded celestial objects
     *
     * @return Enumeration all currently loaded celestial objects
     */
    public Enumeration getObjects() {
      return objects.elements();
    }

    /**
     * Sets the object specified by the parameter.
     *
     * @param object the object associated to the parameter
     * @param parameter the desired parameter
     * @return boolean true if setting was successfully
     */  
    public boolean setParameter( Object object , int parameter ) {
      return setParameter( object, parameter, null );
    }
        
    /**
     * Sets the object specified by the parameter and the
     * name of the celestial object.
     *
     * @param object the object associated to the parameter
     * @param parameter the desired parameter
     * @param name the name of the celestial object
     * @return boolean true if setting was successfully
     */   
    public boolean setParameter( Object object , int parameter, String name ) {
      boolean result = false;
      IniData data;
      
      //System.out.println("Set Parameter : "+object.toString()+" Parameter : "+XMLConstants.treeElementNames[parameter]+" Name : "+name);
      switch ( parameter ) {

        case XMLConstants.FILETYPE :
             this.type = (String)object;
             result = true;
             break;
             
        case XMLConstants.ID :
             this.name = (String)object;
             result = true;
             break;
             
        case XMLConstants.FILENAME :
             this.filename = (String)object;
             result = true;
             break;
             
        case XMLConstants.PATH :
             this.path = (String)object;
             result = true;
             break;        
             
        case XMLConstants.DAY :
             int day = ((Integer)object).intValue();
             if ( day > 0 && day < 32 ) {
                this.day = day;
                result = true;
             }
             break;        
             
        case XMLConstants.MONTH :
             int month = ((Integer)object).intValue();
             if ( month > 0 && month < 13 ) {
                this.month = month;
                result = true;
             }             
             break;        
             
        case XMLConstants.YEAR :
             this.year = ((Integer)object).intValue();
             result = true;
             break;        
             
        case XMLConstants.HOUR :
             int hour = ((Integer)object).intValue();
             if( hour >= 0 && hour <= 24 ) {
               this.hour = hour;
               result = true;
             }
             break;        
             
        case XMLConstants.MINUTES :
             int minutes = ((Integer)object).intValue();
             if( minutes >= 0 && minutes <= 60 ) {
               this.minutes = minutes;
               result = true;
             }
             break;        
             
        case XMLConstants.SECONDS :
             int seconds = ((Integer)object).intValue();
             if( seconds >= 0 && seconds <= 60 ) {
               this.seconds = seconds;
               result = true;
             }
             break;
             
        case XMLConstants.JDAY :
             this.jday = ((Double)object).doubleValue();
             result = true;
             break;        
             
        case XMLConstants.CALCULATION :
             int calculation = ((Integer)object).intValue();
             if ( calculation == SSS3dConstants.KEPLER ||
                  calculation == SSS3dConstants.ELLIPSE ||
                  calculation == SSS3dConstants.ANALYTIC ) {
                  this.calculation = calculation;
                  result = true;
             }
             break;        
             
        case XMLConstants.ANIMATIONSPEED :
             this.animationspeed = new AnimationSpeed( ((AnimationSpeed)object).getType(),
                                                       ((AnimationSpeed)object).getValue() );
             result = true;                                                       
             break;        
             
        case XMLConstants.COMPRESSED :
             this.compressed = ((Boolean)object).booleanValue();
             result = true;
             break;        
             
        case XMLConstants.GLASSES3D :
             this.glasses3d = ((Boolean)object).booleanValue();
             result = true;
             break;        

        case XMLConstants.JOYSTICK :
             this.joystick = ((Boolean)object).booleanValue();
             result = true;
             break; 
             
        case XMLConstants.CAMERAATORIGIN :
             this.cameraatorigin = ((Boolean)object).booleanValue();
             result = true;
             break; 
                                       
        case XMLConstants.CAMERA :
             int camera = ((Integer)object).intValue();
             for( int c = 0; c < SSS3dConstants.CAMERA_POSITIONS.length ; c++ ) {
                if ( camera == c ) {
                  this.camera = camera;
                  result = true;                
                }
             }
             break;        
             
        case XMLConstants.STAR :
             data = getData( ((IniData)object).getName() );
             if ( data == null ) { // check if an element already exists
               objects.add((IniData)object);
               result = true;
             }
             break;        
             
        case XMLConstants.PLANET :
             data = getData( ((IniData)object).getName() );
             if ( data == null ) { // check if an element already exists
               objects.add((IniData)object);
               result = true;
             }
             break;        
             
        case XMLConstants.MOON :
             data = getData( ((IniData)object).getName() );
             if( data == null) {
               objects.add((IniData)object); 
               result = true;
             }
             break;        
             
        case XMLConstants.COMET :
             data = getData( ((IniData)object).getName() );
             if ( data == null ) { // check if an element already exists
               objects.add((IniData)object);
               result = true;
             }
             break;        
             
        case XMLConstants.SATELLITE :
             data = getData( ((IniData)object).getName() );
             if ( data == null ) { // check if an element already exists
               objects.add((IniData)object);
               result = true;
             }
             break;        
             
        case XMLConstants.ROCKET :
             data = getData( ((IniData)object).getName() );
             if ( data == null ) { // check if an element already exists
               objects.add((IniData)object);
               result = true;
             }
             break;  
                   
        case XMLConstants.PARENT :
             if ( name != null ) {
               data = getData(name);
               if ( data != null ) {
                 data.setParent( (String) object );
                 result = true;
               }
             }      
             break;  
        
        case XMLConstants.VISIBLE :
             if ( name != null ) {
               data = getData(name);
               if ( data != null ) {
                 data.setVisible( ((Boolean)object).booleanValue() );
                 result = true;
               }
             }      
             break;        
             
        case XMLConstants.ORBIT :
             if ( name != null ) {
               data = getData(name);
               if ( data != null ) {
                 data.setOrbit( ((Boolean)object).booleanValue() );
                 result = true;
               }
             }
             break;        
             
        case XMLConstants.COLORORBIT :
             if ( name != null ) {
               data = getData(name);
               if ( data != null ) {
                 data.setColorOrbit( ((MyColor)object).getColor() );
                 result = true;
               }
             }
             break;
                     
        case XMLConstants.COORDINATESYSTEM :
             if ( name != null ) {
               data = getData(name);
               if ( data != null ) {
                 data.setCoordinateSystem( ((Boolean)object).booleanValue() );
                 result = true;
               }
             }
             break;
        
             
        case XMLConstants.DOCUMENT :
             document = (Document)object;
             System.out.println(document.toString());
             result = true;
             break;
             
        default : break;
      }
      return result;
    }

    /**
     * Returns the object specified by the parameter.
     *
     * @param parameter the desired parameter
     * @return Object an object reference to the specified parameter
     */
    public Object getParameter( int parameter ) {
        return getParameter( parameter, null );
    }

    /**
     * Returns the object specified by the parameter and
     * the object name.
     *
     * @param parameter the desired parameter
     * @param name the name of the celestial object
     * @return Object an object reference to the specified parameter
     */
    public Object getParameter( int parameter, String name ) {
      Object object = null;
      IniData data;
      
      switch ( parameter ) {
        case XMLConstants.TYPE :
             object = type;
             break;
             
        case XMLConstants.NAME :
             object = name;
             break;
             
        case XMLConstants.FILENAME :
             object = filename;
             break;
             
        case XMLConstants.PATH :
             object = path;
             break;        
             
        case XMLConstants.DAY :
             object = new Integer(day);
             break;        
             
        case XMLConstants.MONTH :
             object = new Integer(month);
             break;        
             
        case XMLConstants.YEAR :
             object = new Integer(year);
             break;        
             
        case XMLConstants.HOUR :
             object = new Integer(hour);
             break;        
             
        case XMLConstants.MINUTES :
             object = new Integer(minutes);
             break;        
             
        case XMLConstants.SECONDS :
             object = new Integer(seconds);
             break;
             
        case XMLConstants.JDAY :
             object = new Double(jday);
             break;        
             
        case XMLConstants.CALCULATION :
             object = new Integer(calculation);
             break;        
             
        case XMLConstants.ANIMATIONSPEED :
             //if(animationspeed != null) System.out.println(animationspeed.toString());
             object = new AnimationSpeed(animationspeed.getType(), animationspeed.getValue());
             break;        
             
        case XMLConstants.COMPRESSED :
             object = new Boolean(compressed);
             break;        
             
        case XMLConstants.GLASSES3D :
             object = new Boolean(glasses3d);
             break;        

        case XMLConstants.JOYSTICK :
             object = new Boolean(joystick);
             break;  
             
        case XMLConstants.CAMERAATORIGIN :
             object = new Boolean(cameraatorigin);
             break;  
                                       
        case XMLConstants.CAMERA :
             object = new Integer(camera);
             break;        
             
        case XMLConstants.STAR :
             object = getData(name);
             break;        
             
        case XMLConstants.PLANET :
             object = getData(name);
             break;        
             
        case XMLConstants.MOON :
             object = getData(name);
             break;        
             
        case XMLConstants.COMET :
             object = getData(name);
             break;        
             
        case XMLConstants.SATELLITE :
             object = getData(name);
             break;        
             
        case XMLConstants.ROCKET :
             object = getData(name);
             break;        

        case XMLConstants.PARENT :
             data = getData(name);
             if ( data != null )  {
              object = data.getParent();
             }  
             break;
             
        case XMLConstants.VISIBLE :
             data = getData(name);
             if ( data != null )  {
              object = new Boolean(data.isVisible());
             }  
             break;        
             
        case XMLConstants.ORBIT :
             data = getData(name);
             if ( data != null )  {
              object = new Boolean(data.hasOrbit());
             } 
             break;        
             
        case XMLConstants.COLORORBIT :
             data = getData(name);
             if ( data != null )  {
              object = data.getColorOrbit();
             } 
             break;
                     
        case XMLConstants.COORDINATESYSTEM :
             data = getData(name);
             if ( data != null )  {
              object = new Boolean(data.hasCoordinateSystem());
             } 
             break;
             
        case XMLConstants.DOCUMENT :
             object = document;
             break;
             
        default : break;

      
      }
      return object;
    }


}// end of class CelestialObjectInfo
