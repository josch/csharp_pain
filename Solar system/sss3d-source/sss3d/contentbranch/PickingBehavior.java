/*
  File: PickingBehavior.java

  University of Applied Science Berne,HTA-Biel/Bienne,
  Computer Science Department.

  Diploma thesis J3D Solar System Simulator
  Originally written by Marcel Portner & Bernhard Hari (c) 2000
  
  CVS - Information :
  
  $Header: /var/cvsreps/projects/c450/2000/sss3d/source_diploma/sss3d/contentbranch/PickingBehavior.java,v 1.6 2000/11/29 09:26:59 portm Exp $
  $Author: portm $
  $Date: 2000/11/29 09:26:59 $
  $State: Exp $
  
*/
package sss3d.contentbranch;

import sss3d.utils.observer.*;

import javax.media.j3d.*;
import com.sun.j3d.utils.picking.behaviors.PickMouseBehavior;
import com.sun.j3d.utils.picking.PickResult;
import com.sun.j3d.utils.geometry.Sphere;
import java.awt.event.*;
import java.awt.AWTEvent;
import java.util.Enumeration;

/**
 * This class handle the picking in the scene.
 * So the celestial objects are pickable with the mouse.
 *
 * @author Marcel Portner & Bernhard Hari
 * @version $Revision: 1.6 $
 */
public class PickingBehavior extends PickMouseBehavior implements InfoObserver {

   private String id = "picking";
   private ObjectsInformation objInfo;

   // Wake up event
   private WakeupOnAWTEvent wakeUp = new WakeupOnAWTEvent(MouseEvent.MOUSE_RELEASED);

   /**
    * Initializes a new PickingBehavior.
    *
    * @param objRoot  the root of the current BranchGroup.
    * @param canvas  the Canvas3D where the scene is displayed.
    * @param bounds  a BoundingSphere for the picking radius.
    * @param objInfo  an instance to the InfoBox via ObjectsInformation.
    */
   public PickingBehavior(BranchGroup objRoot, Canvas3D canvas,
                          BoundingSphere bounds, ObjectsInformation objInfo) {
      super(canvas, objRoot, bounds);

      this.setSchedulingBounds(bounds);
      objRoot.addChild(this);

      // add picking behavior to info observer
      this.objInfo = objInfo;
      this.objInfo.attach(this);
   }

   /**
    * Override Behavior's initialize method to setup wakeup criteria.
    */
   public void initialize() {
      wakeupOn(wakeUp);
   }

   /**
    * Override Behavior's stimulus method to handle the event.
    * This method is called when the right mouse key has been pressed.
    *
    * @param criteria  all pressed keys in a list.
    */
   public void processStimulus(Enumeration criteria) {
      WakeupOnAWTEvent ev;
      AWTEvent[] events;
      MouseEvent eventMouse;

      if(criteria.hasMoreElements()) {
         ev = (WakeupOnAWTEvent)criteria.nextElement();
         events = ev.getAWTEvent();
         eventMouse = (MouseEvent)events[0];

         if(eventMouse.getModifiers() == MouseEvent.BUTTON3_MASK) {
            updateScene(eventMouse.getX(), eventMouse.getY());
         }
      }
      // Set wakeup criteria for next time.
      wakeupOn(wakeUp);
   }

   /**
    * This method is called when the left mouse key has been pressed.
    * It take the closest celestial object on the scene and show the
    * information about this celestial object in the InfoBox.
    *
    * @param xpos  the x position of the mouse.
    * @param ypos  the y position of the mouse.
    */
   public void updateScene(int xpos, int ypos) {
      pickCanvas.setShapeLocation(xpos, ypos);
      PickResult pickResult = pickCanvas.pickClosest();
      // only CelestialObjects are pickable
      if(pickResult != null) {
         CelestialObject cObject = (CelestialObject)pickResult.getNode(PickResult.PRIMITIVE);
         if(cObject != null) {
            objInfo.notifyObservers( cObject.getId(), ((CelestialObjectInfo)cObject.getInfo()).getTypeNbr() );
         } else {
            System.out.println("Picking null pointer!!!!!!!!!!!");
         }
      }
   }

   /**
    * Updates the information state of the object.<br>
    *
    * @param oi a reference to the list of information observers
    * @param id identification of the object that has to be updated
    */
   public void update( String id, int parameter ) {

   }

   /**
    * Returns the ID of the PickingBehavior class.
    *
    * @return the ID to idendification this PickingBehavior class.
    */
   public String getId() {
      return id;
   }

   /**
    * Returns a CelestialObjectInfo.
    *
    * @return a CelestialObjectInfo that is initialized with null.
    */
   public CelestialObjectInfo getInfo() {
      return null;
   }
}
