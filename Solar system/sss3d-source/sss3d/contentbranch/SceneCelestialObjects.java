/*
  File: SceneCelestialObjects.java

  University of Applied Science Berne,HTA-Biel/Bienne,
  Computer Science Department.

  Diploma thesis J3D Solar System Simulator
  Originally written by Marcel Portner & Bernhard Hari (c) 2000
  
  CVS - Information :
  
  $Header: /var/cvsreps/projects/c450/2000/sss3d/source_diploma/sss3d/contentbranch/SceneCelestialObjects.java,v 1.10 2000/12/13 13:37:07 portm Exp $
  $Author: portm $
  $Date: 2000/12/13 13:37:07 $
  $State: Exp $
  
*/
package sss3d.contentbranch;

import javax.media.j3d.*;
import java.awt.Color;

/**
 * This is the interface for all celestial objects scene graphs.
 *
 * @author Marcel Portner & Bernhard Hari
 * @version $Revision: 1.10 $
 * @see sss3d.contentbranch.sun.SceneSun
 * @see sss3d.contentbranch.planets.ScenePlanet
 * @see sss3d.contentbranch.moons.SceneMoon
 * @see sss3d.contentbranch.comets.SceneComet
 */
public interface SceneCelestialObjects {

   /**
    * Should be called to remove this scene from the scenegraph.
    */
   public void destroy();

   /**
    * Returns the BranchGroup of the created celestial object scene.
    *
    * @return a BranchGroup of the given celestial object.
    */
   public BranchGroup createSceneGraph();

   /**
    * This method handle the animation and rotation of the celestial objects.
    *
    * @param animate  if true, start animation; if false stop animation.
    * @param rotate  if true, start rotation; if false stop rotation.
    */
   public void setAnimation(boolean animate, boolean rotate);

   /**
    * Set the animation speed for the celestial objects.
    */
   public void setAnimSpeed();

   /**
    * Add the coordinate system to this scenegraph.
    */
   public void addCoord();
      
   /**
    * Remove the coordinate system from this scenegraph.
    */
   public void removeCoord();

   /**
    * Add the color orbit to this scenegraph.
    */
   public void addOrbit();
   
   /**
    * Remove the color orbit from this scenegraph.
    */
   public void removeOrbit();

   /**
    * Set a new orbit color.
    *
    * @param orbitColor  a color for the orbit.
    */
   public void setOrbitColor(Color orbitColor);

   /**
    * Get the current position of the celestial object.
    *
    * @return a Transfrom3D with the current position.
    */
   public Transform3D getCurrentPosition();
}