/*
  File: Rocket.java

  University of Applied Science Berne,HTA-Biel/Bienne,
  Computer Science Department.

  Diploma thesis J3D Solar System Simulator
  Originally written by Marcel Portner & Bernhard Hari (c) 2000
  
  CVS - Information :
  
  $Header: /var/cvsreps/projects/c450/2000/sss3d/source_diploma/sss3d/contentbranch/rockets/Rocket.java,v 1.4 2000/12/12 15:37:07 harib Exp $
  $Author: harib $
  $Date: 2000/12/12 15:37:07 $
  $State: Exp $
  
*/
package sss3d.contentbranch.rockets;

import sss3d.contentbranch.*;
import sss3d.utils.SSS3dConstants;

import javax.media.j3d.*;
import javax.vecmath.*;

/**
 * This class holds the data for a rocket in the simulator.
 *
 * @author Marcel Portner & Bernhard Hari
 * @version $Revision: 1.4 $
 */
public class Rocket implements CelestialObject {

   // The name of the current rocket
   private String name;

   private float speedFactor = 0;
   
   private CelestialObjectInfo lnkCelestialObjectInfo;   

   /**
    * Constructs a new rocket.
    *
    * @param name  the name of the current rocket.
    */
   public Rocket(String name) {
      this.name = name;
      lnkCelestialObjectInfo = new CelestialObjectInfo(name, SSS3dConstants.ROCKET_TYPE);
   }

   /**
    * Sets the speed factor of this rocket.
    *
    * @param speedFactor the speed factor
    */
   public void setSpeedFactor( float speedFactor ) {
     this.speedFactor = speedFactor;
   }
   
   /**
    * Returns the speed factor of this rocket.
    *
    * @param float the speed factor
    */   
   public float getSpeedFactor() {
     return speedFactor;
   }

   /**
    * Returns a CelestialObjectInfo of the current rocket.
    * This object has all specific information of the current rocket.
    *
    * @return a CelestialObjectInfo of the current rocket.
    */
   public CelestialObjectInfo getInfo() {
      return lnkCelestialObjectInfo;
   }

   /**
    * Gets the ID of the current rocket.
    *
    * @return the ID of the current rocket.
    */
   public String getId() {
      return name;
   }

   /**
    * Gets the radius of the current rocket.
    *
    * @return the radius as a scaled factor of the current rocket.
    */
   public float getRadius() {
      return 0.05f;
   }

   /**
    * Gets the radius of the current rocket.
    *
    * @return the radius as a logarithm factor of the current rocket.
    */
   public float getLogRadius() {
      return 0.004f;
   }

   /**
    * Gets the degree of the current rocket.
    *
    * @return the degree (�) of the current rocket.
    */
   public double getDegree() {
      return 0.0d;
   }

   /**
    * Gets the rotation speed to the own axis of the current rocket.
    *
    * @return the rotation speed to the own axis in day of the current rocket.
    * One day correspond to one second in the animation.
    */
   public long getRotOwnAxis() {
      return 0;
   }

   /**
    * Gets the rotation speed around the sun of the current rocket.
    *
    * @return the rotation speed around the sun in year of the current rocket.
    * One year correspond to 365.242190 seconds (~6 minute) in the animation.
    */
   public long getRotOrbit() {
      return 0;
   }

   /**
    * Set the appearance of this rocket (sphere) whith the right
    * texture.
    *
    * @return the Appearance of the rocket with his right texture.
    */
   public Appearance createAppearance() {

     if ( !name.equals("spaceshuttle") ) {
      // Ambient-diffuse-reflection coefficient
      Color3f diffAmb = new Color3f(1.0f, 1.0f, 1.0f); // white
      // Diffuse-reflection coefficient
      Color3f reflDiff = new Color3f(1.0f, 1.0f, 1.0f); // white
      // Specular-reflection coefficient (reflectance function)
      Color3f reflSpec = new Color3f(1.0f, 1.0f, 1.0f); // white
      // c = shininess: cos^c in the specular reflection
      float c = 1;
      // Emitted light
      Color3f lumEmise = new Color3f(0.0f, 0.0f, 0.0f); // black

      Appearance appearance = new Appearance();

      // Set up the optical properties.
      appearance.setMaterial(new Material(diffAmb, lumEmise, reflDiff, reflSpec, c));

      return appearance;
     }
     
     return null;

   }

   /**
    * Returns the texture of the current rocket.
    *
    * @return the texture of the current rocket.
    */
   private Texture2D getRocketTexture() {

      // Loading of the texture
      NewTextureLoader newTextureLoader = new NewTextureLoader("images/" + getId() + ".jpg",
                                                               NewTextureLoader.GENERATE_MIPMAP);

      ImageComponent2D image;
      try {
         image = newTextureLoader.getImage();
      } catch(NullPointerException e) {
         return null;
      }

      int imageWidth = image.getWidth();
      int imageHeight = image.getHeight();

      Texture2D texture = new Texture2D(Texture.MULTI_LEVEL_MIPMAP, Texture.RGBA,
                                        imageWidth, imageHeight);
      int imageLevel = 0;

      texture.setImage(imageLevel, image);

      while (imageWidth > SMALLEST || imageWidth > SMALLEST) {
         imageLevel++;
         if (imageWidth > SMALLEST) imageWidth /= 2;
         if (imageHeight > SMALLEST) imageHeight /= 2;
         image = newTextureLoader.getScaledImage(imageWidth, imageHeight);
         texture.setImage(imageLevel, image);
      }

      texture.setMagFilter(Texture.MULTI_LEVEL_POINT);
      texture.setMinFilter(Texture.MULTI_LEVEL_POINT);

      return texture;
   }
}
