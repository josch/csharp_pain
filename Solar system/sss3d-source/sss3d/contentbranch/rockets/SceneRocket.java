/*
  File: SceneRocket.java

  University of Applied Science Berne,HTA-Biel/Bienne,
  Computer Science Department.

  Diploma thesis J3D Solar System Simulator
  Originally written by Marcel Portner & Bernhard Hari (c) 2000
  
  CVS - Information :
  
  $Header: /var/cvsreps/projects/c450/2000/sss3d/source_diploma/sss3d/contentbranch/rockets/SceneRocket.java,v 1.8 2000/12/13 13:38:00 portm Exp $
  $Author: portm $
  $Date: 2000/12/13 13:38:00 $
  $State: Exp $
  
*/
package sss3d.contentbranch.rockets;

import sss3d.contentbranch.*;
import sss3d.utils.SSS3dConstants;
import sss3d.utils.observer.*;
import sss3d.contentbranch.orbit.*;
import sss3d.utils.xmlparser.XMLConstants;

import java.awt.Color;
import java.util.*;
import javax.media.j3d.*;
import javax.vecmath.*;

import com.sun.j3d.loaders.lw3d.Lw3dLoader;
import com.sun.j3d.loaders.Loader;
import com.sun.j3d.loaders.Scene;

/**
 * This class describe the scene of a rocket.
 *
 * @author Marcel Portner & Bernhard Hari
 * @version $Revision: 1.8 $
 * @see Rocket
 */
public class SceneRocket implements SceneCelestialObjects, PositionObserver, InfoObserver {

   private TransformGroup sceneTransform;
   private TransformGroup trGrRotAndLeafs;
   private TransformGroup trGrRot;
   private RocketNavigation rocketNavRot;
   private RocketSpeed rocketSpeed;
   private RocketNavigation rocketNavTrans;   

   private Rocket rocket;

   private ObjectsPositions objPos;
   private ObjectsInformation objInfo;
   private InitializationObject iniObject;


   /**
    * Initializes a new SceneRocket.
    *
    * @param rocket   a object reference of a given rocket.
    * @param objPos   a reference to the concrete subject of 
    *                 the observer pattern positions
    * @param objInfo  a reference to the concrete subject of
    *                 the observer pattern information
    */
   public SceneRocket(Rocket rocket, ObjectsPositions objPos,
                      ObjectsInformation objInfo ) {
      this.rocket = rocket;
      this.objPos = objPos;
      this.objPos.attach(this);
      this.objInfo = objInfo;
      this.objInfo.attach(this);
      this.iniObject = objInfo.getInitializationObject();
      System.out.println("SceneRocket.constructor");
   }

   /**
    * Removes this object from the position observer list.
    */
   public void destroy() {
   	  objPos.detach(this);
      objInfo.detach(this);
      rocket = null;      
   }

   /**
    * Updates the position state of the object.<br>
    *
    * @param orbitPos a reference to the list containing all position observer
    */
   public void update(OrbitPositions orbitPos) {

   }

   /**
    * Updates the information state of the object.<br>
    *
    * @param id        identification of the object that has to be updated
    * @param parameter the parameter that has been changed
    */
   public void update( String id, int parameter ) {
   // currently not implemented 
/*   
      if ( id.equals( rocket.getId() ) ||
          id.equals("all") ) {
          
          IniData data = (IniData)objInfo.getParameter( XMLConstants.ROCKET, rocket.getId() );
          
          switch ( parameter ) {
             case XMLConstants.VISIBLE :
                  // implemented inside SceneSolarSystem
                  break;
             case XMLConstants.COORDINATESYSTEM :
                  if ( data.hasCoordinateSystem() ) {
                     addCoord();
                  } else  {
                     removeCoord();
                  }
                  break;
             case XMLConstants.ORBIT :
                  if ( data.hasOrbit() ) {
                     addOrbit();
                  } else  {
                     removeOrbit();
                  }
                  break;
             case XMLConstants.COLORORBIT :
                  setOrbitColor( data.getColorOrbit());
                  break;
             case XMLConstants.ANIMATIONSPEED :
                  setAnimSpeed();
                  break;
             default : break;
          
          }
      }
*/
   }

   /**
    * Returns the ID of the current rocket.
    *
    * @return the ID to idendification the rocket.
    */
   public String getId() {
      return rocket.getId();
   }

   /**
    * Returns a CelestialObjectInfo of the current rocket.
    * This object has all specific information of the current rocket.
    *
    * @return a CelestialObjectInfo of the current rocket.
    */
   public CelestialObjectInfo getInfo() {
      return rocket.getInfo();
   }

   /**
    * This method handle the animation and rotation of the celestial objects.
    * NOT IMPLEMENTED FOR A ROCKET.
    *
    * @param animate  if true, start animation; if false stop animation.
    * @param rotate   if true, start rotation; if false stop rotation.
    */
   public void setAnimation(boolean animate, boolean rotate) {
   }

   /**
    * Set the animation speed for the celestial objects.
    * NOT IMPLEMENTED FOR A ROCKET.    
    *
    * @param speedFactor  a factor for the animation speed.
    */
   public void setAnimSpeed() {
   }

   /**
    * Add the coordinate system to this scenegraph.
    * NOT IMPLEMENTED FOR A ROCKET.    
    */
   public void addCoord() {
   }
   
   /**
    * Remove the coordinate system from this scenegraph.
    * NOT IMPLEMENTED FOR A ROCKET.    
    */
   public void removeCoord() {
   }

   /**
    * Add the color orbit to this scenegraph.
    * NOT IMPLEMENTED FOR A ROCKET.    
    */
   public void addOrbit() {
   }
   
   /**
    * Remove the color orbit from this scenegraph.
    * NOT IMPLEMENTED FOR A ROCKET.    
    */
   public void removeOrbit() {
   }
   
   /**
    * Set a new orbit color.
    * NOT IMPLEMENTED FOR A ROCKET.    
    */
   public void setOrbitColor(Color orbitColor) {
   }
      
   /**
    * Get the current position of the rocket.
    *
    * @return a Transform3D with the current position.
    */
   public Transform3D getCurrentPosition() {
      Transform3D transform = new Transform3D();
      trGrRot.getTransform(transform);
      return transform;
   }
   
   /**
    * Returns the transform group to control the flight path
    *
    * @return TransformGrop the transform group
    */
   public TransformGroup getTransformGroup() {
     return trGrRot;
   }

   /**
    * Returns the BranchGroup of the created rocket scene.
    * It has a rocket as leaf.
    *
    * @return the BranchGroup of the given rocket.
    */
   public BranchGroup createSceneGraph() {
      System.out.println("SceneRocket.createSceneGraph()");
      
      // A BoundingSphere instance as general bounding region.
      BoundingSphere boundsGen = new BoundingSphere(new Point3d(0.0, 0.0, 0.0),
                                                    SSS3dConstants.BOUNDRADIUS);

      // set scale of the object
      float scale;
      if(((Boolean)iniObject.getParameter(XMLConstants.COMPRESSED)).booleanValue()) {
         scale = rocket.getLogRadius();
      } else {
         scale = rocket.getRadius();
      }
      Transform3D tr3Dscale = new Transform3D();
      tr3Dscale.setScale((double)scale);    

      // Create the first TransformGroup node trGrRotAndLeafs for the rocket,
      // the coordinate system and the rotation
      trGrRotAndLeafs = new TransformGroup(tr3Dscale);

      // With the ALLOW_TRANSFORM_WRITE capability, we allow the
      // modification of the TransformGroup's code by the behavior's
      // code at run time.
      trGrRotAndLeafs.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
      trGrRotAndLeafs.setCapability(TransformGroup.ALLOW_TRANSFORM_READ);
      trGrRotAndLeafs.setCapability(Group.ALLOW_CHILDREN_READ);
      trGrRotAndLeafs.setCapability(Group.ALLOW_CHILDREN_WRITE);
      trGrRotAndLeafs.setCapability(Group.ALLOW_CHILDREN_EXTEND);

        
      // Construct the Lw3d loader and load the file
	    Loader lw3dLoader = new Lw3dLoader(Loader.LOAD_ALL);
	    Scene loaderScene = null;
	    try {
	        loaderScene = lw3dLoader.load( getId()+".lws");
	    }
	    catch (Exception e) {
	      System.err.println("Exception loading file: " + e);
	    }

	
    	// Now add the scene graph defined in the lw3d file
	    if (loaderScene.getSceneGroup() != null) {
	    // Instead of using the default view location (which may be
	    // completely bogus for the particular file you're loading),
	    // let's use the initial view from the file.  We can get
	    // this by getting the  view groups from the scene (there's
	    // only one for Lightwave 3D), then using the inverse of the
	    // transform on that view as the transform for the entire scene.

	    // First, get the view groups (shouldn't be null unless there
	    // was something wrong in the load
	      TransformGroup viewGroups[] = loaderScene.getViewGroups();

	      // Get the Transform3D from the view and invert it
	      Transform3D t = new Transform3D();
	      viewGroups[0].getTransform(t);
	      Matrix4d m = new Matrix4d();
	      t.get(m);
	      m.invert();
	      m.rotY(Math.PI);
        t.set(m);

	      // Now we've got the transform we want.  Create an
	      // appropriate TransformGroup and parent the scene to it.
	      // Then insert the new group into the main BranchGroup.
	      sceneTransform = new TransformGroup(t);
        sceneTransform.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
        sceneTransform.setCapability(TransformGroup.ALLOW_TRANSFORM_READ);

	      sceneTransform.addChild(loaderScene.getSceneGroup());
        BoundingSphere bounds = new BoundingSphere(new Point3d(0.0, 0.0, 0.0),
                                                    SSS3dConstants.BOUNDRADIUS);
                                                    
        /*Light light[] = loaderScene.getLightNodes();
        for(int l = 0;l<light.length;l++) {
          System.out.println(light[l].toString());
          light[l].setInfluencingBounds((Bounds)bounds);
          light[l].setColor(new Color3f(1.0f, 1.0f, 1.0f));
          light[l].setEnable(true);
        }*/
        
        // set appearance of rocket
        Hashtable hashtable = loaderScene.getNamedObjects();
        Enumeration enum = hashtable.elements();
        while ( enum.hasMoreElements() ) {
          Object object = (Object)enum.nextElement();
          //System.out.println(object.toString());
          if ( object instanceof TransformGroup ) {
             TransformGroup tg = (TransformGroup)object;
             Enumeration children = tg.getAllChildren();
             while ( children.hasMoreElements() ) {
               Object obj = (Object)children.nextElement();
               //System.out.println(obj.toString());
               if ( obj instanceof Shape3D ) {
                  Shape3D shape = (Shape3D)obj;
                  Appearance appearance = rocket.createAppearance();
                  if ( appearance != null ) {
                       shape.setAppearance( appearance );
                  }
               }
             }
          }
        }
	      trGrRotAndLeafs.addChild(sceneTransform);
        
	    }      
      
        // Create the second TransformGroup node trGrRot for the rotation
      trGrRot = new TransformGroup();
      
      // With the ALLOW_TRANSFORM_READ and ALLOW_TRANSFORM_WRITE
      // capabilities, we allow the modification of the TransformGroup's
      // code by the Behavior's code at run time.
      trGrRot.setCapability(TransformGroup.ALLOW_TRANSFORM_READ);
      trGrRot.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);

      // Attach the trGrScaleAndLeafs node to the trGrRot node.
      trGrRot.addChild(trGrRotAndLeafs);    
      
      rocketNavRot = new RocketNavigation(trGrRot, rocket);
      rocketNavRot.setSchedulingBounds(new BoundingSphere(new Point3d(0.0, 0.0, 0.0),
                                                    SSS3dConstants.BOUNDRADIUS));
      trGrRot.addChild(rocketNavRot);

      rocketSpeed = new RocketSpeed(trGrRot, rocket);
      rocketSpeed.setSchedulingBounds(new BoundingSphere(new Point3d(0.0, 0.0, 0.0),
                                                    SSS3dConstants.BOUNDRADIUS));
      trGrRot.addChild(rocketSpeed);

      BranchGroup brGrAll = new BranchGroup();
      brGrAll.addChild(trGrRot);

      brGrAll.setCapability(BranchGroup.ALLOW_DETACH);
      
      // Return the final version of the BranchGroup node brGrAll.
      return brGrAll;
   }
   
}

