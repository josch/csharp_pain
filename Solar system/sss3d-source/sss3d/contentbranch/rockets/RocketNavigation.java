/*
  File: RocketNavigation.java

  University of Applied Science Berne,HTA-Biel/Bienne,
  Computer Science Department.

  Diploma thesis J3D Solar System Simulator
  Originally written by Marcel Portner & Bernhard Hari (c) 2000
  
  CVS - Information :
  
  $Header: /var/cvsreps/projects/c450/2000/sss3d/source_diploma/sss3d/contentbranch/rockets/RocketNavigation.java,v 1.4 2000/12/12 15:37:07 harib Exp $
  $Author: harib $
  $Date: 2000/12/12 15:37:07 $
  $State: Exp $
  
*/
package sss3d.contentbranch.rockets;

import java.awt.AWTEvent;
import java.util.Enumeration;
import java.awt.event.*;
import javax.vecmath.*;
import javax.media.j3d.*;

/**
 * This class is a keyboard behavior to control the navigation
 * of the a rocket.
 *
 * @author Marcel Portner & Bernhard Hari
 * @version $Revision: 1.4 $
 */
public class RocketNavigation extends Behavior {

   private Rocket rocket;

   /**
    * The TransformGroup node to modify by the keyboard interaction.
    */
   private TransformGroup new_tgControl;
 
   /**
    * Wake up event when a key is pressed.
    */
   private WakeupOnAWTEvent wakeUp = new WakeupOnAWTEvent(KeyEvent.KEY_PRESSED);
   
   /**
    * The angle to turn when the directionkeys are pressed
    */
   private static final float ANGLE = (float) 0.017444f; // 1 degree

   private float speed = 0.0f;

   private Transform3D t3dView  = new Transform3D();
   private Transform3D t3dRot   = new Transform3D();

   /**
    * Constructor that allows to specify the desired target transform group.
    *
    * @param targetTG the target transform group
    * @param rocket   a reference to the rocket object
    */
   public RocketNavigation(TransformGroup targetTG, Rocket rocket) {
      new_tgControl = targetTG;
      this.rocket = rocket;
   }

   /**
    * Override Behavior's initialize method to setup wakeup criteria.
    */
   public void initialize() {
      wakeupOn(wakeUp);
   }

   /**
    * Override Behavior's stimulus method to handle the event.
    * This method is called when a key on the keyboard has been pressed and
    * operates on the specified transform group to move the rocket position.
    *
    * @param criteria  all pressed keys in a list. This will be passed by the system.
    */
   public void processStimulus(Enumeration criteria) {
      WakeupOnAWTEvent ev;
      AWTEvent[] events;

      if(criteria.hasMoreElements()) {
         
         ev = (WakeupOnAWTEvent) criteria.nextElement();//object;
         events = ev.getAWTEvent();
         KeyEvent eventKey = (KeyEvent) events[0];
         int keyCode = eventKey.getKeyCode();

         new_tgControl.getTransform(t3dView);

         switch(keyCode) {
           case KeyEvent.VK_NUMPAD8:  // Up arrow - to move up
             t3dRot.rotX(-ANGLE);
             t3dView.mul(t3dRot);
             new_tgControl.setTransform(t3dView);
             break;

           case KeyEvent.VK_NUMPAD2:  // Down arrow - to move down
             t3dRot.rotX(ANGLE);
             t3dView.mul(t3dRot);
             new_tgControl.setTransform(t3dView);
             break;

           case KeyEvent.VK_NUMPAD6:  // Right arrow - to move right
             t3dRot.rotZ(-ANGLE);
             t3dView.mul(t3dRot);
             new_tgControl.setTransform(t3dView);
             break;

           case KeyEvent.VK_NUMPAD4:  // Left arrow - to move left
             t3dRot.rotZ(ANGLE);
             t3dView.mul(t3dRot);
             new_tgControl.setTransform(t3dView);                    
             break;

           case KeyEvent.VK_NUMPAD7:   // Page Up - to move forward
             speed = rocket.getSpeedFactor();
             speed += 0.0001f;
             rocket.setSpeedFactor(speed);

             break;

           case KeyEvent.VK_NUMPAD1:   // Page Down - to move backward
             speed = rocket.getSpeedFactor();              
             speed -= 0.0001f;
             rocket.setSpeedFactor(speed);              

             break;
           
           case KeyEvent.VK_NUMPAD0:
             speed = 0.0f;
             rocket.setSpeedFactor(speed);
             break;
             
           case KeyEvent.VK_HOME:   // Home - go to the start position
             t3dView.setIdentity();
             new_tgControl.setTransform(t3dView);
             break;

           default:
         }
      }

      // Set wakeup criteria for next time.
      wakeupOn(wakeUp);
   }
}
