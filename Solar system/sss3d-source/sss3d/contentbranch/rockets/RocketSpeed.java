/*
  File: RocketSpeed.java

  University of Applied Science Berne,HTA-Biel/Bienne,
  Computer Science Department.

  Diploma thesis J3D Solar System Simulator
  Originally written by Marcel Portner & Bernhard Hari (c) 2000
  
  CVS - Information :
  
  $Header: /var/cvsreps/projects/c450/2000/sss3d/source_diploma/sss3d/contentbranch/rockets/RocketSpeed.java,v 1.3 2000/12/12 15:37:07 harib Exp $
  $Author: harib $
  $Date: 2000/12/12 15:37:07 $
  $State: Exp $
  
*/
package sss3d.contentbranch.rockets;

import java.util.Enumeration;
import javax.vecmath.*;
import javax.media.j3d.*;

/**
 * This class is a behavior to control the speed
 * of a rocket.
 *
 * @author Marcel Portner & Bernhard Hari
 * @version $Revision: 1.3 $
 */
public class RocketSpeed extends Behavior {

   private Rocket rocket;

   /**
    * The TransformGroup node to modify by the keyboard interaction.
    */
   private TransformGroup new_tgControl;
 
   /**
    * Wake up event on a elapsed frame.
    */
   private WakeupOnElapsedFrames wakeUpFrame = new WakeupOnElapsedFrames(0);
   
   private Transform3D t3dView  = new Transform3D();
   private Vector3f    v3fTrans = new Vector3f();
   private Vector3f    v3fTemp  = new Vector3f();
   private Matrix3f    m3fRot   = new Matrix3f();
   
   /**
    * Constructor that allows to specify the desired target transform group.
    *
    * @param targetTG  the target transform group
    * @param rocket    an object reference to the controlled rocket
    */
   public RocketSpeed(TransformGroup targetTG, Rocket rocket) {
      new_tgControl = targetTG;
      this.rocket = rocket;
   }

   /**
    * Override Behavior's initialize method to setup wakeup criteria.
    */
   public void initialize() {
      wakeupOn(wakeUpFrame);
   }

   /**
    * Override Behavior's stimulus method to handle the event.
    * This method is called each time when a new frame is painted and
    * operates on the specified transform group to move the rocket position.
    *
    * @param criteria  a enumeration - list. This will be passed by the system.
    */
   public void processStimulus(Enumeration criteria) {

      if(criteria.hasMoreElements()) {
         // wake up on elapsed frames...
         if(rocket.getSpeedFactor() != 0.0f) {
            new_tgControl.getTransform(t3dView);
          
            t3dView.get(v3fTrans);
            t3dView.get(m3fRot);
            m3fRot.transform(new Vector3f(0.0f, 0.0f, -rocket.getSpeedFactor()), v3fTemp);
            v3fTrans.add(v3fTemp);
            t3dView.setTranslation(v3fTrans);  
          
            new_tgControl.setTransform(t3dView);
         }     
      }

      // Set wakeup criteria for next time.
      wakeupOn(wakeUpFrame);
   }
}
