/*
  File: IniData.java

  University of Applied Science Berne,HTA-Biel/Bienne,
  Computer Science Department.

  Diploma thesis J3D Solar System Simulator
  Originally written by Marcel Portner & Bernhard Hari (c) 2000
  
  CVS - Information :
  
  $Header: /var/cvsreps/projects/c450/2000/sss3d/source_diploma/sss3d/contentbranch/IniData.java,v 1.3 2000/12/13 14:02:45 portm Exp $
  $Author: portm $
  $Date: 2000/12/13 14:02:45 $
  $State: Exp $
  
*/
package sss3d.contentbranch;

import sss3d.utils.*;

import java.awt.Color;

/**
 * This class holds data such as the name, parent-name, type and the color
 * of the CelestialObject. It also stores further information about
 * visibility of the object, orbit, or his coordinate system.  
 *   
 * @author Marcel Portner & Bernhard Hari
 * @version $Revision: 1.3 $
 */
public class IniData {
  
  // stores the data of the celestial objects inside these types
  private String  parent           = null;
  private String  name             = null;   
  private int     type             = SSS3dConstants.UNKNOWN_TYPE;
  private boolean visible          = false;
  private boolean orbit            = false;
  private boolean coordinatesystem = false;
  private MyColor colororbit;

  
  /**
   * Constructor
   *
   * @param parent the name of the parent celestial object, if it exists
   * @param name the name/id of the celestial object
   * @param type the type of the celestial object represented by it's name
   * @param visible true or false if the celestial object be visible or not
   * @param orbit true or false if the orbit should be visible or not
   * @param coordinatesystem true or false if the coordinate system should be visible or not
   * @param colororbit the string representation of this color, it can be either
   *        BLACK,BLUE,CYAN,DARKGRAY,GRAY,GREEN,LIGHTGRAY,MAGENTA,ORANGE,PINK,RED,WHITE or YELLOW
   *        defined inside MyColor
   * @see sss3d.contentbranch.MyColor
   */
  public IniData(String parent, String name, int type, boolean visible, boolean orbit,
                 boolean coordinatesystem, String colororbit) {
             
    this.parent           = parent;
    this.name             = name;
    this.type             = type;
    this.visible          = visible;
    this.orbit            = orbit;
    this.coordinatesystem = coordinatesystem;
    this.colororbit       = new MyColor(colororbit); 
  }
  
  /**
   * Returns true if this celestial object has a parent object
   *
   * @ returns boolean true or false if a parent object exists
   */
  public boolean hasParent() {
    return (parent != null ) ? true: false;
  }
  
  /**
   * Returns the string representing his parent
   * 
   * @return String the name of the parent celestial object
   */
  public String getParent() {
    return parent;
  }
  
  /**
   * Returns the name of this celestial object.
   *
   * @return String the name of this celestial object
   */
  public String getName() {
    return name;
  }
  
  /**
   * Returns the type of this object
   *
   * @return int type of this celestial object
   */
  public int getType() {
    return type;
  }
  
  /**
   * Returns the current color of the orbit
   *
   * @return Color a color object representing the color of this orbit
   */ 
  public Color getColorOrbit() {
    return colororbit.getColor();;
  } 

  /**
   * Returns true if this object is visible.
   *
   * @return boolean true if this celestial object is visible
   */  
  public boolean isVisible() {
    return visible;
  }
  
  /**
   * Returns true if orbit is visible
   *
   * @return true if object is visible
   */
  public boolean hasOrbit() {
    return orbit;
  }
  
  /**
   * Returns true if the coordinate system is visible
   *
   * @return boolean true if the coordinate system is visible
   */ 
  public boolean hasCoordinateSystem() {
    return coordinatesystem;
  }
  
  /**
   * Set the name of his parent
   *
   * @param parent the name representing his parent ( orbit center )
   */
  public void setParent( String parent ) {
    this.parent = parent;
  }
    
  /**
   * Sets the name of this celestial object
   *
   * @param name the name of this celestial object
   */  
  public void setName( String name ) {
    this.name = name;
  }

  /**
   * Sets the type of this celestial object
   *
   * @param type the type of this celestial object
   */    
  public void setType( int type ) {
    this.type = type;
  }

  /**
   * Sets the visibility of this celestial object
   *
   * @param visible true if this celestial object should be visible
   */  
  public void setVisible( boolean visible ) {
    this.visible = visible;
  }
  
  /**
   * Sets the visibility of the orbit corresponding to this celestial object
   *
   * @param orbit true if the orbit should be visible
   */  
  public void setOrbit( boolean orbit ) {
    this.orbit = orbit;
  }
  
  /**
   * Sets the visibility of the coordinate system corresponding to this celestial object
   *
   * @param coordinatesystem true if the coordinate system should be visible
   */  
  public void setCoordinateSystem( boolean coordinatesystem ) {
    this.coordinatesystem = coordinatesystem;
  }
  
  /**
   * Sets the color of the orbit 
   *
   * @param color the new color of the orbit
   */  
  public void setColorOrbit( Color color ) {
    colororbit.setColor(color);  
  }
  
  /**
   * Sets the color of the orbit given by the color name
   *
   * @param colorName the name of the new color
   */    
  public void setColorOrbit( String colorName ) {
    colororbit.setColor( colorName );  
  }
  
}