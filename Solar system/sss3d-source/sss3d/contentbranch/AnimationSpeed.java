/*
  File: AnimationSpeed.java

  University of Applied Science Berne,HTA-Biel/Bienne,
  Computer Science Department.

  Diploma thesis J3D Solar System Simulator
  Originally written by Marcel Portner & Bernhard Hari (c) 2000

  CVS - Information :

  $Header: /var/cvsreps/projects/c450/2000/sss3d/source_diploma/sss3d/contentbranch/AnimationSpeed.java,v 1.2 2000/12/12 14:29:08 harib Exp $
  $Author: harib $
  $Date: 2000/12/12 14:29:08 $
  $State: Exp $

*/

package sss3d.contentbranch;


/**
 * Holds the data to calculate the animation speed.
 *   
 * @author Marcel Portner & Bernhard Hari
 * @version $Revision: 1.2 $
 */
public class AnimationSpeed {
  
  /**
   * Constant for number of days per second simulation time
   */
  public static final int DAYS_PER_SECOND    =  0;
  
  /**
   * Constant for number of hours per second simulation time
   */
  public static final int HOURS_PER_SECOND   =  1;
  
  /**
   * Constant for number of minutes per second simulation time
   */
  public static final int MINUTES_PER_SECOND =  2;
  
  /**
   * Names for these constants DAYS_PER_SECOND,
   * HOURS_PER_SECOND and MINUTES_PER_SECOND
   */
  public static final String[] NAMES = {
         "days",
         "hours",
         "minutes"
  };

  /**
   * Minimum allowed value for days ( limit checks )
   */
  public static final int DAYS_MIN           =  1;

  /**
   * Maximum allowed value for days ( limit checks )
   */
  public static final int DAYS_MAX           = 10;
  
  /**
   * Minimum allowed value for hours ( limit checks )
   */
  public static final int HOURS_MIN          =  1;
  
  /**
   * Maximum allowed value for hours ( limit checks )
   */  
  public static final int HOURS_MAX          = 24;

  /**
   * Minimum allowed value for minutes ( limit checks )
   */
  public static final int MINUTES_MIN        =  1;

  /**
   * Maximum allowed value for minutes ( limit checks )
   */
  public static final int MINUTES_MAX        = 60;


  // AnimationSpeed is equal to on day per second
  // simulation time ( default )
  private int type  = DAYS_PER_SECOND;
  private int value = 1;

  /**
   * Simple constructor, uses default type/value DAYS_PER_SECOND / 1
   */
  public AnimationSpeed() {
  
  }

  /**
   * Constructor, sets the type and the value 
   * by the given values
   *
   * @param type either DAYS_PER_SECOND, HOURS_PER_SECOND or MINUTES_PER_SECOND
   * @param value a value that correspond to the given type ( checks parameters internally )
   *              If the parameters aren't correct, then the constructor uses the default values.
   */
  public AnimationSpeed( int type, int value ) {
    if ( checkParameters(type,value) ) {
      this.type = type;
      this.value = value;
    }
  }

  /**
   * Returns the type of this AnimationSpeed object
   *
   * @return int the type of this AnimationSpeed object
   */
  public int getType() {
    return type;
  }
  
  /**
   * Returns the value of this AnimationSpeed object
   *
   * @return int the value of this AnimationSpeed object
   */  
  public int getValue() {
    return value;
  }
  
  
  /**
   * Sets the type and the corrresponding value
   *
   * @param type either DAYS_PER_SECOND, HOURS_PER_SECOND or MINUTES_PER_SECOND
   * @param value a value that correspond to the given type ( checks parameters internally )
   *              If the parameters aren't correct, then the function returns false without setting
   *              the new parameters.
   * @param boolean true or false if the new values are set
   */
  public boolean setTypeValue( int type, int value ) {
    boolean set = false;
    if ( checkParameters(type,value) ) {
      this.type = type;
      this.value = value;
      set = true;
    }    
    return set;
  }

  /**
   * Check parameters it there are valid and inside the
   * given limits.
   *
   * @param type either DAYS_PER_SECOND, HOURS_PER_SECOND or MINUTES_PER_SECOND
   * @param value the new value
   * @return boolean true if the values are correct
   */
  private boolean checkParameters( int type, int value ) {
    boolean areValid = false;
    
    switch ( type ) {
      case DAYS_PER_SECOND :
           if ( value >= DAYS_MIN && value <= DAYS_MAX ) {
              areValid = true;
           }
           break;
      case HOURS_PER_SECOND :
           if ( value >= HOURS_MIN && value <= HOURS_MAX ) {
              areValid = true;
           }      
           break;
      case MINUTES_PER_SECOND :
           if ( value >= MINUTES_MIN && value <= MINUTES_MAX ) {
              areValid = true;
           }      
           break;
      default : break;
    }
    return areValid;
  }
}