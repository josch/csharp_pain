/*
  File: CelestialObject.java

  University of Applied Science Berne,HTA-Biel/Bienne,
  Computer Science Department.

  Diploma thesis J3D Solar System Simulator
  Originally written by Marcel Portner & Bernhard Hari (c) 2000
  
  CVS - Information :
  
  $Header: /var/cvsreps/projects/c450/2000/sss3d/source_diploma/sss3d/contentbranch/CelestialObject.java,v 1.6 2000/12/13 14:02:44 portm Exp $
  $Author: portm $
  $Date: 2000/12/13 14:02:44 $
  $State: Exp $
  
*/
package sss3d.contentbranch;

/**
 * This interfaces defines the methods that every celestial
 * object has to implement. Examples are Planets and the Sun.
 *
 * @author Marcel Portner & Bernhard Hari
 * @version $Revision: 1.6 $
 * @see sss3d.contentbranch.sun.Sun
 * @see sss3d.contentbranch.planets.Planet
 */

public interface CelestialObject {
	
   public static final int INITRADIUS = 1;
   public static final int DIVISON    = 30;
   public static final int SMALLEST   = 1;
   public static final int SECOND     = 1000;

   /**
    * Returns a CelestialObjectInfo of the current celestial object.
    * This object has all specific information of the current celestail object.
    *
    * @return a CelestialObjectInfo of the current celestial object.
    */
    public CelestialObjectInfo getInfo();

   /**
    * Gets the ID of the current celestial object.
    *
    * @return the ID of the current celestial object.
    */
    public String getId();
   
   /**
    * Gets the scaled radius of the current celestial object.
    *
    * @return the scaled radius of the current celestial object.
    */
   public float getRadius();

   /**
    * Gets the log. radius of the current celestial object.
    *
    * @return the log. radius of the current celestial object.
    */
   public float getLogRadius();

   /**
    * Gets the degree of the current celestial object.
    *
    * @return the degree of the current celestial object.
    */
   public double getDegree();

   /**
    * Gets the rotation speed to the own axis of the current celestial object.
    *
    * @return the rotation speed to the own axis of the current celestial object.
    */
   public long getRotOwnAxis();

   /**
    * Gets the rotation speed around the sun of the current celestial object.
    *
    * @return the rotation speed around the sun of the current celestial object.
    */
   public long getRotOrbit();    
}
