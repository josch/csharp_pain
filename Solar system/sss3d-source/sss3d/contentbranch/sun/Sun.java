/*
  File: Sun.java

  University of Applied Science Berne,HTA-Biel/Bienne,
  Computer Science Department.

  Diploma thesis J3D Solar System Simulator
  Originally written by Marcel Portner & Bernhard Hari (c) 2000
  
  CVS - Information :
  
  $Header: /var/cvsreps/projects/c450/2000/sss3d/source_diploma/sss3d/contentbranch/sun/Sun.java,v 1.10 2000/12/15 02:53:59 portm Exp $
  $Author: portm $
  $Date: 2000/12/15 02:53:59 $
  $State: Exp $
  
*/
package sss3d.contentbranch.sun;

import sss3d.contentbranch.*;
import sss3d.utils.SSS3dConstants;

import javax.media.j3d.*;
import javax.vecmath.*;
import com.sun.j3d.utils.geometry.Sphere;

/**
 * This class creates the Sun by using the Sphere class
 * and a given texture (sun.jpg).
 *
 * @author Marcel Portner & Bernhard Hari
 * @version $Revision: 1.10 $
 */
public class Sun extends Sphere implements CelestialObject {

   // The name of the sun
   private String name;

   private CelestialObjectInfo lnkCelestialObjectInfo;

   /**
    * Constructs a new Sun Sphere.
    */
   public Sun(String name) {
      super(INITRADIUS, Sphere.GENERATE_NORMALS |
            Sphere.GENERATE_TEXTURE_COORDS, DIVISON);
      this.name = name;
      // parameter of the sun
      lnkCelestialObjectInfo = new CelestialObjectInfo(name, SSS3dConstants.STAR_TYPE);

      // Set up the appearance to this Sphere object
      this.setAppearance(createAppearance());
   }

   /**
    * Gets the ID of the sun.
    *
    * @return the ID of the sun.
    */
   public String getId(){
      return name;
   }

   /**
    * Returns a CelestialObjectInfo of the sun.
    * This object has all specific information of the sun.
    *
    * @return a CelestialObjectInfo of the sun.
    */
   public CelestialObjectInfo getInfo() {
      return lnkCelestialObjectInfo;
   }

   /**
    * Gets the radius of the current sun.
    *
    * @return the radius as a scaled factor of the current sun.
    */
   public float getRadius() {
      return (float)(lnkCelestialObjectInfo.getDiameter() / SSS3dConstants.SCALE);
   }

   /**
    * Gets the radius of the current sun.
    *
    * @return the radius as a logarithm factor of the current sun.
    */
   public float getLogRadius() {
      return (float)(Math.log(lnkCelestialObjectInfo.getDiameter() / 1000) / 10);
   }

   /**
    * Gets the degree of the sun.
    *
    * @return the degree (�) of the sun.
    */
   public double getDegree() {
      return (double)lnkCelestialObjectInfo.getInclinationOfEquatorToOrbit();
   }

   /**
    * Gets the rotation speed to the own axis of the sun.
    *
    * @return the rotation speed to the own axis of the sun.
    * One day correspond to one second in the animation.
    */
   public long getRotOwnAxis() {
      return (long)(lnkCelestialObjectInfo.getRotationPeriod() * SECOND);
   }

   /**
    * The sun has a fix position!.
    *
    * @return <code>0</code>.
    */
   public long getRotOrbit() {
      return 0;
   }

   /**
    * Set the appearance of sun (whith the right
    * texture).
    *
    * @return the Appearance of the sun with his right texture.
    */
   private Appearance createAppearance() {
      // Optical properties of the sun.

      // Ambient-diffuse-reflection coefficient
      Color3f diffAmb = new Color3f(1.0f, 1.0f, 0.2f);
      // Diffuse-reflection coefficient
      Color3f reflDiff = new Color3f(1.0f, 1.0f, 0.0f);
      // Specular-reflection coefficient (reflectance function)
      Color3f reflSpec = new Color3f(1.0f, 1.0f, 0.0f);
      // c = shininess: cos^c in the specular reflection
      float c = 1;
      // Emitted light
      Color3f lumEmise = new Color3f(1.0f, 1.0f, 0.2f);

      Appearance appearance = new Appearance();

      // Set up the optical properties.
      appearance.setMaterial(new Material(diffAmb, lumEmise,reflDiff, reflSpec, c));
/*
      // Loading of the texture
      NewTextureLoader newTextureLoader = new NewTextureLoader("images/" + getId() + ".jpg");

      Texture texture = newTextureLoader.getTexture();
      if(texture == null)
         System.out.println("Attention: the texture left");

      appearance.setTexture(texture);

      // Application mode of the texture
      TextureAttributes texAttr = new TextureAttributes();
      texAttr.setTextureMode(TextureAttributes.MODULATE); // there still are: BLEND, DECAL,
                                                          //                  and REPLACE
      appearance.setTextureAttributes(texAttr);
*/
      return appearance;
   }
}
