/*
  File: SceneSun.java

  University of Applied Science Berne,HTA-Biel/Bienne,
  Computer Science Department.

  Diploma thesis J3D Solar System Simulator
  Originally written by Marcel Portner & Bernhard Hari (c) 2000
  
  CVS - Information :
  
  $Header: /var/cvsreps/projects/c450/2000/sss3d/source_diploma/sss3d/contentbranch/sun/SceneSun.java,v 1.16 2000/12/15 02:53:36 portm Exp $
  $Author: portm $
  $Date: 2000/12/15 02:53:36 $
  $State: Exp $
  
*/
package sss3d.contentbranch.sun;

import sss3d.contentbranch.*;
import sss3d.utils.SSS3dConstants;
import sss3d.utils.observer.*;
import sss3d.utils.xmlparser.XMLConstants;

import javax.media.j3d.*;
import javax.vecmath.*;
import com.sun.j3d.utils.behaviors.interpolators.*;
import java.util.Date;
import java.awt.Color;

/**
 * This class describe the scene of the sun.
 *
 * @author Marcel Portner & Bernhard Hari
 * @version $Revision: 1.16 $
 * @see Sun
 */
public class SceneSun implements SceneCelestialObjects, PositionObserver, InfoObserver {

   private RotationInterpolator rotator;
   private Alpha rotationAlpha;
   private long alphaTime = 0;
   private Date date;
   
   private BranchGroup removableCoordBG;
   private TransformGroup trGrScaleAndLeafs;
   private TransformGroup trGrRot;

   private Sun sun;

   private ObjectsPositions objPos;
   private ObjectsInformation objInfo;
   private InitializationObject iniObject;

   /**
    * Initializes a new SceneSun.
    *
    * @param sun      a object reference of the sun.
    * @param objPos   a reference to the concrete subject of 
    *                 the observer pattern positions
    * @param objInfo  a reference to the concrete subject of
    *                 the observer pattern information
    */
   public SceneSun(Sun sun, ObjectsPositions objPos,
                   ObjectsInformation objInfo) {
      this.sun = sun;
      this.objPos = objPos;
      this.objInfo = objInfo;
      this.objPos.attach(this);
      this.objInfo.attach(this);
      this.iniObject = objInfo.getInitializationObject();
   }

   /**
    * Updates the position state of the object.<br>
    *
    * @param orbitPos a reference to the list containing all position observer
    */
   public void update(OrbitPositions orbitPos) {

   }

   /**
    * Updates the information state of the object.<br>
    *
    * @param id        identification of the object that has to be updated
    * @param parameter the parameter that has been changed
    */
   public void update( String id, int parameter ) {
   
      if ( id.equals( sun.getId() ) ||
         id.equals("all") ) {
         
         IniData data = (IniData)objInfo.getParameter( XMLConstants.STAR, sun.getId() );
         
         switch ( parameter ) {
            case XMLConstants.VISIBLE :
                 // implemented inside SceneSolarSystem
                 break;
            case XMLConstants.COORDINATESYSTEM :
                 if ( data.hasCoordinateSystem() ) {
                    addCoord();
                 } else  {
                    removeCoord();
                 }
                 break;
            case XMLConstants.ORBIT :
                 if ( data.hasOrbit() ) {
                    addOrbit();
                 } else  {
                    removeOrbit();
                 }
                 break;
            case XMLConstants.COLORORBIT :
                 setOrbitColor( data.getColorOrbit());
                 break;
            case XMLConstants.ANIMATIONSPEED :
                 setAnimSpeed();
                 break;
            default : break;
         
         }
      }
   }

   /**
    * Removes this object from the position observer list.
    */
   public void destroy() {
      objPos.detach(this);
      objInfo.detach(this);
      sun = null;      
   }

   /**
    * Returns the ID of the sun.
    *
    * @return the ID to idendification the sun.
    */
   public String getId() {
      return sun.getId();
   }

   /**
    * Returns a CelestialObjectInfo of the sun.
    * This object has all specific information of the sun.
    *
    * @return a CelestialObjectInfo of the sun.
    */
   public CelestialObjectInfo getInfo() {
      return sun.getInfo();
   }

   /**
    * This method handle the rotation of the sun.
    *
    * @param animate  if true, start animation; if false stop animation.
    * @param rotate   if true, start rotation; if false stop rotation.
    */
   public void setAnimation(boolean animate, boolean rotate) {
      date = new Date();
      if(animate) {
         // set the stop time for the rotation.
         alphaTime = date.getTime() - alphaTime;
         rotationAlpha.setStartTime(alphaTime);
      } else {
         // save the stop time for the rotation.
         alphaTime = rotationAlpha.getStartTime();
         alphaTime = date.getTime() - alphaTime;
      }
      // set the rotation to stop or go.
      rotator.setEnable(rotate);
   }

   /**
    * Set the animation speed for the sun.
    */
   public void setAnimSpeed() {
      date = new Date();
      
      AnimationSpeed animationSpeed = (AnimationSpeed)objInfo.getInitializationObject().getParameter(XMLConstants.ANIMATIONSPEED);
      int factor = 0;
      switch(animationSpeed.getType()) {
        case AnimationSpeed.DAYS_PER_SECOND:
          factor = 1;
          break;
        case AnimationSpeed.HOURS_PER_SECOND:
          factor = 24;
          break;
        case AnimationSpeed.MINUTES_PER_SECOND:
          factor = 24 * 60;
          break;
        default:
      }
      int animSpeed = animationSpeed.getValue();
      long duration = (long)(sun.getRotOwnAxis() * factor / animSpeed);
      
      long increasing = rotationAlpha.getIncreasingAlphaDuration();
      // delta t = t1*(1-T2/T1)
      long diff = (long)((date.getTime() - rotationAlpha.getStartTime()) *
                         (1.0f - (float)duration / (float)increasing));
      rotationAlpha.setIncreasingAlphaDuration(duration);
      rotationAlpha.setStartTime(rotationAlpha.getStartTime() + diff);
   }

   /**
    * Add the coordinate system to this scenegraph.
    */
   public void addCoord() {
      trGrScaleAndLeafs.addChild(removableCoordBG);
   }
   
   /**
    * Remove the coordinate system from this scenegraph.
    */
   public void removeCoord() {
      removableCoordBG.detach();
   }

   /**
    * The sun has no color orbit.
    */
   public void addOrbit() {

   }
   
   /**
    * The sun has no color orbit.
    */
   public void removeOrbit() {

   }

   /**
    * The sun has no color orbit.
    */
   public void setOrbitColor(Color orbitColor) {

   }

   /**
    * Get the current position of the sun.
    *
    * @return a Transform3D with the current position.
    */
   public Transform3D getCurrentPosition() {
      Transform3D transform = new Transform3D();
      trGrRot.getTransform(transform);
      return transform;
   }

   /**
    * Returns the BranchGroup of the created sun scene.
    * It has the sun as leaf and it make a rotation.
    *
    * @return the BranchGroup of the sun.
    */
   public BranchGroup createSceneGraph() {

      // A BoundingSphere instance as general bounding region.
      BoundingSphere boundsGen = new BoundingSphere(new Point3d(0.0, 0.0, 0.0),
                                                    SSS3dConstants.BOUNDRADIUS);

      // set scale of the sun
      float scale;
      if(((Boolean)iniObject.getParameter(XMLConstants.COMPRESSED)).booleanValue()) {
         scale = sun.getLogRadius();
      } else {
         scale = sun.getRadius();
      }
      Transform3D tr3Dscale = new Transform3D();
      tr3Dscale.setScale((double)scale);
      
      // Create the first TransformGroup node trGrScaleAndLeafs for the sun,
      // the coordinate system and the Transform3D scale.
      trGrScaleAndLeafs = new TransformGroup(tr3Dscale);

      trGrScaleAndLeafs.setCapability(Group.ALLOW_CHILDREN_READ);
      trGrScaleAndLeafs.setCapability(Group.ALLOW_CHILDREN_WRITE);
      trGrScaleAndLeafs.setCapability(Group.ALLOW_CHILDREN_EXTEND);

      // Attach the leaf node sun to the TransformGroup trGrRotAndLeafs.
      trGrScaleAndLeafs.addChild(sun);  // Sphere sun

      removableCoordBG = new BranchGroup();
      
      // Create and attach a coordinate system to the TransformGroup node
      // trGrRotAndLeafs, that is to the sun.
      removableCoordBG.addChild(new CoordinateSystem(1.2f));
      
      // Allow to detach the Coordinate System
      removableCoordBG.setCapability(BranchGroup.ALLOW_DETACH);

      // Set the picking of the CoordinateSystem BranchGroup to false.
      removableCoordBG.setPickable(false);
      
      trGrScaleAndLeafs.addChild(removableCoordBG);
      
      // Create the second TransformGroup node trGrRot for the rotation
      trGrRot = new TransformGroup();
      
      // With the ALLOW_TRANSFORM_READ and ALLOW_TRANSFORM_WRITE
      // capabilities, we allow the modification of the TransformGroup's
      // code by the Behavior's code at run time.
      trGrRot.setCapability(TransformGroup.ALLOW_TRANSFORM_READ);
      trGrRot.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);

      // Attach the trGrScaleAndLeafs node to the trGrRot node.
      trGrRot.addChild(trGrScaleAndLeafs);
      
      // Prepare the RotationInterpolator (Behavior) for the
      // sun's rotation about its own axis.
      // Create the alpha(t) function.
      rotationAlpha = new Alpha(-1, Alpha.INCREASING_ENABLE, 0, 0,
                                /*sun.getRotOwnAxis()*/Long.MAX_VALUE, 0, 0, 0, 0, 0);

      // Create the sun's rotation about its own axis.
      rotator = new RotationInterpolator(rotationAlpha, trGrRot);
      
      rotator.setSchedulingBounds(boundsGen);

      trGrRot.addChild(rotator);

      BranchGroup brGrAll = new BranchGroup();
      brGrAll.addChild(trGrRot);

      brGrAll.setCapability(BranchGroup.ALLOW_DETACH);
      
      // Return the final version of the BranchGroup node brGrAll.
      return brGrAll;
   }
}
