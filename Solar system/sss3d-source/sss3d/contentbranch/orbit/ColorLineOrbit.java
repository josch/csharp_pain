/*
  File: ColorLineOrbit.java

  University of Applied Science Berne,HTA-Biel/Bienne,
  Computer Science Department.

  Diploma thesis J3D Solar System Simulator
  Originally written by Marcel Portner & Bernhard Hari (c) 2000
  
  CVS - Information :
  
  $Header: /var/cvsreps/projects/c450/2000/sss3d/source_diploma/sss3d/contentbranch/orbit/ColorLineOrbit.java,v 1.3 2000/12/12 16:04:55 harib Exp $
  $Author: harib $
  $Date: 2000/12/12 16:04:55 $
  $State: Exp $
  
*/
package sss3d.contentbranch.orbit;

import javax.media.j3d.*;
import javax.vecmath.*;
import java.awt.Color;

/**
 * This is a class for the color line orbit of the planet.
 *
 * @author Marcel Portner & Bernhard Hari
 * @version $Revision: 1.3 $
 */
public class ColorLineOrbit extends ColorOrbit {

   private LineStripArray orbit;
   
   /**
    * Initializes a new ColorLineOrbit.
    *
    * @param orbitColor  the awt Color for the orbit.
    */
   public ColorLineOrbit(Color orbitColor) {
      super(orbitColor);
   }

   /**
    * This method set the color orbit with the given positions.
    *
    * @param pos  the positions of the orbit.
    */
   public void setPositions(Point3f[] pos) {
      nbOfPoints = pos.length + 1;
      Color3f[] colors = new Color3f[nbOfPoints];
      Color3f color = new Color3f(orbitColor);
      for(int i = 0; i < nbOfPoints; i++) {
         colors[i] = color;
      }

      int[] stripVertexCounts = { nbOfPoints };

      orbit = new LineStripArray(nbOfPoints,
                                 GeometryArray.COORDINATES |
                                 /*GeometryArray.NORMALS |*/
                                 GeometryArray.COLOR_3,
                                 stripVertexCounts);

      orbit.setCoordinates(0, pos);
      orbit.setCoordinate(nbOfPoints - 1, pos[0]);
      orbit.setColors(0, colors);
      orbit.setCapability(GeometryArray.ALLOW_COLOR_WRITE);

      this.setGeometry(orbit);
   }

   /**
    * This method re - set the color of the orbit.
    *
    * @param orbitColor  the awt Color for the orbit.
    */
   public void setColor(Color orbitColor) {
      Color3f[] colors = new Color3f[nbOfPoints];
      Color3f color = new Color3f(orbitColor);
      for(int i = 0; i < nbOfPoints; i++) {
         colors[i] = color;
      }
      orbit.setColors(0, colors);
   }
}

