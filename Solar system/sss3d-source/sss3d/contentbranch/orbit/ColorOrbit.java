/*
  File: ColorOrbit.java

  University of Applied Science Berne,HTA-Biel/Bienne,
  Computer Science Department.

  Diploma thesis J3D Solar System Simulator
  Originally written by Marcel Portner & Bernhard Hari (c) 2000
  
  CVS - Information :
  
  $Header: /var/cvsreps/projects/c450/2000/sss3d/source_diploma/sss3d/contentbranch/orbit/ColorOrbit.java,v 1.4 2000/12/13 13:37:43 portm Exp $
  $Author: portm $
  $Date: 2000/12/13 13:37:43 $
  $State: Exp $
  
*/
package sss3d.contentbranch.orbit;

import javax.media.j3d.Shape3D;
import javax.vecmath.Point3f;
import java.awt.Color;

/**
 * This is a abstract class for the orbit of the planet.
 *
 * @author Marcel Portner & Bernhard Hari
 * @version $Revision: 1.4 $
 * @see ColorPointOrbit
 * @see ColorLineOrbit
 */
abstract public class ColorOrbit extends Shape3D {

    protected Color orbitColor;
    protected int nbOfPoints;

   /**
    * Initializes a new ColorOrbit.
    *
    * @param orbitColor  the awt Color for the orbit.
    */
    public ColorOrbit(Color orbitColor) {
       this.orbitColor = orbitColor;
    }

   /**
    * This method set the color orbit with the given positions.
    *
    * @param pos  the positions of the orbit.
    */
    abstract public void setPositions(Point3f[] pos);

   /**
    * This method re - set the color of the orbit.
    *
    * @param orbitColor  the awt Color for the orbit.
    */
    abstract public void setColor(Color orbitColor);
}
