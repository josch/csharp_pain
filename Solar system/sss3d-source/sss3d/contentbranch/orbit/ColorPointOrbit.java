/*
  File: ColorPointOrbit.java

  University of Applied Science Berne,HTA-Biel/Bienne,
  Computer Science Department.

  Diploma thesis J3D Solar System Simulator
  Originally written by Marcel Portner & Bernhard Hari (c) 2000
  
  CVS - Information :
  
  $Header: /var/cvsreps/projects/c450/2000/sss3d/source_diploma/sss3d/contentbranch/orbit/ColorPointOrbit.java,v 1.4 2000/12/12 16:04:55 harib Exp $
  $Author: harib $
  $Date: 2000/12/12 16:04:55 $
  $State: Exp $
  
*/
package sss3d.contentbranch.orbit;

import javax.media.j3d.*;
import javax.vecmath.*;
import java.awt.Color;

/**
 * This is a class for the color point orbit of the planet.
 * <b>NOT USED ANYMORE FOR SOLAR SYSTEM SIMULATOR !</b>
 *
 * @author Marcel Portner & Bernhard Hari
 * @version $Revision: 1.4 $
 */
public class ColorPointOrbit extends ColorOrbit {

   private PointArray orbit;
   
   /**
    * Initializes a new ColorPointOrbit.
    *
    * @param orbitColor  the awt Color for the orbit.
    */
   public ColorPointOrbit(Color orbitColor) {
      super(orbitColor);
   }

   /**
    * This method set the color orbit with the given positions.
    *
    * @param pos  the positions of the orbit.
    */
   public void setPositions(Point3f[] pos) {
      nbOfPoints = pos.length;
      Color3f[] colors = new Color3f[nbOfPoints];
      Color3f color = new Color3f(orbitColor);
      for(int i = 0; i < nbOfPoints; i++) {
         colors[i] = color;
      }
 
      orbit = new PointArray(nbOfPoints,
                             GeometryArray.COORDINATES |
                             GeometryArray.COLOR_3);
 
      orbit.setCoordinates(0, pos);
      orbit.setColors(0, colors);
 
      this.setGeometry(orbit);
   }

   /**
    * This method re - set the color of the orbit.
    *
    * @param orbitColor  the awt Color for the orbit.
    */
   public void setColor(Color orbitColor) {
      Color3f[] colors = new Color3f[nbOfPoints];
      Color3f color = new Color3f(orbitColor);
      for(int i = 0; i < nbOfPoints; i++) {
         colors[i] = color;
      }
      orbit.setColors(0, colors);
   }
}
