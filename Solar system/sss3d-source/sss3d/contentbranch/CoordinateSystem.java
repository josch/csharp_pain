/*
  File: CoordinateSystem.java

  University of Applied Science Berne,HTA-Biel/Bienne,
  Computer Science Department.

  Diploma thesis J3D Solar System Simulator
  Originally written by Marcel Portner & Bernhard Hari (c) 2000
  
  CVS - Information :
  
  $Header: /var/cvsreps/projects/c450/2000/sss3d/source_diploma/sss3d/contentbranch/CoordinateSystem.java,v 1.3 2000/12/12 14:29:07 harib Exp $
  $Author: harib $
  $Date: 2000/12/12 14:29:07 $
  $State: Exp $
  
*/
package sss3d.contentbranch;

import javax.media.j3d.*;

/**
 * This class creates a right-handed 3D coordinate system.
 *
 * @author Marcel Portner & Bernhard Hari
 * @version $Revision: 1.3 $
 */
public class CoordinateSystem extends Shape3D {

   /**
    * Definition of the geometry of the three axis.
    */
   private static final float EXTREMITES[] = {
                                               // x-axis
                                               0.0f, 0.0f, 0.0f,
                                               1.0f, 0.0f, 0.0f,

                                               // y-axis
                                               0.0f, 0.0f, 0.0f,
                                               0.0f, 1.0f, 0.0f,

                                               // z-axis
                                               0.0f, 0.0f, 0.0f,
                                               0.0f, 0.0f, 1.0f
		                             };

   /**
    * Colors of the three axis.
    */
   private static final float COLOR[] = {
                                          // x-axis
                                          1.0f, 1.0f, 0.0f,
                                          1.0f, 1.0f, 0.0f,
                  
                                          // y-axis
                                          0.0f, 0.0f, 1.0f,
                                          0.0f, 0.0f, 1.0f,
                  
                                          // z-axis
                                          1.0f, 0.0f, 0.0f,
                                          1.0f, 0.0f, 0.0f
	                                };

   /**
    * The scale factor
    */
   private static final float SCALE = 1.0f;
   private float scale;

   /**
    * Initializes a new coordinate system with the length
    * of the axis to one meter.
    */
   public CoordinateSystem() {
      this(SCALE);
   }

   /**
    * Initializes a new coordinate system.
    *
    * @param scale  the scale factor to adjust the axis's length in meter.
    */
   public CoordinateSystem(float scale) {
      this.scale = scale;
      this.setGeometry(createGeometry());
   }

   /**
    * Returns the Geometry of the coordinate system.
    *
    * @return the Geometry of a coordinate system. The axis has three
    * different colors and is scaled.
    */
   private Geometry createGeometry() {

      // Construction of the axis (LineArray).
      LineArray axis = new LineArray(6, LineArray.COORDINATES |
	                             LineArray.COLOR_3);

      // Scalling of the vertices of the 3 axis using scale.
      float scaledExtremites[] = new float[EXTREMITES.length];
      for(int i = 0; i < EXTREMITES.length; i++) {
         scaledExtremites[i] = EXTREMITES[i] * scale;
      }
      
      axis.setCoordinates(0, scaledExtremites);
      axis.setColors(0, COLOR);

      return axis;
   }
}
