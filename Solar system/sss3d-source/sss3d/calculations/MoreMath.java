/*
  File: MoreMath.java

  University of Applied Science Berne,HTA-Biel/Bienne,
  Computer Science Department.

  Diploma thesis J3D Solar System Simulator
  Originally written by Marcel Portner & Bernhard Hari (c) 2000
  
  CVS - Information :
  
  $Header: /var/cvsreps/projects/c450/2000/sss3d/source_diploma/sss3d/calculations/MoreMath.java,v 1.2 2000/12/12 11:16:34 portm Exp $
  $Author: portm $
  $Date: 2000/12/12 11:16:34 $
  $State: Exp $
  
*/
package sss3d.calculations;

/**
 * This class contains mathematical function who are not supportet by JDK.<br>
 * For example sinus hyperbola:
 * <pre>
 *    use : MoreMath.sinh(x);
 * </pre> 
 *
 * @author Marcel Portner & Bernhard Hari
 * @version $Revision: 1.2 $
 */
public final class MoreMath {
   
   /**
    * The constant value of 2 * PI.
    */
   public static final double PI2  = 2.0 * StrictMath.PI;

   /**
    * The constant value for calculation to radian.
    */
   public static final double RAD  = StrictMath.PI / 180.0;

   /**
    * The constant value for calculation to degree.
    */
   public static final double DEG  = 180.0 / StrictMath.PI;

   /**
    * The constant value for calculation to arcs.
    */
   public static final double ARCS = 3600.0 * 180.0 / StrictMath.PI;

   /**
    * Give from a double value the decimal place.
    *
    * @param x  the double value where we want the decimal place.
    *
    * @return the decimal place of the double value.
    */
   public static double frac(double x) {
      return x - StrictMath.floor(x);
   }

   /**
    * Calculate x mod y.
    *
    * @param x  a double value.
    * @param y  a double value.
    *
    * @return the modulo part of x modulo y.
    */
   public static double modulo(double x, double y) {
      return y * frac(x / y);
   }

   /**
    * Calculate the sinus hyperbola.
    *
    * @param x  an angle, in radians.
    *
    * @return the sinh of x [sinh(x) = (e^x - e^-x) / 2].
    */
   public static double sinh(double x) {
      return (StrictMath.exp(x) - StrictMath.exp(-x)) / 2.0;
   }

   /**
    * Calculate the cosinus hyperbola.
    *
    * @param x  an angle, in radians.
    *
    * @return the cosh of x [cosh(x) = (e^x + e^-x) / 2].
    */
   public static double cosh(double x) {
      return (StrictMath.exp(x) + StrictMath.exp(-x)) / 2.0;
   }

   /**
    * Calculate the log of basis 10.
    *
    * @param x  a number greater than 0.0.
    *
    * @return the log of basis 10.
    */
   public static double log10(double x) {
      return StrictMath.log(x) / StrictMath.log(10);
   }

   /**
    * Calculate the cos(alpha+beta) and sin(alpha+beta) with the help of addition
    * low.
    *
    * @param c1  the cos of alpha.
    * @param s1  the sin of alpha.
    * @param c2  the cos of beta.
    * @param s2  the sin of beta.
    *
    * @return a double array with cos(alpha+beta) and sin(alpha+beta).
    */
   public static double[] addThe(double c1, double s1, double c2, double s2) {
      double[] cs = new double[2];
      cs[0] = c1 * c2 - s1 * s2;
      cs[1] = s1 * c2 + c1 * s2;
      return cs;
   }
}
