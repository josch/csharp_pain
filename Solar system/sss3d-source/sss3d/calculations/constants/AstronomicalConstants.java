/*
  File: AstronomicalConstants.java

  University of Applied Science Berne,HTA-Biel/Bienne,
  Computer Science Department.

  Diploma thesis J3D Solar System Simulator
  Originally written by Marcel Portner & Bernhard Hari (c) 2000
  
  CVS - Information :
  
  $Header: /var/cvsreps/projects/c450/2000/sss3d/source_diploma/sss3d/calculations/constants/AstronomicalConstants.java,v 1.5 2000/12/12 11:16:46 portm Exp $
  $Author: portm $
  $Date: 2000/12/12 11:16:46 $
  $State: Exp $
  
*/
package sss3d.calculations.constants;

/**
 * This class contains all important astronomical constants.<br>
 * For example AU is equivalent to one astronomical unit (AU):
 * <pre>
 *    use : AstronomicalConstants.AU = 149597870.66;
 * </pre> 
 *
 * @author Marcel Portner & Bernhard Hari
 * @version $Revision: 1.5 $
 */
public final class AstronomicalConstants {

   /**
    * The radius of the earth in [km].
    */
   public static final double R_EARTH   =   6378.137;      // [km]

   /**
    * The radius of the sun in [km].
    */
   public static final double R_SUN     = 696000.0;        // [km]

   /**
    * The radius of the moon in [km].
    */
   public static final double R_MOON    =   1737.4;        // [km]

   /**
    * The modified julianish time in the epoch 2000.
    */
   public static final double MJD_J2000 = 51544.5;         // MJD der Epoche J2000.0

   /**
    * The epoch of the year 2000.
    */
   public static final double T_J2000   =     0.0;         // Epoche J2000.0

   /**
    * The epoch of the year 1950.
    */
   public static final double T_B1950   =    -0.500002108; // Epoche B1950

   /**
    * The constant of gravitation.
    */
   public static final double KGAUSS    = 0.01720209895;   // Gravitationskonstante

   /**
    * The product of the constant gravitation and the central mass.
    */
   public static final double GM_SUN    = KGAUSS * KGAUSS; // [AE^3/d^2]

   /**
    * The astronomical unit [km].
    */
   public static final double AU        = 149597870.66;    // astronomical unit [km]

   /**
    * The speed of light.
    */
   public static final double C_LIGHT   = 173.14;          // Lichtgeschwindigkeit [AE/d]

   /**
    * A earth year in days.
    */
   public static final double DAYS      = 365.242190;

   /**
    * The 'smallest' exactness of a double value.
    * Value from the C++ library.
    */
   public static final double EPSILON   = 2.2204460492503131E-016;
}