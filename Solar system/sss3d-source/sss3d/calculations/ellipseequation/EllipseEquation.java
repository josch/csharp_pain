/*
  File: EllipseEquation.java

  University of Applied Science Berne,HTA-Biel/Bienne,
  Computer Science Department.

  Diploma thesis J3D Solar System Simulator
  Originally written by Marcel Portner & Bernhard Hari (c) 2000
  
  CVS - Information :
  
  $Header: /var/cvsreps/projects/c450/2000/sss3d/source_diploma/sss3d/calculations/ellipseequation/EllipseEquation.java,v 1.4 2000/12/13 14:08:11 portm Exp $
  $Author: portm $
  $Date: 2000/12/13 14:08:11 $
  $State: Exp $
  
*/
package sss3d.calculations.ellipseequation;

import sss3d.calculations.*;
import sss3d.contentbranch.*;

import javax.vecmath.*;

/**
 * Used to calculate the planet positions using ellipse equation.<br>
 * This is a simple calculation method. For exact positions you
 * have to use the class KeplerEquation.
 * For example:
 * <pre>
 *    HOWTO use class / spezial Instructions.
 *    EllipseEquation ellipseeq = new EllipseEquation();
 *    Point3f [] positions = ellipseeq.getPositions(CelestialObjectInfo,0);
 * </pre> 
 *
 * @author Marcel Portner & Bernhard Hari
 * @version $Revision: 1.4 $
 * @see sss3d.calculations.keplerequation.KeplerEquation
 */
public class EllipseEquation extends OrbitCalculator {
	
   private double max;       // maximum distance from sun
   private double min;       // minimum distance from sun
   private double x0 = 0;    // center -> sun X-axe 
   private double y0 = 0;    // center -> sun Y-axe
   private double z0 = 0;    // center -> sun Z-axe
   private double alpha = 0; // angle - orbit inclination to ecliptic

   /**
    * Returns the positions of the given planet.
    * The algorithm used is an ellipse equation.  
    *
    * @param     objInfo  Information about the planet.
    * @param     jDay calculates positions corresponding to the given julian day
    * @param     compressed true if the universe is logorithmic compressed or false if 
    *            it has the real proportion.
    * @return    Point3f[] an array of vectors containing the positions.
    */   
   public Point3f[] getPositions(CelestialObjectInfo objInfo, double jDay, boolean compressed ) {
     
     /**
      * We used PLANPOS from the Book "Astronomie mit dem Personal Computer"
      * - ISBN : 3-540-66218-9 3.Auflage, to calculate the first positions of
      * the planets. Date given : Year 2000, Jan, 1., Time 00.000. 
      * J2000  =>   JD : 2451544.5
      * We got these values :
      *                [degree] [minutes] [seconds]
      * Sun             0        0         0
      * Mercury         252      24        36.4
      * Venus           181      47        37.5
      * Earth            99      52        04.0
      * Mars            359      08        04.0 
      * Jupiter          36      14        57.3
      * Saturn           45      42        12.5
      * Uranus          316      24        41.6
      * Neptune         303      55        47.9 
      * Pluto           250      32        32.0
      */  
     
     double degree = 0.0;
     String name = (objInfo.getName()).toLowerCase();
     
     if(name.equals("sun")){
     	        degree = 0.0;
     } else if(name.equals("mercury")){
     	        degree = 252.0;
     } else if(name.equals("venus")){
     	        degree = 181.0;      	
     } else if(name.equals("earth")){
     	        degree = 99.0;      	
     } else if(name.equals("mars")){
     	        degree = 359.0;      	
     } else if(name.equals("jupiter")){
     	        degree = 36.0;      	
     } else if(name.equals("saturn")){
     	        degree = 45.0;      	
     } else if(name.equals("uranus")){
     	        degree = 316.0;      	
     } else if(name.equals("neptune")){
     	        degree = 303.0;      	
     } else if(name.equals("pluto")){
     	        degree = 250.0;      	
     } else {
       // do nothing degree = 0.0       	
     }
     
     // last is used to mark the end of the array.
     int nbrOfPoints = objInfo.getNbrOfPositions();
     
     // calculate start position of planet (position);
     // between 0 and NbrOfPositions -1
     int start = (int) Math.round( (degree/360*(nbrOfPoints-1)) );
    
     max   = objInfo.getMaxDistanceFromSun();
     min   = objInfo.getMinDistanceFromSun();
     alpha = Math.toRadians(objInfo.getOrbitInclinationToEcliptic());
     
     Point3f positions[] = new Point3f[nbrOfPoints]; 
     
     double t = 0;
   
     for(int i = 0; i < nbrOfPoints; i++){	
       
       t = ((double)(i) / nbrOfPoints)*(2*Math.PI);
       
       // calculate each x,y,z position for every point 	
       double x = x0 + max*Math.cos(t);
       double z = z0 + (-min)*Math.sin(t);
       double y = y0 + x * Math.tan(alpha);
       
       // add calculated point to the point array
       // add start point of the planet (degree) to the beginning
       // of the positions array 
       if( i < start){
         positions[(nbrOfPoints - start + i)] = new Point3f((float)x,(float)y,(float)z);        	
       } else {
         positions[(i - start)] = new Point3f((float)x,(float)y,(float)z);        	
       }
         
     }
     return positions;  	
   }
   
   /**
    * Returns an array of positons for the given positions, based
    * on the given jDay.<br>
    * NOT IN USE AT THE MOMENT.  
    *
    * @param     objPos  actual positions of the object.
    * @param     jDay    calculates positions corresponding to the given julian day
    * @param     compressed true if the universe is logorithmic compressed or false if 
    *            it has the real proportion.
    * @return    Point3f[] an array of vectors containing the points
    */ 
   public Point3f[] updatePositions(Point3f[] objPos, double jDay, boolean compressed) {
      return objPos;
   }
}
