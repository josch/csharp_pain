/*
  File: Polar.java

  University of Applied Science Berne,HTA-Biel/Bienne,
  Computer Science Department.

  Diploma thesis J3D Solar System Simulator
  Originally written by Marcel Portner & Bernhard Hari (c) 2000
  
  CVS - Information :
  
  $Header: /var/cvsreps/projects/c450/2000/sss3d/source_diploma/sss3d/calculations/Polar.java,v 1.3 2000/12/12 11:16:35 portm Exp $
  $Author: portm $
  $Date: 2000/12/12 11:16:35 $
  $State: Exp $
  
*/
package sss3d.calculations;

/**
 * This class administrated the polar coordinate system.
 *
 * @author Marcel Portner & Bernhard Hari
 * @version $Revision: 1.3 $
 */
public class Polar {

   private double phi;    // Azimut des Vektors
   private double theta;  // Elevation des Vektors
   private double r;      // Betrag des Vektors

   private double x, y, z;

   /**
    * Constructor
    */
   public Polar() {
      phi = 0.0;
      theta = 0.0;
      r = 0.0;
      x = 0.0;
      y = 0.0;
      z = 0.0;
   }
   
   /**
    * Initializes the Polar with distance = 1.0
    * and a given azimut and elevation.
    *
    * @param az  the azimut.
    * @param elev  the elevation.
    */
   public Polar(double az, double elev) {
      phi = az;
      theta = elev;
      r = 1.0;
      toXYZ();
   }

   /**
    * Initializes the Polar with a distance,
    * a given azimut and elevation.
    *
    * @param az  the azimut.
    * @param elev  the elevation.
    * @param r  the distance.
    */
   public Polar(double az, double elev, double r) {
      phi = az;
      theta = elev;
      this.r = r;
      toXYZ();
   }
   
   /**
    * Set the Polar with a distance,
    * a given azimut and elevation.
    *
    * @param az  the azimut.
    * @param elev  the elevation.
    * @param r  the distance.
    */
   public void setPolar(double az, double elev, double r) {
      phi = az;
      theta = elev;
      this.r = r;
      toXYZ();
   }

   /**
    * Scale the distance (r) of the celestial object position
    * to a useful value.
    *
    * @param logarithmic  the distance in logarithmic factor or only a
    *                     scale factor.
    * @param scale  the scaling factor.
    */
   public void scaleDistance(boolean logarithmic, double scale) {
      if(logarithmic) {
         r = 5.0 * StrictMath.log(r * scale);
      } else {
         r *= scale;
      }
      toXYZ();
   }

   /**
    * Calculate the r, phi and theta to x, y and z.
    */
   private void toXYZ() {
      x = r * StrictMath.cos(phi) * StrictMath.cos(theta);
      y = r * StrictMath.sin(phi) * StrictMath.cos(theta);
      z = r                       * StrictMath.sin(theta);
   }

   /**
    * Set the x, y and z value and calculate the
    * r, phi and theta parameter.
    *
    * @param x  the x position in the universe.
    * @param y  the y position in the universe.
    * @param z  the z position in the universe.
    */
   public void setXYZ(double x, double y, double z) {

      this.x = x;
      this.y = y;
      this.z = z;

      // Laenge der Projektion des Vektors in die x-y-Ebene
      double rohSqr = x * x + y * y;

      // Betrag des Vektors
      r = StrictMath.sqrt(rohSqr + z * z);

      // Azimut des Vektors
      if((x == 0.0) && (y == 0.0)) {
         phi = 0.0;
      } else {
         phi = StrictMath.atan2(y, x);
      }
      if(phi < 0.0) {
         phi += MoreMath.PI2;
      }
      
      // Elevation des Vektors
      double roh = StrictMath.sqrt(rohSqr);
      if((z == 0.0) && (roh == 0.0)) {
         theta = 0.0;
      } else {
         theta = StrictMath.atan2(z, roh);
      }
   }

   /**
    * Get the azimut (phi) of the current position.
    *
    * @return the azimut.
    */
   public double getAzimut() {
      return phi;
   }

   /**
    * Get the elevation (theta) of the current position.
    *
    * @return the elevation.
    */
   public double getElevation() {
      return theta;
   }

   /**
    * Get the distance (r) of the current position.
    *
    * @return the distance.
    */
   public double getDistance() {
      return r;
   }

   /**
    * Get the x value of the current position.
    *
    * @return the x value.
    */
   public double getX() {
      return x;
   }

   /**
    * Get the y value of the current position.
    *
    * @return the y value.
    */
   public double getY() {
      return y;
   }

   /**
    * Get the z value of the current position.
    *
    * @return the z value.
    */
   public double getZ() {
      return z;
   }
}
