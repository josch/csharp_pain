/*
  File: DMSConv.java

  University of Applied Science Berne,HTA-Biel/Bienne,
  Computer Science Department.

  Diploma thesis J3D Solar System Simulator
  Originally written by Marcel Portner & Bernhard Hari (c) 2000
  
  CVS - Information :
  
  $Header: /var/cvsreps/projects/c450/2000/sss3d/source_diploma/sss3d/calculations/DMSConv.java,v 1.2 2000/12/12 11:16:34 portm Exp $
  $Author: portm $
  $Date: 2000/12/12 11:16:34 $
  $State: Exp $
  
*/
package sss3d.calculations;

/**
 * This class convert the angle.
 *
 * @author Marcel Portner & Bernhard Hari
 * @version $Revision: 1.2 $
 */
public class DMSConv {

   private int d;      // Winkelgrade
   private int m;      // Bogenminuten
   private double s;   // Bogensekunden

   private double dd;  // Winkel in Grad in dezimaler Darstellung
   
   /**
    * Initializes the DMSConv object with all value to zero.
    */
   public DMSConv() {
      d  = 0;
      m  = 0;
      s  = 0.0;
      dd = 0.0;
   }

   /**
    * Calculate the arcminute and arcsecond to a given angle.
    *
    * @param dd  the angle in degree in decimal notation.
    */
   public DMSConv(double dd) {
      this.dd = dd;
      double x = StrictMath.abs(dd);
      d = (int)x;
      x = (x - d) * 60.0;
      m = (int)x;
      s = (x - m) * 60.0;
      
      if(dd < 0.0) {
         if(d != 0) {
            d *= -1;
         } else {
            if(m != 0) {
               m *= -1;
            } else {
               s *= -1.0;
            }
         }
      }
   }
   
   /**
    * Calculate the angle in decimal notation with the three values.
    *
    * @param d  the angle in degree [�].
    * @param m  the arcminute ['].
    * @param s  the arcsecond [''].
    */
   public DMSConv(int d, int m, double s) {
      this.d = d;
      this.m = m;
      this.s = s;

      double sign;
      if((d < 0) || (m < 0) || (s < 0)) {
         sign = -1.0;
      } else {
         sign =  1.0;
      }
        
      dd = sign * (StrictMath.abs(d) + StrictMath.abs(m) / 60.0 +
                   StrictMath.abs(s) / 3600.0);
   }
   
   /**
    * Give the angle (or the hour).
    *
    * @return the angle.
    */
   public int getAngle() {
      return d;
   }

   /**
    * Give the arcminute (or minute).
    *
    * @return the arcminute.
    */
   public int getMin() {
      return m;
   }

   /**
    * Give the arcsecond (or second).
    *
    * @return the arcsecond.
    */
   public double getSec() {
      return s;
   }

   /**
    * Give the angle in decimal notation.
    *
    * @return the angle.
    */
   public double getDecimal() {
      return dd;
   }
}
