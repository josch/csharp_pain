/*
  File: PositionsArray.java

  University of Applied Science Berne,HTA-Biel/Bienne,
  Computer Science Department.

  Diploma thesis J3D Solar System Simulator
  Originally written by Marcel Portner & Bernhard Hari (c) 2000
  
  CVS - Information :
  
  $Header: /var/cvsreps/projects/c450/2000/sss3d/source_diploma/sss3d/calculations/PositionsArray.java,v 1.2 2000/12/12 11:16:35 portm Exp $
  $Author: portm $
  $Date: 2000/12/12 11:16:35 $
  $State: Exp $
  
*/
package sss3d.calculations;

import javax.vecmath.Point3f;

/**
 * This class is used to keep the calculated positions of
 * a given object.
 *
 * @author Marcel Portner & Bernhard Hari
 * @version $Revision: 1.2 $
 */
public class PositionsArray {

   private String name;             // id of object
   private Point3f[] positions;     // positions of the given object

   /**
    * Constructor
    *
    * @param name the identification of the object.
    * @param positions[] an array of three-dimensional points.
    */
   public PositionsArray(String name, Point3f positions[]) {
   	this.name = name;
   	this.positions = positions;
   }

   /**
    * This method replaces the old positions of the object
    * with the given calculated positions.
    *
    * @param positions[] the new positions ;an array of three-dimensional points.
    */
   public void setPositions(Point3f positions[]){
       this.positions = null;
       this.positions = positions; 	
   }

   /**
    * Returns the positions associated with this object.
    *
    * @return Point3f[] an array of three-dimensional points.
    */
   public Point3f[] getPositions() {
   	return positions;
   }

   /**
    * Returns the id of this object.
    *
    * @return String the id of this object.
    */
   public String getId() {
     return name;	
   }
}
