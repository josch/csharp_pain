/*
  File: TimeCalc.java

  University of Applied Science Berne,HTA-Biel/Bienne,
  Computer Science Department.

  Diploma thesis J3D Solar System Simulator
  Originally written by Marcel Portner & Bernhard Hari (c) 2000
  
  CVS - Information :
  
  $Header: /var/cvsreps/projects/c450/2000/sss3d/source_diploma/sss3d/calculations/TimeCalc.java,v 1.3 2000/12/13 13:36:17 portm Exp $
  $Author: portm $
  $Date: 2000/12/13 13:36:17 $
  $State: Exp $
  
*/
package sss3d.calculations;

/**
 * This class calculate the time.
 *
 * @author Marcel Portner & Bernhard Hari
 * @version $Revision: 1.3 $
 */
public class TimeCalc {

   /**
    * Constructor
    */
   public TimeCalc() {

   }
   
   /**
    * Get the difference between the ephemeriden time and the world time.
    * This method can only calculate the difference of this time from 1825 to 2005.
    *
    * @param time  the time in julianish century.
    *
    * @return the difference between the the ephemeriden time and the world time in
    *         second.
    */
   public double getETminUT(double time) {
      int    i = (int)StrictMath.floor(time / 0.25);
      double t = time - i * 0.25;
      double dtsec;
    
      if((time < -1.75) || (0.05 < time)) {
         return Double.NaN;
      } else {
         switch(i) {
           case -7: dtsec = 10.4 + t * (-80.8 + t *
                            ( 413.9 + t * ( -572.3))); break; // 1825-
           case -6: dtsec =  6.6 + t * ( 46.3 + t *
                            (-358.4 + t * (   18.8))); break; // 1850-
           case -5: dtsec = -3.9 + t * (-10.8 + t *
                            (-166.2 + t * (  867.4))); break; // 1875-
           case -4: dtsec = -2.6 + t * (114.1 + t *
                            ( 327.5 + t * (-1467.4))); break; // 1900-
           case -3: dtsec = 24.2 + t * ( -6.3 + t *
                            (  -8.2 + t * (  483.4))); break; // 1925-
           case -2: dtsec = 29.3 + t * ( 32.5 + t *
                            (  -3.8 + t * (  550.7))); break; // 1950-
           case -1: dtsec = 45.3 + t * (130.5 + t *
                            (-570.5 + t * ( 1516.7))); break; // 1975-
           case  0: t += 0.25;
                    dtsec = 45.3 + t * (130.5 + t *
                            (-570.5 + t * ( 1516.7))); break; // 2000-
                                                              // 2005
           default: dtsec = Double.NaN;
         }
      }
      return dtsec;
   }
   
   
   /**
    * Get the Greenwich-time from a modified julianish time.
    *
    * @param time  the modified julianish time.
    *
    * @return the Greenwich-time in [rad].
    */
   public double gmst(double mjd) {
 
      double secs = 86400.0;        // Anzahl der Sekunden je Tag
      
      double mjd_0 = StrictMath.floor(mjd);
      double ut    = secs * (mjd - mjd_0);     // [s]
      double t_0   = (mjd_0 - 51544.5) / 36525.0; 
      double t     = (mjd   - 51544.5) / 36525.0; 
   
      double gmst  = 24110.54841 + 8640184.812866 * t_0 +
                     1.0027379093 * ut + (0.093104 - 6.2e-6 * t) * t * t;   // [sec]
   
      return (MoreMath.PI2 / secs) * MoreMath.modulo(gmst, secs);   // [Rad]
   }
   
   /**
    * Get the modified julianish time from the calendar date.
    *
    * @param year  the year.
    * @param month  the month.
    * @param day  the day.
    * @param hour  the hour.
    * @param min  the minute.
    * @param sec  the second.
    *
    * @return the modified julianish time.
    */
   public double mjd(int year, int month, int day, 
                     int hour, int min, double sec) {

      if(month <= 2) {
         month += 12;
         --year;
      }

      int b;
     
      if((10000L * (long)year + 100L * (long)month + (long)day) <= 15821004L) {
         b = -2 + ((year + 4716) / 4) - 1179;           // Julianischer Kalender
      } else {
         b = (year / 400) - (year / 100) + (year / 4);  // Gregorianischer Kalender
      }
      
      double mjdMidnight = (double)(365L * (long)year - 679004L + (long)b +
                                   (long)(30.6001 * (double)(month + 1)) + (long)day);
      DMSConv dms = new DMSConv(hour, min, sec);
      double fracOfDay = dms.getDecimal() / 24.0; 
   
      return mjdMidnight + fracOfDay;
   }
   
   /**
    * Get from a modified julianish time an int array with all calendar
    * values.
    *
    * @param mjd  the modified julianish time.
    *
    * @return an int array with the follow values:<br>
    *         int[0] = year.<br>
    *         int[1] = month.<br>
    *         int[2] = day.<br>
    *         int[3] = hour.<br>
    *         int[4] = minute.<br>
    *         int[5] = second.
    */
   public int[] calDat(double mjd) {

      int[] result = new int[6];
    
      // Rechne julianische Tageszahl in Kalenderdatum um
      long a = (long)(mjd + 2400001.0);
      long b, c;
      
      if(a < 2299161L) {  // Julianischer Kalender
         b = 0;
         c = a + 1524L;
      } else {                // Gregorianischer Kalender
         b = (long)(((double)a - 1867216.25) / 36524.25);
         c = a + b - (b / 4L) + 1525L;
      }
    
      long d = (long)(((double)c - 122.1) / 365.25);
      long e = 365L * d + d / 4;
      long f = (long)((double)(c - e) / 30.6001);
    
      result[2] = (int)(c - e - (long)(30.6001 * (double)f));          // Day
      result[1] = (int)(f - 1L - 12L * (f / 14L));                     // Month
      result[0] = (int)(d - 4715L - ((7L + (long)result[1]) / 10L));   // Year
      
      double time = 24.0 * (mjd - StrictMath.floor(mjd));
      DMSConv dms = new DMSConv(time);
      result[3] = dms.getAngle();                                      // Hour
      result[4] = dms.getMin();                                        // Minute
      result[5] = (int)dms.getSec();                                   // Second

      return result;
   }
}