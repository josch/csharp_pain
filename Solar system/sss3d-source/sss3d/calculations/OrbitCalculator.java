/*
  File: OrbitCalculator.java

  University of Applied Science Berne,HTA-Biel/Bienne,
  Computer Science Department.

  Diploma thesis J3D Solar System Simulator
  Originally written by Marcel Portner & Bernhard Hari (c) 2000
  
  CVS - Information :
  
  $Header: /var/cvsreps/projects/c450/2000/sss3d/source_diploma/sss3d/calculations/OrbitCalculator.java,v 1.4 2000/12/13 13:36:17 portm Exp $
  $Author: portm $
  $Date: 2000/12/13 13:36:17 $
  $State: Exp $
  
*/
package sss3d.calculations;

import sss3d.contentbranch.CelestialObjectInfo;

import javax.vecmath.*;

/**
 * Abstract class that defines the skeleton for all used calculation
 * methods. At the moment we have implemented the ellipse equation method.
 * The method Kepler equation is not implemented yet.
 *
 * @author Marcel Portner & Bernhard Hari
 * @version $Revision: 1.4 $
 * @see sss3d.calculations.ellipseequation.EllipseEquation
 * @see sss3d.calculations.keplerequation.KeplerEquation
 */
abstract public class OrbitCalculator {
    /**
     * Returns the positions of the given object.
     *
     * @param     objInfo  Information about the object.
     * @param     jDay calculates positions corresponding to the given julian day.
     * @param     compressed true if the universe is logorithmic compressed or false if 
     *            it has the real proportion.
     * @return    Point3f[] an array of vectors containing the positions.
     */  	
    public abstract Point3f[] getPositions(CelestialObjectInfo objInfo, double jDay, boolean compressed);

    /**
     * Returns an array of positons for the given positions, based
     * on the given jDay.<br>
     * NOT IN USE AT THE MOMENT.  
     *
     * @param     objPos  actual positions of the object.
     * @param     jDay    calculates positions corresponding to the given julian day.
     * @param     compressed true if the universe is logorithmic compressed or false if 
     *            it has the real proportion.
     * @return    Point3f[] an array of vectors containing the points
     */
    public abstract Point3f[] updatePositions(Point3f[] objPos, double jDay, boolean compressed);
}
