/*
  File: Vec3D.java

  University of Applied Science Berne,HTA-Biel/Bienne,
  Computer Science Department.

  Diploma thesis J3D Solar System Simulator
  Originally written by Marcel Portner & Bernhard Hari (c) 2000
  
  CVS - Information :
  
  $Header: /var/cvsreps/projects/c450/2000/sss3d/source_diploma/sss3d/calculations/Vec3D.java,v 1.6 2000/12/15 02:48:08 portm Exp $
  $Author: portm $
  $Date: 2000/12/15 02:48:08 $
  $State: Exp $
  
*/
package sss3d.calculations;

import javax.vecmath.Vector3d;
import javax.vecmath.Matrix3d;

/**
 * This class expand the Vector3d class of the Java3D.
 * Now we can use also the polar coordinate system and
 * we have two new calculation methode.
 *
 * @author Marcel Portner & Bernhard Hari
 * @version $Revision: 1.6 $
 * @see sss3d.calculations.Polar
 */
public class Vec3D extends Vector3d {

   private Polar polar;

   /**
    * Initializes a new Vec3D with a reference to the Polar class.
    */
   public Vec3D() {
      super();
      polar = new Polar();
   }
   
   /**
    * Initializes a new Vec3D with his components.
    *
    * @param x  the x component.
    * @param y  the y component.
    * @param z  the z component.
    */
   public Vec3D(double x, double y, double z) {
      super(x, y, z);
      polar = new Polar();
      polar.setXYZ(x, y, z);
   }
   
   /**
    * Initializes a new Vec3D with a polar component object.
    *
    * @param polar  a polar object with a radius
    *               and two angles.
    * @see sss3d.calculations.Polar
    */
   public Vec3D(Polar polar) {
      super();
      this.polar = polar;
      x = polar.getX();
      y = polar.getY();
      z = polar.getZ();
   }
   
   /**
    * Calculate the norm of this vector.
    *
    * @return the norm of this vector.
    */
   public double norm() {
      return StrictMath.sqrt(dot(this));
   }
   
   /**
    * The multiplication (product) of this vector and a matrix.
    * The result is in this Vec3D object.
    *
    * @param mat  a Matrix3d object.
    */
   public void mul(Matrix3d mat) {
      double m_x = 0.0;
      m_x += mat.m00 * x;
      m_x += mat.m10 * y;
      m_x += mat.m20 * z;
  
      double m_y = 0.0;
      m_y += mat.m01 * x;
      m_y += mat.m11 * y;
      m_y += mat.m21 * z;
  
      double m_z = 0.0;
      m_z += mat.m02 * x;
      m_z += mat.m12 * y;
      m_z += mat.m22 * z;
  
      x = m_x;
      y = m_y;
      z = m_z;
   }

   /**
    * Scale the distance (r) of the celestial object position
    * to a useful value.
    *
    * @param logarithmic  the distance in logarithmic factor or only a
    *                     scale factor.
    * @param scale  the scaling factor.
    */
   public void scaleDistance(boolean logarithmic, double scale) {
      polar.setXYZ(x, y, z);
      polar.scaleDistance(logarithmic, scale);
      x = polar.getX();
      y = polar.getY();
      z = polar.getZ();
   }

   /**
    * Calculate the azimut (phi) of this vector.
    *
    * @return the azimut.
    */
   public double getAzimut() {
      polar.setXYZ(x, y, z);
      return polar.getAzimut();
   }

   /**
    * Calculate the elevation (theta) of this vector.
    *
    * @return the elevation.
    */
   public double getElevation() {
      polar.setXYZ(x, y, z);
      return polar.getElevation();
   }

   /**
    * Calculate the distance (r) of this vector.
    *
    * @return the distance.
    */
   public double getDistance() {
      polar.setXYZ(x, y, z);
      return polar.getDistance();
   }
}
