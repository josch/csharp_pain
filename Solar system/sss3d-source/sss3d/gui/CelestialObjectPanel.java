/*
  File: CelestialObjectPanel.java

  University of Applied Science Berne,HTA-Biel/Bienne,
  Computer Science Department.

  Diploma thesis J3D Solar System Simulator
  Originally written by Marcel Portner & Bernhard Hari (c) 2000

  CVS - Information :

  $Header: /var/cvsreps/projects/c450/2000/sss3d/source_diploma/sss3d/gui/CelestialObjectPanel.java,v 1.8 2000/12/12 12:01:01 harib Exp $
  $Author: harib $
  $Date: 2000/12/12 12:01:01 $
  $State: Exp $

*/

package sss3d.gui;

import sss3d.gui.*;
import sss3d.contentbranch.*;
import sss3d.utils.*;
import sss3d.utils.xmlparser.XMLConstants;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.event.*;

/**
 * Shows the current settings of a given CelestialObject.
 *   
 * @author Marcel Portner & Bernhard Hari
 * @version $Revision: 1.8 $
 */
public class CelestialObjectPanel extends JPanel implements ActionListener{

  private JLabel     name;
  private JLabel     type;
  private JLabel     visible          = new JLabel("Visible");
  private JLabel     coordinateSystem = new JLabel("Coordinate System");
  private JLabel     orbit            = new JLabel("Orbit");
  private JLabel     colorOrbit       = new JLabel("Current Orbit Color");
  private JLabel     currentColor     = new JLabel();
  private JCheckBox  cbVisible        = new JCheckBox();
  private JCheckBox  cbCoordSys       = new JCheckBox();
  private JCheckBox  cbOrbit          = new JCheckBox();
  private JPanel     upperPanel;       
  private JPanel     colorPanel       = new JPanel();
  private JPanel     buttonPanel      = new JPanel();
  private JButton    update           = new JButton("Update");
 
  private InitializationPanel iniPanel;
  private IniData data;
  private Color newColor = null;
  private int parameter;

  /**
   * Creates a CelestialObjectPanel depending on the given parameter and object name.
   *
   * @param iniPanel an object reference to an instance of InitializationPanel
   * @param parameter either XMLConstants.STAR,XMLConstants.PLANET, XMLConstants.MOON,
   *                  XMLConstants.COMET, XMLConstants.SATELLITE or XMLConstants.ROCKET
   * @param objectName the name of the object
   */
  public CelestialObjectPanel( InitializationPanel iniPanel , int parameter, String objectName  ) {
    super();
    this.iniPanel  = iniPanel;
    this.parameter = parameter;
    this.data = (IniData)iniPanel.getObjectsInformation().getParameter(parameter,objectName);

    if ( data.getType() == SSS3dConstants.ROCKET_TYPE ) {
      upperPanel = new JPanel( new GridLayout(2,2) );
    } else  {
      upperPanel = new JPanel( new GridLayout(5,2) );
    }
    
    // create GUI
    setLayout( new BoxLayout( this, BoxLayout.Y_AXIS ) );
    this.setAlignmentY(0.0f);
    
    switch ( data.getType() ) {
      case SSS3dConstants.UNKNOWN_TYPE :
           type = new JLabel("Unknown");
           break;
      case SSS3dConstants.INI_TYPE :
           type = new JLabel("Ini");
           break;
      case SSS3dConstants.CELESTIALOBJECT_TYPE :
           type = new JLabel("Celestial Object");
           break;
      case SSS3dConstants.STAR_TYPE :
           type = new JLabel("Star");
           break;
      case SSS3dConstants.PLANET_TYPE :
           type = new JLabel("Planet");
           break;
      case SSS3dConstants.MOON_TYPE :
           type = new JLabel("Moon");
           break;
      case SSS3dConstants.COMET_TYPE :
           type = new JLabel("Comet");
           break;
      case SSS3dConstants.SATELLITE_TYPE :
           type = new JLabel("Satellite");
           break;
      case SSS3dConstants.ROCKET_TYPE :
           type = new JLabel("Rocket");
           break;  
      default:
           type = new JLabel("Unknown"); 
           break;                                                                               
    }
    upperPanel.add(type);
    name = new JLabel(data.getName());
    upperPanel.add(name);
    
    upperPanel.add(visible);
    cbVisible.setSelected( data.isVisible() );
    upperPanel.add(cbVisible);
    
    if ( data.getType() != SSS3dConstants.ROCKET_TYPE ) {
      upperPanel.add(coordinateSystem);
      cbCoordSys.setSelected( data.hasCoordinateSystem() );
      upperPanel.add(cbCoordSys);
    }
    
    upperPanel.setAlignmentY(TOP_ALIGNMENT);
    add(upperPanel);
    
    if ( data.getType() != SSS3dConstants.STAR_TYPE &&
         data.getType() != SSS3dConstants.ROCKET_TYPE ) {
      upperPanel.add(orbit);
      cbOrbit.setSelected( data.hasOrbit() );
      upperPanel.add(cbOrbit);
    
      upperPanel.add(colorOrbit);
      currentColor.setOpaque(true);
      currentColor.setBackground( data.getColorOrbit() );
      upperPanel.add(currentColor);

    
      colorPanel.setLayout( new BoxLayout( colorPanel, BoxLayout.Y_AXIS ));
      //Set up color chooser for setting text color
      final JColorChooser tcc = new JColorChooser( this.data.getColorOrbit() );
      tcc.getSelectionModel().addChangeListener(
              new ChangeListener() {
                  public void stateChanged(ChangeEvent e) {
                      newColor = tcc.getColor();
                      currentColor.setBackground( newColor );
                    
                  }
              }
      );
      tcc.setPreviewPanel(new JPanel());
      tcc.setBorder(BorderFactory.createTitledBorder(
                                             "Choose orbit color"));
                                             
      colorPanel.add(tcc);
      colorPanel.setAlignmentY(TOP_ALIGNMENT);
      add(colorPanel);
    }
    
        
    buttonPanel.setLayout( new BoxLayout( buttonPanel, BoxLayout.X_AXIS ));
    update.setActionCommand("update");
    update.addActionListener(this);
    buttonPanel.add(update);
    buttonPanel.setAlignmentY(TOP_ALIGNMENT);
    
    add(buttonPanel);
    
    setBorder(BorderFactory.createEmptyBorder(10,10,10,10));
    
    // enable/disable components depending on they can be changed during
    // runtime mode 
    if ( iniPanel != null ) {
      switch ( iniPanel.getMode() ) {
        case SSS3dConstants.RUNTIME_MODE :
             cbVisible.setEnabled(XMLConstants.MODIFIABLE_VALUES_AT_RUNTIME[XMLConstants.VISIBLE]);
             cbOrbit.setEnabled(XMLConstants.MODIFIABLE_VALUES_AT_RUNTIME[XMLConstants.ORBIT]);
             cbCoordSys.setEnabled(XMLConstants.MODIFIABLE_VALUES_AT_RUNTIME[XMLConstants.COORDINATESYSTEM]);
             colorPanel.setEnabled(XMLConstants.MODIFIABLE_VALUES_AT_RUNTIME[XMLConstants.COLORORBIT]);
             break;
        default : break;
      }
    } else  {
        cbVisible.setEnabled(XMLConstants.MODIFIABLE_VALUES_AT_RUNTIME[XMLConstants.VISIBLE]);
        cbOrbit.setEnabled(XMLConstants.MODIFIABLE_VALUES_AT_RUNTIME[XMLConstants.ORBIT]);
        cbCoordSys.setEnabled(XMLConstants.MODIFIABLE_VALUES_AT_RUNTIME[XMLConstants.COORDINATESYSTEM]);
        colorPanel.setEnabled(XMLConstants.MODIFIABLE_VALUES_AT_RUNTIME[XMLConstants.COLORORBIT]);
    }

    this.setVisible(true); 
  }

  /**
   * Implementation of action listener
   *
   * @param e java.awt.event.ActionEvent
   */
  public void actionPerformed(java.awt.event.ActionEvent e) {
    if (e.getActionCommand().equals("update")) {
       // check if the user changed the values
       // if so we have to update the initialization object
       if ( cbVisible.isSelected() != data.isVisible() ) {
         if ( iniPanel != null ) {
           if ( iniPanel.getMode() == SSS3dConstants.INITIALIZATION_MODE ||
                ( iniPanel.getMode() == SSS3dConstants.RUNTIME_MODE && 
                  XMLConstants.MODIFIABLE_VALUES_AT_RUNTIME[XMLConstants.VISIBLE]) ) {   
                  
                  iniPanel.getObjectsInformation().setParameter(new Boolean(cbVisible.isSelected()),XMLConstants.VISIBLE,data.getName());         
           }
         } else  {
           if( XMLConstants.MODIFIABLE_VALUES_AT_RUNTIME[XMLConstants.VISIBLE] )
               iniPanel.getObjectsInformation().setParameter(new Boolean(cbVisible.isSelected()),XMLConstants.VISIBLE,data.getName());         
         }
       }
       
       if ( cbCoordSys.isSelected() != data.hasCoordinateSystem() ) {
         if ( iniPanel != null ) {
           if ( iniPanel.getMode() == SSS3dConstants.INITIALIZATION_MODE ||
                ( iniPanel.getMode() == SSS3dConstants.RUNTIME_MODE && 
                  XMLConstants.MODIFIABLE_VALUES_AT_RUNTIME[XMLConstants.COORDINATESYSTEM]) ) {   
                  
                  iniPanel.getObjectsInformation().setParameter(new Boolean(cbCoordSys.isSelected()),XMLConstants.COORDINATESYSTEM,data.getName());
           }
         } else  {
           if( XMLConstants.MODIFIABLE_VALUES_AT_RUNTIME[XMLConstants.COORDINATESYSTEM] )
               iniPanel.getObjectsInformation().setParameter(new Boolean(cbCoordSys.isSelected()),XMLConstants.COORDINATESYSTEM,data.getName());
         }
                
         
       }
       
       if ( cbOrbit.isSelected() != data.hasOrbit() ) {
         if ( iniPanel != null ) {
           if ( iniPanel.getMode() == SSS3dConstants.INITIALIZATION_MODE ||
                ( iniPanel.getMode() == SSS3dConstants.RUNTIME_MODE && 
                  XMLConstants.MODIFIABLE_VALUES_AT_RUNTIME[XMLConstants.ORBIT]) ) {   
                  iniPanel.getObjectsInformation().setParameter(new Boolean(cbOrbit.isSelected()),XMLConstants.ORBIT,data.getName());                           
           }
         } else  {
           if( XMLConstants.MODIFIABLE_VALUES_AT_RUNTIME[XMLConstants.ORBIT] )
               iniPanel.getObjectsInformation().setParameter(new Boolean(cbOrbit.isSelected()),XMLConstants.ORBIT,data.getName());
         }
       }
       
       if ( newColor != null ) {
       
         if ( iniPanel != null ) {
           if ( iniPanel.getMode() == SSS3dConstants.INITIALIZATION_MODE ||
                ( iniPanel.getMode() == SSS3dConstants.RUNTIME_MODE && 
                  XMLConstants.MODIFIABLE_VALUES_AT_RUNTIME[XMLConstants.COLORORBIT]) ) {   
                           
                  if ( newColor.getRed()   != data.getColorOrbit().getRed() ||
                       newColor.getGreen() != data.getColorOrbit().getGreen() ||
                       newColor.getBlue()  != data.getColorOrbit().getBlue() ||
                       newColor.getAlpha() != data.getColorOrbit().getAlpha() ) {
                       iniPanel.getObjectsInformation().setParameter( new MyColor(newColor) ,XMLConstants.COLORORBIT,data.getName());
                  }
           
           }
         } else  {
           if( XMLConstants.MODIFIABLE_VALUES_AT_RUNTIME[XMLConstants.COLORORBIT] )
               if ( newColor.getRed()   != data.getColorOrbit().getRed() ||
                    newColor.getGreen() != data.getColorOrbit().getGreen() ||
                    newColor.getBlue()  != data.getColorOrbit().getBlue() ||
                    newColor.getAlpha() != data.getColorOrbit().getAlpha() ) {
                    iniPanel.getObjectsInformation().setParameter( new MyColor(newColor) ,XMLConstants.COLORORBIT,data.getName());
               }
         }       
       

       }
    }
  }
  
}