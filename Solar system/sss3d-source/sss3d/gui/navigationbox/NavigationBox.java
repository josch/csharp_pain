 /* 
  File: NavigationBox.java 

  University of Applied Science Berne,HTA-Biel/Bienne,
  Computer Science Department.

  Diploma thesis J3D Solar System Simulator
  Originally written by Marcel Portner & Bernhard Hari (c) 2000
  
  CVS - Information :
  
  $Header: /var/cvsreps/projects/c450/2000/sss3d/source_diploma/sss3d/gui/navigationbox/NavigationBox.java,v 1.4 2000/12/13 13:39:46 portm Exp $
  $Author: portm $
  $Date: 2000/12/13 13:39:46 $
  $State: Exp $
*/

package sss3d.gui.navigationbox;

import sss3d.utils.observer.*;
import sss3d.utils.xmlparser.*;
import sss3d.contentbranch.*;
import sss3d.gui.*;
import sss3d.SolarSystemSimulator;
import sss3d.utils.SSS3dConstants;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;

/**
 * This class is used to display a navigation box at the right side
 * of the solar system simulator. It contains a date panel to modify
 * the current date of the simulator.
 *
 * @author  Marcel Portner & Bernhard Hari
 * @version $Revision: 1.4 $
 * @see     sss3d.gui.DatePanel
 */
public class NavigationBox extends JPanel implements Runnable, InfoObserver, ActionListener {

    private Thread               thread;                  // navigationbox runs in a own thread
    private ObjectsInformation   objInfo;
    
    //GUI
    private DatePanel            datePanel;
    
    private SolarSystemSimulator sss;       // link to control simulation
    
   /**
    * Constructs an NavigationBox.
    *
    * @param sss a reference to the solar system simulator 
    */
    public NavigationBox( SolarSystemSimulator sss ) {
      this.sss = sss;
      
      objInfo = sss.getObjectsInformation();
      objInfo.attach(this);
      
      datePanel = new DatePanel(objInfo);
      
      thread = new Thread(this, "NavigationBox");
      thread.start();
    }
    
    
    /**
     * Run method creates JPanel.
     */
    public void run() {
      setLayout( new BoxLayout( this, BoxLayout.Y_AXIS ) );
      this.setAlignmentX(0.5f);
      JLabel nav = new JLabel("NAVIGATION",JLabel.CENTER);
      this.add(nav);
      this.add(datePanel);
      this.setVisible(true);
    }  
    
    
    /**
     * Updates the information state of the object.<br>
     *
     * @param id identification of the object that has to be updated
     * @param parameter the parameter that has been changed
     */
    public void update( String id, int parameter ) {
    
       if ( id.equals( getId() ) ||
           id.equals("all") ) {
          
           switch ( parameter ) {
              case XMLConstants.DAY :
              case XMLConstants.MONTH :
              case XMLConstants.YEAR :
              case XMLConstants.HOUR :
              case XMLConstants.MINUTES :
              case XMLConstants.SECONDS :
              case XMLConstants.JDAY :
                   datePanel.update();
                   break;
              default : break;
           
           }
       }
 
    }

   /**
    * Returns the ID of the current rocket.
    *
    * @return the ID to idendification the rocket.
    */
   public String getId() {
      return "navigationbox";
   } 
   
   
   /**
    * Not used. 
    * @return null
    */
   public CelestialObjectInfo getInfo() {
      return null;
   }
   
        
   /**
    * Method handles all action events.
    */
    public void actionPerformed(ActionEvent e) {
         String s = e.getActionCommand();
         if(s.equals("exit")) {
            System.exit(0);
         }
    }

}