/*
  File: InitializationPanel.java

  University of Applied Science Berne,HTA-Biel/Bienne,
  Computer Science Department.

  Diploma thesis J3D Solar System Simulator
  Originally written by Marcel Portner & Bernhard Hari (c) 2000

  CVS - Information :

  $Header: /var/cvsreps/projects/c450/2000/sss3d/source_diploma/sss3d/gui/InitializationPanel.java,v 1.12 2000/12/13 14:03:18 portm Exp $
  $Author: portm $
  $Date: 2000/12/13 14:03:18 $
  $State: Exp $

*/
package sss3d.gui;

import sss3d.contentbranch.CelestialObjectInfo;

import sss3d.utils.xmlparser.*;
import sss3d.gui.*;
import sss3d.gui.infobox.*;
import sss3d.contentbranch.*;
import sss3d.utils.SSS3dConstants;
import sss3d.utils.observer.*;

// awt / swing components
import java.awt.*;
import javax.swing.*;
import javax.swing.tree.*;

// tree view
import org.w3c.dom.Document;
import org.w3c.dom.DOMException;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import java.awt.event.WindowEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.ActionListener;

import java.util.*;
import javax.swing.event.*;
import org.w3c.dom.*;

/**
 * Shows the current settings of the J3D - Solar System Simulator.
 *   
 * @author Marcel Portner & Bernhard Hari
 * @version $Revision: 1.12 $
 * @see sss3d.gui.startbox.StartBox
 * @see sss3d.gui.infobox.InfoBox
 */
public class InitializationPanel extends JPanel implements GlobalPanel, InfoObserver{

  // GUI components tree view
  private JScrollPane  treeView;
  private JSplitPane   jSplitPane;
  private JTree        jTreeDocument;
  private Document     document;
  private JPanel       editView = null;
  
  // references the current selected node inside the tree view 
  private AdapterNode         currentNode;  
  
  // references the type of this panel
  private String        type = SSS3dConstants.TYPES[SSS3dConstants.INI_TYPE];

  private InitializationObject iniObject;  
  private ObjectsInformation objInfo;
  
  // Current mode of Solar System Simulator
  private int mode = SSS3dConstants.INITIALIZATION_MODE;
    
  /**
   * Constructor an InitializationPanel attached to the 
   * objects information observer.
   *
   * @param iniObject a reference to an InitializationObject instance
   * @param mode must be either SSS3dConstants.INITIALIZATION_MODE or
   *             SSS3dConstants.RUNTIME_MODE              
   * @see sss3d.contentbranch.InitializationObject
   */
  public InitializationPanel( ObjectsInformation objInfo, int mode ) {
    
    this.document = (Document)objInfo.getInitializationObject().getParameter(XMLConstants.DOCUMENT);
    this.iniObject = objInfo.getInitializationObject();
    this.objInfo = objInfo;
    this.mode = mode;  
    this.objInfo.attach(this);
    
    try {
	  // build GUI
      init();
    }
    catch(Exception e) {
      e.printStackTrace();
    }

  }
  
  /**
   * builds the basic GUI, called by constructor
   */
  public void init() throws Exception {
  	
	  setLayout(new BorderLayout());
		
    if( document != null ) createTreeView();
	  
	  editView = new JPanel();
      editView.setSize(350,500);
	  jSplitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,treeView, editView);
	  jSplitPane.setVisible(true);
	  jSplitPane.setContinuousLayout( true );
      jSplitPane.setDividerLocation( 330 );
    			
	  this.add(jSplitPane,BorderLayout.CENTER);
	  this.setVisible(true);
  }
  
  /**
   * Returns the type of this panel.
   *
   * @param String the type representing this object ( SSS3dConstants.INI_TYPE )
   * @see sss3d.gui.GlobalPanel
   */
  public String getType() {
    return type;
  }
  
  /**
   * Returns the document of this panel.
   *
   * @return Document the document containing the information
   * @see sss3d.gui.GlobalPanel
   */  
  public Document getDocument() {
    return document;
  }

  /**
   * Returns the reference to the ObjectInformation object.
   *
   * @return ObjectsInformation a reference to the ObjectsInformation object
   */  
  public ObjectsInformation getObjectsInformation() {
    return objInfo;
  }
  
  /**
   * Returns the mode
   * 
   * @return int SSS3dConstants.INITIALIZATION_MODE or SSS3dConstants.RUNTIME_MODE
   * @see sss3d.utils.SSS3dConstants
   */  
  public int getMode() {
    return mode;
  }
  
  /**
   * Creates the edit view.
   */
  public void createEditView() {
    if ( editView != null) {
      remove(editView);
      editView.setVisible(false);
      editView = null;
    }
    
    String nodeName = currentNode.domNode.getNodeName();
    
    if ( nodeName.equals(XMLConstants.treeElementNames[XMLConstants.STAR]) ||
         nodeName.equals(XMLConstants.treeElementNames[XMLConstants.PLANET]) ||
         nodeName.equals(XMLConstants.treeElementNames[XMLConstants.MOON]) ||
         nodeName.equals(XMLConstants.treeElementNames[XMLConstants.COMET]) ||
         nodeName.equals(XMLConstants.treeElementNames[XMLConstants.SATELLITE]) ||
         nodeName.equals(XMLConstants.treeElementNames[XMLConstants.ROCKET]) ) {
         
           
           NamedNodeMap nnm = currentNode.domNode.getAttributes();
           Node attribute = nnm.getNamedItem(XMLConstants.treeElementNames[XMLConstants.OBJECTNAME]);        
           
           int parameter = -1;
           if (nodeName.equals(XMLConstants.treeElementNames[XMLConstants.STAR])) {
             parameter = XMLConstants.STAR;
           } else if (nodeName.equals(XMLConstants.treeElementNames[XMLConstants.PLANET])) {
             parameter = XMLConstants.PLANET;
           } else if (nodeName.equals(XMLConstants.treeElementNames[XMLConstants.MOON])) {
             parameter = XMLConstants.MOON;
           } else if (nodeName.equals(XMLConstants.treeElementNames[XMLConstants.COMET])) {
             parameter = XMLConstants.COMET;
           } else if (nodeName.equals(XMLConstants.treeElementNames[XMLConstants.SATELLITE])) {
             parameter = XMLConstants.SATELLITE;
           } else if (nodeName.equals(XMLConstants.treeElementNames[XMLConstants.ROCKET])) {
             parameter = XMLConstants.ROCKET;
           }
           
           editView = new CelestialObjectPanel(this, parameter, attribute.getNodeValue());

    } else if( nodeName.equals(XMLConstants.treeElementNames[XMLConstants.DAY] ) ||
               nodeName.equals(XMLConstants.treeElementNames[XMLConstants.MONTH] ) ||
               nodeName.equals(XMLConstants.treeElementNames[XMLConstants.YEAR] ) ||
               nodeName.equals(XMLConstants.treeElementNames[XMLConstants.HOUR] ) ||
               nodeName.equals(XMLConstants.treeElementNames[XMLConstants.MINUTES] ) ||
               nodeName.equals(XMLConstants.treeElementNames[XMLConstants.SECONDS] ) ||
               nodeName.equals(XMLConstants.treeElementNames[XMLConstants.JDAY] ) 
    ) {
           editView = new DatePanel(this);

    } else if( nodeName.equals(XMLConstants.treeElementNames[XMLConstants.ANIMATIONSPEED] )) {
           editView = new SliderPanel(this, XMLConstants.ANIMATIONSPEED);

    } else if( nodeName.equals(XMLConstants.treeElementNames[XMLConstants.COMPRESSED] )) {
           editView = new BooleanPanel(this,XMLConstants.COMPRESSED);

    } else if( nodeName.equals(XMLConstants.treeElementNames[XMLConstants.GLASSES3D] )) {
           editView = new BooleanPanel(this,XMLConstants.GLASSES3D);

    } else if( nodeName.equals(XMLConstants.treeElementNames[XMLConstants.JOYSTICK] )) {
           editView = new BooleanPanel(this,XMLConstants.JOYSTICK);

    } else if( nodeName.equals(XMLConstants.treeElementNames[XMLConstants.CAMERAATORIGIN] )) {
           editView = new BooleanPanel(this,XMLConstants.CAMERAATORIGIN);

    } else if( nodeName.equals(XMLConstants.treeElementNames[XMLConstants.CAMERA] )) {
           editView = new ComboBoxPanel(this,XMLConstants.CAMERA);

    } else if( nodeName.equals(XMLConstants.treeElementNames[XMLConstants.CALCULATION] )) {
           editView = new ComboBoxPanel(this,XMLConstants.CALCULATION);

    } else  {
      editView = new JPanel();
      editView.setSize(350,500);
    }

    if ( editView != null ) {
         editView.setVisible(true);
         
         jSplitPane.setRightComponent(editView);    
         jSplitPane.setDividerLocation(330);
    }
    
  }
  
  
  /**
   * Builds the complete tree view , called by init function
   * 
   * @param name the name
   */
  private void createTreeView()
  {
    jTreeDocument = new JTree(new DomToTreeModelAdapter(document));
    jTreeDocument.putClientProperty("JTree.lineStyle", "Angled");
    jTreeDocument.setEditable(true);
    jTreeDocument.getSelectionModel().setSelectionMode
             (TreeSelectionModel.SINGLE_TREE_SELECTION);
    jTreeDocument.setShowsRootHandles(true);
    MyRenderer renderer = new MyRenderer(this);
    jTreeDocument.setCellRenderer(renderer);              
   
    // build tree view
    treeView = new JScrollPane(jTreeDocument);
    treeView.setVisible(true);

   	   
    // Wire the two views together. Use a selection listener 
    // created with an anonymous inner-class adapter.
    jTreeDocument.addTreeSelectionListener(
      new TreeSelectionListener() {
        public void valueChanged(TreeSelectionEvent e) {
          TreePath p = e.getNewLeadSelectionPath();
          if (p != null) {
            currentNode =
            (AdapterNode) p.getLastPathComponent();
        	                 
            // build the correct view, corresponding to the selected
            // node inside the edit view panel
            createEditView();
          
          }
        }
      }
    );		   
	   
  }
  
  /**
   * Returns the Node of the given parameter.
   *
   * @return parameter XMLConstants.parameter
   */  
  private Node getNode( int parameter ) {
    Node node = null;
    
    NodeList nodes = document.getElementsByTagName(XMLConstants.treeElementNames[parameter]);
    
    for(int i = 0; i < nodes.getLength(); i++) {
    
       Node child = nodes.item(i);
       if ( child.hasChildNodes() ) {
          node = child.getFirstChild();
       }
    }
    return node;
  }


  /**
   * Updates the TreeView panel after the user has changed the values.
   *
   * @param node node that needs to be updated
   */
  public void updateTreeViewInfo( Object object, int parameter ) {
    

    Node node = getNode(parameter);


      switch ( parameter ){
        case XMLConstants.DAY :
        case XMLConstants.MONTH :
        case XMLConstants.YEAR :
        case XMLConstants.HOUR :
        case XMLConstants.MINUTES :
        case XMLConstants.SECONDS :
             node.setNodeValue( ((Integer)object).toString() );
             break;
        case XMLConstants.JDAY :        
             node.setNodeValue( ((Double)object).toString() );
             break;
        case XMLConstants.CALCULATION :
             node.setNodeValue( SSS3dConstants.CALCULATION_METHODS[((Integer)object).intValue()]);
             break;
        case XMLConstants.CAMERA :
             node.setNodeValue(SSS3dConstants.CAMERA_POSITIONS[((Integer)object).intValue()]);
             break;
        case XMLConstants.ANIMATIONSPEED :{
               NodeList nodes = document.getElementsByTagName(
                        XMLConstants.treeElementNames[XMLConstants.ANIMATIONSPEED]);
               for(int i = 0; i < nodes.getLength(); i++) {
                   Node child = nodes.item(i);
                   NamedNodeMap nnm = child.getAttributes();
                   Node type  = nnm.getNamedItem(XMLConstants.treeElementNames[XMLConstants.TYPE]);
                   Node value = nnm.getNamedItem(XMLConstants.treeElementNames[XMLConstants.VALUE]);
                   type.setNodeValue( AnimationSpeed.NAMES[((AnimationSpeed)object).getType()].toLowerCase() );
                   value.setNodeValue( Integer.toString(((AnimationSpeed)object).getValue()) );
               }
             }
             break;
        case XMLConstants.COMPRESSED :
        case XMLConstants.GLASSES3D :
        case XMLConstants.JOYSTICK :
        case XMLConstants.CAMERAATORIGIN :
             node.setNodeValue(((Boolean)object).toString());
             break;
                                                                 
                  
        default : break;
      } 

    
    // update document view
    jTreeDocument.expandPath(jTreeDocument.getSelectionPath());
    jTreeDocument.repaint();
  }
 

    /**
     * Returns the id of the concrete observer.
     *
     * @return    String the id of the concrete observer
     */     
    public String getId() {
      return "initializationpanel";
    }
 
 
    /**
     * The update methode is used to keep the state of the concrete observer
     * consistent with the state of the ObjectInfo.<br>
     *
     * @param     objInfo a reference to the ObjectInfo
     * @param     id the id of the concrete object that has to be updated
     */ 	
    public void update( String id, int parameter) {
        if ( id.equals("initializationpanel") ||
             id.equals("all") && parameter != SSS3dConstants.RELOAD ) {
             //System.out.println("update initializationpanel "+parameter );
             updateTreeViewInfo( objInfo.getParameter(parameter), parameter );
        }
    }
    

     
    /**
     * The method isn't used at the moment, but it is necessary
     * to implement it -> observer pattern.
     *
     * @return null
     */     
    public CelestialObjectInfo getInfo() {
      return null;
    }
}