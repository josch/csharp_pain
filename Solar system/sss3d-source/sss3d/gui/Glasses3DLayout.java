/*
  File: Glasses3DLayout.java

  University of Applied Science Berne,HTA-Biel/Bienne,
  Computer Science Department.

  Diploma thesis J3D Solar System Simulator
  Originally written by Marcel Portner & Bernhard Hari (c) 2000
  
  CVS - Information :
  
  $Header: /var/cvsreps/projects/c450/2000/sss3d/source_diploma/sss3d/gui/Glasses3DLayout.java,v 1.5 2000/12/13 13:47:25 portm Exp $
  $Author: portm $
  $Date: 2000/12/13 13:47:25 $
  $State: Exp $
  
*/
package sss3d.gui;

import java.awt.*;
import java.io.Serializable;
import java.util.Hashtable;

/**
 * The XYLayout component is a custom layout manager. It places
 * components in a container at specific x and y coordinates relative to
 * the upper-left corner of the container. Regardless of the type of display,
 * the container always retains the relative x and y positions of the components.
 * Resizing a container with an XYLayout, however, does not resize or reposition
 * the components within the container.
 *
 * @author Marcel Portner & Bernhard Hari
 * @version $Revision: 1.5 $
 */
public class Glasses3DLayout implements LayoutManager2, Serializable {

   private static final long serialVersionUID = 200L;
   private int width;
   private int height;
   private Hashtable info;
   private static final XYConstraints defaultConstraints = new XYConstraints();
   private XYConstraints leftEyeConstraints;
   private XYConstraints rightEyeConstraints;

   /**
    * Creates an XYLayout component with the default screensize
    * of 640 x 480.
    */ 
   public Glasses3DLayout() {
      this(640, 480);
   }

   /**
    * Creates an XYLayout component using the width and height
    * specified with the width and height parameters.
    *
    * @param width  The width of the layout manager in pixels. A value less than or
    *               equal to 0 means that the container's preferred size is used.
    * @param height The height of the layout manager in pixels. A value less than
    *               or equal to 0 means that the container's preferred size is used.
    */
   public Glasses3DLayout(int width, int height) {
      info = new Hashtable();
      this.width = width;
      this.height = width;
      int offsethorizontal = 220; //227?
      int offsetvertical = (int)(height * 0.05);
      int w = width - offsethorizontal;
      int h = (height - offsetvertical) / 2;

      leftEyeConstraints = new XYConstraints(offsethorizontal, 0, w, h);
      rightEyeConstraints = new XYConstraints(0, h + offsetvertical, w, h);
   }

   /**
    * Returns the currently width of this component.
    * 
    * @returns int the width
    */
   public int getWidth() {
      return width;
   }

   /**
    * The width of the XYLayout component in pixels. A value less than or
    * equal to 0 means that the container's preferred width is used. 
    *
    * @param width the new width
    */
   public void setWidth(int width) {
      this.width = width;
   }

   /**
    * Returns the currently height of this component.
    * 
    * @returns int the height
    */
   public int getHeight() {
      return height;
   }

   /**
    * The height of the XYLayout object in pixels. A value less than
    * or equal to 0 means that the container's preferred height is used. 
    *
    * @param height the new height
    */
   public void setHeight(int height) {
      this.height = height;
   }


   /**
    * Returns the left panel.
    *
    * @returns Object the left panel
    */
   public Object getLeftPanel() {
      return leftEyeConstraints;
   }

   /**
    * Returns the right panel.
    *
    * @returns Object the right panel
    */
   public Object getRightPanel() {
      return rightEyeConstraints;
   }

   /**
    * Returns a string that displays the width and height values. 
    *
    * @return String a string representation of this object
    */ 
   public String toString() {
      return "XYLayout[width=" + width + ",height=" + height + "]";
   }

   /**
    * Adds the specified component to the layout.
    *
    * @param s a string
    * @param component the component to be added
    */
   public void addLayoutComponent(String s, Component component) {
   }

   /**
    * Removes the specified component from the layout.
    *
    * @param component the component to be removed
    */
   public void removeLayoutComponent(Component component) {
      info.remove(component);
   }

   /**
    * Returns the dimension of this container.
    *
    * @returns Dimension the size of this container
    */
   public Dimension preferredLayoutSize(Container container) {
      return getLayoutSize(container, true);
   }

   /**
    * Returns the minimum dimension of this container.
    *
    * @returns Dimension the minimum size of this container
    */   
   public Dimension minimumLayoutSize(Container container) {
      return getLayoutSize(container, false);
   }

   /**
    * Lays out the container in the specified panel.
    *
    * @param parent the component which needs to be laid out
    */
   public void layoutContainer(Container container) {
      Insets insets = container.getInsets();
      int i = container.getComponentCount();
      for(int j = 0; j < i; j++) {
         Component component = container.getComponent(j);
         if(component.isVisible()) {
            Rectangle rectangle = getComponentBounds(component, true);
            component.setBounds(insets.left + rectangle.x,
                                insets.top + rectangle.y,
                                rectangle.width,
                                rectangle.height);
         }
      }
   }

   /**
    * Adds the specified component to the layout.
    *
    * @param component the component to be added
    * @param obj an instance of a XYConstraints object 
    */
   public void addLayoutComponent(Component component, Object obj) {
      if(obj instanceof XYConstraints) {
         info.put(component, obj);
      }
   }

   /**
    * Returns the maximum size of this component.
    *
    * @returns Dimension the maximum size of this component.
    */
   public Dimension maximumLayoutSize(Container container) {
      return new Dimension(0x7fffffff, 0x7fffffff);
   }

   /**
    * Returns the alignment along the x axis. This specifies how the component would
    * like to be aligned relative to other components. The value should be a number
    * between 0 and 1 where 0 represents alignment along the origin, 1 is aligned the
    * furthest away from the origin, 0.5 is centered, etc.
    *
    * @param float the horizontal alignment
    */
   public float getLayoutAlignmentX(Container container) {
      return 0.5F;
   }
   
   /**
    * Returns the alignment along the y axis.
    * This specifies how the component would like to be aligned relative to other
    * components. The value should be a number between 0 and 1 where 0 represents alignment
    * along the origin, 1 is aligned the furthest away from the origin, 0.5 is centered, etc.
    *
    * @returns float the horizontal alignment
    */   
   public float getLayoutAlignmentY(Container container) {
      return 0.5F;
   }

   /**
    * Invalidates the layout, indicating that if the layout manager has cached
    * information it should be discarded.
    *
    * @param container the target 
    */
   public void invalidateLayout(Container container) {
   }

   
   /**
    * Returns the bounds of the specified XYConstraints component.
    *
    * @param component an instance of XYConstraints
    * @param flag true if preferred size of component is used
    *             false if minimum size of component is used
    * @return Rectangle the bounds of this components
    */            
   private Rectangle getComponentBounds(Component component, boolean flag) {
      XYConstraints xyconstraints = (XYConstraints)info.get(component);
      if(xyconstraints == null) {
         xyconstraints = defaultConstraints;
      }
      Rectangle rectangle = new Rectangle(xyconstraints.getX(),
                                          xyconstraints.getY(),
                                          xyconstraints.getWidth(),
                                          xyconstraints.getHeight());
      if(rectangle.width <= 0 || rectangle.height <= 0) {
         Dimension dimension = flag ? component.getPreferredSize() : component.getMinimumSize();
         if(rectangle.width <= 0) {
            rectangle.width = dimension.width;
         }
         if(rectangle.height <= 0) {
            rectangle.height = dimension.height;
         }
      }
      return rectangle;
   }

   /**
    * Returns the size of the layout.
    *
    * @param container the container
    * @param flag true if preferred size of container is used
    *             false if minimum size of container is used
    * @returns Dimension the size of the given container
    */
   private Dimension getLayoutSize(Container container, boolean flag) {
      Dimension dimension = new Dimension(0, 0);
      if(width <= 0 || height <= 0) {
         int i = container.getComponentCount();
         for(int j = 0; j < i; j++) {
            Component component = container.getComponent(j);
            if(component.isVisible()) {
               Rectangle rectangle = getComponentBounds(component, flag);
               dimension.width = Math.max(dimension.width, rectangle.x + rectangle.width);
               dimension.height = Math.max(dimension.height, rectangle.y + rectangle.height);
            }
         }
      }
      if(width > 0) {
         dimension.width = width;
      }
      if(height > 0) {
         dimension.height = height;
      }
      Insets insets = container.getInsets();
      dimension.width += insets.left + insets.right;
      dimension.height += insets.top + insets.bottom;
      return dimension;
   }
}
