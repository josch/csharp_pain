/*
  File: SliderPanel.java

  University of Applied Science Berne,HTA-Biel/Bienne,
  Computer Science Department.

  Diploma thesis J3D Solar System Simulator
  Originally written by Marcel Portner & Bernhard Hari (c) 2000

  CVS - Information :

  $Header: /var/cvsreps/projects/c450/2000/sss3d/source_diploma/sss3d/gui/SliderPanel.java,v 1.5 2000/12/12 12:01:00 harib Exp $
  $Author: harib $
  $Date: 2000/12/12 12:01:00 $
  $State: Exp $

*/

package sss3d.gui;

import sss3d.gui.*;
import sss3d.contentbranch.*;
import sss3d.utils.xmlparser.*;
import sss3d.utils.SSS3dConstants;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.event.*;

/**
 * Creates a panel for adjustment with a slider
 *   
 * @author Marcel Portner & Bernhard Hari
 * @version $Revision: 1.5 $
 */
public class SliderPanel extends JPanel implements ActionListener, ChangeListener{

  private InitializationPanel  iniPanel;
  private int                  parameter      = -1;
  private AnimationSpeed       animationSpeed;
  private JSlider              slider;
  private JButton              button;
  private JLabel               currentValue;
  private JRadioButton         days;
  private JRadioButton         hours;
  private JRadioButton         minutes;
  
  /**
   * Creates a SliderPanel depending on the given parameter
   *
   * @param iniPanel a reference to this Initialization ( it can be null )
   * @param parameter parameter witch needs to be adjusted
   * @see javax.swing.JSlider
   */
  public SliderPanel( InitializationPanel iniPanel, int parameter ) {
    super();
    
    this.iniPanel  = iniPanel;   
    this.parameter = parameter;
  
    // create GUI
    setLayout( new BoxLayout( this, BoxLayout.Y_AXIS ) );
    
    // create GUI for AnimationSpeed
    if ( parameter == XMLConstants.ANIMATIONSPEED ) {
       animationSpeed = (AnimationSpeed)iniPanel.getObjectsInformation().getParameter(XMLConstants.ANIMATIONSPEED);
       if (animationSpeed != null) {
       
          JLabel title = new JLabel(XMLConstants.treeElementNames[parameter]);
          title.setAlignmentX(LEFT_ALIGNMENT);
          add( title );             
          add(Box.createRigidArea(new Dimension(0, 10)));
          ButtonGroup bg = new ButtonGroup();
          days    = new JRadioButton(AnimationSpeed.NAMES[AnimationSpeed.DAYS_PER_SECOND]);    
          hours   = new JRadioButton(AnimationSpeed.NAMES[AnimationSpeed.HOURS_PER_SECOND]);
          minutes = new JRadioButton(AnimationSpeed.NAMES[AnimationSpeed.MINUTES_PER_SECOND]);
          bg.add(days);
          bg.add(hours);
          bg.add(minutes);
          days.setActionCommand(AnimationSpeed.NAMES[AnimationSpeed.DAYS_PER_SECOND]);
          hours.setActionCommand(AnimationSpeed.NAMES[AnimationSpeed.HOURS_PER_SECOND]);
          minutes.setActionCommand(AnimationSpeed.NAMES[AnimationSpeed.MINUTES_PER_SECOND]);
          days.addActionListener(this);
          hours.addActionListener(this);
          minutes.addActionListener(this);
          days.setAlignmentX(LEFT_ALIGNMENT);
          hours.setAlignmentX(LEFT_ALIGNMENT);
          minutes.setAlignmentX(LEFT_ALIGNMENT);
          add(days);
          add(hours);
          add(minutes);
          
          int min = 1;
          int max = 10;

          
          switch ( animationSpeed.getType() ) {
            case AnimationSpeed.DAYS_PER_SECOND :
                 min = AnimationSpeed.DAYS_MIN;
                 max = AnimationSpeed.DAYS_MAX;
                 days.setSelected(true);
                 break;
            case AnimationSpeed.HOURS_PER_SECOND :
                 min = AnimationSpeed.HOURS_MIN;
                 max = AnimationSpeed.HOURS_MAX;            
                 hours.setSelected(true);
                 break;
            case AnimationSpeed.MINUTES_PER_SECOND :
                 min = AnimationSpeed.MINUTES_MIN;
                 max = AnimationSpeed.MINUTES_MAX;            
                 minutes.setSelected(true);            
                 break;
            default : days.setSelected(true); break;
          }
          
          slider = new JSlider(JSlider.HORIZONTAL, min, max, animationSpeed.getValue());
          slider.setMinorTickSpacing(1);
          slider.setLabelTable( getHashTable( animationSpeed.getType() ) );
          slider.setPaintLabels(true);
          slider.setPaintTicks(true);
          slider.setSnapToTicks(true);
          slider.setBorder(BorderFactory.createEmptyBorder(10,0,10,0));
          slider.setAlignmentX(LEFT_ALIGNMENT);
          add(slider);
          
          JPanel value  = new JPanel();
          value.setBorder(BorderFactory.createEmptyBorder(0,0,10,0));
          value.setLayout( new BoxLayout( value, BoxLayout.X_AXIS ) );
          JLabel cValue = new JLabel("Current Value : ");
          cValue.setAlignmentX(LEFT_ALIGNMENT);
          value.add(cValue);
          value.add(Box.createRigidArea(new Dimension(20, 0)));
          currentValue = new JLabel(Integer.toString(animationSpeed.getValue()));
          currentValue.setAlignmentX(RIGHT_ALIGNMENT);
          value.add(currentValue);
          value.setAlignmentX(LEFT_ALIGNMENT);
          add(value);
          
          button = new JButton("update");
          button.setActionCommand("update");
          button.addActionListener(this);
          slider.addChangeListener(this);
          add(button);
          
          // enable/disable components depending on they can be changed during
          // runtime mode 
          if ( iniPanel != null ) {
            switch ( iniPanel.getMode() ) {
              case SSS3dConstants.RUNTIME_MODE :
                   days.setEnabled(XMLConstants.MODIFIABLE_VALUES_AT_RUNTIME[XMLConstants.ANIMATIONSPEED]);
                   hours.setEnabled(XMLConstants.MODIFIABLE_VALUES_AT_RUNTIME[XMLConstants.ANIMATIONSPEED]);
                   minutes.setEnabled(XMLConstants.MODIFIABLE_VALUES_AT_RUNTIME[XMLConstants.ANIMATIONSPEED]);
                   slider.setEnabled(XMLConstants.MODIFIABLE_VALUES_AT_RUNTIME[XMLConstants.ANIMATIONSPEED]);
                   break;
              default : break;
            }
          } else {
              days.setEnabled(XMLConstants.MODIFIABLE_VALUES_AT_RUNTIME[XMLConstants.ANIMATIONSPEED]);
              hours.setEnabled(XMLConstants.MODIFIABLE_VALUES_AT_RUNTIME[XMLConstants.ANIMATIONSPEED]);
              minutes.setEnabled(XMLConstants.MODIFIABLE_VALUES_AT_RUNTIME[XMLConstants.ANIMATIONSPEED]);
              slider.setEnabled(XMLConstants.MODIFIABLE_VALUES_AT_RUNTIME[XMLConstants.ANIMATIONSPEED]);          
          }
       }
    }

   
    setBorder(BorderFactory.createEmptyBorder(10,10,10,10));
 

    this.setAlignmentX(LEFT_ALIGNMENT);
    this.setVisible(true);   

  }
  
  
  /**
   * Returns a Hashtable depending on the selected type,
   * containing the labels for the JSlider
   *
   * @param type the selected type
   * @return Hashtable a Hashtable containing the labels for the JSlider
   * @see javax.swing.JSlider
   */
  private Hashtable getHashTable( int type ) {
     Hashtable labelTable = new Hashtable();
     
     switch ( type )   {    
       case AnimationSpeed.DAYS_PER_SECOND :
            for( int i = AnimationSpeed.DAYS_MIN ; i <= AnimationSpeed.DAYS_MAX; i++) {
              if ( i % 1 == 0 || i == AnimationSpeed.DAYS_MIN || i == AnimationSpeed.DAYS_MAX ) {
                labelTable.put( new Integer( i ), new JLabel( Integer.toString(i) ) );
              }
            }
            break;
       case AnimationSpeed.HOURS_PER_SECOND :
            for( int i = AnimationSpeed.HOURS_MIN ; i <= AnimationSpeed.HOURS_MAX; i++) {
              if ( i % 4 == 0 || i == AnimationSpeed.HOURS_MIN || i == AnimationSpeed.HOURS_MAX ) {
                labelTable.put( new Integer( i ), new JLabel( Integer.toString(i) ) );
              }
            }
            break;
       case AnimationSpeed.MINUTES_PER_SECOND :
            for( int i = AnimationSpeed.MINUTES_MIN ; i <= AnimationSpeed.MINUTES_MAX; i++) {
              if ( i % 5 == 0 || i == AnimationSpeed.MINUTES_MIN || i == AnimationSpeed.MINUTES_MAX ) {
                labelTable.put( new Integer( i ), new JLabel( Integer.toString(i) ) );
              }
            }
            break;
       default : break;                                    
     }
     return labelTable;
  }
  
  
  /**
   * Implementation of action listener
   *
   * @param e java.awt.event.ActionEvent
   */
  public void actionPerformed(java.awt.event.ActionEvent e) {
    if (e.getActionCommand().equals("update")) {
        // update AnimationSpeed object
        if ( days.isSelected() ) {
            if ( animationSpeed.setTypeValue( AnimationSpeed.DAYS_PER_SECOND,slider.getValue() ) ) {

              if ( iniPanel != null ) {
                if ( iniPanel.getMode() == SSS3dConstants.INITIALIZATION_MODE ||
                     ( iniPanel.getMode() == SSS3dConstants.RUNTIME_MODE && 
                     XMLConstants.MODIFIABLE_VALUES_AT_RUNTIME[XMLConstants.ANIMATIONSPEED]) ) {
                          
                  iniPanel.getObjectsInformation().setParameter( animationSpeed , XMLConstants.ANIMATIONSPEED );

                  iniPanel.updateTreeViewInfo(animationSpeed, XMLConstants.ANIMATIONSPEED);

                } 
              } else {
                  if( XMLConstants.MODIFIABLE_VALUES_AT_RUNTIME[XMLConstants.ANIMATIONSPEED] )
                    iniPanel.getObjectsInformation().setParameter( animationSpeed , XMLConstants.ANIMATIONSPEED );
              }     
            }
        } else if ( hours.isSelected() ) {
            if ( animationSpeed.setTypeValue( AnimationSpeed.HOURS_PER_SECOND,slider.getValue() ) ) {

              if ( iniPanel != null ) {
                if ( iniPanel.getMode() == SSS3dConstants.INITIALIZATION_MODE ||
                     ( iniPanel.getMode() == SSS3dConstants.RUNTIME_MODE && 
                     XMLConstants.MODIFIABLE_VALUES_AT_RUNTIME[XMLConstants.ANIMATIONSPEED]) ) {
                          
                  iniPanel.getObjectsInformation().setParameter( animationSpeed , XMLConstants.ANIMATIONSPEED );

                  iniPanel.updateTreeViewInfo(animationSpeed, XMLConstants.ANIMATIONSPEED);

                } 
              } else {
                  if ( XMLConstants.MODIFIABLE_VALUES_AT_RUNTIME[XMLConstants.ANIMATIONSPEED] )
                    iniPanel.getObjectsInformation().setParameter( animationSpeed , XMLConstants.ANIMATIONSPEED );
              }     
            }
        } else if ( minutes.isSelected() ) {
            if ( animationSpeed.setTypeValue( AnimationSpeed.MINUTES_PER_SECOND,slider.getValue() ) ) {

              if ( iniPanel != null ) {
                if ( iniPanel.getMode() == SSS3dConstants.INITIALIZATION_MODE ||
                     ( iniPanel.getMode() == SSS3dConstants.RUNTIME_MODE && 
                     XMLConstants.MODIFIABLE_VALUES_AT_RUNTIME[XMLConstants.ANIMATIONSPEED]) ) {
                          
                  iniPanel.getObjectsInformation().setParameter( animationSpeed , XMLConstants.ANIMATIONSPEED );

                  iniPanel.updateTreeViewInfo(animationSpeed, XMLConstants.ANIMATIONSPEED);

                } 
              } else {
                  if ( XMLConstants.MODIFIABLE_VALUES_AT_RUNTIME[XMLConstants.ANIMATIONSPEED] ) 
                    iniPanel.getObjectsInformation().setParameter( animationSpeed , XMLConstants.ANIMATIONSPEED );
              }     
            }                                       

        }
        slider.requestFocus();
    
    // set appereance of jslider object if user selected an other radio button    
    } else if (e.getActionCommand().equals(AnimationSpeed.NAMES[AnimationSpeed.DAYS_PER_SECOND])) {
        slider.setMinimum(AnimationSpeed.DAYS_MIN);
        slider.setMaximum(AnimationSpeed.DAYS_MAX);
        slider.setLabelTable( getHashTable(AnimationSpeed.DAYS_PER_SECOND) );
        slider.setPaintLabels(true);
        slider.setPaintTicks(true);
        slider.requestFocus();
        slider.updateUI();

    } else if (e.getActionCommand().equals(AnimationSpeed.NAMES[AnimationSpeed.HOURS_PER_SECOND])) {
        slider.setMinimum(AnimationSpeed.HOURS_MIN);
        slider.setMaximum(AnimationSpeed.HOURS_MAX);
        slider.setLabelTable( getHashTable(AnimationSpeed.HOURS_PER_SECOND) );
        slider.setPaintLabels(true);            
        slider.setPaintTicks(true);
        slider.requestFocus();
        slider.updateUI();

    } else if (e.getActionCommand().equals(AnimationSpeed.NAMES[AnimationSpeed.MINUTES_PER_SECOND])) {
        slider.setMinimum(AnimationSpeed.MINUTES_MIN);
        slider.setMaximum(AnimationSpeed.MINUTES_MAX);
        slider.setLabelTable( getHashTable(AnimationSpeed.MINUTES_PER_SECOND) );
        slider.setPaintLabels(true);            
        slider.setPaintTicks(true);
        slider.requestFocus();
        slider.updateUI();
    }
  }

  /**
   * Implementation of change listener. 
   *
   * @param e java.awt.event.ChangeEvent
   */  
  public void stateChanged(ChangeEvent e) {
         JSlider source = (JSlider)e.getSource();
         if (!source.getValueIsAdjusting()) {
             // sets user information
             int value = (int)source.getValue();
             currentValue.setText( Integer.toString(value) );
         }
  }
  
}