/*
  File: BooleanPanel.java

  University of Applied Science Berne,HTA-Biel/Bienne,
  Computer Science Department.

  Diploma thesis J3D Solar System Simulator
  Originally written by Marcel Portner & Bernhard Hari (c) 2000

  CVS - Information :

  $Header: /var/cvsreps/projects/c450/2000/sss3d/source_diploma/sss3d/gui/BooleanPanel.java,v 1.6 2000/12/13 13:47:55 portm Exp $
  $Author: portm $
  $Date: 2000/12/13 13:47:55 $
  $State: Exp $

*/

package sss3d.gui;

import sss3d.gui.*;
import sss3d.contentbranch.*;
import sss3d.utils.xmlparser.XMLConstants;
import sss3d.utils.SSS3dConstants;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.event.*;

/**
 * A simple JPanel with a JComboBox containing the boolean
 * values true and false. Also a title will be displayed depending
 * on the given parameter.
 *   
 * @author Marcel Portner & Bernhard Hari
 * @version $Revision: 1.6 $
 */
public class BooleanPanel extends JPanel implements ActionListener{

  private InitializationPanel  iniPanel;
  private int parameter = -1;
  
  private String[] booleanStrings = { "false", "true" };
  
  private final int TRUE  = 1;
  private final int FALSE = 0;
  
  private JComboBox booleanList;

  /**
   * Creates a simple boolean panel
   * 
   * @param iniPanel an object reference to an instance of InitializationPanel
   * @param parameter the parameter with two possible outcomes true and false
   */  
  public BooleanPanel( InitializationPanel iniPanel, int parameter ) {
     super();
    
     this.iniPanel  = iniPanel;    
     this.parameter = parameter;
    
     // create GUI
     setLayout( new BoxLayout( this, BoxLayout.Y_AXIS ) );
          
    
     // Create the combo box
     booleanList = new JComboBox( booleanStrings );
     
     boolean value = ((Boolean)iniPanel.getObjectsInformation().getParameter(parameter)).booleanValue();
     if ( value ) {
       booleanList.setSelectedIndex(TRUE);
     } else  {
       booleanList.setSelectedIndex(FALSE);
     }
     
     booleanList.addActionListener(this);
     booleanList.setActionCommand("changed");
     booleanList.setAlignmentX(LEFT_ALIGNMENT);
     setBorder(BorderFactory.createEmptyBorder(10,10,10,10));
     
     JLabel title = new JLabel(XMLConstants.treeElementNames[parameter]);
     title.setAlignmentX(LEFT_ALIGNMENT);
     add( title );
     add(Box.createRigidArea(new Dimension(0, 10)));
     add(booleanList);
     this.setAlignmentX(LEFT_ALIGNMENT);
    
     // enable/disable components depending on they can be changed during
     // runtime mode 
     if ( iniPanel != null ) {
       switch ( iniPanel.getMode() ) {
         case SSS3dConstants.RUNTIME_MODE :
              booleanList.setEnabled(XMLConstants.MODIFIABLE_VALUES_AT_RUNTIME[parameter]);
              break;
         default : break;
       }
     } else  {
         booleanList.setEnabled(XMLConstants.MODIFIABLE_VALUES_AT_RUNTIME[parameter]);
     }
         
     this.setVisible(true); 
  }

  /**
   * Implementation of action listener
   *
   * @param e java.awt.event.ActionEvent
   */
  public void actionPerformed(java.awt.event.ActionEvent e) {
    if (e.getActionCommand().equals("changed")) {

      if ( booleanList.getSelectedIndex() == TRUE ) {
         if ( iniPanel != null ) {
           if ( iniPanel.getMode() == SSS3dConstants.INITIALIZATION_MODE ||
                ( iniPanel.getMode() == SSS3dConstants.RUNTIME_MODE && 
                  XMLConstants.MODIFIABLE_VALUES_AT_RUNTIME[parameter]) ) {         
                
                iniPanel.getObjectsInformation().setParameter( new Boolean(true),parameter );
                iniPanel.updateTreeViewInfo(new Boolean(true),parameter);
           }
         
         } else {
           if(XMLConstants.MODIFIABLE_VALUES_AT_RUNTIME[parameter])
              iniPanel.getObjectsInformation().setParameter( new Boolean(true),parameter ); 
         }

      } else  {
         if ( iniPanel != null ) {
           if ( iniPanel.getMode() == SSS3dConstants.INITIALIZATION_MODE ||
                ( iniPanel.getMode() == SSS3dConstants.RUNTIME_MODE && 
                  XMLConstants.MODIFIABLE_VALUES_AT_RUNTIME[parameter]) ) {         
                
                iniPanel.getObjectsInformation().setParameter( new Boolean(false),parameter );
                iniPanel.updateTreeViewInfo(new Boolean(false),parameter);
           }
         
         } else {
           if(XMLConstants.MODIFIABLE_VALUES_AT_RUNTIME[parameter])
              iniPanel.getObjectsInformation().setParameter( new Boolean(false),parameter );
         }
      }
    }
  }
}
