/*
  File: ComboBoxPanel.java

  University of Applied Science Berne,HTA-Biel/Bienne,
  Computer Science Department.

  Diploma thesis J3D Solar System Simulator
  Originally written by Marcel Portner & Bernhard Hari (c) 2000

  CVS - Information :

  $Header: /var/cvsreps/projects/c450/2000/sss3d/source_diploma/sss3d/gui/ComboBoxPanel.java,v 1.7 2000/12/12 12:01:01 harib Exp $
  $Author: harib $
  $Date: 2000/12/12 12:01:01 $
  $State: Exp $

*/

package sss3d.gui;

import sss3d.utils.SSS3dConstants;
import sss3d.gui.*;
import sss3d.utils.xmlparser.XMLConstants;
import sss3d.contentbranch.*;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.event.*;

/**
 * Shows a combo box view
 *   
 * @author Marcel Portner & Bernhard Hari
 * @version $Revision: 1.7 $
 */
public class ComboBoxPanel extends JPanel implements ActionListener{

  private InitializationPanel  iniPanel;
  private int parameter = -1;
  
  private JComboBox list;

  /**
   * Creates a ComboBox panel depending on the given parameter.
   *
   * @param iniPanel an object reference to an instance of InitializationPanel
   * @param parameter the parameter with two possible outcomes true and false
   */
  public ComboBoxPanel( InitializationPanel iniPanel, int parameter ) {
    super();
    
    this.iniPanel  = iniPanel;
    this.parameter = parameter;
  
    // create GUI
    setLayout( new BoxLayout( this, BoxLayout.Y_AXIS ) );
  
    // Create the combo box
    if ( parameter == XMLConstants.CAMERA) {
        list = new JComboBox( SSS3dConstants.CAMERA_POSITIONS );
        int value = ((Integer)iniPanel.getObjectsInformation().getParameter(parameter)).intValue();
        list.setSelectedIndex(value);

    } else if ( parameter == XMLConstants.CALCULATION) {
        list = new JComboBox( SSS3dConstants.CALCULATION_METHODS );
        int value = ((Integer)iniPanel.getObjectsInformation().getParameter(parameter)).intValue();
        list.setSelectedIndex(value);
    }
    list.addActionListener(this);
    list.setActionCommand("changed");
    list.setAlignmentX(LEFT_ALIGNMENT);
   
    setBorder(BorderFactory.createEmptyBorder(10,10,10,10));
   
    JLabel title = new JLabel(XMLConstants.treeElementNames[parameter]);
    title.setAlignmentX(LEFT_ALIGNMENT);
    add( title );
    add(Box.createRigidArea(new Dimension(0, 10)));
    add(list);
    this.setAlignmentX(LEFT_ALIGNMENT);
    
    
    // enable/disable components depending on they can be changed during
    // runtime mode
    if ( iniPanel != null )  {  
      switch ( iniPanel.getMode() ) {
        case SSS3dConstants.RUNTIME_MODE :
             list.setEnabled(XMLConstants.MODIFIABLE_VALUES_AT_RUNTIME[parameter]);
             break;
        default : break;
      }
    } else {
      list.setEnabled(XMLConstants.MODIFIABLE_VALUES_AT_RUNTIME[parameter]);
    }
    
    this.setVisible(true); 
 
  }

  /**
   * Implementation of action listener
   *
   * @param e java.awt.event.ActionEvent
   */
  public void actionPerformed(java.awt.event.ActionEvent e) {
    if (e.getActionCommand().equals("changed")) {
       if ( parameter == XMLConstants.CAMERA ) {
           if( iniPanel != null ){
            if ( iniPanel.getMode() == SSS3dConstants.INITIALIZATION_MODE ||
                 ( iniPanel.getMode() == SSS3dConstants.RUNTIME_MODE && 
                   XMLConstants.MODIFIABLE_VALUES_AT_RUNTIME[XMLConstants.CAMERA]) ) { 
                             
               iniPanel.getObjectsInformation().setParameter( new Integer(list.getSelectedIndex()), XMLConstants.CAMERA );
               iniPanel.updateTreeViewInfo(new Integer(list.getSelectedIndex()), XMLConstants.CAMERA );
            }
           } else  {
               if( XMLConstants.MODIFIABLE_VALUES_AT_RUNTIME[XMLConstants.CAMERA] )
                   iniPanel.getObjectsInformation().setParameter( new Integer(list.getSelectedIndex()), XMLConstants.CAMERA );
           }
       } else if ( parameter == XMLConstants.CALCULATION ) {
           if( iniPanel != null ){
            if ( iniPanel.getMode() == SSS3dConstants.INITIALIZATION_MODE ||
                 ( iniPanel.getMode() == SSS3dConstants.RUNTIME_MODE && 
                   XMLConstants.MODIFIABLE_VALUES_AT_RUNTIME[XMLConstants.CALCULATION]) ) { 
                             
               iniPanel.getObjectsInformation().setParameter( new Integer(list.getSelectedIndex()), XMLConstants.CALCULATION );
               iniPanel.updateTreeViewInfo(new Integer(list.getSelectedIndex()),XMLConstants.CALCULATION);
            }
           } else  {
               if( XMLConstants.MODIFIABLE_VALUES_AT_RUNTIME[XMLConstants.CALCULATION] )
                   iniPanel.getObjectsInformation().setParameter( new Integer(list.getSelectedIndex()), XMLConstants.CALCULATION );
           }
       }
    }
  }
  
}