/*
  File: AdapterNode.java

  University of Applied Science Berne,HTA-Biel/Bienne,
  Computer Science Department.

  Diploma thesis J3D Solar System Simulator
  Originally written by Marcel Portner & Bernhard Hari (c) 2000
  
  CVS - Information :
  
  $Header: /var/cvsreps/projects/c450/2000/sss3d/source_diploma/sss3d/gui/infobox/AdapterNode.java,v 1.3 2000/12/12 09:07:47 harib Exp $
  $Author: harib $
  $Date: 2000/12/12 09:07:47 $
  $State: Exp $
  
*/
package sss3d.gui.infobox;

import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.JTree;
import java.awt.Component;
import javax.swing.ImageIcon;
import javax.swing.JLabel; 
   
import sss3d.utils.SSS3dConstants; 
import sss3d.utils.xmlparser.XMLConstants;   

/**
 * This class wraps a DOM node and returns the text we want to
 * display in the tree. It also returns children, index values,
 * and child counts.
 *
 * @author Marcel Portner & Bernhard Hari
 * @version $Revision: 1.3 $
 */
public class AdapterNode extends DefaultMutableTreeNode
{ 
  public org.w3c.dom.Node domNode;
  
  // show tree in compressed mode
  private boolean compress = true;
  
  private boolean treeElement(String elementName) {
     for (int i = 0; i < XMLConstants.treeElementNames.length; i++) {
       if ( elementName.equals(XMLConstants.treeElementNames[i]) ) return true;
     }
     return false;
  }

  /**
   * Constructor
   *
   * @param node construct an Adapter node from a DOM node ( org.w3c.dom.Node )
   */  
  public AdapterNode(org.w3c.dom.Node node) {
    domNode = node;
  }


  /**
   * Return a string that identifies this node in the tree
   *
   * @return String identification string from this node
   */
  public String toString() {
    String s = XMLConstants.typeName[domNode.getNodeType()];
    String nodeName = domNode.getNodeName();
    if (! nodeName.startsWith("#")) {
       s += ": " + nodeName;
    }
    if (compress) {
       String t = content().trim();
       int x = t.indexOf("\n");
       if (x >= 0) t = t.substring(0, x);
       s += " " + t;
       return s;
    }
    if (domNode.getNodeValue() != null) {
       if (s.startsWith("ProcInstr")) 
          s += ", "; 
       else 
          s += ": ";
       // Trim the value to get rid of NL's at the front
       String t = domNode.getNodeValue().trim();
       int x = t.indexOf("\n");
       if (x >= 0) t = t.substring(0, x);
       s += t;
    }
    return s;
  }

  /**
   * Returns the content from this node (value)
   *
   * @return String content from this node
   */
  public String content() {
    String s = "";
    org.w3c.dom.NodeList nodeList = domNode.getChildNodes();
    for (int i = 0; i < nodeList.getLength(); i++) {
      org.w3c.dom.Node node = nodeList.item(i);
      int type = node.getNodeType();
      AdapterNode adpNode = new AdapterNode(node); //inefficient, but works
      if (type == XMLConstants.ELEMENT_TYPE) {
        // Skip subelements that are displayed in the tree.   
        if ( treeElement(node.getNodeName()) ) continue;

        // EXTRA-CREDIT HOMEWORK:
        //   Special case the SLIDE element to use the TITLE text
        //   and ignore TITLE element when constructing the tree.

        // EXTRA-CREDIT
        //   Convert ITEM elements to html lists using
        //   <ul>, <li>, </ul> tags

        s += "<" + node.getNodeName() + ">";
        s += adpNode.content();
        s += "</" + node.getNodeName() + ">";
      } else if (type == XMLConstants.TEXT_TYPE) {
        s += node.getNodeValue();
      } else if (type == XMLConstants.ENTITYREF_TYPE) {
        // The content is in the TEXT node under it
        s += adpNode.content();
      } else if (type == XMLConstants.CDATA_TYPE) {
        // The "value" has the text, same as a text node.
        //   while EntityRef has it in a text node underneath.
        //   (because EntityRef can contain multiple subelements)
        // Convert angle brackets and ampersands for display
        StringBuffer sb = new StringBuffer( node.getNodeValue() );
        for (int j=0; j<sb.length(); j++) {
          if (sb.charAt(j) == '<') {
            sb.setCharAt(j, '&');
            sb.insert(j+1, "lt;");
            j += 3;
          } else if (sb.charAt(j) == '&') {
            sb.setCharAt(j, '&');
            sb.insert(j+1, "amp;");
            j += 4;
          }
        }
        s += "<pre>" + sb + "\n</pre>";
      }
       // Ignoring these:
       //   ATTR_TYPE      -- not in the DOM tree
       //   ENTITY_TYPE    -- does not appear in the DOM
       //   PROCINSTR_TYPE -- not "data"
       //   COMMENT_TYPE   -- not "data"
       //   DOCUMENT_TYPE  -- Root node only. No data to display.
       //   DOCTYPE_TYPE   -- Appears under the root only
       //   DOCFRAG_TYPE   -- equiv. to "document" for fragments
       //   NOTATION_TYPE  -- nothing but binary data in here
    }
    return s;
  }

  /**
   * Returns index of child node
   *
   * @param child reference to the child node
   * @return int index of child node
   */
  public int index(AdapterNode child) {
    int count = childCount();
    for (int i=0; i<count; i++) {
      AdapterNode n = this.child(i);
      if (child.domNode == n.domNode) return i;
    }
    return -1; // Should never get here.
  }

  /**
   * Returns child node given by it's index
   *
   * @param searchIndex index of child
   * @return AdapterNode child node
   */
  public AdapterNode child(int searchIndex) {
    //Note: JTree index is zero-based. 
    org.w3c.dom.Node node = 
         domNode.getChildNodes().item(searchIndex);
    if (compress) {
      // Return Nth displayable node
      int elementNodeIndex = 0;
      for (int i=0; i<domNode.getChildNodes().getLength(); i++) {
        node = domNode.getChildNodes().item(i);
        if (node.getNodeType() == XMLConstants.ELEMENT_TYPE 
        && treeElement( node.getNodeName() )
        && elementNodeIndex++ == searchIndex) {
           break; 
        }
      }
    }
    return new AdapterNode(node); 
  }

  /**
   * Returns the number of childs, attached to this node
   *
   * @return int number of childs attached to this node
   */
  public int childCount() {
    if (!compress) {
      // Indent this
      return domNode.getChildNodes().getLength();  
    } 
    int count = 0;
    for (int i=0; i<domNode.getChildNodes().getLength(); i++) {
       org.w3c.dom.Node node = domNode.getChildNodes().item(i); 
       if (node.getNodeType() == XMLConstants.ELEMENT_TYPE
       && treeElement( node.getNodeName() )) 
       {
         // Note: 
         //   Have to check for proper type. 
         //   The DOCTYPE element also has the right name
         ++count;
       }
    }
    return count;
  }
  
  /**
   * Sets new value of the AdapterNode
   *
   * @param value new value
   */
  public void setValue( String value )
  {
    try {
      domNode.setNodeValue( value );
    }catch(Exception e) {
       System.out.println("exception: " + e.getMessage());
       e.printStackTrace();
    }
  }

}