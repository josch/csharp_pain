/*
  File: InfoBox.java

  University of Applied Science Berne,HTA-Biel/Bienne,
  Computer Science Department.

  Diploma thesis J3D Solar System Simulator
  Originally written by Marcel Portner & Bernhard Hari (c) 2000
  
  CVS - Information :
  
  $Header: /var/cvsreps/projects/c450/2000/sss3d/source_diploma/sss3d/gui/infobox/InfoBox.java,v 1.18 2000/12/15 02:56:36 portm Exp $
  $Author: portm $
  $Date: 2000/12/15 02:56:36 $
  $State: Exp $
  
*/
package sss3d.gui.infobox;

import sss3d.contentbranch.*;
import sss3d.utils.observer.*;
import sss3d.utils.SSS3dConstants;
import sss3d.gui.*;
import sss3d.*;
import sss3d.utils.xmlparser.XMLConstants;

import java.awt.*;
import javax.vecmath.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;

/**
 * Infobox creates a JFrame containing all information about the simulator. Also, it
 * allows to control the simulator by changing values. 
 *
 * @author Marcel Portner & Bernhard Hari
 * @version $Revision: 1.18 $
 */
public class InfoBox extends JFrame implements PositionObserver, InfoObserver, Runnable {

   private JTabbedPane jtp;                // swing component used to display positons and 
                                           // information inside the main window
   private SolarSystemSimulator sss;       // link to control simulation

   private JFrame frame;                   // main frame of infobox
   private Thread thread;                  // infobox runs in a own thread
   
   public JMenuItem miGo, miStop, miRot;
   
   public JMenu mObjectsPos;
   public JMenu mObjectsInfo;
   public JMenu mObjectsHide;
   public JMenu mObjectsColor;
   
   // about dialog box
   private JDialog aboutBox;               
   private String aboutText = "Ziel der Diplomarbeit ist es, ein Java3D-Programm zu entwickeln,\n"+
                              "welches eine virtuelle dreidimensionale animierte Darstellung des\n"+
                              "Sonnensystems simuliert. Diese virtuelle animierte Darstellung wird\n"+
                              "die Sonne, alle Planeten mit ihren Satelliten (Monde) und die \n"+
                              "bekanntesten periodischen Kometen enthalten. Die Umlaufbahn der\n"+
                              "Planeten werden nach dem Kepler Gesetzt berechnet und durch farbige\n"+
                              "Ellipsen dargestellt. Man hat zusätzlich die Möglichkeit die \n"+
                              "Konstellation des Planetensystem ab einem bestimmten Datum zu\n"+
                              "simulieren und sich dann im Sonnensystem zu navigieren. Wenn der\n"+
                              "Programmbenützer eine 3D-Schutterbrille besitzt, wird er auch die\n"+
                              "Möglichkeit haben, das Sonnensystem in 3D zu betrachten.\n"+
                              "J3D - Solar System Simulator wird komplett in Java geschrieben und\n"+
                              "läuft somit auf allen Plattformen die Java2 und Java3D unterstützen.\n";


   private ObjectsPositions     objPos;    
   private ObjectsInformation   objInfo;
   private InitializationObject iniObject;

   private Dimension screenSize;

  /**
   * Constructs an InfoBox. 
   * @param     solar   reference to the instance of the solar system simulator
   * @param     objPos  reference to the positions of all objects
   * @param     objInfo reference to the informations of all objects
   */
   public InfoBox( SolarSystemSimulator solar, ObjectsPositions objPos, ObjectsInformation objInfo ) {
     
     super("InfoBox - J3D Solar System Simulator Version 1.0");
     
     sss = solar;

     this.objPos = objPos;
     this.objPos.attach(this);
     this.objInfo = objInfo;
     this.objInfo.attach(this);
     iniObject = objInfo.getInitializationObject();

     thread = new Thread(this, "InfoBox");
     thread.start();
     frame = this;
   }

   /**
    * Run method creates JFrame.
    */
   public void run() {
     this.setSize(800,550);
     this.setLocation(20, 20);

     this.addWindowListener(new WindowCmd());

     screenSize = Toolkit.getDefaultToolkit().getScreenSize();

     JPanel panel = new JPanel(new BorderLayout());
     jtp = new JTabbedPane();
     panel.add(jtp, BorderLayout.CENTER);

     panel.add(createMenuBar(), BorderLayout.NORTH);
     this.getContentPane().add(panel);

     this.setVisible(true);
   }

   /**
    * Updates objects positions - not in use at the moment.
    */
   public void update( OrbitPositions orbitPos ){

   }

   /**
    * Updates objects positions for a specific object.
    * 
    * @param id id of to be updated object
    * @param parameter the parameter that has to be updated
    */
   public void update( String id, int parameter ) {

     if ( !id.equals("infobox") &&
          !id.equals("initializationpanel") ) {
            
        switch ( parameter ) {
           case SSS3dConstants.STAR_TYPE :
           case SSS3dConstants.PLANET_TYPE :
           case SSS3dConstants.MOON_TYPE :
           case SSS3dConstants.COMET_TYPE :
           case SSS3dConstants.SATELLITE_TYPE :
           case SSS3dConstants.ROCKET_TYPE :
                CelestialObjectInfo coi = objInfo.getInfo(id);
                addInfoTab(coi);
                break;
        }
     }
   }

   /**
    * Get the ID from this class.
    *
    * @return String returns the ID.
    */
   public String getId() {
     return "infobox";
   }

   /**
    * Returns object information.In this case we don't have information,because the
    * InfoBox isn't a celestial object. 
    *
    * @return CelestialObjectInfo returns always null
    */
   public CelestialObjectInfo getInfo() {
     return null;
   }

   /**
    * Adds an information tab to the main frame. 
    *
    * @param CelstialObjectInfo information object
    */
   public void addInfoTab( CelestialObjectInfo cObjInfo ) {
      int tabs = jtp.getTabCount();
      boolean found = false;
      String id = (cObjInfo.getName()).toLowerCase();
      String text = id +"-info";
      String title = "";

      // check if tab already exists
      for(int i = 0; i < tabs; i++) {
        title = jtp.getTitleAt(i);
        if(title.equals(text)) {  
          found = true;
        }
      }

      if(!found) {
        jtp.insertTab(text, null, new InfoPanel(cObjInfo, this), null, 0);
        jtp.setSelectedIndex(0);
        // set menu item selected
        JCheckBoxMenuItem jcbmi = null;
        int length = mObjectsInfo.getItemCount();
        for(int i = 0; i < length; i++) {
          jcbmi = (JCheckBoxMenuItem)mObjectsInfo.getItem(i);
          if((jcbmi.getText().toLowerCase()).equals(id)) {
            jcbmi.setState(true);	
          }
        } 
      }
   }

   /**
    * Removes an information tab from the main frame. 
    *
    * @param CelstialObjectInfo to be deleted information object
    */
   public void removeInfoTab( CelestialObjectInfo cObjInfo ) {
      int tabs = jtp.getTabCount();
      int index = -1;
      String id = (cObjInfo.getName()).toLowerCase()+"-info";
      String title = "";
        
      for(int i=0; i < tabs;i++) {
        title = jtp.getTitleAt(i);
        if(title.equals(id)) {
           index = i;
        }
      }
      if(index != -1) {
        jtp.removeTabAt(index);
      }
   }

   /**
    * Adds a position tab to the main frame. 
    *
    * @param CelstialObjectInfo information object
    */
   public void addPosTab( CelestialObjectInfo cObjInfo ) {
      int tabs = jtp.getTabCount();
      boolean found = false;
      String text = (cObjInfo.getName()).toLowerCase() +"-pos";
      String title = "";
      
      boolean compressed = ((Boolean)iniObject.getParameter(XMLConstants.COMPRESSED)).booleanValue();
      double time  = ((Double)iniObject.getParameter(XMLConstants.JDAY)).doubleValue();

      Point3f[] positions = objPos.getPositions(cObjInfo, time, compressed);
      
      for(int i = 0; i < tabs; i++) {
        title = jtp.getTitleAt(i);
        if(title.equals(text)) { 
          found = true;
        }
      }
      if(!found) {       
        jtp.insertTab(text, null, new PositionsPanel(cObjInfo ,positions), null, 0);
        jtp.setSelectedIndex(0);        
      }
   }

   /**
    * Removes a position tab from the main frame. 
    *
    * @param CelstialObjectInfo to be removed information object
    */
   public void removePosTab( CelestialObjectInfo cObjInfo ) {
      int tabs = jtp.getTabCount();
      int index = -1;
      String id = (cObjInfo.getName()).toLowerCase() +"-pos";
      String title = "";
       
      for(int i = 0; i < tabs; i++) {
        title = jtp.getTitleAt(i);
        if(title.equals(id)) {
           index = i;
        }
      }
      if(index != -1) {
        jtp.removeTabAt(index);
      }
   }

   /**
    * Adds the initialization tab to the main frame. 
    */
   public void addIniTab() {
      int tabs = jtp.getTabCount();
      boolean found = false;
      String text = "initialization";
      String title = "";
      
      for(int i = 0; i < tabs; i++) {
        title = jtp.getTitleAt(i);
        if(title.equals(text)) { 
          found = true;
        }
      }
      if(!found) {       
        jtp.insertTab(text, null, new InitializationPanel( objInfo,SSS3dConstants.RUNTIME_MODE ), null, 0);
        jtp.setSelectedIndex(0);        
      }
   }


   /**
    * Removes the initialization tab from the main frame. 
    */
   public void removeIniTab() {
      int tabs = jtp.getTabCount();
      int index = -1;
      String title = "";
       
      for(int i = 0; i < tabs; i++) {
        title = jtp.getTitleAt(i);
        if(title.equals("initialization")) {
           index = i;
        }
      }
      if(index != -1) {
        jtp.removeTabAt(index);
      }
   }

   /**
    * Creates the menubar for the infobox. 
    */
   private JMenuBar createMenuBar() {

      // MenuBar
      JMenuBar menuBar = new JMenuBar();
      JMenuItem mi;
      JCheckBoxMenuItem cb;

      // File Menu
      JMenu file = (JMenu) menuBar.add(new JMenu("File"));
      file.setMnemonic('F');
      
      mi = (JMenuItem) file.add(new JMenuItem("Exit"));
      mi.setMnemonic('x');
      mi.addActionListener(new ActionCmdMenu());

      // Animate Menu
      JMenu animate = (JMenu) menuBar.add(new JMenu("Animate"));
      animate.setMnemonic('A');
      miGo = (JMenuItem) animate.add(new JMenuItem("Go"));
      miGo.setMnemonic('G');
      miGo.setEnabled(false);
      miGo.addActionListener(new ActionCmdMenu());
      miStop = (JMenuItem) animate.add(new JMenuItem("Stop all"));
      miStop.setMnemonic('S');
      miStop.setEnabled(true);
      miStop.addActionListener(new ActionCmdMenu());
      miRot = (JMenuItem) animate.add(new JMenuItem("Rotate only"));
      miRot.setMnemonic('m');
      miRot.setEnabled(true);
      miRot.addActionListener(new ActionCmdMenu());

      // View Menu
      JMenu view = (JMenu) menuBar.add(new JMenu("View"));
      view.setMnemonic('V');
      mi = (JMenuItem) view.add(new JMenuItem("Init position"));
      mi.setMnemonic('I');
      mi.addActionListener(new ActionCmdMenu());

      cb = (JCheckBoxMenuItem) view.add(new JCheckBoxMenuItem("Navigation"));
      cb.setMnemonic('N');
      cb.setSelected(false);
      cb.addActionListener(new ActionCmdMenu());
      
      cb = (JCheckBoxMenuItem) view.add(new JCheckBoxMenuItem("Initialization"));
      cb.setMnemonic('Z');
      cb.setSelected(false);
      cb.addActionListener(new ActionCmdMenu());
      
      cb = (JCheckBoxMenuItem) view.add(new JCheckBoxMenuItem("Background"));
      cb.setMnemonic('B');
      cb.setSelected(false);
      cb.addActionListener(new ActionCmdMenu());

      cb = (JCheckBoxMenuItem) view.add(new JCheckBoxMenuItem("Orbit"));
      cb.setMnemonic('O');
      cb.setSelected(true);
      //cb.setEnabled(false);
      cb.addActionListener(new ActionCmdMenu());
      
      cb = (JCheckBoxMenuItem) view.add(new JCheckBoxMenuItem("Coordinate"));
      cb.setMnemonic('C');
      cb.setSelected(true);
      //cb.setEnabled(false);
      cb.addActionListener(new ActionCmdMenu());

      // create menu view - object positions
      mObjectsPos = new JMenu("Object Positions");

      for(int i = 0; i < SSS3dConstants.MOON; i++) {
         mObjectsPos.add(new MyJCheckBoxMenuItem(this,
                                                 mObjectsPos,
                                                 SSS3dConstants.CELESTIAL[i],
                                                 objInfo));
      }

      view.add(mObjectsPos);

      // create menu view - object information
      mObjectsInfo = new JMenu("Object Information");
      
      for(int i = 0; i < SSS3dConstants.MOON; i++) {
         mObjectsInfo.add(new MyJCheckBoxMenuItem(this,
                                                  mObjectsInfo,
                                                  SSS3dConstants.CELESTIAL[i],
                                                  objInfo));
      }

      view.add(mObjectsInfo);

      // Help Menu
      JMenu help = (JMenu) menuBar.add(new JMenu("Help"));
      help.setMnemonic('H');
      mi = (JMenuItem) help.add(new JMenuItem("About"));
      mi.setMnemonic('A');
      mi.addActionListener(new ActionCmdMenu());

      return menuBar;
   }

   /**
    * Window Listener for the InfoBox Frame.
    */
   class WindowCmd extends WindowAdapter {
      public void windowClosing(WindowEvent e) {
         System.exit(0);
      }
   }

   /**
    * Class Action Listener for the file menu.
    */
   class ActionCmdMenu implements ActionListener {
      public void actionPerformed(ActionEvent e) {
         String s = e.getActionCommand();
         if(s.equals("Exit")) {
            System.exit(0);
         } else if(s.equals("Go")) {
            miGo.setEnabled(false);
            miStop.setEnabled(true);
            miRot.setEnabled(true);
            sss.lnkSceneSolarSystem.setAnimation(true, true);
            if(sss.infoPanel != null) {
               sss.infoPanel.startDate();
            }
     	  } else if(s.equals("Stop all")) {
     	     miGo.setEnabled(true);
     	     miStop.setEnabled(false);
     	     miRot.setEnabled(false);
     	     sss.lnkSceneSolarSystem.setAnimation(false, false);
     	     if(sss.infoPanel != null) {
               sss.infoPanel.stopDate();
            }
     	  } else if(s.equals("Rotate only")) {
     	     miGo.setEnabled(true);
     	     miStop.setEnabled(false);
     	     miRot.setEnabled(false);
     	     sss.lnkSceneSolarSystem.setAnimation(false, true);
     	     if(sss.infoPanel != null) {
               sss.infoPanel.stopDate();
            }
     	  } else if(s.equals("Init position")) {
            sss.lnkViewBranch.setStartPosition();
     	  } else if(s.equals("Background")) {
            JCheckBoxMenuItem cbmi = (JCheckBoxMenuItem)e.getSource();
            sss.lnkSceneSolarSystem.setBackground(cbmi.isSelected());
     	  } else if(s.equals("Initialization")) {
            JCheckBoxMenuItem cbmi = (JCheckBoxMenuItem)e.getSource();
            if ( cbmi.isSelected() ) {
              addIniTab();
            } else {
              removeIniTab();
            }
     	  } else if(s.equals("Navigation")) {
            JCheckBoxMenuItem cbmi = (JCheckBoxMenuItem)e.getSource();
            if ( cbmi.isSelected() ) {
              sss.addNavigation();
            } else {
              sss.removeNavigation();
            }
     	  } else if(s.equals("Orbit")) {
            JCheckBoxMenuItem cbmi = (JCheckBoxMenuItem)e.getSource();
            sss.lnkSceneSolarSystem.setOrbit(cbmi.isSelected());
     	  } else if(s.equals("Coordinate")) {
            JCheckBoxMenuItem cbmi = (JCheckBoxMenuItem)e.getSource();
            sss.lnkSceneSolarSystem.setCoordinate(cbmi.isSelected());
     	  } else if(s.equals("About")) {
            if(aboutBox == null) {
               aboutBox = new JDialog(frame, "About the Solar System Simulator", true);
               JPanel groupPanel = new JPanel(new BorderLayout());
               ImageIcon groupPicture = new ImageIcon("images/sss.gif", "Solar System Simulator");
               aboutBox.getContentPane().add(groupPanel, BorderLayout.CENTER);
               JLabel groupLabel = (new JLabel(groupPicture));
               groupPanel.add(groupLabel, BorderLayout.NORTH);
               JPanel textPanel = new JPanel();
               groupPanel.add(textPanel, BorderLayout.CENTER);
               JTextArea textArea = (JTextArea) textPanel.add(new JTextArea(aboutText));
               textArea.setEditable(false);
               textArea.setFont(new Font("Courier", Font.PLAIN, 14));
               textArea.setBackground(Color.lightGray);
               JPanel buttonPanel = new JPanel();
               groupPanel.add(buttonPanel, BorderLayout.SOUTH);
               JButton button = (JButton) buttonPanel.add(new JButton("OK"));
               button.addActionListener(new ActionListener() {
                  public void actionPerformed(ActionEvent e) {
                     aboutBox.setVisible(false);
                  }
               });
            }
            aboutBox.pack();
            aboutBox.setLocation((screenSize.width - aboutBox.getWidth()) / 2,
                                 (screenSize.height - aboutBox.getHeight()) / 2);
            aboutBox.show();
         }
      }
   }
}
