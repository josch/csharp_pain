/*
  File: MyJCheckBoxMenuItem.java

  University of Applied Science Berne,HTA-Biel/Bienne,
  Computer Science Department.

  Diploma thesis J3D Solar System Simulator
  Originally written by Marcel Portner & Bernhard Hari (c) 2000
  
  CVS - Information :
  
  $Header: /var/cvsreps/projects/c450/2000/sss3d/source_diploma/sss3d/gui/infobox/MyJCheckBoxMenuItem.java,v 1.7 2000/12/13 13:39:35 portm Exp $
  $Author: portm $
  $Date: 2000/12/13 13:39:35 $
  $State: Exp $
  
*/
package sss3d.gui.infobox;

import sss3d.contentbranch.CelestialObjectInfo;
import sss3d.utils.observer.*;

import java.awt.*;
import javax.swing.*;
import javax.vecmath.*;

import java.awt.event.*;
import java.applet.*;
import javax.swing.*;
import javax.swing.event.*;

/**
 * MyJCheckBoxMenuItem is a subclass from JCheckBoxMenuItem. It was build to 
 * fit our needs for the items inside the menu.
 * @author Marcel Portner & Bernhard Hari
 * @version $Revision: 1.7 $
 */
public class MyJCheckBoxMenuItem extends JCheckBoxMenuItem implements ActionListener {

   private JCheckBoxMenuItem cb = null;          
   private JMenu jmenu = null;                    // parent menu
   private String name = "";                      // name of this item
   private InfoBox infobox = null;                // reference to infobox
   private ObjectsInformation objInfo = null;     

   /**
    * Constructor creates a new menu item, based on the parameters given.
    * This constructor must be used to add an information menu item.
    *
    * @param     ibox reference to the infobox
    * @param     jmenu reference to the parent menu
    * @param     text name of the new menu item
    * @param     objInfo reference to object information
    */
   public MyJCheckBoxMenuItem(InfoBox infobox, JMenu jmenu,
                              String name, ObjectsInformation objInfo){
          
      super(name);
      this.name = name;
      this.jmenu = jmenu;
      this.infobox = infobox;
      cb = (JCheckBoxMenuItem) jmenu.add(this);
      cb.setSelected(false);
      cb.setEnabled(true);
      cb.addActionListener(this);
      
      this.objInfo = objInfo;
  
   }
  
  
   /**
    * Method handles a swing component event. Based on the event it performs
    * the required action.
    *
    * @param     e the action event
    */
   public void actionPerformed(ActionEvent e) {
      JCheckBoxMenuItem cbmi = (JCheckBoxMenuItem)e.getSource();
      
      String menu = jmenu.getText();
      
      if(menu.equals("Object Positions")) {
         if(cbmi.isSelected()) {
            CelestialObjectInfo cObjInfo = objInfo.getInfo(name);
            if(cObjInfo != null) {
               infobox.addPosTab( cObjInfo );
            }
         } else {
            CelestialObjectInfo cObjInfo = objInfo.getInfo(name);
            if(cObjInfo != null) {
               infobox.removePosTab( cObjInfo );
            }
         }
      } else if(menu.equals("Object Information")) {
         if(cbmi.isSelected()) {
            CelestialObjectInfo cObjInfo = objInfo.getInfo(name);
            if(cObjInfo != null) {
               infobox.addInfoTab( cObjInfo );
            }
         } else {
            CelestialObjectInfo cObjInfo = objInfo.getInfo(name);
            if(cObjInfo != null) {
               infobox.removeInfoTab( cObjInfo );
            }
         }
      }
   }
} // end of class MyJCheckBoxMenuItem