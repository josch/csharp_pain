/*
  File: PositionsPanel.java

  University of Applied Science Berne,HTA-Biel/Bienne,
  Computer Science Department.

  Diploma thesis J3D Solar System Simulator
  Originally written by Marcel Portner & Bernhard Hari (c) 2000
  
  CVS - Information :
  
  $Header: /var/cvsreps/projects/c450/2000/sss3d/source_diploma/sss3d/gui/infobox/PositionsPanel.java,v 1.3 2000/12/13 13:39:35 portm Exp $
  $Author: portm $
  $Date: 2000/12/13 13:39:35 $
  $State: Exp $
  
*/
package sss3d.gui.infobox;

import sss3d.contentbranch.CelestialObjectInfo;

import java.awt.*;
import javax.swing.*;
import javax.vecmath.*;

/**
 * This class is used to display the positions informations
 * inside the InfoBox. It contains a reference to a private
 * table and the algorithms to fill the tables.
 *
 * @author Marcel Portner & Bernhard Hari
 * @version $Revision: 1.3 $
 */
public class PositionsPanel extends JPanel {

  private JTable table;
  private String[] columnHeads = {"Position #","X","Y","Z"};
  private int ROWS;
  private int COLUMNS = 4;
  
  private Object[][] data;
 
  /**
   * Constructor.
   *
   * @param cObjInfo contains the number of positions to display
   * @param positions an array of three-dimensional points  containing the positions
   */  
  public PositionsPanel( CelestialObjectInfo cObjInfo, Point3f[] positions ) {
  	
    ROWS = cObjInfo.getNbrOfPositions();
    
    setLayout(new BorderLayout());
    
    data = new Object[ROWS][COLUMNS];
    
    // fill the table
    for(int r = 0; r < ROWS; r++) {
      for(int c = 0; c < COLUMNS; c++) {
      	switch(c){
      	  case 0:
      	    data[r][c] = Integer.toString(r);
      	    break;
      	  case 1:
      	    data[r][c] = Float.toString(positions[r].x);
      	    break;
      	  case 2:
      	    data[r][c] = Float.toString(positions[r].y);
      	    break;
      	  case 3:
      	    data[r][c] = Float.toString(positions[r].z);
      	    break;
      	  default: data[r][c] = "null";
      	}
      }	
    }
    
    // create the new table
    table = new JTable(data,columnHeads);
    JScrollPane jsp = new JScrollPane(table,
                                 ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,
                                 ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
    
    add(jsp,BorderLayout.CENTER);	
    
  }
}