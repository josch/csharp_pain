/*
  File: MyRenderer.java

  University of Applied Science Berne,HTA-Biel/Bienne,
  Computer Science Department.

  Diploma thesis J3D Solar System Simulator
  Originally written by Marcel Portner & Bernhard Hari (c) 2000
  
  CVS - Information :
  
  $Header: /var/cvsreps/projects/c450/2000/sss3d/source_diploma/sss3d/gui/infobox/MyRenderer.java,v 1.5 2000/12/13 14:03:33 portm Exp $
  $Author: portm $
  $Date: 2000/12/13 14:03:33 $
  $State: Exp $
  
*/
package sss3d.gui.infobox;

import sss3d.utils.*;
import sss3d.utils.xmlparser.XMLConstants;
import sss3d.gui.*;

import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.JTree;
import java.awt.Component;

import javax.swing.JPanel;
import org.w3c.dom.*;


/**
 * This class is used to render each element inside the tree view of
 * the document object.
 *
 * @author Marcel Portner & Bernhard Hari
 * @version $Revision: 1.5 $
 */
public class MyRenderer extends DefaultTreeCellRenderer {

        private JPanel panel;
        private Document document;
 
  /**
   * Constructor.
   * 
   * @param infoPanel an object reference to its associated InfoPanel
   */     
  public MyRenderer( JPanel panel ) {
      this.panel = panel;
  }

  /**
   * Returns the tree cell renderer component.
   * Implementation of method from DefaultTreeCellRenderer
   * 
   * @see javax.swing.tree.DefaultTreeCellRenderer
   */
  public Component getTreeCellRendererComponent(
                      JTree tree,
                      Object value,
                      boolean sel,
                      boolean expanded,
                      boolean leaf,
                      int row,
                      boolean hasFocus) {
                      
       
      AdapterNode node = (AdapterNode)value;
      String nodeName  = new String(node.domNode.getNodeName());
      
      if ( nodeName.startsWith("#document")) {
          if ( ((GlobalPanel)panel).getType().equals(SSS3dConstants.TYPES[SSS3dConstants.INI_TYPE]) ) {
            nodeName = "Initialization";
          } else if ( ((GlobalPanel)panel).getType().equals(SSS3dConstants.TYPES[SSS3dConstants.CELESTIALOBJECT_TYPE]) ) {
            nodeName = ((InfoPanel)panel).getCelestialObjectInfo().getName();;
          } else  {
            nodeName = "UNKNOWN";
          }
      } else if ( nodeName.startsWith(XMLConstants.treeElementNames[XMLConstants.STAR]) ) {
                  nodeName = XMLConstants.treeElementNames[XMLConstants.STAR] +" = "+ getObjectAttribute(node,XMLConstants.OBJECTNAME);
      } else if ( nodeName.startsWith(XMLConstants.treeElementNames[XMLConstants.PLANET]) ) {
                  nodeName = XMLConstants.treeElementNames[XMLConstants.PLANET] +" = "+ getObjectAttribute(node,XMLConstants.OBJECTNAME);
      } else if ( nodeName.startsWith(XMLConstants.treeElementNames[XMLConstants.MOON]) ) {
                  nodeName = XMLConstants.treeElementNames[XMLConstants.MOON] +" = "+ getObjectAttribute(node,XMLConstants.OBJECTNAME);
      } else if ( nodeName.startsWith(XMLConstants.treeElementNames[XMLConstants.COMET]) ) {
                  nodeName = XMLConstants.treeElementNames[XMLConstants.COMET] +" = "+ getObjectAttribute(node,XMLConstants.OBJECTNAME);
      } else if ( nodeName.startsWith(XMLConstants.treeElementNames[XMLConstants.SATELLITE]) ) {
                  nodeName = XMLConstants.treeElementNames[XMLConstants.SATELLITE] +" = "+ getObjectAttribute(node,XMLConstants.OBJECTNAME);
      } else if ( nodeName.startsWith(XMLConstants.treeElementNames[XMLConstants.ROCKET]) ) {
                  nodeName = XMLConstants.treeElementNames[XMLConstants.ROCKET] +" = "+ getObjectAttribute(node,XMLConstants.OBJECTNAME);
      } else if ( nodeName.startsWith(XMLConstants.treeElementNames[XMLConstants.ANIMATIONSPEED]) ) {
                  nodeName = XMLConstants.treeElementNames[XMLConstants.ANIMATIONSPEED] +" = "+ getObjectAttribute(node,XMLConstants.TYPE)+
                             " value = "+getObjectAttribute(node,XMLConstants.VALUE);
      } else if ( !nodeName.startsWith(XMLConstants.treeElementNames[XMLConstants.CELESTIALOBJECT]) &&
                  !nodeName.startsWith(XMLConstants.treeElementNames[XMLConstants.INI]) &&
                  !nodeName.startsWith(XMLConstants.treeElementNames[XMLConstants.GENERAL]) &&
                  !nodeName.startsWith(XMLConstants.treeElementNames[XMLConstants.INFO]) &&
                  !nodeName.startsWith(XMLConstants.treeElementNames[XMLConstants.PARAMETER]) &&
                  !nodeName.startsWith(XMLConstants.treeElementNames[XMLConstants.VALUES])){
                  nodeName += " = " + node.content();
      }

      
      if ( ((GlobalPanel)panel).getType().equals(SSS3dConstants.TYPES[SSS3dConstants.CELESTIALOBJECT_TYPE]) ) {
        ((InfoPanel)panel).setParameterText();
      }
      

      super.getTreeCellRendererComponent(
                      tree, nodeName, sel,
                      expanded, leaf, row,
                      hasFocus);

      return this;
  }

  /**
   * Returns an object attribute given it's node and the parameter
   *
   * @param node the node witch attribute it has to get
   * @param paramter the attribute parameter
   */
  private String getObjectAttribute(AdapterNode node, int parameter) {
    
    String str = null;
    
    NamedNodeMap nnm = node.domNode.getAttributes();
    
    Node n = nnm.getNamedItem(XMLConstants.treeElementNames[parameter]);
    if(n != null)
       str = n.getNodeValue();

    return str;
  }
}