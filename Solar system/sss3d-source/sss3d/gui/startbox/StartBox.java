 /* 
  File: StartBox.java 

  University of Applied Science Berne,HTA-Biel/Bienne,
  Computer Science Department.

  Diploma thesis J3D Solar System Simulator
  Originally written by Marcel Portner & Bernhard Hari (c) 2000
  
  CVS - Information :
  
  $Header: /var/cvsreps/projects/c450/2000/sss3d/source_diploma/sss3d/gui/startbox/StartBox.java,v 1.11 2000/12/13 13:41:13 portm Exp $
  $Author: portm $
  $Date: 2000/12/13 13:41:13 $
  $State: Exp $
*/

package sss3d.gui.startbox;

import sss3d.utils.xmlparser.*;
import sss3d.contentbranch.*;
import sss3d.gui.*;
import sss3d.SolarSystemSimulator;
import sss3d.utils.SSS3dConstants;

import org.w3c.dom.*;

import java.io.*;
import java.awt.*;
//import javax.vecmath.*;
import java.awt.event.*;
//import java.applet.*;
import javax.swing.*;
import javax.swing.event.*;


import java.io.IOException;


/**
 * Startbox creates a JFrame containing all setup information for the simulator.
 * Also, it allows to control the simulator by changing values. 
 *
 * @author Marcel Portner & Bernhard Hari
 * @version $Revision: 1.11 $
 */
public class StartBox extends JFrame implements Runnable, ActionListener {

    private Thread     thread;                  // startbox runs in a own thread
    
    //GUI
    private JTextField              pathField;
    private JPanel                  fileChooser;
    private JPanel                  panel;
    private JMenuBar                menubar   = null;
    private InitializationPanel     iniView   = null;
    private InitializationObject    iniObject = null;
    private JButton                 start;
    private JButton                 open;
    private JRadioButton            def;
    private JRadioButton            brw;
    private ButtonGroup             group;
    private String path = "";
    
    private SolarSystemSimulator sss;         // link to control simulation
    private Dimension            screenSize;
    
    private int widthFC  = 420;               // width File Chooser
    private int heightFC = 170;               // height File Chooser
    private int widthIV  = 800;               // width Initialization View
    private int heightIV = 540;               // height Initialization View
    
   /**
    * Creates a startbox. Called once at startup of solar system simulator.
    *
    * @param sss a reference to an instance of the solar system simulator 
    */
    public StartBox( SolarSystemSimulator sss ) {
      this.sss = sss;

      thread = new Thread(this, "StartBox");
      thread.start();
    }
    
    /**
     * Run method creates JFrame.
     */
    public void run() {

      this.setTitle("StartBox - J3D Solar System Simulator Version 1.0");
      this.setSize(widthFC,heightFC);
      this.addWindowListener(new WindowCmd());
      screenSize = Toolkit.getDefaultToolkit().getScreenSize();
      this.setLocation( (int)( screenSize.getWidth() / 2) - (widthFC / 2),
                        (int)( screenSize.getHeight()/ 2) - (heightFC/ 2));
      
      panel = new JPanel(new BorderLayout());
      createMenuBar();
      panel.add(menubar, BorderLayout.NORTH);  
      fileChooser = createFileChooserPanel();   
      panel.add(fileChooser, BorderLayout.CENTER);

      this.getContentPane().add(panel);
      
      // set default value for path to
      // $current_dir$/default.xml
      String userDir = null;
      try {
          userDir = System.getProperty("user.dir");
          userDir += System.getProperty("file.separator");
          userDir += "default.xml";
          path = userDir;
      } catch(Exception e) {
          // use default directory if 
          // an exception has been raised
      }  

      this.setVisible(true);

      open.requestFocus();
      open.setFocusPainted(true);
    }  
    
    /**
     * Creates the menubar for the infobox. 
     */
    private void createMenuBar() {

       if(menubar != null) menubar = null;
       
       // MenuBar
       menubar = new JMenuBar();
       JMenuItem mi;

       // File Menu
       JMenu file = (JMenu) menubar.add(new JMenu("File"));
       file.setMnemonic('F');

       mi = (JMenuItem) file.add(new JMenuItem("Exit"));
       mi.setMnemonic('x');
       mi.setActionCommand("exit");
       mi.addActionListener(this);
    }
     
    /**
     * Creates the file chooser panel.
     *
     * @return JPanel a new panel to select an initialization file
     */
    private JPanel createFileChooserPanel() {
        if (iniView != null) {
          this.setSize( new Dimension(widthFC,heightFC) );
          this.setLocation( (int)( screenSize.getWidth() / 2) - (widthFC / 2),
                            (int)( screenSize.getHeight()/ 2) - (heightFC/ 2));
        }
        
        def = new JRadioButton("",true);
        brw = new JRadioButton("",false);
        JLabel  defLabel = new JLabel("default.xml - in current directory");
              pathField  = new JTextField(path,255);
        open     = new JButton("Open");
        JButton browse   = new JButton("Browse...");
        
        pathField.setEditable(false);
        
        // Group the radio buttons.
        group = new ButtonGroup();
        group.add(def);
        group.add(brw);
        def.setActionCommand("rbDefault");
        brw.setActionCommand("rbBrowse");
        def.addActionListener(this);
        brw.addActionListener(this);
        
        // add actionlisteners
        open.setActionCommand("open");
        browse.setActionCommand("browse");
        open.addActionListener(this);
        browse.addActionListener(this);
        
        JPanel controls     = new JPanel();
        JPanel defaultPanel = new JPanel();
        JPanel browsePanel  = new JPanel();
        JPanel openPanel    = new JPanel();
        controls.setLayout(new BoxLayout(controls,BoxLayout.Y_AXIS));
        defaultPanel.setLayout(new BoxLayout(defaultPanel,BoxLayout.X_AXIS));
        browsePanel.setLayout(new BoxLayout(browsePanel,BoxLayout.X_AXIS));
        openPanel.setLayout(new BoxLayout(openPanel,BoxLayout.X_AXIS));
        
        defaultPanel.add(def);
        defaultPanel.add(defLabel);
        
        browsePanel.add(brw);
        browsePanel.add(pathField);
        browsePanel.add(browse);

        openPanel.add(open);

        defaultPanel.setAlignmentX(0.0f);
        browsePanel.setAlignmentX(0.0f);
        openPanel.setAlignmentX(0.0f);
        
        defaultPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        browsePanel.setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 5));
        openPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        
        controls.add(defaultPanel);
        controls.add(browsePanel);
        controls.add(openPanel);
        controls.setBorder(BorderFactory.createTitledBorder(
                                 "Choose initialization file"));
        return controls;
    }

    /**
     * Shows a FileChooser Dialog to select 
     * an initialization file.
     */
    private void openFileChooser() {
        String userDir = null;
        try {
          userDir = System.getProperty("user.dir");
        } catch(Exception e) {
          // use default directory if 
          // an exception has been raised
        }
        
        JFileChooser fc = new JFileChooser(userDir);
        fc.addChoosableFileFilter(new XMLFilter());
        
        int returnVal = fc.showOpenDialog(this);
        if(returnVal == JFileChooser.APPROVE_OPTION) {
           pathField.setText( fc.getSelectedFile().getAbsolutePath() );
        }
    }
    
    /**
     * Shows the initialization panel
     */
    private void createInitializationView() {
        boolean checked = false;
        
        // check if we can open the selected file
        checked = openXMLFile();

        // create view
        if (checked) {
        
          // set initialization object - observer pattern
          sss.setInitializationObject(iniObject);
          
          // clean old view
          this.remove(panel);
          fileChooser.setVisible(false);
          fileChooser = null;
          menubar.setVisible(false);
          menubar = null;
          panel.setVisible(false);
          panel = null;
        
          // resize startbox
          this.setSize( new Dimension(widthIV,heightIV) );
          this.setLocation( (int)( screenSize.getWidth() / 2) - (widthIV / 2),
                            (int)( screenSize.getHeight()/ 2) - (heightIV/ 2));
        
          // construct initialization view                   
          panel = new JPanel(new BorderLayout());
          createMenuBar();
          start = new JButton("Start");
          start.setActionCommand("start");
          start.addActionListener(this);
          iniView = new InitializationPanel( sss.getObjectsInformation(), SSS3dConstants.INITIALIZATION_MODE );
          panel.add(iniView,BorderLayout.CENTER);

          panel.add(menubar,BorderLayout.NORTH);
          
          panel.add(start,BorderLayout.SOUTH);
          
          this.getContentPane().add(panel);
          this.setVisible(true);
          
          start.requestFocus();
          start.setFocusPainted(true);

        } else {
            // show warning
        }
    }
    
    /**
     * Method handles all action events.
     */
     public void actionPerformed(ActionEvent e) {
          String s = e.getActionCommand();
          if(s.equals("exit")) {
             System.exit(0);
          } else if(s.equals("open")) {
              if ( brw.isSelected() ) {
                 path = pathField.getText();
              }
              if( !path.equals("") )
              createInitializationView();
          } else if(s.equals("browse")) {
              openFileChooser();
              pathField.setEditable(true);
              brw.setSelected(true);
          } else if(s.equals("rbDefault")) {
              pathField.setEditable(false);
              // set default value for path to
              // $current_dir$/default.xml
              String userDir = null;
              try {
                userDir = System.getProperty("user.dir");
                userDir += System.getProperty("file.separator");
                userDir += "default.xml";
                path = userDir;
              } catch(Exception ePath) {
               // use default directory if 
               // an exception has been raised
              }  
              pathField.setEditable(false);
              
          } else if(s.equals("rbBrowse")) {
              pathField.setEditable(true);
          } else if(s.equals("start")) {
              System.out.println("Start J3D - Solar System Simulator");
              this.setVisible(false);
              InitializationObject iniObject = sss.getObjectsInformation().getInitializationObject();
              boolean glasses3d = ((Boolean)iniObject.getParameter(XMLConstants.GLASSES3D)).booleanValue();
              if(glasses3d) {
                 sss.createStereoScene();
              } else {
                 sss.createScene();
              }
          }
     }

    /**
     * Open XML File.
     * 
     * @return boolean true if the program could open the specified XML file.
     */
    private boolean openXMLFile() {
      boolean opened = false;
      
      File f = new File(path);
      if ( f.exists() ) {
        iniObject = new InitializationObject(path);
        if(iniObject != null) opened = true;
        System.out.println(path);
      }

      return opened;
    }

    /**
     * Window Listener for the InfoBox Frame.
     */
    class WindowCmd extends WindowAdapter {
       public void windowClosing(WindowEvent e) {
          System.exit(0);
       }
    }
}