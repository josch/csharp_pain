/*
  File: SmallInfoPanel.java

  University of Applied Science Berne,HTA-Biel/Bienne,
  Computer Science Department.

  Diploma thesis J3D Solar System Simulator
  Originally written by Marcel Portner & Bernhard Hari (c) 2000

  CVS - Information :

  $Header: /var/cvsreps/projects/c450/2000/sss3d/source_diploma/sss3d/gui/SmallInfoPanel.java,v 1.6 2000/12/13 13:45:56 portm Exp $
  $Author: portm $
  $Date: 2000/12/13 13:45:56 $
  $State: Exp $

*/
package sss3d.gui;

import sss3d.utils.SSS3dConstants;
import sss3d.calculations.TimeCalc;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.event.*;

/**
 * Shows a small info panel at the bottom of the solar system simulator
 *   
 * @author Marcel Portner & Bernhard Hari
 * @version $Revision: 1.6 $
 */
public class SmallInfoPanel extends JPanel implements Runnable {

   private static final int DIGITSAFTERCOMA = 4;
   private JLabel navigation, factor, date;
   private boolean mode = true;
   private double julianDate;
   private double dateStep;
   private Thread thread;
   private boolean runDate = true;
   private TimeCalc timeCalc;

   /**
    * Creates a small info panel
    */
   public SmallInfoPanel() {
      super();
      timeCalc = new TimeCalc();
      
      // create GUI
      setLayout(new FlowLayout(FlowLayout.RIGHT, 15, 0));
    
      navigation = new JLabel("Move");
      this.add(navigation);
      
      date = new JLabel("Date:");
      this.add(date);
      
      factor = new JLabel("Step Factor: F3");
      this.add(factor);
  
      this.setVisible(true); 
  
      thread = new Thread(this, "InfoPanel");
      thread.start();
   }
    
   /**
    * Sets the navigation mode to Turn or Move
    */ 
   public void setNavigationMode() {
      if(mode) {
         navigation.setText("Turn");
      } else {
         navigation.setText("Move");
      }
      mode = !mode;
   }
   
   /**
    * Sets the step factor to F1/ F2/ F3 or F4
    *
    * @param stepFactor the new step factor
    */
   public void setFactor(int stepFactor) {
      if(stepFactor == 1) {
         factor.setText("Step Factor: F1");
      } else if(stepFactor == 2) {
         factor.setText("Step Factor: F2");
      } else if(stepFactor == 3) {
         factor.setText("Step Factor: F3");
      } else if(stepFactor == 4) {
         factor.setText("Step Factor: F4");
      }
   }
   
   /**
    * Sets the gregorian date an julian date.
    *
    * @param julianDate the new date
    */
   public void setDate(double julianDate) {
      this.julianDate = julianDate;
      int gregorianDate [] = timeCalc.calDat(julianDate - 2400001.0);
      
      String text = new String("Date: ");
      if( gregorianDate[2] < 10 ) text += "0";
      text += gregorianDate[2] + ". "; // day
      if( gregorianDate[1] < 10 ) text += "0";
      text += gregorianDate[1] + ". "; // month
      text += gregorianDate[0] + " ";  // year
      if( gregorianDate[3] < 10 ) text += "0";
      text += gregorianDate[3] + ":";  // hour
      if( gregorianDate[4] < 10 ) text += "0";
      text += gregorianDate[4] + " ";  // minute
      
      // not necessary - time calc returns always 0 for second
      /*if( gregorianDate[5] < 10 ) text += "0";
      text += gregorianDate[5] + "  "; // second
      */

      String jdate = " JD: ";
      jdate += Double.toString(julianDate);
      int pos = jdate.indexOf(".");
      if(jdate.length() - pos > DIGITSAFTERCOMA) {
         text += jdate.substring(0, pos + DIGITSAFTERCOMA);
      } else {
         text += jdate;
      }
      
      date.setText(text);
   }
   
   /**
    * Sets increasing time step.
    *
    * @param step - the time step
    */
   public void setStep(double step) {
      dateStep = step;
   }
   
   /**
    * Stops the date display from updating
    */
   public void stopDate() {
      runDate = false;
   }
   
   /** 
    * Starts the date display to update itself
    */
   public void startDate() {
      runDate = true;
   }
   
   /**
    * Run method. Updates the date after an elapsed time frame.
    */
   public void run() {
      while(true) {
         if(runDate) {
            try {
               Thread.sleep(100);
            } catch(InterruptedException ie) { }
            julianDate += dateStep / 10;
            setDate(julianDate);
         }
      }
   }
}
