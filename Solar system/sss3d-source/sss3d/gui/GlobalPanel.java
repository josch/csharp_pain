/*
  File: GlobalPanel.java

  University of Applied Science Berne,HTA-Biel/Bienne,
  Computer Science Department.

  Diploma thesis J3D Solar System Simulator
  Originally written by Marcel Portner & Bernhard Hari (c) 2000
  
  CVS - Information :
  
  $Header: /var/cvsreps/projects/c450/2000/sss3d/source_diploma/sss3d/gui/GlobalPanel.java,v 1.1 2000/11/17 15:56:38 harib Exp $
  $Author: harib $
  $Date: 2000/11/17 15:56:38 $
  $State: Exp $
  
*/
package sss3d.gui;

/**
 * This interfaces defines the methods that every panel
 * object has to implement. 
 * At the moment only InitializationPanel & InfoPanel
 *
 * @author Marcel Portner & Bernhard Hari
 * @version $Revision: 1.1 $
 */

public interface GlobalPanel {
	
   /**
    * Returns the type of the panel
    *
    * @return String type of the panel.
    */
    public String getType();
    
   /**
    * Returns the document of this panel.
    *
    * @return org.w3c.dom.Document document.
    */
    public org.w3c.dom.Document getDocument();    

}