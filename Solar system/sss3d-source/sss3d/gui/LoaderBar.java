/*
  File: LoaderBar.java

  University of Applied Science Berne,HTA-Biel/Bienne,
  Computer Science Department.

  Diploma thesis J3D Solar System Simulator
  Originally written by Marcel Portner & Bernhard Hari (c) 2000
  
  CVS - Information :
  
  $Header: /var/cvsreps/projects/c450/2000/sss3d/source_diploma/sss3d/gui/LoaderBar.java,v 1.6 2000/12/15 02:46:52 harib Exp $
  $Author: harib $
  $Date: 2000/12/15 02:46:52 $
  $State: Exp $
  
*/
package sss3d.gui;

import sss3d.SolarSystemSimulator;

import java.awt.*;

/**
 * This class describe a pseudo progress bar in a own thread.
 *
 * @author Marcel Portner & Bernhard Hari
 * @version $Revision: 1.6 $
 */
public class LoaderBar extends Window implements Runnable {

   private int max;
   private String cname;
   private int value;
   private Image logo;

   /**
    * Initializes a new LoaderBar.
    *
    * @param max  the maximum of the loaded objects.
    * @param sss3d  the parent Frame.
    */
   public LoaderBar(int max, Frame sss3d) {
      super(sss3d);
      this.max = max - 1;
      Thread thread = new Thread(this, "ProgressBar");
      thread.start();
   }

   /**
    * The new LoaderBar thread.
    */
   public void run() {
      this.setBackground(Color.lightGray);

      this.setLayout(new BorderLayout());

      Label currentName = new Label("Start", Label.CENTER);
      Label currentNr = new Label("0 of " + max, Label.CENTER);
      
      Panel panel = new Panel();
      panel.setLayout(new GridLayout(3, 1));
      panel.add(new Label("Celestial object", Label.CENTER));
      panel.add(currentName);
      panel.add(currentNr);
      //Logo logo = new Logo();
      //this.add(logo, BorderLayout.CENTER);
      this.add(panel, BorderLayout.CENTER);
      this.pack();
      Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
      Dimension frameSize = this.getSize();
      if(frameSize.height > screenSize.height) {
         frameSize.height = screenSize.height;
      }
      if(frameSize.width > screenSize.width) {
         frameSize.width = screenSize.width;
      }
      this.setLocation((screenSize.width  - frameSize.width)  / 2,
                       (screenSize.height - frameSize.height) / 2);
      this.setVisible(true);
      while(value < max) {
         try {
            Thread.sleep(100);
         } catch(InterruptedException ie) { }
         currentName.setText(cname);
         currentNr.setText(value + " of " + max);
         this.repaint();
      }
   }

   /**
    * Set the actual loading number.
    *
    * @param i  the actual loading number.
    */
   public void setCount(int i) {
      value = i;
   }

   /**
    * Set the actual loading object name.
    *
    * @param name the actual loading object name.
    */
   public void setNote(String name) {
      cname = name;
   }
}

class Logo extends Canvas {

   private Image logo;
   private MediaTracker tracker;
   
   public Logo() {
   
      logo = Toolkit.getDefaultToolkit().getImage("images//sss3dlogo.jpg");
      tracker = new MediaTracker(this);

      tracker.addImage(logo, 0);
      try{
         tracker.waitForID(0);
         
      } catch(InterruptedException e) {
         System.out.println(e.toString());
      }
      requestFocus();
      repaint(); 
   }
   
   public void update(Graphics g){
   paint(g);
  }
   
   public void paint(Graphics g) {
      try {tracker.waitForID(0);}catch (InterruptedException e){};
      g.drawImage(logo, 0, 0, this);
   }
 
   public Dimension getMinimumSize() {
      return new Dimension(120, 120);
   }
 
   public Dimension getPreferredSize() {
      return getMinimumSize();
   }
}
