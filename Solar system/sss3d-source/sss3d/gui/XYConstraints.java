/*
  File: XYConstraints.java

  University of Applied Science Berne,HTA-Biel/Bienne,
  Computer Science Department.

  Diploma thesis J3D Solar System Simulator
  Originally written by Marcel Portner & Bernhard Hari (c) 2000

  CVS - Information :

  $Header: /var/cvsreps/projects/c450/2000/sss3d/source_diploma/sss3d/gui/XYConstraints.java,v 1.4 2000/12/13 13:45:16 portm Exp $
  $Author: portm $
  $Date: 2000/12/13 13:45:16 $
  $State: Exp $

*/

package sss3d.gui;

import java.io.Serializable;

/**
 * The XYConstraints object is the constraint object created when a component
 * is added to an XYLayout . The constraints are the x and y coordinates that
 * position the upper-left corner of the control relative to the container, and
 * the width and height of the control.
 *   
 * @author Marcel Portner & Bernhard Hari
 * @version $Revision: 1.4 $
 */
public class XYConstraints implements Cloneable, Serializable {

   private int x;
   private int y;
   private int width;
   private int height;

  /**
   * Constructs a XYConstraints object with x, y, width, and height set to zero.
   */
   public XYConstraints() {
      this(0, 0, 0, 0);
   }

  /**
   * Constructs a XYConstraints object using the constraints passed to this constructor.
   *
   * @param x      The x coordinate that specifies the number of pixels from the left edge of the container.
   * @param y      The y coordinate that specifies the number of pixels from the top edge of the container.
   * @param width  The width of the control added to the container in pixels. A value less than or equal to 0 means that the control's preferred width is used.
   * @param height The height of the control added to the container in pixels. A value less than or equal to 0 means that the control's preferred height is used.
   */
   public XYConstraints(int x, int y, int width, int height) {
      this.x      = x;
      this.y      = y;
      this.width  = width;
      this.height = height;
   }

   /**
    * Returns the distancce from the left side of the container in pixels.
    *
    * @return int the distance x
    */
   public int getX() {
      return x;
   }

   /**
    * Stores the distance from the left side of the container in pixels. 
    *
    * @param x the new value for x
    */
   public void setX(int x) {
      this.x = x;
   }

   /**
    * Returns the distancce from the top of the container in pixels.
    *
    * @return int the distance y
    */
   public int getY() {
      return y;
   }
   
   /**
    * Stores the distance from the top of the container in pixels. 
    *
    * @param y the new value for y
    */
   public void setY(int y) {
      this.y = y;
   }
   
   /**
    * Returns the width of the control in pixels.
    *
    * @return int the currently used width
    */
   public int getWidth() {
      return width;
   }
   
   /**
    * Stores the width of the control in pixels. The width of the control added to the 
    * container in pixels. A value less than or equal to 0 means that the control's
    * preferred width is used. 
    */
   public void setWidth(int width) {
      this.width = width;
   }

   /**
    * Returns the height of the control in pixels.
    *
    * @return int the currently used height
    */
   public int getHeight() {
      return height;
   }

   /**
    * Stores the height of the control in pixels. The height of the control added to the
    * container in pixels. A value less than or equal to 0 means that the control's 
    * preferred height is used. 
    *
    * @param int the new value for height
    */
   public void setHeight(int height) {
      this.height = height;
   }

   /**
    * Returns the hashcode for this XYConstraints.
    *
    * @return int the hashcode
    */
   public int hashCode() {
      return x ^ y * 37 ^ width * 43 ^ height * 47;
   }

   /**
    * Compares another XYConstraints object to this one.
    * equals() returns true if the objects are equal; otherwise, equals() returns false.
    * 
    * @param obj a XYConstraints object
    * @return boolean true if objects are equal, otherwise false
    */
   public boolean equals(Object obj) {
      if(obj instanceof XYConstraints) {
         XYConstraints xyconstraints = (XYConstraints)obj;
         return xyconstraints.x == x &&
                xyconstraints.y == y &&
                xyconstraints.width == width &&
                xyconstraints.height == height;
      } else {
         return false;
      }
   }

   /**
    * Creates a copy of this XYConstraints object. The
    * constraints are identical in both XYConstraints objects.
    *
    * @return Object a copy of this XYConstraints object
    */
   public Object clone() {
      return new XYConstraints(x, y, width, height);
   }

   /**
    * Returns a string representation ot this XYConstraints object.
    *
    * @return String a string representation of this object
    */
   public String toString() {
      return "XYConstraints[" + x + "," + y + "," + width + "," + height + "]";
   }
}
