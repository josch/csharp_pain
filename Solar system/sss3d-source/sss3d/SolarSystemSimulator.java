/*
  File: SolarSystemSimulator.java

  University of Applied Science Berne,HTA-Biel/Bienne,
  Computer Science Department.

  Diploma thesis J3D Solar System Simulator
  Originally written by Marcel Portner & Bernhard Hari (c) 2000
  
  CVS - Information :
  
  $Header: /var/cvsreps/projects/c450/2000/sss3d/source_diploma/sss3d/SolarSystemSimulator.java,v 1.21 2000/12/15 02:47:29 portm Exp $
  $Author: portm $
  $Date: 2000/12/15 02:47:29 $
  $State: Exp $
  
*/
package sss3d;

import sss3d.gui.*;
import sss3d.gui.infobox.*;
import sss3d.gui.startbox.*;
import sss3d.gui.navigationbox.*;
import sss3d.utils.CapturingCanvas3D;
import sss3d.utils.observer.*;
import sss3d.utils.xmlparser.XMLConstants;
import sss3d.viewbranch.*;
import sss3d.contentbranch.*;
import sss3d.utils.joystick.*; // to add joystick functionality
import sss3d.contentbranch.rockets.SceneRocket; 

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.media.j3d.*;
import javax.vecmath.Point3d;

/**
 * Main Programm.<br>
 *
 * @author Marcel Portner & Bernhard Hari
 * @version $Revision: 1.21 $
 */
public class SolarSystemSimulator extends JFrame {
    
   public  InfoBox            lnkInfoBox;
   private NavigationBox      navigationBox;
   private ObjectsPositions   objPos  = null;
   private ObjectsInformation objInfo = null;
   private StartBox startbox;
    
   private InitializationObject iniObject;
   public  ViewBranch lnkViewBranch;
   public  SceneSolarSystem lnkSceneSolarSystem;
   public  SmallInfoPanel infoPanel;
   public  CapturingCanvas3D canvas;
   
   private Locale locale;
   private GraphicsConfiguration config;
   private Dimension screenSize;

   /**
    *  Default constructor.
    *  Builds up the whole solar system simulator.
    */
   public SolarSystemSimulator() {
      super("J3D Solar System Simulator Version 1.0");

      String vers = System.getProperty("java.version");
      if (vers.compareTo("1.3") < 0) {
         System.out.println("!!!WARNING: Use JDK version 1.3 or higher!!!");
         System.exit(0);
      }

      screenSize = Toolkit.getDefaultToolkit().getScreenSize();      

      this.setVisible(false);
      this.addWindowListener(new WindowCmd());

      // Force SwingApplet to come up in the System L&F
      String laf = UIManager.getSystemLookAndFeelClassName();
      try {
	      UIManager.setLookAndFeel(laf);
	      // If you want the Cross Platform L&F instead, comment out the above line and
	      // uncomment the following:
	      // UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
      } catch (UnsupportedLookAndFeelException exc) {
	       System.err.println("Warning: UnsupportedLookAndFeel: " + laf);
      } catch (Exception exc) {
	       System.err.println("Error loading " + laf + ": " + exc);
      }

      GraphicsConfigTemplate3D tmpl = new GraphicsConfigTemplate3D();
      GraphicsEnvironment env = GraphicsEnvironment.getLocalGraphicsEnvironment();
      GraphicsDevice device = env.getDefaultScreenDevice();
      config = device.getBestConfiguration(tmpl);

      startbox = new StartBox(this);
   }

   /**
    * Sets the instance of the InitializationObject.
    * It also creates a new instance of the ObjectsPosition and
    * ObjectsInformation ( Observer Pattern ).
    *
    * @param iniObject an instance of an InitializationObject
    */
   public void setInitializationObject(InitializationObject iniObject) {
     this.iniObject = iniObject; 
     objPos  = new ObjectsPositions( iniObject );
     objInfo = new ObjectsInformation( iniObject );
   }
   
   /**
    * Returns the object of the initialization object.
    *
    * @returns InitializationObject a reference to the initialization object
    */
   public InitializationObject getInitializationObject() {
     return iniObject;
   } 
   
   /**
    * Returns the instance of the information observer.
    *
    * @returns ObjectsInformation a reference to the ObjectsInformation object.
    */
   public ObjectsInformation getObjectsInformation() {
     return objInfo;
   }
   
   /**
    * Creates the solar system 3D scene.
    */
   public void createScene() {
      this.setSize(screenSize.width - 200, screenSize.height - 150);
      this.setLocation((screenSize.width  - this.getWidth())  / 2,
                       (screenSize.height - this.getHeight()) / 2);
      
      canvas = new CapturingCanvas3D(config);  // The used Canvas3D
      this.getContentPane().setLayout(new BorderLayout());
      this.getContentPane().add(canvas, BorderLayout.CENTER);

      infoPanel = new SmallInfoPanel();
      double date = ((Double)objInfo.getParameter(XMLConstants.JDAY)).doubleValue();
      infoPanel.setDate(date);
      AnimationSpeed animationSpeed = (AnimationSpeed)objInfo.getParameter(XMLConstants.ANIMATIONSPEED);
      int factor = 0;
      switch(animationSpeed.getType()) {
        case AnimationSpeed.DAYS_PER_SECOND:
          factor = 1;
          break;
        case AnimationSpeed.HOURS_PER_SECOND:
          factor = 24;
          break;
        case AnimationSpeed.MINUTES_PER_SECOND:
          factor = 24 * 60;
          break;
        default:
      }
      int animSpeed = animationSpeed.getValue();
      infoPanel.setStep((double)animSpeed / (double)factor);
      this.getContentPane().add(infoPanel, BorderLayout.SOUTH);
      
      // Create a virtual universe.
      VirtualUniverse universe = new VirtualUniverse();

      // A single hi-res. Locale node is created and attached to 
      // the virtual universe.
      locale = new Locale(universe);
      
      // Necessary to use NewTextureLoader in other classes.
      NewTextureLoader.setImageObserver(this);  // AWT Component

      lnkSceneSolarSystem = new SceneSolarSystem(canvas, objPos, objInfo, this);

      BranchGroup scene = lnkSceneSolarSystem.createSceneGraph();

      // The ViewBranch class creates the instances of ViewPlatform,
      // View, etc.
      lnkViewBranch = new ViewBranch(this, canvas, null);
      BranchGroup view = lnkViewBranch.myViewBranch();

      lnkInfoBox = new InfoBox(this, objPos, objInfo);
      navigationBox = new NavigationBox(this);

      this.setVisible(true);
      
      // initialize joystick if available
      if(((Boolean)objInfo.getParameter(XMLConstants.JOYSTICK)).booleanValue()) {
         JoystickInputDevice joyDevice = new JoystickInputDevice(0);
         if(joyDevice.initialize()) {
            lnkViewBranch.getPhysicalEnvironment().addInputDevice(joyDevice);
 			
            //set joystick enable to spaceshuttle only
            SceneRocket sceneRocket = (SceneRocket) lnkSceneSolarSystem.getCelestial("spaceshuttle");
           
            Sensor joy1 = joyDevice.getSensor(1);
            SensorBehavior s = new SensorBehavior(sceneRocket.getTransformGroup(), joy1);
            s.setSchedulingBounds(new BoundingSphere(new Point3d(0.0, 0.0, 0.0), Float.MAX_VALUE));
         
            BranchGroup joystick = new BranchGroup();
            joystick.addChild(s);
            joystick.compile();
       
            locale.addBranchGraph(joystick);
         }
      }
      locale.addBranchGraph(view);	
      locale.addBranchGraph(scene);
   }

   /**
    * Creates the solar system 3D scene.
    * Used if simulator is started with 3D Glasses.
    */
   public void createStereoScene() {
      this.setSize(screenSize.width, screenSize.height);
      this.setLocation(0, 0);

      Canvas3D canvasL = new Canvas3D(config);
      Canvas3D canvasR = new Canvas3D(config);

      canvasL.setMonoscopicViewPolicy(View.LEFT_EYE_VIEW);
      canvasR.setMonoscopicViewPolicy(View.RIGHT_EYE_VIEW);

      canvasL.setStereoEnable(true);
      canvasR.setStereoEnable(true);
/*      
      Point3d pointL = new Point3d(0.0142, 0.135, 0.4572);
      Point3d pointR = new Point3d(0.0208, 0.135, 0.4572);
      canvasL.setLeftManualEyeInImagePlate(pointL);
      canvasR.setRightManualEyeInImagePlate(pointR);
*/
      Screen3D screenL = canvasL.getScreen3D();
      Screen3D screenR = canvasR.getScreen3D();
      
      screenL.setPhysicalScreenHeight((screenL.getPhysicalScreenHeight() * 1.5));
      screenR.setPhysicalScreenHeight((screenR.getPhysicalScreenHeight() * 1.5));

      Glasses3DLayout layout = new Glasses3DLayout(screenSize.width, screenSize.height);

      this.getContentPane().setBackground(Color.black);
      this.getContentPane().setLayout(layout);
      this.getContentPane().add(canvasL, layout.getLeftPanel());
      this.getContentPane().add(canvasR, layout.getRightPanel());

      // Create a virtual universe.
      VirtualUniverse universe = new VirtualUniverse();

      // A single hi-res. Locale node is created and attached to 
      // the virtual universe.
      Locale locale = new Locale(universe);
      
      // Necessary to use NewTextureLoader in other classes.
      NewTextureLoader.setImageObserver(this);  // AWT Component

      lnkSceneSolarSystem = new SceneSolarSystem(canvasL, objPos, objInfo, this);

      locale.addBranchGraph(lnkSceneSolarSystem.createSceneGraph());

      // The ViewBranch class creates the instances of ViewPlatform,
      // View, etc.
      lnkViewBranch = new ViewBranch(this, canvasL, canvasR);

      locale.addBranchGraph(lnkViewBranch.myViewBranch());

      this.setVisible(true);
      this.setResizable(false);
   }

   /**
    * Adds the navigation panel to the right side of the simulator.
    */
   public void addNavigation() {
      this.getContentPane().add(navigationBox, BorderLayout.EAST);
      this.setVisible(true);
   }
   
   /**
    * Removes the navigation panel to the right side of the simulator.
    */   
   public void removeNavigation() {
      this.getContentPane().remove(navigationBox);
      this.setVisible(true);
   }

   /**
    * Removes the scene.
    *
    * @param scene a reference to the BranchGroup that has to be removed
    */
   public void removeScene(BranchGroup scene) {
      locale.removeBranchGraph(scene);
   }

   /**
    * Adds a new scene.
    *
    * @param scene a reference to the BranchGroup that has to be added
    */

   public void addScene(BranchGroup scene) {    
      locale.addBranchGraph(scene);     
   }

   /**
    * Window Listener for the SolarSystemSimulator JFrame.
    */
   class WindowCmd extends WindowAdapter {
      public void windowClosing(WindowEvent e) {
         System.exit(0);
      }
   }
    
   /**
    *  Allows SolarSystemSimulator to be run as an application.
    *  @param java.lang.String args[]
    */
   public static void main(String args[]) {
      System.out.println("\nSolarSystemSimulator copyright Bernhard Hari & Marcel Portner");
      new SolarSystemSimulator();
   }
}