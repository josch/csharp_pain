/*
  File: FollowCelestialObject.java

  University of Applied Science Berne,HTA-Biel/Bienne,
  Computer Science Department.

  Diploma thesis J3D Solar System Simulator
  Originally written by Marcel Portner & Bernhard Hari (c) 2000
  
  CVS - Information :
  
  $Header: /var/cvsreps/projects/c450/2000/sss3d/source_diploma/sss3d/viewbranch/FollowCelestialObject.java,v 1.11 2000/12/13 13:43:41 portm Exp $
  $Author: portm $
  $Date: 2000/12/13 13:43:41 $
  $State: Exp $
  
*/
package sss3d.viewbranch;

import sss3d.SolarSystemSimulator;
import sss3d.utils.SSS3dConstants;
import sss3d.utils.xmlparser.XMLConstants;

import java.util.Enumeration;
import javax.vecmath.*;
import javax.media.j3d.*;

/**
 * This class follow a celestial object.
 *
 * @author Marcel Portner & Bernhard Hari
 * @version $Revision: 1.11 $
 */
public class FollowCelestialObject extends Behavior {

   /**
    * The TransformGroup node to follow the celestial object.
    */
   private TransformGroup new_tgView;
   
   /**
    * The reference to the main class.
    */
   private SolarSystemSimulator sss3d;

   /**
    * Wake up event for each new frame.
    */
   private WakeupOnElapsedFrames wakeUp = new WakeupOnElapsedFrames(0);

   /**
    * Constructor that allows to specify the desired target transform group.
    *
    * @param targetTG  the target transform group
    * @param sss3d  reference to the main class
    */
   public FollowCelestialObject(TransformGroup targetTG, SolarSystemSimulator sss3d) {
      new_tgView = targetTG;
      this.sss3d = sss3d;
   }

   /**
    * Override Behavior's initialize method to setup wakeup criteria.
    */
   public void initialize() {
      wakeupOn(wakeUp);
   }

   /**
    * Override Behavior's stimulus method to handle the event.
    * This method is called for each new frame and
    * operates on the specified transform group to follow an object with the camera.
    *
    * @param criteria  an enumeration of triggered wakeup criteria for this behavior.
    */
   public void processStimulus(Enumeration criteria) {

      if(criteria.hasMoreElements()) {
         Transform3D followObject = sss3d.lnkSceneSolarSystem.getCurrentPosition();

         boolean follow = !((Boolean)sss3d.getInitializationObject().getParameter(XMLConstants.CAMERAATORIGIN)).booleanValue();
         boolean compressed = ((Boolean)sss3d.getInitializationObject().getParameter(XMLConstants.COMPRESSED)).booleanValue();

         // Default camera position
         if(followObject == null) {
            // rotate about 270 degree at the X - axis
            Matrix3d rot = new Matrix3d();
            rot.rotX(Math.toRadians(270));
         
            Vector3d trans;
            if(compressed) {
               trans = new Vector3d(0.0, 70.0, 0.0);
            } else {
               trans = new Vector3d(0.0, 1000.0, 0.0);
            }
            followObject = new Transform3D(rot, trans, 1.0);
         } else if(follow) {
            // Give an offset.
            Vector3d offset = new Vector3d();

            int camera = ((Integer)sss3d.getInitializationObject().getParameter(XMLConstants.CAMERA)).intValue();
   
            switch(camera) {
              case SSS3dConstants.CAMERA_SUN :
                  followObject.get(offset);
                  if(compressed) {
                     offset.add(new Vector3d(0.0, 0.0, 11.0));
                  } else {
                     offset.add(new Vector3d(0.0, 0.0, 500.0));
                  }
                  followObject.set(offset);
                  break;
              case SSS3dConstants.CAMERA_MERCURY :
                  followObject.get(offset);
                  if(compressed) {
                     offset.add(new Vector3d(0.0, 0.0, 2.0));
                  } else {
                     offset.add(new Vector3d(0.0, 0.0, 5.0));
                  }
                  followObject.set(offset);
                  break;
              case SSS3dConstants.CAMERA_VENUS :
                  followObject.get(offset);
                  if(compressed) {
                     offset.add(new Vector3d(0.0, 0.0, 2.0));
                  } else {
                     offset.add(new Vector3d(0.0, 0.0, 9.0));
                  }
                  followObject.set(offset);
                  break;
              case SSS3dConstants.CAMERA_EARTH :
                  followObject.get(offset);
                  if(compressed) {
                     offset.add(new Vector3d(0.0, 0.0, 2.0));
                  } else {
                     offset.add(new Vector3d(0.0, 0.0, 10.0));
                  }
                  followObject.set(offset);
                  break;
              case SSS3dConstants.CAMERA_MARS :
                  followObject.get(offset);
                  if(compressed) {
                     offset.add(new Vector3d(0.0, 0.0, 2.0));
                  } else {
                     offset.add(new Vector3d(0.0, 0.0, 9.0));
                  }
                  followObject.set(offset);
                  break;
              case SSS3dConstants.CAMERA_JUPITER :
                  followObject.get(offset);
                  if(compressed) {
                     offset.add(new Vector3d(0.0, 0.0, 8.0));
                  } else {
                     offset.add(new Vector3d(0.0, 0.0, 40.0));
                  }
                  followObject.set(offset);
                  break;
              case SSS3dConstants.CAMERA_SATURN :
                  followObject.get(offset);
                  if(compressed) {
                     offset.add(new Vector3d(0.0, 0.0, 9.0));
                  } else {
                     offset.add(new Vector3d(0.0, 0.0, 45.0));
                  }
                  followObject.set(offset);
                  break;
              case SSS3dConstants.CAMERA_URANUS :
                  followObject.get(offset);
                  if(compressed) {
                     offset.add(new Vector3d(0.0, 0.0, 2.0));
                  } else {
                     offset.add(new Vector3d(0.0, 0.0, 25.0));
                  }
                  followObject.set(offset);
                  break;
              case SSS3dConstants.CAMERA_NEPTUNE :
                  followObject.get(offset);
                  if(compressed) {
                     offset.add(new Vector3d(0.0, 0.0, 2.0));
                  } else {
                     offset.add(new Vector3d(0.0, 0.0, 25.0));
                  }
                  followObject.set(offset);
                  break;
              case SSS3dConstants.CAMERA_PLUTO :
                  followObject.get(offset);
                  if(compressed) {
                     offset.add(new Vector3d(0.0, 0.0, 2.0));
                  } else {
                     offset.add(new Vector3d(0.0, 0.0, 5.0));
                  }
                  followObject.set(offset);
                  break;
              case SSS3dConstants.CAMERA_MOON :
                  followObject.get(offset);
                  if(compressed) {
                     offset.add(new Vector3d(0.0, 0.0, 2.0));
                  } else {
                     offset.add(new Vector3d(0.0, 0.0, 5.0));
                  }
                  followObject.set(offset);
                  break;
              case SSS3dConstants.CAMERA_SPACESHUTTLE :
                  Matrix3d m3dRot = new Matrix3d();
                  Vector3d v3dTemp = new Vector3d();
                  followObject.get(m3dRot);
                  followObject.get(offset);
                  if(compressed) {
                     m3dRot.transform(new Vector3d(0.0, 0.02, 0.1), v3dTemp);
                  } else {
                     m3dRot.transform(new Vector3d(0.0, 0.2, 1.0), v3dTemp);
                  }
                  offset.add(v3dTemp);
                  followObject.setTranslation(offset);
                  break;
              default :
                  followObject.get(offset);
                  if(compressed) {
                     offset.add(new Vector3d(0.0, 0.0, 2.0));
                  } else {
                     offset.add(new Vector3d(0.0, 0.0, 5.0));
                  }
                  followObject.set(offset);
                  break;
            }
         }
         new_tgView.setTransform(followObject);
      }

      // Set wakeup criteria for next time.
      wakeupOn(wakeUp);
   }
}
