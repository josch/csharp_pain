/*
  File: ViewBranch.java

  University of Applied Science Berne,HTA-Biel/Bienne,
  Computer Science Department.

  Diploma thesis J3D Solar System Simulator
  Originally written by Marcel Portner & Bernhard Hari (c) 2000
  
  CVS - Information :
  
  $Header: /var/cvsreps/projects/c450/2000/sss3d/source_diploma/sss3d/viewbranch/ViewBranch.java,v 1.13 2000/12/13 13:44:36 portm Exp $
  $Author: portm $
  $Date: 2000/12/13 13:44:36 $
  $State: Exp $
  
*/
package sss3d.viewbranch;

import sss3d.utils.SSS3dConstants;
import sss3d.SolarSystemSimulator;
import sss3d.utils.xmlparser.XMLConstants;

import javax.media.j3d.*;
import javax.vecmath.*;
import com.sun.j3d.utils.behaviors.keyboard.*;

/**
 * This class creates all necessary objects on the "View Branch"
 * side of the scene graph.
 *
 * @author Marcel Portner & Bernhard Hari
 * @version $Revision: 1.13 $
 */
public class ViewBranch extends Object {

   private Canvas3D canvasL;
   private Canvas3D canvasR;
   private SolarSystemSimulator sss3d;
   private PhysicalEnvironment environment;
   
   private TransformGroup vpTrGrKey;

   private KeyNavigation keyNav;

   /**
    * Initializes a new ViewBranch that allows to specify the desired Canvas3D.
    *
    * @param sss3d  reference to the main class
    * @param canvasL  the Canvas3D being used for the left eye or the default
    * @param canvasR  the Canvas3D being used for the right eye or null
    */
   public ViewBranch(SolarSystemSimulator sss3d, Canvas3D canvasL, Canvas3D canvasR) {
      this.canvasL = canvasL;
      this.canvasR = canvasR;
      this.sss3d = sss3d;
   }

   /**
    * Give the physical environment.
    *
    * @return the physical environment.
    */
   public PhysicalEnvironment getPhysicalEnvironment() {
     return environment;
   }

   /**
    * Create the ViewBranch.
    *
    * @return the root of the ViewBranch.
    */
   public BranchGroup myViewBranch() {
      // Create the minimal PhysicalBody and PhysicalEnvironnement
      // instances with default parameters.
      PhysicalBody body = new PhysicalBody();
      environment = new PhysicalEnvironment();

      // Create a View instance and attach the Canvas3D, the PhysicalBody
      // and the PhysicalEnvironment to it.
      View view = new View();
      view.addCanvas3D(canvasL);
      if(canvasR != null) {
         view.addCanvas3D(canvasR);
      }
      view.setPhysicalBody(body);
      view.setPhysicalEnvironment(environment);
      
      /*
      //PHYSICAL_EYE, PHYSICAL_SCREEN, VIRTUAL_EYE, or VIRTUAL_SCREEN
      view.setFrontClipPolicy(View.PHYSICAL_SCREEN);
      view.setBackClipPolicy(View.PHYSICAL_SCREEN);
      */
      // set back and front clipping for different views
      if ( ((Boolean)sss3d.getObjectsInformation().getParameter(XMLConstants.COMPRESSED)).booleanValue() ) {
          view.setFrontClipDistance(SSS3dConstants.FRONTCLIP_COMPRESSED);
          view.setBackClipDistance(SSS3dConstants.BACKCLIP_COMPRESSED);
      } else  {
          view.setFrontClipDistance(SSS3dConstants.FRONTCLIP);
          view.setBackClipDistance(SSS3dConstants.BACKCLIP);
      }
      /*
      // RELATIVE_TO_SCREEN, RELATIVE_TO_WINDOW, RELATIVE_TO_FIELD_OF_VIEW, RELATIVE_TO_COEXISTENCE
      view.setWindowEyepointPolicy(View.RELATIVE_TO_COEXISTENCE);
      view.setLeftManualEyeInCoexistence(new Point3d(0.0142, 0.0135, 0.4572));
      view.setRightManualEyeInCoexistence(new Point3d(0.0208, 0.0135, 0.4572));
      */

      // Create a ViewPlatform instance and bind it with the View instance.
      ViewPlatform vp = new ViewPlatform();
      // vp.setActivationRadius(SSS3dConstants.VISIBILITY);

      view.attachViewPlatform(vp);

      // Create the necessary TransformGroup node for the ViewPlatform's
      // motion, which is guided by the keys.
      vpTrGrKey = new TransformGroup();

      // With the ALLOW_TRANSFORM_READ and ALLOW_TRANSFORM_WRITE
      // capabilities, we allow the modification of the TransformGroup's
      // code by the Behavior's code at run time.
      vpTrGrKey.setCapability(TransformGroup.ALLOW_TRANSFORM_READ);
      vpTrGrKey.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);

      // Attach the ViewPlatform instance to the TransformGroup
      // node vpTrGrKey.
      vpTrGrKey.addChild(vp);

      // View-platform's motion (rotation, zoom and translation)
      keyNav = new KeyNavigation(vpTrGrKey, sss3d);
      keyNav.setSchedulingBounds(new BoundingSphere(new Point3d(0.0, 0.0, 0.0),
                                                    SSS3dConstants.BOUNDRADIUS));

      // Attach the keyNav instance to the TransformGroup
      // node vpTrGrKey.
      vpTrGrKey.addChild(keyNav);

      // Create the necessary TransformGroup node for the ViewPlatform's
      // motion, which is guided by the keys.
      TransformGroup follow = new TransformGroup();

      // With the ALLOW_TRANSFORM_WRITE capabilities, we allow
      // the modification of the TransformGroup's code by
      // the Behavior's code at run time.
      follow.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);

      // Attach the vpTrGrKey instance to the TransformGroup
      // node follow.
      follow.addChild(vpTrGrKey);

      // Create a follow celestial object
      FollowCelestialObject fObject = new FollowCelestialObject(follow, sss3d);
      fObject.setSchedulingBounds(new BoundingSphere(new Point3d(0.0, 0.0, 0.0),
                                                     SSS3dConstants.BOUNDRADIUS));

      // Attach the fObject instance to the TransformGroup
      // node follow.
      follow.addChild(fObject);

      // Add the TransformGroup node vpTrGrKey to the new
      // BranchGroup node vbBrGr.
      BranchGroup vbBrGr = new BranchGroup();
      vbBrGr.addChild(follow);

      // Compile the ViewBranch to optimize the performances.
      vbBrGr.compile();

      // Return the final version of the view branch BranchGroup node vbBrGr.
      return vbBrGr;
   }

   /**
    * Set the camera position to the init state.
    */
   public void setStartPosition() {
      try {
         vpTrGrKey.setTransform(new Transform3D());
      } catch(CapabilityNotSetException cnse) {
         System.out.println("Error: " + cnse.toString());
      } catch(BadTransformException bte) {
         System.out.println("Error: " + bte.toString());
      }
   }
}
