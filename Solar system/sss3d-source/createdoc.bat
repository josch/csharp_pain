@echo off
echo Documentation script for J3D Solar System Simulator
if "%1"=="" goto error

echo.
echo Please be patient, generating the Documentation will take some time...
echo Now generating Documentation....
javadoc -d %1 -version -author -use -package -splitindex -windowtitle "Diplomawork Computergraphics: J3D Solar System Simulator" -doctitle "Solar System Simulator - Classdocumentation" sss3d sss3d.calculations sss3d.calculations.keplerequation sss3d.calculations.ellipseequation sss3d.calculations.constants sss3d.contentbranch sss3d.contentbranch.comets sss3d.contentbranch.planets sss3d.contentbranch.moons sss3d.contentbranch.rockets sss3d.contentbranch.sun sss3d.contentbranch.orbit sss3d.gui sss3d.gui.infobox sss3d.gui.startbox sss3d.gui.navigationbox sss3d.utils sss3d.utils.astronomy sss3d.utils.joystick sss3d.utils.observer sss3d.utils.xmlparser sss3d.viewbranch

goto end

:error
echo.
echo Usage: createdoc.bat output_path
echo.
echo               To regenerate the Documentation of
echo               the project J3D Solar System Simulator to output_path
echo               give an *existing* output_path with *write permission*
echo.
goto end

:end

