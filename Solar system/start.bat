@echo off
echo THIS IS A BATCH SCRIPT TO START
echo *** J3D Solar System Simulator ***
echo Diploma thesis of Bernhard Hari and Marcel Portner, 2000
echo Biel school of engineering and architecture, Biel, Switzerland

javaw -Xmx128m -jar sss3d.jar


