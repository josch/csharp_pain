using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Net;
using System.IO;

namespace MegaTokyoDownloader
{
    public partial class WebDownloader : Form
    {
        private delegate void addloghandler(string text);

        private Thread t = null;

        public WebDownloader()
        {
            InitializeComponent();
        }

        private void Durchsuchen_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                tbPfad.Text = folderBrowserDialog1.SelectedPath;
            }
        }

        private void Starten_Click(object sender, EventArgs e)
        {
            Starten.Enabled = false;
            Abbrechen.Enabled = true;
            t = new Thread(new ThreadStart(Download));
            t.Start();
        }

        private void Abbrechen_Click(object sender, EventArgs e)
        {
            t.Abort();
            Abbrechen.Enabled = false;
            Starten.Enabled = true;
        }

        void Download()
        {
            WebClient wc = new WebClient();
            for (int i = 286; i <= 326; i++)
            {
                string si = i.ToString();
                while (si.Length < 4)
                {
                    si = "0" + si;
                }

                wc.DownloadFile("http://www.giantitp.com/oots/oots" + si + ".gif", "E:\\oots\\oots" + si + ".gif");
                Invoke(new addloghandler(addlog), new object[] { si });

                /*
                if(i<10)
                {
                    if (File.Exists(tbPfad.Text + "\\000" + i + ".gif"))
                    {
                        this.Invoke(new addloghandler(addlog), new object[] { "000" + i + ".gif" + " already exits!\r\n" });
                    }
                    else
                    {
                        try
                        {
                            this.Invoke(new addloghandler(addlog), new object[] { "Downloading http://www.megatokyo.com/strips/000" + i.ToString() + ".gif..." });
                            wc.DownloadFile("http://www.megatokyo.com/strips/000" + i + ".gif", tbPfad.Text + "\\000" + i + ".gif");
                            this.Invoke(new addloghandler(addlog), new object[] { " Complete!\r\n" });
                        }
                        catch (WebException we)
                        {
                            this.Invoke(new addloghandler(addlog), new object[] { "Download failed: " + we.Message + "\r\n" });
                        }
                    }
                }
                if (i >= 10 && i < 100)
                {
                    if (File.Exists(tbPfad.Text + "\\00" + i + ".gif"))
                    {
                        this.Invoke(new addloghandler(addlog), new object[] { "00" + i + ".gif" + " already exits!\r\n" });
                    }
                    else
                    {
                        try
                        {
                            this.Invoke(new addloghandler(addlog), new object[] { "Downloading http://www.megatokyo.com/strips/00" + i.ToString() + ".gif..." });
                            wc.DownloadFile("http://www.megatokyo.com/strips/00" + i + ".gif", tbPfad.Text + "\\00" + i + ".gif");
                            this.Invoke(new addloghandler(addlog), new object[] { " Complete!\r\n" });
                        }
                        catch (WebException we)
                        {
                            this.Invoke(new addloghandler(addlog), new object[] { "Download failed: " + we.Message + "\r\n" });
                            this.Invoke(new addloghandler(addlog), new object[] { "Trying to download JPG \r\n" });
                            if (File.Exists(tbPfad.Text + "\\00" + i + ".jpg"))
                            {
                                this.Invoke(new addloghandler(addlog), new object[] { "00" + i + ".jpg" + " already exits!\r\n" });
                            }
                            else
                            {
                                try
                                {
                                    this.Invoke(new addloghandler(addlog), new object[] { "Downloading http://www.megatokyo.com/strips/00" + i.ToString() + ".jpg..." });
                                    wc.DownloadFile("http://www.megatokyo.com/strips/00" + i + ".jpg", tbPfad.Text + "\\00" + i + ".jpg");
                                    this.Invoke(new addloghandler(addlog), new object[] { " Complete!\r\n" });
                                }
                                catch (WebException we2)
                                {
                                    this.Invoke(new addloghandler(addlog), new object[] { "Download failed: " + we2.Message + "\r\n" });
                                    this.Invoke(new addloghandler(addlog), new object[] { "No GIF or JPG exists! \r\n" });
                                }
                            }
                        }
                    }
                }
                if (i >= 100 && i < 1000)
                {
                    if (File.Exists(tbPfad.Text + "\\0" + i + ".gif"))
                    {
                        this.Invoke(new addloghandler(addlog), new object[] { "0" + i + ".gif" + " already exits!\r\n" });
                    }
                    else
                    {
                        try
                        {
                            this.Invoke(new addloghandler(addlog), new object[] { "Downloading http://www.megatokyo.com/strips/0" + i.ToString() + ".gif..." });
                            wc.DownloadFile("http://www.megatokyo.com/strips/0" + i + ".gif", tbPfad.Text + "\\0" + i + ".gif");
                            this.Invoke(new addloghandler(addlog), new object[] { " Complete!\r\n" });
                        }
                        catch (WebException we)
                        {
                            this.Invoke(new addloghandler(addlog), new object[] { "Download failed: " + we.Message + "\r\n" });
                            this.Invoke(new addloghandler(addlog), new object[] { "Trying to download JPG \r\n" });
                            if (File.Exists(tbPfad.Text + "\\0" + i + ".jpg"))
                            {
                                this.Invoke(new addloghandler(addlog), new object[] { "0" + i + ".jpg" + " already exits!\r\n" });
                            }
                            else
                            {
                                try
                                {
                                    this.Invoke(new addloghandler(addlog), new object[] { "Downloading http://www.megatokyo.com/strips/0" + i.ToString() + ".jpg..." });
                                    wc.DownloadFile("http://www.megatokyo.com/strips/0" + i + ".jpg", tbPfad.Text + "\\0" + i + ".jpg");
                                    this.Invoke(new addloghandler(addlog), new object[] { " Complete!\r\n" });
                                }
                                catch (WebException we2)
                                {
                                    this.Invoke(new addloghandler(addlog), new object[] { "Download failed: " + we2.Message + "\r\n" });
                                    this.Invoke(new addloghandler(addlog), new object[] { "No GIF or JPG exists! \r\n" });
                                }
                            }
                        }
                    }
                }
                 */
            }
        }

        void addlog(string text)
        {
            Log.Text += text;
            Log.Select(Log.Text.Length, 0);
            Log.ScrollToCaret();
        }

        private void MegaTokyoDownloader_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (t != null)
            {
                t.Abort();
            }
        }
    }
}