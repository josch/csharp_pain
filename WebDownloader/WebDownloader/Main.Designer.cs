namespace MegaTokyoDownloader
{
    partial class WebDownloader
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.tbPfad = new System.Windows.Forms.TextBox();
            this.btnDurchsuchen = new System.Windows.Forms.Button();
            this.Log = new System.Windows.Forms.TextBox();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.Starten = new System.Windows.Forms.Button();
            this.Abbrechen = new System.Windows.Forms.Button();
            this.nUDFrom = new System.Windows.Forms.NumericUpDown();
            this.nUDto = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tbURI = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.tbFilename = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.lExample = new System.Windows.Forms.Label();
            this.gbSettings = new System.Windows.Forms.GroupBox();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.label11 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.tbNewProjectName = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.nUDFrom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nUDto)).BeginInit();
            this.gbSettings.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 172);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Speicherort:";
            // 
            // tbPfad
            // 
            this.tbPfad.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbPfad.Location = new System.Drawing.Point(80, 170);
            this.tbPfad.Name = "tbPfad";
            this.tbPfad.Size = new System.Drawing.Size(295, 20);
            this.tbPfad.TabIndex = 1;
            // 
            // btnDurchsuchen
            // 
            this.btnDurchsuchen.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDurchsuchen.Location = new System.Drawing.Point(381, 168);
            this.btnDurchsuchen.Name = "btnDurchsuchen";
            this.btnDurchsuchen.Size = new System.Drawing.Size(81, 21);
            this.btnDurchsuchen.TabIndex = 2;
            this.btnDurchsuchen.Text = "Durchsuchen";
            this.btnDurchsuchen.Click += new System.EventHandler(this.Durchsuchen_Click);
            // 
            // Log
            // 
            this.Log.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Log.Location = new System.Drawing.Point(12, 334);
            this.Log.Multiline = true;
            this.Log.Name = "Log";
            this.Log.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Log.Size = new System.Drawing.Size(468, 170);
            this.Log.TabIndex = 3;
            // 
            // folderBrowserDialog1
            // 
            this.folderBrowserDialog1.SelectedPath = "folderBrowserDialog1";
            // 
            // Starten
            // 
            this.Starten.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Starten.Location = new System.Drawing.Point(12, 305);
            this.Starten.Name = "Starten";
            this.Starten.Size = new System.Drawing.Size(75, 23);
            this.Starten.TabIndex = 4;
            this.Starten.Text = "Starten!";
            this.Starten.Click += new System.EventHandler(this.Starten_Click);
            // 
            // Abbrechen
            // 
            this.Abbrechen.Enabled = false;
            this.Abbrechen.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Abbrechen.Location = new System.Drawing.Point(92, 305);
            this.Abbrechen.Name = "Abbrechen";
            this.Abbrechen.Size = new System.Drawing.Size(75, 23);
            this.Abbrechen.TabIndex = 5;
            this.Abbrechen.Text = "Abbrechen";
            this.Abbrechen.Click += new System.EventHandler(this.Abbrechen_Click);
            // 
            // nUDFrom
            // 
            this.nUDFrom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nUDFrom.Location = new System.Drawing.Point(108, 66);
            this.nUDFrom.Name = "nUDFrom";
            this.nUDFrom.Size = new System.Drawing.Size(50, 20);
            this.nUDFrom.TabIndex = 6;
            this.nUDFrom.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // nUDto
            // 
            this.nUDto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nUDto.Location = new System.Drawing.Point(190, 66);
            this.nUDto.Name = "nUDto";
            this.nUDto.Size = new System.Drawing.Size(50, 20);
            this.nUDto.TabIndex = 7;
            this.nUDto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(77, 68);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(25, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "von";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(164, 68);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(20, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "bis";
            // 
            // tbURI
            // 
            this.tbURI.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbURI.Location = new System.Drawing.Point(80, 14);
            this.tbURI.Name = "tbURI";
            this.tbURI.Size = new System.Drawing.Size(382, 20);
            this.tbURI.TabIndex = 10;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 16);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(54, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Webpath:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 44);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(68, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "Filename #1:";
            // 
            // tbFilename
            // 
            this.tbFilename.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbFilename.Location = new System.Drawing.Point(80, 42);
            this.tbFilename.Name = "tbFilename";
            this.tbFilename.Size = new System.Drawing.Size(187, 20);
            this.tbFilename.TabIndex = 13;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 68);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(40, 13);
            this.label6.TabIndex = 14;
            this.label6.Text = "Z�hler:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 94);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(68, 13);
            this.label7.TabIndex = 15;
            this.label7.Text = "Filename #2:";
            // 
            // textBox1
            // 
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox1.Location = new System.Drawing.Point(80, 92);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(187, 20);
            this.textBox1.TabIndex = 16;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 120);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(103, 13);
            this.label8.TabIndex = 17;
            this.label8.Text = "Possible Extensions:";
            // 
            // textBox2
            // 
            this.textBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox2.Location = new System.Drawing.Point(115, 118);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(178, 20);
            this.textBox2.TabIndex = 18;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(299, 120);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(118, 13);
            this.label9.TabIndex = 19;
            this.label9.Text = "(Example: \"gif jpg png\")";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 147);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(46, 13);
            this.label10.TabIndex = 20;
            this.label10.Text = "Beispiel:";
            // 
            // lExample
            // 
            this.lExample.AutoSize = true;
            this.lExample.Location = new System.Drawing.Point(77, 147);
            this.lExample.Name = "lExample";
            this.lExample.Size = new System.Drawing.Size(13, 13);
            this.lExample.TabIndex = 21;
            this.lExample.Text = "0";
            // 
            // gbSettings
            // 
            this.gbSettings.Controls.Add(this.numericUpDown1);
            this.gbSettings.Controls.Add(this.label11);
            this.gbSettings.Controls.Add(this.label4);
            this.gbSettings.Controls.Add(this.lExample);
            this.gbSettings.Controls.Add(this.nUDFrom);
            this.gbSettings.Controls.Add(this.label10);
            this.gbSettings.Controls.Add(this.btnDurchsuchen);
            this.gbSettings.Controls.Add(this.nUDto);
            this.gbSettings.Controls.Add(this.tbPfad);
            this.gbSettings.Controls.Add(this.label9);
            this.gbSettings.Controls.Add(this.label1);
            this.gbSettings.Controls.Add(this.label2);
            this.gbSettings.Controls.Add(this.textBox2);
            this.gbSettings.Controls.Add(this.label3);
            this.gbSettings.Controls.Add(this.label8);
            this.gbSettings.Controls.Add(this.tbURI);
            this.gbSettings.Controls.Add(this.textBox1);
            this.gbSettings.Controls.Add(this.label5);
            this.gbSettings.Controls.Add(this.label7);
            this.gbSettings.Controls.Add(this.tbFilename);
            this.gbSettings.Controls.Add(this.label6);
            this.gbSettings.Location = new System.Drawing.Point(12, 68);
            this.gbSettings.Name = "gbSettings";
            this.gbSettings.Size = new System.Drawing.Size(468, 202);
            this.gbSettings.TabIndex = 22;
            this.gbSettings.TabStop = false;
            this.gbSettings.Text = "Einstellungen";
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Location = new System.Drawing.Point(329, 66);
            this.numericUpDown1.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numericUpDown1.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(34, 20);
            this.numericUpDown1.TabIndex = 23;
            this.numericUpDown1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.numericUpDown1.Value = new decimal(new int[] {
            4,
            0,
            0,
            0});
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(246, 68);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(77, 13);
            this.label11.TabIndex = 22;
            this.label11.Text = "Anzahl Stellen:";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(128, 12);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(352, 21);
            this.comboBox1.TabIndex = 23;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(12, 15);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(110, 13);
            this.label12.TabIndex = 24;
            this.label12.Text = "Vorhandene Projekte:";
            // 
            // button2
            // 
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Location = new System.Drawing.Point(161, 39);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(181, 23);
            this.button2.TabIndex = 26;
            this.button2.Text = "Projekt Einstellungen bearbeiten";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Location = new System.Drawing.Point(348, 39);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(132, 23);
            this.button3.TabIndex = 27;
            this.button3.Text = "�nderungen speichern";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Location = new System.Drawing.Point(12, 276);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(135, 23);
            this.button1.TabIndex = 28;
            this.button1.Text = "Neues Projekt erstellen";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Location = new System.Drawing.Point(405, 276);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 29;
            this.button4.Text = "Speichern";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(153, 281);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(20, 13);
            this.label13.TabIndex = 30;
            this.label13.Text = "als";
            // 
            // tbNewProjectName
            // 
            this.tbNewProjectName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbNewProjectName.Location = new System.Drawing.Point(179, 278);
            this.tbNewProjectName.Name = "tbNewProjectName";
            this.tbNewProjectName.Size = new System.Drawing.Size(220, 20);
            this.tbNewProjectName.TabIndex = 31;
            // 
            // WebDownloader
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(492, 516);
            this.Controls.Add(this.tbNewProjectName);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.gbSettings);
            this.Controls.Add(this.Abbrechen);
            this.Controls.Add(this.Starten);
            this.Controls.Add(this.Log);
            this.MaximizeBox = false;
            this.Name = "WebDownloader";
            this.Text = "Der Webcomic Downloader - relax, it downloads for j00";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MegaTokyoDownloader_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.nUDFrom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nUDto)).EndInit();
            this.gbSettings.ResumeLayout(false);
            this.gbSettings.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbPfad;
        private System.Windows.Forms.Button btnDurchsuchen;
        private System.Windows.Forms.TextBox Log;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.Button Starten;
        private System.Windows.Forms.Button Abbrechen;
        private System.Windows.Forms.NumericUpDown nUDFrom;
        private System.Windows.Forms.NumericUpDown nUDto;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbURI;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbFilename;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label lExample;
        private System.Windows.Forms.GroupBox gbSettings;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox tbNewProjectName;
    }
}

