using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.IO;
using System.Threading;

namespace IPUpdate
{
    public partial class MainWindow : Form
    {
        private delegate void UpdateTextBoxHandler(string text);
        Thread t;

        public MainWindow()
        {
            InitializeComponent();
            t = new Thread(new ThreadStart(Request));
            t.Start();
        }

        private void UpdateTextBox(string text)
        {
            //label1.Text = text;
            textBox1.Text = text;
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            t.Abort();
            t.Start();
        }

        private void Request()
        {
                CookieContainer cookie = new CookieContainer();

                byte[] postdata2 = Encoding.ASCII.GetBytes("login=Johannes.Schauer&kennwort=porenta&sprache=&x=0&y=18");
                byte[] postdata = Encoding.ASCII.GetBytes("domain=johannes-schauer.de&" +
                    "action=UPDATE&" +
                    "expertmode=true&" +
                    "subdomain_name_old=dyndns&" +
                    "subdomain_typ_old=A&" +
                    "subdomain_ziel_old=99.99.99.99&" +
                    "subdomain_ttl_old=&" +
                    "subdomain_premx_old=&" +
                    "subdomain_dir_old=dyndns&" +
                    "dotnet_old=0&" +
                    "frontpage_old=0&" +
                    "frontpage_login_old=&" +
                    "frontpage_kennwort_old=&" +
                    "ssl_old=0&" +
                    "subdomain_name=dyndns&" +
                    "subdomain_inklwww=-1&" +
                    "subdomain_dir=&" +
                    "frontpage_login=&" +
                    "frontpage_kennwort=&" +
                    "aspnet=0&ssl=0&" +
                    "subdomain_typ=A&" +
                    "subdomain_ttl=&" +
                    "subdomain_ziel=66.66.66.66&" +
                    "subdomain_premx=");
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create("http://webserverconfig.de/index.php");
                req.CookieContainer = cookie;
                req.Method = "POST";
                req.ContentType = "application/x-www-form-urlencoded";
                Stream write = req.GetRequestStream();
                write.Write(postdata2, 0, postdata2.Length);
                write.Close();
                req.GetResponse().Close();

                HttpWebRequest req2 = (HttpWebRequest)WebRequest.Create("http://webserverconfig.de/subdomains.php");
                req2.CookieContainer = cookie;
                req2.Method = "POST";
                req2.ContentType = "application/x-www-form-urlencoded";
                Stream write2 = req2.GetRequestStream();
                write2.Write(postdata, 0, postdata.Length);
                write2.Close();
                req2.GetResponse().Close();
        }
    }
}