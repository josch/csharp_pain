namespace Alarm
{
    partial class Alarm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnStop = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.tbWeckzeit = new System.Windows.Forms.TextBox();
            this.cbAktivieren = new System.Windows.Forms.CheckBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lUhrzeit = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnStop
            // 
            this.btnStop.BackColor = System.Drawing.SystemColors.Control;
            this.btnStop.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnStop.Enabled = false;
            this.btnStop.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnStop.Font = new System.Drawing.Font("Verdana", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStop.Location = new System.Drawing.Point(0, 131);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(341, 40);
            this.btnStop.TabIndex = 0;
            this.btnStop.Text = "STOP";
            this.btnStop.UseVisualStyleBackColor = false;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // tbWeckzeit
            // 
            this.tbWeckzeit.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbWeckzeit.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbWeckzeit.Location = new System.Drawing.Point(180, 4);
            this.tbWeckzeit.Name = "tbWeckzeit";
            this.tbWeckzeit.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.tbWeckzeit.Size = new System.Drawing.Size(75, 33);
            this.tbWeckzeit.TabIndex = 2;
            this.tbWeckzeit.Text = "6:00";
            this.tbWeckzeit.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbWeckzeit.TextChanged += new System.EventHandler(this.tbWeckzeit_TextChanged);
            // 
            // cbAktivieren
            // 
            this.cbAktivieren.AutoSize = true;
            this.cbAktivieren.BackColor = System.Drawing.SystemColors.Control;
            this.cbAktivieren.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbAktivieren.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbAktivieren.Location = new System.Drawing.Point(85, 12);
            this.cbAktivieren.Name = "cbAktivieren";
            this.cbAktivieren.Size = new System.Drawing.Size(89, 20);
            this.cbAktivieren.TabIndex = 3;
            this.cbAktivieren.Text = "Aktivieren";
            this.cbAktivieren.UseVisualStyleBackColor = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.cbAktivieren);
            this.panel1.Controls.Add(this.tbWeckzeit);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(341, 40);
            this.panel1.TabIndex = 5;
            // 
            // lUhrzeit
            // 
            this.lUhrzeit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lUhrzeit.Font = new System.Drawing.Font("Verdana", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lUhrzeit.Location = new System.Drawing.Point(0, 40);
            this.lUhrzeit.Name = "lUhrzeit";
            this.lUhrzeit.Size = new System.Drawing.Size(341, 91);
            this.lUhrzeit.TabIndex = 6;
            this.lUhrzeit.Text = "00:00:00";
            this.lUhrzeit.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Alarm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(341, 171);
            this.Controls.Add(this.lUhrzeit);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btnStop);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Location = new System.Drawing.Point(1300, 0);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Alarm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Alarm";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.TextBox tbWeckzeit;
        private System.Windows.Forms.CheckBox cbAktivieren;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lUhrzeit;
    }
}

