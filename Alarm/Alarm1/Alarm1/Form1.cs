using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Media;

namespace Alarm
{
    public partial class Alarm : Form
    {
        private SoundPlayer sp = null;

        public Alarm()
        {
            InitializeComponent();
            sp = new SoundPlayer("c:\\Windows\\Media\\ringin.wav");
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            sp.Stop();
            btnStop.Enabled = false;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            lUhrzeit.Text = DateTime.Now.ToLongTimeString();
            if (cbAktivieren.Checked)
            {
                if (DateTime.Parse(lUhrzeit.Text) == DateTime.Parse(tbWeckzeit.Text))
                {
                    sp.PlayLooping();
                    btnStop.Enabled = true;
                }
            }
        }

        private void tbWeckzeit_TextChanged(object sender, EventArgs e)
        {
            cbAktivieren.Checked = false;
        }
    }
}