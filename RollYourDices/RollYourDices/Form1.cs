using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Threading;

namespace RollYourDices
{
    public partial class Form1 : Form
    {
        Thread t;
        private delegate void AddLabelHandler(string text);
        private delegate void ClearPanelHandler();

        int DiceCount;
        int Dice;

        public Form1()
        {
            InitializeComponent();
            t = new Thread(new ThreadStart(Roll));
        }

        private void bRoll_Click(object sender, EventArgs e)
        {
            DiceCount = (int)nUDDiceCount.Value;
            Dice = int.Parse(cbDice.SelectedItem.ToString());
            t.Start();
        }

        private void Roll()
        {
            Invoke(new ClearPanelHandler(ClearPanel));
            for (int i = 0; i < DiceCount; i++)
            {
                Random ran = new Random();
                Invoke(new AddLabelHandler(AddLabel), new object[] { (Math.Ceiling(ran.NextDouble() * Dice)).ToString() });

                Thread.Sleep((int)(Math.Ceiling(ran.NextDouble() * 100)));
            }
        }

        private void ClearPanel()
        {
            flpRolls.Controls.Clear();
        }

        private void AddLabel(string text)
        {
            Label l = new Label();
            l.Text = text;
            l.AutoSize = false;
            l.Size = new Size(64, 64);
            l.TextAlign = ContentAlignment.MiddleCenter;
            l.Font = new Font(new FontFamily("Microsoft Sans Serif"), 20, FontStyle.Bold);
            l.Margin = new Padding(0);
            flpRolls.Controls.Add(l);
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            t.Abort();
        }

        private void bStop_Click(object sender, EventArgs e)
        {
            t.Abort();
        }
    }
}