namespace RollYourDices
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cbDice = new System.Windows.Forms.ComboBox();
            this.nUDDiceCount = new System.Windows.Forms.NumericUpDown();
            this.bRoll = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.flpRolls = new System.Windows.Forms.FlowLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.bStop = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.nUDDiceCount)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // cbDice
            // 
            this.cbDice.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbDice.FormattingEnabled = true;
            this.cbDice.Items.AddRange(new object[] {
            "4",
            "6",
            "8",
            "10",
            "12",
            "20",
            "30",
            "100"});
            this.cbDice.Location = new System.Drawing.Point(26, 3);
            this.cbDice.Name = "cbDice";
            this.cbDice.Size = new System.Drawing.Size(51, 21);
            this.cbDice.TabIndex = 0;
            // 
            // nUDDiceCount
            // 
            this.nUDDiceCount.Location = new System.Drawing.Point(83, 4);
            this.nUDDiceCount.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nUDDiceCount.Name = "nUDDiceCount";
            this.nUDDiceCount.Size = new System.Drawing.Size(47, 20);
            this.nUDDiceCount.TabIndex = 1;
            this.nUDDiceCount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.nUDDiceCount.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // bRoll
            // 
            this.bRoll.Location = new System.Drawing.Point(136, 1);
            this.bRoll.Name = "bRoll";
            this.bRoll.Size = new System.Drawing.Size(62, 22);
            this.bRoll.TabIndex = 2;
            this.bRoll.Text = "Roll!";
            this.bRoll.UseVisualStyleBackColor = true;
            this.bRoll.Click += new System.EventHandler(this.bRoll_Click);
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.Controls.Add(this.bStop);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.cbDice);
            this.panel1.Controls.Add(this.bRoll);
            this.panel1.Controls.Add(this.nUDDiceCount);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(378, 43);
            this.panel1.TabIndex = 3;
            // 
            // flpRolls
            // 
            this.flpRolls.AutoScroll = true;
            this.flpRolls.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpRolls.Location = new System.Drawing.Point(0, 43);
            this.flpRolls.Name = "flpRolls";
            this.flpRolls.Size = new System.Drawing.Size(378, 171);
            this.flpRolls.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(3, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(17, 20);
            this.label1.TabIndex = 3;
            this.label1.Text = "d";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // bStop
            // 
            this.bStop.Location = new System.Drawing.Point(204, 1);
            this.bStop.Name = "bStop";
            this.bStop.Size = new System.Drawing.Size(48, 22);
            this.bStop.TabIndex = 4;
            this.bStop.Text = "Stop";
            this.bStop.UseVisualStyleBackColor = true;
            this.bStop.Click += new System.EventHandler(this.bStop_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(378, 214);
            this.Controls.Add(this.flpRolls);
            this.Controls.Add(this.panel1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.nUDDiceCount)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox cbDice;
        private System.Windows.Forms.NumericUpDown nUDDiceCount;
        private System.Windows.Forms.Button bRoll;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.FlowLayoutPanel flpRolls;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button bStop;
    }
}

