using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ImageViewer
{
    public partial class Main : Form
    {
        Bitmap bm;

        public Main()
        {
            InitializeComponent();
        }

        private void tsmi�ffnen_Click(object sender, EventArgs e)
        {
            if (openImageDialog.ShowDialog() == DialogResult.OK)
            {
                bm = new Bitmap(Image.FromFile(openImageDialog.FileName));
                pictureBox1.ImageLocation = openImageDialog.FileName;
                pictureBox1.Width = bm.Width;
                pictureBox1.Height = bm.Height;
            }
        }

        private void toolStripDropDownButton1_DropDownItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            switch (e.ClickedItem.Text)
            {
                case "1000%":
                    pictureBox1.Width = 10*bm.Width;
                    pictureBox1.Height = 10*bm.Height;
                    break;
            }
        }
    }
}