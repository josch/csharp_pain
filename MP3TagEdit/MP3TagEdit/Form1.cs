using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace MP3TagEdit
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnOpen_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                lFilename.Text = openFileDialog1.FileName;
                MP3IDv11 mp3 = new MP3IDv11(openFileDialog1.FileName);
                tbTitle.Text = mp3.Title;
                tbArtist.Text = mp3.Artist;
                tbAlbum.Text = mp3.Album;
                nUDYear.Value = mp3.Year;
                tbComment.Text = mp3.Comment;
                nUDTrack.Value = mp3.Track;
                cbGenre.SelectedIndex = (int)mp3.Genre != 255 ? (int)mp3.Genre : 80;
            }
        }
    }
}