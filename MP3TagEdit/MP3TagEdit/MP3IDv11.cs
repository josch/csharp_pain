using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace MP3TagEdit
{
    class MP3IDv11
    {
        private string filename;
        private byte[] title;
        private byte[] artist;
        private byte[] album;
        private byte[] year;
        private byte[] comment;
        private byte track;
        private byte genre;

        public string Title
        {
            get { return Encoding.UTF8.GetString(title); }
            set
            {
                if(value.Length<31)
                    Encoding.UTF8.GetBytes(value, 0, value.Length, title, 0);
                else
                    throw new SystemException("Titel l�nger als 30 Zeichen!");
            }
        }

        public string Artist
        {
            get { return Encoding.UTF8.GetString(artist); }
            set
            {
                if (value.Length < 31)
                    Encoding.UTF8.GetBytes(value, 0, value.Length, artist, 0);
                else
                    throw new SystemException("Artist l�nger als 30 Zeichen!");
            }
        }

        public string Album
        {
            get { return Encoding.UTF8.GetString(album); }
            set
            {
                if (value.Length < 31)
                    Encoding.UTF8.GetBytes(value, 0, value.Length, album, 0);
                else
                    throw new SystemException("Album l�nger als 30 Zeichen!");
            }
        }

        public int Year
        {
            get
            {
                int year = 0;
                int.TryParse(Encoding.UTF8.GetString(this.year), out year);
                return year;
            }
            set
            {
                if (value.ToString().Length < 5)
                    Encoding.UTF8.GetBytes(value.ToString(), 0, value.ToString().Length, year, 0);
                else
                    throw new SystemException("Year l�nger als 4 Zeichen!");
            }
        }

        public string Comment
        {
            get { return Encoding.UTF8.GetString(comment); }
            set
            {
                if (value.Length < 29)
                    Encoding.UTF8.GetBytes(value, 0, value.Length, comment, 0);
                else
                    throw new SystemException("Comment l�nger als 28 Zeichen!");
            }
        }

        public int Track
        {
            get { return (int)track; }
            set { track = (byte)value; }
        }

        public Genres Genre
        {
            get { return (Genres)genre; }
            set { genre = (byte)value; }
        }

        public enum Genres : byte
        {
            Blues = 0,
            ClassicRock = 1,
            Country = 2,
            Dance = 3,
            Disco = 4,
            Funk = 5,
            Grunge = 6,
            HipHop = 7,
            Jazz = 8,
            Metal = 9,
            NewAge = 10,
            Oldies = 11,
            Other = 12,
            Pop = 13,
            RnB = 14,
            Rap = 15,
            Reggae = 16,
            Rock = 17,
            Techno = 18,
            Industrial = 19,
            Alternative = 20,
            Ska = 21,
            DeathMetal = 22,
            Pranks = 23,
            Soundtrack = 24,
            EuroTechno = 25,
            Ambient = 26,
            TripHop = 27,
            Vocal = 28,
            JazzFunk = 29,
            Fusion = 30,
            Trance = 31,
            Classical = 32,
            Instrumental = 33,
            Acid = 34,
            House = 35,
            Game = 36,
            SoundClip = 37,
            Gospel = 38,
            Noise = 39,
            AlternRock = 40,
            Bass = 41,
            Soul = 42,
            Punk = 43,
            Space = 44,
            Meditative = 45,
            InstrumentalPop = 46,
            InstrumentalRock = 47,
            Ethnic = 48,
            Gothic = 49,
            Darkwave = 50,
            TechnoIndustrial = 51,
            Electronic = 52,
            PopFolk = 53,
            Eurodance = 54,
            Dream = 55,
            SouthernRock = 56,
            Comedy = 57,
            Cult = 58,
            Gangsta = 59,
            Top40 = 60,
            ChristianRap = 61,
            PopFunk = 62,
            Jungle = 63,
            NativeAmerican = 64,
            Cabaret = 65,
            NewWave = 66,
            Psychadelic = 67,
            Rave = 68,
            Showtunes = 69,
            Trailer = 70,
            LoFi = 71,
            Tribal = 72,
            AcidPunk = 73,
            AcidJazz = 74,
            Polka = 75,
            Retro = 76,
            Musical = 77,
            RocknRoll = 78,
            HardRock = 79,
            None = 255,
        }

        public MP3IDv11(string filename)
        {
            if (File.Exists(filename))
            {
                this.filename = filename;
                ReloadID();
            }
            else
                throw new FileNotFoundException("Existiert nicht!", filename);
        }

        public void SetAttributes(string title, string artist, string album, int year, string comment, int track, Genres genre)
        {
            Title = title;
            Artist = artist;
            Album = album;
            Year = year;
            Comment = comment;
            Track = track;
            Genre = genre;
        }

        public void WriteID(string title, string artist, string album, int year, string comment, int track, Genres genre)
        {
            SetAttributes(title, artist, album, year, comment, track, genre);

            byte[] tag = new byte[128];

            Array.Copy(new byte[] { (byte)'T', (byte)'A', (byte)'G' }, 0, tag, 0, 3);
            Array.Copy(this.title, 0, tag, 3, this.title.Length);
            Array.Copy(this.artist, 0, tag, 33, this.artist.Length);
            Array.Copy(this.album, 0, tag, 63, this.album.Length);
            Array.Copy(this.year, 0, tag, 93, this.year.Length);
            Array.Copy(this.comment, 0, tag, 97, this.Comment.Length);
            tag[125] = 0;
            tag[126] = this.track;
            tag[127] = this.genre;

            FileStream file = new FileStream(filename, FileMode.Open, FileAccess.Write, FileShare.Write);
            file.Write(tag, (int)(file.Length - tag.Length), tag.Length);
            file.Close();
        }

        public void WriteID()
        {
            WriteID(Title, Artist, Album, Year, Comment, Track, Genre);
        }

        public void ReloadID()
        {
            byte[] tag = new byte[128];

            FileStream file = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.Read);
            file.Position = file.Length - tag.Length;
            file.Read(tag, 0, tag.Length);
            file.Close();

            if (Encoding.UTF8.GetString(tag, 0, 3) == "TAG")
            {
                title = new byte[30];
                Array.Copy(tag, 3, title, 0, title.Length);
                artist = new byte[30];
                Array.Copy(tag, 33, artist, 0, artist.Length);
                album = new byte[30];
                Array.Copy(tag, 63, album, 0, album.Length);
                year = new byte[4];
                Array.Copy(tag, 93, year, 0, year.Length);
                comment = new byte[28]; //unsaubere umsetzung: auch f�r ID3v1 ist der comment nur 28 zeichen lang
                Array.Copy(tag, 97, comment, 0, comment.Length);
                if (tag[125] == 0) //ID3v11
                {
                    track = tag[126];
                }
                else //ID3v1
                {
                    track = 0;
                }
                genre = tag[127];
            }
            else
                throw new Exception("Kein MP3IDv1 Tag vorhanden oder falsches Format!");
        }
    }
}
