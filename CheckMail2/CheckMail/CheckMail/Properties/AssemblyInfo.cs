using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("CheckMail")]
[assembly: AssemblyDescription("Noch ein Programm, welches ich meinem Schatz gewidmet habe, aber diesmal hoffentlich auch ein f�r sie pers�nlich sinnvolles. :-) Einfach Web.de Benutzername und Passwort einstellen, Intervall des �berpr�fens festlegen, alles minimieren und wie gewohnt weiterarbeiten. Im gew�hlten zeitlichen Abstand erscheint am Trayicon des Programms ein Balloon-Tooltip der die Anzahl der eventuell neuen Mails und deren Absender anzeigt.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("F�r meine liebste Angelika")]
[assembly: AssemblyProduct("CheckMail")]
[assembly: AssemblyCopyright("Copyright �  2005 by Johannes Schauer")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM componenets.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("8b5e833c-fac2-4f03-a850-9023fd2b3439")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
[assembly: AssemblyVersion("1.1.0.3")]
[assembly: AssemblyFileVersion("1.1.0.3")]
[assembly: System.Resources.NeutralResourcesLanguage("de-DE")]
