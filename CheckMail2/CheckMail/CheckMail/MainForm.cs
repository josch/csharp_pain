/*
 * Created by SharpDevelop.
 * User: Johannes
 * Date: 10.06.2005
 * Time: 12:55
 */
using System;
using System.Drawing;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Configuration;
using CheckMail.Properties;

namespace CheckMail
{
	/// <summary>
	/// Description of MainForm.
	/// </summary>
	public class CheckMail : System.Windows.Forms.Form
	{
		private System.ComponentModel.IContainer components;
        private System.Windows.Forms.ListBox lbMsgDetails;
        private System.Windows.Forms.Label lNeueEmails;
		private System.Windows.Forms.ListBox lbFolders;
        private System.Windows.Forms.Button btnCheckNow;
		private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.NotifyIcon notifyIcon1;
		
		private TcpClient cl = null; //Globaler TcpClient
		private NetworkStream str = null; //Globaler Netzwerkstream
		private int neueEmails = 0; //Anzahl der neuen Emails
        private TextBox tbUser; //Textbox Benutzername
        private TextBox tbPass; //Textbox Passwort
        private string GlobalSenders = ""; //String Liste aller Sender neuer Emails
        private string user = ""; //Benutzername
        private CheckBox cbAutoCheck; //Checkbox AutoCheck ja/nein
        private NumericUpDown nUDIntervall; //Periode mit der gepr�ft wird
        private Label label1;
        private System.Windows.Forms.Timer timer1;
        private ContextMenuStrip contextMenuStrip1;
        private ToolStripMenuItem BeendenToolStripMenuItem;
        private ToolStripMenuItem minimierenToolStripMenuItem;
        private ToolStripMenuItem tothefrontToolStripMenuItem;
        private string pass = ""; //Passwort
        private Thread t = null;
        private Button btnBeenden;
        private ToolStripMenuItem �berToolStripMenuItem;
        private StatusStrip statusStrip1;
        private ToolStripStatusLabel toolStripStatusLabel1;
        private Button bProxy;
        private Settings sett = new Settings();

        private delegate void ClearFolderListHandler();
        private delegate void AddFolderListItemHandler(string text);
        private delegate void ClearMsgDetailsListHandler();
        private delegate void AddMsgDetailsListItemHandler(string text);
        private delegate void SetNeueEmailLabelHandler(string text);
        private delegate void SetNotifyIconTextHandler(string text);
        private delegate void SetBtnCheckNowEnabledHandler(bool enabled);
        private delegate void SetSchlie�enToolStripMenuItemEnabledHandler(bool enabled);
        private delegate void SetBtnBeendenEnabledHandler(bool enabled);


		public CheckMail()
		{
			InitializeComponent();
            tbUser.Text = sett.user;
            tbPass.Text = sett.pass;
            toolStripStatusLabel1.Text = "Fertig";
		}
		
		[STAThread]
		public static void Main(string[] args)
		{
			Application.Run(new CheckMail());            
		}
		
		#region Windows Forms Designer generated code
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CheckMail));
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.�berToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.minimierenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tothefrontToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.BeendenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnCheckNow = new System.Windows.Forms.Button();
            this.lbFolders = new System.Windows.Forms.ListBox();
            this.lNeueEmails = new System.Windows.Forms.Label();
            this.lbMsgDetails = new System.Windows.Forms.ListBox();
            this.tbUser = new System.Windows.Forms.TextBox();
            this.tbPass = new System.Windows.Forms.TextBox();
            this.cbAutoCheck = new System.Windows.Forms.CheckBox();
            this.nUDIntervall = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.btnBeenden = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.bProxy = new System.Windows.Forms.Button();
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nUDIntervall)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.BalloonTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.notifyIcon1.ContextMenuStrip = this.contextMenuStrip1;
            this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
            this.notifyIcon1.Text = "0 neue Emails";
            this.notifyIcon1.Visible = true;
            this.notifyIcon1.DoubleClick += new System.EventHandler(this.notifyIcon1_DoubleClick);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Enabled = true;
            this.contextMenuStrip1.GripMargin = new System.Windows.Forms.Padding(2);
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.�berToolStripMenuItem,
            this.minimierenToolStripMenuItem,
            this.tothefrontToolStripMenuItem,
            this.BeendenToolStripMenuItem});
            this.contextMenuStrip1.Location = new System.Drawing.Point(78, 0);
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.contextMenuStrip1.Size = new System.Drawing.Size(158, 92);
            // 
            // �berToolStripMenuItem
            // 
            this.�berToolStripMenuItem.Name = "�berToolStripMenuItem";
            this.�berToolStripMenuItem.Text = "�ber...";
            this.�berToolStripMenuItem.Click += new System.EventHandler(this.�berToolStripMenuItem_Click);
            // 
            // minimierenToolStripMenuItem
            // 
            this.minimierenToolStripMenuItem.Name = "minimierenToolStripMenuItem";
            this.minimierenToolStripMenuItem.Text = "Minimieren";
            this.minimierenToolStripMenuItem.Click += new System.EventHandler(this.minimierenToolStripMenuItem_Click);
            // 
            // tothefrontToolStripMenuItem
            // 
            this.tothefrontToolStripMenuItem.Name = "tothefrontToolStripMenuItem";
            this.tothefrontToolStripMenuItem.Text = "In den Vordergrund";
            this.tothefrontToolStripMenuItem.Click += new System.EventHandler(this.tothefrontToolStripMenuItem_Click);
            // 
            // BeendenToolStripMenuItem
            // 
            this.BeendenToolStripMenuItem.Name = "BeendenToolStripMenuItem";
            this.BeendenToolStripMenuItem.Text = "Beenden";
            this.BeendenToolStripMenuItem.Click += new System.EventHandler(this.BeendenToolStripMenuItem_Click);
            // 
            // btnOK
            // 
            this.btnOK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOK.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnOK.Location = new System.Drawing.Point(408, 410);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(136, 23);
            this.btnOK.TabIndex = 6;
            this.btnOK.Text = "&Minimize to Tray";
            this.btnOK.Click += new System.EventHandler(this.BtnOKClick);
            // 
            // btnCheckNow
            // 
            this.btnCheckNow.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCheckNow.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnCheckNow.Location = new System.Drawing.Point(228, 10);
            this.btnCheckNow.Name = "btnCheckNow";
            this.btnCheckNow.Size = new System.Drawing.Size(104, 23);
            this.btnCheckNow.TabIndex = 3;
            this.btnCheckNow.Text = "Check &Now";
            this.btnCheckNow.Click += new System.EventHandler(this.BtnCheckNowClick);
            // 
            // lbFolders
            // 
            this.lbFolders.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbFolders.FormattingEnabled = true;
            this.lbFolders.HorizontalScrollbar = true;
            this.lbFolders.Location = new System.Drawing.Point(12, 38);
            this.lbFolders.Name = "lbFolders";
            this.lbFolders.SelectionMode = System.Windows.Forms.SelectionMode.None;
            this.lbFolders.Size = new System.Drawing.Size(137, 366);
            this.lbFolders.TabIndex = 2;
            this.lbFolders.TabStop = false;
            // 
            // lNeueEmails
            // 
            this.lNeueEmails.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lNeueEmails.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lNeueEmails.Location = new System.Drawing.Point(14, 410);
            this.lNeueEmails.Name = "lNeueEmails";
            this.lNeueEmails.Size = new System.Drawing.Size(208, 21);
            this.lNeueEmails.TabIndex = 11;
            this.lNeueEmails.Text = "Neue Emails:";
            this.lNeueEmails.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbMsgDetails
            // 
            this.lbMsgDetails.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbMsgDetails.FormattingEnabled = true;
            this.lbMsgDetails.HorizontalScrollbar = true;
            this.lbMsgDetails.Location = new System.Drawing.Point(157, 38);
            this.lbMsgDetails.Name = "lbMsgDetails";
            this.lbMsgDetails.SelectionMode = System.Windows.Forms.SelectionMode.None;
            this.lbMsgDetails.Size = new System.Drawing.Size(387, 366);
            this.lbMsgDetails.TabIndex = 12;
            this.lbMsgDetails.TabStop = false;
            // 
            // tbUser
            // 
            this.tbUser.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbUser.Location = new System.Drawing.Point(12, 12);
            this.tbUser.Name = "tbUser";
            this.tbUser.Size = new System.Drawing.Size(102, 20);
            this.tbUser.TabIndex = 1;
            this.tbUser.Text = "J.Schauer";
            this.tbUser.TextChanged += new System.EventHandler(this.tbUser_TextChanged);
            // 
            // tbPass
            // 
            this.tbPass.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbPass.Location = new System.Drawing.Point(120, 12);
            this.tbPass.Name = "tbPass";
            this.tbPass.PasswordChar = '*';
            this.tbPass.Size = new System.Drawing.Size(102, 20);
            this.tbPass.TabIndex = 2;
            this.tbPass.Text = "porenta";
            this.tbPass.TextChanged += new System.EventHandler(this.tbPass_TextChanged);
            // 
            // cbAutoCheck
            // 
            this.cbAutoCheck.AutoSize = true;
            this.cbAutoCheck.Checked = true;
            this.cbAutoCheck.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbAutoCheck.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbAutoCheck.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.cbAutoCheck.Location = new System.Drawing.Point(338, 13);
            this.cbAutoCheck.Name = "cbAutoCheck";
            this.cbAutoCheck.Size = new System.Drawing.Size(94, 17);
            this.cbAutoCheck.TabIndex = 4;
            this.cbAutoCheck.Text = "Auto Check alle";
            this.cbAutoCheck.CheckedChanged += new System.EventHandler(this.cbAutoCheck_CheckedChanged);
            // 
            // nUDIntervall
            // 
            this.nUDIntervall.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nUDIntervall.Location = new System.Drawing.Point(438, 12);
            this.nUDIntervall.Maximum = new decimal(new int[] {
            60,
            0,
            0,
            0});
            this.nUDIntervall.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nUDIntervall.Name = "nUDIntervall";
            this.nUDIntervall.Size = new System.Drawing.Size(51, 20);
            this.nUDIntervall.TabIndex = 5;
            this.nUDIntervall.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.nUDIntervall.ValueChanged += new System.EventHandler(this.nUDIntervall_ValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label1.Location = new System.Drawing.Point(495, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 13);
            this.label1.TabIndex = 13;
            this.label1.Text = "Minuten";
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 300000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // btnBeenden
            // 
            this.btnBeenden.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBeenden.Location = new System.Drawing.Point(321, 410);
            this.btnBeenden.Name = "btnBeenden";
            this.btnBeenden.Size = new System.Drawing.Size(81, 23);
            this.btnBeenden.TabIndex = 14;
            this.btnBeenden.Text = "&Beenden";
            this.btnBeenden.Click += new System.EventHandler(this.btnBeenden_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.Table;
            this.statusStrip1.Location = new System.Drawing.Point(0, 439);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(556, 22);
            this.statusStrip1.SizingGrip = false;
            this.statusStrip1.TabIndex = 15;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Spring = true;
            this.toolStripStatusLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // bProxy
            // 
            this.bProxy.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bProxy.Location = new System.Drawing.Point(157, 410);
            this.bProxy.Name = "bProxy";
            this.bProxy.Size = new System.Drawing.Size(71, 23);
            this.bProxy.TabIndex = 16;
            this.bProxy.Text = "Proxy";
            // 
            // CheckMail
            // 
            this.AcceptButton = this.btnCheckNow;
            this.ClientSize = new System.Drawing.Size(556, 461);
            this.ControlBox = false;
            this.Controls.Add(this.bProxy);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.btnBeenden);
            this.Controls.Add(this.tbPass);
            this.Controls.Add(this.nUDIntervall);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbUser);
            this.Controls.Add(this.lbMsgDetails);
            this.Controls.Add(this.lNeueEmails);
            this.Controls.Add(this.cbAutoCheck);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.btnCheckNow);
            this.Controls.Add(this.lbFolders);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "CheckMail";
            this.ShowInTaskbar = false;
            this.Text = "CheckMail";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.CheckMail_FormClosed);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.CheckMail_FormClosing);
            this.contextMenuStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.nUDIntervall)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion

        #region CORE-Functions
        /*
         * SendCommand
         * 
         * Schreibt einen �bergebenen String in den Netzwerstream und gibt die Antwort
         * Des Servers zur�ck
         */

		string SendCommand(string command)
		{
            try
            {
                byte[] send = Encoding.ASCII.GetBytes(command);
                str.Write(send, 0, send.Length);

                byte[] receive = new byte[4096];

                int k = str.Read(receive, 0, receive.Length);

                string msg = "";
                for (int i = 0; i < k; i++)
                {
                    msg += Convert.ToChar(receive[i]).ToString();
                }

                return msg;
            }
            catch (Exception e)
            {
                toolStripStatusLabel1.Text = e.Message;
                return "";
            }
		}
		
        /*
         * Login
         * 
         * Erstellt einen neuen TcpClient und Netzwerkstream mit imap.web.de:143 und
         * sendet die Login Daten
         */

		string Login()
		{
            try
            {
                cl = new TcpClient("imap.web.de", 143);
                cl.ReceiveTimeout = 30000;
                cl.SendTimeout = 30000;

                str = cl.GetStream();
                str.ReadTimeout = 45000;
                str.WriteTimeout = 45000;
                byte[] buffer = new byte[4096];

                int k = str.Read(buffer, 0, buffer.Length);

                string msg = "";
                for (int i = 0; i < k; i++)
                {
                    msg += Convert.ToChar(buffer[i]).ToString();
                }

                SendCommand("? LOGIN " + user + " " + pass + "\r\n");

                return msg;
            }
            catch (Exception e)
            {
                toolStripStatusLabel1.Text = e.Message;
                return "";
            }
		}
		
        /*
         * GetUnreadIDs
         * 
         * Nach �bergabe eines Ordners wird dieser selectet und nach ungelesenen Nachrichten durchsucht.
         * Der zur�ckgegebene String wird auf die Nummern gek�rzt und diese durch ein Split
         * in ein int-Array geschrieben welches zur�ck gegeben wird
         */

		int[] GetUnreadIDs(string folder)
		{
			SendCommand("? SELECT "+folder+"\r\n");
			string response = SendCommand("? SEARCH UNSEEN\r\n");
			if(response.Length > 34)
			{
				response = response.Remove(0,9);
				response = response.Remove(response.Length-25, 25);
				string[] collection = response.Split(' ');
				int[] IDs = new int[collection.Length];
				if(collection.Length>0)
				{
					for(int i=0; i<collection.Length; i++)
					{
						IDs[i] = int.Parse(collection[i]);
					}
				}
				return IDs;
			}
			return null;
		}
		
        /*
         * GetFolderList
         * 
         * Hauptfuntion. Nachdem Variablen und Listen zur�ck gesetzt wurden und der Check-Button
         * und das Schlie�en-MenuItem und -Button deaktiviert wurden, wird ein Login gemacht
         * und die Liste Aller Ordner im rot-Verzeichnis abgerufen. Der zur�ckgegebene  String wir
         * durch ein Split aufgeteilt und jedes Element des Erzeugtn Arrays in einer Schleife
         * bearbeitet. In dieser wird er von unn�tigen Teilen befreit und wenn er nicht zu
         * unerw�nschten Ordnern geh�rt auf ungelesene Nachrichten �berpr�ft. Existieren solche,
         * werden diese durch �bergabe des Ordners und s�mtlicher ungelesener IDs in diesem
         * auf Absender und Betreff untersucht und der MsgDetails Liste hinzugef�gt.
         * An die Ordnerliste wird die Zahl der neuen Nachrichten in einer Klammer angeh�ngt und
         * die Zahl der Gesamtemails um die jemweilige Anzahl erh�ht. Diese neue Anzahl wird
         * sofort im entsprechenden Label aktualisiert. Egal ob neue emails vorhaneden waren
         * oder nicht wird der Ordner der Liste hinzugef�gt.
         * Nachdem die Schleife abgearbeitet ist, Zeigt ein Balloon Tip (fette Sache!!!)
         * im Falle neuer Nachrichten die Anzahl und die Absender in der Taskleiste an.
         * Danach wird nur noch der angezeigte Hover Text des Trayicons aktualisiert und
         * der Button und das Schlie�en-MenuItem und -Button wieder aktiviert.
         */

		void GetFolderList()
		{
			neueEmails = 0;
            GlobalSenders = "Von: ";
            this.Invoke(new SetSchlie�enToolStripMenuItemEnabledHandler(SetSchlie�enToolStripMenuItemEnabled), new object[] { false});
            this.Invoke(new SetBtnBeendenEnabledHandler(SetBtnBeendenEnabled), new object[] { false });
            this.Invoke(new SetBtnCheckNowEnabledHandler(SetBtnCheckNowEnabled), new object[] { false });
            this.Invoke(new ClearFolderListHandler(ClearFolderList));
            this.Invoke(new ClearMsgDetailsListHandler(ClearMsgDetailsList));
            this.Invoke(new SetNotifyIconTextHandler(SetNotifyIconText), new object[] { neueEmails.ToString() + " neue Emails" });
            this.Invoke(new SetNeueEmailLabelHandler(SetNeueEmailLabel), new object[] { neueEmails.ToString() });

            if (Login() != "")
            {
                try
                {
                    string list = SendCommand("? LIST \"\" *\r\n");
                    list = list.Remove(list.Length - 22, 22);
                    string[] collection = list.Split('\n');

                    for (int i = 0; i < collection.Length; i++)
                    {
                        collection[i] = collection[i].Remove(0, 27);
                        collection[i] = collection[i].Remove(collection[i].Length - 2, 2);
                        if (collection[i] != "Unerw&APw-nscht" && collection[i] != "Postausgang" && collection[i] != "Gesendet" && collection[i] != "Papierkorb" && collection[i] != "Entwurf")
                        {
                            int[] IDs = GetUnreadIDs(collection[i]);
                            if (IDs != null)
                            {
                                UpdateUnreadMsgDetails(collection[i], IDs);
                                collection[i] += " (" + IDs.Length.ToString() + ")";
                                neueEmails += IDs.Length;
                                this.Invoke(new SetNeueEmailLabelHandler(SetNeueEmailLabel), new object[] { neueEmails.ToString() });
                            }
                            this.Invoke(new AddFolderListItemHandler(AddFolderListItem), new object[] { collection[i] });
                        }
                    }

                    if (neueEmails > 0)
                    {
                        notifyIcon1.ShowBalloonTip(1000, neueEmails.ToString() + " neue Emails", GlobalSenders, ToolTipIcon.Info);
                    }

                    this.Invoke(new SetNotifyIconTextHandler(SetNotifyIconText), new object[] { neueEmails.ToString() + " neue Emails" });

                    Logout();
                }
                catch (Exception e)
                {
                    toolStripStatusLabel1.Text = e.Message;
                }
            }

            this.Invoke(new SetBtnCheckNowEnabledHandler(SetBtnCheckNowEnabled), new object[] { true });
            this.Invoke(new SetSchlie�enToolStripMenuItemEnabledHandler(SetSchlie�enToolStripMenuItemEnabled), new object[] { true });
            this.Invoke(new SetBtnBeendenEnabledHandler(SetBtnBeendenEnabled), new object[] { true });
		}
		
        /*
         * UpdateUnreadMsgDetails
         * 
         * Nachdem ein neuer �berpunkt in der MsgDetails Liste mit dem entsprechenden Ordner als �berschrift angelegt
         * wurde, wird der �bergebene Ordner auf dem Server selectet und eine schleife arbeitet alle
         * durch ein int-Array als ungelesen �bergebenen Emails ab. Dabei werden die von FETCH zur�ck
         * gegebenen Strings f�r SENDER und SUBJECT durch eine Regular Expression von ihrem Drumherum
         * befreit und nachdem das getan ist die Email wieder auf ungelesen gesetzt.
         * Die MsgDetails Liste wird um den entsprechenden Eintrag mit Absender und Betreff erweitert und
         * die Liste mit allen Absendern um den entsprechenden string erweitert.
         */

		void UpdateUnreadMsgDetails(string folder, int[] IDs)
		{
            this.Invoke(new AddMsgDetailsListItemHandler(AddMsgDetailsListItem), new object[] { "Neu in Ordner: " + folder });
            SendCommand("? SELECT " + folder + "\r\n");
            
			for(int i = 0; i<IDs.Length; i++)
			{
				string sender = SendCommand("? FETCH "+IDs[i]+" (FLAGS BODY[HEADER.FIELDS (SENDER)])\r\n");
                Regex re1 = new Regex("Sender: (?<sender>[^\n\r]+)", RegexOptions.IgnoreCase);
				Match m1 = re1.Match(sender);
				sender = m1.Groups["sender"].Value;
				
				string subject = SendCommand("? FETCH "+IDs[i]+" (FLAGS BODY[HEADER.FIELDS (SUBJECT)])\r\n");
                Regex re2 = new Regex("Subject: (?<subject>[^\n\r]+)", RegexOptions.IgnoreCase);
				Match m2 = re2.Match(subject);
				subject = m2.Groups["subject"].Value;
				
				SendCommand("? STORE "+IDs[i]+" -FLAGS (\\SEEN)\r\n");
                this.Invoke(new AddMsgDetailsListItemHandler(AddMsgDetailsListItem), new object[] { " > "+sender + " - " + subject});

                GlobalSenders += sender + " ";
			}
		}

        /*
         * Logout
         * 
         * Ein LOGOUT wird gesendet und danach der Stream und der TcpClient geschlossen.
         */

		void Logout()
		{
			SendCommand("? LOGOUT\r\n");
			str.Close();
			cl.Close();
        }
        #endregion

        #region delgates
        void AddFolderListItem(string text)
        {
            lbFolders.Items.Add(text);
        }

        void AddMsgDetailsListItem(string text)
        {
            lbMsgDetails.Items.Add(text);
        }

        void ClearFolderList()
        {
            lbFolders.Items.Clear();
        }

        void ClearMsgDetailsList()
        {
            lbMsgDetails.Items.Clear();
        }

        void SetNeueEmailLabel(string text)
        {
            lNeueEmails.Text = "Neue Emails: " + text;
        }

        void SetNotifyIconText(string text)
        {
            notifyIcon1.Text = text;
        }

        void SetBtnCheckNowEnabled(bool enabled)
        {
            btnCheckNow.Enabled = enabled;
        }

        void SetSchlie�enToolStripMenuItemEnabled(bool enabled)
        {
            BeendenToolStripMenuItem.Enabled = enabled;
        }

        void SetBtnBeendenEnabled(bool enabled)
        {
            btnBeenden.Enabled = enabled;
        }
        #endregion

        #region form events

        /*
         * BtnCheckNowClick
         * 
         * Benutzername und Passwort werden aus den Textboxen geholt und ein neuer Thread
         * f�r das aktualisieren gestartet, wenn der Button nicht deaktiviert sein sollte,
         * was nur der Fall ist, wenn schon eine �berpr�fung l�uft.
         */

        void BtnCheckNowClick(object sender, EventArgs e)
        {
            user = tbUser.Text;
            pass = tbPass.Text;
            if (btnCheckNow.Enabled)
            {
                Thread t = new Thread(new ThreadStart(GetFolderList));
                t.Start();
            }
        }

        /*
         * timer1_Tick
         * 
         * wird bei jedem Tick von timer1 ausgef�hrt - wie BtnCheckNowClick
         */

        private void timer1_Tick(object sender, EventArgs e)
        {
            user = tbUser.Text;
            pass = tbPass.Text;
            if (btnCheckNow.Enabled)
            {
                t = new Thread(new ThreadStart(GetFolderList));
                t.Start();
            }
        }

        /*
         * BtnOKClick
         * 
         * Die Anwendung wird in das TrayIcon minimiert
         */

        void BtnOKClick(object sender, System.EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            this.Visible = false;
        }

        /*
         * cbAutoCheck_CheckedChanged
         * 
         * je nachdem ob checked oder nicht wird die IntervallNumericUpDown zusammen
         * mit dem Timer an und aus gestellt wenn sich checked �ndert.
         */

        private void cbAutoCheck_CheckedChanged(object sender, EventArgs e)
        {
            nUDIntervall.Enabled = cbAutoCheck.Checked;
            timer1.Enabled = cbAutoCheck.Checked;
        }

        /*
         * nUDIntervall_ValueChanged
         * 
         * Wird der Wert des numericUPDown g�ndert so wird dieser von Minuten in Millisekunden
         * umgerechnet sofort in den Interval Wert von timer1 �bertragen
         */

        private void nUDIntervall_ValueChanged(object sender, EventArgs e)
        {
            timer1.Interval = Convert.ToInt32(nUDIntervall.Value) * 60000;
        }

        /*
         * btnBeenden_Click
         * 
         * Beendet das Programm
         */

        private void btnBeenden_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #region notifyIcon related
        /*
         * notifyIcon1_DoubleClick
         * 
         * Holt das Programm egal aus welchem WindowState in den Vordergrund
         */

        private void notifyIcon1_DoubleClick(object sender, EventArgs e)
        {
            this.Visible = true;
            this.WindowState = FormWindowState.Normal;
            this.BringToFront();
        }

        /*
         * minimierenToolStripMenuItem_Click
         * 
         * �berpr�ft den WindowState und �ndert diesen zusammen mit der Sichtbarkeit 
         * und dem ToolStripMenuItem-Text dementsprechend
         */

        private void minimierenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Normal)
            {
                this.WindowState = FormWindowState.Minimized;
                this.Visible = false;
                this.minimierenToolStripMenuItem.Text = "Wiederherstellen";
            }
            else
            {
                this.Visible = true;
                this.WindowState = FormWindowState.Normal;
                this.minimierenToolStripMenuItem.Text = "Minimieren";
            }
        }

        /*
         * tothefrontToolStripMenuItem_Click
         * 
         * Holt das Programm egal aus welchem WindowState in den Vordergrund
         */

        private void tothefrontToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Visible = true;
            this.WindowState = FormWindowState.Normal;
            this.BringToFront();
        }

        /*
         * �berToolStripMenuItem_Click
         * 
         * �ffnet den �ber... Dialog
         */

        private void �berToolStripMenuItem_Click(object sender, EventArgs e)
        {
            �ber �ber = new �ber();
            �ber.ShowDialog();
        }

        /*
         * toolStripMenuItem1_Click
         * 
         * Beendet das Programm
         */

        private void BeendenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion

        /*
         * CheckMail_FormClosed
         * 
         * Wenn die Form geschlossen ist
         */

        private void CheckMail_FormClosed(object sender, FormClosedEventArgs e)
        {
            notifyIcon1.Dispose();
            
            if (str != null)
            {
                str.Close();
            }
            if (cl != null)
            {
                cl.Close();
            }
        }

        /*
         * CheckMail_FormClosing
         * 
         * Bevor die Form geschlossen ist. Speichert Benutzerdaten.
         */

        private void CheckMail_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (t != null)
            {
                t.Abort();
            }
            sett.user = tbUser.Text;
            sett.pass = tbPass.Text;
            sett.Save();
        }

        /*
         * Damit kein automatisches Checken erfolgt, w�hrend Werte ge�ndert werden
         */

        private void tbUser_TextChanged(object sender, EventArgs e)
        {
            cbAutoCheck.Checked = false;
        }

        private void tbPass_TextChanged(object sender, EventArgs e)
        {
            cbAutoCheck.Checked = false;
        }

        #endregion


    }
}
