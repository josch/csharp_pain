using System;
using System.Collections.Generic;
using System.Text;
using System.Net;

namespace IpWorks
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                uint baseip = 0;
                uint BorrowedBits = 0;
                uint fixedbits = 0;
                uint subnet = 0;
                uint extendedsubnet = 0;

                Console.WriteLine("Bitte Netzwerkadresse eingeben");
                string sIP = Console.ReadLine();
                IPAddress ipNetwork;

                if (!IPAddress.TryParse(sIP, out ipNetwork))
                { Console.WriteLine("IP ung�ltig"); break; }

                uint baseip1 = ipNetwork.GetAddressBytes()[0];
                uint baseip2 = ipNetwork.GetAddressBytes()[1];
                uint baseip3 = ipNetwork.GetAddressBytes()[2];
                uint baseip4 = ipNetwork.GetAddressBytes()[3];
                baseip = (baseip1 << 24) | (baseip2 << 16) | (baseip3 << 8) | baseip4;

                Console.WriteLine("IP: {0}.{1}.{2}.{3}", Convert.ToString(baseip1), Convert.ToString(baseip2), Convert.ToString(baseip3), Convert.ToString(baseip4));

                Console.WriteLine("Erste drei Bits: {0}", Convert.ToString(ipNetwork.GetAddressBytes()[0] >> 5, 2));

                fixedbits = 0;
                subnet = 0;

                if (baseip1 >> 7 == 0) //Klasse A
                {
                    fixedbits = 8;
                    Console.WriteLine("A-Klasse - Standart-Subnet:");
                }
                else if (baseip1 >> 6 == 2) //Klasse B
                {
                    fixedbits = 16;
                    Console.WriteLine("B-Klasse - Standart-Subnet:");
                }
                else if (baseip1 >> 5 == 6)  //Klasse C
                {
                    fixedbits = 24;
                    Console.WriteLine("C-Klasse - Standart-Subnet:");
                }

                baseip = (uint)((int)baseip >> (int)(32 - fixedbits)) << (int)(32 - fixedbits);
                baseip1 = baseip >> 24;
                baseip2 = (baseip & 16711680) >> 16;
                baseip3 = (baseip & 65280) >> 8;
                baseip4 = (baseip & 255);

                Console.WriteLine("Korrigierte Netzwerkadresse: {0}.{1}.{2}.{3}", Convert.ToString(baseip1), Convert.ToString(baseip2), Convert.ToString(baseip3), Convert.ToString(baseip4));

                subnet = (uint)((int)(Math.Pow(2, fixedbits) - 1) << (int)(32 - fixedbits));
                uint subnet1 = subnet >> 24;
                uint subnet2 = (subnet & 16711680) >> 16;
                uint subnet3 = (subnet & 65280) >> 8;
                uint subnet4 = (subnet & 255);

                Console.WriteLine("{0}.{1}.{2}.{3}", Convert.ToString(subnet1), subnet2.ToString(), subnet3.ToString(), subnet4.ToString());

                Console.WriteLine("Anzahl der zu borgenden Bits eingeben (maximal {0})", Convert.ToString(30-fixedbits));
                BorrowedBits = Convert.ToUInt32(Console.ReadLine());
                if (BorrowedBits > (30 - fixedbits))
                { break; }

                extendedsubnet = subnet | (uint)((int)(Math.Pow(2, BorrowedBits)-1) << (int)(32-fixedbits-BorrowedBits));
                uint extsubnet1 = extendedsubnet >> 24;
                uint extsubnet2 = (extendedsubnet & 16711680) >> 16;
                uint extsubnet3 = (extendedsubnet & 65280) >> 8;
                uint extsubnet4 = (extendedsubnet & 255);

                Console.WriteLine("Erweiterte Subnetzmaske: {0}.{1}.{2}.{3}", Convert.ToString(extsubnet1), Convert.ToString(extsubnet2), Convert.ToString(extsubnet3), Convert.ToString(extsubnet4));
                Console.WriteLine("M�gliche Netze: {0}", Math.Pow(2, BorrowedBits).ToString());
                Console.WriteLine("Hosts pro Netz: {0}", (Math.Pow(2, 32 - BorrowedBits - fixedbits) - 2).ToString());

                for (int i = 0; i < Math.Pow(2, BorrowedBits); i++)
                {
                    uint tempip = baseip | (uint)(i << (int)(32 - fixedbits - BorrowedBits));
                    uint tempip1 = tempip >> 24;
                    uint tempip2 = (tempip & 16711680) >> 16;
                    uint tempip3 = (tempip & 65280) >> 8;
                    uint tempip4 = (tempip & 255);
                    Console.WriteLine("{0}.{1}.{2}.{3}", Convert.ToString(tempip1), Convert.ToString(tempip2), Convert.ToString(tempip3), Convert.ToString(tempip4));
                }
            }
            Console.ReadLine();
        }
    }
}
