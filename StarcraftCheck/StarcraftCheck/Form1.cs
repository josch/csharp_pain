using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Media;
using System.Diagnostics;

namespace StarcraftChat
{
    public partial class Form1 : Form
    {
        Thread t;
        SoundPlayer sp = new SoundPlayer(Properties.Resources.zocken);

        UdpClient udp = new UdpClient(6111, AddressFamily.InterNetwork);
        
        public Form1()
        {
            InitializeComponent();
            t = new Thread(new ThreadStart(Listen));
            t.Start();
        }

        private void Listen()
        {
            IPEndPoint ipep = new IPEndPoint(IPAddress.Parse("192.168.0.255"), 6111);
            while (true)
            {
                byte[] buffer = udp.Receive(ref ipep);
                if (buffer.Length > 20)
                {
                    sp.Play();
                    MessageBox.Show("Ein neues Starcraft ist offen!!!");
                }
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
            {
                udp.Send(new byte[] {0x97, 0x43, 0x14, 0x00, 0x02, 0x00,
0x00, 0x00, 0x50, 0x58, 0x45, 0x53, 0xcd, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}, 20, new IPEndPoint(IPAddress.Parse("192.168.0.255"), 6111));
            }
            bool found = false;
            foreach(Process p in Process.GetProcesses())
            {
                if(p.ProcessName == "starcraft")
                {
                    found = true;
                    break;
                }
            }
            checkBox1.Checked = !found;
            label1.Text = found ? "Starcraft l�uft" : "";
            if (found)
            {
                udp.Close();
            }
            else
            {
                try
                {
                    udp = new UdpClient();
                }
                catch { }
            }
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
            {
                t.Resume();
            }
            else
            {
                t.Suspend();
            }
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            t.Abort();
            udp.Close();
        }
    }
}