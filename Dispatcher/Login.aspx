<%@ Page Language="C#" %>
<%@ import Namespace="System.Data" %>
<%@ import Namespace="System.Data.SqlClient" %>
<script runat="server">

    SqlConnection connection;
    SqlCommand command;
    
    void Page_Load(Object sender, EventArgs e) {
        connection=new SqlConnection(ConfigurationSettings.AppSettings["Connection"]);
    
        command=new SqlCommand("SELECT Password FROM Dispatcher WHERE Alias=@Alias",connection);
        command.Parameters.Add("@Alias",SqlDbType.VarChar,10);
    }
    
    void BtLogin_Click(Object sender, EventArgs e) {
        connection.Open();
    
        command.Parameters["@Alias"].Value=tbAlias.Text;
        if(tbPassword.Text==(string)command.ExecuteScalar()) {
            Response.Redirect("default.aspx");
        } else {
            Session.Abandon();
    
            Response.Redirect("Login.aspx");
        }
    
        connection.Close();
    }

</script>
<html>
<head>
</head>
<body>
    <form runat="server">
        <p>
            <font face="Tahoma">Alias&nbsp;<asp:TextBox id="tbAlias" runat="server"></asp:TextBox>
            <br />
            Password&nbsp;<asp:TextBox id="tbPassword" runat="server" TextMode="Password"></asp:TextBox>
            <br />
            <asp:Button id="btLogin" onclick="BtLogin_Click" runat="server" Font-Names="Tahoma" Text="Login"></asp:Button>
            </font>
        </p>
    </form>
</body>
</html>
