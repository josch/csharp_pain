<%@ Page Language="C#" %>
<script runat="server">

    void BtLogout_Click(object sender, EventArgs e) {
            Session.Abandon();
    
            Response.Redirect("Login.aspx");
    }

</script>
<html>
<head>
</head>
<body>
    <form runat="server">
        <p>
            <asp:HyperLink id="hlDispatcher" runat="server" NavigateUrl="dispatcher.aspx">Manage Dispatcher</asp:HyperLink>
            <br />
            <asp:HyperLink id="hlReporter" runat="server" NavigateUrl="reporter.aspx">Manage Reporter</asp:HyperLink>
        </p>
        <p>
            <br />
            <asp:HyperLink id="hlTrains" runat="server" NavigateUrl="trains.aspx">Manage Trains</asp:HyperLink>
        </p>
        <p>
            <br />
            <asp:Button id="btLogout" onclick="BtLogout_Click" runat="server" Text="Logout"></asp:Button>
        </p>
    </form>
</body>
</html>
