﻿<%@ Page Language="C#" %>
<%@ import Namespace="System.Data" %>
<%@ import Namespace="System.Data.SqlClient" %>
<script runat="server">

    SqlConnection connection;
    
    void Page_Load(Object sender,EventArgs e) {
        connection=new SqlConnection("Data Source=localhost;Initial Catalog=DynaDis;Integrated Security=true");
    
        if(!IsPostBack) {
            DataGrid_Bind();
        }
    }
    
    void DataGrid_Bind() {
        SqlDataAdapter dataAdapter=new SqlDataAdapter("SELECT * FROM Dispatcher",connection);
    
        DataTable dataTable=new DataTable();
        dataAdapter.Fill(dataTable);
    
        dataGrid.DataSource=dataTable.DefaultView;
        dataGrid.DataBind();
    }
    
    void DataGrid_Edit(Object sender, DataGridCommandEventArgs e) {
        dataGrid.EditItemIndex=(int)e.Item.ItemIndex;
        DataGrid_Bind();
    }
    
    void DataGrid_Cancel(Object sender, DataGridCommandEventArgs e) {
        dataGrid.EditItemIndex=-1;
        DataGrid_Bind();
    }
    
    void DataGrid_Update(Object sender, DataGridCommandEventArgs e) {
        SqlCommand command=new SqlCommand("UpdateDispatcher",connection);
        command.CommandType=CommandType.StoredProcedure;
        command.Parameters.Add("@Id",SqlDbType.Int,4,"Id");
        command.Parameters.Add("@FirstName",SqlDbType.VarChar,50,"FirstName");
        command.Parameters.Add("@LastName",SqlDbType.VarChar,50,"LastName");
        command.Parameters.Add("@Street",SqlDbType.VarChar,50,"Street");
        command.Parameters.Add("@City",SqlDbType.VarChar,50,"City");
        command.Parameters.Add("@Alias",SqlDbType.VarChar,10,"Alias");
        command.Parameters.Add("@Password",SqlDbType.VarChar,10,"Password");
    
        command.Parameters["@Id"].Value=dataGrid.DataKeys[(int)e.Item.ItemIndex];
    
        for(int i=2;i<e.Item.Cells.Count;i++) {
            command.Parameters[i-1].Value=((System.Web.UI.WebControls.TextBox)e.Item.Cells[i].Controls[0]).Text;
        }
    
        connection.Open();
        command.ExecuteNonQuery();
        connection.Close();
    
        dataGrid.EditItemIndex=-1;
        DataGrid_Bind();
    }
    
    void DataGrid_Delete(Object sender, DataGridCommandEventArgs e)
    {
        SqlCommand command = new SqlCommand("DELETE FROM Dispatcher WHERE Id=@Id",connection);
        command.Parameters.Add("@Id",SqlDbType.Int,4);
    
        command.Parameters["@Id"].Value=dataGrid.DataKeys[(int)e.Item.ItemIndex];
    
        connection.Open();
        command.ExecuteNonQuery();
        connection.Close();
    
        DataGrid_Bind();
    }
    
    void btInsert_Click(object sender, EventArgs e) {
        SqlCommand command=new SqlCommand("InsertDispatcher", connection);
        command.CommandType=CommandType.StoredProcedure;
        command.Parameters.Add("@Id",SqlDbType.Int,4).Direction=ParameterDirection.Output;
        command.Parameters.Add("@FirstName",SqlDbType.VarChar,50);
        command.Parameters.Add("@LastName",SqlDbType.VarChar,50);
        command.Parameters.Add("@Street",SqlDbType.VarChar,50);
        command.Parameters.Add("@City",SqlDbType.VarChar,50);
        command.Parameters.Add("@Alias",SqlDbType.VarChar,10);
        command.Parameters.Add("@Password",SqlDbType.VarChar,10);
    
        command.Parameters["@FirstName"].Value=tbFirstName.Text;
        command.Parameters["@LastName"].Value=tbLastName.Text;
        command.Parameters["@Street"].Value=tbStreet.Text;
        command.Parameters["@City"].Value=tbCity.Text;
        command.Parameters["@Alias"].Value=tbAlias.Text;
        command.Parameters["@Password"].Value=tbPassword.Text;
    
        connection.Open();
        command.ExecuteNonQuery();
        connection.Close();
    
        DataGrid_Bind();
    }

</script>
<html>
<head>
</head>
<body>
    <form runat="server">
        <table>
            <tbody>
                <tr>
                    <td rowspan="7">
                        <asp:DataGrid id="dataGrid" runat="server" OnCancelCommand="DataGrid_Cancel" OnEditCommand="DataGrid_Edit" OnUpdateCommand="DataGrid_Update" OnDeleteCommand="DataGrid_Delete" AutoGenerateColumns="false" DataKeyField="Id">
                            <Columns>
                                <asp:EditCommandColumn EditText="Edit" CancelText="Cancel" UpdateText="Update" ItemStyle-Wrap="false" />
                                <asp:ButtonColumn Text="Delete" CommandName="Delete" />
                                
                                <asp:BoundColumn HeaderText="Id" DataField="Id" ReadOnly="True" ItemStyle-Wrap="false" />
                                <asp:BoundColumn HeaderText="FirstName" DataField="FirstName" />
                                <asp:BoundColumn HeaderText="LastName" DataField="LastName" />
                                <asp:BoundColumn HeaderText="Street" DataField="Street" />
                                <asp:BoundColumn HeaderText="City" DataField="City" />
                                <asp:BoundColumn HeaderText="Alias" DataField="Alias" />
                                <asp:BoundColumn HeaderText="Password" DataField="Password" />
                            </Columns>
                        </asp:DataGrid>
                    </td>
                    <td valign="top">
                        <table>
                            <tbody>
                                <tr>
                                    <td>
                                        First Name
                                    </td>
                                    <td>
                                        <asp:TextBox id="tbFirstName" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        LastName
                                    </td>
                                    <td>
                                        <asp:TextBox id="tbLastName" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Street
                                    </td>
                                    <td>
                                        <asp:TextBox id="tbStreet" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        City
                                    </td>
                                    <td>
                                        <asp:TextBox id="tbCity" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Alias
                                    </td>
                                    <td>
                                        <asp:TextBox id="tbAlias" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Password
                                    </td>
                                    <td>
                                        <asp:TextBox id="tbPassword" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" colspan="2">
                                        <asp:Button id="btInsert" onclick="btInsert_Click" runat="server" Text="Insert"></asp:Button>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" colspan="2">
                                        <br />
                                        <asp:HyperLink id="ZurueckHl" runat="server" NavigateUrl="default.aspx">zurück zum Hauptmenü</asp:HyperLink>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
    </form>
</body>
</html>
