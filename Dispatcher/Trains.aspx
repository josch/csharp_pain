﻿<%@ Page Language="C#" %>
<%@ import Namespace="System.Data" %>
<%@ import Namespace="System.Data.SqlClient" %>
<script runat="server">

    SqlConnection connection;
    
    void Page_Load(Object sender,EventArgs e) {
        connection=new SqlConnection("Data Source=localhost;Initial Catalog=DynaDis;Integrated Security=true");
    
        if(!IsPostBack) {
            DataGrid_Bind();
        }
    }
    
    void DataGrid_Bind() {
        SqlDataAdapter dataAdapter=new SqlDataAdapter("SELECT * FROM Trains",connection);
    
        DataTable dataTable=new DataTable();
        dataAdapter.Fill(dataTable);
    
        dataGrid.DataSource=dataTable.DefaultView;
        dataGrid.DataBind();
    }
    
    void DataGrid_Edit(Object sender, DataGridCommandEventArgs e) {
        dataGrid.EditItemIndex=(int)e.Item.ItemIndex;
        DataGrid_Bind();
    }
    
    void DataGrid_Cancel(Object sender, DataGridCommandEventArgs e) {
        dataGrid.EditItemIndex=-1;
        DataGrid_Bind();
    }
    
    void DataGrid_Update(Object sender, DataGridCommandEventArgs e) {
        SqlCommand command=new SqlCommand("UpdateTrains",connection);
        command.CommandType=CommandType.StoredProcedure;
        command.Parameters.Add("@Id",SqlDbType.Int,4,"Id");
        command.Parameters.Add("@Departure",SqlDbType.VarChar,50,"Departure");
        command.Parameters.Add("@DepartureStation",SqlDbType.VarChar,50,"DepartureStation");
        command.Parameters.Add("@Arrival",SqlDbType.VarChar,50,"Arrival");
        command.Parameters.Add("@ArrivalStation",SqlDbType.VarChar,50,"ArrivalStation");
        command.Parameters.Add("@Available",SqlDbType.VarChar,10,"Available");
        command.Parameters.Add("@Planned",SqlDbType.VarChar,10,"Planned");
    
        command.Parameters["@Id"].Value=dataGrid.DataKeys[(int)e.Item.ItemIndex];
    
        for(int i=2;i<e.Item.Cells.Count;i++) {
            command.Parameters[i-1].Value=((System.Web.UI.WebControls.TextBox)e.Item.Cells[i].Controls[0]).Text;
        }
    
        connection.Open();
        command.ExecuteNonQuery();
        connection.Close();
    
        dataGrid.EditItemIndex=-1;
        DataGrid_Bind();
    }
    
    void btInsert_Click(object sender, EventArgs e) {
        SqlCommand command=new SqlCommand("InsertTrains", connection);
        command.CommandType=CommandType.StoredProcedure;
        command.Parameters.Add("@Id",SqlDbType.Int,4).Direction=ParameterDirection.Output;
        command.Parameters.Add("@Departure",SqlDbType.VarChar,50);
        command.Parameters.Add("@DepartureStation",SqlDbType.VarChar,50);
        command.Parameters.Add("@Arrival",SqlDbType.VarChar,50);
        command.Parameters.Add("@ArrivalStation",SqlDbType.VarChar,50);
        command.Parameters.Add("@Available",SqlDbType.VarChar,10);
        command.Parameters.Add("@Planned",SqlDbType.VarChar,10);
    
        command.Parameters["@Departure"].Value=tbDeparture.Text;
        command.Parameters["@DepartureStation"].Value=tbDepartureStation.Text;
        command.Parameters["@Arrival"].Value=tbArrival.Text;
        command.Parameters["@ArrivalStation"].Value=tbArrivalStation.Text;
        command.Parameters["@Available"].Value=tbAvailable.Text;
        command.Parameters["@Planned"].Value=tbPlanned.Text;
    
        connection.Open();
        command.ExecuteNonQuery();
        connection.Close();
    
        DataGrid_Bind();
    }

</script>
<html>
<head>
</head>
<body>
    <form runat="server">
        <table>
            <tbody>
                <tr>
                    <td rowspan="7">
                        <asp:DataGrid id="dataGrid" runat="server" AutoGenerateColumns="false" OnUpdateCommand="DataGrid_Update" OnCancelCommand="DataGrid_Cancel" OnEditCommand="DataGrid_Edit" DataKeyField="Id">
                            <Columns>
                                <asp:EditCommandColumn EditText="Edit" CancelText="Cancel" UpdateText="Update" ItemStyle-Wrap="false" />
                                <asp:ButtonColumn Text="Delete" CommandName="Delete" />
                                <asp:BoundColumn HeaderText="Id" DataField="Id" ReadOnly="True" ItemStyle-Wrap="false" />
                                <asp:BoundColumn HeaderText="Departure" DataField="Departure" />
                                <asp:BoundColumn HeaderText="Departure Station" DataField="DepartureStation" />
                                <asp:BoundColumn HeaderText="Arrival" DataField="Arrival" />
                                <asp:BoundColumn HeaderText="Arrival Station" DataField="ArrivalStation" />
                                <asp:BoundColumn HeaderText="Available" DataField="Available" />
                                <asp:BoundColumn HeaderText="Planned" DataField="Planned" />
                            </Columns>
                        </asp:DataGrid>
                    </td>
                    <td valign="top">
                        <table>
                            <tbody>
                                <tr>
                                    <td>
                                        Departure
                                    </td>
                                    <td>
                                        <asp:TextBox id="tbDeparture" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Departure Station
                                    </td>
                                    <td>
                                        <asp:TextBox id="tbDepartureStation" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Arrival
                                    </td>
                                    <td>
                                        <asp:TextBox id="tbArrival" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Arrival Station
                                    </td>
                                    <td>
                                        <asp:TextBox id="tbArrivalStation" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Available
                                    </td>
                                    <td>
                                        <asp:TextBox id="tbAvailable" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Planned
                                    </td>
                                    <td>
                                        <asp:TextBox id="tbPlanned" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" colspan="2">
                                        <asp:Button id="btInsert" onclick="btInsert_Click" runat="server" Text="Insert"></asp:Button>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" colspan="2">
                                        <br />
                                        <asp:HyperLink id="ZurueckHl" runat="server" NavigateUrl="default.aspx">zurück zum Hauptmenü</asp:HyperLink>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
    </form>
</body>
</html>
