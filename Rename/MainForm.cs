using System;
using System.IO;
using System.Drawing;
using System.Windows.Forms;

namespace Rename
{
	public class MainForm : System.Windows.Forms.Form
	{
		private System.Windows.Forms.ColumnHeader columnHeader2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.TextBox StaticTextBox;
		private System.Windows.Forms.GroupBox UebertragenFilterGroupBox;
		private System.Windows.Forms.ColumnHeader NameColumnHeader;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Label VorschauLabel;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.Button AlleEntfernenButton;
		private System.Windows.Forms.Button EntfernenButton;
		private System.Windows.Forms.Button AlleUebertragenButton;
		private System.Windows.Forms.TextBox EnthaeltTextBox;
		private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.ListView VorschauListView;
		private System.Windows.Forms.ColumnHeader GroesseColumnHeader;
		private System.Windows.Forms.ComboBox ExtensionComboBox;
		private System.Windows.Forms.Button TestenButton;
		private System.Windows.Forms.NumericUpDown FromNumericUpDown;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.ListView FileListView;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Button DurchsuchenButton;
		private System.Windows.Forms.ListView SourceListView;
		private System.Windows.Forms.Button UebertragenButton;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Button SelectedUpButton;
		private System.Windows.Forms.ColumnHeader columnHeader1;
		private System.Windows.Forms.PictureBox VorschauPictureBox;
		private System.Windows.Forms.ColumnHeader columnHeader3;
		private System.Windows.Forms.Button SelectedDownButton;
		private System.Windows.Forms.Button AktualisierenButton;
		private System.Windows.Forms.TextBox FolderTextBox;
		private System.Windows.Forms.NumericUpDown ToNumericUpDown;
		private System.Windows.Forms.CheckBox EnthaeltCheckBox;
		
		public MainForm()
		{
			InitializeComponent();
		}
		
		[STAThread]
		public static void Main(string[] args)
		{
			Application.Run(new MainForm());
		}
		
		#region Windows Forms Designer generated code
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent() {
			this.EnthaeltCheckBox = new System.Windows.Forms.CheckBox();
			this.ToNumericUpDown = new System.Windows.Forms.NumericUpDown();
			this.FolderTextBox = new System.Windows.Forms.TextBox();
			this.AktualisierenButton = new System.Windows.Forms.Button();
			this.SelectedDownButton = new System.Windows.Forms.Button();
			this.columnHeader3 = new System.Windows.Forms.ColumnHeader();
			this.VorschauPictureBox = new System.Windows.Forms.PictureBox();
			this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
			this.SelectedUpButton = new System.Windows.Forms.Button();
			this.label2 = new System.Windows.Forms.Label();
			this.UebertragenButton = new System.Windows.Forms.Button();
			this.SourceListView = new System.Windows.Forms.ListView();
			this.DurchsuchenButton = new System.Windows.Forms.Button();
			this.panel1 = new System.Windows.Forms.Panel();
			this.FileListView = new System.Windows.Forms.ListView();
			this.label4 = new System.Windows.Forms.Label();
			this.FromNumericUpDown = new System.Windows.Forms.NumericUpDown();
			this.TestenButton = new System.Windows.Forms.Button();
			this.ExtensionComboBox = new System.Windows.Forms.ComboBox();
			this.GroesseColumnHeader = new System.Windows.Forms.ColumnHeader();
			this.VorschauListView = new System.Windows.Forms.ListView();
			this.label3 = new System.Windows.Forms.Label();
			this.folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
			this.EnthaeltTextBox = new System.Windows.Forms.TextBox();
			this.AlleUebertragenButton = new System.Windows.Forms.Button();
			this.EntfernenButton = new System.Windows.Forms.Button();
			this.AlleEntfernenButton = new System.Windows.Forms.Button();
			this.button2 = new System.Windows.Forms.Button();
			this.VorschauLabel = new System.Windows.Forms.Label();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.NameColumnHeader = new System.Windows.Forms.ColumnHeader();
			this.UebertragenFilterGroupBox = new System.Windows.Forms.GroupBox();
			this.StaticTextBox = new System.Windows.Forms.TextBox();
			this.label5 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.columnHeader2 = new System.Windows.Forms.ColumnHeader();
			((System.ComponentModel.ISupportInitialize)(this.ToNumericUpDown)).BeginInit();
			this.panel1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.FromNumericUpDown)).BeginInit();
			this.groupBox1.SuspendLayout();
			this.UebertragenFilterGroupBox.SuspendLayout();
			this.SuspendLayout();
			// 
			// EnthaeltCheckBox
			// 
			this.EnthaeltCheckBox.Location = new System.Drawing.Point(8, 32);
			this.EnthaeltCheckBox.Name = "EnthaeltCheckBox";
			this.EnthaeltCheckBox.Size = new System.Drawing.Size(64, 16);
			this.EnthaeltCheckBox.TabIndex = 0;
			this.EnthaeltCheckBox.Text = "enthält:";
			this.EnthaeltCheckBox.CheckedChanged += new System.EventHandler(this.EnthaeltCheckBoxCheckedChanged);
			// 
			// ToNumericUpDown
			// 
			this.ToNumericUpDown.Enabled = false;
			this.ToNumericUpDown.Location = new System.Drawing.Point(168, 64);
			this.ToNumericUpDown.Maximum = new System.Decimal(new int[] {
						10000,
						0,
						0,
						0});
			this.ToNumericUpDown.Minimum = new System.Decimal(new int[] {
						1,
						0,
						0,
						-2147483648});
			this.ToNumericUpDown.Name = "ToNumericUpDown";
			this.ToNumericUpDown.ReadOnly = true;
			this.ToNumericUpDown.Size = new System.Drawing.Size(56, 20);
			this.ToNumericUpDown.TabIndex = 7;
			this.ToNumericUpDown.Value = new System.Decimal(new int[] {
						1,
						0,
						0,
						-2147483648});
			// 
			// FolderTextBox
			// 
			this.FolderTextBox.Location = new System.Drawing.Point(8, 352);
			this.FolderTextBox.Name = "FolderTextBox";
			this.FolderTextBox.Size = new System.Drawing.Size(256, 20);
			this.FolderTextBox.TabIndex = 0;
			this.FolderTextBox.Text = "";
			// 
			// AktualisierenButton
			// 
			this.AktualisierenButton.Location = new System.Drawing.Point(80, 384);
			this.AktualisierenButton.Name = "AktualisierenButton";
			this.AktualisierenButton.Size = new System.Drawing.Size(88, 23);
			this.AktualisierenButton.TabIndex = 3;
			this.AktualisierenButton.Text = "Aktualisieren";
			this.AktualisierenButton.Click += new System.EventHandler(this.AktualisierenButtonClick);
			// 
			// SelectedDownButton
			// 
			this.SelectedDownButton.Enabled = false;
			this.SelectedDownButton.Location = new System.Drawing.Point(576, 168);
			this.SelectedDownButton.Name = "SelectedDownButton";
			this.SelectedDownButton.Size = new System.Drawing.Size(24, 40);
			this.SelectedDownButton.TabIndex = 11;
			this.SelectedDownButton.Text = "٧";
			this.SelectedDownButton.Click += new System.EventHandler(this.SelectedDownButtonClick);
			// 
			// columnHeader3
			// 
			this.columnHeader3.Text = "Umbenannt";
			this.columnHeader3.Width = 114;
			// 
			// VorschauPictureBox
			// 
			this.VorschauPictureBox.BackColor = System.Drawing.SystemColors.Control;
			this.VorschauPictureBox.Location = new System.Drawing.Point(0, 0);
			this.VorschauPictureBox.Name = "VorschauPictureBox";
			this.VorschauPictureBox.Size = new System.Drawing.Size(320, 240);
			this.VorschauPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
			this.VorschauPictureBox.TabIndex = 12;
			this.VorschauPictureBox.TabStop = false;
			// 
			// columnHeader1
			// 
			this.columnHeader1.Text = "Name";
			this.columnHeader1.Width = 125;
			// 
			// SelectedUpButton
			// 
			this.SelectedUpButton.Enabled = false;
			this.SelectedUpButton.Location = new System.Drawing.Point(576, 120);
			this.SelectedUpButton.Name = "SelectedUpButton";
			this.SelectedUpButton.Size = new System.Drawing.Size(24, 40);
			this.SelectedUpButton.TabIndex = 10;
			this.SelectedUpButton.Text = "٨";
			this.SelectedUpButton.Click += new System.EventHandler(this.SelectedUpButtonClick);
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(40, 32);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(56, 23);
			this.label2.TabIndex = 2;
			this.label2.Text = "Statisch: ";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// UebertragenButton
			// 
			this.UebertragenButton.Enabled = false;
			this.UebertragenButton.Location = new System.Drawing.Point(272, 48);
			this.UebertragenButton.Name = "UebertragenButton";
			this.UebertragenButton.Size = new System.Drawing.Size(112, 23);
			this.UebertragenButton.TabIndex = 6;
			this.UebertragenButton.Text = "Übertragen >";
			this.UebertragenButton.Click += new System.EventHandler(this.UebertragenButtonClick);
			// 
			// SourceListView
			// 
			this.SourceListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
						this.columnHeader2});
			this.SourceListView.GridLines = true;
			this.SourceListView.HideSelection = false;
			this.SourceListView.Location = new System.Drawing.Point(392, 8);
			this.SourceListView.Name = "SourceListView";
			this.SourceListView.Size = new System.Drawing.Size(176, 336);
			this.SourceListView.TabIndex = 15;
			this.SourceListView.View = System.Windows.Forms.View.Details;
			this.SourceListView.ItemActivate += new System.EventHandler(this.SourceListViewItemActivate);
			this.SourceListView.SelectedIndexChanged += new System.EventHandler(this.SourceListViewSelectedIndexChanged);
			// 
			// DurchsuchenButton
			// 
			this.DurchsuchenButton.Location = new System.Drawing.Point(176, 384);
			this.DurchsuchenButton.Name = "DurchsuchenButton";
			this.DurchsuchenButton.Size = new System.Drawing.Size(88, 23);
			this.DurchsuchenButton.TabIndex = 1;
			this.DurchsuchenButton.Text = "Durchsuchen";
			this.DurchsuchenButton.Click += new System.EventHandler(this.DurchsuchenButtonClick);
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.VorschauPictureBox);
			this.panel1.Location = new System.Drawing.Point(280, 352);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(320, 240);
			this.panel1.TabIndex = 13;
			// 
			// FileListView
			// 
			this.FileListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
						this.NameColumnHeader,
						this.GroesseColumnHeader});
			this.FileListView.Enabled = false;
			this.FileListView.GridLines = true;
			this.FileListView.HideSelection = false;
			this.FileListView.Location = new System.Drawing.Point(8, 8);
			this.FileListView.Name = "FileListView";
			this.FileListView.Size = new System.Drawing.Size(256, 336);
			this.FileListView.Sorting = System.Windows.Forms.SortOrder.Ascending;
			this.FileListView.TabIndex = 2;
			this.FileListView.View = System.Windows.Forms.View.Details;
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(24, 96);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(72, 23);
			this.label4.TabIndex = 6;
			this.label4.Text = "Dateiendung:";
			this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// FromNumericUpDown
			// 
			this.FromNumericUpDown.Location = new System.Drawing.Point(72, 64);
			this.FromNumericUpDown.Maximum = new System.Decimal(new int[] {
						10000,
						0,
						0,
						0});
			this.FromNumericUpDown.Name = "FromNumericUpDown";
			this.FromNumericUpDown.Size = new System.Drawing.Size(56, 20);
			this.FromNumericUpDown.TabIndex = 3;
			this.FromNumericUpDown.ValueChanged += new System.EventHandler(this.FromNumericUpDownValueChanged);
			this.FromNumericUpDown.Leave += new System.EventHandler(this.FromNumericUpDownLeave);
			// 
			// TestenButton
			// 
			this.TestenButton.Location = new System.Drawing.Point(24, 160);
			this.TestenButton.Name = "TestenButton";
			this.TestenButton.Size = new System.Drawing.Size(72, 23);
			this.TestenButton.TabIndex = 11;
			this.TestenButton.Text = "Testen";
			this.TestenButton.Click += new System.EventHandler(this.TestenButtonClick);
			// 
			// ExtensionComboBox
			// 
			this.ExtensionComboBox.Items.AddRange(new object[] {
						".bmp",
						".gif",
						".jpg",
						".pcx",
						".png",
						".tiff"});
			this.ExtensionComboBox.Location = new System.Drawing.Point(104, 96);
			this.ExtensionComboBox.Name = "ExtensionComboBox";
			this.ExtensionComboBox.Size = new System.Drawing.Size(121, 21);
			this.ExtensionComboBox.Sorted = true;
			this.ExtensionComboBox.TabIndex = 8;
			this.ExtensionComboBox.Text = ".jpg";
			this.ExtensionComboBox.TextChanged += new System.EventHandler(this.ExtensionComboBoxTextChanged);
			// 
			// GroesseColumnHeader
			// 
			this.GroesseColumnHeader.Text = "Größe";
			this.GroesseColumnHeader.Width = 77;
			// 
			// VorschauListView
			// 
			this.VorschauListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
						this.columnHeader1,
						this.columnHeader3});
			this.VorschauListView.GridLines = true;
			this.VorschauListView.HideSelection = false;
			this.VorschauListView.Location = new System.Drawing.Point(616, 8);
			this.VorschauListView.Name = "VorschauListView";
			this.VorschauListView.Size = new System.Drawing.Size(248, 336);
			this.VorschauListView.TabIndex = 4;
			this.VorschauListView.View = System.Windows.Forms.View.Details;
			this.VorschauListView.ItemActivate += new System.EventHandler(this.SourceListViewItemActivate);
			this.VorschauListView.SelectedIndexChanged += new System.EventHandler(this.SourceListViewSelectedIndexChanged);
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(136, 64);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(24, 23);
			this.label3.TabIndex = 4;
			this.label3.Text = "Bis:";
			this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// folderBrowserDialog
			// 
			this.folderBrowserDialog.Description = "Wählen Sie den Pfad aus, unter dem Sie Dateien umbenennen möchten.";
			this.folderBrowserDialog.SelectedPath = "E:\\Fotos";
			this.folderBrowserDialog.ShowNewFolderButton = false;
			// 
			// EnthaeltTextBox
			// 
			this.EnthaeltTextBox.Enabled = false;
			this.EnthaeltTextBox.Location = new System.Drawing.Point(8, 56);
			this.EnthaeltTextBox.Name = "EnthaeltTextBox";
			this.EnthaeltTextBox.Size = new System.Drawing.Size(96, 20);
			this.EnthaeltTextBox.TabIndex = 1;
			this.EnthaeltTextBox.Text = "";
			// 
			// AlleUebertragenButton
			// 
			this.AlleUebertragenButton.Enabled = false;
			this.AlleUebertragenButton.Location = new System.Drawing.Point(272, 88);
			this.AlleUebertragenButton.Name = "AlleUebertragenButton";
			this.AlleUebertragenButton.Size = new System.Drawing.Size(112, 24);
			this.AlleUebertragenButton.TabIndex = 5;
			this.AlleUebertragenButton.Text = "Alle übertragen >>";
			this.AlleUebertragenButton.Click += new System.EventHandler(this.AlleUebertragenButtonClick);
			// 
			// EntfernenButton
			// 
			this.EntfernenButton.Enabled = false;
			this.EntfernenButton.Location = new System.Drawing.Point(272, 272);
			this.EntfernenButton.Name = "EntfernenButton";
			this.EntfernenButton.Size = new System.Drawing.Size(112, 23);
			this.EntfernenButton.TabIndex = 8;
			this.EntfernenButton.Text = "< Entfernen";
			this.EntfernenButton.Click += new System.EventHandler(this.EntfernenButtonClick);
			// 
			// AlleEntfernenButton
			// 
			this.AlleEntfernenButton.Enabled = false;
			this.AlleEntfernenButton.Location = new System.Drawing.Point(272, 232);
			this.AlleEntfernenButton.Name = "AlleEntfernenButton";
			this.AlleEntfernenButton.Size = new System.Drawing.Size(112, 23);
			this.AlleEntfernenButton.TabIndex = 9;
			this.AlleEntfernenButton.Text = "<< Alle entfernen";
			this.AlleEntfernenButton.Click += new System.EventHandler(this.AlleEntfernenButtonClick);
			// 
			// button2
			// 
			this.button2.Location = new System.Drawing.Point(104, 160);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(120, 23);
			this.button2.TabIndex = 12;
			this.button2.Text = "Umbenennen";
			// 
			// VorschauLabel
			// 
			this.VorschauLabel.Location = new System.Drawing.Point(104, 128);
			this.VorschauLabel.Name = "VorschauLabel";
			this.VorschauLabel.Size = new System.Drawing.Size(120, 23);
			this.VorschauLabel.TabIndex = 10;
			this.VorschauLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.button2);
			this.groupBox1.Controls.Add(this.TestenButton);
			this.groupBox1.Controls.Add(this.VorschauLabel);
			this.groupBox1.Controls.Add(this.label5);
			this.groupBox1.Controls.Add(this.ExtensionComboBox);
			this.groupBox1.Controls.Add(this.ToNumericUpDown);
			this.groupBox1.Controls.Add(this.label4);
			this.groupBox1.Controls.Add(this.label3);
			this.groupBox1.Controls.Add(this.FromNumericUpDown);
			this.groupBox1.Controls.Add(this.label2);
			this.groupBox1.Controls.Add(this.StaticTextBox);
			this.groupBox1.Controls.Add(this.label1);
			this.groupBox1.Location = new System.Drawing.Point(616, 368);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(248, 200);
			this.groupBox1.TabIndex = 14;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Optionen";
			// 
			// NameColumnHeader
			// 
			this.NameColumnHeader.Text = "Name";
			this.NameColumnHeader.Width = 158;
			// 
			// UebertragenFilterGroupBox
			// 
			this.UebertragenFilterGroupBox.Controls.Add(this.EnthaeltTextBox);
			this.UebertragenFilterGroupBox.Controls.Add(this.EnthaeltCheckBox);
			this.UebertragenFilterGroupBox.Location = new System.Drawing.Point(264, 128);
			this.UebertragenFilterGroupBox.Name = "UebertragenFilterGroupBox";
			this.UebertragenFilterGroupBox.Size = new System.Drawing.Size(112, 88);
			this.UebertragenFilterGroupBox.TabIndex = 7;
			this.UebertragenFilterGroupBox.TabStop = false;
			this.UebertragenFilterGroupBox.Text = "Alle Übertragen/ Entfernen-Filter";
			// 
			// StaticTextBox
			// 
			this.StaticTextBox.Location = new System.Drawing.Point(104, 32);
			this.StaticTextBox.Name = "StaticTextBox";
			this.StaticTextBox.Size = new System.Drawing.Size(120, 20);
			this.StaticTextBox.TabIndex = 1;
			this.StaticTextBox.Text = "100_";
			this.StaticTextBox.TextChanged += new System.EventHandler(this.StaticTextBoxTextChanged);
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(24, 128);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(72, 24);
			this.label5.TabIndex = 9;
			this.label5.Text = "Vorschau:";
			this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(32, 64);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(32, 24);
			this.label1.TabIndex = 0;
			this.label1.Text = "Von:";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// columnHeader2
			// 
			this.columnHeader2.Text = "Name";
			this.columnHeader2.Width = 170;
			// 
			// MainForm
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(872, 597);
			this.Controls.Add(this.SourceListView);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.SelectedDownButton);
			this.Controls.Add(this.SelectedUpButton);
			this.Controls.Add(this.AlleEntfernenButton);
			this.Controls.Add(this.EntfernenButton);
			this.Controls.Add(this.UebertragenFilterGroupBox);
			this.Controls.Add(this.UebertragenButton);
			this.Controls.Add(this.AlleUebertragenButton);
			this.Controls.Add(this.VorschauListView);
			this.Controls.Add(this.AktualisierenButton);
			this.Controls.Add(this.FileListView);
			this.Controls.Add(this.DurchsuchenButton);
			this.Controls.Add(this.FolderTextBox);
			this.Name = "MainForm";
			this.Text = "Rename";
			((System.ComponentModel.ISupportInitialize)(this.ToNumericUpDown)).EndInit();
			this.panel1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.FromNumericUpDown)).EndInit();
			this.groupBox1.ResumeLayout(false);
			this.UebertragenFilterGroupBox.ResumeLayout(false);
			this.ResumeLayout(false);
		}
		#endregion
		
		/*
		 * ein folderBrowserDialog übergibt bei Ungültigkeit der Textboxeingabe
		 * UpdateFileList view den Pfad und entleert SourceListView
		 */
		void DurchsuchenButtonClick(object sender, System.EventArgs e)
		{
			if(FolderTextBox.Text != "")
			{
				folderBrowserDialog.SelectedPath = FolderTextBox.Text;
			}
			
			if(folderBrowserDialog.ShowDialog() == DialogResult.OK)
			{
				FolderTextBox.Text = folderBrowserDialog.SelectedPath;
				UpdateFileListView(folderBrowserDialog.SelectedPath);
				SourceListView.Items.Clear(); //Bei jedem Durchsuchen wird SourceListView geleert
			}
			
			UebertragenButton.Enabled = true;
			AlleUebertragenButton.Enabled = true;
		}
		
		/*
		 * UpdateFileListView wird der Inhalt der TextBox übergeben und SourceListView
		 * wird gelöscht
		 */
		void AktualisierenButtonClick(object sender, System.EventArgs e)
		{
			if(FolderTextBox.Text != "")
			{
				UpdateFileListView(FolderTextBox.Text);
			}
			
			SourceListView.Items.Clear(); //Bei jedem Durchsuchen wird SourceListView geleert
			
			UebertragenButton.Enabled = true;
			AlleUebertragenButton.Enabled = true;
		}
		
		/*
		 * Der durch die TextBox oder den Browse Dialog übergebene Pfad wird auf seine
		 * Gültigkeit hin geprüft und dann die Dateinamen mit ihren zugehörigen
		 * Dateigrößen in FileListView übertragen
		 */
		void UpdateFileListView(string path)
		{
			DirectoryInfo di = new DirectoryInfo(path);
			
			if(di.Exists)
			{
				FileListView.Enabled = true;
				FileListView.Items.Clear();
				FileInfo[] fiArr = di.GetFiles();
				
				foreach(FileInfo fi in fiArr)
				{
					FileListView.Items.Add(fi.Name);
					FileListView.Items[FileListView.Items.Count-1].SubItems.Add(Convert.ToString(fi.Length));
				}
			} else {
				FileListView.Items.Clear();
				FileListView.Items.Add("Kann Ordner nicht finden");
				FileListView.Enabled = false;
			}
		}
		
		/*
		 * Geht alle Markierten Einträge von FileListView durch, löscht sie
		 * und hängt sie an SourceListView an
		 */
		void UebertragenButtonClick(object sender, System.EventArgs e)
		{
			foreach(ListViewItem i in FileListView.SelectedItems)
			{
				FileListView.Items.Remove(i);
				SourceListView.Items.Add(i);
			}
			
			AlleEntfernenButton.Enabled = true;
			EntfernenButton.Enabled = true;
			
			if(FileListView.Items.Count == 0)
			{
				UebertragenButton.Enabled = false;
				AlleUebertragenButton.Enabled = false;
			}
		}
		
		/*
		 * Überprüft, ob nach Kriterien übertragen werden soll und ob das
		 * Textfeld nicht leer ist.
		 * Dann überträgt es entweder alle dateien die den gefordeten String
		 * enthalten oder im anderen Fall alle von FileListView nach
		 * SourceListView
		 * Aktiviert alle Buttons die in Interaktion mit SourceListView stehen
		 */
		void AlleUebertragenButtonClick(object sender, System.EventArgs e)
		{
			if(EnthaeltCheckBox.Checked && EnthaeltTextBox.Text != "")
			{
				foreach(ListViewItem i in FileListView.Items)
				{
					if(i.Text.IndexOf(EnthaeltTextBox.Text)!=-1)
					{
						FileListView.Items.Remove(i);
						SourceListView.Items.Add(i);
					}
				}
			} else {
				foreach(ListViewItem i in FileListView.Items)
				{
					FileListView.Items.Remove(i);
					SourceListView.Items.Add(i);
				}
			}
			
			AlleEntfernenButton.Enabled = true;
			EntfernenButton.Enabled = true;
			UebertragenButton.Enabled = false;
			AlleUebertragenButton.Enabled = false;
		}
		
		/*
		 * Bei einer Änderung des Status der Checkbox wird das Textfeld entweder
		 * aktiviert oder deaktiviert
		 */
		void EnthaeltCheckBoxCheckedChanged(object sender, System.EventArgs e)
		{
			switch(EnthaeltCheckBox.Checked)
			{
				case true:
					{
						EnthaeltTextBox.Enabled = true;
						break;
					}
				case false:
					{
						EnthaeltTextBox.Enabled = false;
						break;
					}
			}
		}
		
		/*
		 * Entfernt alle markierten Einträge aus SourceListView und fügt sie
		 * in FileListView ein
		 * Deaktiviert alle Buttons die im Zusammenhang mit SourceListView stehen
		 * wenn die Liste leer ist
		 */
		void EntfernenButtonClick(object sender, System.EventArgs e)
		{
			foreach(ListViewItem i in SourceListView.SelectedItems)
			{
				SourceListView.Items.Remove(i);
				FileListView.Items.Add(i);
			}
			
			if(SourceListView.Items.Count == 0)
			{
				SelectedUpButton.Enabled = false;
				SelectedDownButton.Enabled = false;
				AlleEntfernenButton.Enabled = false;
				EntfernenButton.Enabled = false;
			}
			
			UebertragenButton.Enabled = true;
			AlleUebertragenButton.Enabled = true;
		}
		
		/*
		 * Überprüft, ob nach Kriterien übertragen werden soll und ob das
		 * Textfeld nicht leer ist.
		 * Dann überträgt es entweder alle dateien die den gefordeten String
		 * enthalten oder im anderen Fall alle von SourceListView nach
		 * FileListView
		 * Deaktiviert alle Buttons, die im Zusammenhang mit SourceListView stehen
		 */
		void AlleEntfernenButtonClick(object sender, System.EventArgs e)
		{
			if(EnthaeltCheckBox.Checked && EnthaeltTextBox.Text != "")
			{
				foreach(ListViewItem i in SourceListView.Items)
				{
					if(i.Text.IndexOf(EnthaeltTextBox.Text)!=-1)
					{
						SourceListView.Items.Remove(i);
						FileListView.Items.Add(i);
					}
				}
			} else {
				foreach(ListViewItem i in SourceListView.Items)
				{
					SourceListView.Items.Remove(i);
					FileListView.Items.Add(i);
				}
			}
			
			SelectedUpButton.Enabled = false;
			SelectedDownButton.Enabled = false;
			AlleEntfernenButton.Enabled = false;
			EntfernenButton.Enabled = false;
			UebertragenButton.Enabled = true;
			AlleUebertragenButton.Enabled = true;
		}
		
		/*
		 * Verschiebt alle Selektierten Elemente um einen Platz nach oben
		 */
		void SelectedUpButtonClick(object sender, System.EventArgs e)
		{
			//Erstellt ein ListViewItem Array um die selektierten ListViewItems
			//aufnehmen zu können
			ListViewItem[] SelectedItemsArr = new ListViewItem[SourceListView.SelectedItems.Count];
			//Erstellt ein weiteres ListViewItem Arry für alle List View Items nach
			//der Selektion mit entsprechender Größe (Gesamtzahl aller Items minus
			//dem Index des Letztmarkierten Eintrags)
			ListViewItem[] AfterItemsArr = new ListViewItem[SourceListView.Items.Count
			                                                -SourceListView.SelectedItems[SourceListView.SelectedItems.Count-1].Index-1];
			
			//Kopiert alle markierten Items in das entsprehende Array
			SourceListView.SelectedItems.CopyTo(SelectedItemsArr, 0);
			
			//Kopiert alle Items nach der Selektion in das entsprechende Array
			foreach(ListViewItem i in SourceListView.Items)
			{
				if(i.Index > SourceListView.SelectedItems[SourceListView.SelectedItems.Count-1].Index)
				{
					//Subtrahiert den Index des ersten unmarkierten Eintrags vom aktuellen Index um die
					//richtige Position im Array zu erhalten
					AfterItemsArr[i.Index-SourceListView.SelectedItems[SourceListView.SelectedItems.Count-1].Index-1] = i;
				}
			}
			
			//Löscht alle Einträge nach der Selektion aus dem ListView
			foreach(ListViewItem i in SourceListView.Items)
			{
				if(i.Index > SourceListView.SelectedItems[SourceListView.SelectedItems.Count-1].Index)
				{
					i.Remove();
				}
			}
			
			//Löscht alle markierten Einträge
			foreach(ListViewItem i in SourceListView.Items)
			{
				if(i.Selected)
				{
					i.Remove();
				}
			}
			
			//Erstellt ein Temporäres Item und kopiert das letzte Item des momentanen ListViews
			//rein um das Item dann aus dem ListView zu löschen
			ListViewItem TempItem = SourceListView.Items[SourceListView.Items.Count-1];
			SourceListView.Items[SourceListView.Items.Count-1].Remove();
			
			//Fügt dem ListView alle Einträge aus dem Array mit den Selektierten Items hinzu
			foreach(ListViewItem i in SelectedItemsArr)
			{
				SourceListView.Items.Add(i);
			}
			
			//Hängt das TempItem an
			SourceListView.Items.Add(TempItem);
			
			//Fügt alle Einträge des Arrays mit Items nach der anfänglichen Selektion ein
			foreach(ListViewItem i in AfterItemsArr)
			{
				SourceListView.Items.Add(i);
			}
			
			//Aktiviert den Down Button, da die Selektion nach dem EInfügen in die Liste
			//der letzte Eintrag war und somit von SourceListViewSelectedIndexChanged
			//deaktivuert wurde und das Ereignis nach Einfügen der restlichen Einträge
			//niocht nochmals aufgerufen wird
			SelectedDownButton.Enabled = true;
		}
		
		/*
		 * Verschiebt alle Selektierten Elemente um einen Platz nach unten
		 */
		void SelectedDownButtonClick(object sender, System.EventArgs e)
		{
			ListViewItem[] SelectedItemsArr = new ListViewItem[SourceListView.SelectedItems.Count];
			ListViewItem[] AfterItemsArr = new ListViewItem[SourceListView.Items.Count
			                                                -SourceListView.SelectedItems[SourceListView.SelectedItems.Count-1].Index-2];
			
			SourceListView.SelectedItems.CopyTo(SelectedItemsArr, 0);
			
			ListViewItem TempItem = SourceListView.Items[SourceListView.SelectedItems[SourceListView.SelectedItems.Count-1].Index+1];
			SourceListView.Items[SourceListView.SelectedItems[SourceListView.SelectedItems.Count-1].Index+1].Remove();
			
			foreach(ListViewItem i in SourceListView.Items)
			{
				if(i.Index > SourceListView.SelectedItems[SourceListView.SelectedItems.Count-1].Index)
				{
					AfterItemsArr[i.Index-SourceListView.SelectedItems[SourceListView.SelectedItems.Count-1].Index-1] = i;
				}
			}
			
			foreach(ListViewItem i in SourceListView.Items)
			{
				if(i.Index > SourceListView.SelectedItems[SourceListView.SelectedItems.Count-1].Index)
				{
					i.Remove();
				}
			}
			
			foreach(ListViewItem i in SourceListView.Items)
			{
				if(i.Selected)
				{
					i.Remove();
				}
			}
			
			SourceListView.Items.Add(TempItem);
			
			foreach(ListViewItem i in SelectedItemsArr)
			{
				SourceListView.Items.Add(i);
			}
			
			foreach(ListViewItem i in AfterItemsArr)
			{
				SourceListView.Items.Add(i);
			}
			
			if(SourceListView.SelectedItems[SourceListView.SelectedItems.Count-1].Index == SourceListView.Items.Count-1)
			{
				SelectedDownButton.Enabled = false;
			}
			else
			{
				SelectedDownButton.Enabled = true;
			}
		}
		
		/*
		 * Überprüft ob die selektierten Elemente entweder ganz oben oder ganz
		 * unten liegen und aktiviert bzw. deaktiviert die Up und Down Buttons
		 */
		void SourceListViewSelectedIndexChanged(object sender, System.EventArgs e)
		{
			if(SourceListView.SelectedItems.Count == 0)
			{
				SelectedUpButton.Enabled = false;
				SelectedDownButton.Enabled = false;
			}
			else
			{
				if(SourceListView.SelectedItems[0].Index == 0)
				{
					SelectedUpButton.Enabled = false;
				}
				else
				{
					SelectedUpButton.Enabled = true;
				}
				
				if(SourceListView.SelectedItems[SourceListView.SelectedItems.Count-1].Index == SourceListView.Items.Count-1)
				{
					SelectedDownButton.Enabled = false;
				}
				else
				{
					SelectedDownButton.Enabled = true;
				}
			}
		}
		
		/*
		 * Wird ein Item angewählt überprüft die Funktion das Bild auf Hoch oder
		 * Querformat und ändert die Größe und Lage der Picture Box in der Weise,
		 * dass das Bild korrekt angezeigt wird.
		 */
		void SourceListViewItemActivate(object sender, System.EventArgs e)
		{
			if(File.Exists(FolderTextBox.Text+"\\"+SourceListView.SelectedItems[0].Text))
			{
				VorschauPictureBox.Visible = false;
				
				VorschauPictureBox.Image = Image.FromFile(FolderTextBox.Text+"\\"+SourceListView.SelectedItems[0].Text);
				
				decimal AspectRatio = Convert.ToDecimal(VorschauPictureBox.Image.Width)/Convert.ToDecimal(VorschauPictureBox.Image.Height);
				
				if(AspectRatio > 1)//Querformat
				{
					VorschauPictureBox.Width = 320;
					VorschauPictureBox.Height = Convert.ToInt32(320/AspectRatio);
					VorschauPictureBox.Left = 0;
					VorschauPictureBox.Top = 120-VorschauPictureBox.Height/2;
				}
				else if(AspectRatio < 1)//Hochformat
				{
					VorschauPictureBox.Width = Convert.ToInt32(240*AspectRatio);
					VorschauPictureBox.Height = 240;
					VorschauPictureBox.Top = 0;
					VorschauPictureBox.Left = 160-VorschauPictureBox.Width/2;
				}
				
				VorschauPictureBox.Visible = true;
			}
		}
		void FromNumericUpDownValueChanged(object sender, System.EventArgs e)
		{
			ToNumericUpDown.Value = FromNumericUpDown.Value + SourceListView.Items.Count - 1;
		}
		
		void FromNumericUpDownLeave(object sender, System.EventArgs e)
		{
			ToNumericUpDown.Value = FromNumericUpDown.Value + SourceListView.Items.Count - 1;
			VorschauLabel.Text = StaticTextBox.Text + FromNumericUpDown.Value.ToString() + ExtensionComboBox.Text;
		}
		void StaticTextBoxTextChanged(object sender, System.EventArgs e)
		{
			VorschauLabel.Text = StaticTextBox.Text + FromNumericUpDown.Value.ToString() + ExtensionComboBox.Text;
		}
		
		void ExtensionComboBoxTextChanged(object sender, System.EventArgs e)
		{
			VorschauLabel.Text = StaticTextBox.Text + FromNumericUpDown.Value.ToString() + ExtensionComboBox.Text;
		}
		
		void TestenButtonClick(object sender, System.EventArgs e)
		{
			foreach(ListViewItem i in VorschauListView.Items)
			{
				i.Remove();
			}
			
			ListViewItem[] Items = new ListViewItem[SourceListView.Items.Count];
			foreach(ListViewItem i in SourceListView.Items)
			{
				Items[i.Index]=(ListViewItem)i.Clone();
			}
			
			foreach(ListViewItem i in Items)
			{
				VorschauListView.Items.Add(i.Text);
			}
		}
	}
}

//TODO: Führende nullen des NumericUPDown werden nicht übernommen
