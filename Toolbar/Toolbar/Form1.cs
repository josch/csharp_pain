using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Toolbar
{
    public partial class Form1 : ShellLib.ApplicationDesktopToolbar
    {
        public Form1()
        {
            InitializeComponent();
            this.Edge = AppBarEdges.Left;
        }
    }
}