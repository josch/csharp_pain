
using System;
using System.Drawing;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Collections;
using System.Diagnostics;
using System.Threading;

namespace Netstat
{
	interface IDWPing
	{
		short ping(string strHostName);
		int Timeout{set;}
		int Repeats{set;}
		int AvgTime{get;}
		int AvgTTL{get;}
		string ErrorMsg{get;}
	}
	
	struct ICMPHeader
	{
		public byte type;
		public byte code;
		public ushort chksum;
		public ushort id;
		public ushort seq;
		public ulong timestamp;
		
		public byte[] toByteArray()
		{
			byte[] arResult = new byte[22];
            arResult[0] = this.type;
            arResult[1] = this.code;
            arResult[2] = (byte)chksum;
            arResult[3] = (byte)(chksum >> 8);
            arResult[4] = (byte)(chksum >> 16);
            arResult[5] = (byte)(chksum >> 24);
            arResult[6] = (byte)id;
            arResult[7] = (byte)(id >> 8);
            arResult[8] = (byte)(id >> 16);
            arResult[9] = (byte)(id >> 24);
            arResult[10] = (byte)seq;
            arResult[11] = (byte)(seq >> 8);
            arResult[12] = (byte)(seq >> 16);
            arResult[13] = (byte)(seq >> 24);
            arResult[14] = (byte)timestamp;
            arResult[15] = (byte)(timestamp >> 8);
            arResult[16] = (byte)(timestamp >> 16);
            arResult[17] = (byte)(timestamp >> 24);
            arResult[18] = (byte)(timestamp >> 32);
            arResult[19] = (byte)(timestamp >> 40);
            arResult[20] = (byte)(timestamp >> 48);
            arResult[21] = (byte)(timestamp >> 56);
            
            return arResult;
		}
	}
	
	public class CDWPing : IDWPing
	{
		private int m_Timeout;
		private int[] m_arTime;
		private bool[] m_arResults;
		private byte[] m_arTTL;
		private int m_idxPing;
		private string m_strErrorMsg;

		public int Timeout{set{m_Timeout = Math.Max(value,1);}}
		public int Repeats
		{set{
			int n = Math.Max(value, 1);
			m_arTime	= new int[n];
			m_arTTL	 = new byte[n];
			m_arResults = new bool[n];
		}}
		public int AvgTime		 { get{ return this.calcAvgTime(); }}
		public int AvgTTL		  { get{ return this.calcAvgTTL();  }}
		public string ErrorMsg { get{ return m_strErrorMsg; }}

		public CDWPing()
		{
			m_Timeout = 200;
			m_arTime	= new int[1];
			m_arResults = new bool[1];
			m_arTTL	 = new Byte[1];
			m_strErrorMsg = "Blah";
		}

		public short ping(string strHostName)
		{
			m_strErrorMsg = "Doll";

			this.clearStats();			
			
			short result = 0;
		
			try
			{
				IPEndPoint lep;
				
				const int echoPort = 7;
				
				if(this.isIPAddress(strHostName))
				{
					IPAddress ipAddr = IPAddress.Parse(strHostName);
					lep = new IPEndPoint(ipAddr, echoPort);
				}
				else
				{
					IPHostEntry lipa = Dns.Resolve(strHostName);
					lep = new IPEndPoint(lipa.AddressList[0], echoPort);
				}
				
				for(m_idxPing = 0; m_idxPing < m_arTime.Length; m_idxPing++)
				{
					if(tryPing(lep))
					{
						m_arResults[m_idxPing] = true;
						result = 1;
					}
					else
					{
						m_arResults[m_idxPing] = false;
					}
				}
			}
			catch(SocketException ex)
			{
				result = 0;
				m_strErrorMsg = ex.Message;
			}
			catch(Exception ex)
			{
				result = 0;
				m_strErrorMsg = ex.Message;
			}
			
			return result;
		}
		
		private bool tryPing(IPEndPoint lep)
		{
			bool bResult = false;
			
			Socket sock = new Socket(AddressFamily.InterNetwork, SocketType.Raw, ProtocolType.Icmp);
			
			ICMPHeader header = new ICMPHeader();
			header.type = 8;
			header.code = 0;
			header.id = Convert.ToUInt16(Process.GetCurrentProcess().Id);
			header.seq = 0;
			header.chksum = 0;
			DateTime startTime = DateTime.Now;
			header.timestamp = Convert.ToUInt64(startTime.Ticks);
			
			byte[] arHeader = header.toByteArray();
			
			byte[] arBytes = new byte[arHeader.Length + 32];
			
			byte fill = Convert.ToByte('E');
			
			for(int i=0; i<arBytes.Length; i++)
			{
				arBytes[i] = fill;
			}
			
			arHeader.CopyTo(arBytes, 0);
			
			header.chksum = this.generateIPChecksum(arBytes);
			
			arHeader = header.toByteArray();
			arHeader.CopyTo(arBytes, 0);
			
			sock.SendTo(arBytes, lep);
			
			if(this.isSocketReadible(sock))
			{
				IPEndPoint sender1 = new IPEndPoint(IPAddress.Any, 0);
				EndPoint tempRemoteEP = (EndPoint)sender1;
				
				byte[] receiveBuffer = new byte[1024];
				
				int nReceived = sock.ReceiveFrom(receiveBuffer, ref tempRemoteEP);
				
				bResult = this.verifyReceivedMessage(receiveBuffer, nReceived, arHeader.Length);
				
				m_arTTL[m_idxPing] = receiveBuffer[8];
			}
			else
			{
				m_strErrorMsg = "Socket nicht lesbar";
			}
			
			DateTime now = DateTime.Now;
			TimeSpan elapsedTime = new TimeSpan(now.Ticks - startTime.Ticks);
			m_arTime[m_idxPing] = elapsedTime.Milliseconds;
			
			return bResult;
		}
		
		private bool isIPAddress(string strAddress)
		{
			bool bResult = true;
			
			foreach(char ch in strAddress)
			{
				if((!Char.IsDigit(ch))&&(ch!='.'))
				{
					bResult = false;
					break;
				}
			}
			
			return bResult;
		}
		
		private ushort generateIPChecksum(byte[] arBytes)
		{
			ulong chksum = 0;
			
			int nSize = arBytes.Length;
			int i = 0;
			
			while(nSize > 1)
			{
				chksum += (ulong)((((ushort)arBytes[i+1])<<8)+(ushort)arBytes[i]);
				nSize -= 2;
				i += 2;
			}
			
			if(nSize >0)
			{
				chksum += arBytes[i];
			}
			
			chksum = (chksum >> 16) + (chksum & 0xFFFF);
			chksum += (chksum >> 16);
			
			ushort result = (ushort)(~chksum);
			return result;
		}
		
		private bool isSocketReadible(Socket s)
		{
			bool bResult = false;
			int n = 0;
			
			while(n < m_Timeout)
			{
				if(s.Poll(200, SelectMode.SelectRead))
				{
					bResult = true;
					break;
				}
				
				n++;
			}
			
			return bResult;
		}
		
		private bool verifyReceivedMessage(byte[] arBytes, int nReceived, int minLengthSent)
		{
			int nLengthIPHeader = arBytes[0] & 0x0F;
			nLengthIPHeader *= 4;
			
			if(nLengthIPHeader + minLengthSent > nReceived)
			{
				m_strErrorMsg = "Nicht genug Daten zur�ck gekommen";
				return false;
			}
			
			if(arBytes[nLengthIPHeader]!=0)
			{
				m_strErrorMsg = "Falscher Typ zur�ck gekommen";
				return false;
			}
			
			int nId = arBytes[nLengthIPHeader + 6] + arBytes[nLengthIPHeader + 7]*256;
			
			if(nId != Convert.ToUInt16(Process.GetCurrentProcess().Id))
			{
				m_strErrorMsg = "Daten vom falschen Programm zur�ck gekommen";
				return false;
			}
			
			return true;
		}
		
		private int calcAvgTime()
		{
			int result = 0;
			
			foreach(int i in m_arTime)
			{
				result+=i;
			}
			
			result /= m_arTime.Length;
			return result;
		}
		
		private int calcAvgTTL()
		{
			int result = 0;
			
			foreach(int i in m_arTTL)
			{
				if(m_arResults[i])
				{
					result += i;
				}
			}
			
			result /= m_arTTL.Length;
			return result;
		}
		
		private void clearStats()
		{
			for(int i = 0; i<m_arTime.Length; i++)
			{
				m_arTime[i] = 0;
				m_arTTL[i] = 0;
			}
		}
	}
	
	public class MainForm : System.Windows.Forms.Form
	{
		private System.Windows.Forms.GroupBox groupBox3;
		private System.Windows.Forms.StatusBarPanel statusBarPanel1;
		private System.Windows.Forms.GroupBox groupBox9;
		private System.Windows.Forms.GroupBox groupBox8;
		private System.Windows.Forms.GroupBox groupBox7;
		private System.Windows.Forms.Label ip20label;
		private System.Windows.Forms.GroupBox groupBox5;
		private System.Windows.Forms.GroupBox groupBox4;
		private System.Windows.Forms.GroupBox SingleCheckGroupBox;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Label ip16label;
		private System.Windows.Forms.Label ip21label;
		private System.Windows.Forms.GroupBox ScanGroupBox;
		private System.Windows.Forms.Button SingleCheckButton;
		private System.Windows.Forms.StatusBar statusBar1;
		private System.Windows.Forms.GroupBox groupBox18;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.GroupBox groupBox16;
		private System.Windows.Forms.GroupBox groupBox17;
		private System.Windows.Forms.GroupBox groupBox14;
		private System.Windows.Forms.GroupBox groupBox15;
		private System.Windows.Forms.GroupBox groupBox12;
		private System.Windows.Forms.GroupBox groupBox13;
		private System.Windows.Forms.GroupBox groupBox10;
		private System.Windows.Forms.GroupBox groupBox11;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Panel BottomPanel;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label ip22label;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Panel CheckSinglePanel;
		private System.Windows.Forms.TextBox FullScanToTextBox;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.TextBox FullScanFromTextBox;
		private System.Windows.Forms.Label ip14label;
		private System.Windows.Forms.Label label14;
		private System.Windows.Forms.Label ip17label;
		private System.Windows.Forms.TextBox textBox1;
		private System.Windows.Forms.Panel ScanPanel;
		private System.Windows.Forms.Label label21;
		private System.Windows.Forms.Label ScanResultLabel;
		private System.Windows.Forms.Label label22;
		private System.Windows.Forms.Label label25;
		private System.Windows.Forms.Label label20;
		private System.Windows.Forms.Label label27;
		private System.Windows.Forms.Label label26;
		private System.Windows.Forms.Label label29;
		private System.Windows.Forms.Label label28;
		private System.Windows.Forms.Label ip40label;
		private System.Windows.Forms.Label label19;
		private System.Windows.Forms.TextBox SingleCheckTextBox;
		private System.Windows.Forms.Label ip1label;
		private System.Windows.Forms.Label ip41label;
		private System.Windows.Forms.Label ip28label;
		private System.Windows.Forms.Label ip15label;
		private System.Windows.Forms.Button ScanButton;
		private System.Windows.Forms.GroupBox groupBox6;
		private System.Windows.Forms.Label label30;
		private System.Windows.Forms.Label label31;
		private System.Windows.Forms.Label label32;
		private System.Windows.Forms.Label label33;
		private System.Windows.Forms.Label label34;
		private System.Windows.Forms.Label label35;
		private System.Windows.Forms.Label label37;
		private System.Windows.Forms.GroupBox AreaGroupBox;
		private CDWPing Ping;
		public MainForm()
		{
			InitializeComponent();
			
			Ping = new CDWPing();
		}
		
		[STAThread]
		public static void Main(string[] args)
		{
			Application.Run(new MainForm());
		}
		
		private void InitializeComponent() {
			this.AreaGroupBox = new System.Windows.Forms.GroupBox();
			this.label37 = new System.Windows.Forms.Label();
			this.label35 = new System.Windows.Forms.Label();
			this.label34 = new System.Windows.Forms.Label();
			this.label33 = new System.Windows.Forms.Label();
			this.label32 = new System.Windows.Forms.Label();
			this.label31 = new System.Windows.Forms.Label();
			this.label30 = new System.Windows.Forms.Label();
			this.groupBox6 = new System.Windows.Forms.GroupBox();
			this.ScanButton = new System.Windows.Forms.Button();
			this.ip15label = new System.Windows.Forms.Label();
			this.ip28label = new System.Windows.Forms.Label();
			this.ip41label = new System.Windows.Forms.Label();
			this.ip1label = new System.Windows.Forms.Label();
			this.SingleCheckTextBox = new System.Windows.Forms.TextBox();
			this.label19 = new System.Windows.Forms.Label();
			this.ip40label = new System.Windows.Forms.Label();
			this.label28 = new System.Windows.Forms.Label();
			this.label29 = new System.Windows.Forms.Label();
			this.label26 = new System.Windows.Forms.Label();
			this.label27 = new System.Windows.Forms.Label();
			this.label20 = new System.Windows.Forms.Label();
			this.label25 = new System.Windows.Forms.Label();
			this.label22 = new System.Windows.Forms.Label();
			this.ScanResultLabel = new System.Windows.Forms.Label();
			this.label21 = new System.Windows.Forms.Label();
			this.ScanPanel = new System.Windows.Forms.Panel();
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.ip17label = new System.Windows.Forms.Label();
			this.label14 = new System.Windows.Forms.Label();
			this.ip14label = new System.Windows.Forms.Label();
			this.FullScanFromTextBox = new System.Windows.Forms.TextBox();
			this.label13 = new System.Windows.Forms.Label();
			this.FullScanToTextBox = new System.Windows.Forms.TextBox();
			this.CheckSinglePanel = new System.Windows.Forms.Panel();
			this.label8 = new System.Windows.Forms.Label();
			this.ip22label = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.BottomPanel = new System.Windows.Forms.Panel();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.groupBox11 = new System.Windows.Forms.GroupBox();
			this.groupBox10 = new System.Windows.Forms.GroupBox();
			this.groupBox13 = new System.Windows.Forms.GroupBox();
			this.groupBox12 = new System.Windows.Forms.GroupBox();
			this.groupBox15 = new System.Windows.Forms.GroupBox();
			this.groupBox14 = new System.Windows.Forms.GroupBox();
			this.groupBox17 = new System.Windows.Forms.GroupBox();
			this.groupBox16 = new System.Windows.Forms.GroupBox();
			this.panel1 = new System.Windows.Forms.Panel();
			this.groupBox18 = new System.Windows.Forms.GroupBox();
			this.statusBar1 = new System.Windows.Forms.StatusBar();
			this.SingleCheckButton = new System.Windows.Forms.Button();
			this.ScanGroupBox = new System.Windows.Forms.GroupBox();
			this.ip21label = new System.Windows.Forms.Label();
			this.ip16label = new System.Windows.Forms.Label();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.SingleCheckGroupBox = new System.Windows.Forms.GroupBox();
			this.groupBox4 = new System.Windows.Forms.GroupBox();
			this.groupBox5 = new System.Windows.Forms.GroupBox();
			this.ip20label = new System.Windows.Forms.Label();
			this.groupBox7 = new System.Windows.Forms.GroupBox();
			this.groupBox8 = new System.Windows.Forms.GroupBox();
			this.groupBox9 = new System.Windows.Forms.GroupBox();
			this.statusBarPanel1 = new System.Windows.Forms.StatusBarPanel();
			this.groupBox3 = new System.Windows.Forms.GroupBox();
			this.AreaGroupBox.SuspendLayout();
			this.groupBox6.SuspendLayout();
			this.ScanPanel.SuspendLayout();
			this.CheckSinglePanel.SuspendLayout();
			this.BottomPanel.SuspendLayout();
			this.groupBox11.SuspendLayout();
			this.groupBox10.SuspendLayout();
			this.groupBox13.SuspendLayout();
			this.groupBox12.SuspendLayout();
			this.groupBox15.SuspendLayout();
			this.groupBox14.SuspendLayout();
			this.groupBox17.SuspendLayout();
			this.groupBox16.SuspendLayout();
			this.panel1.SuspendLayout();
			this.groupBox18.SuspendLayout();
			this.ScanGroupBox.SuspendLayout();
			this.groupBox1.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.SingleCheckGroupBox.SuspendLayout();
			this.groupBox4.SuspendLayout();
			this.groupBox5.SuspendLayout();
			this.groupBox7.SuspendLayout();
			this.groupBox8.SuspendLayout();
			this.groupBox9.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.statusBarPanel1)).BeginInit();
			this.groupBox3.SuspendLayout();
			this.SuspendLayout();
			// 
			// AreaGroupBox
			// 
			this.AreaGroupBox.Controls.Add(this.label3);
			this.AreaGroupBox.Controls.Add(this.FullScanToTextBox);
			this.AreaGroupBox.Controls.Add(this.label2);
			this.AreaGroupBox.Controls.Add(this.FullScanFromTextBox);
			this.AreaGroupBox.Location = new System.Drawing.Point(16, 24);
			this.AreaGroupBox.Name = "AreaGroupBox";
			this.AreaGroupBox.Size = new System.Drawing.Size(136, 88);
			this.AreaGroupBox.TabIndex = 6;
			this.AreaGroupBox.TabStop = false;
			this.AreaGroupBox.Text = "Bereich";
			// 
			// label37
			// 
			this.label37.Location = new System.Drawing.Point(184, 24);
			this.label37.Name = "label37";
			this.label37.Size = new System.Drawing.Size(160, 16);
			this.label37.TabIndex = 45;
			this.label37.Text = "192.168.0.27";
			// 
			// label35
			// 
			this.label35.Location = new System.Drawing.Point(184, 24);
			this.label35.Name = "label35";
			this.label35.Size = new System.Drawing.Size(160, 16);
			this.label35.TabIndex = 43;
			this.label35.Text = "192.168.0.29";
			// 
			// label34
			// 
			this.label34.Location = new System.Drawing.Point(16, 24);
			this.label34.Name = "label34";
			this.label34.Size = new System.Drawing.Size(160, 16);
			this.label34.TabIndex = 42;
			this.label34.Text = "192.168.0.30";
			// 
			// label33
			// 
			this.label33.Location = new System.Drawing.Point(184, 24);
			this.label33.Name = "label33";
			this.label33.Size = new System.Drawing.Size(160, 16);
			this.label33.TabIndex = 41;
			this.label33.Text = "192.168.0.31";
			// 
			// label32
			// 
			this.label32.Location = new System.Drawing.Point(16, 24);
			this.label32.Name = "label32";
			this.label32.Size = new System.Drawing.Size(160, 16);
			this.label32.TabIndex = 40;
			this.label32.Text = "192.168.0.32";
			// 
			// label31
			// 
			this.label31.Location = new System.Drawing.Point(184, 24);
			this.label31.Name = "label31";
			this.label31.Size = new System.Drawing.Size(160, 16);
			this.label31.TabIndex = 48;
			this.label31.Text = "192.168.0.33";
			// 
			// label30
			// 
			this.label30.Location = new System.Drawing.Point(16, 24);
			this.label30.Name = "label30";
			this.label30.Size = new System.Drawing.Size(160, 16);
			this.label30.TabIndex = 47;
			this.label30.Text = "192.168.0.34";
			// 
			// groupBox6
			// 
			this.groupBox6.Controls.Add(this.label34);
			this.groupBox6.Controls.Add(this.label33);
			this.groupBox6.Location = new System.Drawing.Point(8, 328);
			this.groupBox6.Name = "groupBox6";
			this.groupBox6.Size = new System.Drawing.Size(360, 56);
			this.groupBox6.TabIndex = 21;
			this.groupBox6.TabStop = false;
			this.groupBox6.Text = "165";
			// 
			// ScanButton
			// 
			this.ScanButton.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.World);
			this.ScanButton.Location = new System.Drawing.Point(16, 120);
			this.ScanButton.Name = "ScanButton";
			this.ScanButton.Size = new System.Drawing.Size(136, 24);
			this.ScanButton.TabIndex = 0;
			this.ScanButton.Text = "Scannen";
			this.ScanButton.Click += new System.EventHandler(this.ScanButtonClick);
			// 
			// ip15label
			// 
			this.ip15label.Location = new System.Drawing.Point(184, 24);
			this.ip15label.Name = "ip15label";
			this.ip15label.Size = new System.Drawing.Size(160, 16);
			this.ip15label.TabIndex = 34;
			this.ip15label.Text = "192.168.0.15";
			// 
			// ip28label
			// 
			this.ip28label.Location = new System.Drawing.Point(16, 24);
			this.ip28label.Name = "ip28label";
			this.ip28label.Size = new System.Drawing.Size(160, 16);
			this.ip28label.TabIndex = 44;
			this.ip28label.Text = "192.168.0.28";
			// 
			// ip41label
			// 
			this.ip41label.Location = new System.Drawing.Point(184, 24);
			this.ip41label.Name = "ip41label";
			this.ip41label.Size = new System.Drawing.Size(160, 16);
			this.ip41label.TabIndex = 40;
			this.ip41label.Text = "192.168.0.41";
			// 
			// ip1label
			// 
			this.ip1label.Location = new System.Drawing.Point(16, 24);
			this.ip1label.Name = "ip1label";
			this.ip1label.Size = new System.Drawing.Size(160, 16);
			this.ip1label.TabIndex = 34;
			this.ip1label.Text = "192.168.0.1";
			// 
			// SingleCheckTextBox
			// 
			this.SingleCheckTextBox.Location = new System.Drawing.Point(80, 24);
			this.SingleCheckTextBox.Name = "SingleCheckTextBox";
			this.SingleCheckTextBox.Size = new System.Drawing.Size(40, 21);
			this.SingleCheckTextBox.TabIndex = 0;
			this.SingleCheckTextBox.Text = "";
			// 
			// label19
			// 
			this.label19.Location = new System.Drawing.Point(184, 24);
			this.label19.Name = "label19";
			this.label19.Size = new System.Drawing.Size(160, 16);
			this.label19.TabIndex = 42;
			this.label19.Text = "192.168.0.23";
			// 
			// ip40label
			// 
			this.ip40label.Location = new System.Drawing.Point(16, 24);
			this.ip40label.Name = "ip40label";
			this.ip40label.Size = new System.Drawing.Size(160, 16);
			this.ip40label.TabIndex = 41;
			this.ip40label.Text = "192.168.0.40";
			// 
			// label28
			// 
			this.label28.Location = new System.Drawing.Point(16, 24);
			this.label28.Name = "label28";
			this.label28.Size = new System.Drawing.Size(160, 16);
			this.label28.TabIndex = 45;
			this.label28.Text = "192.168.0.36";
			// 
			// label29
			// 
			this.label29.Location = new System.Drawing.Point(184, 24);
			this.label29.Name = "label29";
			this.label29.Size = new System.Drawing.Size(160, 16);
			this.label29.TabIndex = 46;
			this.label29.Text = "192.168.0.35";
			// 
			// label26
			// 
			this.label26.Location = new System.Drawing.Point(16, 24);
			this.label26.Name = "label26";
			this.label26.Size = new System.Drawing.Size(160, 16);
			this.label26.TabIndex = 43;
			this.label26.Text = "192.168.0.38";
			// 
			// label27
			// 
			this.label27.Location = new System.Drawing.Point(184, 24);
			this.label27.Name = "label27";
			this.label27.Size = new System.Drawing.Size(160, 16);
			this.label27.TabIndex = 44;
			this.label27.Text = "192.168.0.37";
			// 
			// label20
			// 
			this.label20.Location = new System.Drawing.Point(16, 24);
			this.label20.Name = "label20";
			this.label20.Size = new System.Drawing.Size(160, 16);
			this.label20.TabIndex = 43;
			this.label20.Text = "192.168.0.24";
			// 
			// label25
			// 
			this.label25.Location = new System.Drawing.Point(184, 24);
			this.label25.Name = "label25";
			this.label25.Size = new System.Drawing.Size(160, 16);
			this.label25.TabIndex = 42;
			this.label25.Text = "192.168.0.39";
			// 
			// label22
			// 
			this.label22.Location = new System.Drawing.Point(16, 24);
			this.label22.Name = "label22";
			this.label22.Size = new System.Drawing.Size(160, 16);
			this.label22.TabIndex = 45;
			this.label22.Text = "192.168.0.26";
			// 
			// ScanResultLabel
			// 
			this.ScanResultLabel.BackColor = System.Drawing.Color.Transparent;
			this.ScanResultLabel.Location = new System.Drawing.Point(8, 152);
			this.ScanResultLabel.Name = "ScanResultLabel";
			this.ScanResultLabel.Size = new System.Drawing.Size(160, 312);
			this.ScanResultLabel.TabIndex = 7;
			// 
			// label21
			// 
			this.label21.Location = new System.Drawing.Point(184, 24);
			this.label21.Name = "label21";
			this.label21.Size = new System.Drawing.Size(160, 16);
			this.label21.TabIndex = 44;
			this.label21.Text = "192.168.0.25";
			// 
			// ScanPanel
			// 
			this.ScanPanel.AutoScroll = true;
			this.ScanPanel.Controls.Add(this.ScanGroupBox);
			this.ScanPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.ScanPanel.Location = new System.Drawing.Point(0, 223);
			this.ScanPanel.Name = "ScanPanel";
			this.ScanPanel.Size = new System.Drawing.Size(184, 488);
			this.ScanPanel.TabIndex = 6;
			// 
			// textBox1
			// 
			this.textBox1.Location = new System.Drawing.Point(16, 48);
			this.textBox1.Multiline = true;
			this.textBox1.Name = "textBox1";
			this.textBox1.ReadOnly = true;
			this.textBox1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.textBox1.Size = new System.Drawing.Size(328, 120);
			this.textBox1.TabIndex = 1;
			this.textBox1.Text = "";
			// 
			// ip17label
			// 
			this.ip17label.Location = new System.Drawing.Point(184, 24);
			this.ip17label.Name = "ip17label";
			this.ip17label.Size = new System.Drawing.Size(160, 16);
			this.ip17label.TabIndex = 36;
			this.ip17label.Text = "192.168.0.17";
			// 
			// label14
			// 
			this.label14.Location = new System.Drawing.Point(16, 24);
			this.label14.Name = "label14";
			this.label14.Size = new System.Drawing.Size(160, 16);
			this.label14.TabIndex = 38;
			this.label14.Text = "192.168.0.18";
			// 
			// ip14label
			// 
			this.ip14label.Location = new System.Drawing.Point(16, 24);
			this.ip14label.Name = "ip14label";
			this.ip14label.Size = new System.Drawing.Size(160, 16);
			this.ip14label.TabIndex = 33;
			this.ip14label.Text = "192.168.0.14";
			// 
			// FullScanFromTextBox
			// 
			this.FullScanFromTextBox.Location = new System.Drawing.Point(96, 16);
			this.FullScanFromTextBox.Name = "FullScanFromTextBox";
			this.FullScanFromTextBox.Size = new System.Drawing.Size(32, 21);
			this.FullScanFromTextBox.TabIndex = 4;
			this.FullScanFromTextBox.Text = "1";
			// 
			// label13
			// 
			this.label13.Location = new System.Drawing.Point(184, 24);
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size(160, 16);
			this.label13.TabIndex = 37;
			this.label13.Text = "192.168.0.19";
			// 
			// FullScanToTextBox
			// 
			this.FullScanToTextBox.Location = new System.Drawing.Point(96, 56);
			this.FullScanToTextBox.Name = "FullScanToTextBox";
			this.FullScanToTextBox.Size = new System.Drawing.Size(32, 21);
			this.FullScanToTextBox.TabIndex = 5;
			this.FullScanToTextBox.Text = "41";
			// 
			// CheckSinglePanel
			// 
			this.CheckSinglePanel.Controls.Add(this.SingleCheckGroupBox);
			this.CheckSinglePanel.Dock = System.Windows.Forms.DockStyle.Top;
			this.CheckSinglePanel.Location = new System.Drawing.Point(0, 0);
			this.CheckSinglePanel.Name = "CheckSinglePanel";
			this.CheckSinglePanel.Size = new System.Drawing.Size(184, 184);
			this.CheckSinglePanel.TabIndex = 7;
			this.CheckSinglePanel.Visible = false;
			// 
			// label8
			// 
			this.label8.Location = new System.Drawing.Point(184, 24);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(160, 16);
			this.label8.TabIndex = 33;
			this.label8.Text = "192.168.0.13";
			// 
			// ip22label
			// 
			this.ip22label.Location = new System.Drawing.Point(16, 24);
			this.ip22label.Name = "ip22label";
			this.ip22label.Size = new System.Drawing.Size(160, 16);
			this.ip22label.TabIndex = 41;
			this.ip22label.Text = "192.168.0.22";
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(16, 24);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(208, 16);
			this.label4.TabIndex = 0;
			this.label4.Text = "Von 192.168.0.110 bis 192.168.0.141";
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(16, 24);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(160, 16);
			this.label5.TabIndex = 0;
			this.label5.Text = "192.168.0.10";
			// 
			// label6
			// 
			this.label6.Location = new System.Drawing.Point(184, 24);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(160, 16);
			this.label6.TabIndex = 33;
			this.label6.Text = "192.168.0.11";
			// 
			// label7
			// 
			this.label7.Location = new System.Drawing.Point(16, 24);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(160, 16);
			this.label7.TabIndex = 33;
			this.label7.Text = "192.168.0.12";
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.CheckSinglePanel);
			this.BottomPanel.Controls.Add(this.ScanPanel);
			this.BottomPanel.Dock = System.Windows.Forms.DockStyle.Right;
			this.BottomPanel.Location = new System.Drawing.Point(816, 0);
			this.BottomPanel.Name = "BottomPanel";
			this.BottomPanel.Size = new System.Drawing.Size(184, 711);
			this.BottomPanel.TabIndex = 8;
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(16, 24);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(64, 24);
			this.label1.TabIndex = 1;
			this.label1.Text = "192.168.0.";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(8, 16);
			this.label2.Name = "label2";
			this.label2.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.label2.Size = new System.Drawing.Size(88, 24);
			this.label2.TabIndex = 2;
			this.label2.Text = "von 192.168.0.";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(16, 56);
			this.label3.Name = "label3";
			this.label3.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.label3.Size = new System.Drawing.Size(80, 24);
			this.label3.TabIndex = 3;
			this.label3.Text = "bis 192.168.0.";
			this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// groupBox11
			// 
			this.groupBox11.Controls.Add(this.ip20label);
			this.groupBox11.Controls.Add(this.ip21label);
			this.groupBox11.Location = new System.Drawing.Point(8, 648);
			this.groupBox11.Name = "groupBox11";
			this.groupBox11.Size = new System.Drawing.Size(360, 56);
			this.groupBox11.TabIndex = 26;
			this.groupBox11.TabStop = false;
			this.groupBox11.Text = "160";
			// 
			// groupBox10
			// 
			this.groupBox10.Controls.Add(this.ip22label);
			this.groupBox10.Controls.Add(this.label19);
			this.groupBox10.Location = new System.Drawing.Point(8, 584);
			this.groupBox10.Name = "groupBox10";
			this.groupBox10.Size = new System.Drawing.Size(360, 56);
			this.groupBox10.TabIndex = 25;
			this.groupBox10.TabStop = false;
			this.groupBox10.Text = "161";
			// 
			// groupBox13
			// 
			this.groupBox13.Controls.Add(this.label7);
			this.groupBox13.Controls.Add(this.label8);
			this.groupBox13.Location = new System.Drawing.Point(448, 456);
			this.groupBox13.Name = "groupBox13";
			this.groupBox13.Size = new System.Drawing.Size(360, 56);
			this.groupBox13.TabIndex = 28;
			this.groupBox13.TabStop = false;
			this.groupBox13.Text = "156";
			// 
			// groupBox12
			// 
			this.groupBox12.Controls.Add(this.label5);
			this.groupBox12.Controls.Add(this.label6);
			this.groupBox12.Location = new System.Drawing.Point(448, 72);
			this.groupBox12.Name = "groupBox12";
			this.groupBox12.Size = new System.Drawing.Size(360, 56);
			this.groupBox12.TabIndex = 27;
			this.groupBox12.TabStop = false;
			this.groupBox12.Text = "152";
			// 
			// groupBox15
			// 
			this.groupBox15.Controls.Add(this.ip17label);
			this.groupBox15.Controls.Add(this.ip16label);
			this.groupBox15.Location = new System.Drawing.Point(448, 584);
			this.groupBox15.Name = "groupBox15";
			this.groupBox15.Size = new System.Drawing.Size(360, 56);
			this.groupBox15.TabIndex = 30;
			this.groupBox15.TabStop = false;
			this.groupBox15.Text = "158";
			// 
			// groupBox14
			// 
			this.groupBox14.Controls.Add(this.ip14label);
			this.groupBox14.Controls.Add(this.ip15label);
			this.groupBox14.Location = new System.Drawing.Point(448, 520);
			this.groupBox14.Name = "groupBox14";
			this.groupBox14.Size = new System.Drawing.Size(360, 56);
			this.groupBox14.TabIndex = 29;
			this.groupBox14.TabStop = false;
			this.groupBox14.Text = "157";
			// 
			// groupBox17
			// 
			this.groupBox17.Controls.Add(this.textBox1);
			this.groupBox17.Controls.Add(this.label4);
			this.groupBox17.Location = new System.Drawing.Point(448, 232);
			this.groupBox17.Name = "groupBox17";
			this.groupBox17.Size = new System.Drawing.Size(360, 184);
			this.groupBox17.TabIndex = 32;
			this.groupBox17.TabStop = false;
			this.groupBox17.Text = "WLAN Clients";
			// 
			// groupBox16
			// 
			this.groupBox16.Controls.Add(this.label13);
			this.groupBox16.Controls.Add(this.label14);
			this.groupBox16.Location = new System.Drawing.Point(448, 648);
			this.groupBox16.Name = "groupBox16";
			this.groupBox16.Size = new System.Drawing.Size(360, 56);
			this.groupBox16.TabIndex = 31;
			this.groupBox16.TabStop = false;
			this.groupBox16.Text = "159";
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.groupBox18);
			this.panel1.Controls.Add(this.groupBox17);
			this.panel1.Controls.Add(this.groupBox16);
			this.panel1.Controls.Add(this.groupBox15);
			this.panel1.Controls.Add(this.groupBox14);
			this.panel1.Controls.Add(this.groupBox13);
			this.panel1.Controls.Add(this.groupBox12);
			this.panel1.Controls.Add(this.groupBox11);
			this.panel1.Controls.Add(this.groupBox10);
			this.panel1.Controls.Add(this.groupBox8);
			this.panel1.Controls.Add(this.groupBox7);
			this.panel1.Controls.Add(this.groupBox6);
			this.panel1.Controls.Add(this.groupBox5);
			this.panel1.Controls.Add(this.groupBox4);
			this.panel1.Controls.Add(this.groupBox3);
			this.panel1.Controls.Add(this.groupBox2);
			this.panel1.Controls.Add(this.groupBox1);
			this.panel1.Controls.Add(this.groupBox9);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel1.Location = new System.Drawing.Point(0, 0);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(816, 711);
			this.panel1.TabIndex = 9;
			// 
			// groupBox18
			// 
			this.groupBox18.Controls.Add(this.ip1label);
			this.groupBox18.Location = new System.Drawing.Point(448, 168);
			this.groupBox18.Name = "groupBox18";
			this.groupBox18.Size = new System.Drawing.Size(360, 56);
			this.groupBox18.TabIndex = 33;
			this.groupBox18.TabStop = false;
			this.groupBox18.Text = "Fileserver";
			// 
			// statusBar1
			// 
			this.statusBar1.Location = new System.Drawing.Point(0, 711);
			this.statusBar1.Name = "statusBar1";
			this.statusBar1.Panels.AddRange(new System.Windows.Forms.StatusBarPanel[] {
						this.statusBarPanel1});
			this.statusBar1.Size = new System.Drawing.Size(1000, 22);
			this.statusBar1.TabIndex = 2;
			this.statusBar1.Text = "Fertig";
			// 
			// SingleCheckButton
			// 
			this.SingleCheckButton.Location = new System.Drawing.Point(16, 56);
			this.SingleCheckButton.Name = "SingleCheckButton";
			this.SingleCheckButton.Size = new System.Drawing.Size(136, 24);
			this.SingleCheckButton.TabIndex = 2;
			this.SingleCheckButton.Text = "�berpr�fen";
			// 
			// ScanGroupBox
			// 
			this.ScanGroupBox.Controls.Add(this.ScanResultLabel);
			this.ScanGroupBox.Controls.Add(this.AreaGroupBox);
			this.ScanGroupBox.Controls.Add(this.ScanButton);
			this.ScanGroupBox.Location = new System.Drawing.Point(8, 8);
			this.ScanGroupBox.Name = "ScanGroupBox";
			this.ScanGroupBox.Size = new System.Drawing.Size(168, 472);
			this.ScanGroupBox.TabIndex = 0;
			this.ScanGroupBox.TabStop = false;
			this.ScanGroupBox.Text = "Bereichs-Scan";
			// 
			// ip21label
			// 
			this.ip21label.Location = new System.Drawing.Point(184, 24);
			this.ip21label.Name = "ip21label";
			this.ip21label.Size = new System.Drawing.Size(160, 16);
			this.ip21label.TabIndex = 40;
			this.ip21label.Text = "192.168.0.21";
			// 
			// ip16label
			// 
			this.ip16label.Location = new System.Drawing.Point(16, 24);
			this.ip16label.Name = "ip16label";
			this.ip16label.Size = new System.Drawing.Size(160, 16);
			this.ip16label.TabIndex = 35;
			this.ip16label.Text = "192.168.0.16";
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.ip41label);
			this.groupBox1.Controls.Add(this.ip40label);
			this.groupBox1.Location = new System.Drawing.Point(8, 8);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(360, 56);
			this.groupBox1.TabIndex = 16;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "170";
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.label25);
			this.groupBox2.Controls.Add(this.label26);
			this.groupBox2.Location = new System.Drawing.Point(8, 72);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(360, 56);
			this.groupBox2.TabIndex = 17;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "169";
			// 
			// SingleCheckGroupBox
			// 
			this.SingleCheckGroupBox.Controls.Add(this.SingleCheckButton);
			this.SingleCheckGroupBox.Controls.Add(this.label1);
			this.SingleCheckGroupBox.Controls.Add(this.SingleCheckTextBox);
			this.SingleCheckGroupBox.Location = new System.Drawing.Point(8, 8);
			this.SingleCheckGroupBox.Name = "SingleCheckGroupBox";
			this.SingleCheckGroupBox.Size = new System.Drawing.Size(168, 160);
			this.SingleCheckGroupBox.TabIndex = 0;
			this.SingleCheckGroupBox.TabStop = false;
			this.SingleCheckGroupBox.Text = "Spezifische �berpr�fung";
			// 
			// groupBox4
			// 
			this.groupBox4.Controls.Add(this.label29);
			this.groupBox4.Controls.Add(this.label30);
			this.groupBox4.Location = new System.Drawing.Point(8, 200);
			this.groupBox4.Name = "groupBox4";
			this.groupBox4.Size = new System.Drawing.Size(360, 56);
			this.groupBox4.TabIndex = 19;
			this.groupBox4.TabStop = false;
			this.groupBox4.Text = "167";
			// 
			// groupBox5
			// 
			this.groupBox5.Controls.Add(this.label31);
			this.groupBox5.Controls.Add(this.label32);
			this.groupBox5.Location = new System.Drawing.Point(8, 264);
			this.groupBox5.Name = "groupBox5";
			this.groupBox5.Size = new System.Drawing.Size(360, 56);
			this.groupBox5.TabIndex = 20;
			this.groupBox5.TabStop = false;
			this.groupBox5.Text = "166";
			// 
			// ip20label
			// 
			this.ip20label.Location = new System.Drawing.Point(16, 24);
			this.ip20label.Name = "ip20label";
			this.ip20label.Size = new System.Drawing.Size(160, 16);
			this.ip20label.TabIndex = 39;
			this.ip20label.Text = "192.168.0.20";
			// 
			// groupBox7
			// 
			this.groupBox7.Controls.Add(this.ip28label);
			this.groupBox7.Controls.Add(this.label35);
			this.groupBox7.Location = new System.Drawing.Point(8, 392);
			this.groupBox7.Name = "groupBox7";
			this.groupBox7.Size = new System.Drawing.Size(360, 56);
			this.groupBox7.TabIndex = 22;
			this.groupBox7.TabStop = false;
			this.groupBox7.Text = "164";
			// 
			// groupBox8
			// 
			this.groupBox8.Controls.Add(this.label22);
			this.groupBox8.Controls.Add(this.label37);
			this.groupBox8.Location = new System.Drawing.Point(8, 456);
			this.groupBox8.Name = "groupBox8";
			this.groupBox8.Size = new System.Drawing.Size(360, 56);
			this.groupBox8.TabIndex = 23;
			this.groupBox8.TabStop = false;
			this.groupBox8.Text = "163";
			// 
			// groupBox9
			// 
			this.groupBox9.Controls.Add(this.label20);
			this.groupBox9.Controls.Add(this.label21);
			this.groupBox9.Location = new System.Drawing.Point(8, 520);
			this.groupBox9.Name = "groupBox9";
			this.groupBox9.Size = new System.Drawing.Size(360, 56);
			this.groupBox9.TabIndex = 24;
			this.groupBox9.TabStop = false;
			this.groupBox9.Text = "162";
			// 
			// statusBarPanel1
			// 
			this.statusBarPanel1.Text = "statusBarPanel1";
			// 
			// groupBox3
			// 
			this.groupBox3.Controls.Add(this.label27);
			this.groupBox3.Controls.Add(this.label28);
			this.groupBox3.Location = new System.Drawing.Point(8, 136);
			this.groupBox3.Name = "groupBox3";
			this.groupBox3.Size = new System.Drawing.Size(360, 56);
			this.groupBox3.TabIndex = 18;
			this.groupBox3.TabStop = false;
			this.groupBox3.Text = "168";
			// 
			// MainForm
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
			this.ClientSize = new System.Drawing.Size(1000, 733);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.BottomPanel);
			this.Controls.Add(this.statusBar1);
			this.Name = "MainForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "MainForm";
			this.Closing += new System.ComponentModel.CancelEventHandler(this.MainFormClosing);
			this.AreaGroupBox.ResumeLayout(false);
			this.groupBox6.ResumeLayout(false);
			this.ScanPanel.ResumeLayout(false);
			this.CheckSinglePanel.ResumeLayout(false);
			this.BottomPanel.ResumeLayout(false);
			this.groupBox11.ResumeLayout(false);
			this.groupBox10.ResumeLayout(false);
			this.groupBox13.ResumeLayout(false);
			this.groupBox12.ResumeLayout(false);
			this.groupBox15.ResumeLayout(false);
			this.groupBox14.ResumeLayout(false);
			this.groupBox17.ResumeLayout(false);
			this.groupBox16.ResumeLayout(false);
			this.panel1.ResumeLayout(false);
			this.groupBox18.ResumeLayout(false);
			this.ScanGroupBox.ResumeLayout(false);
			this.groupBox1.ResumeLayout(false);
			this.groupBox2.ResumeLayout(false);
			this.SingleCheckGroupBox.ResumeLayout(false);
			this.groupBox4.ResumeLayout(false);
			this.groupBox5.ResumeLayout(false);
			this.groupBox7.ResumeLayout(false);
			this.groupBox8.ResumeLayout(false);
			this.groupBox9.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.statusBarPanel1)).EndInit();
			this.groupBox3.ResumeLayout(false);
			this.ResumeLayout(false);
		}
		
		void FullScan()
		{
			ScanButton.Text = "Abbrechen";
			ScanResultLabel.Text = "";
			
			int from = Convert.ToInt16(FullScanFromTextBox.Text);
			int to = Convert.ToInt16(FullScanToTextBox.Text);
			
			for(int i = from; i<=to; i++)
			{
				string Addr = "192.168.0.";
				Addr += Convert.ToString(i);
				
				statusBar1.Text = "Pr�fe: ";
				statusBar1.Text += Addr;
				
				if(Ping.ping(Addr)==1)
				{
					ScanResultLabel.Text += Addr;
					
					IPHostEntry hostInfo = Dns.GetHostByAddress(IPAddress.Parse(Addr));
					ScanResultLabel.Text += " ";
					ScanResultLabel.Text += hostInfo.HostName.ToLower();
					ScanResultLabel.Text += "\r\n";
//					
					MarkInterface(i, hostInfo.HostName.ToLower());
				}
				
				if(ScanButton.Enabled==false)
				{
					ScanButton.Enabled = true;
					break;
				}
			}
			
			statusBar1.Text = "Fertig";
			ScanButton.Text = "Scannen";
		}
		
		void MainFormClosing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			Environment.Exit(0);
		}
		
		void ScanButtonClick(object sender, System.EventArgs e)
		{
			if(ScanButton.Text=="Scannen")
			{
				new Thread(new ThreadStart(FullScan)).Start();
			}
			else if(ScanButton.Text=="Abbrechen")
			{
				ScanButton.Enabled = false;
			}
		}
		
		void MarkInterface(int ip, string name)
		{
			switch(ip)
			{
			case 1:
				{
						if(name!="")
						{
							ip1label.ForeColor = Color.Green;
							ip1label.Text = "192.168.0.1 ";
							ip1label.Text += name;
						}
						else
						{
							ip1label.ForeColor = Color.Black;
							ip1label.Text = "192.168.0.1";
						}
					break;
				}
			case 14:
				{
						if(name!="")
						{
							ip14label.ForeColor = Color.Green;
							ip14label.Text = "192.168.0.14 ";
							ip14label.Text += name;
						}
						else
						{
							ip14label.ForeColor = Color.Black;
							ip14label.Text = "192.168.0.14";
						}
					break;
				}
			case 15:
				{
						if(name!="")
						{
							ip15label.ForeColor = Color.Green;
							ip15label.Text = "192.168.0.15 ";
							ip15label.Text += name;
						}
						else
						{
							ip15label.ForeColor = Color.Black;
							ip15label.Text = "192.168.0.15";
						}
					break;
				}
			case 16:
				{
						if(name!="")
						{
							ip16label.ForeColor = Color.Green;
							ip16label.Text = "192.168.0.16 ";
							ip16label.Text += name;
						}
						else
						{
							ip16label.ForeColor = Color.Black;
							ip16label.Text = "192.168.0.16";
						}
					break;
				}
			case 17:
				{
						if(name!="")
						{
							ip17label.ForeColor = Color.Green;
							ip17label.Text = "192.168.0.17 ";
							ip17label.Text += name;
						}
						else
						{
							ip17label.ForeColor = Color.Black;
							ip17label.Text = "192.168.0.17";
						}
					break;
				}
			case 20:
				{
						if(name!="")
						{
							ip20label.ForeColor = Color.Green;
							ip20label.Text = "192.168.0.20 ";
							ip20label.Text += name;
						}
						else
						{
							ip20label.ForeColor = Color.Black;
							ip20label.Text = "192.168.0.20";
						}
					break;
				}
			case 21:
				{
						if(name!="")
						{
							ip21label.ForeColor = Color.Green;
							ip21label.Text = "192.168.0.21 ";
							ip21label.Text += name;
						}
						else
						{
							ip21label.ForeColor = Color.Black;
							ip21label.Text = "192.168.0.21";
						}
					break;
				}
			case 22:
				{
						if(name!="")
						{
							ip22label.ForeColor = Color.Green;
							ip22label.Text = "192.168.0.22 ";
							ip22label.Text += name;
						}
						else
						{
							ip22label.ForeColor = Color.Black;
							ip22label.Text = "192.168.0.22";
						}
					break;
				}
			case 28:
				{
						if(name!="")
						{
							ip28label.ForeColor = Color.Green;
							ip28label.Text = "192.168.0.28 ";
							ip28label.Text += name;
						}
						else
						{
							ip28label.ForeColor = Color.Black;
							ip28label.Text = "192.168.0.28";
						}
					break;
				}
			case 40:
				{
						if(name!="")
						{
							ip40label.ForeColor = Color.Green;
							ip40label.Text = "192.168.0.40 ";
							ip40label.Text += name;
						}
						else
						{
							ip40label.ForeColor = Color.Black;
							ip40label.Text = "192.168.0.40";
						}
					break;
				}
			case 41:
				{
						if(name!="")
						{
							ip41label.ForeColor = Color.Green;
							ip41label.Text = "192.168.0.41 ";
							ip41label.Text += name;
						}
						else
						{
							ip41label.ForeColor = Color.Black;
							ip41label.Text = "192.168.0.41";
						}
					break;
				}
			}
		}
		
	}
}
