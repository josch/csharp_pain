using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Management;

namespace WinXPKeyGen
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            ManagementObjectCollection wmicoll = (new ManagementObjectSearcher("SELECT * FROM Win32_WindowsProductActivation")).Get();
            try
            {
                foreach (ManagementObject obj in wmicoll)
                {
                    if ((UInt32)obj.Properties["ActivationRequired"].Value == 1)
                    {
                        Random ran = new Random();
                        int value = Math.Ceiling(ran.NextDouble() * 36);
                        switch (value)
                        {
                        }
                        obj.InvokeMethod("SetProductKey", new object[] { "HMT4VK8GJHW86HKMBH6DFJV9G" });
                    }
                }
            }
            catch (ManagementException me)
            {
                MessageBox.Show(me.Message);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }
    }
}