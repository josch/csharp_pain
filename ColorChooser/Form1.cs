using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ColorChooser
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void tbRot_Scroll(object sender, EventArgs e)
        {
            UpdatePanels();
        }

        private void tbGr�n_Scroll(object sender, EventArgs e)
        {
            UpdatePanels();
        }

        private void tbBlau_Scroll(object sender, EventArgs e)
        {
            UpdatePanels();
        }

        void UpdatePanels()
        {
            panel1.BackColor = Color.FromArgb(tbRot.Value, tbGr�n.Value, tbBlau.Value);
            panel2.BackColor = Color.FromArgb(255-tbRot.Value, 255-tbGr�n.Value, 255-tbBlau.Value);
            tbFarbe1.Text = "R: " + tbRot.Value.ToString() + " G: " + tbGr�n.Value.ToString()
                + " B: " + tbBlau.Value.ToString() + " #" + String.Format("{0:x2}", tbRot.Value).ToUpper()
                + String.Format("{0:x2}", tbGr�n.Value).ToUpper() + String.Format("{0:x2}", tbBlau.Value).ToUpper();
            tbFarbe2.Text = "R: " + (255-tbRot.Value).ToString() + " G: " + (255-tbGr�n.Value).ToString()
                + " B: " + (255-tbBlau.Value).ToString() + " #" + String.Format("{0:x2}", 255-tbRot.Value).ToUpper()
                + String.Format("{0:x2}", 255-tbGr�n.Value).ToUpper() + String.Format("{0:x2}", 255-tbBlau.Value).ToUpper();
        }
    }
}