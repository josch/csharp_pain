namespace ColorChooser
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.tbRot = new System.Windows.Forms.TrackBar();
            this.tbGr�n = new System.Windows.Forms.TrackBar();
            this.tbBlau = new System.Windows.Forms.TrackBar();
            this.tbFarbe1 = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.tbFarbe2 = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.tbRot)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbGr�n)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbBlau)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(304, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(219, 190);
            this.panel1.TabIndex = 0;
            // 
            // tbRot
            // 
            this.tbRot.Location = new System.Drawing.Point(12, 12);
            this.tbRot.Maximum = 255;
            this.tbRot.Name = "tbRot";
            this.tbRot.Size = new System.Drawing.Size(286, 42);
            this.tbRot.TabIndex = 1;
            this.tbRot.TickFrequency = 10;
            this.tbRot.Scroll += new System.EventHandler(this.tbRot_Scroll);
            // 
            // tbGr�n
            // 
            this.tbGr�n.Location = new System.Drawing.Point(12, 60);
            this.tbGr�n.Maximum = 255;
            this.tbGr�n.Name = "tbGr�n";
            this.tbGr�n.Size = new System.Drawing.Size(286, 42);
            this.tbGr�n.TabIndex = 2;
            this.tbGr�n.TickFrequency = 10;
            this.tbGr�n.Scroll += new System.EventHandler(this.tbGr�n_Scroll);
            // 
            // tbBlau
            // 
            this.tbBlau.Location = new System.Drawing.Point(12, 108);
            this.tbBlau.Maximum = 255;
            this.tbBlau.Name = "tbBlau";
            this.tbBlau.Size = new System.Drawing.Size(286, 42);
            this.tbBlau.TabIndex = 3;
            this.tbBlau.TickFrequency = 10;
            this.tbBlau.Scroll += new System.EventHandler(this.tbBlau_Scroll);
            // 
            // tbFarbe1
            // 
            this.tbFarbe1.Location = new System.Drawing.Point(12, 156);
            this.tbFarbe1.Name = "tbFarbe1";
            this.tbFarbe1.Size = new System.Drawing.Size(286, 20);
            this.tbFarbe1.TabIndex = 4;
            // 
            // panel2
            // 
            this.panel2.Location = new System.Drawing.Point(529, 12);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(208, 190);
            this.panel2.TabIndex = 5;
            // 
            // tbFarbe2
            // 
            this.tbFarbe2.Location = new System.Drawing.Point(12, 182);
            this.tbFarbe2.Name = "tbFarbe2";
            this.tbFarbe2.Size = new System.Drawing.Size(286, 20);
            this.tbFarbe2.TabIndex = 6;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(747, 212);
            this.Controls.Add(this.tbFarbe2);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.tbFarbe1);
            this.Controls.Add(this.tbBlau);
            this.Controls.Add(this.tbGr�n);
            this.Controls.Add(this.tbRot);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "Form1";
            this.Text = "ColorChooser";
            ((System.ComponentModel.ISupportInitialize)(this.tbRot)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbGr�n)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbBlau)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TrackBar tbRot;
        private System.Windows.Forms.TrackBar tbGr�n;
        private System.Windows.Forms.TrackBar tbBlau;
        private System.Windows.Forms.TextBox tbFarbe1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox tbFarbe2;
    }
}

