using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace HexViewer
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                FileStream fs = File.Open(openFileDialog1.FileName, FileMode.Open);
                tbHEX.Text = "";
                for (int i = 0; i < fs.Length; i++ )
                {
                    if (i % 3 == 0)
                    {
                        tbHEX.Text += " ";
                    }
                    if (i - 54 % 150 == 0)
                    {
                        tbHEX.Text += "\r\n";
                    }
                    tbHEX.Text += String.Format("{0:x2}", fs.ReadByte()).ToUpper();
                    i++;
                }
                fs.Close();
            }
        }
    }
}