using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using IWshRuntimeLibrary;

namespace Install
{
    public partial class Main : Form
    {
        public Main()
        {
            InitializeComponent();
        }

        private void bFolder_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                lFolder.Text = "Folder: " + folderBrowserDialog1.SelectedPath;
            }
        }

        private void bFile_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                lFile.Text = "File: " + openFileDialog1.FileName;
            }
        }

        private void bStart_Click(object sender, EventArgs e)
        {
            string filename = openFileDialog1.FileName.Split('\\')[openFileDialog1.FileName.Split('\\').Length - 1];
            string destinationpath = folderBrowserDialog1.SelectedPath + "\\" + filename;
            System.IO.File.Copy(openFileDialog1.FileName, destinationpath);

            WshShell shell = new WshShell();
            if (cbStartup.Checked)
            {
                IWshShortcut autostart = (IWshShortcut)shell.CreateShortcut("C:\\neu.nlk");
                autostart.TargetPath = destinationpath;
                autostart.Save();
            }
            if (cbDesktop.Checked)
            {
                IWshShortcut desktop = (IWshShortcut)shell.CreateShortcut(Environment.SpecialFolder.DesktopDirectory.ToString() + "\\" + filename);
                desktop.TargetPath = destinationpath;
                desktop.Save();
            }
            if (cbStartmenu.Checked)
            {
                IWshShortcut startmenu = (IWshShortcut)shell.CreateShortcut(Environment.SpecialFolder.Programs.ToString() + "\\" + filename);
                startmenu.TargetPath = destinationpath;
                startmenu.Save();
            }
        }
    }
}