namespace Install
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lFile = new System.Windows.Forms.Label();
            this.lFolder = new System.Windows.Forms.Label();
            this.cbStartup = new System.Windows.Forms.CheckBox();
            this.cbDesktop = new System.Windows.Forms.CheckBox();
            this.cbStartmenu = new System.Windows.Forms.CheckBox();
            this.bFile = new System.Windows.Forms.Button();
            this.bFolder = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.bStart = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lFile
            // 
            this.lFile.AutoSize = true;
            this.lFile.Location = new System.Drawing.Point(12, 9);
            this.lFile.Name = "lFile";
            this.lFile.Size = new System.Drawing.Size(22, 13);
            this.lFile.TabIndex = 0;
            this.lFile.Text = "File:";
            // 
            // lFolder
            // 
            this.lFolder.AutoSize = true;
            this.lFolder.Location = new System.Drawing.Point(12, 47);
            this.lFolder.Name = "lFolder";
            this.lFolder.Size = new System.Drawing.Size(35, 13);
            this.lFolder.TabIndex = 1;
            this.lFolder.Text = "Folder:";
            // 
            // cbStartup
            // 
            this.cbStartup.AutoSize = true;
            this.cbStartup.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbStartup.Location = new System.Drawing.Point(12, 88);
            this.cbStartup.Name = "cbStartup";
            this.cbStartup.Size = new System.Drawing.Size(91, 17);
            this.cbStartup.TabIndex = 2;
            this.cbStartup.Text = "Run on Startup";
            // 
            // cbDesktop
            // 
            this.cbDesktop.AutoSize = true;
            this.cbDesktop.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbDesktop.Location = new System.Drawing.Point(12, 111);
            this.cbDesktop.Name = "cbDesktop";
            this.cbDesktop.Size = new System.Drawing.Size(102, 17);
            this.cbDesktop.TabIndex = 3;
            this.cbDesktop.Text = "Desktop Shortcut";
            // 
            // cbStartmenu
            // 
            this.cbStartmenu.AutoSize = true;
            this.cbStartmenu.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbStartmenu.Location = new System.Drawing.Point(12, 134);
            this.cbStartmenu.Name = "cbStartmenu";
            this.cbStartmenu.Size = new System.Drawing.Size(108, 17);
            this.cbStartmenu.TabIndex = 4;
            this.cbStartmenu.Text = "Show in Startmenu";
            // 
            // bFile
            // 
            this.bFile.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bFile.Location = new System.Drawing.Point(12, 25);
            this.bFile.Name = "bFile";
            this.bFile.Size = new System.Drawing.Size(85, 19);
            this.bFile.TabIndex = 6;
            this.bFile.Text = "Open *.exe";
            this.bFile.Click += new System.EventHandler(this.bFile_Click);
            // 
            // bFolder
            // 
            this.bFolder.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bFolder.Location = new System.Drawing.Point(12, 63);
            this.bFolder.Name = "bFolder";
            this.bFolder.Size = new System.Drawing.Size(85, 19);
            this.bFolder.TabIndex = 7;
            this.bFolder.Text = "Open Folder";
            this.bFolder.Click += new System.EventHandler(this.bFolder_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.Filter = "Executables (*.exe)|*.exe|All files (*.*)|*.*";
            this.openFileDialog1.InitialDirectory = "/";
            // 
            // folderBrowserDialog1
            // 
            this.folderBrowserDialog1.Description = "Einen neuen Ordner erstellen und OK klicken";
            this.folderBrowserDialog1.RootFolder = System.Environment.SpecialFolder.ProgramFiles;
            this.folderBrowserDialog1.SelectedPath = "folderBrowserDialog1";
            // 
            // bStart
            // 
            this.bStart.Location = new System.Drawing.Point(187, 99);
            this.bStart.Name = "bStart";
            this.bStart.Size = new System.Drawing.Size(81, 40);
            this.bStart.TabIndex = 8;
            this.bStart.Text = "Start";
            this.bStart.Click += new System.EventHandler(this.bStart_Click);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(301, 157);
            this.Controls.Add(this.bStart);
            this.Controls.Add(this.bFolder);
            this.Controls.Add(this.bFile);
            this.Controls.Add(this.cbStartmenu);
            this.Controls.Add(this.cbDesktop);
            this.Controls.Add(this.cbStartup);
            this.Controls.Add(this.lFolder);
            this.Controls.Add(this.lFile);
            this.Name = "Main";
            this.Text = "Install";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lFile;
        private System.Windows.Forms.Label lFolder;
        private System.Windows.Forms.CheckBox cbStartup;
        private System.Windows.Forms.CheckBox cbDesktop;
        private System.Windows.Forms.CheckBox cbStartmenu;
        private System.Windows.Forms.Button bFile;
        private System.Windows.Forms.Button bFolder;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.Button bStart;
    }
}

