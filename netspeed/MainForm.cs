/*
 * Created by SharpDevelop.
 * User: Johannes
 * Date: 25.05.2005
 * Time: 21:00
 *
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Drawing;
using System.Windows.Forms;
using System.Threading;
using System.Net;
using System.IO;

namespace netspeed
{
	/// <summary>
	/// Description of MainForm.
	/// </summary>
	public class MainForm : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Panel HGpanel;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.Button RunButton;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Button AbortButton;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.ProgressBar progress;
		
		private Thread t = null;
		private Thread t2 = null;
		
		public MainForm()
		{
			InitializeComponent();
			
		}
		
		[STAThread]
		public static void Main(string[] args)
		{
			Application.Run(new MainForm());
		}
		
		#region Windows Forms Designer generated code
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent() {
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(MainForm));
			this.progress = new System.Windows.Forms.ProgressBar();
			this.label4 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.AbortButton = new System.Windows.Forms.Button();
			this.label7 = new System.Windows.Forms.Label();
			this.panel1 = new System.Windows.Forms.Panel();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.RunButton = new System.Windows.Forms.Button();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.HGpanel = new System.Windows.Forms.Panel();
			this.panel1.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.groupBox1.SuspendLayout();
			this.SuspendLayout();
			// 
			// progress
			// 
			this.progress.Location = new System.Drawing.Point(8, 24);
			this.progress.Name = "progress";
			this.progress.Size = new System.Drawing.Size(232, 24);
			this.progress.Step = 1;
			this.progress.TabIndex = 6;
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(16, 56);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(216, 24);
			this.label4.TabIndex = 7;
			this.label4.Text = "Vollständig:";
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(16, 56);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(216, 24);
			this.label5.TabIndex = 9;
			this.label5.Text = "Verbleibend:";
			// 
			// AbortButton
			// 
			this.AbortButton.Location = new System.Drawing.Point(176, 8);
			this.AbortButton.Name = "AbortButton";
			this.AbortButton.Size = new System.Drawing.Size(72, 24);
			this.AbortButton.TabIndex = 8;
			this.AbortButton.Text = "A&bbrechen";
			this.AbortButton.Click += new System.EventHandler(this.AbortButtonClick);
			// 
			// label7
			// 
			this.label7.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label7.Location = new System.Drawing.Point(40, 64);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(184, 32);
			this.label7.TabIndex = 12;
			this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// panel1
			// 
			this.panel1.BackColor = System.Drawing.Color.Red;
			this.panel1.Controls.Add(this.HGpanel);
			this.panel1.Location = new System.Drawing.Point(272, 8);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(24, 336);
			this.panel1.TabIndex = 5;
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(16, 24);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(216, 24);
			this.label1.TabIndex = 1;
			this.label1.Text = "Vergangen:";
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(16, 40);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(216, 24);
			this.label2.TabIndex = 2;
			this.label2.Text = "Geschwindigkeit:";
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(16, 80);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(216, 23);
			this.label3.TabIndex = 3;
			this.label3.Text = "Geladen:";
			// 
			// label6
			// 
			this.label6.Location = new System.Drawing.Point(16, 88);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(216, 23);
			this.label6.TabIndex = 10;
			this.label6.Text = "Gesamt:";
			// 
			// RunButton
			// 
			this.RunButton.Location = new System.Drawing.Point(8, 8);
			this.RunButton.Name = "RunButton";
			this.RunButton.Size = new System.Drawing.Size(136, 24);
			this.RunButton.TabIndex = 0;
			this.RunButton.Text = "&Run Benchmark!";
			this.RunButton.Click += new System.EventHandler(this.RunButtonClick);
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.label6);
			this.groupBox2.Controls.Add(this.label1);
			this.groupBox2.Controls.Add(this.label5);
			this.groupBox2.Location = new System.Drawing.Point(8, 224);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(248, 120);
			this.groupBox2.TabIndex = 11;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Zeit";
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.label3);
			this.groupBox1.Controls.Add(this.label4);
			this.groupBox1.Controls.Add(this.progress);
			this.groupBox1.Location = new System.Drawing.Point(8, 104);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(248, 112);
			this.groupBox1.TabIndex = 10;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Fortschritt";
			// 
			// HGpanel
			// 
			this.HGpanel.BackColor = System.Drawing.Color.White;
			this.HGpanel.Location = new System.Drawing.Point(0, 0);
			this.HGpanel.Name = "HGpanel";
			this.HGpanel.Size = new System.Drawing.Size(24, 336);
			this.HGpanel.TabIndex = 4;
			// 
			// MainForm
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
			this.ClientSize = new System.Drawing.Size(304, 349);
			this.Controls.Add(this.label7);
			this.Controls.Add(this.groupBox2);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.AbortButton);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.RunButton);
			this.Controls.Add(this.panel1);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "MainForm";
			this.Text = "netspeed";
			this.Closed += new System.EventHandler(this.MainFormClosed);
			this.panel1.ResumeLayout(false);
			this.groupBox2.ResumeLayout(false);
			this.groupBox1.ResumeLayout(false);
			this.ResumeLayout(false);
		}
		#endregion
		void RunButtonClick(object sender, System.EventArgs e)
		{
			RunButton.Enabled = false;
			t = new Thread(new ThreadStart(RunBenchmark));
			t.Start();
		}
		
		void MainFormClosed(object sender, System.EventArgs e)
		{
			if(t!=null)
			{
				t.Abort();
			}
			if(t2!=null)
			{
				t2.Abort();
			}
		}
		
		void RunBenchmark()
		{
			WebClient wc = new WebClient();
			
			DateTime before = DateTime.Now;
			
			t2 = new Thread(new ThreadStart(ShowInfo));
			t2.Start();
			wc.DownloadFile("http://192.168.0.1/test.avi", "test.avi");
			t2.Abort();
			
			TimeSpan duration = DateTime.Now.Subtract(before);
			
			label2.Text = "Geschwindigkeit: "+Math.Round(548672/duration.TotalSeconds).ToString()+" kByte/s\r\n";
			label3.Text = "Geladen: 535.8 MByte";
			
			switch(MessageBox.Show("Soll die heruntergeladene Datei gelöscht werden?", "Löschen?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1)) {
				case DialogResult.Yes:
					File.Delete("test.avi");
					break;
				case DialogResult.No:
					break;
			}
			
			RunButton.Enabled = true;
		}
		
		void ShowInfo()
		{
			DateTime before = DateTime.Now;
			float filesize = 0.0f;
			
			while(true)
			{
				FileInfo fi = new FileInfo("test.avi");
				if(fi.Exists)
				{
					filesize = fi.Length;
				}
				TimeSpan duration = DateTime.Now.Subtract(before);
				label1.Text = "Vergangen: "+TimeSpan.FromSeconds(Math.Round(Convert.ToDouble(duration.TotalSeconds),0));
				if(duration.TotalSeconds!=0)
				{
					label2.Text = "Geschwindigkeit: "+Math.Round((filesize/1024)/duration.TotalSeconds).ToString()+" kByte/s";
					label7.Text = Math.Round((filesize*0.000008)/duration.TotalSeconds,2).ToString()+" MBit/s";
					label5.Text = "Verbleibend: "+TimeSpan.FromSeconds(Math.Round((535.8-filesize/1048576)/(filesize/1048576/duration.TotalSeconds),0));
					label6.Text = "Gesamt: "+TimeSpan.FromSeconds(Math.Round((535.8)/(filesize/1048576/duration.TotalSeconds),0));
					HGpanel.SetBounds(0,0,24,Convert.ToInt32((1-Math.Round((filesize*0.00000008)/duration.TotalSeconds,2))*336));
				}
				label3.Text = "Geladen: "+Math.Round((filesize/1048576),2).ToString()+" MByte von 535.8 MByte";
				label4.Text = "Vollständig: "+Math.Round((((filesize/1048576)/535.8)*100),0).ToString()+"%";
				
				progress.Value = Convert.ToInt32((((filesize/1048576)/535.8)*100));
				Thread.Sleep(1000);
			}
		}
		void AbortButtonClick(object sender, System.EventArgs e)
		{
			if(t!=null)
			{
				t.Abort();
			}
			if(t2!=null)
			{
				t2.Abort();
			}
			RunButton.Enabled = true;
		}
		
		void MainFormLoad(object sender, System.EventArgs e)
		{
			
		}
		
		void TextBox1TextChanged(object sender, System.EventArgs e)
		{
					}
		
	}
}
