/*
 * Created by SharpDevelop.
 * User: Johannes
 * Date: 14.03.2005
 * Time: 22:53
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.IO;
using System.Text;

namespace Replace
{
	class MainClass
	{
		public static void Main(string[] args)
		{
			Console.WriteLine("Beginne ProcessDirectory()");
			ProcessDirectory(Directory.GetCurrentDirectory());
		}
		
		public static void ProcessDirectory(string path)
		{
			Console.WriteLine("Bearbeite {0}", path);
			string[] fileEntries = Directory.GetFileSystemEntries(path, "*.html");
			foreach(string fileName in fileEntries)
			{
				ProcessFile(fileName);
			}
			
			string[] subdirectoryEntries = Directory.GetDirectories(path);
			foreach(string subdirectory in subdirectoryEntries)
			{
				ProcessDirectory(subdirectory);
			}
		}
		
		public static void ProcessFile(string filename)
		{
			Console.WriteLine("Bearbeite {0}", filename);
			string line = String.Empty;
			try 
        	{
        	    StreamReader sr = new StreamReader(filename);
        	    line = sr.ReadToEnd();
        	    sr.Close();
        	    
        	    StringBuilder sb = new StringBuilder(line);
        	    sb.Replace("http://www.d2wissen.de/items", "..");
        	    
        	    StreamWriter sw = new StreamWriter(filename);
        	    sw.Write(sb.ToString());
        	    sw.Close();
        	}
        	catch (Exception e) 
        	{
        	    Console.WriteLine("{0} could not be read.",filename);
        	    Console.WriteLine(e.Message);
        	}
		}
	}
}
