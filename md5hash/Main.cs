/*
 * Created by SharpDevelop.
 * User: Johannes
 * Date: 09.03.2005
 * Time: 15:24
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Security.Cryptography;
using System.Text;

namespace md5hash
{
	class MainClass
	{
		public static void Main(string[] args)
		{
			MD5 md5 = new MD5CryptoServiceProvider();
			ASCIIEncoding encoder = new ASCIIEncoding();
			
			Console.WriteLine(encoder.GetString(md5.ComputeHash(encoder.GetBytes("i"))));
		}
	}
}
