/*
 * User: aputze
 * Date: 03.11.2004
 * Time: 11:11
 */

using System;
using System.Collections;

using System.Data;

namespace ViwoTour {
	public class CoursePlan : DataTable {
		Hashtable Rows_By_DEPARTURE_STATION;
		Hashtable Rows_By_ARRIVAL_STATION;
		
		public CoursePlan() : base("COURSE_PLAN") {
			Columns.Add(new DataColumn("ID",typeof(int)));
			Columns.Add(new DataColumn("DEPARTURE",typeof(DateTime)));
			Columns.Add(new DataColumn("DEPARTURE_STATION",typeof(string)));
			Columns.Add(new DataColumn("ARRIVAL",typeof(DateTime)));
			Columns.Add(new DataColumn("ARRIVAL_STATION",typeof(string)));
		}
		
		public ArrayList get_Rows_By_DEPARTURE_STATION(string s) { return (ArrayList)Rows_By_DEPARTURE_STATION[s]; }
		public ArrayList get_Rows_By_ARRIVAL_STATION(string s) { return (ArrayList)Rows_By_ARRIVAL_STATION[s]; }
		
		public void create_Rows_By_DEPARTURE_STATION() {
			Rows_By_DEPARTURE_STATION=new Hashtable();
			
			for(int i=0;i<Rows.Count;i++) {
				DataRow row=Rows[i];
				
				if(!Rows_By_DEPARTURE_STATION.Contains(row["DEPARTURE_STATION"])) {
					Rows_By_DEPARTURE_STATION.Add(row["DEPARTURE_STATION"],new ArrayList());
				}
				((ArrayList)Rows_By_DEPARTURE_STATION[row["DEPARTURE_STATION"]]).Add(i);
			}
		}
		public void create_Rows_By_ARRIVAL_STATION() {
			Rows_By_ARRIVAL_STATION=new Hashtable();
			
			for(int i=0;i<Rows.Count;i++) {
				DataRow row=Rows[i];
				
				if(!Rows_By_ARRIVAL_STATION.Contains(row["ARRIVAL_STATION"])) {
					Rows_By_ARRIVAL_STATION.Add(row["ARRIVAL_STATION"],new ArrayList());
				}
				((ArrayList)Rows_By_ARRIVAL_STATION[row["ARRIVAL_STATION"]]).Add(i);
			}
		}
	}
}
