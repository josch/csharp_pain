/*
 * User: aputze
 * Date: 03.11.2004
 * Time: 11:09
 */

using System;
using System.Collections;

using System.Data;

namespace ViwoTour {
	public class CourseBook : DataTable {
		ArrayList ID_Of_Rows;
		ArrayList STATION_Of_Rows;
		
		public CourseBook() : base("COURSE_BOOK") {
			Columns.Add(new DataColumn("ID",typeof(int)));
			Columns.Add(new DataColumn("DEPARTUR",typeof(DateTime)));
			Columns.Add(new DataColumn("DEPARTURE_STATION",typeof(string)));
			Columns.Add(new DataColumn("ARRIVAL",typeof(DateTime)));
			Columns.Add(new DataColumn("ARRIVAL_STATION",typeof(string)));
		}
		
		public ArrayList get_ID_Of_Rows() { return ID_Of_Rows; }
		public ArrayList get_STATION_Of_Rows() { return STATION_Of_Rows; }
		
		public void create_ID_Of_Rows() {
			ID_Of_Rows=new ArrayList();
			
			foreach(DataRow row in Rows) {
				if(!ID_Of_Rows.Contains(row["ID"])) {
					ID_Of_Rows.Add(row["ID"]);
				}
			}
		}
		public void create_STATION_Of_Rows() {
			STATION_Of_Rows=new ArrayList();
			
			ArrayList DEPARTURE_STATION_Of_Rows=new ArrayList();
			foreach(DataRow row in Rows) {
				if(!DEPARTURE_STATION_Of_Rows.Contains(row["DEPARTURE_STATION"])) {
					DEPARTURE_STATION_Of_Rows.Add(row["DEPARTURE_STATION"]);
				}
			}
		
			ArrayList ARRIVAL_STATION_Of_Rows=new ArrayList();
			foreach(DataRow row in Rows) {
				if(!ARRIVAL_STATION_Of_Rows.Contains(row["ARRIVAL_STATION"])) {
					ARRIVAL_STATION_Of_Rows.Add(row["ARRIVAL_STATION"]);
				}
			}
			
			foreach(string s in DEPARTURE_STATION_Of_Rows) {
				if(ARRIVAL_STATION_Of_Rows.Contains(s)) {
					STATION_Of_Rows.Add(s);
				}
			}
		}
	}
}
