/*
 * User: aputze
 * Date: 28.10.2004
 * Time: 15:48
 */

using System;

namespace ViwoTour {
	public class EdgeData {
		public static int lastId=0;
		public int Id;
		
		public TimeSpan Duration;
		
		public EdgeData() {
			Id=0;
			
			Duration=new TimeSpan();
		}
		
		public void GenId() { Id=++lastId; }
		
		public object Clone() {
			EdgeData edgeData=new EdgeData();
			edgeData.Id=Id;
			
			edgeData.Duration=Duration;
			
			return edgeData;
		}
	}
	
	public class Edge {
		public EdgeData edgeData;
		
		public Vertex v1;
		public Vertex v2;
		
		public Edge() {
			edgeData=new EdgeData();
			
			v1=null;
			v2=null;
		}
		public Edge(EdgeData edgeData) {
			this.edgeData=edgeData;
			
			v1=null;
			v2=null;
		}
		
		public object Clone() {
			Edge edge=new Edge();
			edge.edgeData=(EdgeData)edgeData.Clone();
			
			edge.v1=v1;
			edge.v2=v2;
			
			return edge;
		}
	}
}
