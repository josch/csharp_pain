/*
 * User: aputze
 * Date: 28.10.2004
 * Time: 15:48
 */

using System;
using System.Collections;

using System.Data;

namespace ViwoTour {
	public class Graph {
		public ArrayList V;
		public ArrayList E;
		
		public Hashtable V_By_DepartureStation;
		public Hashtable V_By_ArrivalStation;
		
		public Hashtable E_By_v1;
		public Hashtable E_By_v2;
		
		public Graph() {
			V=new ArrayList();
			E=new ArrayList();
			
			V_By_DepartureStation=new Hashtable();
			V_By_ArrivalStation=new Hashtable();
			
			E_By_v1=new Hashtable();
			E_By_v2=new Hashtable();
		}
		
		public void Load(CoursePlan coursePlan) {
			foreach(DataRow row in coursePlan.Rows) {
				VertexData vertexData=new VertexData();
				vertexData.Id=(int)row["ID"];
				vertexData.Departure=(DateTime)row["DEPARTURE"];
				vertexData.DepartureStation=(string)row["DEPARTURE_STATION"];
				vertexData.Arrival=(DateTime)row["ARRIVAL"];
				vertexData.ArrivalStation=(string)row["ARRIVAL_STATION"];
				vertexData.Workforce=(int)row["WORKFORCE"];
				
				Vertex v=new Vertex(vertexData);
				V.Add(v);
			}
			
			foreach(Vertex v1 in V) {
				foreach(Vertex v2 in V) {
					if(v1.vertexData.ArrivalStation==v2.vertexData.DepartureStation &&
					   (v2.vertexData.Departure-v1.vertexData.Arrival)>=Properties.Edge.MinimalDuration &&
					   (v2.vertexData.Departure-v1.vertexData.Arrival)<=Properties.Edge.MaximalDuration) {
						EdgeData edgeData=new EdgeData();
						edgeData.Duration=v2.vertexData.Departure-v1.vertexData.Arrival;
						
						Edge e=new Edge(edgeData);
						e.v1=v1;
						e.v2=v2;
						E.Add(e);
						
						v1.outboundDegree++;
						v2.inboundDegree++;
					}
				}
			}
			
			foreach(Vertex v in V) {
				if(!V_By_DepartureStation.Contains(v.vertexData.DepartureStation)) { V_By_DepartureStation.Add(v.vertexData.DepartureStation,new ArrayList()); }			
				((ArrayList)V_By_DepartureStation[v.vertexData.DepartureStation]).Add(v);
				
				
				if(!V_By_ArrivalStation.Contains(v.vertexData.ArrivalStation)) { V_By_ArrivalStation.Add(v.vertexData.ArrivalStation,new ArrayList()); }			
				((ArrayList)V_By_ArrivalStation[v.vertexData.ArrivalStation]).Add(v);
			}
			
			foreach(Edge e in E) {
				if(!E_By_v1.Contains(e.v1)) { E_By_v1.Add(e.v1,new ArrayList()); }
				((ArrayList)E_By_v1[e.v1]).Add(e);
				
				if(!E_By_v2.Contains(e.v2)) { E_By_v2.Add(e.v2,new ArrayList()); }
				((ArrayList)E_By_v2[e.v2]).Add(e);
			}
		}
	}
}
