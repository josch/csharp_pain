/*
 * Created by SharpDevelop.
 * User: Johannes Schauer
 * Date: 08.11.2004
 * Time: 21:13
 * 
 */
using System;
using System.Collections;
using System.Data;
using System.Windows.Forms;

using System.Threading;


namespace ViwoTour
{
	public class ViwoTourMain : System.Windows.Forms.Form
	{
		private System.Windows.Forms.TextBox scopeTextBox;
		private System.Windows.Forms.Label Panel4Label3;
		private System.Windows.Forms.ProgressBar ProgressTProgressBar;
		private System.Windows.Forms.GroupBox Panel4GroupBox1;
		private System.Windows.Forms.Label Panel5Label0;
		private System.Windows.Forms.Label Panel5Label1;
		private System.Windows.Forms.Label Panel5Label3;
		private System.Windows.Forms.Label Panel5Label4;
		private System.Windows.Forms.Label Panel5Label5;
		private System.Windows.Forms.Label Panel5Label7;
		private System.Windows.Forms.Label Panel5Label8;
		private System.Windows.Forms.TextBox Panel2TextBox1;
		private System.Windows.Forms.Button NextButton;
		private System.Windows.Forms.Button Panel1StandardButton;
		private System.Windows.Forms.TextBox DataSourceTextBox;
		private System.Windows.Forms.TextBox InitialCatalogTextBox;
		private System.Windows.Forms.Label Panel0Label1;
		private System.Windows.Forms.GroupBox Panel4GroupBox2;
		private System.Windows.Forms.Label Panel0Label2;
		private System.Windows.Forms.Panel Panel3;
		private System.Windows.Forms.Panel Panel2;
		private System.Windows.Forms.Panel Panel1;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label Panel1Label1;
		private System.Windows.Forms.Panel Panel5;
		private System.Windows.Forms.Panel Panel4;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox maxTouringTextBox;
		private System.Windows.Forms.Label Panel2Label1;
		private System.Windows.Forms.Label Panel2Label0;
		private System.Windows.Forms.Label Panel3Label0;
		private System.Windows.Forms.Button AbortButton;
		private System.Windows.Forms.Button Panel4StandardButton;
		private System.Windows.Forms.Button Panel5SelectButton;
		private System.Windows.Forms.Button Panel5SearchButton;
		private System.Windows.Forms.Label Panel4Label1;
		private System.Windows.Forms.GroupBox Panel1GroupBox1;
		private System.Windows.Forms.Label Panel1Label4;
		private System.Windows.Forms.Label Panel4Label2;
		private System.Windows.Forms.GroupBox LineBottom;
		private System.Windows.Forms.Label Panel4Label4;
		private System.Windows.Forms.Label Panel1Label0;
		private System.Windows.Forms.TextBox minWaitingTextBox;
		private System.Windows.Forms.Label Panel1Label2;
		private System.Windows.Forms.Label Panel1Label3;
		private System.Windows.Forms.ProgressBar ProgressSProgressBar;
		private System.Windows.Forms.PictureBox pictureBox1;
		private System.Windows.Forms.Label Panel4Label5;
		private System.Windows.Forms.Button BackButton;
		private System.Windows.Forms.Label Panel6Label1;
		private System.Windows.Forms.Label Panel6Label0;
		private System.Windows.Forms.TextBox maxWaitingTextBox;
		private System.Windows.Forms.Label Panel4Label0;
		private System.Windows.Forms.Label Panel3Label1;
		private System.Windows.Forms.Panel Panel0;
		private System.Windows.Forms.Panel Panel6;
		private System.Windows.Forms.ListBox StationsListBox;
		private System.Windows.Forms.TextBox Panel6TextBox1;
		private System.Windows.Forms.Button Panel2MakeButton;
		
		protected ViwoTour viwoTour;
						
		int ActualStep = 0;
		
		public ViwoTourMain()
		{
			viwoTour=new ViwoTour();
			
			InitializeComponent();
			viwoTour.search_TProgress+=new ProgressHandler(Search_TProgress);
			viwoTour.select_TProgress+=new ProgressHandler(Select_TProgress);
			
			minWaitingTextBox.Text=Properties.MinWaiting.ToString();
			maxWaitingTextBox.Text=Properties.MaxWaiting.ToString();
			maxTouringTextBox.Text=Properties.MaxTouring.ToString();
			scopeTextBox.Text=Properties.Scope.ToString();
			
		}
		
		[STAThread]
		public static void Main(string[] args)
		{
			Application.Run(new ViwoTourMain());
		}
		
		#region Windows Forms Designer generated code
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent() {
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(ViwoTourMain));
			this.Panel2MakeButton = new System.Windows.Forms.Button();
			this.Panel6TextBox1 = new System.Windows.Forms.TextBox();
			this.StationsListBox = new System.Windows.Forms.ListBox();
			this.Panel6 = new System.Windows.Forms.Panel();
			this.Panel0 = new System.Windows.Forms.Panel();
			this.Panel3Label1 = new System.Windows.Forms.Label();
			this.Panel4Label0 = new System.Windows.Forms.Label();
			this.maxWaitingTextBox = new System.Windows.Forms.TextBox();
			this.Panel6Label0 = new System.Windows.Forms.Label();
			this.Panel6Label1 = new System.Windows.Forms.Label();
			this.BackButton = new System.Windows.Forms.Button();
			this.Panel4Label5 = new System.Windows.Forms.Label();
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.ProgressSProgressBar = new System.Windows.Forms.ProgressBar();
			this.Panel1Label3 = new System.Windows.Forms.Label();
			this.Panel1Label2 = new System.Windows.Forms.Label();
			this.minWaitingTextBox = new System.Windows.Forms.TextBox();
			this.Panel1Label0 = new System.Windows.Forms.Label();
			this.Panel4Label4 = new System.Windows.Forms.Label();
			this.LineBottom = new System.Windows.Forms.GroupBox();
			this.Panel4Label2 = new System.Windows.Forms.Label();
			this.Panel1Label4 = new System.Windows.Forms.Label();
			this.Panel1GroupBox1 = new System.Windows.Forms.GroupBox();
			this.Panel4Label1 = new System.Windows.Forms.Label();
			this.Panel5SearchButton = new System.Windows.Forms.Button();
			this.Panel5SelectButton = new System.Windows.Forms.Button();
			this.Panel4StandardButton = new System.Windows.Forms.Button();
			this.AbortButton = new System.Windows.Forms.Button();
			this.Panel3Label0 = new System.Windows.Forms.Label();
			this.Panel2Label0 = new System.Windows.Forms.Label();
			this.Panel2Label1 = new System.Windows.Forms.Label();
			this.maxTouringTextBox = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.Panel4 = new System.Windows.Forms.Panel();
			this.Panel5 = new System.Windows.Forms.Panel();
			this.Panel1Label1 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.Panel1 = new System.Windows.Forms.Panel();
			this.Panel2 = new System.Windows.Forms.Panel();
			this.Panel3 = new System.Windows.Forms.Panel();
			this.Panel0Label2 = new System.Windows.Forms.Label();
			this.Panel4GroupBox2 = new System.Windows.Forms.GroupBox();
			this.Panel0Label1 = new System.Windows.Forms.Label();
			this.InitialCatalogTextBox = new System.Windows.Forms.TextBox();
			this.DataSourceTextBox = new System.Windows.Forms.TextBox();
			this.Panel1StandardButton = new System.Windows.Forms.Button();
			this.NextButton = new System.Windows.Forms.Button();
			this.Panel2TextBox1 = new System.Windows.Forms.TextBox();
			this.Panel5Label8 = new System.Windows.Forms.Label();
			this.Panel5Label7 = new System.Windows.Forms.Label();
			this.Panel5Label5 = new System.Windows.Forms.Label();
			this.Panel5Label4 = new System.Windows.Forms.Label();
			this.Panel5Label3 = new System.Windows.Forms.Label();
			this.Panel5Label1 = new System.Windows.Forms.Label();
			this.Panel5Label0 = new System.Windows.Forms.Label();
			this.Panel4GroupBox1 = new System.Windows.Forms.GroupBox();
			this.ProgressTProgressBar = new System.Windows.Forms.ProgressBar();
			this.Panel4Label3 = new System.Windows.Forms.Label();
			this.scopeTextBox = new System.Windows.Forms.TextBox();
			this.Panel6.SuspendLayout();
			this.Panel0.SuspendLayout();
			this.Panel1GroupBox1.SuspendLayout();
			this.Panel4.SuspendLayout();
			this.Panel5.SuspendLayout();
			this.Panel1.SuspendLayout();
			this.Panel2.SuspendLayout();
			this.Panel3.SuspendLayout();
			this.Panel4GroupBox2.SuspendLayout();
			this.Panel4GroupBox1.SuspendLayout();
			this.SuspendLayout();
			// 
			// Panel2MakeButton
			// 
			this.Panel2MakeButton.Location = new System.Drawing.Point(16, 96);
			this.Panel2MakeButton.Name = "Panel2MakeButton";
			this.Panel2MakeButton.Size = new System.Drawing.Size(128, 24);
			this.Panel2MakeButton.TabIndex = 4;
			this.Panel2MakeButton.Text = "Tabellen erstellen";
			this.Panel2MakeButton.Click += new System.EventHandler(this.Panel2MakeButtonClick);
			// 
			// Panel6TextBox1
			// 
			this.Panel6TextBox1.Location = new System.Drawing.Point(16, 80);
			this.Panel6TextBox1.Multiline = true;
			this.Panel6TextBox1.Name = "Panel6TextBox1";
			this.Panel6TextBox1.Size = new System.Drawing.Size(296, 216);
			this.Panel6TextBox1.TabIndex = 2;
			this.Panel6TextBox1.Text = "";
			// 
			// StationsListBox
			// 
			this.StationsListBox.Location = new System.Drawing.Point(16, 80);
			this.StationsListBox.Name = "StationsListBox";
			this.StationsListBox.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
			this.StationsListBox.Size = new System.Drawing.Size(296, 212);
			this.StationsListBox.Sorted = true;
			this.StationsListBox.TabIndex = 0;
			// 
			// Panel6
			// 
			this.Panel6.Controls.Add(this.Panel6TextBox1);
			this.Panel6.Controls.Add(this.Panel6Label1);
			this.Panel6.Controls.Add(this.Panel6Label0);
			this.Panel6.Location = new System.Drawing.Point(168, 8);
			this.Panel6.Name = "Panel6";
			this.Panel6.Size = new System.Drawing.Size(320, 304);
			this.Panel6.TabIndex = 12;
			this.Panel6.Visible = false;
			// 
			// Panel0
			// 
			this.Panel0.BackColor = System.Drawing.SystemColors.Control;
			this.Panel0.Controls.Add(this.Panel0Label2);
			this.Panel0.Controls.Add(this.Panel0Label1);
			this.Panel0.Location = new System.Drawing.Point(168, 8);
			this.Panel0.Name = "Panel0";
			this.Panel0.Size = new System.Drawing.Size(320, 304);
			this.Panel0.TabIndex = 1;
			// 
			// Panel3Label1
			// 
			this.Panel3Label1.Location = new System.Drawing.Point(16, 48);
			this.Panel3Label1.Name = "Panel3Label1";
			this.Panel3Label1.Size = new System.Drawing.Size(288, 16);
			this.Panel3Label1.TabIndex = 2;
			this.Panel3Label1.Text = "Wählen Sie die gewünschten Stationen aus.";
			// 
			// Panel4Label0
			// 
			this.Panel4Label0.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.Panel4Label0.Location = new System.Drawing.Point(16, 16);
			this.Panel4Label0.Name = "Panel4Label0";
			this.Panel4Label0.Size = new System.Drawing.Size(224, 16);
			this.Panel4Label0.TabIndex = 0;
			this.Panel4Label0.Text = "Schritt Vier";
			// 
			// maxWaitingTextBox
			// 
			this.maxWaitingTextBox.Location = new System.Drawing.Point(88, 48);
			this.maxWaitingTextBox.Name = "maxWaitingTextBox";
			this.maxWaitingTextBox.Size = new System.Drawing.Size(192, 20);
			this.maxWaitingTextBox.TabIndex = 1;
			this.maxWaitingTextBox.Text = "01:00:00";
			// 
			// Panel6Label0
			// 
			this.Panel6Label0.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.Panel6Label0.Location = new System.Drawing.Point(16, 16);
			this.Panel6Label0.Name = "Panel6Label0";
			this.Panel6Label0.Size = new System.Drawing.Size(152, 16);
			this.Panel6Label0.TabIndex = 0;
			this.Panel6Label0.Text = "Schritt Sechs";
			// 
			// Panel6Label1
			// 
			this.Panel6Label1.Location = new System.Drawing.Point(16, 48);
			this.Panel6Label1.Name = "Panel6Label1";
			this.Panel6Label1.Size = new System.Drawing.Size(232, 16);
			this.Panel6Label1.TabIndex = 1;
			this.Panel6Label1.Text = "Hier die Auswertung:";
			// 
			// BackButton
			// 
			this.BackButton.Enabled = false;
			this.BackButton.Location = new System.Drawing.Point(232, 320);
			this.BackButton.Name = "BackButton";
			this.BackButton.Size = new System.Drawing.Size(80, 24);
			this.BackButton.TabIndex = 2;
			this.BackButton.Text = "< Zurück";
			this.BackButton.Click += new System.EventHandler(this.BackButtonClick);
			// 
			// Panel4Label5
			// 
			this.Panel4Label5.Location = new System.Drawing.Point(24, 232);
			this.Panel4Label5.Name = "Panel4Label5";
			this.Panel4Label5.Size = new System.Drawing.Size(72, 16);
			this.Panel4Label5.TabIndex = 5;
			this.Panel4Label5.Text = "Sichtweite:";
			this.Panel4Label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// pictureBox1
			// 
			this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
			this.pictureBox1.Location = new System.Drawing.Point(0, 0);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(164, 314);
			this.pictureBox1.TabIndex = 8;
			this.pictureBox1.TabStop = false;
			// 
			// ProgressSProgressBar
			// 
			this.ProgressSProgressBar.Location = new System.Drawing.Point(16, 128);
			this.ProgressSProgressBar.Name = "ProgressSProgressBar";
			this.ProgressSProgressBar.Size = new System.Drawing.Size(296, 16);
			this.ProgressSProgressBar.TabIndex = 4;
			// 
			// Panel1Label3
			// 
			this.Panel1Label3.Location = new System.Drawing.Point(16, 72);
			this.Panel1Label3.Name = "Panel1Label3";
			this.Panel1Label3.Size = new System.Drawing.Size(64, 16);
			this.Panel1Label3.TabIndex = 3;
			this.Panel1Label3.Text = "Host:";
			this.Panel1Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// Panel1Label2
			// 
			this.Panel1Label2.Location = new System.Drawing.Point(16, 24);
			this.Panel1Label2.Name = "Panel1Label2";
			this.Panel1Label2.Size = new System.Drawing.Size(272, 48);
			this.Panel1Label2.TabIndex = 2;
			this.Panel1Label2.Text = "Geben Sie den Namen oder die IP des Datenbank-Rechners ein (z.B.: localhost) und " +
"den Namen der Datenbank.";
			// 
			// minWaitingTextBox
			// 
			this.minWaitingTextBox.Location = new System.Drawing.Point(88, 16);
			this.minWaitingTextBox.Name = "minWaitingTextBox";
			this.minWaitingTextBox.Size = new System.Drawing.Size(192, 20);
			this.minWaitingTextBox.TabIndex = 0;
			this.minWaitingTextBox.Text = "00:05:00";
			// 
			// Panel1Label0
			// 
			this.Panel1Label0.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.Panel1Label0.Location = new System.Drawing.Point(16, 16);
			this.Panel1Label0.Name = "Panel1Label0";
			this.Panel1Label0.Size = new System.Drawing.Size(100, 16);
			this.Panel1Label0.TabIndex = 4;
			this.Panel1Label0.Text = "Schritt Eins";
			// 
			// Panel4Label4
			// 
			this.Panel4Label4.Location = new System.Drawing.Point(8, 17);
			this.Panel4Label4.Name = "Panel4Label4";
			this.Panel4Label4.Size = new System.Drawing.Size(72, 16);
			this.Panel4Label4.TabIndex = 1;
			this.Panel4Label4.Text = "Maximum:";
			this.Panel4Label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// LineBottom
			// 
			this.LineBottom.Location = new System.Drawing.Point(0, 314);
			this.LineBottom.Name = "LineBottom";
			this.LineBottom.Size = new System.Drawing.Size(498, 3);
			this.LineBottom.TabIndex = 10;
			this.LineBottom.TabStop = false;
			// 
			// Panel4Label2
			// 
			this.Panel4Label2.Location = new System.Drawing.Point(16, 16);
			this.Panel4Label2.Name = "Panel4Label2";
			this.Panel4Label2.Size = new System.Drawing.Size(64, 16);
			this.Panel4Label2.TabIndex = 2;
			this.Panel4Label2.Text = "Minimum:";
			this.Panel4Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// Panel1Label4
			// 
			this.Panel1Label4.Location = new System.Drawing.Point(16, 104);
			this.Panel1Label4.Name = "Panel1Label4";
			this.Panel1Label4.Size = new System.Drawing.Size(64, 16);
			this.Panel1Label4.TabIndex = 5;
			this.Panel1Label4.Text = "Datenbank:";
			this.Panel1Label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// Panel1GroupBox1
			// 
			this.Panel1GroupBox1.Controls.Add(this.Panel1StandardButton);
			this.Panel1GroupBox1.Controls.Add(this.DataSourceTextBox);
			this.Panel1GroupBox1.Controls.Add(this.InitialCatalogTextBox);
			this.Panel1GroupBox1.Controls.Add(this.Panel1Label2);
			this.Panel1GroupBox1.Controls.Add(this.Panel1Label3);
			this.Panel1GroupBox1.Controls.Add(this.Panel1Label4);
			this.Panel1GroupBox1.Location = new System.Drawing.Point(16, 96);
			this.Panel1GroupBox1.Name = "Panel1GroupBox1";
			this.Panel1GroupBox1.Size = new System.Drawing.Size(296, 176);
			this.Panel1GroupBox1.TabIndex = 3;
			this.Panel1GroupBox1.TabStop = false;
			this.Panel1GroupBox1.Text = "Datenquelle";
			// 
			// Panel4Label1
			// 
			this.Panel4Label1.Location = new System.Drawing.Point(16, 48);
			this.Panel4Label1.Name = "Panel4Label1";
			this.Panel4Label1.Size = new System.Drawing.Size(272, 16);
			this.Panel4Label1.TabIndex = 1;
			this.Panel4Label1.Text = "Geben Sie hier Ihre gewünschten Einstellungen ein.";
			// 
			// Panel5SearchButton
			// 
			this.Panel5SearchButton.Location = new System.Drawing.Point(16, 72);
			this.Panel5SearchButton.Name = "Panel5SearchButton";
			this.Panel5SearchButton.Size = new System.Drawing.Size(120, 24);
			this.Panel5SearchButton.TabIndex = 2;
			this.Panel5SearchButton.Text = "Suchen";
			this.Panel5SearchButton.Click += new System.EventHandler(this.Panel5SearchButtonClick);
			// 
			// Panel5SelectButton
			// 
			this.Panel5SelectButton.Enabled = false;
			this.Panel5SelectButton.Location = new System.Drawing.Point(16, 192);
			this.Panel5SelectButton.Name = "Panel5SelectButton";
			this.Panel5SelectButton.Size = new System.Drawing.Size(120, 24);
			this.Panel5SelectButton.TabIndex = 6;
			this.Panel5SelectButton.Text = "Auswählen";
			this.Panel5SelectButton.Click += new System.EventHandler(this.Panel5SelectButtonClick);
			// 
			// Panel4StandardButton
			// 
			this.Panel4StandardButton.Location = new System.Drawing.Point(144, 264);
			this.Panel4StandardButton.Name = "Panel4StandardButton";
			this.Panel4StandardButton.Size = new System.Drawing.Size(168, 24);
			this.Panel4StandardButton.TabIndex = 6;
			this.Panel4StandardButton.Text = "Auf Standart zurücksetzen";
			this.Panel4StandardButton.Click += new System.EventHandler(this.Panel5StandardButtonClick);
			// 
			// AbortButton
			// 
			this.AbortButton.Location = new System.Drawing.Point(408, 320);
			this.AbortButton.Name = "AbortButton";
			this.AbortButton.Size = new System.Drawing.Size(80, 24);
			this.AbortButton.TabIndex = 3;
			this.AbortButton.Text = "Abbrechen";
			this.AbortButton.Click += new System.EventHandler(this.AbortButtonClick);
			// 
			// Panel3Label0
			// 
			this.Panel3Label0.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.Panel3Label0.Location = new System.Drawing.Point(16, 16);
			this.Panel3Label0.Name = "Panel3Label0";
			this.Panel3Label0.Size = new System.Drawing.Size(184, 16);
			this.Panel3Label0.TabIndex = 1;
			this.Panel3Label0.Text = "Schritt Drei";
			// 
			// Panel2Label0
			// 
			this.Panel2Label0.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.Panel2Label0.Location = new System.Drawing.Point(16, 16);
			this.Panel2Label0.Name = "Panel2Label0";
			this.Panel2Label0.Size = new System.Drawing.Size(184, 16);
			this.Panel2Label0.TabIndex = 3;
			this.Panel2Label0.Text = "Schritt Zwei";
			// 
			// Panel2Label1
			// 
			this.Panel2Label1.Location = new System.Drawing.Point(16, 48);
			this.Panel2Label1.Name = "Panel2Label1";
			this.Panel2Label1.Size = new System.Drawing.Size(288, 40);
			this.Panel2Label1.TabIndex = 0;
			this.Panel2Label1.Text = "Klicken Sie auf Tabellen erstellen um die Verbindung mit der Datenbank herzustell" +
"en und die Tabellen vorzubereiten.";
			// 
			// maxTouringTextBox
			// 
			this.maxTouringTextBox.Location = new System.Drawing.Point(88, 16);
			this.maxTouringTextBox.Name = "maxTouringTextBox";
			this.maxTouringTextBox.Size = new System.Drawing.Size(192, 20);
			this.maxTouringTextBox.TabIndex = 0;
			this.maxTouringTextBox.Text = "10:00:00";
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(48, 24);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(128, 24);
			this.label4.TabIndex = 0;
			this.label4.Text = "label4";
			// 
			// Panel4
			// 
			this.Panel4.Controls.Add(this.scopeTextBox);
			this.Panel4.Controls.Add(this.Panel4StandardButton);
			this.Panel4.Controls.Add(this.Panel4Label5);
			this.Panel4.Controls.Add(this.Panel4GroupBox2);
			this.Panel4.Controls.Add(this.Panel4GroupBox1);
			this.Panel4.Controls.Add(this.Panel4Label1);
			this.Panel4.Controls.Add(this.Panel4Label0);
			this.Panel4.Location = new System.Drawing.Point(168, 8);
			this.Panel4.Name = "Panel4";
			this.Panel4.Size = new System.Drawing.Size(320, 304);
			this.Panel4.TabIndex = 9;
			this.Panel4.Visible = false;
			// 
			// Panel5
			// 
			this.Panel5.Controls.Add(this.Panel5Label8);
			this.Panel5.Controls.Add(this.Panel5Label7);
			this.Panel5.Controls.Add(this.Panel5Label5);
			this.Panel5.Controls.Add(this.Panel5Label4);
			this.Panel5.Controls.Add(this.Panel5Label3);
			this.Panel5.Controls.Add(this.Panel5SelectButton);
			this.Panel5.Controls.Add(this.ProgressTProgressBar);
			this.Panel5.Controls.Add(this.ProgressSProgressBar);
			this.Panel5.Controls.Add(this.Panel5SearchButton);
			this.Panel5.Controls.Add(this.Panel5Label1);
			this.Panel5.Controls.Add(this.Panel5Label0);
			this.Panel5.Location = new System.Drawing.Point(168, 8);
			this.Panel5.Name = "Panel5";
			this.Panel5.Size = new System.Drawing.Size(320, 304);
			this.Panel5.TabIndex = 11;
			this.Panel5.Visible = false;
			// 
			// Panel1Label1
			// 
			this.Panel1Label1.Location = new System.Drawing.Point(16, 48);
			this.Panel1Label1.Name = "Panel1Label1";
			this.Panel1Label1.Size = new System.Drawing.Size(296, 32);
			this.Panel1Label1.TabIndex = 0;
			this.Panel1Label1.Text = "Geben Sie die Daten für die Verbindung zur entsprechenden SQL Datenbank ein.";
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(0, 0);
			this.label3.Name = "label3";
			this.label3.TabIndex = 0;
			// 
			// Panel1
			// 
			this.Panel1.Controls.Add(this.Panel1Label0);
			this.Panel1.Controls.Add(this.Panel1GroupBox1);
			this.Panel1.Controls.Add(this.Panel1Label1);
			this.Panel1.Location = new System.Drawing.Point(168, 8);
			this.Panel1.Name = "Panel1";
			this.Panel1.Size = new System.Drawing.Size(320, 304);
			this.Panel1.TabIndex = 4;
			this.Panel1.Visible = false;
			// 
			// Panel2
			// 
			this.Panel2.Controls.Add(this.Panel2MakeButton);
			this.Panel2.Controls.Add(this.Panel2Label0);
			this.Panel2.Controls.Add(this.Panel2TextBox1);
			this.Panel2.Controls.Add(this.Panel2Label1);
			this.Panel2.Location = new System.Drawing.Point(168, 8);
			this.Panel2.Name = "Panel2";
			this.Panel2.Size = new System.Drawing.Size(320, 304);
			this.Panel2.TabIndex = 6;
			this.Panel2.Visible = false;
			// 
			// Panel3
			// 
			this.Panel3.Controls.Add(this.Panel3Label1);
			this.Panel3.Controls.Add(this.Panel3Label0);
			this.Panel3.Controls.Add(this.StationsListBox);
			this.Panel3.Location = new System.Drawing.Point(168, 8);
			this.Panel3.Name = "Panel3";
			this.Panel3.Size = new System.Drawing.Size(320, 304);
			this.Panel3.TabIndex = 7;
			this.Panel3.Visible = false;
			// 
			// Panel0Label2
			// 
			this.Panel0Label2.Location = new System.Drawing.Point(16, 64);
			this.Panel0Label2.Name = "Panel0Label2";
			this.Panel0Label2.Size = new System.Drawing.Size(240, 32);
			this.Panel0Label2.TabIndex = 1;
			this.Panel0Label2.Text = "Um fortzufahren klicken Sie bitte auf Weiter.";
			// 
			// Panel4GroupBox2
			// 
			this.Panel4GroupBox2.Controls.Add(this.Panel4Label4);
			this.Panel4GroupBox2.Controls.Add(this.maxTouringTextBox);
			this.Panel4GroupBox2.Location = new System.Drawing.Point(16, 168);
			this.Panel4GroupBox2.Name = "Panel4GroupBox2";
			this.Panel4GroupBox2.Size = new System.Drawing.Size(296, 48);
			this.Panel4GroupBox2.TabIndex = 3;
			this.Panel4GroupBox2.TabStop = false;
			this.Panel4GroupBox2.Text = "Tourenlänge";
			// 
			// Panel0Label1
			// 
			this.Panel0Label1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.Panel0Label1.Location = new System.Drawing.Point(16, 16);
			this.Panel0Label1.Name = "Panel0Label1";
			this.Panel0Label1.Size = new System.Drawing.Size(296, 24);
			this.Panel0Label1.TabIndex = 0;
			this.Panel0Label1.Text = "Willkommen bei ViwoTour";
			// 
			// InitialCatalogTextBox
			// 
			this.InitialCatalogTextBox.Location = new System.Drawing.Point(88, 104);
			this.InitialCatalogTextBox.Name = "InitialCatalogTextBox";
			this.InitialCatalogTextBox.Size = new System.Drawing.Size(192, 20);
			this.InitialCatalogTextBox.TabIndex = 4;
			this.InitialCatalogTextBox.Text = "viwotour";
			// 
			// DataSourceTextBox
			// 
			this.DataSourceTextBox.Location = new System.Drawing.Point(88, 72);
			this.DataSourceTextBox.Name = "DataSourceTextBox";
			this.DataSourceTextBox.Size = new System.Drawing.Size(192, 20);
			this.DataSourceTextBox.TabIndex = 0;
			this.DataSourceTextBox.Text = "localhost";
			// 
			// Panel1StandardButton
			// 
			this.Panel1StandardButton.Location = new System.Drawing.Point(64, 136);
			this.Panel1StandardButton.Name = "Panel1StandardButton";
			this.Panel1StandardButton.Size = new System.Drawing.Size(168, 24);
			this.Panel1StandardButton.TabIndex = 6;
			this.Panel1StandardButton.Text = "Auf Standart zurücksetzen";
			this.Panel1StandardButton.Click += new System.EventHandler(this.Panel1StandardButtonClick);
			// 
			// NextButton
			// 
			this.NextButton.Location = new System.Drawing.Point(312, 320);
			this.NextButton.Name = "NextButton";
			this.NextButton.Size = new System.Drawing.Size(80, 24);
			this.NextButton.TabIndex = 1;
			this.NextButton.Text = "Weiter >";
			this.NextButton.Click += new System.EventHandler(this.NextButtonClick);
			// 
			// Panel2TextBox1
			// 
			this.Panel2TextBox1.Location = new System.Drawing.Point(16, 136);
			this.Panel2TextBox1.Multiline = true;
			this.Panel2TextBox1.Name = "Panel2TextBox1";
			this.Panel2TextBox1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.Panel2TextBox1.Size = new System.Drawing.Size(296, 152);
			this.Panel2TextBox1.TabIndex = 1;
			this.Panel2TextBox1.Text = "";
			// 
			// Panel5Label8
			// 
			this.Panel5Label8.Location = new System.Drawing.Point(264, 224);
			this.Panel5Label8.Name = "Panel5Label8";
			this.Panel5Label8.Size = new System.Drawing.Size(48, 16);
			this.Panel5Label8.TabIndex = 13;
			// 
			// Panel5Label7
			// 
			this.Panel5Label7.Location = new System.Drawing.Point(264, 104);
			this.Panel5Label7.Name = "Panel5Label7";
			this.Panel5Label7.Size = new System.Drawing.Size(48, 16);
			this.Panel5Label7.TabIndex = 12;
			// 
			// Panel5Label5
			// 
			this.Panel5Label5.Location = new System.Drawing.Point(16, 224);
			this.Panel5Label5.Name = "Panel5Label5";
			this.Panel5Label5.Size = new System.Drawing.Size(128, 16);
			this.Panel5Label5.TabIndex = 10;
			this.Panel5Label5.Text = "Auswählen:";
			// 
			// Panel5Label4
			// 
			this.Panel5Label4.Location = new System.Drawing.Point(16, 168);
			this.Panel5Label4.Name = "Panel5Label4";
			this.Panel5Label4.Size = new System.Drawing.Size(144, 16);
			this.Panel5Label4.TabIndex = 9;
			this.Panel5Label4.Text = "Klicken Sie auf Auswählen.";
			// 
			// Panel5Label3
			// 
			this.Panel5Label3.Location = new System.Drawing.Point(16, 104);
			this.Panel5Label3.Name = "Panel5Label3";
			this.Panel5Label3.Size = new System.Drawing.Size(104, 16);
			this.Panel5Label3.TabIndex = 8;
			this.Panel5Label3.Text = "Suchen:";
			// 
			// Panel5Label1
			// 
			this.Panel5Label1.Location = new System.Drawing.Point(16, 48);
			this.Panel5Label1.Name = "Panel5Label1";
			this.Panel5Label1.Size = new System.Drawing.Size(248, 16);
			this.Panel5Label1.TabIndex = 1;
			this.Panel5Label1.Text = "Klicken Sie auf Suchen.";
			// 
			// Panel5Label0
			// 
			this.Panel5Label0.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.Panel5Label0.Location = new System.Drawing.Point(16, 16);
			this.Panel5Label0.Name = "Panel5Label0";
			this.Panel5Label0.Size = new System.Drawing.Size(192, 16);
			this.Panel5Label0.TabIndex = 0;
			this.Panel5Label0.Text = "Schritt Fünf";
			// 
			// Panel4GroupBox1
			// 
			this.Panel4GroupBox1.Controls.Add(this.Panel4Label3);
			this.Panel4GroupBox1.Controls.Add(this.Panel4Label2);
			this.Panel4GroupBox1.Controls.Add(this.maxWaitingTextBox);
			this.Panel4GroupBox1.Controls.Add(this.minWaitingTextBox);
			this.Panel4GroupBox1.Location = new System.Drawing.Point(16, 72);
			this.Panel4GroupBox1.Name = "Panel4GroupBox1";
			this.Panel4GroupBox1.Size = new System.Drawing.Size(296, 80);
			this.Panel4GroupBox1.TabIndex = 2;
			this.Panel4GroupBox1.TabStop = false;
			this.Panel4GroupBox1.Text = "Wartezeit";
			// 
			// ProgressTProgressBar
			// 
			this.ProgressTProgressBar.Location = new System.Drawing.Point(16, 248);
			this.ProgressTProgressBar.Name = "ProgressTProgressBar";
			this.ProgressTProgressBar.Size = new System.Drawing.Size(296, 16);
			this.ProgressTProgressBar.TabIndex = 5;
			// 
			// Panel4Label3
			// 
			this.Panel4Label3.Location = new System.Drawing.Point(16, 48);
			this.Panel4Label3.Name = "Panel4Label3";
			this.Panel4Label3.Size = new System.Drawing.Size(64, 16);
			this.Panel4Label3.TabIndex = 3;
			this.Panel4Label3.Text = "Maximum:";
			this.Panel4Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// scopeTextBox
			// 
			this.scopeTextBox.Location = new System.Drawing.Point(104, 232);
			this.scopeTextBox.Name = "scopeTextBox";
			this.scopeTextBox.Size = new System.Drawing.Size(192, 20);
			this.scopeTextBox.TabIndex = 7;
			this.scopeTextBox.Text = "";
			// 
			// ViwoTourMain
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(496, 351);
			this.Controls.Add(this.Panel3);
			this.Controls.Add(this.Panel4);
			this.Controls.Add(this.Panel6);
			this.Controls.Add(this.Panel2);
			this.Controls.Add(this.Panel5);
			this.Controls.Add(this.Panel1);
			this.Controls.Add(this.LineBottom);
			this.Controls.Add(this.pictureBox1);
			this.Controls.Add(this.BackButton);
			this.Controls.Add(this.NextButton);
			this.Controls.Add(this.AbortButton);
			this.Controls.Add(this.Panel0);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.MaximizeBox = false;
			this.Name = "ViwoTourMain";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "ViwoTour";
			this.Panel6.ResumeLayout(false);
			this.Panel0.ResumeLayout(false);
			this.Panel1GroupBox1.ResumeLayout(false);
			this.Panel4.ResumeLayout(false);
			this.Panel5.ResumeLayout(false);
			this.Panel1.ResumeLayout(false);
			this.Panel2.ResumeLayout(false);
			this.Panel3.ResumeLayout(false);
			this.Panel4GroupBox2.ResumeLayout(false);
			this.Panel4GroupBox1.ResumeLayout(false);
			this.ResumeLayout(false);
		}
		#endregion


		
		void AbortButtonClick(object sender, System.EventArgs e)
		{
			this.Close();
		}
		
		void NextButtonClick(object sender, System.EventArgs e)
		{
			switch(ActualStep)
			{
				case 0:
					{
						Panel0.Visible = false;
						Panel1.Visible = true;
						BackButton.Enabled = true;
						ActualStep++;
						break;
					}
				case 1:
					{
						Panel1.Visible = false;
						Panel2.Visible = true;
						NextButton.Enabled = false;
						ActualStep++;
						break;
					}
				case 2:
					{
						Panel2.Visible = false;
						Panel3.Visible = true;
						ActualStep++;
						break;
					}
				case 3:
					{
						Panel3.Visible = false;
						Panel4.Visible = true;
						ActualStep++;
						break;
					}
				case 4:
					{
						Panel4.Visible = false;
						Panel5.Visible = true;
						ActualStep++;
						break;
					}
				case 5:
					{
						Panel5.Visible = false;
						Panel6.Visible = true;
						NextButton.Enabled = false;
						ActualStep++;
						break;
					}
			}
		}

		
		void BackButtonClick(object sender, System.EventArgs e)
		{
			switch(ActualStep)
			{
				case 1:
					{
						Panel1.Visible = false;
						Panel0.Visible = true;
						BackButton.Enabled = false;
						ActualStep--;
						break;
					}
				case 2:
					{
						Panel2.Visible = false;
						Panel1.Visible = true;
						NextButton.Enabled = true;
						ActualStep--;
						break;
					}
				case 3:
					{
						Panel3.Visible = false;
						Panel2.Visible = true;
						NextButton.Enabled = false;
						ActualStep--;
						break;
					}
				case 4:
					{
						Panel4.Visible = false;
						Panel3.Visible = true;
						ActualStep--;
						break;
					}
				case 5:
					{
						Panel5.Visible = false;
						Panel4.Visible = true;
						ActualStep--;
						break;
					}
				case 6:
					{
						Panel6.Visible = false;
						Panel5.Visible = true;
						NextButton.Enabled = true;
						ActualStep--;
						break;
					}
			}
		}
		
		void Panel1StandardButtonClick(object sender, System.EventArgs e)
		{
			DataSourceTextBox.Text = "localhost";
			InitialCatalogTextBox.Text = "viwotour";
		}
		
		void Panel2MakeButtonClick(object sender, System.EventArgs e)
		{
			BackButton.Enabled = false;
			NextButton.Enabled = false;
			Panel2MakeButton.Enabled = false;
			new Thread(new ThreadStart(PrepareClick)).Start();
		}
		
		void Panel5StandardButtonClick(object sender, System.EventArgs e)
		{
			minWaitingTextBox.Text=Properties.MinWaiting.ToString();
			maxWaitingTextBox.Text=Properties.MaxWaiting.ToString();
			maxTouringTextBox.Text=Properties.MaxTouring.ToString();
			scopeTextBox.Text=Properties.Scope.ToString();
		}
		
		void Panel5SearchButtonClick(object sender, System.EventArgs e)
		{
			BackButton.Enabled = false;
			Panel5SearchButton.Enabled = false;
			
			viwoTour.S.Clear();
			foreach(string station in StationsListBox.SelectedItems) {
				viwoTour.S.Add(station);
			}
			
			new Thread(new ThreadStart(SearchStart)).Start();
			
		}
		
		void Panel5SelectButtonClick(object sender, System.EventArgs e)
		{
			Panel5SelectButton.Enabled = false;
			new Thread(new ThreadStart(SelectStart)).Start();
		}
		
		void PrepareClick()
		{
			Properties.DataSource = DataSourceTextBox.Text;
			Properties.InitialCatalog = InitialCatalogTextBox.Text;
			
			Panel2TextBox1.Text += "Verbinde mit "+Properties.InitialCatalog+" auf "+Properties.DataSource+"...";
			viwoTour.Connect();
			Panel2TextBox1.Text += " Fertig";
			
			Panel2TextBox1.Text += "\r\nVorbereiten...";
			viwoTour.Prepare();
			Panel2TextBox1.Text += " Fertig";
			
			Panel2TextBox1.Text += "\r\nLaden...";
			viwoTour.Load();
			Panel2TextBox1.Text += "Fertig\r\n";
			
			StationsListBox.Items.Clear();
			foreach(string station in viwoTour.courseBook.get_STATION_Of_Rows()) {
				StationsListBox.Items.Add(station);
			}
			
			Panel2MakeButton.Enabled = true;
			NextButton.Enabled = true;
			BackButton.Enabled = true;
		}
		
		void SearchStart()
		{
			NextButton.Enabled = false;
			viwoTour.create_A();
			viwoTour.search_T();
			
			Panel5SelectButton.Enabled = true;
		}
		
		void SelectStart()
		{
			viwoTour.select_T();
			
			FillResultTextBox();
			
			
			NextButton.Enabled = true;
			BackButton.Enabled = true;
			Panel5SearchButton.Enabled = true;
		}
		
		void Search_TProgress(int progress)
		{
			ProgressSProgressBar.Value=progress;
			Panel5Label7.Text = Convert.ToString(progress)+"%";
		}
		
		void Select_TProgress(int progress)
		{
			ProgressTProgressBar.Value=progress;
			Panel5Label8.Text = Convert.ToString(progress)+"%";
		}

		void FillResultTextBox()
		{
			ArrayList TouredIDs=new ArrayList();
			ArrayList NonTouredIDs=(ArrayList)viwoTour.courseBook.get_ID_Of_Rows().Clone();
			
			int Tours=0;
			
			int ProductiveRows=0;
			int NonProductiveRows=0;
			
			TimeSpan ProductiveTouring=new TimeSpan();
			TimeSpan NonProductiveTouring=new TimeSpan();
			
			foreach(ArrayList t_select in viwoTour.T_select) {
				Tours++;
				
				for(int i=0;i<t_select.Count;i++) {
					DataRow row=viwoTour.coursePlan.Rows[(int)t_select[i]];
					
					if(TouredIDs.Contains(row["ID"])) {
						NonProductiveRows++;
						
						NonProductiveTouring+=(DateTime)row["ARRIVAL"]-(DateTime)row["DEPARTURE"];
					} else {
						ProductiveRows++;
						
						ProductiveTouring+=(DateTime)row["ARRIVAL"]-(DateTime)row["DEPARTURE"];
					}
					
					if(!TouredIDs.Contains(row["ID"])) { TouredIDs.Add(row["ID"]); }
				}
				for(int i=1;i<t_select.Count;i++) {
					DataRow row1=viwoTour.coursePlan.Rows[(int)t_select[i-1]];
					DataRow row2=viwoTour.coursePlan.Rows[(int)t_select[i]];
					
					NonProductiveTouring+=(DateTime)row2["DEPARTURE"]-(DateTime)row1["ARRIVAL"];
				}
			}
			foreach(int ID in TouredIDs) { NonTouredIDs.Remove(ID); }
			
			Panel6TextBox1.Text += "Touren: "+Tours.ToString();
			
//			touredRowsListBox.Items.Clear();
//			foreach(int ID in TouredIDs) { touredRowsListBox.Items.Add(ID); }
//			nonTouredRowsListBox.Items.Clear();
//			foreach(int ID in NonTouredIDs) { nonTouredRowsListBox.Items.Add(ID); }
			
			Panel6TextBox1.Text += "\r\nAbgearbeitete Fahrten: "+TouredIDs.Count.ToString();
			Panel6TextBox1.Text += "\r\nNichtabgearbeitete Fahrten: "+NonTouredIDs.Count.ToString();
			
			Panel6TextBox1.Text += "\r\n\r\nProduktive Fahrten: "+ProductiveRows.ToString();
			Panel6TextBox1.Text += "\r\nUnproduktive Fahrten: "+NonProductiveRows.ToString();
			Panel6TextBox1.Text += "\r\nFahrtenproduktivität: "+(100.0*(double)ProductiveRows/(double)(ProductiveRows+NonProductiveRows)).ToString()+" %";
			
			Panel6TextBox1.Text += "\r\n\r\nProduktive Fahrzeit: "+ProductiveTouring.ToString();
			Panel6TextBox1.Text += "\r\nUnproduktive Fahrzeit: "+NonProductiveTouring.ToString();
			Panel6TextBox1.Text += "\r\nFahrzeitproduktivität: "+(100.0*(double)ProductiveTouring.Ticks/(double)(ProductiveTouring.Ticks+NonProductiveTouring.Ticks)).ToString()+" %";
		}
	}
}
