using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace TempMonitor
{
    public partial class Form1 : Form
    {
        [DllImport("asusdmi.dll")]
        public static extern Int32 GetCPUTemperature();
        [DllImport("asusdmi.dll")]
        public static extern Int32 GetMBTemperature();

        public Form1()
        {
            InitializeComponent();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            CPU.Text = "CPU Temp: " + GetCPUTemperature().ToString() + "�C";
            Bitmap bm = new Bitmap(32, 32);
            Graphics g = Graphics.FromImage(bm);
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.Default;
            g.FillRectangle(Brushes.Transparent, new Rectangle(0, 0, bm.Width, bm.Height));
            g.DrawString("55", new Font("Lucida Console", 23), Brushes.Red, -6, 0);
            this.notifyIcon1.Icon = Icon.FromHandle(bm.GetHicon());
            g.Dispose();
            bm.Dispose();
        }
    }
}

/*
 Icon oIcon = null;
       int dimension=16;
       try
       {
                Bitmap bm = new Bitmap(dimension,dimension);
                Graphics g = Graphics.FromImage((Image)bm); 
                g.SmoothingMode = SmoothingMode.AntiAlias; 
                Font oFont = new Font("Arial",8,FontStyle.Regular,GraphicsUnit.Pixel);
                g.FillRectangle(Brushes.Transparent,new Rectangle(0, 0, bm.Width, bm.Height));  
                g.FillEllipse(Brushes.Turquoise ,0,0,dimension,dimension);  
                g.DrawString(IconMessage,oFont,new SolidBrush(System.Drawing.Color.Black), 2,3);
                oIcon = Icon.FromHandle(bm.GetHicon()); 
                oFont.Dispose(); 
                g.Dispose();
                bm.Dispose();
       }
       catch (Exception e) 
       {
         Debug.WriteLine(e.Message);
       }
		 
       return oIcon;
*/