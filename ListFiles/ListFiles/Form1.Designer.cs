namespace ListFiles
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.tbOutput = new System.Windows.Forms.TextBox();
            this.openFolderButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.cbUseSubfolders = new System.Windows.Forms.CheckBox();
            this.goButton = new System.Windows.Forms.Button();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.cbShowSubfolderName = new System.Windows.Forms.CheckBox();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.Controls.Add(this.cbShowSubfolderName);
            this.panel1.Controls.Add(this.goButton);
            this.panel1.Controls.Add(this.cbUseSubfolders);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.openFolderButton);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(599, 108);
            this.panel1.TabIndex = 0;
            // 
            // tbOutput
            // 
            this.tbOutput.BackColor = System.Drawing.SystemColors.Window;
            this.tbOutput.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbOutput.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbOutput.Location = new System.Drawing.Point(0, 108);
            this.tbOutput.Multiline = true;
            this.tbOutput.Name = "tbOutput";
            this.tbOutput.ReadOnly = true;
            this.tbOutput.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbOutput.Size = new System.Drawing.Size(599, 373);
            this.tbOutput.TabIndex = 1;
            // 
            // openFolderButton
            // 
            this.openFolderButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.openFolderButton.Location = new System.Drawing.Point(12, 12);
            this.openFolderButton.Name = "openFolderButton";
            this.openFolderButton.Size = new System.Drawing.Size(75, 23);
            this.openFolderButton.TabIndex = 0;
            this.openFolderButton.Text = "Open Folder";
            this.openFolderButton.UseVisualStyleBackColor = true;
            this.openFolderButton.Click += new System.EventHandler(this.openFolderButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(93, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 13);
            this.label1.TabIndex = 1;
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cbUseSubfolders
            // 
            this.cbUseSubfolders.AutoSize = true;
            this.cbUseSubfolders.Checked = true;
            this.cbUseSubfolders.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbUseSubfolders.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbUseSubfolders.Location = new System.Drawing.Point(12, 41);
            this.cbUseSubfolders.Name = "cbUseSubfolders";
            this.cbUseSubfolders.Size = new System.Drawing.Size(95, 17);
            this.cbUseSubfolders.TabIndex = 2;
            this.cbUseSubfolders.Text = "Use Subfolders";
            this.cbUseSubfolders.UseVisualStyleBackColor = true;
            this.cbUseSubfolders.CheckedChanged += new System.EventHandler(this.cbUseSubfolders_CheckedChanged);
            // 
            // goButton
            // 
            this.goButton.Enabled = false;
            this.goButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.goButton.Location = new System.Drawing.Point(12, 64);
            this.goButton.Name = "goButton";
            this.goButton.Size = new System.Drawing.Size(75, 23);
            this.goButton.TabIndex = 3;
            this.goButton.Text = "Go!";
            this.goButton.UseVisualStyleBackColor = true;
            this.goButton.Click += new System.EventHandler(this.goButton_Click);
            // 
            // cbShowSubfolderName
            // 
            this.cbShowSubfolderName.AutoSize = true;
            this.cbShowSubfolderName.Location = new System.Drawing.Point(114, 41);
            this.cbShowSubfolderName.Name = "cbShowSubfolderName";
            this.cbShowSubfolderName.Size = new System.Drawing.Size(154, 17);
            this.cbShowSubfolderName.TabIndex = 4;
            this.cbShowSubfolderName.Text = "Show Names of Subfolders";
            this.cbShowSubfolderName.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(599, 481);
            this.Controls.Add(this.tbOutput);
            this.Controls.Add(this.panel1);
            this.Name = "Form1";
            this.Text = "ListFiles";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button goButton;
        private System.Windows.Forms.CheckBox cbUseSubfolders;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button openFolderButton;
        private System.Windows.Forms.TextBox tbOutput;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.CheckBox cbShowSubfolderName;
    }
}

