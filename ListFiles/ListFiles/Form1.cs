using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace ListFiles
{
    public partial class Form1 : Form
    {
        private DirectoryInfo currentDir;

        public Form1()
        {
            InitializeComponent();
        }

        private void openFolderButton_Click(object sender, EventArgs e)
        {
            if(folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                currentDir = new DirectoryInfo(folderBrowserDialog1.SelectedPath);
                label1.Text = currentDir.FullName;
                goButton.Enabled = true;
            }
        }

        private void goButton_Click(object sender, EventArgs e)
        {
            tbOutput.Text = GetFiles(currentDir);
        }

        private string GetFiles(DirectoryInfo currentDir)
        {
            string result = "";
            if (cbUseSubfolders.Checked)
            {
                foreach (DirectoryInfo dir in currentDir.GetDirectories())
                {
                    if (cbShowSubfolderName.Checked)
                    {
                        result += "#Begin Directory " + dir.Name + "\r\n";
                    }

                    result += GetFiles(dir) + "\r\n";


                    if (cbShowSubfolderName.Checked)
                    {
                        result += "#End Directory " + dir.Name + "\r\n";
                    }
                }
            }
            foreach (FileInfo file in currentDir.GetFiles())
            {
                result += file.Name + "\r\n";
            }
            return result;
        }

        private void cbUseSubfolders_CheckedChanged(object sender, EventArgs e)
        {
            cbShowSubfolderName.Enabled = cbUseSubfolders.Checked;
        }
    }
}