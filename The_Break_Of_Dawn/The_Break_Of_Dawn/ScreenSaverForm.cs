using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace The_Break_Of_Dawn
{
    public partial class ScreenSaverForm : Form
    {
        private Point mouseLocation;
        private bool isActive = false;

        public ScreenSaverForm()
        {
            InitializeComponent();
            Cursor.Hide();
            Capture = true;
        }

        private void ScreenSaverForm_MouseMove(object sender, MouseEventArgs e)
        {            // Set IsActive and MouseLocation only the first time this event is called.
            if (!isActive)
            {
                mouseLocation = MousePosition;
                isActive = true;
            }
            else
            {
                // If the mouse has moved significantly since first call, close.
                if ((Math.Abs(MousePosition.X - mouseLocation.X) > 10) ||
                    (Math.Abs(MousePosition.Y - mouseLocation.Y) > 10))
                {
                    Close();
                }
            }
        }

        private void ScreenSaverForm_MouseDown(object sender, MouseEventArgs e)
        {
            Close();
        }

        private void ScreenSaverForm_KeyDown(object sender, KeyEventArgs e)
        {
            Close();
        }
    }
}