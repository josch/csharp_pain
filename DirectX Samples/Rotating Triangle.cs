using System;
using System.Drawing;
using System.Windows.Forms;

using Microsoft.DirectX;
using Microsoft.DirectX.Direct3D;

namespace Solar_System
{
	public class MainForm : System.Windows.Forms.Form
	{
		private System.Windows.Forms.ToolBar toolBar1;
		private System.Windows.Forms.MainMenu mainMenu1;
		private System.Windows.Forms.StatusBarPanel statusBarPanel1;
		private System.Windows.Forms.StatusBar statusBar1;
		private System.Windows.Forms.ToolBarButton toolBarButton1;
		private System.Windows.Forms.MenuItem menuItem1;
		
		private Device m_Device = null;
		private VertexBuffer m_VertexBuffer = null;
		
		public MainForm()
		{
			InitializeComponent();
		}
		
		public void InitGfx()
		{
			try
			{
				PresentParameters pp = new PresentParameters();
				pp.Windowed = true;
				pp.SwapEffect = SwapEffect.Discard;
				pp.EnableAutoDepthStencil = true;
				pp.AutoDepthStencilFormat = DepthFormat.D16;
				
				m_Device = new Device(Manager.Adapters.Default.Adapter,
				                      DeviceType.Hardware,
				                      this,
				                      CreateFlags.HardwareVertexProcessing,
				                      pp);
				m_Device.DeviceReset += new System.EventHandler(this.OnResetDevice);
				m_Device.VertexFormat = CustomVertex.PositionNormalColored.Format;
				this.OnCreateDevice(m_Device, null);
				this.OnResetDevice(m_Device, null);
			}
			catch(DirectXException e)
			{
				MessageBox.Show(e.Message);
			}
		}
		
		public void OnCreateDevice(object sender, EventArgs e)
		{
			Device dev = (Device)sender;
			
			m_VertexBuffer = new VertexBuffer(typeof(CustomVertex.PositionNormalColored),
			                                  100, dev, 0,
			                                  CustomVertex.PositionNormalColored.Format,
			                                  Pool.Managed);
			m_VertexBuffer.Created += new System.EventHandler(this.OnCreateVertexBuffer);
			this.OnCreateVertexBuffer(m_VertexBuffer, null);
		}
		
		public void OnResetDevice(object sender, EventArgs e)
		{
			Device dev = (Device)sender;
			dev.RenderState.CullMode = Cull.None;
			dev.RenderState.ZBufferEnable = true;
			dev.RenderState.Lighting = true;
		}
		
		public void OnCreateVertexBuffer(object sender, EventArgs e)
		{
			VertexBuffer vb = (VertexBuffer)sender;
			
			CustomVertex.PositionNormalColored[] verts = (CustomVertex.PositionNormalColored[])vb.Lock(0,0);
			
//			verts[0] = new CustomVertex.PositionNormalColored(-1.0f,-1.0f,0.0f,Color.Yellow.ToArgb());
//			verts[1] = new CustomVertex.PositionNormalColored(1.0f,-1.0f,0.0f,Color.Green.ToArgb());
//			verts[2] = new CustomVertex.PositionNormalColored(0.0f,1.0f,0.0f,Color.Red.ToArgb());
			
			for (int i = 0; i < 50; i++)
			{
				float theta = (float)(2 * Math.PI * i) / 49;
				verts[2 * i].SetPosition(new Vector3((float)Math.Sin(theta),-1,
				                                     (float)Math.Cos(theta)));
				verts[2 * i].SetNormal(new Vector3((float)Math.Sin(theta),0,
				                                   (float)Math.Cos(theta)));
				verts[2 * i + 1].SetPosition(new Vector3((float)Math.Sin(theta),1,
				                                         (float)Math.Cos(theta));
				verts[2 * i + 1].SetNormal(new Vector3((float)Math.Sin(theta),0,
				                                       (float)Math.Cos(theta)));
			}
			
			vb.Unlock();
		}
		
		public void Render()
		{
			
			m_Device.Clear(ClearFlags.Target, Color.Blue, 0.0f, 0);
			m_Device.BeginScene();
			SetupLights();
			SetupMatrices();
			m_Device.SetStreamSource(0,m_VertexBuffer,0);
			m_Device.DrawPrimitives(PrimitiveType.TriangleList,0,1);
			m_Device.EndScene();
			m_Device.Present();
		}
		
		public void SetupMatrices()
		{
			int  iTime  = Environment.TickCount % 1000;
			float fAngle = iTime * (2.0f * (float)Math.PI) / 1000.0f;
			m_Device.Transform.World = Matrix.Rotation(fAngle);
			
			m_Device.Transform.View = Matrix.LookAtLH(new Vector3(0.0f,3.0f,-5.0f),
			                                          new Vector3(0.0f,0.0f,0.0f),
			                                          new Vector3(0.0f,1.0f,0.0f));
			m_Device.Transform.Projection = Matrix.PerspectiveFovLH((float)Math.PI/4,
			                                                        1.0f,1.0f,100.0f);
		}
		
		public void SetupLights()
		{
			m_Device.Lights[0].Diffuse = Color.White;
			m_Device.Lights[0].Type = LightType.Directional;
			m_Device.Lights[0].Direction = new Vector3(-1, -1, 3);
			m_Device.Lights[0].Commit();
			m_Device.Lights[0].Enabled = true;
			
			m_Device.RenderState.Ambient = Color.FromArgb(0x40, 0x40, 0x40);
		}
		
		public void Shutdown()
		{
			m_Device.Dispose();
		}
		
		[STAThread]
		public static void Main(string[] args)
		{
			MainForm mf = new MainForm();
			mf.InitGfx();
			mf.Show();
			
			DateTime begin;
			DateTime end;
			TimeSpan period;
			
			while(mf.Created)
			{
				begin = DateTime.Now;
				mf.Render();
				Application.DoEvents();
				end = DateTime.Now;
				period = end - begin;
				if(period.Milliseconds!=0)
				{
					mf.statusBarPanel1.Text = Convert.ToString(1.0f/period.Milliseconds);
				}
			}
			
			mf.Shutdown();
		}
		
		private void InitializeComponent() {
			this.menuItem1 = new System.Windows.Forms.MenuItem();
			this.toolBarButton1 = new System.Windows.Forms.ToolBarButton();
			this.statusBar1 = new System.Windows.Forms.StatusBar();
			this.statusBarPanel1 = new System.Windows.Forms.StatusBarPanel();
			this.mainMenu1 = new System.Windows.Forms.MainMenu();
			this.toolBar1 = new System.Windows.Forms.ToolBar();
			((System.ComponentModel.ISupportInitialize)(this.statusBarPanel1)).BeginInit();
			this.SuspendLayout();
			// 
			// menuItem1
			// 
			this.menuItem1.Index = 0;
			this.menuItem1.Text = "Datei";
			// 
			// statusBar1
			// 
			this.statusBar1.Location = new System.Drawing.Point(0, 439);
			this.statusBar1.Name = "statusBar1";
			this.statusBar1.Panels.AddRange(new System.Windows.Forms.StatusBarPanel[] {
						this.statusBarPanel1});
			this.statusBar1.ShowPanels = true;
			this.statusBar1.Size = new System.Drawing.Size(664, 22);
			this.statusBar1.TabIndex = 1;
			this.statusBar1.Text = "statusBar1";
			// 
			// statusBarPanel1
			// 
			this.statusBarPanel1.Text = "statusBarPanel1";
			this.statusBarPanel1.Width = 200;
			// 
			// mainMenu1
			// 
			this.mainMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
						this.menuItem1});
			// 
			// toolBar1
			// 
			this.toolBar1.Buttons.AddRange(new System.Windows.Forms.ToolBarButton[] {
						this.toolBarButton1});
			this.toolBar1.DropDownArrows = true;
			this.toolBar1.Location = new System.Drawing.Point(0, 0);
			this.toolBar1.Name = "toolBar1";
			this.toolBar1.ShowToolTips = true;
			this.toolBar1.Size = new System.Drawing.Size(664, 28);
			this.toolBar1.TabIndex = 3;
			// 
			// MainForm
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(664, 461);
			this.Controls.Add(this.toolBar1);
			this.Controls.Add(this.statusBar1);
			this.Menu = this.mainMenu1;
			this.Name = "MainForm";
			this.Text = "MainForm";
			this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.MainFormKeyPress);
			((System.ComponentModel.ISupportInitialize)(this.statusBarPanel1)).EndInit();
			this.ResumeLayout(false);
		}
		
		protected void MainFormKeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
		{
			if((int)e.KeyChar == (int)Keys.Escape)
			{
				this.Close();
			}
		}
		
		protected void OnPaint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			this.Render();
		}
		
	}
}
