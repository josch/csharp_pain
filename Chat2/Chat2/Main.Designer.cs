namespace Chat
{
    partial class Chat22
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Chat22));
            this.Sendbtn = new System.Windows.Forms.Button();
            this.InputTextBox = new System.Windows.Forms.TextBox();
            this.tbUsername = new System.Windows.Forms.TextBox();
            this.btnColor = new System.Windows.Forms.Button();
            this.cbL33T = new System.Windows.Forms.CheckBox();
            this.ColorDlg = new System.Windows.Forms.ColorDialog();
            this.lvOnline = new System.Windows.Forms.ListView();
            this.Sendstatus = new System.Windows.Forms.Timer(this.components);
            this.CheckTimeouts = new System.Windows.Forms.Timer(this.components);
            this.pColor = new System.Windows.Forms.Panel();
            this.btnPRIV = new System.Windows.Forms.Button();
            this.btnWAKE = new System.Windows.Forms.Button();
            this.llHilfe = new System.Windows.Forms.LinkLabel();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.cbSilent = new System.Windows.Forms.CheckBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.lOpazitšt = new System.Windows.Forms.Label();
            this.nUDOpacity = new System.Windows.Forms.NumericUpDown();
            this.cbAlwaysOnTop = new System.Windows.Forms.CheckBox();
            this.OutputTextBox = new System.Windows.Forms.RichTextBox();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.silentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.hauptfensteranzeigenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.chatbeendenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nUDOpacity)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // Sendbtn
            // 
            this.Sendbtn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Sendbtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Sendbtn.Location = new System.Drawing.Point(0, 0);
            this.Sendbtn.Name = "Sendbtn";
            this.Sendbtn.Size = new System.Drawing.Size(569, 69);
            this.Sendbtn.TabIndex = 0;
            this.Sendbtn.Text = "senden";
            this.Sendbtn.Click += new System.EventHandler(this.Sendbtn_Click);
            // 
            // InputTextBox
            // 
            this.InputTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.InputTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.InputTextBox.Location = new System.Drawing.Point(0, 0);
            this.InputTextBox.Multiline = true;
            this.InputTextBox.Name = "InputTextBox";
            this.InputTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.InputTextBox.Size = new System.Drawing.Size(569, 69);
            this.InputTextBox.TabIndex = 0;
            // 
            // tbUsername
            // 
            this.tbUsername.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbUsername.Location = new System.Drawing.Point(3, 29);
            this.tbUsername.Name = "tbUsername";
            this.tbUsername.Size = new System.Drawing.Size(146, 20);
            this.tbUsername.TabIndex = 3;
            // 
            // btnColor
            // 
            this.btnColor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnColor.Location = new System.Drawing.Point(74, 57);
            this.btnColor.Name = "btnColor";
            this.btnColor.Size = new System.Drawing.Size(75, 20);
            this.btnColor.TabIndex = 4;
            this.btnColor.Text = "Farbe";
            this.btnColor.Click += new System.EventHandler(this.btnColor_Click);
            // 
            // cbL33T
            // 
            this.cbL33T.AutoSize = true;
            this.cbL33T.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbL33T.Location = new System.Drawing.Point(3, 83);
            this.cbL33T.Name = "cbL33T";
            this.cbL33T.Size = new System.Drawing.Size(44, 17);
            this.cbL33T.TabIndex = 5;
            this.cbL33T.Text = "L33T";
            // 
            // ColorDlg
            // 
            this.ColorDlg.AnyColor = true;
            this.ColorDlg.FullOpen = true;
            // 
            // lvOnline
            // 
            this.lvOnline.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lvOnline.CheckBoxes = true;
            this.lvOnline.FullRowSelect = true;
            this.lvOnline.Location = new System.Drawing.Point(3, 107);
            this.lvOnline.MultiSelect = false;
            this.lvOnline.Name = "lvOnline";
            this.lvOnline.Size = new System.Drawing.Size(146, 284);
            this.lvOnline.TabIndex = 6;
            this.lvOnline.View = System.Windows.Forms.View.List;
            this.lvOnline.DoubleClick += new System.EventHandler(this.lvOnline_DoubleClick);
            // 
            // Sendstatus
            // 
            this.Sendstatus.Enabled = true;
            this.Sendstatus.Interval = 5000;
            this.Sendstatus.Tick += new System.EventHandler(this.Sendstatus_Tick);
            // 
            // CheckTimeouts
            // 
            this.CheckTimeouts.Enabled = true;
            this.CheckTimeouts.Interval = 15000;
            this.CheckTimeouts.Tick += new System.EventHandler(this.CheckTimeouts_Tick);
            // 
            // pColor
            // 
            this.pColor.Location = new System.Drawing.Point(3, 56);
            this.pColor.Name = "pColor";
            this.pColor.Size = new System.Drawing.Size(65, 21);
            this.pColor.TabIndex = 9;
            // 
            // btnPRIV
            // 
            this.btnPRIV.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPRIV.Location = new System.Drawing.Point(3, 397);
            this.btnPRIV.Name = "btnPRIV";
            this.btnPRIV.Size = new System.Drawing.Size(66, 20);
            this.btnPRIV.TabIndex = 10;
            this.btnPRIV.Text = "PRIV";
            this.btnPRIV.Click += new System.EventHandler(this.btnPRIV_Click);
            // 
            // btnWAKE
            // 
            this.btnWAKE.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnWAKE.Location = new System.Drawing.Point(88, 397);
            this.btnWAKE.Name = "btnWAKE";
            this.btnWAKE.Size = new System.Drawing.Size(61, 20);
            this.btnWAKE.TabIndex = 11;
            this.btnWAKE.Text = "WAKE";
            this.btnWAKE.Click += new System.EventHandler(this.btnWAKE_Click);
            // 
            // llHilfe
            // 
            this.llHilfe.AutoSize = true;
            this.llHilfe.Location = new System.Drawing.Point(125, 85);
            this.llHilfe.Name = "llHilfe";
            this.llHilfe.Size = new System.Drawing.Size(24, 13);
            this.llHilfe.TabIndex = 12;
            this.llHilfe.TabStop = true;
            this.llHilfe.Text = "Hilfe";
            this.llHilfe.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.llHilfe_LinkClicked);
            // 
            // btnClear
            // 
            this.btnClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClear.Location = new System.Drawing.Point(3, 3);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(65, 20);
            this.btnClear.TabIndex = 13;
            this.btnClear.Text = "Clear";
            // 
            // btnSave
            // 
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.Location = new System.Drawing.Point(74, 3);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 20);
            this.btnSave.TabIndex = 14;
            this.btnSave.Text = "Save";
            // 
            // cbSilent
            // 
            this.cbSilent.AutoSize = true;
            this.cbSilent.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbSilent.Location = new System.Drawing.Point(61, 84);
            this.cbSilent.Name = "cbSilent";
            this.cbSilent.Size = new System.Drawing.Size(45, 17);
            this.cbSilent.TabIndex = 15;
            this.cbSilent.Text = "Silent";
            this.cbSilent.CheckedChanged += new System.EventHandler(this.cbSilent_CheckedChanged);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.lOpazitšt);
            this.panel1.Controls.Add(this.nUDOpacity);
            this.panel1.Controls.Add(this.cbAlwaysOnTop);
            this.panel1.Controls.Add(this.btnClear);
            this.panel1.Controls.Add(this.cbSilent);
            this.panel1.Controls.Add(this.tbUsername);
            this.panel1.Controls.Add(this.btnColor);
            this.panel1.Controls.Add(this.btnSave);
            this.panel1.Controls.Add(this.cbL33T);
            this.panel1.Controls.Add(this.llHilfe);
            this.panel1.Controls.Add(this.lvOnline);
            this.panel1.Controls.Add(this.btnWAKE);
            this.panel1.Controls.Add(this.pColor);
            this.panel1.Controls.Add(this.btnPRIV);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel1.Location = new System.Drawing.Point(571, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(152, 468);
            this.panel1.TabIndex = 16;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(110, 446);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(11, 13);
            this.label1.TabIndex = 19;
            this.label1.Text = "%";
            // 
            // lOpazitšt
            // 
            this.lOpazitšt.AutoSize = true;
            this.lOpazitšt.Location = new System.Drawing.Point(6, 446);
            this.lOpazitšt.Name = "lOpazitšt";
            this.lOpazitšt.Size = new System.Drawing.Size(45, 13);
            this.lOpazitšt.TabIndex = 18;
            this.lOpazitšt.Text = "Opazitšt:";
            // 
            // nUDOpacity
            // 
            this.nUDOpacity.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nUDOpacity.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.nUDOpacity.Location = new System.Drawing.Point(57, 446);
            this.nUDOpacity.Minimum = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.nUDOpacity.Name = "nUDOpacity";
            this.nUDOpacity.Size = new System.Drawing.Size(47, 20);
            this.nUDOpacity.TabIndex = 17;
            this.nUDOpacity.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.nUDOpacity.ValueChanged += new System.EventHandler(this.nUDOpacity_ValueChanged);
            // 
            // cbAlwaysOnTop
            // 
            this.cbAlwaysOnTop.AutoSize = true;
            this.cbAlwaysOnTop.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbAlwaysOnTop.Location = new System.Drawing.Point(3, 423);
            this.cbAlwaysOnTop.Name = "cbAlwaysOnTop";
            this.cbAlwaysOnTop.Size = new System.Drawing.Size(85, 17);
            this.cbAlwaysOnTop.TabIndex = 16;
            this.cbAlwaysOnTop.Text = "Always on top";
            this.cbAlwaysOnTop.CheckedChanged += new System.EventHandler(this.cbAlwaysOnTop_CheckedChanged);
            // 
            // OutputTextBox
            // 
            this.OutputTextBox.BackColor = System.Drawing.SystemColors.Window;
            this.OutputTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.OutputTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.OutputTextBox.Location = new System.Drawing.Point(0, 0);
            this.OutputTextBox.Name = "OutputTextBox";
            this.OutputTextBox.ReadOnly = true;
            this.OutputTextBox.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.ForcedVertical;
            this.OutputTextBox.Size = new System.Drawing.Size(569, 391);
            this.OutputTextBox.TabIndex = 18;
            this.OutputTextBox.Text = "";
            // 
            // splitContainer1
            // 
            this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.OutputTextBox);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.InputTextBox);
            this.splitContainer1.Panel2.Controls.Add(this.Sendbtn);
            this.splitContainer1.Size = new System.Drawing.Size(571, 468);
            this.splitContainer1.SplitterDistance = 393;
            this.splitContainer1.TabIndex = 19;
            this.splitContainer1.Text = "splitContainer1";
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.BalloonTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.notifyIcon1.ContextMenuStrip = this.contextMenuStrip1;
            this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
            this.notifyIcon1.Text = "Chat 2.2";
            this.notifyIcon1.Visible = true;
            this.notifyIcon1.DoubleClick += new System.EventHandler(this.notifyIcon1_DoubleClick);
            this.notifyIcon1.BalloonTipClicked += new System.EventHandler(this.notifyIcon1_BalloonTipClicked);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Enabled = true;
            this.contextMenuStrip1.GripMargin = new System.Windows.Forms.Padding(2);
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.silentToolStripMenuItem,
            this.hauptfensteranzeigenToolStripMenuItem,
            this.chatbeendenToolStripMenuItem});
            this.contextMenuStrip1.Location = new System.Drawing.Point(79, 59);
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.contextMenuStrip1.Size = new System.Drawing.Size(174, 70);
            // 
            // silentToolStripMenuItem
            // 
            this.silentToolStripMenuItem.CheckOnClick = true;
            this.silentToolStripMenuItem.Name = "silentToolStripMenuItem";
            this.silentToolStripMenuItem.Text = "Silent";
            this.silentToolStripMenuItem.CheckedChanged += new System.EventHandler(this.silentToolStripMenuItem_CheckedChanged);
            // 
            // hauptfensteranzeigenToolStripMenuItem
            // 
            this.hauptfensteranzeigenToolStripMenuItem.Name = "hauptfensteranzeigenToolStripMenuItem";
            this.hauptfensteranzeigenToolStripMenuItem.Text = "Hauptfenster anzeigen";
            this.hauptfensteranzeigenToolStripMenuItem.Click += new System.EventHandler(this.hauptfensteranzeigenToolStripMenuItem_Click);
            // 
            // chatbeendenToolStripMenuItem
            // 
            this.chatbeendenToolStripMenuItem.Name = "chatbeendenToolStripMenuItem";
            this.chatbeendenToolStripMenuItem.Text = "Chat beenden";
            this.chatbeendenToolStripMenuItem.Click += new System.EventHandler(this.chatbeendenToolStripMenuItem_Click);
            // 
            // Chat22
            // 
            this.AcceptButton = this.Sendbtn;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(723, 468);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Chat22";
            this.Text = "Chat 2.2.1";
            this.Shown += new System.EventHandler(this.Chat21_Shown);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Chat_FormClosing);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nUDOpacity)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            this.splitContainer1.ResumeLayout(false);
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button Sendbtn;
        private System.Windows.Forms.TextBox InputTextBox;
        private System.Windows.Forms.TextBox tbUsername;
        private System.Windows.Forms.Button btnColor;
        private System.Windows.Forms.CheckBox cbL33T;
        private System.Windows.Forms.ColorDialog ColorDlg;
        private System.Windows.Forms.ListView lvOnline;
        private System.Windows.Forms.Timer Sendstatus;
        private System.Windows.Forms.Timer CheckTimeouts;
        private System.Windows.Forms.Panel pColor;
        private System.Windows.Forms.Button btnPRIV;
        private System.Windows.Forms.Button btnWAKE;
        private System.Windows.Forms.LinkLabel llHilfe;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.CheckBox cbSilent;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RichTextBox OutputTextBox;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.NotifyIcon notifyIcon1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem chatbeendenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem hauptfensteranzeigenToolStripMenuItem;
        private System.Windows.Forms.CheckBox cbAlwaysOnTop;
        private System.Windows.Forms.Label lOpazitšt;
        private System.Windows.Forms.NumericUpDown nUDOpacity;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ToolStripMenuItem silentToolStripMenuItem;
    }
}

