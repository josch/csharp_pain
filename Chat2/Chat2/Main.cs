using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.Media;

namespace Chat
{
    public partial class Chat22 : Form
    {
        private delegate void OutputHandler(string time, string user, string message, string color);
        private delegate void AddOnlineHandler(IPAddress ip, string user);
        private delegate void CheckTimeoutsHandler();
        private delegate void PlayWakeSoundHandler();
        private delegate void PlayMessageSoundHandler();
        UdpClient receive = null;
        UdpClient send = null;
        Thread t = null;
        Properties.Settings sett = new Properties.Settings();
        SoundPlayer WakeSound = new SoundPlayer(Properties.Resources.notify);
        SoundPlayer MessageSound = new SoundPlayer(Properties.Resources.transmission);

        public Chat22()
        {
            InitializeComponent();

            tbUsername.ForeColor = sett.color;
            tbUsername.Text = sett.username;
            pColor.BackColor = sett.color;

            send = new UdpClient();
            receive = new UdpClient(11000);
        }

        void Listen()
        {
            IPEndPoint EP = new IPEndPoint(IPAddress.Any, 11000);
            byte[] buffer = null;
            string tempreceived = "";
            string received = "";
            string STATtempreceived = "";
            string STATreceived = "";
            string[] token;
            string[] tokentext;
            string[] user;
            bool userfound = false;
            while (true)
            {
                try
                {
                    buffer = receive.Receive(ref EP);
                }
                catch
                {
                    //MessageBox.Show(se.Message + " " + se.SocketErrorCode, "Eine fuckin' SocketException");
                }
                token = Encoding.Unicode.GetString(buffer).Split('‼');
                tokentext = token[2].Split(' ');
                switch(tokentext[0])
                {
                    case "STAT":
                        STATreceived = EP.Address.ToString() + " " + token[0] + ": " + token[2];
                        if (STATreceived != STATtempreceived)
                        {
                            this.Invoke(new AddOnlineHandler(AddOnline), new object[] { EP.Address, tokentext[1] });
                        }
                        STATtempreceived = STATreceived;
                        break;
                    case "PRIV":
                        userfound = false;
                        user = tokentext[1].Split('+');
                        foreach (string u in user)
                        {
                            if (u == tbUsername.Text)
                            {
                                userfound = true;
                                break;
                            }
                        }
                        if (userfound)
                        {
                            received = EP.Address.ToString() + " " + token[0] + ": " + token[2];
                            if (received != tempreceived)
                            {
                                this.Invoke(new OutputHandler(Output), new object[] { DateTime.Now.ToLongTimeString(), token[0], token[2], token[1] });
                                this.Invoke(new PlayMessageSoundHandler(PlayMessageSound));
                            }
                            tempreceived = received;
                        }
                        break;
                    case "WAKE":
                        userfound = false;
                        user = tokentext[1].Split('+');
                        foreach (string u in user)
                        {
                            if (u == tbUsername.Text)
                            {
                                userfound = true;
                                break;
                            }
                        }
                        if (userfound)
                        {
                            received = EP.Address.ToString() + " " + token[0] + ": " + token[2];
                            if (received != tempreceived)
                            {
                                this.Invoke(new OutputHandler(Output), new object[] { DateTime.Now.ToLongTimeString(), token[0], token[2], token[1] });
                                this.Invoke(new PlayWakeSoundHandler(PlayWakeSound));
                            }
                            tempreceived = received;
                        }
                        break;
                    default:
                        received = EP.Address.ToString() + " " + token[0] + ": " + token[2];
                        if (received != tempreceived)
                        {
                            this.Invoke(new OutputHandler(Output), new object[] { DateTime.Now.ToLongTimeString(), token[0], token[2], token[1] });
                            this.Invoke(new PlayMessageSoundHandler(PlayMessageSound));
                        }
                        tempreceived = received;
                        break;
                }
            }
        }

        void Output(string time, string user, string message, string color)
        {
            string[] token = color.Split(' ');
            OutputTextBox.SelectionStart = OutputTextBox.Text.Length;
            OutputTextBox.SelectionFont = new Font("Terminal", 7, FontStyle.Regular);
            OutputTextBox.SelectionColor = Color.Black;
            OutputTextBox.AppendText("\r\n" + time);
            OutputTextBox.SelectionStart = OutputTextBox.Text.Length;
            OutputTextBox.SelectionFont = new Font("Arial", 9, FontStyle.Bold);
            OutputTextBox.SelectionColor = Color.FromArgb(int.Parse(token[0]), int.Parse(token[1]), int.Parse(token[2]), int.Parse(token[3]));
            OutputTextBox.AppendText(" "+user+": ");
            OutputTextBox.SelectionStart = OutputTextBox.Text.Length;
            OutputTextBox.SelectionFont = new Font("Arial", 9, FontStyle.Regular);
            OutputTextBox.SelectionColor = Color.Black;
            OutputTextBox.AppendText(message);
            OutputTextBox.Select(OutputTextBox.Text.Length - 1, 0);
            OutputTextBox.ScrollToCaret();

            if (this.Visible == false)
            {
                notifyIcon1.ShowBalloonTip(32767, user, message, ToolTipIcon.Info);
            }
        }

        void AddOnline(IPAddress ip, string user)
        {
            bool found = false;
            foreach (ListViewItem lvi in lvOnline.Items)
            {
                if (lvi.Text == ip.ToString() + " " + user)
                {
                    lvi.SubItems[1].Text = DateTime.Now.ToString();
                    found = true;
                    break;
                }
            }
            if (!found)
            {
                ListViewItem lvi = new ListViewItem();
                lvi.Name = ip.ToString();
                lvi.Text = ip.ToString() + " " + user;
                lvi.SubItems.Add(DateTime.Now.ToString());
                lvOnline.Items.Add(lvi);
            }
        }

        private void Chat_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                e.Cancel = true;
                this.WindowState = FormWindowState.Minimized;
                this.Visible = false;
            }
        }

        private void Sendbtn_Click(object sender, EventArgs e)
        {
            if (InputTextBox.Text != "")
            {
                string text = InputTextBox.Text;
                if (cbL33T.Checked && (text.Length < 4 || ((text.Substring(0, 4) != "PRIV") && (text.Substring(0, 4) != "WAKE"))))
                {
                    text = text.Replace('a', '4');
                    text = text.Replace('A', '4');
                    text = text.Replace("d", "|>");
                    text = text.Replace("D", "|>");
                    text = text.Replace('e', '3');
                    text = text.Replace('E', '3');
                    text = text.Replace('i', '[');
                    text = text.Replace('I', '[');
                    text = text.Replace('l', '1');
                    text = text.Replace('L', '1');
                    text = text.Replace("m", "|\\/|");
                    text = text.Replace("M", "|\\/|");
                    text = text.Replace("n", "|\\|");
                    text = text.Replace("N", "|\\|");
                    text = text.Replace('o', '0');
                    text = text.Replace('O', '0');
                    text = text.Replace('s', '5');
                    text = text.Replace('S', '5');
                    text = text.Replace('t', '7');
                    text = text.Replace('T', '7');
                    text = text.Replace('u', '0');
                    text = text.Replace('u', '0');
                    text = text.Replace("w", "\\/\\/");
                    text = text.Replace("W", "\\/\\/");
                }
                Senden(text);
                InputTextBox.Text = "";
            }
        }

        private void btnColor_Click(object sender, EventArgs e)
        {
            ColorDlg.ShowDialog();
            tbUsername.ForeColor = ColorDlg.Color;
            pColor.BackColor = ColorDlg.Color;
        }

        void Senden(string text)
        {
            byte[] buf = Encoding.Unicode.GetBytes(tbUsername.Text + "‼" + tbUsername.ForeColor.A.ToString() + " " + tbUsername.ForeColor.R.ToString() + " " + tbUsername.ForeColor.G.ToString() + " " + tbUsername.ForeColor.B.ToString() + "‼" + text);
            try
            {
                for (int i = 0; i < 2; i++)
                {
                    send.Send(buf, buf.Length, new IPEndPoint(IPAddress.Parse("192.168.0.255"), 11000));
                }
            }
            catch
            {
                //MessageBox.Show(se.Message + " " + se.SocketErrorCode, "Eine fuckin' SocketException");
            }
        }

        private void Sendstatus_Tick(object sender, EventArgs e)
        {
            Senden("STAT " + tbUsername.Text);
        }

        private void CheckTimeouts_Tick(object sender, EventArgs e)
        {
            foreach (ListViewItem lvi in lvOnline.Items)
            {
                if ((DateTime.Now - DateTime.Parse(lvi.SubItems[1].Text)) > TimeSpan.FromSeconds(25))
                {
                    lvi.Remove();
                }
            }
        }

        private void Chat21_Shown(object sender, EventArgs e)
        {
            t = new Thread(new ThreadStart(Listen));
            t.Start();

            Senden("STAT " + tbUsername.Text);
        }

        void PlayWakeSound()
        {
            if (!cbSilent.Checked)
            {
                WakeSound.Play();
            }
        }

        void PlayMessageSound()
        {
            if (!cbSilent.Checked && !InputTextBox.Focused)
            {
                MessageSound.Play();
            }
        }

        private void btnPRIV_Click(object sender, EventArgs e)
        {
            InputTextBox.Text = "PRIV ";
            foreach (ListViewItem lvi in lvOnline.CheckedItems)
            {
                InputTextBox.Text += lvi.Text.Split(' ')[1];
                InputTextBox.Text += "+";
            }
            InputTextBox.Text = InputTextBox.Text.Remove(InputTextBox.Text.Length-1, 1);
            InputTextBox.Text += " ";
            InputTextBox.Focus();
            InputTextBox.Select(InputTextBox.Text.Length, 0);
        }

        private void btnWAKE_Click(object sender, EventArgs e)
        {
            InputTextBox.Text = "WAKE ";
            foreach (ListViewItem lvi in lvOnline.CheckedItems)
            {
                InputTextBox.Text += lvi.Text.Split(' ')[1];
                InputTextBox.Text += "+";
            }
            InputTextBox.Text = InputTextBox.Text.Remove(InputTextBox.Text.Length - 1, 1);
            InputTextBox.Text += " ";
            InputTextBox.Focus();
            InputTextBox.Select(InputTextBox.Text.Length, 0);
        }

        private void llHilfe_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Hilfe hilfe = new Hilfe();
            hilfe.Show();
        }

        private void chatbeendenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            sett.username = tbUsername.Text;
            sett.color = tbUsername.ForeColor;
            sett.Save();
            t.Abort();
            receive.Close();
            send.Close();
            this.Dispose();
        }

        private void hauptfensteranzeigenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Visible = true;
            this.WindowState = FormWindowState.Normal;
        }

        private void notifyIcon1_DoubleClick(object sender, EventArgs e)
        {
            this.Visible = true;
            this.WindowState = FormWindowState.Normal;
        }

        private void cbAlwaysOnTop_CheckedChanged(object sender, EventArgs e)
        {
            this.TopMost = cbAlwaysOnTop.Checked;
        }

        private void nUDOpacity_ValueChanged(object sender, EventArgs e)
        {
            this.Opacity = (double)nUDOpacity.Value / 100;
        }

        private void silentToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            cbSilent.Checked = silentToolStripMenuItem.Checked;
        }

        private void cbSilent_CheckedChanged(object sender, EventArgs e)
        {
            silentToolStripMenuItem.Checked = cbSilent.Checked;
        }

        private void notifyIcon1_BalloonTipClicked(object sender, EventArgs e)
        {
            this.Visible = true;
            this.WindowState = FormWindowState.Normal;
        }

        private void lvOnline_DoubleClick(object sender, EventArgs e)
        {
            MessageBox.Show(lvOnline.SelectedItems[0].Name.ToString());
        }
    }
}