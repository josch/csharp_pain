namespace FITWorks
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.StatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.nNewDivideSpecialTab = new System.Windows.Forms.Button();
            this.bMultiplyResult = new System.Windows.Forms.Button();
            this.tbFactor = new System.Windows.Forms.TextBox();
            this.bNewDivideTab = new System.Windows.Forms.Button();
            this.bNewMinusTab = new System.Windows.Forms.Button();
            this.bNewPlusTab = new System.Windows.Forms.Button();
            this.bErgebnis = new System.Windows.Forms.Button();
            this.bDeleteTab = new System.Windows.Forms.Button();
            this.TabContainer = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.ResultDisplay = new FITWorks.FITDisplay();
            this.bDeleteAllTabs = new System.Windows.Forms.Button();
            this.statusStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.TabContainer.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.StatusLabel});
            this.statusStrip1.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.Table;
            this.statusStrip1.Location = new System.Drawing.Point(0, 659);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(919, 23);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // StatusLabel
            // 
            this.StatusLabel.Name = "StatusLabel";
            this.StatusLabel.Text = "0/512 Zeilen";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.bDeleteAllTabs);
            this.panel1.Controls.Add(this.nNewDivideSpecialTab);
            this.panel1.Controls.Add(this.bMultiplyResult);
            this.panel1.Controls.Add(this.tbFactor);
            this.panel1.Controls.Add(this.bNewDivideTab);
            this.panel1.Controls.Add(this.bNewMinusTab);
            this.panel1.Controls.Add(this.bNewPlusTab);
            this.panel1.Controls.Add(this.bErgebnis);
            this.panel1.Controls.Add(this.bDeleteTab);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(919, 29);
            this.panel1.TabIndex = 5;
            // 
            // nNewDivideSpecialTab
            // 
            this.nNewDivideSpecialTab.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.nNewDivideSpecialTab.Location = new System.Drawing.Point(246, 3);
            this.nNewDivideSpecialTab.Name = "nNewDivideSpecialTab";
            this.nNewDivideSpecialTab.Size = new System.Drawing.Size(115, 23);
            this.nNewDivideSpecialTab.TabIndex = 8;
            this.nNewDivideSpecialTab.Text = "Neuer /SpecialTab";
            this.nNewDivideSpecialTab.Click += new System.EventHandler(this.nNewDivideSpecialTab_Click);
            // 
            // bMultiplyResult
            // 
            this.bMultiplyResult.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bMultiplyResult.Location = new System.Drawing.Point(851, 3);
            this.bMultiplyResult.Name = "bMultiplyResult";
            this.bMultiplyResult.Size = new System.Drawing.Size(56, 23);
            this.bMultiplyResult.TabIndex = 7;
            this.bMultiplyResult.Text = "*Result";
            this.bMultiplyResult.Click += new System.EventHandler(this.bMultiplyResult_Click);
            // 
            // tbFactor
            // 
            this.tbFactor.Location = new System.Drawing.Point(775, 5);
            this.tbFactor.Name = "tbFactor";
            this.tbFactor.Size = new System.Drawing.Size(70, 20);
            this.tbFactor.TabIndex = 6;
            this.tbFactor.Text = "1";
            // 
            // bNewDivideTab
            // 
            this.bNewDivideTab.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bNewDivideTab.Location = new System.Drawing.Point(165, 3);
            this.bNewDivideTab.Name = "bNewDivideTab";
            this.bNewDivideTab.Size = new System.Drawing.Size(75, 23);
            this.bNewDivideTab.TabIndex = 5;
            this.bNewDivideTab.Text = "Neuer /Tab";
            this.bNewDivideTab.Click += new System.EventHandler(this.bNewDivideTab_Click);
            // 
            // bNewMinusTab
            // 
            this.bNewMinusTab.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bNewMinusTab.Location = new System.Drawing.Point(84, 3);
            this.bNewMinusTab.Name = "bNewMinusTab";
            this.bNewMinusTab.Size = new System.Drawing.Size(75, 23);
            this.bNewMinusTab.TabIndex = 4;
            this.bNewMinusTab.Text = "Neuer -Tab";
            this.bNewMinusTab.Click += new System.EventHandler(this.bNewMinusTab_Click);
            // 
            // bNewPlusTab
            // 
            this.bNewPlusTab.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bNewPlusTab.Location = new System.Drawing.Point(3, 3);
            this.bNewPlusTab.Name = "bNewPlusTab";
            this.bNewPlusTab.Size = new System.Drawing.Size(75, 23);
            this.bNewPlusTab.TabIndex = 3;
            this.bNewPlusTab.Text = "Neuer +Tab";
            this.bNewPlusTab.Click += new System.EventHandler(this.bNewPlusTab_Click);
            // 
            // bErgebnis
            // 
            this.bErgebnis.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bErgebnis.Location = new System.Drawing.Point(667, 3);
            this.bErgebnis.Name = "bErgebnis";
            this.bErgebnis.Size = new System.Drawing.Size(75, 23);
            this.bErgebnis.TabIndex = 2;
            this.bErgebnis.Text = "Ergebnis";
            this.bErgebnis.Click += new System.EventHandler(this.bErgebnis_Click);
            // 
            // bDeleteTab
            // 
            this.bDeleteTab.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bDeleteTab.Location = new System.Drawing.Point(574, 3);
            this.bDeleteTab.Name = "bDeleteTab";
            this.bDeleteTab.Size = new System.Drawing.Size(87, 23);
            this.bDeleteTab.TabIndex = 1;
            this.bDeleteTab.Text = "Tab löschen";
            this.bDeleteTab.Click += new System.EventHandler(this.bDeleteTab_Click);
            // 
            // TabContainer
            // 
            this.TabContainer.Controls.Add(this.tabPage1);
            this.TabContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TabContainer.Location = new System.Drawing.Point(0, 29);
            this.TabContainer.Name = "TabContainer";
            this.TabContainer.SelectedIndex = 0;
            this.TabContainer.Size = new System.Drawing.Size(919, 630);
            this.TabContainer.TabIndex = 6;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.ResultDisplay);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Size = new System.Drawing.Size(911, 604);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Result";
            // 
            // ResultDisplay
            // 
            this.ResultDisplay.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ResultDisplay.Fixpunkt = new System.Drawing.Point(0, 0);
            this.ResultDisplay.Lmin = 65536;
            this.ResultDisplay.Location = new System.Drawing.Point(0, 0);
            this.ResultDisplay.Name = "ResultDisplay";
            this.ResultDisplay.Size = new System.Drawing.Size(911, 604);
            this.ResultDisplay.TabIndex = 0;
            // 
            // bDeleteAllTabs
            // 
            this.bDeleteAllTabs.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bDeleteAllTabs.Location = new System.Drawing.Point(460, 3);
            this.bDeleteAllTabs.Name = "bDeleteAllTabs";
            this.bDeleteAllTabs.Size = new System.Drawing.Size(108, 23);
            this.bDeleteAllTabs.TabIndex = 9;
            this.bDeleteAllTabs.Text = "Alle Tabs löschen";
            this.bDeleteAllTabs.Click += new System.EventHandler(this.bDeleteAllTabs_Click);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(919, 682);
            this.Controls.Add(this.TabContainer);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.statusStrip1);
            this.Name = "Main";
            this.Text = "FITWorks";
            this.statusStrip1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.TabContainer.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel StatusLabel;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TabControl TabContainer;
        private System.Windows.Forms.Button bDeleteTab;
        private System.Windows.Forms.Button bErgebnis;
        private System.Windows.Forms.Button bNewPlusTab;
        private System.Windows.Forms.Button bNewMinusTab;
        private System.Windows.Forms.TabPage tabPage1;
        private FITDisplay ResultDisplay;
        private System.Windows.Forms.Button bNewDivideTab;
        private System.Windows.Forms.Button bMultiplyResult;
        private System.Windows.Forms.TextBox tbFactor;
        private System.Windows.Forms.Button nNewDivideSpecialTab;
        private System.Windows.Forms.Button bDeleteAllTabs;
    }
}

