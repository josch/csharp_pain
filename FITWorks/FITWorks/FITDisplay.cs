using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Threading;

namespace FITWorks
{
    public partial class FITDisplay : UserControl
    {
        public string kind = "";

        private Point fixpunkt;
        public Point Fixpunkt
        {
            get { return fixpunkt; }
            set { fixpunkt = value; }
        }
        public int[,] FIT16BitGreyScale = new int[512, 512];

        private Bitmap BMP8BitGreyScale = new Bitmap(512, 512);
        private Bitmap BMPRect = new Bitmap(128, 128);

        private int minvalue = 65536;
        public int Lmin
        {
            get { return minvalue; }
            set { minvalue = value; }
        }
        private int maxvalue = 0;
        private Thread t;


        public FITDisplay()
        {
            InitializeComponent();
        }

        public FITDisplay(string Kind)
        {
            InitializeComponent();
            kind = Kind;
            if (kind == "result")
            {
                bLoad.Enabled = false;
                bSaveFIT.Enabled = true;
                lFilename.Visible = false;
            }
            if (kind == "plus" || kind=="minus" || kind=="divide" || kind=="dividespecial")
            {
                bLoad.Enabled = true;
                bSaveFIT.Enabled = false;
                lFilename.Visible = true;
            }
        }


        private void DrawBMPfromFITArray()
        {
            int Grey8Bit = 0;
            double dGrey8Bit = 0;
            int trimstart = int.Parse(tbTrimStart.Text);
            int trimend = int.Parse(tbTrimEnd.Text);
            float trimmultipler = 65536.0f / (65536.0f - ((float)trimstart + (65536.0f - (float)trimend)));

            if (rbLinear.Checked)
            {
                if (!cbTrimValues.Checked)
                {
                    for (int row = 0; row < 512; row++)
                    {
                        for (int col = 0; col < 512; col++)
                        {
                            Grey8Bit = FIT16BitGreyScale[col, row] / 0x100;
                            if (Grey8Bit > 255)
                            {
                                Grey8Bit = 255;
                            }
                            if (Grey8Bit < 0)
                            {
                                Grey8Bit = 0;
                            }
                            BMP8BitGreyScale.SetPixel(col, row, Color.FromArgb(Grey8Bit, Grey8Bit, Grey8Bit));
                        }
                    }
                }
                else
                {
                    for (int row = 0; row < 512; row++)
                    {
                        for (int col = 0; col < 512; col++)
                        {
                            Grey8Bit = ((FIT16BitGreyScale[col, row] - trimstart) * (int)trimmultipler) / 0x100; //minvalue ist falsch da geh�rt trimstart hin
                            if (Grey8Bit > 255)
                            {
                                Grey8Bit = 255;
                            }
                            if (Grey8Bit < 0)
                            {
                                Grey8Bit = 0;
                            }
                            BMP8BitGreyScale.SetPixel(col, row, Color.FromArgb(Grey8Bit, Grey8Bit, Grey8Bit));
                        }
                    }
                }
            }
            else if (rbExponentiell.Checked)
            {
                if (!cbTrimValues.Checked)
                {
                    for (int row = 0; row < 512; row++)
                    {
                        for (int col = 0; col < 512; col++)
                        {
                            dGrey8Bit = Math.Pow(float.Parse(tbBase.Text), FIT16BitGreyScale[col, row]) / 0x100;
                            if (Grey8Bit > 255)
                            {
                                Grey8Bit = 255;
                            }
                            if (Grey8Bit < 0)
                            {
                                Grey8Bit = 0;
                            }
                            BMP8BitGreyScale.SetPixel(col, row, Color.FromArgb((int)dGrey8Bit, (int)dGrey8Bit, (int)dGrey8Bit));
                        }
                    }
                }
                else
                {
                    for (int row = 0; row < 512; row++)
                    {
                        for (int col = 0; col < 512; col++)
                        {
                            dGrey8Bit = Math.Pow(float.Parse(tbBase.Text), (FIT16BitGreyScale[col, row] - minvalue) * (int)trimmultipler) / 0x100;
                            if (Grey8Bit > 255)
                            {
                                Grey8Bit = 255;
                            }
                            if (Grey8Bit < 0)
                            {
                                Grey8Bit = 0;
                            }
                            BMP8BitGreyScale.SetPixel(col, row, Color.FromArgb((int)dGrey8Bit, (int)dGrey8Bit, (int)dGrey8Bit));
                        }
                    }
                }
            }
            pbMain.Image = BMP8BitGreyScale;
        }

        private void bLoad_Click(object sender, EventArgs e)
        {
            if (openFITFileDialog.ShowDialog() == DialogResult.OK)
            {
                parseFITintoArray(openFITFileDialog.FileName);

                lFilename.Text = openFITFileDialog.FileName.Split('\\')[openFITFileDialog.FileName.Split('\\').Length - 1];

                t = new Thread(new ThreadStart(DrawBMPfromFITArray));
                t.Start();
            }
        }

        private void pbMain_MouseMove(object sender, MouseEventArgs e)
        {
            try
            {
                lx.Text = "x = " + e.X;
                ly.Text = "y = " + e.Y;
                lL.Text = "L = " + FIT16BitGreyScale[e.X, e.Y];
                int X = e.X;
                int Y = e.Y;
                if (X < 64 / int.Parse(cbZoomFactor.Text))
                    X = 64 / int.Parse(cbZoomFactor.Text);
                if (Y < 64 / int.Parse(cbZoomFactor.Text))
                    Y = 64 / int.Parse(cbZoomFactor.Text);
                if (X > 512 - 64 / int.Parse(cbZoomFactor.Text))
                    X = 512 - 64 / int.Parse(cbZoomFactor.Text);
                if (Y > 512 - 64 / int.Parse(cbZoomFactor.Text))
                    Y = 512 - 64 / int.Parse(cbZoomFactor.Text);
                pbZoom.Image = BMP8BitGreyScale.Clone(new Rectangle(X - 64 / int.Parse(cbZoomFactor.Text), Y - 64 / int.Parse(cbZoomFactor.Text), 128 / int.Parse(cbZoomFactor.Text), 128 / int.Parse(cbZoomFactor.Text)), PixelFormat.Format32bppArgb);
            }
            catch { };
        }


        private void bAktualisieren_Click(object sender, EventArgs e)
        {
            DrawBMPfromFITArray();
        }

        private void pbMain_MouseClick(object sender, MouseEventArgs e)
        {
            fixpunkt.X = e.X;
            fixpunkt.Y = e.Y;
            lxf.Text = "xf = " + e.X;
            lyf.Text = "yf = " + e.Y;
        }

        private void bSaveFIT_Click(object sender, EventArgs e)
        {
            if (saveFITFileDialog.ShowDialog() == DialogResult.OK)
            {
                string header = "SIMPLE  =                    T / file does conform to FITS standard             BITPIX  =                   16 / number of bits per data pixel                  NAXIS   =                    2 / number of data axes                            NAXIS1  =                  512 / length of data axis 1                          NAXIS2  =                  512 / length of data axis 2                          EXTEND  =                    T / FITS dataset may contain extensions            COMMENT   FITS (Flexible Image Transport System) format defined in Astronomy andCOMMENT   Astrophysics Supplement Series v44/p363, v44/p371, v73/p359, v73/p365.COMMENT   Contact the NASA Science Office of Standards and Technology for the   COMMENT   FITS Definition document #100 and other FITS information.             BZERO   =               32768. / DATA ZERO                                      BSCALE  =                   1. / DATA SCALE                                     EXPOSURE=                   0. / EXPOSURE IN SECONDS                            HBIN    =                    2 / HORIZONTAL BIN FACTOR                          VBIN    =                    2 / VERTICAL BIN FACTOR                            DATE    = '2004-12-14T22:28:48' / file creation date (YYYY-MM-DDThh:mm:ss UTC)  END";
                FileStream FITStream = new FileStream(saveFITFileDialog.FileName, FileMode.Create);
                byte[] FITFile = new byte[529920];
                byte[] bheader = Encoding.ASCII.GetBytes(header);
                bheader.CopyTo(FITFile, 0);
                for (int i = 2880; i < 527168; i += 2)
                {
                    int Row = ((i - 2880) / 2) / 512;
                    int Col = ((i - 2880) / 2) - Row * 512;
                    int Grey16Bit = FIT16BitGreyScale[Col, 511 - Row];
                    if (Grey16Bit > 65535)
                        Grey16Bit = 65535;
                    if (Grey16Bit < 0)
                        Grey16Bit = 0;
                    if (Grey16Bit >= 32768)
                    {
                        Grey16Bit = Grey16Bit - 32768;
                    }
                    else
                    {
                        Grey16Bit = Grey16Bit + 32768;
                    }
                    int Grey16BitFirstByte = Grey16Bit / 0x100;
                    int Grey16BitSecondByte = Grey16Bit - Grey16BitFirstByte*0x100;
                    FITFile[i] = (byte)Grey16BitFirstByte;
                    FITFile[i + 1] = (byte)Grey16BitSecondByte;
                }
                FITStream.Write(FITFile, 0, FITFile.Length);
                FITStream.Close();
            }
        }

        private void parseFITintoArray(string path)
        {
            minvalue = 65536;
            maxvalue = 0;

            FileStream FITStream = new FileStream(path, FileMode.Open);
            byte[] FITFile = new byte[FITStream.Length];
            FITStream.Read(FITFile, 0, FITFile.Length);
            FITStream.Close();

            for (int i = 2880; i < 527168; i += 2)
            {
                int Grey16Bit = (FITFile[i] * 0x100) + FITFile[i + 1];
                if (Grey16Bit >= 32768)
                {
                    Grey16Bit = Grey16Bit - 32768;
                }
                else
                {
                    Grey16Bit = Grey16Bit + 32768;
                }

                int Row = ((i - 2880) / 2) / 512;
                int Col = ((i - 2880) / 2) - Row * 512;
                FIT16BitGreyScale[Col, 511 - Row] = Grey16Bit;
                if (Grey16Bit < minvalue)
                {
                    minvalue = Grey16Bit;
                }
                if (Grey16Bit > maxvalue)
                {
                    maxvalue = Grey16Bit;
                }
            }

            lLmax.Text = "L max = " + maxvalue;
            lLmin.Text = "L min = " + minvalue;
        }

        private void bSaveBMP_Click(object sender, EventArgs e)
        {
            if (saveBMPFileDialog.ShowDialog() == DialogResult.OK)
            {
                BMP8BitGreyScale.Save(saveBMPFileDialog.FileName, ImageFormat.Bmp);
            }
        }
    }
}
