using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace FITWorks
{
    public partial class Main : Form
    {
        public Main()
        {
            InitializeComponent();
        }

        private void bNewResultTab_Click(object sender, EventArgs e)
        {
            TabPage tp = new TabPage("=TabPage "+(TabContainer.TabPages.Count + 1).ToString());
            FITDisplay fd = new FITDisplay("result");
            fd.Dock = DockStyle.Fill;
            tp.Controls.Add(fd);
            TabContainer.TabPages.Add(tp);
        }

        private void bDeleteTab_Click(object sender, EventArgs e)
        {
            if (TabContainer.SelectedTab.Text != "Result")
            {
                TabContainer.SelectedTab.Dispose();
            }
        }

        private void bDeleteAllTabs_Click(object sender, EventArgs e)
        {
            while (TabContainer.TabCount > 1)
            {
                TabContainer.TabPages[TabContainer.TabCount - 1].Dispose();
            }
        }

        private void bNewPlusTab_Click(object sender, EventArgs e)
        {
                TabPage tp = new TabPage("+TabPage " + (TabContainer.TabPages.Count).ToString());
                FITDisplay fd = new FITDisplay("plus");
                fd.Dock = DockStyle.Fill;
                tp.Controls.Add(fd);
                TabContainer.TabPages.Add(tp);
        }

        private void bNewMinusTab_Click(object sender, EventArgs e)
        {
            if (TabContainer.TabCount < 3)
            {
                TabPage tp = new TabPage("-TabPage " + (TabContainer.TabPages.Count).ToString());
                FITDisplay fd = new FITDisplay("minus");
                fd.Dock = DockStyle.Fill;
                tp.Controls.Add(fd);
                TabContainer.TabPages.Add(tp);
            }
        }

        private void bNewDivideTab_Click(object sender, EventArgs e)
        {
            if (TabContainer.TabCount < 3)
            {
                TabPage tp = new TabPage("/TabPage " + (TabContainer.TabPages.Count).ToString());
                FITDisplay fd = new FITDisplay("divide");
                fd.Dock = DockStyle.Fill;
                tp.Controls.Add(fd);
                TabContainer.TabPages.Add(tp);
            }
        }

        private void nNewDivideSpecialTab_Click(object sender, EventArgs e)
        {
            if (TabContainer.TabCount < 3)
            {
                TabPage tp = new TabPage("/SpecialTabPage " + (TabContainer.TabPages.Count).ToString());
                FITDisplay fd = new FITDisplay("dividespecial");
                fd.Dock = DockStyle.Fill;
                tp.Controls.Add(fd);
                TabContainer.TabPages.Add(tp);
            }
        }

        private void bErgebnis_Click(object sender, EventArgs e)
        {
            int minx = 512;
            int miny = 512;
            int minx2 = 512;
            int miny2 = 512;
            int pluscount = 0;

            for (int i = 0; i < 512; i++)
                for (int j = 0; j < 512; j++)
                    ResultDisplay.FIT16BitGreyScale[i, j]=0;

            for (int i = 1; i < TabContainer.TabCount; i++)
            {
                FITDisplay fd = (FITDisplay)TabContainer.TabPages[i].Controls[0];
                if (fd.Fixpunkt.X < minx)
                    minx = fd.Fixpunkt.X;
                if (fd.Fixpunkt.Y < miny)
                    miny = fd.Fixpunkt.Y;
                if (512 - fd.Fixpunkt.X < minx2)
                    minx2 = 512 - fd.Fixpunkt.X;
                if (512 - fd.Fixpunkt.Y < miny2)
                    miny2 = 512 - fd.Fixpunkt.Y;
                if (fd.kind == "plus")
                    pluscount++;
            }



            for(int k = 1; k<TabContainer.TabCount; k++)
            {
                FITDisplay fd = (FITDisplay)TabContainer.TabPages[k].Controls[0];
                if(fd.kind == "plus")
                {
                    for (int i = fd.Fixpunkt.X - minx; i < fd.Fixpunkt.X + minx2; i++)
                    {
                        for (int j = fd.Fixpunkt.Y - miny; j < fd.Fixpunkt.Y + miny2; j++)
                        {
                            ResultDisplay.FIT16BitGreyScale[i - (fd.Fixpunkt.X - minx), j - (fd.Fixpunkt.Y - miny)] += fd.FIT16BitGreyScale[i, j] / pluscount;
                        }
                    }
                }
                else if(fd.kind == "minus")
                {
                    for (int i = fd.Fixpunkt.X-minx; i < fd.Fixpunkt.X+minx2; i++)
                    {
                        for (int j = fd.Fixpunkt.Y-miny; j < fd.Fixpunkt.Y+miny2; j++)
                        {
                            ResultDisplay.FIT16BitGreyScale[i - (fd.Fixpunkt.X - minx), j - (fd.Fixpunkt.Y - miny)] -= fd.FIT16BitGreyScale[i, j];
                        }
                    }
                }
                else if (fd.kind == "divide")
                {
                    for (int i = fd.Fixpunkt.X - minx; i < fd.Fixpunkt.X + minx2; i++)
                    {
                        for (int j = fd.Fixpunkt.Y - miny; j < fd.Fixpunkt.Y + miny2; j++)
                        {
                            ResultDisplay.FIT16BitGreyScale[i - (fd.Fixpunkt.X - minx), j - (fd.Fixpunkt.Y - miny)] /= fd.FIT16BitGreyScale[i, j];
                        }
                    }
                }
                else if (fd.kind == "dividespecial")
                {
                    for (int i = fd.Fixpunkt.X - minx; i < fd.Fixpunkt.X + minx2; i++)
                    {
                        for (int j = fd.Fixpunkt.Y - miny; j < fd.Fixpunkt.Y + miny2; j++)
                        {
                            ResultDisplay.FIT16BitGreyScale[i - (fd.Fixpunkt.X - minx), j - (fd.Fixpunkt.Y - miny)] = (ResultDisplay.FIT16BitGreyScale[i - (fd.Fixpunkt.X - minx), j - (fd.Fixpunkt.Y - miny)]*fd.Lmin) / fd.FIT16BitGreyScale[i, j];
                        }
                    }
                }
            }
        }

        private void bMultiplyResult_Click(object sender, EventArgs e)
        {
            decimal factor = decimal.Parse(tbFactor.Text);
            for (int i = 0; i < 512; i++)
            {
                for (int j = 0; j < 512; j++)
                {
                    ResultDisplay.FIT16BitGreyScale[i, j] = (int)(ResultDisplay.FIT16BitGreyScale[i, j]*factor);
                }
            }
        }
    }
}
