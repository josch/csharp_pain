namespace FITWorks
{
    partial class FITDisplay
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel2 = new System.Windows.Forms.Panel();
            this.lFilename = new System.Windows.Forms.Label();
            this.bSaveFIT = new System.Windows.Forms.Button();
            this.bLoad = new System.Windows.Forms.Button();
            this.bAktualisieren = new System.Windows.Forms.Button();
            this.bSaveBMP = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.ldL = new System.Windows.Forms.Label();
            this.tbTrimEnd = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tbTrimStart = new System.Windows.Forms.TextBox();
            this.cbTrimValues = new System.Windows.Forms.CheckBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.lLmax = new System.Windows.Forms.Label();
            this.lLmin = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tbBase = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.rbExponentiell = new System.Windows.Forms.RadioButton();
            this.rbLinear = new System.Windows.Forms.RadioButton();
            this.panel4 = new System.Windows.Forms.Panel();
            this.gbFixpunkt = new System.Windows.Forms.GroupBox();
            this.lyf = new System.Windows.Forms.Label();
            this.lxf = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.label9 = new System.Windows.Forms.Label();
            this.cbZoomFactor = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.lL = new System.Windows.Forms.Label();
            this.pbZoom = new System.Windows.Forms.PictureBox();
            this.lx = new System.Windows.Forms.Label();
            this.ly = new System.Windows.Forms.Label();
            this.pbMain = new System.Windows.Forms.PictureBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.openFITFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.saveFITFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.saveBMPFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel4.SuspendLayout();
            this.gbFixpunkt.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbZoom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbMain)).BeginInit();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.lFilename);
            this.panel2.Controls.Add(this.bSaveFIT);
            this.panel2.Controls.Add(this.bLoad);
            this.panel2.Controls.Add(this.bAktualisieren);
            this.panel2.Controls.Add(this.bSaveBMP);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(713, 28);
            this.panel2.TabIndex = 4;
            // 
            // lFilename
            // 
            this.lFilename.AutoSize = true;
            this.lFilename.Location = new System.Drawing.Point(502, 7);
            this.lFilename.Name = "lFilename";
            this.lFilename.Size = new System.Drawing.Size(54, 13);
            this.lFilename.TabIndex = 8;
            this.lFilename.Text = "Dateiname";
            this.lFilename.Visible = false;
            // 
            // bSaveFIT
            // 
            this.bSaveFIT.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bSaveFIT.Location = new System.Drawing.Point(183, 2);
            this.bSaveFIT.Name = "bSaveFIT";
            this.bSaveFIT.Size = new System.Drawing.Size(94, 23);
            this.bSaveFIT.TabIndex = 7;
            this.bSaveFIT.Text = "FIT speichern";
            this.bSaveFIT.Click += new System.EventHandler(this.bSaveFIT_Click);
            // 
            // bLoad
            // 
            this.bLoad.Enabled = false;
            this.bLoad.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bLoad.Location = new System.Drawing.Point(3, 2);
            this.bLoad.Name = "bLoad";
            this.bLoad.Size = new System.Drawing.Size(75, 23);
            this.bLoad.TabIndex = 6;
            this.bLoad.Text = "FIT Laden";
            this.bLoad.Click += new System.EventHandler(this.bLoad_Click);
            // 
            // bAktualisieren
            // 
            this.bAktualisieren.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bAktualisieren.Location = new System.Drawing.Point(283, 2);
            this.bAktualisieren.Name = "bAktualisieren";
            this.bAktualisieren.Size = new System.Drawing.Size(75, 23);
            this.bAktualisieren.TabIndex = 4;
            this.bAktualisieren.Text = "Aktualisieren";
            this.bAktualisieren.Click += new System.EventHandler(this.bAktualisieren_Click);
            // 
            // bSaveBMP
            // 
            this.bSaveBMP.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bSaveBMP.Location = new System.Drawing.Point(84, 2);
            this.bSaveBMP.Name = "bSaveBMP";
            this.bSaveBMP.Size = new System.Drawing.Size(93, 23);
            this.bSaveBMP.TabIndex = 3;
            this.bSaveBMP.Text = "BMP speichern";
            this.bSaveBMP.Click += new System.EventHandler(this.bSaveBMP_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.groupBox4);
            this.panel1.Controls.Add(this.groupBox3);
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 28);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(127, 441);
            this.panel1.TabIndex = 8;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.ldL);
            this.groupBox4.Controls.Add(this.tbTrimEnd);
            this.groupBox4.Controls.Add(this.label1);
            this.groupBox4.Controls.Add(this.tbTrimStart);
            this.groupBox4.Controls.Add(this.cbTrimValues);
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox4.Location = new System.Drawing.Point(0, 157);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(127, 122);
            this.groupBox4.TabIndex = 3;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Trim L";
            // 
            // ldL
            // 
            this.ldL.AutoSize = true;
            this.ldL.Location = new System.Drawing.Point(6, 104);
            this.ldL.Name = "ldL";
            this.ldL.Size = new System.Drawing.Size(25, 13);
            this.ldL.TabIndex = 6;
            this.ldL.Text = "ΔL =";
            // 
            // tbTrimEnd
            // 
            this.tbTrimEnd.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbTrimEnd.Location = new System.Drawing.Point(6, 81);
            this.tbTrimEnd.Name = "tbTrimEnd";
            this.tbTrimEnd.Size = new System.Drawing.Size(115, 20);
            this.tbTrimEnd.TabIndex = 5;
            this.tbTrimEnd.Text = "0";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 65);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(27, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "< L <";
            // 
            // tbTrimStart
            // 
            this.tbTrimStart.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbTrimStart.Location = new System.Drawing.Point(6, 42);
            this.tbTrimStart.Name = "tbTrimStart";
            this.tbTrimStart.Size = new System.Drawing.Size(115, 20);
            this.tbTrimStart.TabIndex = 3;
            this.tbTrimStart.Text = "0";
            // 
            // cbTrimValues
            // 
            this.cbTrimValues.AutoSize = true;
            this.cbTrimValues.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbTrimValues.Location = new System.Drawing.Point(6, 19);
            this.cbTrimValues.Name = "cbTrimValues";
            this.cbTrimValues.Size = new System.Drawing.Size(74, 17);
            this.cbTrimValues.TabIndex = 2;
            this.cbTrimValues.Text = "Trim Values";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.lLmax);
            this.groupBox3.Controls.Add(this.lLmin);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox3.Location = new System.Drawing.Point(0, 94);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(127, 63);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Eigenschaften";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 42);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(29, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "L Ø =";
            // 
            // lLmax
            // 
            this.lLmax.AutoSize = true;
            this.lLmax.Location = new System.Drawing.Point(6, 29);
            this.lLmax.Name = "lLmax";
            this.lLmax.Size = new System.Drawing.Size(40, 13);
            this.lLmax.TabIndex = 1;
            this.lLmax.Text = "L max =";
            // 
            // lLmin
            // 
            this.lLmin.AutoSize = true;
            this.lLmin.Location = new System.Drawing.Point(6, 16);
            this.lLmin.Name = "lLmin";
            this.lLmin.Size = new System.Drawing.Size(37, 13);
            this.lLmin.TabIndex = 0;
            this.lLmin.Text = "L min =";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tbBase);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.rbExponentiell);
            this.groupBox1.Controls.Add(this.rbLinear);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(127, 94);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Werte Anzeige";
            // 
            // tbBase
            // 
            this.tbBase.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbBase.Location = new System.Drawing.Point(30, 65);
            this.tbBase.Name = "tbBase";
            this.tbBase.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.tbBase.Size = new System.Drawing.Size(91, 20);
            this.tbBase.TabIndex = 4;
            this.tbBase.Text = "1,00068";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 67);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(18, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "n =";
            // 
            // rbExponentiell
            // 
            this.rbExponentiell.AutoSize = true;
            this.rbExponentiell.Location = new System.Drawing.Point(6, 42);
            this.rbExponentiell.Name = "rbExponentiell";
            this.rbExponentiell.Size = new System.Drawing.Size(105, 17);
            this.rbExponentiell.TabIndex = 1;
            this.rbExponentiell.TabStop = false;
            this.rbExponentiell.Text = "Exponentiell (n^L)";
            // 
            // rbLinear
            // 
            this.rbLinear.AutoSize = true;
            this.rbLinear.Checked = true;
            this.rbLinear.Location = new System.Drawing.Point(6, 19);
            this.rbLinear.Name = "rbLinear";
            this.rbLinear.Size = new System.Drawing.Size(50, 17);
            this.rbLinear.TabIndex = 0;
            this.rbLinear.Text = "Linear";
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.gbFixpunkt);
            this.panel4.Controls.Add(this.groupBox2);
            this.panel4.Controls.Add(this.panel6);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(561, 28);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(152, 441);
            this.panel4.TabIndex = 9;
            // 
            // gbFixpunkt
            // 
            this.gbFixpunkt.Controls.Add(this.lyf);
            this.gbFixpunkt.Controls.Add(this.lxf);
            this.gbFixpunkt.Dock = System.Windows.Forms.DockStyle.Top;
            this.gbFixpunkt.Location = new System.Drawing.Point(0, 279);
            this.gbFixpunkt.Name = "gbFixpunkt";
            this.gbFixpunkt.Size = new System.Drawing.Size(152, 52);
            this.gbFixpunkt.TabIndex = 7;
            this.gbFixpunkt.TabStop = false;
            this.gbFixpunkt.Text = "Fixpunkt";
            // 
            // lyf
            // 
            this.lyf.AutoSize = true;
            this.lyf.Location = new System.Drawing.Point(6, 29);
            this.lyf.Name = "lyf";
            this.lyf.Size = new System.Drawing.Size(20, 13);
            this.lyf.TabIndex = 1;
            this.lyf.Text = "yf =";
            // 
            // lxf
            // 
            this.lxf.AutoSize = true;
            this.lxf.Location = new System.Drawing.Point(6, 16);
            this.lxf.Name = "lxf";
            this.lxf.Size = new System.Drawing.Size(20, 13);
            this.lxf.TabIndex = 0;
            this.lxf.Text = "xf =";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox2.Location = new System.Drawing.Point(0, 217);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(152, 62);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Eigenschaften Ausschnitt";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 42);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(29, 13);
            this.label7.TabIndex = 2;
            this.label7.Text = "L Ø =";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 29);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(40, 13);
            this.label6.TabIndex = 1;
            this.label6.Text = "L max =";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 16);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(37, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "L min =";
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.label9);
            this.panel6.Controls.Add(this.cbZoomFactor);
            this.panel6.Controls.Add(this.label8);
            this.panel6.Controls.Add(this.lL);
            this.panel6.Controls.Add(this.pbZoom);
            this.panel6.Controls.Add(this.lx);
            this.panel6.Controls.Add(this.ly);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel6.Location = new System.Drawing.Point(0, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(152, 217);
            this.panel6.TabIndex = 5;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(113, 148);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(24, 13);
            this.label9.TabIndex = 6;
            this.label9.Text = "fach";
            // 
            // cbZoomFactor
            // 
            this.cbZoomFactor.FormattingEnabled = true;
            this.cbZoomFactor.Items.AddRange(new object[] {
            "1",
            "2",
            "4",
            "8",
            "16"});
            this.cbZoomFactor.Location = new System.Drawing.Point(56, 145);
            this.cbZoomFactor.Name = "cbZoomFactor";
            this.cbZoomFactor.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.cbZoomFactor.Size = new System.Drawing.Size(51, 21);
            this.cbZoomFactor.TabIndex = 5;
            this.cbZoomFactor.Text = "1";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(11, 148);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(39, 13);
            this.label8.TabIndex = 4;
            this.label8.Text = "Zoom =";
            // 
            // lL
            // 
            this.lL.AutoSize = true;
            this.lL.Location = new System.Drawing.Point(11, 197);
            this.lL.Name = "lL";
            this.lL.Size = new System.Drawing.Size(18, 13);
            this.lL.TabIndex = 3;
            this.lL.Text = "L =";
            // 
            // pbZoom
            // 
            this.pbZoom.Location = new System.Drawing.Point(12, 11);
            this.pbZoom.Name = "pbZoom";
            this.pbZoom.Size = new System.Drawing.Size(128, 128);
            this.pbZoom.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbZoom.TabIndex = 0;
            this.pbZoom.TabStop = false;
            // 
            // lx
            // 
            this.lx.AutoSize = true;
            this.lx.Location = new System.Drawing.Point(11, 171);
            this.lx.Name = "lx";
            this.lx.Size = new System.Drawing.Size(17, 13);
            this.lx.TabIndex = 1;
            this.lx.Text = "x =";
            // 
            // ly
            // 
            this.ly.AutoSize = true;
            this.ly.Location = new System.Drawing.Point(11, 184);
            this.ly.Name = "ly";
            this.ly.Size = new System.Drawing.Size(17, 13);
            this.ly.TabIndex = 2;
            this.ly.Text = "y =";
            // 
            // pbMain
            // 
            this.pbMain.Cursor = System.Windows.Forms.Cursors.Cross;
            this.pbMain.Location = new System.Drawing.Point(3, 4);
            this.pbMain.Name = "pbMain";
            this.pbMain.Size = new System.Drawing.Size(512, 512);
            this.pbMain.TabIndex = 10;
            this.pbMain.TabStop = false;
            this.pbMain.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pbMain_MouseMove);
            this.pbMain.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pbMain_MouseClick);
            // 
            // panel3
            // 
            this.panel3.AutoScroll = true;
            this.panel3.Controls.Add(this.pbMain);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(127, 28);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(434, 441);
            this.panel3.TabIndex = 11;
            // 
            // openFITFileDialog
            // 
            this.openFITFileDialog.Filter = "FIT files|*.fit|All files|*.*";
            // 
            // saveFITFileDialog
            // 
            this.saveFITFileDialog.Filter = "FIT files (*.fit)|*.fit|All files (*.*)|*.*";
            // 
            // saveBMPFileDialog
            // 
            this.saveBMPFileDialog.Filter = "Bitmaps (*.bmp)|*.bmp|All Files (*.*)|*.*";
            // 
            // FITDisplay
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel2);
            this.Name = "FITDisplay";
            this.Size = new System.Drawing.Size(713, 469);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.gbFixpunkt.ResumeLayout(false);
            this.gbFixpunkt.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbZoom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbMain)).EndInit();
            this.panel3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button bAktualisieren;
        private System.Windows.Forms.Button bSaveBMP;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label ldL;
        private System.Windows.Forms.TextBox tbTrimEnd;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbTrimStart;
        private System.Windows.Forms.CheckBox cbTrimValues;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lLmax;
        private System.Windows.Forms.Label lLmin;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox tbBase;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RadioButton rbExponentiell;
        private System.Windows.Forms.RadioButton rbLinear;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox cbZoomFactor;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lL;
        private System.Windows.Forms.PictureBox pbZoom;
        private System.Windows.Forms.Label lx;
        private System.Windows.Forms.Label ly;
        private System.Windows.Forms.PictureBox pbMain;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button bLoad;
        private System.Windows.Forms.OpenFileDialog openFITFileDialog;
        private System.Windows.Forms.Button bSaveFIT;
        private System.Windows.Forms.GroupBox gbFixpunkt;
        private System.Windows.Forms.Label lyf;
        private System.Windows.Forms.Label lxf;
        private System.Windows.Forms.SaveFileDialog saveFITFileDialog;
        private System.Windows.Forms.Label lFilename;
        private System.Windows.Forms.SaveFileDialog saveBMPFileDialog;
    }
}
