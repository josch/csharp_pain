using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace Editor
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {
            int firstCharIndexOfCurrentLine = richTextBox1.GetFirstCharIndexOfCurrentLine();
            int currentLine = richTextBox1.GetLineFromCharIndex(firstCharIndexOfCurrentLine);
            int firstCharIndexOfNextLine = richTextBox1.GetFirstCharIndexFromLine(currentLine + 1);
            int lastCharIndexOfCurrentLine = firstCharIndexOfNextLine - 1;
            if (firstCharIndexOfNextLine == -1)
            {
                lastCharIndexOfCurrentLine = richTextBox1.Text.Length;
            }

            string line = richTextBox1.Text.Substring(firstCharIndexOfCurrentLine, lastCharIndexOfCurrentLine - firstCharIndexOfCurrentLine);

            Regex r = new Regex("([ \\t{}();])");
            string[] tokens = r.Split(line);
            int index = firstCharIndexOfCurrentLine;
            foreach (string token in tokens)
            {
                richTextBox1.SelectionStart = index;
                richTextBox1.SelectionLength = token.Length;
                richTextBox1.SelectionBackColor = Color.White;
                if (token == "blah")
                {
                    richTextBox1.SelectionBackColor = Color.Red;
                }
                index += token.Length;
            }
        }

        private void richTextBox1_Click(object sender, EventArgs e)
        {
            //toolStripStatusLabel1.Text = richTextBox1.GetLineFromCharIndex(richTextBox1.GetFirstCharIndexOfCurrentLine()).ToString();
        }

        private void richTextBox1_KeyUp(object sender, KeyEventArgs e)
        {
            //toolStripStatusLabel1.Text = richTextBox1.GetLineFromCharIndex(richTextBox1.GetFirstCharIndexOfCurrentLine()).ToString();
        }
    }
}