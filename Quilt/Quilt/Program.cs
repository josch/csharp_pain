using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace Quilt
{
    class Program
    {
        static void Main(string[] args)
        {
            Bitmap bm = new Bitmap("d:\\quilt.bmp");
            Random ran = new Random();
            for(int i = 0; i<bm.Width; i++)
            {
                for(int j=0; j<bm.Height; j++)
                {
                    bm.SetPixel(i, j, Color.FromArgb(ran.Next(0, 255), ran.Next(0, 255), ran.Next(0, 255)));
                }
            }
            bm.Save("d:\\quilt2.bmp");
        }
    }
}
