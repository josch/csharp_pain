/*
 * Created by SharpDevelop.
 * User: Johannes
 * Date: 28.03.2005
 * Time: 13:40
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Drawing;
using System.Windows.Forms;

namespace BitrateCalc
{
	/// <summary>
	/// Description of MainForm.
	/// </summary>
	public class MainForm : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.TextBox tbAudioBitratekBits;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label lVideoBitratekBits;
		private System.Windows.Forms.Label lAudioSizeMB;
		private System.Windows.Forms.TextBox tbLenghtmin;
		private System.Windows.Forms.Label lLenghtsec;
		private System.Windows.Forms.TextBox tbFilesizeMB;
		private System.Windows.Forms.Label lVideoSizeMB;
		private System.Windows.Forms.Label lFilesizeByte;
		public MainForm()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			
			//
			// TODO: Add constructor code after the InitializeComponent() call.
			//
		}
		
		[STAThread]
		public static void Main(string[] args)
		{
			Application.Run(new MainForm());
		}
		
		#region Windows Forms Designer generated code
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent() {
			this.lFilesizeByte = new System.Windows.Forms.Label();
			this.lVideoSizeMB = new System.Windows.Forms.Label();
			this.tbFilesizeMB = new System.Windows.Forms.TextBox();
			this.lLenghtsec = new System.Windows.Forms.Label();
			this.tbLenghtmin = new System.Windows.Forms.TextBox();
			this.lAudioSizeMB = new System.Windows.Forms.Label();
			this.lVideoBitratekBits = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.tbAudioBitratekBits = new System.Windows.Forms.TextBox();
			this.label5 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// lFilesizeByte
			// 
			this.lFilesizeByte.Location = new System.Drawing.Point(304, 40);
			this.lFilesizeByte.Name = "lFilesizeByte";
			this.lFilesizeByte.Size = new System.Drawing.Size(136, 23);
			this.lFilesizeByte.TabIndex = 10;
			this.lFilesizeByte.Text = "(- byte)";
			this.lFilesizeByte.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lVideoSizeMB
			// 
			this.lVideoSizeMB.Location = new System.Drawing.Point(192, 144);
			this.lVideoSizeMB.Name = "lVideoSizeMB";
			this.lVideoSizeMB.Size = new System.Drawing.Size(152, 24);
			this.lVideoSizeMB.TabIndex = 12;
			this.lVideoSizeMB.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// tbFilesizeMB
			// 
			this.tbFilesizeMB.Location = new System.Drawing.Point(192, 40);
			this.tbFilesizeMB.Name = "tbFilesizeMB";
			this.tbFilesizeMB.TabIndex = 3;
			this.tbFilesizeMB.Text = "";
			this.tbFilesizeMB.TextChanged += new System.EventHandler(this.TbFilesizeTextChanged);
			// 
			// lLenghtsec
			// 
			this.lLenghtsec.Location = new System.Drawing.Point(304, 8);
			this.lLenghtsec.Name = "lLenghtsec";
			this.lLenghtsec.Size = new System.Drawing.Size(136, 23);
			this.lLenghtsec.TabIndex = 9;
			this.lLenghtsec.Text = "(- sek)";
			this.lLenghtsec.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// tbLenghtmin
			// 
			this.tbLenghtmin.Location = new System.Drawing.Point(192, 8);
			this.tbLenghtmin.Name = "tbLenghtmin";
			this.tbLenghtmin.TabIndex = 1;
			this.tbLenghtmin.Text = "";
			this.tbLenghtmin.TextChanged += new System.EventHandler(this.TbLenghtminTextChanged);
			// 
			// lAudioSizeMB
			// 
			this.lAudioSizeMB.Location = new System.Drawing.Point(192, 112);
			this.lAudioSizeMB.Name = "lAudioSizeMB";
			this.lAudioSizeMB.Size = new System.Drawing.Size(152, 24);
			this.lAudioSizeMB.TabIndex = 11;
			this.lAudioSizeMB.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lVideoBitratekBits
			// 
			this.lVideoBitratekBits.Location = new System.Drawing.Point(192, 176);
			this.lVideoBitratekBits.Name = "lVideoBitratekBits";
			this.lVideoBitratekBits.Size = new System.Drawing.Size(152, 24);
			this.lVideoBitratekBits.TabIndex = 13;
			this.lVideoBitratekBits.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(8, 72);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(184, 24);
			this.label3.TabIndex = 4;
			this.label3.Text = "Bitrate des Audiostreams in kBit/s:";
			this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// tbAudioBitratekBits
			// 
			this.tbAudioBitratekBits.Location = new System.Drawing.Point(192, 72);
			this.tbAudioBitratekBits.Name = "tbAudioBitratekBits";
			this.tbAudioBitratekBits.TabIndex = 5;
			this.tbAudioBitratekBits.Text = "128";
			this.tbAudioBitratekBits.TextChanged += new System.EventHandler(this.TbAudioBitratekBitsTextChanged);
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(8, 144);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(184, 24);
			this.label5.TabIndex = 7;
			this.label5.Text = "Größe des Videostreams in MB:";
			this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// label6
			// 
			this.label6.Location = new System.Drawing.Point(8, 176);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(184, 24);
			this.label6.TabIndex = 8;
			this.label6.Text = "Bitrate des Videostreams in kBit/s:";
			this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(48, 8);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(144, 24);
			this.label1.TabIndex = 0;
			this.label1.Text = "Länge des Films in Minuten:";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(56, 40);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(136, 24);
			this.label2.TabIndex = 2;
			this.label2.Text = "Zielgröße in MB:";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(8, 112);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(184, 24);
			this.label4.TabIndex = 6;
			this.label4.Text = "Größe des Audiostreams in MB:";
			this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// BitrateCalc
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(448, 205);
			this.Controls.Add(this.lVideoBitratekBits);
			this.Controls.Add(this.lVideoSizeMB);
			this.Controls.Add(this.lAudioSizeMB);
			this.Controls.Add(this.lFilesizeByte);
			this.Controls.Add(this.lLenghtsec);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.tbAudioBitratekBits);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.tbFilesizeMB);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.tbLenghtmin);
			this.Controls.Add(this.label1);
			this.Name = "BitrateCalc";
			this.Text = "BitrateCalc";
			this.ResumeLayout(false);
		}
		#endregion
		
		void TbLenghtminTextChanged(object sender, System.EventArgs e)
		{
			if(tbLenghtmin.Text!="")
			{
				lLenghtsec.Text = "("+Convert.ToString(Convert.ToDecimal(tbLenghtmin.Text)*60)+" sek)";
				CalculateAudioStream();
				CalculateVideoStream();
				CalculateVideoBitrate();
			} else {
				lLenghtsec.Text = "(- sek)";
				lAudioSizeMB.Text = "";
				lVideoSizeMB.Text = "";
				lVideoBitratekBits.Text = "";
			}
		}
		
		void TbFilesizeTextChanged(object sender, System.EventArgs e)
		{
			if(tbFilesizeMB.Text!="")
			{
				lFilesizeByte.Text = "("+Convert.ToString(Convert.ToDecimal(tbFilesizeMB.Text)*1048576)+" byte)";
				CalculateAudioStream();
				CalculateVideoStream();
				CalculateVideoBitrate();
			} else {
				lFilesizeByte.Text = "(- byte)";
				lAudioSizeMB.Text = "";
				lVideoSizeMB.Text = "";
				lVideoBitratekBits.Text = "";
			}
		}
		
		void TbAudioBitratekBitsTextChanged(object sender, System.EventArgs e)
		{
			if(tbAudioBitratekBits.Text!="")
			{
				CalculateAudioStream();
				CalculateVideoStream();
				CalculateVideoBitrate();
			} else {
				lAudioSizeMB.Text = "";
				lVideoSizeMB.Text = "";
				lVideoBitratekBits.Text = "";
			}
		}
		
		void CalculateAudioStream()
		{
			if(tbAudioBitratekBits.Text!=""&&tbLenghtmin.Text!="")
			{
				lAudioSizeMB.Text = Convert.ToString(Math.Round(Convert.ToDouble(tbLenghtmin.Text)*Convert.ToDouble(tbAudioBitratekBits.Text)*0.007152557373046875, 2))+" ("+Convert.ToString(Math.Round(Convert.ToDouble(tbLenghtmin.Text)*Convert.ToDouble(tbAudioBitratekBits.Text)*7500))+" byte)";
			}
		}
		
		void CalculateVideoStream()
		{
			if(tbAudioBitratekBits.Text!=""&&tbLenghtmin.Text!=""&&tbFilesizeMB.Text!="")
			{
				lVideoSizeMB.Text = Convert.ToString(Math.Round(Convert.ToDouble(tbFilesizeMB.Text)-Convert.ToDouble(tbLenghtmin.Text)*Convert.ToDouble(tbAudioBitratekBits.Text)*0.007152557373046875, 2))+" ("+Convert.ToString(Math.Round(Convert.ToDouble(tbFilesizeMB.Text)*1048576-Convert.ToDouble(tbLenghtmin.Text)*Convert.ToDouble(tbAudioBitratekBits.Text)*7500))+" byte)";
			}
		}
		
		void CalculateVideoBitrate()
		{
			if(tbAudioBitratekBits.Text!=""&&tbLenghtmin.Text!=""&&tbFilesizeMB.Text!="")
			{
				lVideoBitratekBits.Text = Convert.ToString(Math.Round((Convert.ToDouble(tbFilesizeMB.Text)*8388.608-Convert.ToDouble(tbLenghtmin.Text)*Convert.ToDouble(tbAudioBitratekBits.Text)*60)/(Convert.ToDouble(tbLenghtmin.Text)*60)));
			}
		}
	}
}
