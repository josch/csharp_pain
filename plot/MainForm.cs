/*
 * Created by SharpDevelop.
 * User: windows
 * Date: 25.01.2005
 * Time: 23:07
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Windows.Forms;

namespace plot
{
	/// <summary>
	/// Description of MainForm.	
	/// </summary>
	public class MainForm : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button button1;
		public MainForm()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			
			//
			// TODO: Add constructor code after the InitializeComponent() call.
			//
		}
		
		[STAThread]
		public static void Main(string[] args)
		{
			Application.Run(new MainForm());
		}
		
		#region Windows Forms Designer generated code
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent() {
			this.button1 = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(632, 80);
			this.button1.Name = "button1";
			this.button1.TabIndex = 0;
			this.button1.Text = "Test";
			this.button1.Click += new System.EventHandler(this.Button1Click);
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(616, 160);
			this.label1.Name = "label1";
			this.label1.TabIndex = 1;
			this.label1.Text = "label1";
			// 
			// MainForm
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(720, 517);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.button1);
			this.Name = "MainForm";
			this.Text = "MainForm";
			this.Paint += new System.Windows.Forms.PaintEventHandler(this.MainFormPaint);
			this.ResumeLayout(false);
		}
		#endregion
		void MainFormPaint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			Bitmap func = new Bitmap("c:\\plot.bmp");
			
			Pen pen = new Pen(Color.Black);
			
			for(int k=-256;k<256;k++)
			{
				int y=Convert.ToInt32(Math.Floor(Convert.ToDouble(Math.Pow(k, 2)/102)));
				
				if(-256<=y&&256>y)
				{
					func.SetPixel(k+255,255-y, Color.Black);
				}
				
				func.SetPixel(k+256, 255, Color.Black);
			}
			
			for(int k=0; k<511; k++)
			{
				func.SetPixel(255, k, Color.Black);
			}
			
			e.Graphics.DrawLine(pen, 255, 0, 255, 511);
			
			e.Graphics.DrawImage(func, 0, 0);
		}
		
		void Button1Click(object sender, System.EventArgs e)
		{
			label1.Text = "";
			
			string foo = "blah, blah, blah name: ich blah, blah";
			
			bool found = false;
			
			short num = 0;
			
			foreach(char ch in foo)
			{
				if(ch==Convert.ToChar(":"))
				{
					found = true;
					break;
				}
				
				if(found)
				{
					label1.Text += ch;
					
					num++;
					
					if(num==4)
					{
						found=false;
					}
				}
			}
		}
		
	}
}
