using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace JCMS
{
    public partial class NewNode : Form
    {
        public string NodeName;

        public NewNode()
        {
            InitializeComponent();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            NodeName = ((TextBox)sender).Text;
        }
    }
}