using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace JCMS
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void neuerEintragToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MainTreeView.SelectedNode.Name != "Template")
            {
                NewNode newNode = new NewNode();
                if (newNode.ShowDialog() == DialogResult.OK)
                {
                    MainTreeView.SelectedNode.Nodes.Add(newNode.NodeName);
                }
            }
        }

        private void MainTreeView_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (e.Node.Name == "Template")
            {
                TemplatePanel.BringToFront();
            }
        }
    }
}