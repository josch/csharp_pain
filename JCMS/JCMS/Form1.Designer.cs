namespace JCMS
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.TreeNode treeNode1 = new System.Windows.Forms.TreeNode("Inhaltsverzeichnis");
            System.Windows.Forms.TreeNode treeNode2 = new System.Windows.Forms.TreeNode("Template");
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.mainMenuStrip = new System.Windows.Forms.MenuStrip();
            this.dateiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.MainTreeView = new System.Windows.Forms.TreeView();
            this.menuStrip2 = new System.Windows.Forms.MenuStrip();
            this.neuerEintragToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.TemplatePanel = new System.Windows.Forms.Panel();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.webBrowser1 = new System.Windows.Forms.WebBrowser();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tpInhaltDesign = new System.Windows.Forms.TabPage();
            this.tpInhaltCode = new System.Windows.Forms.TabPage();
            this.bAktualisieren = new System.Windows.Forms.Button();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.mainMenuStrip.SuspendLayout();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.menuStrip2.SuspendLayout();
            this.TemplatePanel.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tpInhaltDesign.SuspendLayout();
            this.tpInhaltCode.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainMenuStrip
            // 
            this.mainMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dateiToolStripMenuItem});
            this.mainMenuStrip.Location = new System.Drawing.Point(0, 0);
            this.mainMenuStrip.Name = "mainMenuStrip";
            this.mainMenuStrip.Size = new System.Drawing.Size(843, 24);
            this.mainMenuStrip.TabIndex = 0;
            // 
            // dateiToolStripMenuItem
            // 
            this.dateiToolStripMenuItem.Name = "dateiToolStripMenuItem";
            this.dateiToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.dateiToolStripMenuItem.Text = "Datei";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 24);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.MainTreeView);
            this.splitContainer1.Panel1.Controls.Add(this.menuStrip2);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.panel1);
            this.splitContainer1.Panel2.Controls.Add(this.TemplatePanel);
            this.splitContainer1.Size = new System.Drawing.Size(843, 443);
            this.splitContainer1.SplitterDistance = 219;
            this.splitContainer1.TabIndex = 1;
            // 
            // MainTreeView
            // 
            this.MainTreeView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MainTreeView.Location = new System.Drawing.Point(0, 0);
            this.MainTreeView.Name = "MainTreeView";
            treeNode1.Name = "Inhaltsverzeichnis";
            treeNode1.Text = "Inhaltsverzeichnis";
            treeNode2.Name = "Template";
            treeNode2.Text = "Template";
            this.MainTreeView.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode1,
            treeNode2});
            this.MainTreeView.Size = new System.Drawing.Size(219, 419);
            this.MainTreeView.TabIndex = 4;
            this.MainTreeView.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.MainTreeView_AfterSelect);
            // 
            // menuStrip2
            // 
            this.menuStrip2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.menuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.neuerEintragToolStripMenuItem});
            this.menuStrip2.Location = new System.Drawing.Point(0, 419);
            this.menuStrip2.Name = "menuStrip2";
            this.menuStrip2.Size = new System.Drawing.Size(219, 24);
            this.menuStrip2.TabIndex = 3;
            this.menuStrip2.Text = "menuStrip2";
            // 
            // neuerEintragToolStripMenuItem
            // 
            this.neuerEintragToolStripMenuItem.Name = "neuerEintragToolStripMenuItem";
            this.neuerEintragToolStripMenuItem.Size = new System.Drawing.Size(85, 20);
            this.neuerEintragToolStripMenuItem.Text = "Neuer Eintrag";
            this.neuerEintragToolStripMenuItem.Click += new System.EventHandler(this.neuerEintragToolStripMenuItem_Click);
            // 
            // TemplatePanel
            // 
            this.TemplatePanel.Controls.Add(this.textBox1);
            this.TemplatePanel.Location = new System.Drawing.Point(41, 18);
            this.TemplatePanel.Name = "TemplatePanel";
            this.TemplatePanel.Size = new System.Drawing.Size(369, 321);
            this.TemplatePanel.TabIndex = 0;
            // 
            // textBox1
            // 
            this.textBox1.Font = new System.Drawing.Font("Lucida Console", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(3, 50);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(363, 268);
            this.textBox1.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.tabControl1);
            this.panel1.Location = new System.Drawing.Point(202, 37);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(406, 363);
            this.panel1.TabIndex = 1;
            // 
            // webBrowser1
            // 
            this.webBrowser1.AllowNavigation = false;
            this.webBrowser1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.webBrowser1.IsWebBrowserContextMenuEnabled = false;
            this.webBrowser1.Location = new System.Drawing.Point(3, 3);
            this.webBrowser1.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser1.Name = "webBrowser1";
            this.webBrowser1.Size = new System.Drawing.Size(392, 331);
            this.webBrowser1.TabIndex = 0;
            this.webBrowser1.Url = new System.Uri("J:\\SICHERUNG\\johannes-schauer\\index.html", System.UriKind.Absolute);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tpInhaltDesign);
            this.tabControl1.Controls.Add(this.tpInhaltCode);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(406, 363);
            this.tabControl1.TabIndex = 1;
            // 
            // tpInhaltDesign
            // 
            this.tpInhaltDesign.Controls.Add(this.webBrowser1);
            this.tpInhaltDesign.Location = new System.Drawing.Point(4, 22);
            this.tpInhaltDesign.Name = "tpInhaltDesign";
            this.tpInhaltDesign.Padding = new System.Windows.Forms.Padding(3);
            this.tpInhaltDesign.Size = new System.Drawing.Size(398, 337);
            this.tpInhaltDesign.TabIndex = 0;
            this.tpInhaltDesign.Text = "Design";
            this.tpInhaltDesign.UseVisualStyleBackColor = true;
            // 
            // tpInhaltCode
            // 
            this.tpInhaltCode.Controls.Add(this.textBox2);
            this.tpInhaltCode.Controls.Add(this.bAktualisieren);
            this.tpInhaltCode.Location = new System.Drawing.Point(4, 22);
            this.tpInhaltCode.Name = "tpInhaltCode";
            this.tpInhaltCode.Padding = new System.Windows.Forms.Padding(3);
            this.tpInhaltCode.Size = new System.Drawing.Size(398, 337);
            this.tpInhaltCode.TabIndex = 1;
            this.tpInhaltCode.Text = "Code";
            // 
            // bAktualisieren
            // 
            this.bAktualisieren.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bAktualisieren.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bAktualisieren.Location = new System.Drawing.Point(3, 311);
            this.bAktualisieren.Name = "bAktualisieren";
            this.bAktualisieren.Size = new System.Drawing.Size(392, 23);
            this.bAktualisieren.TabIndex = 1;
            this.bAktualisieren.Text = "Aktualisieren";
            this.bAktualisieren.UseVisualStyleBackColor = true;
            // 
            // textBox2
            // 
            this.textBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox2.Font = new System.Drawing.Font("Lucida Console", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox2.Location = new System.Drawing.Point(3, 3);
            this.textBox2.Multiline = true;
            this.textBox2.Name = "textBox2";
            this.textBox2.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBox2.Size = new System.Drawing.Size(392, 308);
            this.textBox2.TabIndex = 2;
            this.textBox2.Text = resources.GetString("textBox2.Text");
            this.textBox2.WordWrap = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(843, 467);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.mainMenuStrip);
            this.Name = "Form1";
            this.Text = "Form1";
            this.mainMenuStrip.ResumeLayout(false);
            this.mainMenuStrip.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.menuStrip2.ResumeLayout(false);
            this.menuStrip2.PerformLayout();
            this.TemplatePanel.ResumeLayout(false);
            this.TemplatePanel.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tpInhaltDesign.ResumeLayout(false);
            this.tpInhaltCode.ResumeLayout(false);
            this.tpInhaltCode.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip mainMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem dateiToolStripMenuItem;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.TreeView MainTreeView;
        private System.Windows.Forms.MenuStrip menuStrip2;
        private System.Windows.Forms.ToolStripMenuItem neuerEintragToolStripMenuItem;
        private System.Windows.Forms.Panel TemplatePanel;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.WebBrowser webBrowser1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tpInhaltDesign;
        private System.Windows.Forms.TabPage tpInhaltCode;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Button bAktualisieren;
    }
}

