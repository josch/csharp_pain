using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace ubbParser
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            textBox2.Text = DateTime.Now.ToLongDateString();
            //textBox2.Text = DateTime.Now.Year + "-" + DateTime.Now.Month + "-" + DateTime.Now.Day + " " + DateTime.Now.ToShortTimeString();//GetFormattedText(textBox1.Text);
        }

        public string GetFormattedText(string text)
        {
            DateTime bevore = DateTime.Now;
            int normalTextBeginn = 0;
            string normalText = "";
            for (int i = 0; i < text.Length; i++)
            {
                if (text[i] == '[') //erkenne m�glichen Tag anfang
                {
                    int startTagStart = i;
                    int startTagEnd = text.IndexOf(']', i);
                    if (startTagEnd != -1)
                    {
                        string parameter = "";
                        string ubbTag = GetUbbTag(text.Substring(startTagStart, startTagEnd + 1 - startTagStart), out parameter);
                        if (ubbTag != "") //ubb Tag?
                        {
                            //existiert ein gleicher Tag innerhalb? Erhalte den wahren EndTag
                            int endTagStart = startTagEnd + 1 + GetEndTagStart(ubbTag, text.Substring(startTagEnd + 1, text.Length - (startTagEnd + 1)));

                            if (endTagStart != startTagEnd) //existiert eine TagEnde?
                            {
                                int endTagEnd = endTagStart + ("[/" + ubbTag + "]").Length;
                                string html = "";

                                switch (ubbTag)
                                {
                                    case "b":
                                        html = Schachtel(
                                            text.Substring(startTagEnd + 1, endTagStart - (startTagEnd + 1)),
                                            "<span style=\"font-weight:bold; \"></span>");
                                        text = text.Remove(startTagStart, endTagEnd - startTagStart);
                                        text = text.Insert(startTagStart, html);
                                        break;
                                    case "i":
                                        html = Schachtel(
                                            text.Substring(startTagEnd + 1, endTagStart - (startTagEnd + 1)),
                                            "<span style=\"font-style:italic; \"></span>");
                                        text = text.Remove(startTagStart, endTagEnd - startTagStart);
                                        text = text.Insert(startTagStart, html);
                                        break;
                                    case "u":
                                        html = Schachtel(
                                            text.Substring(startTagEnd + 1, endTagStart - (startTagEnd + 1)),
                                            "<span style=\"text-decoration:underline; \"></span>");
                                        text = text.Remove(startTagStart, endTagEnd - startTagStart);
                                        text = text.Insert(startTagStart, html);
                                        break;
                                    case "color":
                                        html = Schachtel(
                                            text.Substring(startTagEnd + 1, endTagStart - (startTagEnd + 1)),
                                            "<span style=\"color:" + parameter + "; \"></span>");
                                        text = text.Remove(startTagStart, endTagEnd - startTagStart);
                                        text = text.Insert(startTagStart, html);
                                        break;
                                    case "size":
                                        html = Schachtel(
                                            text.Substring(startTagEnd + 1, endTagStart - (startTagEnd + 1)),
                                            "<span style=\"font-size:" + parameter + "px; \"></span>");
                                        text = text.Remove(startTagStart, endTagEnd - startTagStart);
                                        text = text.Insert(startTagStart, html);
                                        break;
                                    case "img":
                                        html = "<img src=\""
                                        + text.Substring(startTagEnd + 1, endTagStart - (startTagEnd + 1))
                                        + "\" alt=\""
                                        + text.Substring(startTagEnd + 1, endTagStart - (startTagEnd + 1))
                                        + "\" />";
                                        text = text.Remove(startTagStart, endTagEnd - startTagStart);
                                        text = text.Insert(startTagStart, html);
                                        break;
                                    case "url":
                                        if (parameter == "")
                                        {
                                            html = "<a href=\""
                                            + text.Substring(startTagEnd + 1, endTagStart - (startTagEnd + 1))
                                            + "\">"
                                            + text.Substring(startTagEnd + 1, endTagStart - (startTagEnd + 1))
                                            + "</a>";
                                        }
                                        else
                                        {
                                            html = "<a href=\""
                                            + parameter
                                            + "\">"
                                            + text.Substring(startTagEnd + 1, endTagStart - (startTagEnd + 1))
                                            + "</a>";
                                        }
                                        text = text.Remove(startTagStart, endTagEnd - startTagStart);
                                        text = text.Insert(startTagStart, html);
                                        break;
                                    case "email":
                                        html = "<a href=\"mailto:"
                                        + text.Substring(startTagEnd + 1, endTagStart - (startTagEnd + 1))
                                        + "\">"
                                        + text.Substring(startTagEnd + 1, endTagStart - (startTagEnd + 1))
                                        + "</a>";
                                        text = text.Remove(startTagStart, endTagEnd - startTagStart);
                                        text = text.Insert(startTagStart, html);
                                        break;
                                    case "code":
                                        html = "<pre>" + text.Substring(startTagEnd + 1, endTagStart - (startTagEnd + 1)) + "</pre>";
                                        text = text.Remove(startTagStart, endTagEnd - startTagStart);
                                        text = text.Insert(startTagStart, html);
                                        break;
                                    case "quote":
                                        html = "<div class=\"quote\">"
                                        + "<span style=\"font-style:italic; \">Zitat von: <span style=\"font-weight:bold; \">"
                                        + parameter + "</span></span><div style=\"margin-left: 20px; \">"
                                        + TextParser(text.Substring(startTagEnd + 1, endTagStart - (startTagEnd + 1)))
                                        + "</div></div>";
                                        text = text.Remove(startTagStart, endTagEnd - startTagStart);
                                        text = text.Insert(startTagStart, html);
                                        break;
                                    case "list":
                                        switch (parameter)
                                        {
                                            case "1":
                                                html = "<ol style=\"list-style-type:decimal; \">";
                                                html += ParseList(text.Substring(startTagEnd + 1, endTagStart - (startTagEnd + 1)));
                                                html += "</ol>";
                                                text = text.Remove(startTagStart, endTagEnd - startTagStart);
                                                text = text.Insert(startTagStart, html);
                                                break;
                                            case "a":
                                                html = "<ol style=\"list-style-type:lower-latin; \">";
                                                html += ParseList(text.Substring(startTagEnd + 1, endTagStart - (startTagEnd + 1)));
                                                html += "</ol>";
                                                text = text.Remove(startTagStart, endTagEnd - startTagStart);
                                                text = text.Insert(startTagStart, html);
                                                break;
                                            default:
                                                html = "<ul style=\"list-style-type:circle; \">";
                                                html += ParseList(text.Substring(startTagEnd + 1, endTagStart - (startTagEnd + 1)));
                                                html += "</ul>";
                                                text = text.Remove(startTagStart, endTagEnd - startTagStart);
                                                text = text.Insert(startTagStart, html);
                                                break;
                                        }
                                        break;
                                }
                                normalText = text.Substring(normalTextBeginn, startTagStart - normalTextBeginn);
                                normalText = ReplaceHyperlinks(normalText);
                                normalText = normalText.Replace("\r\n", "<br />");
                                text = text.Remove(normalTextBeginn, startTagStart - normalTextBeginn);
                                text = text.Insert(normalTextBeginn, normalText);
                                i = normalTextBeginn + normalText.Length + html.Length - 1;
                                normalTextBeginn = i;
                            }
                        }
                    }
                }
            }
            normalText = text.Substring(normalTextBeginn, text.Length - normalTextBeginn);
            normalText = ReplaceHyperlinks(normalText);
            normalText = normalText.Replace("\r\n", "<br />");
            text = text.Remove(normalTextBeginn, text.Length - normalTextBeginn);
            text = text.Insert(normalTextBeginn, normalText);

            return text + (DateTime.Now - bevore).Milliseconds;
        }

        private string ParseList(string list)
        {
            string[] tokens = list.Split(new string[] { "[*]" }, StringSplitOptions.RemoveEmptyEntries);
            list = "";
            foreach (string item in tokens)
            {
                list += "<li>" + TextParser(item) + "</li>";
            }

            return list;
        }

        private string ReplaceHyperlinks(string text)
        {
            for (int i = 0; i < text.Length; i++)
            {
                int hyperlinkStart = text.IndexOf("http://", i);
                if (hyperlinkStart != -1)
                {
                    Regex whitespace = new Regex(@"\s");
                    Match mwhitespace = whitespace.Match(text, hyperlinkStart + 6);
                    string url = "";
                    if (mwhitespace.Success)
                    {
                        url = text.Substring(hyperlinkStart, mwhitespace.Index - hyperlinkStart);
                        text = text.Remove(hyperlinkStart, mwhitespace.Index - hyperlinkStart);
                        text = text.Insert(hyperlinkStart, "<a href=\"" + url + "\">" + url + "</a>");
                        i = hyperlinkStart + 2*url.Length + 15;
                    }
                    else
                    {
                        url = text.Substring(hyperlinkStart, text.Length - hyperlinkStart);
                        text = text.Remove(hyperlinkStart, text.Length - hyperlinkStart);
                        text = text.Insert(hyperlinkStart, "<a href=\"" + url + "\">" + url + "</a>");
                        i = hyperlinkStart + 2 * url.Length + 15;
                    }
                }
                else
                {
                    break;
                }
            }
            return text;
        }

        private string GetUbbTag(string text, out string parameter)
        {
            string result = "";
            parameter = "";
            text = text.Substring(1, text.Length - 2); //Klammern entfernen

            string[] token = text.Split(new char[] { '=' }, 2); //Teile in zwei teile - dies beugt vor, dass geteilt wird wenn ein = in einer url ist

            if (token[0] == "b"
                || token[0] == "u"
                || token[0] == "i"
                || token[0] == "color"
                || token[0] == "size"
                || token[0] == "quote"
                || token[0] == "code"
                || token[0] == "list"
                || token[0] == "url"
                || token[0] == "email"
                || token[0] == "img")
            {
                result = token[0];

                if (token.Length == 2)
                {
                    parameter = token[1];
                }
            }

            return token[0];
        }

        private string Schachtel(string innerCode, string html) //nur daf�r da geschachtelte UBBs zu einem span zusammen zu fassen
        {
            if (innerCode.Length > 0 && innerCode[0] == '[')
            {
                int startTagEnd = innerCode.IndexOf(']');
                string parameter = "";
                string ubbTag = GetUbbTag(innerCode.Substring(0, startTagEnd + 1), out parameter);
                if (ubbTag != ""
                    && (ubbTag == "b"
                    || ubbTag == "i"
                    || ubbTag == "u"
                    || ubbTag == "color"
                    || ubbTag == "size"))
                {
                    int endTagStart = startTagEnd + 1 + GetEndTagStart(ubbTag, innerCode.Substring(startTagEnd + 1, innerCode.Length - (startTagEnd + 1)));
                    if (endTagStart + ("[/" + ubbTag + "]").Length == innerCode.Length) //�berpr�fen ob endtag am ende liegt
                    {
                        switch (ubbTag)
                        {
                            case "b":
                                innerCode = innerCode.Substring(startTagEnd + 1, endTagStart - (startTagEnd + 1));
                                html = html.Insert(html.Length - 9, "font-weight:bold; ");
                                html = Schachtel(innerCode, html);
                                break;
                            case "i":
                                innerCode = innerCode.Substring(startTagEnd + 1, endTagStart - (startTagEnd + 1));
                                html = html.Insert(html.Length - 9, "font-style:italic; ");
                                html = Schachtel(innerCode, html);
                                break;
                            case "u":
                                innerCode = innerCode.Substring(startTagEnd + 1, endTagStart - (startTagEnd + 1));
                                html = html.Insert(html.Length - 9, "text-decoration:underline; ");
                                html = Schachtel(innerCode, html);
                                break;
                            case "color":
                                innerCode = innerCode.Substring(startTagEnd + 1, endTagStart - (startTagEnd + 1));
                                html = html.Insert(html.Length - 9, "color:" + parameter + "; ");
                                html = Schachtel(innerCode, html);
                                break;
                            case "size":
                                innerCode = innerCode.Substring(startTagEnd + 1, endTagStart - (startTagEnd + 1));
                                html = html.Insert(html.Length - 9, "font-size:" + parameter + "px; ");
                                html = Schachtel(innerCode, html);
                                break;
                        }
                    }
                    else
                    {
                        html = html.Insert(html.Length - "</span>".Length, TextParser(innerCode));
                    }
                }
                else
                {
                    html = html.Insert(html.Length - "</span>".Length, TextParser(innerCode));
                }
            }
            else
            {
                html = html.Insert(html.Length - "</span>".Length, TextParser(innerCode));
            }
            //wenn was schief l�uft, ist der code halt nicht geschachtelt und es wird normal weiter gemacht

            return html;
        }

        private string TextParser(string text) //genereller Parser f�r innerCode
        {
            int textBeginn = 0;
            string normalText = "";
            for (int i = 0; i < text.Length; i++)
            {
                if (text[i] == '[') //erkenne m�glichen Tag anfang
                {
                    int startTagStart = i;
                    int startTagEnd = text.IndexOf(']', i);
                    if (startTagEnd != -1)
                    {
                        string parameter = "";
                        string ubbTag = GetUbbTag(text.Substring(startTagStart, startTagEnd + 1 - startTagStart), out parameter);
                        if (ubbTag != "") //ubb Tag?
                        {
                            int endTagStart = startTagEnd + 1 + GetEndTagStart(ubbTag, text.Substring(startTagEnd + 1, text.Length - (startTagEnd + 1)));
                            if (endTagStart != startTagEnd) //existiert eine TagEnde?
                            {
                                int endTagEnd = endTagStart + ("[/" + ubbTag + "]").Length;
                                string html = "";

                                switch (ubbTag)
                                {
                                    case "b":
                                        html = Schachtel(
                                            text.Substring(startTagEnd + 1, endTagStart - (startTagEnd + 1)),
                                            "<span style=\"font-weight:bold; \"></span>");
                                        text = text.Remove(startTagStart, endTagEnd - startTagStart);
                                        text = text.Insert(startTagStart, html);
                                        break;
                                    case "i":
                                        html = Schachtel(
                                            text.Substring(startTagEnd + 1, endTagStart - (startTagEnd + 1)),
                                            "<span style=\"font-style:italic; \"></span>");
                                        text = text.Remove(startTagStart, endTagEnd - startTagStart);
                                        text = text.Insert(startTagStart, html);
                                        break;
                                    case "u":
                                        html = Schachtel(
                                            text.Substring(startTagEnd + 1, endTagStart - (startTagEnd + 1)),
                                            "<span style=\"text-decoration:underline; \"></span>");
                                        text = text.Remove(startTagStart, endTagEnd - startTagStart);
                                        text = text.Insert(startTagStart, html);
                                        break;
                                    case "color":
                                        html = Schachtel(
                                            text.Substring(startTagEnd + 1, endTagStart - (startTagEnd + 1)),
                                            "<span style=\"color:" + parameter + "; \"></span>");
                                        text = text.Remove(startTagStart, endTagEnd - startTagStart);
                                        text = text.Insert(startTagStart, html);
                                        break;
                                    case "size":
                                        html = Schachtel(
                                            text.Substring(startTagEnd + 1, endTagStart - (startTagEnd + 1)),
                                            "<span style=\"font-size:" + parameter + "px; \"></span>");
                                        text = text.Remove(startTagStart, endTagEnd - startTagStart);
                                        text = text.Insert(startTagStart, html);
                                        break;
                                    case "img":
                                        html = "<img src=\""
                                        + text.Substring(startTagEnd + 1, endTagStart - (startTagEnd + 1))
                                        + "\" alt=\""
                                        + text.Substring(startTagEnd + 1, endTagStart - (startTagEnd + 1))
                                        + "\" />";
                                        text = text.Remove(startTagStart, endTagEnd - startTagStart);
                                        text = text.Insert(startTagStart, html);
                                        break;
                                    case "url":
                                        if (parameter == "")
                                        {
                                            html = "<a href=\""
                                            + text.Substring(startTagEnd + 1, endTagStart - (startTagEnd + 1))
                                            + "\">"
                                            + text.Substring(startTagEnd + 1, endTagStart - (startTagEnd + 1))
                                            + "</a>";
                                        }
                                        else
                                        {
                                            html = "<a href=\""
                                            + parameter
                                            + "\">"
                                            + text.Substring(startTagEnd + 1, endTagStart - (startTagEnd + 1))
                                            + "</a>";
                                        }
                                        text = text.Remove(startTagStart, endTagEnd - startTagStart);
                                        text = text.Insert(startTagStart, html);
                                        break;
                                    case "email":
                                        html = "<a href=\"mailto:"
                                        + text.Substring(startTagEnd + 1, endTagStart - (startTagEnd + 1))
                                        + "\">"
                                        + text.Substring(startTagEnd + 1, endTagStart - (startTagEnd + 1))
                                        + "</a>";
                                        text = text.Remove(startTagStart, endTagEnd - startTagStart);
                                        text = text.Insert(startTagStart, html);
                                        break;
                                    case "quote":
                                        html = "<div class=\"quote\">"
                                        + "<span style=\"font-style:italic; \">Zitat von: <span style=\"font-weight:bold; \">"
                                        + parameter + "</span></span><div style=\"margin-left: 20px; \">"
                                        + TextParser(text.Substring(startTagEnd + 1, endTagStart - (startTagEnd + 1)))
                                        + "</div></div>";
                                        text = text.Remove(startTagStart, endTagEnd - startTagStart);
                                        text = text.Insert(startTagStart, html);
                                        break;
                                }
                                normalText = text.Substring(textBeginn, startTagStart - textBeginn);
                                normalText = ReplaceHyperlinks(normalText);
                                normalText = normalText.Replace("\r\n", "<br />");
                                text = text.Remove(textBeginn, startTagStart - textBeginn);
                                text = text.Insert(textBeginn, normalText);
                                i = textBeginn + normalText.Length + html.Length - 1;
                                textBeginn = i;
                            }
                        }
                    }
                }
            }

            normalText = text.Substring(textBeginn, text.Length - textBeginn);
            normalText = ReplaceHyperlinks(normalText);
            normalText = normalText.Replace("\r\n", "<br />");
            text = text.Remove(textBeginn, text.Length - textBeginn);
            text = text.Insert(textBeginn, normalText);
            return text;
        }

        private int GetEndTagStart(string ubbCode, string text)
        {
            int firstEndTag = text.IndexOf("[/" + ubbCode + "]");
            for (int i = 0; i < text.Length; i++)
            {
                int startTagStart = text.IndexOf("["+ubbCode,i);
                if (startTagStart != -1)
                {
                    int startTagEnd = text.IndexOf("]", startTagStart);
                    if (startTagStart < firstEndTag)
                    {
                        int endTagStart = startTagEnd + 1 + GetEndTagStart(ubbCode, text.Substring(startTagEnd + 1, text.Length - (startTagEnd + 1)));
                        firstEndTag = text.IndexOf("[/" + ubbCode + "]", endTagStart + ("[/" + ubbCode + "]").Length);
                        i = endTagStart + ("[/" + ubbCode + "]").Length;
                    }
                }
            }
            return firstEndTag;
        }
    }
}