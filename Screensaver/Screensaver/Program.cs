using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.IO;
using System.Threading;

namespace Screensaver
{
    class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            MainWindow mw = new MainWindow();
            mw.FormBorderStyle = FormBorderStyle.None;
            mw.KeyDown += new KeyEventHandler(onKeyDown);
            Controller c = new Controller(mw);
            mw.einst = new Einstellungen();
            mw.Show();
            while (mw.Created)
            {
                c.Render(mw.einst.speed, mw.einst.text, mw.einst.BackgroundColor, mw.einst.TextColor, mw.einst.width, mw.einst.top);
                Application.DoEvents();
            }
        }

        static void onKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.F)
            {
                ((Form)sender).WindowState = ((Form)sender).WindowState == FormWindowState.Maximized ? FormWindowState.Normal : FormWindowState.Maximized;
            }
            else if (e.KeyData == Keys.E)
            {
                if (((MainWindow)sender).einst.ShowDialog() == DialogResult.OK)
                {
                }
            }
        }
    }
}