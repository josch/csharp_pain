using System.Windows.Forms;
using Microsoft.DirectX;
using Microsoft.DirectX.Direct3D;
using System.Drawing;
using System.IO;
using System;

namespace Screensaver
{
    class Controller
    {
        private Control targetControl;
        private Device device;
        private PresentParameters presentParameters;
        private bool deviceLost;
        private Microsoft.DirectX.Direct3D.Font font1;
        int counter = 0;

        public Controller(Control targetControl)
        {
            this.targetControl = targetControl;
            deviceLost = false;
            presentParameters = new PresentParameters();
            presentParameters.Windowed = true;
            presentParameters.SwapEffect = SwapEffect.Discard;
            presentParameters.AutoDepthStencilFormat = DepthFormat.D16;
            presentParameters.EnableAutoDepthStencil = true;

            device = new Device(
                Manager.Adapters.Default.Adapter,
                DeviceType.Hardware,
                targetControl,
                CreateFlags.HardwareVertexProcessing | CreateFlags.PureDevice,
                presentParameters);

            font1 = new Microsoft.DirectX.Direct3D.Font(device, new System.Drawing.Font(FontFamily.GenericSansSerif, 620));
        }

        public void Render(int speed, string text, Color backgroundcolor, Color textcolor, int textwidth, int top)
        {
            if (deviceLost)
            {
                AttemptRecovery();
            }

            if (deviceLost)
            {
                return;
            }

            device.Clear(
                ClearFlags.Target | ClearFlags.ZBuffer,
                backgroundcolor,
                1.0f,
                0);

            device.BeginScene();

            if (counter > textwidth)
                counter = 1;

            for (int i = 0; i <= targetControl.Size.Width / textwidth + 1; i++)
            {
                font1.DrawText(null, text, new Point(i * textwidth - counter, top), textcolor);
                counter+=speed;
            }

            device.EndScene();
            
            try
            {
                device.Present();
            }
            catch (Exception)
            {
                deviceLost = true;
            }
        }

        protected void AttemptRecovery()
        {
            try
            {
                device.TestCooperativeLevel();
            }
            catch (Exception)
            {
                try
                {
                    device.Reset(presentParameters);
                    deviceLost = false;
                }
                catch { }
            }
        }
    }
}
