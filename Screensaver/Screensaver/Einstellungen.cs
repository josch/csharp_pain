using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Screensaver
{
    public partial class Einstellungen : Form
    {
        public Color TextColor;
        public Color BackgroundColor;
        public int width;
        public int speed;
        public string text;
        public int top;

        public Einstellungen()
        {
            TextColor = System.Drawing.Color.Black;
            BackgroundColor = System.Drawing.Color.White;
            width = 1800;
            speed = 1;
            text = "Tpst";
            top = -165;
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (TextColorDialog.ShowDialog() == DialogResult.OK)
            {
                ((Button)sender).BackColor = TextColorDialog.Color;
                TextColor = TextColorDialog.Color;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (BackgroundColorDialog.ShowDialog() == DialogResult.OK)
            {
                ((Button)sender).BackColor = BackgroundColorDialog.Color;
                BackgroundColor = BackgroundColorDialog.Color;
            }
        }

        private void numericUpDown2_ValueChanged(object sender, EventArgs e)
        {
            width = (int)((NumericUpDown)sender).Value;
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            speed = (int)((NumericUpDown)sender).Value;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            text = ((TextBox)sender).Text;
        }

        private void numericUpDown3_ValueChanged(object sender, EventArgs e)
        {
            top = (int)((NumericUpDown)sender).Value;
        }
    }
}