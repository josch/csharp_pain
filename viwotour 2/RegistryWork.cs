/*
 * Created by SharpDevelop.
 * User: windows
 * Date: 08.12.2004
 * Time: 15:18
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */

using System;
using Microsoft.Win32;

namespace ViwoTour
{
	/// <summary>
	/// Description of RegistryWork.
	/// </summary>
	public class RegistryWork
	{
		RegistryKey SoftwareKey = Registry.LocalMachine.OpenSubKey("Software", true);
		RegistryKey ViwoTourKey;
		
		public RegistryWork()
		{
			try
			{
				ViwoTourKey = SoftwareKey.OpenSubKey("viwotour", true);
				Properties.DataSource = Convert.ToString(ViwoTourKey.GetValue("DataSource"));
				Properties.InitialCatalog = Convert.ToString(ViwoTourKey.GetValue("InitialCatalog"));
				Properties.StationsList = Convert.ToString(ViwoTourKey.GetValue("StationsList"));
			}
			catch
			{
				ViwoTourKey = SoftwareKey.CreateSubKey("viwotour");
				ViwoTourKey.SetValue("DataSource", Properties.DataSource);
				ViwoTourKey.SetValue("InitialCatalog", Properties.InitialCatalog);
				ViwoTourKey.SetValue("StationsList", Properties.StationsList);
			}
		}
		
		public void setKey(string name, string val)
		{
			ViwoTourKey.SetValue(name, val);
		}
		

	}
}
