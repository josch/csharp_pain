/*
 * User: aputze
 * Date: 14.11.2004
 * Time: 14:01
 */

using System;
using System.Collections;

using System.Data;
using System.Threading;

using System.Windows.Forms;

namespace ViwoTour {
	public class MainForm : System.Windows.Forms.Form {
		private System.Windows.Forms.TextBox scopeTextBox;
		private System.Windows.Forms.Label toursLabel;
		private System.Windows.Forms.Label maxWaitingLabel;
		private System.Windows.Forms.TextBox productivity_TouringTextBox;
		private System.Windows.Forms.TabControl tabControl;
		private System.Windows.Forms.Label nonProductiveTouringLabel;
		private System.Windows.Forms.Button summaryButton;
		private System.Windows.Forms.MenuItem search_TMenuItem;
		private System.Windows.Forms.Label search_TLabel;
		private System.Windows.Forms.TextBox nonTouredRowsTextBox;
		private System.Windows.Forms.Label productivity_TouringLabel;
		private System.Windows.Forms.TextBox dataSourceTextBox;
		private System.Windows.Forms.MenuItem storeMenuItem;
		private System.Windows.Forms.MenuItem sqlMenuItem;
		private System.Windows.Forms.Label select_TLabel;
		private System.Windows.Forms.ListBox nonTouredRowsListBox;
		private System.Windows.Forms.TextBox productivity_RowsTextBox;
		private System.Windows.Forms.TextBox initialCatalogTextBox;
		private System.Windows.Forms.CheckedListBox stationCheckedListBox;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox nonProductiveTouringTextBox;
		private System.Windows.Forms.TabPage sqlTabPage;
		private System.Windows.Forms.TextBox productiveTouringTextBox;
		private System.Windows.Forms.TextBox maxTouringTextBox;
		private System.Windows.Forms.Label productivity_RowsLabel;
		private System.Windows.Forms.TextBox touredRowsTextBox;
		private System.Windows.Forms.TabPage propertiesTabPage;
		private System.Windows.Forms.MenuItem graphMenuItem;
		private System.Windows.Forms.StatusBar statusBar;
		private System.Windows.Forms.TabPage summaryTabPage;
		private System.Windows.Forms.TextBox nonProductiveRowsTextBox;
		private System.Windows.Forms.ListBox touredRowsListBox;
		private System.Windows.Forms.TabPage stationTabPage;
		private System.Windows.Forms.Label scopeLabel;
		private System.Windows.Forms.MenuItem select_TMenuItem;
		private System.Windows.Forms.MenuItem disconnectMenuItem;
		private System.Windows.Forms.Label dataSourceLabel;
		private System.Windows.Forms.Label touredRowsLabel;
		private System.Windows.Forms.ProgressBar select_TProgressBar;
		private System.Windows.Forms.Label productiveTouringLabel;
		private System.Windows.Forms.Label initialCatalogLabel;
		private System.Windows.Forms.TextBox maxWaitingTextBox;
		private System.Windows.Forms.TextBox minWaitingTextBox;
		private System.Windows.Forms.MenuItem create_AMenuItem;
		private System.Windows.Forms.TextBox toursTextBox;
		private System.Windows.Forms.TextBox productiveRowsTextBox;
		private System.Windows.Forms.MenuItem loadMenuItem;
		private System.Windows.Forms.Label minWaitingLabel;
		private System.Windows.Forms.MenuItem connectMenuItem;
		private System.Windows.Forms.MainMenu mainMenu;
		private System.Windows.Forms.TabPage progressTabPage;
		private System.Windows.Forms.Label maxTouringLabel;
		private System.Windows.Forms.Label productiveRowsLabel;
		private System.Windows.Forms.ProgressBar search_TProgressBar;
		private System.Windows.Forms.MenuItem prepareMenuItem;
		
		private ViwoTour viwoTour;
		
		public MainForm() {
			InitializeComponent();
			
			viwoTour=new ViwoTour();
			viwoTour.search_TProgress+=new ProgressHandler(Search_TProgress);
			viwoTour.select_TProgress+=new ProgressHandler(Select_TProgress);
		}
		
		#region Windows Forms Designer generated code
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent() {
			this.prepareMenuItem = new System.Windows.Forms.MenuItem();
			this.search_TProgressBar = new System.Windows.Forms.ProgressBar();
			this.productiveRowsLabel = new System.Windows.Forms.Label();
			this.maxTouringLabel = new System.Windows.Forms.Label();
			this.progressTabPage = new System.Windows.Forms.TabPage();
			this.mainMenu = new System.Windows.Forms.MainMenu();
			this.connectMenuItem = new System.Windows.Forms.MenuItem();
			this.minWaitingLabel = new System.Windows.Forms.Label();
			this.loadMenuItem = new System.Windows.Forms.MenuItem();
			this.productiveRowsTextBox = new System.Windows.Forms.TextBox();
			this.toursTextBox = new System.Windows.Forms.TextBox();
			this.create_AMenuItem = new System.Windows.Forms.MenuItem();
			this.minWaitingTextBox = new System.Windows.Forms.TextBox();
			this.maxWaitingTextBox = new System.Windows.Forms.TextBox();
			this.initialCatalogLabel = new System.Windows.Forms.Label();
			this.productiveTouringLabel = new System.Windows.Forms.Label();
			this.select_TProgressBar = new System.Windows.Forms.ProgressBar();
			this.touredRowsLabel = new System.Windows.Forms.Label();
			this.dataSourceLabel = new System.Windows.Forms.Label();
			this.disconnectMenuItem = new System.Windows.Forms.MenuItem();
			this.select_TMenuItem = new System.Windows.Forms.MenuItem();
			this.scopeLabel = new System.Windows.Forms.Label();
			this.stationTabPage = new System.Windows.Forms.TabPage();
			this.touredRowsListBox = new System.Windows.Forms.ListBox();
			this.nonProductiveRowsTextBox = new System.Windows.Forms.TextBox();
			this.summaryTabPage = new System.Windows.Forms.TabPage();
			this.statusBar = new System.Windows.Forms.StatusBar();
			this.graphMenuItem = new System.Windows.Forms.MenuItem();
			this.propertiesTabPage = new System.Windows.Forms.TabPage();
			this.touredRowsTextBox = new System.Windows.Forms.TextBox();
			this.productivity_RowsLabel = new System.Windows.Forms.Label();
			this.maxTouringTextBox = new System.Windows.Forms.TextBox();
			this.productiveTouringTextBox = new System.Windows.Forms.TextBox();
			this.sqlTabPage = new System.Windows.Forms.TabPage();
			this.nonProductiveTouringTextBox = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.stationCheckedListBox = new System.Windows.Forms.CheckedListBox();
			this.initialCatalogTextBox = new System.Windows.Forms.TextBox();
			this.productivity_RowsTextBox = new System.Windows.Forms.TextBox();
			this.nonTouredRowsListBox = new System.Windows.Forms.ListBox();
			this.select_TLabel = new System.Windows.Forms.Label();
			this.sqlMenuItem = new System.Windows.Forms.MenuItem();
			this.storeMenuItem = new System.Windows.Forms.MenuItem();
			this.dataSourceTextBox = new System.Windows.Forms.TextBox();
			this.productivity_TouringLabel = new System.Windows.Forms.Label();
			this.nonTouredRowsTextBox = new System.Windows.Forms.TextBox();
			this.search_TLabel = new System.Windows.Forms.Label();
			this.search_TMenuItem = new System.Windows.Forms.MenuItem();
			this.summaryButton = new System.Windows.Forms.Button();
			this.nonProductiveTouringLabel = new System.Windows.Forms.Label();
			this.tabControl = new System.Windows.Forms.TabControl();
			this.productivity_TouringTextBox = new System.Windows.Forms.TextBox();
			this.maxWaitingLabel = new System.Windows.Forms.Label();
			this.toursLabel = new System.Windows.Forms.Label();
			this.scopeTextBox = new System.Windows.Forms.TextBox();
			this.progressTabPage.SuspendLayout();
			this.stationTabPage.SuspendLayout();
			this.summaryTabPage.SuspendLayout();
			this.propertiesTabPage.SuspendLayout();
			this.sqlTabPage.SuspendLayout();
			this.tabControl.SuspendLayout();
			this.SuspendLayout();
			// 
			// prepareMenuItem
			// 
			this.prepareMenuItem.Index = 2;
			this.prepareMenuItem.Text = "Prepare";
			this.prepareMenuItem.Click += new System.EventHandler(this.PrepareMenuItemClick);
			// 
			// search_TProgressBar
			// 
			this.search_TProgressBar.Location = new System.Drawing.Point(8, 48);
			this.search_TProgressBar.Name = "search_TProgressBar";
			this.search_TProgressBar.Size = new System.Drawing.Size(400, 24);
			this.search_TProgressBar.TabIndex = 3;
			// 
			// productiveRowsLabel
			// 
			this.productiveRowsLabel.Location = new System.Drawing.Point(296, 16);
			this.productiveRowsLabel.Name = "productiveRowsLabel";
			this.productiveRowsLabel.Size = new System.Drawing.Size(112, 24);
			this.productiveRowsLabel.TabIndex = 8;
			this.productiveRowsLabel.Text = "Productive Rows";
			// 
			// maxTouringLabel
			// 
			this.maxTouringLabel.Location = new System.Drawing.Point(16, 88);
			this.maxTouringLabel.Name = "maxTouringLabel";
			this.maxTouringLabel.Size = new System.Drawing.Size(112, 24);
			this.maxTouringLabel.TabIndex = 4;
			this.maxTouringLabel.Text = "Max Touring";
			// 
			// progressTabPage
			// 
			this.progressTabPage.Controls.Add(this.select_TProgressBar);
			this.progressTabPage.Controls.Add(this.select_TLabel);
			this.progressTabPage.Controls.Add(this.search_TProgressBar);
			this.progressTabPage.Controls.Add(this.search_TLabel);
			this.progressTabPage.Location = new System.Drawing.Point(4, 22);
			this.progressTabPage.Name = "progressTabPage";
			this.progressTabPage.Size = new System.Drawing.Size(576, 436);
			this.progressTabPage.TabIndex = 3;
			this.progressTabPage.Text = "Progress";
			// 
			// mainMenu
			// 
			this.mainMenu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
						this.sqlMenuItem,
						this.graphMenuItem});
			// 
			// connectMenuItem
			// 
			this.connectMenuItem.Index = 0;
			this.connectMenuItem.Text = "Connect";
			this.connectMenuItem.Click += new System.EventHandler(this.ConnectMenuItemClick);
			// 
			// minWaitingLabel
			// 
			this.minWaitingLabel.Location = new System.Drawing.Point(16, 16);
			this.minWaitingLabel.Name = "minWaitingLabel";
			this.minWaitingLabel.Size = new System.Drawing.Size(112, 24);
			this.minWaitingLabel.TabIndex = 0;
			this.minWaitingLabel.Text = "Min Waiting";
			// 
			// loadMenuItem
			// 
			this.loadMenuItem.Index = 3;
			this.loadMenuItem.Text = "Load";
			this.loadMenuItem.Click += new System.EventHandler(this.LoadMenuItemClick);
			// 
			// productiveRowsTextBox
			// 
			this.productiveRowsTextBox.Location = new System.Drawing.Point(424, 16);
			this.productiveRowsTextBox.Name = "productiveRowsTextBox";
			this.productiveRowsTextBox.ReadOnly = true;
			this.productiveRowsTextBox.Size = new System.Drawing.Size(136, 21);
			this.productiveRowsTextBox.TabIndex = 9;
			this.productiveRowsTextBox.Text = "";
			// 
			// toursTextBox
			// 
			this.toursTextBox.Location = new System.Drawing.Point(144, 16);
			this.toursTextBox.Name = "toursTextBox";
			this.toursTextBox.ReadOnly = true;
			this.toursTextBox.Size = new System.Drawing.Size(136, 21);
			this.toursTextBox.TabIndex = 1;
			this.toursTextBox.Text = "";
			// 
			// create_AMenuItem
			// 
			this.create_AMenuItem.Index = 0;
			this.create_AMenuItem.Text = "create_A";
			this.create_AMenuItem.Click += new System.EventHandler(this.Create_AMenuItemClick);
			// 
			// minWaitingTextBox
			// 
			this.minWaitingTextBox.Location = new System.Drawing.Point(144, 16);
			this.minWaitingTextBox.Name = "minWaitingTextBox";
			this.minWaitingTextBox.Size = new System.Drawing.Size(136, 21);
			this.minWaitingTextBox.TabIndex = 1;
			this.minWaitingTextBox.Text = "";
			this.minWaitingTextBox.TextChanged += new System.EventHandler(this.MinWaitingTextBoxTextChanged);
			// 
			// maxWaitingTextBox
			// 
			this.maxWaitingTextBox.Location = new System.Drawing.Point(144, 48);
			this.maxWaitingTextBox.Name = "maxWaitingTextBox";
			this.maxWaitingTextBox.Size = new System.Drawing.Size(136, 21);
			this.maxWaitingTextBox.TabIndex = 3;
			this.maxWaitingTextBox.Text = "";
			this.maxWaitingTextBox.TextChanged += new System.EventHandler(this.MaxWaitingTextBoxTextChanged);
			// 
			// initialCatalogLabel
			// 
			this.initialCatalogLabel.Location = new System.Drawing.Point(16, 48);
			this.initialCatalogLabel.Name = "initialCatalogLabel";
			this.initialCatalogLabel.Size = new System.Drawing.Size(112, 24);
			this.initialCatalogLabel.TabIndex = 2;
			this.initialCatalogLabel.Text = "Initial Catalog";
			// 
			// productiveTouringLabel
			// 
			this.productiveTouringLabel.Location = new System.Drawing.Point(296, 120);
			this.productiveTouringLabel.Name = "productiveTouringLabel";
			this.productiveTouringLabel.Size = new System.Drawing.Size(112, 24);
			this.productiveTouringLabel.TabIndex = 14;
			this.productiveTouringLabel.Text = "Productive Touring";
			// 
			// select_TProgressBar
			// 
			this.select_TProgressBar.Location = new System.Drawing.Point(8, 120);
			this.select_TProgressBar.Name = "select_TProgressBar";
			this.select_TProgressBar.Size = new System.Drawing.Size(400, 24);
			this.select_TProgressBar.TabIndex = 5;
			// 
			// touredRowsLabel
			// 
			this.touredRowsLabel.Location = new System.Drawing.Point(16, 48);
			this.touredRowsLabel.Name = "touredRowsLabel";
			this.touredRowsLabel.Size = new System.Drawing.Size(112, 24);
			this.touredRowsLabel.TabIndex = 2;
			this.touredRowsLabel.Text = "Toured Rows";
			// 
			// dataSourceLabel
			// 
			this.dataSourceLabel.Location = new System.Drawing.Point(16, 16);
			this.dataSourceLabel.Name = "dataSourceLabel";
			this.dataSourceLabel.Size = new System.Drawing.Size(112, 24);
			this.dataSourceLabel.TabIndex = 0;
			this.dataSourceLabel.Text = "Data Source";
			// 
			// disconnectMenuItem
			// 
			this.disconnectMenuItem.Index = 1;
			this.disconnectMenuItem.Text = "Disconnect";
			this.disconnectMenuItem.Click += new System.EventHandler(this.DisconnectMenuItemClick);
			// 
			// select_TMenuItem
			// 
			this.select_TMenuItem.Index = 2;
			this.select_TMenuItem.Text = "select_T";
			this.select_TMenuItem.Click += new System.EventHandler(this.Select_TMenuItemClick);
			// 
			// scopeLabel
			// 
			this.scopeLabel.Location = new System.Drawing.Point(16, 128);
			this.scopeLabel.Name = "scopeLabel";
			this.scopeLabel.Size = new System.Drawing.Size(112, 24);
			this.scopeLabel.TabIndex = 6;
			this.scopeLabel.Text = "Scope";
			// 
			// stationTabPage
			// 
			this.stationTabPage.Controls.Add(this.stationCheckedListBox);
			this.stationTabPage.Location = new System.Drawing.Point(4, 22);
			this.stationTabPage.Name = "stationTabPage";
			this.stationTabPage.Size = new System.Drawing.Size(576, 436);
			this.stationTabPage.TabIndex = 2;
			this.stationTabPage.Text = "Station";
			// 
			// touredRowsListBox
			// 
			this.touredRowsListBox.Location = new System.Drawing.Point(144, 48);
			this.touredRowsListBox.Name = "touredRowsListBox";
			this.touredRowsListBox.Size = new System.Drawing.Size(136, 95);
			this.touredRowsListBox.TabIndex = 3;
			// 
			// nonProductiveRowsTextBox
			// 
			this.nonProductiveRowsTextBox.Location = new System.Drawing.Point(424, 48);
			this.nonProductiveRowsTextBox.Name = "nonProductiveRowsTextBox";
			this.nonProductiveRowsTextBox.ReadOnly = true;
			this.nonProductiveRowsTextBox.Size = new System.Drawing.Size(136, 21);
			this.nonProductiveRowsTextBox.TabIndex = 11;
			this.nonProductiveRowsTextBox.Text = "";
			// 
			// summaryTabPage
			// 
			this.summaryTabPage.Controls.Add(this.summaryButton);
			this.summaryTabPage.Controls.Add(this.productivity_TouringTextBox);
			this.summaryTabPage.Controls.Add(this.productivity_TouringLabel);
			this.summaryTabPage.Controls.Add(this.nonProductiveTouringTextBox);
			this.summaryTabPage.Controls.Add(this.nonProductiveTouringLabel);
			this.summaryTabPage.Controls.Add(this.productiveTouringTextBox);
			this.summaryTabPage.Controls.Add(this.productiveTouringLabel);
			this.summaryTabPage.Controls.Add(this.productivity_RowsTextBox);
			this.summaryTabPage.Controls.Add(this.productivity_RowsLabel);
			this.summaryTabPage.Controls.Add(this.nonProductiveRowsTextBox);
			this.summaryTabPage.Controls.Add(this.label2);
			this.summaryTabPage.Controls.Add(this.productiveRowsTextBox);
			this.summaryTabPage.Controls.Add(this.productiveRowsLabel);
			this.summaryTabPage.Controls.Add(this.nonTouredRowsTextBox);
			this.summaryTabPage.Controls.Add(this.nonTouredRowsListBox);
			this.summaryTabPage.Controls.Add(this.label1);
			this.summaryTabPage.Controls.Add(this.touredRowsTextBox);
			this.summaryTabPage.Controls.Add(this.touredRowsListBox);
			this.summaryTabPage.Controls.Add(this.touredRowsLabel);
			this.summaryTabPage.Controls.Add(this.toursTextBox);
			this.summaryTabPage.Controls.Add(this.toursLabel);
			this.summaryTabPage.Location = new System.Drawing.Point(4, 22);
			this.summaryTabPage.Name = "summaryTabPage";
			this.summaryTabPage.Size = new System.Drawing.Size(576, 364);
			this.summaryTabPage.TabIndex = 4;
			this.summaryTabPage.Text = "Summary";
			// 
			// statusBar
			// 
			this.statusBar.Location = new System.Drawing.Point(0, 390);
			this.statusBar.Name = "statusBar";
			this.statusBar.Size = new System.Drawing.Size(584, 24);
			this.statusBar.TabIndex = 0;
			// 
			// graphMenuItem
			// 
			this.graphMenuItem.Index = 1;
			this.graphMenuItem.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
						this.create_AMenuItem,
						this.search_TMenuItem,
						this.select_TMenuItem});
			this.graphMenuItem.Text = "Graph";
			// 
			// propertiesTabPage
			// 
			this.propertiesTabPage.Controls.Add(this.scopeTextBox);
			this.propertiesTabPage.Controls.Add(this.scopeLabel);
			this.propertiesTabPage.Controls.Add(this.maxTouringTextBox);
			this.propertiesTabPage.Controls.Add(this.maxTouringLabel);
			this.propertiesTabPage.Controls.Add(this.maxWaitingTextBox);
			this.propertiesTabPage.Controls.Add(this.maxWaitingLabel);
			this.propertiesTabPage.Controls.Add(this.minWaitingTextBox);
			this.propertiesTabPage.Controls.Add(this.minWaitingLabel);
			this.propertiesTabPage.Location = new System.Drawing.Point(4, 22);
			this.propertiesTabPage.Name = "propertiesTabPage";
			this.propertiesTabPage.Size = new System.Drawing.Size(576, 436);
			this.propertiesTabPage.TabIndex = 1;
			this.propertiesTabPage.Text = "Properties";
			// 
			// touredRowsTextBox
			// 
			this.touredRowsTextBox.Location = new System.Drawing.Point(144, 152);
			this.touredRowsTextBox.Name = "touredRowsTextBox";
			this.touredRowsTextBox.ReadOnly = true;
			this.touredRowsTextBox.Size = new System.Drawing.Size(136, 21);
			this.touredRowsTextBox.TabIndex = 4;
			this.touredRowsTextBox.Text = "";
			// 
			// productivity_RowsLabel
			// 
			this.productivity_RowsLabel.Location = new System.Drawing.Point(296, 80);
			this.productivity_RowsLabel.Name = "productivity_RowsLabel";
			this.productivity_RowsLabel.Size = new System.Drawing.Size(112, 24);
			this.productivity_RowsLabel.TabIndex = 12;
			this.productivity_RowsLabel.Text = "Productivity";
			// 
			// maxTouringTextBox
			// 
			this.maxTouringTextBox.Location = new System.Drawing.Point(144, 88);
			this.maxTouringTextBox.Name = "maxTouringTextBox";
			this.maxTouringTextBox.Size = new System.Drawing.Size(136, 21);
			this.maxTouringTextBox.TabIndex = 5;
			this.maxTouringTextBox.Text = "";
			this.maxTouringTextBox.TextChanged += new System.EventHandler(this.MaxTouringTextBoxTextChanged);
			// 
			// productiveTouringTextBox
			// 
			this.productiveTouringTextBox.Location = new System.Drawing.Point(424, 120);
			this.productiveTouringTextBox.Name = "productiveTouringTextBox";
			this.productiveTouringTextBox.ReadOnly = true;
			this.productiveTouringTextBox.Size = new System.Drawing.Size(136, 21);
			this.productiveTouringTextBox.TabIndex = 15;
			this.productiveTouringTextBox.Text = "";
			// 
			// sqlTabPage
			// 
			this.sqlTabPage.Controls.Add(this.initialCatalogTextBox);
			this.sqlTabPage.Controls.Add(this.initialCatalogLabel);
			this.sqlTabPage.Controls.Add(this.dataSourceTextBox);
			this.sqlTabPage.Controls.Add(this.dataSourceLabel);
			this.sqlTabPage.Location = new System.Drawing.Point(4, 22);
			this.sqlTabPage.Name = "sqlTabPage";
			this.sqlTabPage.Size = new System.Drawing.Size(576, 436);
			this.sqlTabPage.TabIndex = 0;
			this.sqlTabPage.Text = "Sql";
			// 
			// nonProductiveTouringTextBox
			// 
			this.nonProductiveTouringTextBox.Location = new System.Drawing.Point(424, 152);
			this.nonProductiveTouringTextBox.Name = "nonProductiveTouringTextBox";
			this.nonProductiveTouringTextBox.ReadOnly = true;
			this.nonProductiveTouringTextBox.Size = new System.Drawing.Size(136, 21);
			this.nonProductiveTouringTextBox.TabIndex = 17;
			this.nonProductiveTouringTextBox.Text = "";
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(16, 184);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(120, 24);
			this.label1.TabIndex = 5;
			this.label1.Text = "Non-Toured Rows";
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(296, 48);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(112, 24);
			this.label2.TabIndex = 10;
			this.label2.Text = "Non-Productive Rows";
			// 
			// stationCheckedListBox
			// 
			this.stationCheckedListBox.Dock = System.Windows.Forms.DockStyle.Fill;
			this.stationCheckedListBox.Location = new System.Drawing.Point(0, 0);
			this.stationCheckedListBox.Name = "stationCheckedListBox";
			this.stationCheckedListBox.Size = new System.Drawing.Size(576, 436);
			this.stationCheckedListBox.TabIndex = 0;
			// 
			// initialCatalogTextBox
			// 
			this.initialCatalogTextBox.Location = new System.Drawing.Point(144, 48);
			this.initialCatalogTextBox.Name = "initialCatalogTextBox";
			this.initialCatalogTextBox.Size = new System.Drawing.Size(136, 21);
			this.initialCatalogTextBox.TabIndex = 3;
			this.initialCatalogTextBox.Text = "";
			this.initialCatalogTextBox.TextChanged += new System.EventHandler(this.InitialCatalogTextBoxTextChanged);
			// 
			// productivity_RowsTextBox
			// 
			this.productivity_RowsTextBox.Location = new System.Drawing.Point(424, 80);
			this.productivity_RowsTextBox.Name = "productivity_RowsTextBox";
			this.productivity_RowsTextBox.ReadOnly = true;
			this.productivity_RowsTextBox.Size = new System.Drawing.Size(136, 21);
			this.productivity_RowsTextBox.TabIndex = 13;
			this.productivity_RowsTextBox.Text = "";
			// 
			// nonTouredRowsListBox
			// 
			this.nonTouredRowsListBox.Location = new System.Drawing.Point(144, 184);
			this.nonTouredRowsListBox.Name = "nonTouredRowsListBox";
			this.nonTouredRowsListBox.Size = new System.Drawing.Size(136, 95);
			this.nonTouredRowsListBox.TabIndex = 6;
			// 
			// select_TLabel
			// 
			this.select_TLabel.Location = new System.Drawing.Point(16, 88);
			this.select_TLabel.Name = "select_TLabel";
			this.select_TLabel.Size = new System.Drawing.Size(112, 24);
			this.select_TLabel.TabIndex = 4;
			this.select_TLabel.Text = "select_T";
			// 
			// sqlMenuItem
			// 
			this.sqlMenuItem.Index = 0;
			this.sqlMenuItem.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
						this.connectMenuItem,
						this.disconnectMenuItem,
						this.prepareMenuItem,
						this.loadMenuItem,
						this.storeMenuItem});
			this.sqlMenuItem.Text = "Sql";
			// 
			// storeMenuItem
			// 
			this.storeMenuItem.Index = 4;
			this.storeMenuItem.Text = "Store";
			this.storeMenuItem.Click += new System.EventHandler(this.StoreMenuItemClick);
			// 
			// dataSourceTextBox
			// 
			this.dataSourceTextBox.Location = new System.Drawing.Point(144, 16);
			this.dataSourceTextBox.Name = "dataSourceTextBox";
			this.dataSourceTextBox.Size = new System.Drawing.Size(136, 21);
			this.dataSourceTextBox.TabIndex = 1;
			this.dataSourceTextBox.Text = "";
			this.dataSourceTextBox.TextChanged += new System.EventHandler(this.DataSourceTextBoxTextChanged);
			// 
			// productivity_TouringLabel
			// 
			this.productivity_TouringLabel.Location = new System.Drawing.Point(296, 184);
			this.productivity_TouringLabel.Name = "productivity_TouringLabel";
			this.productivity_TouringLabel.Size = new System.Drawing.Size(112, 24);
			this.productivity_TouringLabel.TabIndex = 18;
			this.productivity_TouringLabel.Text = "Productivity";
			// 
			// nonTouredRowsTextBox
			// 
			this.nonTouredRowsTextBox.Location = new System.Drawing.Point(144, 288);
			this.nonTouredRowsTextBox.Name = "nonTouredRowsTextBox";
			this.nonTouredRowsTextBox.ReadOnly = true;
			this.nonTouredRowsTextBox.Size = new System.Drawing.Size(136, 21);
			this.nonTouredRowsTextBox.TabIndex = 7;
			this.nonTouredRowsTextBox.Text = "";
			// 
			// search_TLabel
			// 
			this.search_TLabel.Location = new System.Drawing.Point(16, 16);
			this.search_TLabel.Name = "search_TLabel";
			this.search_TLabel.Size = new System.Drawing.Size(112, 24);
			this.search_TLabel.TabIndex = 2;
			this.search_TLabel.Text = "search_T";
			// 
			// search_TMenuItem
			// 
			this.search_TMenuItem.Index = 1;
			this.search_TMenuItem.Text = "search_T";
			this.search_TMenuItem.Click += new System.EventHandler(this.Search_TMenuItemClick);
			// 
			// summaryButton
			// 
			this.summaryButton.Location = new System.Drawing.Point(16, 328);
			this.summaryButton.Name = "summaryButton";
			this.summaryButton.Size = new System.Drawing.Size(120, 24);
			this.summaryButton.TabIndex = 20;
			this.summaryButton.Text = "Summary";
			this.summaryButton.Click += new System.EventHandler(this.SummaryButtonClick);
			// 
			// nonProductiveTouringLabel
			// 
			this.nonProductiveTouringLabel.Location = new System.Drawing.Point(296, 152);
			this.nonProductiveTouringLabel.Name = "nonProductiveTouringLabel";
			this.nonProductiveTouringLabel.Size = new System.Drawing.Size(112, 24);
			this.nonProductiveTouringLabel.TabIndex = 16;
			this.nonProductiveTouringLabel.Text = "Non-Productive Touring";
			// 
			// tabControl
			// 
			this.tabControl.Controls.Add(this.sqlTabPage);
			this.tabControl.Controls.Add(this.propertiesTabPage);
			this.tabControl.Controls.Add(this.stationTabPage);
			this.tabControl.Controls.Add(this.progressTabPage);
			this.tabControl.Controls.Add(this.summaryTabPage);
			this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tabControl.Location = new System.Drawing.Point(0, 0);
			this.tabControl.Name = "tabControl";
			this.tabControl.SelectedIndex = 0;
			this.tabControl.Size = new System.Drawing.Size(584, 390);
			this.tabControl.TabIndex = 1;
			// 
			// productivity_TouringTextBox
			// 
			this.productivity_TouringTextBox.Location = new System.Drawing.Point(424, 184);
			this.productivity_TouringTextBox.Name = "productivity_TouringTextBox";
			this.productivity_TouringTextBox.ReadOnly = true;
			this.productivity_TouringTextBox.Size = new System.Drawing.Size(136, 21);
			this.productivity_TouringTextBox.TabIndex = 19;
			this.productivity_TouringTextBox.Text = "";
			// 
			// maxWaitingLabel
			// 
			this.maxWaitingLabel.Location = new System.Drawing.Point(16, 48);
			this.maxWaitingLabel.Name = "maxWaitingLabel";
			this.maxWaitingLabel.Size = new System.Drawing.Size(112, 24);
			this.maxWaitingLabel.TabIndex = 2;
			this.maxWaitingLabel.Text = "Max Waiting";
			// 
			// toursLabel
			// 
			this.toursLabel.Location = new System.Drawing.Point(16, 16);
			this.toursLabel.Name = "toursLabel";
			this.toursLabel.Size = new System.Drawing.Size(112, 24);
			this.toursLabel.TabIndex = 0;
			this.toursLabel.Text = "Tours";
			// 
			// scopeTextBox
			// 
			this.scopeTextBox.Location = new System.Drawing.Point(144, 128);
			this.scopeTextBox.Name = "scopeTextBox";
			this.scopeTextBox.Size = new System.Drawing.Size(136, 21);
			this.scopeTextBox.TabIndex = 7;
			this.scopeTextBox.Text = "";
			this.scopeTextBox.TextChanged += new System.EventHandler(this.ScopeTextBoxTextChanged);
			// 
			// MainForm
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
			this.ClientSize = new System.Drawing.Size(584, 414);
			this.Controls.Add(this.tabControl);
			this.Controls.Add(this.statusBar);
			this.Menu = this.mainMenu;
			this.Name = "MainForm";
			this.Text = "MainForm";
			this.Load += new System.EventHandler(this.MainFormLoad);
			this.progressTabPage.ResumeLayout(false);
			this.stationTabPage.ResumeLayout(false);
			this.summaryTabPage.ResumeLayout(false);
			this.propertiesTabPage.ResumeLayout(false);
			this.sqlTabPage.ResumeLayout(false);
			this.tabControl.ResumeLayout(false);
			this.ResumeLayout(false);
		}
		#endregion
		
		[STAThread]
		public static void Main() {
			Application.Run(new MainForm());
		}
		void MainFormLoad(object sender, System.EventArgs e) {
			minWaitingTextBox.Text=Properties.MinWaiting.ToString();
			maxWaitingTextBox.Text=Properties.MaxWaiting.ToString();
			
			maxTouringTextBox.Text=Properties.MaxTouring.ToString();
			
			scopeTextBox.Text=Properties.Scope.ToString();
		}
		
		void DataSourceTextBoxTextChanged(object sender, System.EventArgs e) { Properties.DataSource=dataSourceTextBox.Text; }
		void InitialCatalogTextBoxTextChanged(object sender, System.EventArgs e) { Properties.InitialCatalog=initialCatalogTextBox.Text; }
		
		void MinWaitingTextBoxTextChanged(object sender, System.EventArgs e) {
			try { Properties.MinWaiting=TimeSpan.Parse(minWaitingTextBox.Text); } catch {}
		}
		void MaxWaitingTextBoxTextChanged(object sender, System.EventArgs e) {
			try { Properties.MaxWaiting=TimeSpan.Parse(maxWaitingTextBox.Text); } catch {}
		}
		void MaxTouringTextBoxTextChanged(object sender, System.EventArgs e) {
			try { Properties.MaxTouring=TimeSpan.Parse(maxTouringTextBox.Text); } catch {}
		}
		void ScopeTextBoxTextChanged(object sender, System.EventArgs e) {
			try { Properties.Scope=System.Int32.Parse(scopeTextBox.Text); } catch {}
		}
		
		void ConnectMenuItemClick(object sender, System.EventArgs e) {
			viwoTour.Connect();
			statusBar.Text="Connected";
		}
		void DisconnectMenuItemClick(object sender, System.EventArgs e) {
			viwoTour.Disconnect();
			statusBar.Text="Disconnected";
		}
		
		void PrepareMenuItemClick(object sender, System.EventArgs e) {
			viwoTour.Prepare();
			statusBar.Text="Prepared";
		}
		
		void LoadMenuItemClick(object sender, System.EventArgs e) {
			statusBar.Text="Load...";
			viwoTour.Load();
			statusBar.Text="Loaded";
			
			//HACK
			stationCheckedListBox.Items.Clear();
			foreach(string station in viwoTour.courseBook.get_STATION_Of_Rows()) {
				stationCheckedListBox.Items.Add(station);
			}
		}
		void StoreMenuItemClick(object sender, System.EventArgs e) {
			statusBar.Text="Store...";
			viwoTour.Store();
			statusBar.Text="Stored";
		}
		
		void Create_AMenuItemClick(object sender, System.EventArgs e) {
			new Thread(new ThreadStart(Create_AStart)).Start();
		}
		void Create_AStart() {
			statusBar.Text="create_A...";
			viwoTour.create_A();
			statusBar.Text="create_A done";
		}
		
		void Search_TMenuItemClick(object sender, System.EventArgs e) {
			//HACK
			viwoTour.S.Clear();
			foreach(string station in stationCheckedListBox.CheckedItems) {
				viwoTour.S.Add(station);
			}
			
			new Thread(new ThreadStart(Search_TStart)).Start();
		}
		void Search_TStart() {
			statusBar.Text="search_T...";
			
			DateTime begin=DateTime.Now;
			viwoTour.search_T();
			DateTime end=DateTime.Now;
			
			statusBar.Text="search_T done in "+(end-begin);
		}
		void Search_TProgress(int Progress) { search_TProgressBar.Value=Progress; }
		
		void Select_TMenuItemClick(object sender, System.EventArgs e) {
			new Thread(new ThreadStart(Select_TStart)).Start();
		}
		void Select_TStart() {
			statusBar.Text="select_T...";
			
			DateTime begin=DateTime.Now;
			viwoTour.select_T();
			DateTime end=DateTime.Now;
			
			statusBar.Text="select_T done in "+(end-begin);
		}
		void Select_TProgress(int Progress) { select_TProgressBar.Value=Progress; }
		
		void SummaryButtonClick(object sender, System.EventArgs e) {
			ArrayList TouredIDs=new ArrayList();
			ArrayList NonTouredIDs=(ArrayList)viwoTour.courseBook.get_ID_Of_Rows().Clone();
			
			int Tours=0;
			
			int ProductiveRows=0;
			int NonProductiveRows=0;
			
			TimeSpan ProductiveTouring=new TimeSpan();
			TimeSpan NonProductiveTouring=new TimeSpan();
			
			foreach(ArrayList t_select in viwoTour.T_select) {
				Tours++;
				
				for(int i=0;i<t_select.Count;i++) {
					DataRow row=viwoTour.coursePlan.Rows[(int)t_select[i]];
					
					if(TouredIDs.Contains(row["ID"])) {
						NonProductiveRows++;
						
						NonProductiveTouring+=(DateTime)row["ARRIVAL"]-(DateTime)row["DEPARTURE"];
					} else {
						ProductiveRows++;
						
						ProductiveTouring+=(DateTime)row["ARRIVAL"]-(DateTime)row["DEPARTURE"];
					}
					
					if(!TouredIDs.Contains(row["ID"])) { TouredIDs.Add(row["ID"]); }
				}
				for(int i=1;i<t_select.Count;i++) {
					DataRow row1=viwoTour.coursePlan.Rows[(int)t_select[i-1]];
					DataRow row2=viwoTour.coursePlan.Rows[(int)t_select[i]];
					
					NonProductiveTouring+=(DateTime)row2["DEPARTURE"]-(DateTime)row1["ARRIVAL"];
				}
			}
			foreach(int ID in TouredIDs) { NonTouredIDs.Remove(ID); }
			
			toursTextBox.Text=Tours.ToString();
			
			touredRowsListBox.Items.Clear();
			foreach(int ID in TouredIDs) { touredRowsListBox.Items.Add(ID); }
			nonTouredRowsListBox.Items.Clear();
			foreach(int ID in NonTouredIDs) { nonTouredRowsListBox.Items.Add(ID); }
			
			touredRowsTextBox.Text=TouredIDs.Count.ToString();
			nonTouredRowsTextBox.Text=NonTouredIDs.Count.ToString();
			
			productiveRowsTextBox.Text=ProductiveRows.ToString();
			nonProductiveRowsTextBox.Text=NonProductiveRows.ToString();
			productivity_RowsTextBox.Text=(100.0*(double)ProductiveRows/(double)(ProductiveRows+NonProductiveRows)).ToString()+" %";
			
			productiveTouringTextBox.Text=ProductiveTouring.ToString();
			nonProductiveTouringTextBox.Text=NonProductiveTouring.ToString();
			productivity_TouringTextBox.Text=(100.0*(double)ProductiveTouring.Ticks/(double)(ProductiveTouring.Ticks+NonProductiveTouring.Ticks)).ToString()+" %";
		}
		
	}
}
