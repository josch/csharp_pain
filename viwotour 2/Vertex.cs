/*
 * User: aputze
 * Date: 28.10.2004
 * Time: 15:48
 */

using System;

namespace ViwoTour {
	public class VertexData {
		public static int lastId=0;
		public int Id;
		
		public DateTime Departure;
		public string DepartureStation;
		
		public DateTime Arrival;
		public string ArrivalStation;
		
		public int Workforce;
		
		public VertexData() {
			Id=0;
			
			Departure=new DateTime();
			DepartureStation="";
			
			Arrival=new DateTime();
			ArrivalStation="";
			
			Workforce=0;
		}
		
		public void GenId() { Id=++lastId; }
		
		public object Clone() {
			VertexData vertexData=new VertexData();
			vertexData.Id=Id;
			
			vertexData.Departure=Departure;
			vertexData.DepartureStation=DepartureStation;
			vertexData.Arrival=Arrival;
			vertexData.ArrivalStation=ArrivalStation;
			
			return vertexData;
		}
	}
	
	public class Vertex {
		public VertexData vertexData;
		
		public int inboundDegree;
		public int outboundDegree;
		
		public Vertex() {
			vertexData=new VertexData();
			
			inboundDegree=0;
			outboundDegree=0;
		}
		public Vertex(VertexData vertexData) {
			this.vertexData=vertexData;
			
			inboundDegree=0;
			outboundDegree=0;
		}
		
		public object Clone() {
			Vertex vertex=new Vertex();
			vertex.vertexData=(VertexData)vertexData.Clone();
			
			vertex.inboundDegree=inboundDegree;
			vertex.outboundDegree=outboundDegree;
			
			return vertexData;
		}
	}
}
