/*
 * Created by SharpDevelop.
 * User: windows
 * Date: 08.12.2004
 * Time: 14:54
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */

using System;
using Microsoft.Win32;

namespace ViwoTour
{
	/// <summary>
	/// Description of Registry.
	/// </summary>
	public class RegistryWork
	{
		public Registry()
		{
			RegistryKey SoftwareKey = Registry.LocalMachine.OpenSubKey("Software", true);
			
			try
			{
				RegistryKey ViwoTourKey = SoftwareKey.OpenSubKey("viwotour", true);
				Properties.DataSource = Convert.ToString(ViwoTourKey.GetValue("DataSource"));
				Properties.InitialCatalog = Convert.ToString(ViwoTourKey.GetValue("InitialCatalog"));
			}
			catch
			{
				RegistryKey newkey = SoftwareKey.CreateSubKey("viwotour");
				newkey.SetValue("DataSource", Properties.DataSource);
				newkey.SetValue("InitialCatalog", Properties.InitialCatalog);
			}
		}
	}
}
