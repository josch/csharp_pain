/*
 * User: aputze
 * Date: 03.11.2004
 * Time: 11:13
 */

using System;

using System.Data;

namespace ViwoTour {
	public class TourPlan : DataTable {
		public TourPlan() : base("TOUR_PLAN") {
			Columns.Add(new DataColumn("ID",typeof(int)));
			Columns.Add(new DataColumn("ID1",typeof(int)));
			Columns.Add(new DataColumn("DEPARTURE",typeof(DateTime)));
			Columns.Add(new DataColumn("DEPARTURE_STATION",typeof(string)));
			Columns.Add(new DataColumn("ARRIVAL",typeof(DateTime)));
			Columns.Add(new DataColumn("ARRIVAL_STATION",typeof(string)));
		}
	}
}
