/*
 * Created by SharpDevelop.
 * User: windows
 * Date: 24.11.2004
 * Time: 15:42
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */

using System;
using System.Drawing;
using System.Windows.Forms;
using System.Data;

namespace ViwoTour
{
	/// <summary>
	/// Description of MainWindow.
	/// </summary>
	public class MainWindow : System.Windows.Forms.Form
	{
		private System.ComponentModel.IContainer components;
		private System.Windows.Forms.TreeView Baumansicht;
		private System.Windows.Forms.StatusBar Statusleiste;
		private System.Windows.Forms.MenuItem ExtrasMenuItem;
		private System.Windows.Forms.Label TourenoptionenLabel0;
		private System.Windows.Forms.MenuItem AnsichtMenuItem;
		private System.Windows.Forms.MenuItem UeberMenuItem;
		private System.Windows.Forms.DataGrid QuellTabelleAnzeigenDataGrid;
		private System.Windows.Forms.StatusBarPanel StatusBarPanel2;
		private System.Windows.Forms.Label DatenbankEinstellungenLabel0;
		private System.Windows.Forms.TextBox DataSourceTextBox;
		private System.Windows.Forms.Label DatenbankEinstellungenLabel2;
		private System.Windows.Forms.Label DatenbankEinstellungenLabel3;
		private System.Windows.Forms.Label DatenbankEinstellungenLabel4;
		private System.Windows.Forms.Panel StartbahnhoefePanel;
		private System.Windows.Forms.ToolBar Werkzeugleiste;
		private System.Windows.Forms.Button TourenoptionenWeiterButton;
		private System.Windows.Forms.MenuItem StatusleisteMenuItem;
		private System.Windows.Forms.GroupBox DatenbankEinstellungenGroupBox1;
		private System.Windows.Forms.MenuItem HilfeMenuItem;
		private System.Windows.Forms.ToolBarButton StatusleisteToolBarButton;
		private System.Windows.Forms.Splitter splitter1;
		private System.Windows.Forms.MenuItem BaumansichtMenuItem;
		private System.Windows.Forms.MenuItem OptionenMenuItem;
		private System.Windows.Forms.TextBox InitialCatalogTextBox;
		private System.Windows.Forms.Button DatenbankEinstellungenTestButton;
		private System.Windows.Forms.MenuItem DateiMenuItem;
		private System.Windows.Forms.MainMenu MainMenu;
		private System.Windows.Forms.Button DatenbankEinstellungenWeiterButton;
		private System.Windows.Forms.MenuItem WerkzeugleisteMenuItem;
		private System.Windows.Forms.Button StartbahnhoefeWeiterButton;
		private System.Windows.Forms.Timer Timer;
		private System.Windows.Forms.MenuItem BeendenMenuItem;
		private System.Windows.Forms.MenuItem menuItem1;
		private System.Windows.Forms.ImageList ImageList;
		private System.Windows.Forms.Panel TourenoptionenPanel;
		private System.Windows.Forms.Label StartbahnhoefeLabel0;
		private System.Windows.Forms.Button DatenbankEinstellungenStandardButton;
		private System.Windows.Forms.StatusBarPanel StatusBarPanel1;
		private System.Windows.Forms.ToolBarButton BaumansichtToolBarButton;
		private System.Windows.Forms.ListBox StationsListBox;
		private System.Windows.Forms.Panel DatenbankEinstellungenPanel;
		
		private DataSet QuellTabelleAnzeigenDataSet;
		
		private short status = 0;
		
		/*
		 * 0 nicht verbunden
		 * 1 verbunden
		 * 2 prepared
		 * 3 startbahnh�fe listbox gef�llt
		 */
		
		protected About about;
		protected Optionen optionen;
		protected ViwoTour viwotour;
		protected RegistryWork registrywork;
		
		public MainWindow()
		{
			InitializeComponent();
			
			viwotour = new ViwoTour();
			about = new About();
			optionen = new Optionen();
			registrywork = new RegistryWork();
			QuellTabelleAnzeigenDataSet = new DataSet();
			
			InitViwoTour();
		}
		
		#region Windows Forms Designer generated code
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent() {
			this.components = new System.ComponentModel.Container();
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(MainWindow));
			this.DatenbankEinstellungenPanel = new System.Windows.Forms.Panel();
			this.StationsListBox = new System.Windows.Forms.ListBox();
			this.BaumansichtToolBarButton = new System.Windows.Forms.ToolBarButton();
			this.StatusBarPanel1 = new System.Windows.Forms.StatusBarPanel();
			this.DatenbankEinstellungenStandardButton = new System.Windows.Forms.Button();
			this.StartbahnhoefeLabel0 = new System.Windows.Forms.Label();
			this.TourenoptionenPanel = new System.Windows.Forms.Panel();
			this.ImageList = new System.Windows.Forms.ImageList(this.components);
			this.menuItem1 = new System.Windows.Forms.MenuItem();
			this.BeendenMenuItem = new System.Windows.Forms.MenuItem();
			this.Timer = new System.Windows.Forms.Timer(this.components);
			this.StartbahnhoefeWeiterButton = new System.Windows.Forms.Button();
			this.WerkzeugleisteMenuItem = new System.Windows.Forms.MenuItem();
			this.DatenbankEinstellungenWeiterButton = new System.Windows.Forms.Button();
			this.MainMenu = new System.Windows.Forms.MainMenu();
			this.DateiMenuItem = new System.Windows.Forms.MenuItem();
			this.DatenbankEinstellungenTestButton = new System.Windows.Forms.Button();
			this.InitialCatalogTextBox = new System.Windows.Forms.TextBox();
			this.OptionenMenuItem = new System.Windows.Forms.MenuItem();
			this.BaumansichtMenuItem = new System.Windows.Forms.MenuItem();
			this.splitter1 = new System.Windows.Forms.Splitter();
			this.StatusleisteToolBarButton = new System.Windows.Forms.ToolBarButton();
			this.HilfeMenuItem = new System.Windows.Forms.MenuItem();
			this.DatenbankEinstellungenGroupBox1 = new System.Windows.Forms.GroupBox();
			this.StatusleisteMenuItem = new System.Windows.Forms.MenuItem();
			this.TourenoptionenWeiterButton = new System.Windows.Forms.Button();
			this.Werkzeugleiste = new System.Windows.Forms.ToolBar();
			this.StartbahnhoefePanel = new System.Windows.Forms.Panel();
			this.DatenbankEinstellungenLabel4 = new System.Windows.Forms.Label();
			this.DatenbankEinstellungenLabel3 = new System.Windows.Forms.Label();
			this.DatenbankEinstellungenLabel2 = new System.Windows.Forms.Label();
			this.DataSourceTextBox = new System.Windows.Forms.TextBox();
			this.DatenbankEinstellungenLabel0 = new System.Windows.Forms.Label();
			this.StatusBarPanel2 = new System.Windows.Forms.StatusBarPanel();
			this.QuellTabelleAnzeigenDataGrid = new System.Windows.Forms.DataGrid();
			this.UeberMenuItem = new System.Windows.Forms.MenuItem();
			this.AnsichtMenuItem = new System.Windows.Forms.MenuItem();
			this.TourenoptionenLabel0 = new System.Windows.Forms.Label();
			this.ExtrasMenuItem = new System.Windows.Forms.MenuItem();
			this.Statusleiste = new System.Windows.Forms.StatusBar();
			this.Baumansicht = new System.Windows.Forms.TreeView();
			this.DatenbankEinstellungenPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.StatusBarPanel1)).BeginInit();
			this.TourenoptionenPanel.SuspendLayout();
			this.DatenbankEinstellungenGroupBox1.SuspendLayout();
			this.StartbahnhoefePanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.StatusBarPanel2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.QuellTabelleAnzeigenDataGrid)).BeginInit();
			this.SuspendLayout();
			// 
			// DatenbankEinstellungenPanel
			// 
			this.DatenbankEinstellungenPanel.Controls.Add(this.DatenbankEinstellungenLabel0);
			this.DatenbankEinstellungenPanel.Controls.Add(this.DatenbankEinstellungenWeiterButton);
			this.DatenbankEinstellungenPanel.Controls.Add(this.DatenbankEinstellungenGroupBox1);
			this.DatenbankEinstellungenPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.DatenbankEinstellungenPanel.Location = new System.Drawing.Point(235, 28);
			this.DatenbankEinstellungenPanel.Name = "DatenbankEinstellungenPanel";
			this.DatenbankEinstellungenPanel.Size = new System.Drawing.Size(685, 467);
			this.DatenbankEinstellungenPanel.TabIndex = 6;
			// 
			// StationsListBox
			// 
			this.StationsListBox.Location = new System.Drawing.Point(16, 48);
			this.StationsListBox.Name = "StationsListBox";
			this.StationsListBox.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
			this.StationsListBox.Size = new System.Drawing.Size(440, 290);
			this.StationsListBox.Sorted = true;
			this.StationsListBox.TabIndex = 1;
			this.StationsListBox.SelectedIndexChanged += new System.EventHandler(this.StationsListBoxSelectedIndexChanged);
			// 
			// BaumansichtToolBarButton
			// 
			this.BaumansichtToolBarButton.ImageIndex = 0;
			this.BaumansichtToolBarButton.Pushed = true;
			this.BaumansichtToolBarButton.Style = System.Windows.Forms.ToolBarButtonStyle.ToggleButton;
			this.BaumansichtToolBarButton.ToolTipText = "Baumansicht";
			// 
			// StatusBarPanel1
			// 
			this.StatusBarPanel1.MinWidth = 400;
			this.StatusBarPanel1.Text = "Bereit";
			this.StatusBarPanel1.ToolTipText = "Statusleiste";
			this.StatusBarPanel1.Width = 400;
			// 
			// DatenbankEinstellungenStandardButton
			// 
			this.DatenbankEinstellungenStandardButton.Location = new System.Drawing.Point(240, 224);
			this.DatenbankEinstellungenStandardButton.Name = "DatenbankEinstellungenStandardButton";
			this.DatenbankEinstellungenStandardButton.Size = new System.Drawing.Size(168, 24);
			this.DatenbankEinstellungenStandardButton.TabIndex = 6;
			this.DatenbankEinstellungenStandardButton.Text = "Auf Standart zur�cksetzen";
			this.DatenbankEinstellungenStandardButton.Click += new System.EventHandler(this.DatenbankEinstellungenStandardButtonClick);
			// 
			// StartbahnhoefeLabel0
			// 
			this.StartbahnhoefeLabel0.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.StartbahnhoefeLabel0.Location = new System.Drawing.Point(16, 8);
			this.StartbahnhoefeLabel0.Name = "StartbahnhoefeLabel0";
			this.StartbahnhoefeLabel0.Size = new System.Drawing.Size(248, 24);
			this.StartbahnhoefeLabel0.TabIndex = 0;
			this.StartbahnhoefeLabel0.Text = "Schritt Zwei: Startbahnh�fe";
			// 
			// TourenoptionenPanel
			// 
			this.TourenoptionenPanel.Controls.Add(this.TourenoptionenWeiterButton);
			this.TourenoptionenPanel.Controls.Add(this.TourenoptionenLabel0);
			this.TourenoptionenPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.TourenoptionenPanel.Location = new System.Drawing.Point(235, 28);
			this.TourenoptionenPanel.Name = "TourenoptionenPanel";
			this.TourenoptionenPanel.Size = new System.Drawing.Size(685, 467);
			this.TourenoptionenPanel.TabIndex = 10;
			// 
			// ImageList
			// 
			this.ImageList.ImageSize = new System.Drawing.Size(16, 16);
			this.ImageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageList.ImageStream")));
			this.ImageList.TransparentColor = System.Drawing.Color.Transparent;
			// 
			// menuItem1
			// 
			this.menuItem1.Index = 1;
			this.menuItem1.Text = "Hilfe";
			this.menuItem1.Click += new System.EventHandler(this.MenuItem1Click);
			// 
			// BeendenMenuItem
			// 
			this.BeendenMenuItem.Index = 0;
			this.BeendenMenuItem.Text = "Beenden";
			this.BeendenMenuItem.Click += new System.EventHandler(this.BeendenMenuItemClick);
			// 
			// Timer
			// 
			this.Timer.Enabled = true;
			this.Timer.Interval = 1000;
			this.Timer.Tick += new System.EventHandler(this.TimerTick);
			// 
			// StartbahnhoefeWeiterButton
			// 
			this.StartbahnhoefeWeiterButton.Location = new System.Drawing.Point(336, 360);
			this.StartbahnhoefeWeiterButton.Name = "StartbahnhoefeWeiterButton";
			this.StartbahnhoefeWeiterButton.Size = new System.Drawing.Size(120, 24);
			this.StartbahnhoefeWeiterButton.TabIndex = 2;
			this.StartbahnhoefeWeiterButton.Text = "Weiter >>";
			this.StartbahnhoefeWeiterButton.Click += new System.EventHandler(this.StartbahnhoefeWeiterButtonClick);
			// 
			// WerkzeugleisteMenuItem
			// 
			this.WerkzeugleisteMenuItem.Checked = true;
			this.WerkzeugleisteMenuItem.Index = 0;
			this.WerkzeugleisteMenuItem.Text = "Werkzeugleiste";
			this.WerkzeugleisteMenuItem.Click += new System.EventHandler(this.WerkzeugleisteMenuItemClick);
			// 
			// DatenbankEinstellungenWeiterButton
			// 
			this.DatenbankEinstellungenWeiterButton.Location = new System.Drawing.Point(336, 360);
			this.DatenbankEinstellungenWeiterButton.Name = "DatenbankEinstellungenWeiterButton";
			this.DatenbankEinstellungenWeiterButton.Size = new System.Drawing.Size(120, 24);
			this.DatenbankEinstellungenWeiterButton.TabIndex = 5;
			this.DatenbankEinstellungenWeiterButton.Text = "Weiter >>";
			this.DatenbankEinstellungenWeiterButton.Click += new System.EventHandler(this.DatenbankEinstellungenWeiterButtonClick);
			// 
			// MainMenu
			// 
			this.MainMenu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
						this.DateiMenuItem,
						this.AnsichtMenuItem,
						this.ExtrasMenuItem,
						this.HilfeMenuItem});
			// 
			// DateiMenuItem
			// 
			this.DateiMenuItem.Index = 0;
			this.DateiMenuItem.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
						this.BeendenMenuItem});
			this.DateiMenuItem.Text = "Datei";
			// 
			// DatenbankEinstellungenTestButton
			// 
			this.DatenbankEinstellungenTestButton.Location = new System.Drawing.Point(48, 224);
			this.DatenbankEinstellungenTestButton.Name = "DatenbankEinstellungenTestButton";
			this.DatenbankEinstellungenTestButton.Size = new System.Drawing.Size(160, 24);
			this.DatenbankEinstellungenTestButton.TabIndex = 7;
			this.DatenbankEinstellungenTestButton.Text = "Verbindung testen";
			this.DatenbankEinstellungenTestButton.Click += new System.EventHandler(this.DatenbankEinstellungenTestButtonClick);
			// 
			// InitialCatalogTextBox
			// 
			this.InitialCatalogTextBox.Location = new System.Drawing.Point(128, 160);
			this.InitialCatalogTextBox.Name = "InitialCatalogTextBox";
			this.InitialCatalogTextBox.Size = new System.Drawing.Size(192, 20);
			this.InitialCatalogTextBox.TabIndex = 4;
			this.InitialCatalogTextBox.Text = "";
			this.InitialCatalogTextBox.TextChanged += new System.EventHandler(this.InitialCatalogTextBoxTextChanged);
			// 
			// OptionenMenuItem
			// 
			this.OptionenMenuItem.Index = 0;
			this.OptionenMenuItem.Text = "Optionen";
			this.OptionenMenuItem.Click += new System.EventHandler(this.OptionenMenuItemClick);
			// 
			// BaumansichtMenuItem
			// 
			this.BaumansichtMenuItem.Checked = true;
			this.BaumansichtMenuItem.Index = 1;
			this.BaumansichtMenuItem.Text = "Baumansicht";
			this.BaumansichtMenuItem.Click += new System.EventHandler(this.BaumansichtMenuItemClick);
			// 
			// splitter1
			// 
			this.splitter1.Location = new System.Drawing.Point(232, 28);
			this.splitter1.Name = "splitter1";
			this.splitter1.Size = new System.Drawing.Size(3, 467);
			this.splitter1.TabIndex = 7;
			this.splitter1.TabStop = false;
			// 
			// StatusleisteToolBarButton
			// 
			this.StatusleisteToolBarButton.ImageIndex = 1;
			this.StatusleisteToolBarButton.Pushed = true;
			this.StatusleisteToolBarButton.Style = System.Windows.Forms.ToolBarButtonStyle.ToggleButton;
			this.StatusleisteToolBarButton.ToolTipText = "Statusleiste";
			// 
			// HilfeMenuItem
			// 
			this.HilfeMenuItem.Index = 3;
			this.HilfeMenuItem.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
						this.UeberMenuItem,
						this.menuItem1});
			this.HilfeMenuItem.Text = "?";
			// 
			// DatenbankEinstellungenGroupBox1
			// 
			this.DatenbankEinstellungenGroupBox1.Controls.Add(this.DatenbankEinstellungenTestButton);
			this.DatenbankEinstellungenGroupBox1.Controls.Add(this.DatenbankEinstellungenStandardButton);
			this.DatenbankEinstellungenGroupBox1.Controls.Add(this.DataSourceTextBox);
			this.DatenbankEinstellungenGroupBox1.Controls.Add(this.InitialCatalogTextBox);
			this.DatenbankEinstellungenGroupBox1.Controls.Add(this.DatenbankEinstellungenLabel2);
			this.DatenbankEinstellungenGroupBox1.Controls.Add(this.DatenbankEinstellungenLabel3);
			this.DatenbankEinstellungenGroupBox1.Controls.Add(this.DatenbankEinstellungenLabel4);
			this.DatenbankEinstellungenGroupBox1.Location = new System.Drawing.Point(16, 40);
			this.DatenbankEinstellungenGroupBox1.Name = "DatenbankEinstellungenGroupBox1";
			this.DatenbankEinstellungenGroupBox1.Size = new System.Drawing.Size(440, 304);
			this.DatenbankEinstellungenGroupBox1.TabIndex = 4;
			this.DatenbankEinstellungenGroupBox1.TabStop = false;
			this.DatenbankEinstellungenGroupBox1.Text = "Datenquelle";
			// 
			// StatusleisteMenuItem
			// 
			this.StatusleisteMenuItem.Checked = true;
			this.StatusleisteMenuItem.Index = 2;
			this.StatusleisteMenuItem.Text = "Statusleiste";
			this.StatusleisteMenuItem.Click += new System.EventHandler(this.StatusleisteMenuItemClick);
			// 
			// TourenoptionenWeiterButton
			// 
			this.TourenoptionenWeiterButton.Location = new System.Drawing.Point(336, 360);
			this.TourenoptionenWeiterButton.Name = "TourenoptionenWeiterButton";
			this.TourenoptionenWeiterButton.Size = new System.Drawing.Size(120, 24);
			this.TourenoptionenWeiterButton.TabIndex = 1;
			this.TourenoptionenWeiterButton.Text = "Weiter >>";
			// 
			// Werkzeugleiste
			// 
			this.Werkzeugleiste.Buttons.AddRange(new System.Windows.Forms.ToolBarButton[] {
						this.BaumansichtToolBarButton,
						this.StatusleisteToolBarButton});
			this.Werkzeugleiste.DropDownArrows = true;
			this.Werkzeugleiste.ImageList = this.ImageList;
			this.Werkzeugleiste.Location = new System.Drawing.Point(0, 0);
			this.Werkzeugleiste.Name = "Werkzeugleiste";
			this.Werkzeugleiste.ShowToolTips = true;
			this.Werkzeugleiste.Size = new System.Drawing.Size(920, 28);
			this.Werkzeugleiste.TabIndex = 4;
			this.Werkzeugleiste.ButtonClick += new System.Windows.Forms.ToolBarButtonClickEventHandler(this.WerkzeugleisteButtonClick);
			// 
			// StartbahnhoefePanel
			// 
			this.StartbahnhoefePanel.Controls.Add(this.StartbahnhoefeWeiterButton);
			this.StartbahnhoefePanel.Controls.Add(this.StationsListBox);
			this.StartbahnhoefePanel.Controls.Add(this.StartbahnhoefeLabel0);
			this.StartbahnhoefePanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.StartbahnhoefePanel.Location = new System.Drawing.Point(235, 28);
			this.StartbahnhoefePanel.Name = "StartbahnhoefePanel";
			this.StartbahnhoefePanel.Size = new System.Drawing.Size(685, 467);
			this.StartbahnhoefePanel.TabIndex = 8;
			// 
			// DatenbankEinstellungenLabel4
			// 
			this.DatenbankEinstellungenLabel4.Location = new System.Drawing.Point(64, 160);
			this.DatenbankEinstellungenLabel4.Name = "DatenbankEinstellungenLabel4";
			this.DatenbankEinstellungenLabel4.Size = new System.Drawing.Size(64, 16);
			this.DatenbankEinstellungenLabel4.TabIndex = 5;
			this.DatenbankEinstellungenLabel4.Text = "Datenbank:";
			this.DatenbankEinstellungenLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// DatenbankEinstellungenLabel3
			// 
			this.DatenbankEinstellungenLabel3.Location = new System.Drawing.Point(64, 104);
			this.DatenbankEinstellungenLabel3.Name = "DatenbankEinstellungenLabel3";
			this.DatenbankEinstellungenLabel3.Size = new System.Drawing.Size(64, 16);
			this.DatenbankEinstellungenLabel3.TabIndex = 3;
			this.DatenbankEinstellungenLabel3.Text = "Host:";
			this.DatenbankEinstellungenLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// DatenbankEinstellungenLabel2
			// 
			this.DatenbankEinstellungenLabel2.Location = new System.Drawing.Point(16, 24);
			this.DatenbankEinstellungenLabel2.Name = "DatenbankEinstellungenLabel2";
			this.DatenbankEinstellungenLabel2.Size = new System.Drawing.Size(416, 48);
			this.DatenbankEinstellungenLabel2.TabIndex = 2;
			this.DatenbankEinstellungenLabel2.Text = "Geben Sie den Namen oder die IP des Datenbank-Rechners ein (z.B.: localhost) und " +
"den Namen der Datenbank.";
			// 
			// DataSourceTextBox
			// 
			this.DataSourceTextBox.Location = new System.Drawing.Point(128, 104);
			this.DataSourceTextBox.Name = "DataSourceTextBox";
			this.DataSourceTextBox.Size = new System.Drawing.Size(192, 20);
			this.DataSourceTextBox.TabIndex = 0;
			this.DataSourceTextBox.Text = "";
			this.DataSourceTextBox.TextChanged += new System.EventHandler(this.DataSourceTextBoxTextChanged);
			// 
			// DatenbankEinstellungenLabel0
			// 
			this.DatenbankEinstellungenLabel0.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.DatenbankEinstellungenLabel0.Location = new System.Drawing.Point(16, 8);
			this.DatenbankEinstellungenLabel0.Name = "DatenbankEinstellungenLabel0";
			this.DatenbankEinstellungenLabel0.Size = new System.Drawing.Size(400, 24);
			this.DatenbankEinstellungenLabel0.TabIndex = 0;
			this.DatenbankEinstellungenLabel0.Text = "Schritt Eins: Datenbank Einstellungen";
			// 
			// StatusBarPanel2
			// 
			this.StatusBarPanel2.MinWidth = 300;
			this.StatusBarPanel2.ToolTipText = "Datum/Uhrzeit";
			this.StatusBarPanel2.Width = 300;
			// 
			// QuellTabelleAnzeigenDataGrid
			// 
			this.QuellTabelleAnzeigenDataGrid.AlternatingBackColor = System.Drawing.Color.Lavender;
			this.QuellTabelleAnzeigenDataGrid.BackColor = System.Drawing.Color.WhiteSmoke;
			this.QuellTabelleAnzeigenDataGrid.BackgroundColor = System.Drawing.Color.LightGray;
			this.QuellTabelleAnzeigenDataGrid.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.QuellTabelleAnzeigenDataGrid.CaptionBackColor = System.Drawing.Color.LightSteelBlue;
			this.QuellTabelleAnzeigenDataGrid.CaptionFont = new System.Drawing.Font("Microsoft Sans Serif", 8F);
			this.QuellTabelleAnzeigenDataGrid.CaptionForeColor = System.Drawing.Color.MidnightBlue;
			this.QuellTabelleAnzeigenDataGrid.DataMember = "";
			this.QuellTabelleAnzeigenDataGrid.Dock = System.Windows.Forms.DockStyle.Fill;
			this.QuellTabelleAnzeigenDataGrid.FlatMode = true;
			this.QuellTabelleAnzeigenDataGrid.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
			this.QuellTabelleAnzeigenDataGrid.ForeColor = System.Drawing.Color.MidnightBlue;
			this.QuellTabelleAnzeigenDataGrid.GridLineColor = System.Drawing.Color.Gainsboro;
			this.QuellTabelleAnzeigenDataGrid.GridLineStyle = System.Windows.Forms.DataGridLineStyle.None;
			this.QuellTabelleAnzeigenDataGrid.HeaderBackColor = System.Drawing.Color.MidnightBlue;
			this.QuellTabelleAnzeigenDataGrid.HeaderFont = new System.Drawing.Font("Microsoft Sans Serif", 8F);
			this.QuellTabelleAnzeigenDataGrid.HeaderForeColor = System.Drawing.Color.WhiteSmoke;
			this.QuellTabelleAnzeigenDataGrid.LinkColor = System.Drawing.Color.Teal;
			this.QuellTabelleAnzeigenDataGrid.Location = new System.Drawing.Point(235, 28);
			this.QuellTabelleAnzeigenDataGrid.Name = "QuellTabelleAnzeigenDataGrid";
			this.QuellTabelleAnzeigenDataGrid.ParentRowsBackColor = System.Drawing.Color.Gainsboro;
			this.QuellTabelleAnzeigenDataGrid.ParentRowsForeColor = System.Drawing.Color.MidnightBlue;
			this.QuellTabelleAnzeigenDataGrid.SelectionBackColor = System.Drawing.Color.CadetBlue;
			this.QuellTabelleAnzeigenDataGrid.SelectionForeColor = System.Drawing.Color.WhiteSmoke;
			this.QuellTabelleAnzeigenDataGrid.Size = new System.Drawing.Size(685, 467);
			this.QuellTabelleAnzeigenDataGrid.TabIndex = 9;
			// 
			// UeberMenuItem
			// 
			this.UeberMenuItem.Index = 0;
			this.UeberMenuItem.Text = "�ber...";
			this.UeberMenuItem.Click += new System.EventHandler(this.UeberMenuItemClick);
			// 
			// AnsichtMenuItem
			// 
			this.AnsichtMenuItem.Index = 1;
			this.AnsichtMenuItem.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
						this.WerkzeugleisteMenuItem,
						this.BaumansichtMenuItem,
						this.StatusleisteMenuItem});
			this.AnsichtMenuItem.Text = "Ansicht";
			// 
			// TourenoptionenLabel0
			// 
			this.TourenoptionenLabel0.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.TourenoptionenLabel0.Location = new System.Drawing.Point(16, 8);
			this.TourenoptionenLabel0.Name = "TourenoptionenLabel0";
			this.TourenoptionenLabel0.Size = new System.Drawing.Size(208, 24);
			this.TourenoptionenLabel0.TabIndex = 0;
			this.TourenoptionenLabel0.Text = "Schritt Drei: Tourenoptionen";
			// 
			// ExtrasMenuItem
			// 
			this.ExtrasMenuItem.Index = 2;
			this.ExtrasMenuItem.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
						this.OptionenMenuItem});
			this.ExtrasMenuItem.Text = "Extras";
			// 
			// Statusleiste
			// 
			this.Statusleiste.Location = new System.Drawing.Point(0, 495);
			this.Statusleiste.Name = "Statusleiste";
			this.Statusleiste.Panels.AddRange(new System.Windows.Forms.StatusBarPanel[] {
						this.StatusBarPanel1,
						this.StatusBarPanel2});
			this.Statusleiste.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Statusleiste.ShowPanels = true;
			this.Statusleiste.Size = new System.Drawing.Size(920, 22);
			this.Statusleiste.TabIndex = 1;
			// 
			// Baumansicht
			// 
			this.Baumansicht.Dock = System.Windows.Forms.DockStyle.Left;
			this.Baumansicht.ImageList = this.ImageList;
			this.Baumansicht.Location = new System.Drawing.Point(0, 28);
			this.Baumansicht.Name = "Baumansicht";
			this.Baumansicht.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
						new System.Windows.Forms.TreeNode("Datenbank Einstellungen", new System.Windows.Forms.TreeNode[] {
									new System.Windows.Forms.TreeNode("Quell Tabelle anzeigen")}),
						new System.Windows.Forms.TreeNode("Startbahnh�fe"),
						new System.Windows.Forms.TreeNode("Tourenoptionen"),
						new System.Windows.Forms.TreeNode("Suche durchf�hren"),
						new System.Windows.Forms.TreeNode("Auswertung anzeigen")});
			this.Baumansicht.Size = new System.Drawing.Size(232, 467);
			this.Baumansicht.TabIndex = 5;
			this.Baumansicht.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.BaumansichtAfterSelect);
			// 
			// MainWindow
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(920, 517);
			this.Controls.Add(this.QuellTabelleAnzeigenDataGrid);
			this.Controls.Add(this.StartbahnhoefePanel);
			this.Controls.Add(this.TourenoptionenPanel);
			this.Controls.Add(this.DatenbankEinstellungenPanel);
			this.Controls.Add(this.splitter1);
			this.Controls.Add(this.Baumansicht);
			this.Controls.Add(this.Werkzeugleiste);
			this.Controls.Add(this.Statusleiste);
			this.Menu = this.MainMenu;
			this.Name = "MainWindow";
			this.Text = "MainWindow";
			this.Closed += new System.EventHandler(this.MainWindowClosed);
			this.DatenbankEinstellungenPanel.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.StatusBarPanel1)).EndInit();
			this.TourenoptionenPanel.ResumeLayout(false);
			this.DatenbankEinstellungenGroupBox1.ResumeLayout(false);
			this.StartbahnhoefePanel.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.StatusBarPanel2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.QuellTabelleAnzeigenDataGrid)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion
		
		[STAThread]
		public static void Main(string[] args)
		{
			Application.Run(new MainWindow());
		}
			
		void WerkzeugleisteMenuItemClick(object sender, System.EventArgs e)
		{
			if(WerkzeugleisteMenuItem.Checked)
			{
				WerkzeugleisteMenuItem.Checked = false;
				Werkzeugleiste.Visible = false;
			}
			else
			{
				WerkzeugleisteMenuItem.Checked = true;
				Werkzeugleiste.Visible = true;
			}
		}
		
		void UeberMenuItemClick(object sender, System.EventArgs e)
		{
			about.ShowDialog();
		}
		
		void BaumansichtMenuItemClick(object sender, System.EventArgs e)
		{
			if(BaumansichtMenuItem.Checked)
			{
				BaumansichtMenuItem.Checked = false;
				Baumansicht.Visible = false;
			}
			else
			{
				BaumansichtMenuItem.Checked = true;
				Baumansicht.Visible = true;
			}
		}
		
		void StatusleisteMenuItemClick(object sender, System.EventArgs e)
		{
			if(StatusleisteMenuItem.Checked)
			{
				StatusleisteMenuItem.Checked = false;
				Statusleiste.Visible = false;
			}
			else
			{
				StatusleisteMenuItem.Checked = true;
				Statusleiste.Visible = true;
			}
		}
		
		void WerkzeugleisteButtonClick(object sender, System.Windows.Forms.ToolBarButtonClickEventArgs e)
		{
			if(e.Button == BaumansichtToolBarButton)
			{
				if(BaumansichtToolBarButton.Pushed)
				{
					Baumansicht.Visible = true;
				}
				else
				{
					Baumansicht.Visible = false;
				}
			}
			else if(e.Button == StatusleisteToolBarButton)
			{
				if(StatusleisteToolBarButton.Pushed)
				{
					Statusleiste.Visible = true;
				}
				else
				{
					Statusleiste.Visible = false;
				}
			}
		}
		
		void OptionenMenuItemClick(object sender, System.EventArgs e)
		{
			optionen.ShowDialog();
		}
		
		void BaumansichtAfterSelect(object sender, System.Windows.Forms.TreeViewEventArgs e)
		{			
			switch(e.Node.Text)
			{
				case "Datenbank Einstellungen":
					{
						DatenbankEinstellungenPanel.BringToFront();break;
					}
				case "Quell Tabelle anzeigen":
					{
						if(status==0)
						{
							Properties.DataSource = DataSourceTextBox.Text;
							Properties.InitialCatalog = InitialCatalogTextBox.Text;
							
							StatusBarPanel1.Text = "Verbinde...";
							
							if(viwotour.Connect())
							{
								StatusBarPanel1.Text = "Vorbereiten...";
								viwotour.Prepare();
								viwotour.Load();
								viwotour.dataAdapter.Fill(QuellTabelleAnzeigenDataSet);
								QuellTabelleAnzeigenDataGrid.SetDataBinding(QuellTabelleAnzeigenDataSet, "table");
								StatusBarPanel1.Text = "Fertig Vorbereitet!";
								QuellTabelleAnzeigenDataGrid.BringToFront();
								status = 2;
							}
							else
							{
								StatusBarPanel1.Text = "Verbindung fehlgeschlagen! Datenbank Einstellungen �berpr�fen!";
								QuellTabelleAnzeigenDataSet.Clear();
								QuellTabelleAnzeigenDataGrid.BringToFront();
							}
						}
						else if(status==1)
						{
							StatusBarPanel1.Text = "Vorbereiten...";
							viwotour.Prepare();
							viwotour.Load();
							viwotour.dataAdapter.Fill(QuellTabelleAnzeigenDataSet);
							QuellTabelleAnzeigenDataGrid.SetDataBinding(QuellTabelleAnzeigenDataSet, "table");
							StatusBarPanel1.Text = "Fertig Vorbereitet!";
							QuellTabelleAnzeigenDataGrid.BringToFront();
							status = 2;
						}
						else if(status>=2)
						{
							QuellTabelleAnzeigenDataGrid.BringToFront();
						}
						break;
					}
				case "Startbahnh�fe":
					{
						if(status==0)
						{
							Properties.DataSource = DataSourceTextBox.Text;
							Properties.InitialCatalog = InitialCatalogTextBox.Text;
							
							StatusBarPanel1.Text = "Verbinde...";
							
							if(viwotour.Connect())
							{
								StatusBarPanel1.Text = "Vorbereiten...";
								viwotour.Prepare();
								viwotour.Load();
								viwotour.dataAdapter.Fill(QuellTabelleAnzeigenDataSet);
								QuellTabelleAnzeigenDataGrid.SetDataBinding(QuellTabelleAnzeigenDataSet, "table");
								StatusBarPanel1.Text = "Fertig Vorbereitet!";
								
								StationsListBox.Items.Clear();
								foreach(string station in viwotour.courseBook.get_STATION_Of_Rows())
								{
									StationsListBox.Items.Add(station);
								}
								
								SelectStations();
								
								StartbahnhoefePanel.BringToFront();
								status = 3;
							}
							else
							{
								StatusBarPanel1.Text = "Verbindung fehlgeschlagen! Datenbank Einstellungen �berpr�fen!";
								QuellTabelleAnzeigenDataSet.Clear();
								StationsListBox.Items.Clear();
								
								StartbahnhoefePanel.BringToFront();
							}
						}
						else if(status==1)
						{
							StatusBarPanel1.Text = "Vorbereiten...";
							viwotour.Prepare();
							viwotour.Load();
							viwotour.dataAdapter.Fill(QuellTabelleAnzeigenDataSet);
							QuellTabelleAnzeigenDataGrid.SetDataBinding(QuellTabelleAnzeigenDataSet, "table");
							StatusBarPanel1.Text = "Fertig Vorbereitet!";
							
							StationsListBox.Items.Clear();
							foreach(string station in viwotour.courseBook.get_STATION_Of_Rows())
							{
								StationsListBox.Items.Add(station);
							}
								
							SelectStations();
								
							StartbahnhoefePanel.BringToFront();
							status = 3;
						}
						else if(status==2)
						{
							StationsListBox.Items.Clear();
							foreach(string station in viwotour.courseBook.get_STATION_Of_Rows())
							{
								StationsListBox.Items.Add(station);
							}
							
							SelectStations();
								
							StartbahnhoefePanel.BringToFront();
							status = 3;
						}
						else if(status>=3)
						{
							StartbahnhoefePanel.BringToFront();
						}
						break;
					}
				case "Tourenoptionen":
					{
						TourenoptionenPanel.BringToFront();
						break;
					}
			}
		}
		
		void DatenbankEinstellungenStandardButtonClick(object sender, System.EventArgs e)
		{
			DataSourceTextBox.Text = "localhost";
			InitialCatalogTextBox.Text = "viwotour";
		}
		
		void DatenbankEinstellungenWeiterButtonClick(object sender, System.EventArgs e)
		{
			StartbahnhoefePanel.BringToFront();
			
			SelectNode("Startbahnh�fe");
		}
		
		void SelectNode(string name)
		{
			foreach(System.Windows.Forms.TreeNode node in Baumansicht.Nodes)
			{
				if(node.Text == name){Baumansicht.SelectedNode = node;break;}
			}
		}
		
		void BeendenMenuItemClick(object sender, System.EventArgs e)
		{
			if(MessageBox.Show("Viwotour wirklich beenden?", "Beenden?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
			{
				Application.Exit();
			}
		}
		
		void DatenbankEinstellungenTestButtonClick(object sender, System.EventArgs e)
		{
			Properties.DataSource = DataSourceTextBox.Text;
			Properties.InitialCatalog = InitialCatalogTextBox.Text;
			
			if(status!=0)
			{
				StatusBarPanel1.Text = "Trenne Verbindung...";
				viwotour.Disconnect();
				StatusBarPanel1.Text = "Verbindung erfolgreich getrennt!";
			}
			
			StatusBarPanel1.Text = "Verbinde...";
			
			if(viwotour.Connect())
			{
				StatusBarPanel1.Text = "Verbindung zu "+InitialCatalogTextBox.Text+" auf "+DataSourceTextBox.Text+" erfolgreich!";
				status=1;
			}
			else
			{
				StatusBarPanel1.Text = "Verbindung fehlgeschlagen! Datenbank Einstellungen �berpr�fen!";
				status=0;
			}
		}
		
		void TimerTick(object sender, System.EventArgs e)
		{
			StatusBarPanel2.Text = System.Convert.ToString(status);
		}
		
		void InitialCatalogTextBoxTextChanged(object sender, System.EventArgs e)
		{
			Properties.InitialCatalog = InitialCatalogTextBox.Text;
			status = 0;
		}
		
		void InitViwoTour()
		{
			DataSourceTextBox.Text = Properties.DataSource;
			InitialCatalogTextBox.Text = Properties.InitialCatalog;
		}
		
		void MainWindowClosed(object sender, System.EventArgs e)
		{
			registrywork.setKey("DataSource", Properties.DataSource);
			registrywork.setKey("InitialCatalog", Properties.InitialCatalog);
			registrywork.setKey("StationsList", Properties.StationsList);
		}
		
		void DataSourceTextBoxTextChanged(object sender, System.EventArgs e)
		{
			Properties.DataSource = DataSourceTextBox.Text;
			status = 0;
		}
		
		void StationsListBoxSelectedIndexChanged(object sender, System.EventArgs e)
		{
			Properties.StationsList = "";
			
			for(int i=0; i < StationsListBox.SelectedItems.Count; i++)
			{
				Properties.StationsList += StationsListBox.SelectedItems[i].ToString()+"|";
			}
		}
		
		void SelectStations()
		{
			string[] tokens = Properties.StationsList.Split(new char[]{'|'});
			
			for(int i=0; i<(StationsListBox.Items.Count-1); i++)
			{
				for(int j=0; j<tokens.Length; j++)
				{
					if(StationsListBox.Items[i].ToString()==tokens[j])
					{
						StationsListBox.SetSelected(i, true);
					}
				}
			}
		}
		
		void StartbahnhoefeWeiterButtonClick(object sender, System.EventArgs e)
		{
			TourenoptionenPanel.BringToFront();
			
			SelectNode("Startbahnh�fe");
		}
		
		void MenuItem1Click(object sender, System.EventArgs e)
		{
		}
		
	}
}
