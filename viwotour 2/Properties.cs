/*
 * User: aputze
 * Date: 28.10.2004
 * Time: 16:41
 */

using System;

namespace ViwoTour {
 	public class Properties {
		public static string DataSource;
		public static string InitialCatalog;
		
		public static string StationsList;
		
		public static TimeSpan MinWaiting=new TimeSpan(0,5,0);
		public static TimeSpan MaxWaiting=new TimeSpan(0,15,0);
		
		public static TimeSpan MaxTouring=new TimeSpan(10,0,0);
		
		public static int Scope=10000;
 	}
}
