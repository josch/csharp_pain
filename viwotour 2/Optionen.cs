/*
 * Created by SharpDevelop.
 * User: windows
 * Date: 01.12.2004
 * Time: 14:55
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */

using System;
using System.Drawing;
using System.Windows.Forms;

namespace ViwoTour
{
	/// <summary>
	/// Description of Optionen.
	/// </summary>
	public class Optionen : System.Windows.Forms.Form
	{
		private System.Windows.Forms.CheckBox EinstellungenSpeichernCheckBox;
		private System.Windows.Forms.Button OKButton;
		private System.Windows.Forms.Button AbbrechenButton;
		public Optionen()
		{
			InitializeComponent();
		}
		
		#region Windows Forms Designer generated code
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent() {
			this.AbbrechenButton = new System.Windows.Forms.Button();
			this.OKButton = new System.Windows.Forms.Button();
			this.EinstellungenSpeichernCheckBox = new System.Windows.Forms.CheckBox();
			this.SuspendLayout();
			// 
			// AbbrechenButton
			// 
			this.AbbrechenButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.AbbrechenButton.Location = new System.Drawing.Point(224, 192);
			this.AbbrechenButton.Name = "AbbrechenButton";
			this.AbbrechenButton.Size = new System.Drawing.Size(88, 24);
			this.AbbrechenButton.TabIndex = 2;
			this.AbbrechenButton.Text = "Abbrechen";
			// 
			// OKButton
			// 
			this.OKButton.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.OKButton.Location = new System.Drawing.Point(120, 192);
			this.OKButton.Name = "OKButton";
			this.OKButton.Size = new System.Drawing.Size(88, 24);
			this.OKButton.TabIndex = 1;
			this.OKButton.Text = "OK";
			// 
			// EinstellungenSpeichernCheckBox
			// 
			this.EinstellungenSpeichernCheckBox.Location = new System.Drawing.Point(24, 32);
			this.EinstellungenSpeichernCheckBox.Name = "EinstellungenSpeichernCheckBox";
			this.EinstellungenSpeichernCheckBox.Size = new System.Drawing.Size(264, 40);
			this.EinstellungenSpeichernCheckBox.TabIndex = 0;
			this.EinstellungenSpeichernCheckBox.Text = "Einstellungen nach dem Beenden speichern(noch nicht implementiert)";
			// 
			// Optionen
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
			this.ClientSize = new System.Drawing.Size(328, 229);
			this.Controls.Add(this.AbbrechenButton);
			this.Controls.Add(this.OKButton);
			this.Controls.Add(this.EinstellungenSpeichernCheckBox);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "Optionen";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Optionen";
			this.ResumeLayout(false);
		}
		#endregion
	}
}
