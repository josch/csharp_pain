/*
 * User: aputze
 * Date: 12.11.2004
 * Time: 23:43
 */

using System;
using System.Collections;

using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;

namespace ViwoTour {
	public delegate void ProgressHandler(int Progress);
	
	public class ViwoTour {
		public CourseBook courseBook;
		public CoursePlan coursePlan;
		public TourPlan tourPlan;
		
		protected SqlConnection connection;
		
		public SqlDataAdapter dataAdapter;
		protected SqlDataAdapter dataAdapter1;
		protected SqlDataAdapter dataAdapter2;
		
		protected SqlCommand command;
		protected SqlCommand command1;
		
		protected bool[,] A;
		
		public ArrayList S;
		
		public ArrayList T_search;
		public ArrayList T_select;
		
		public ViwoTour() {
			courseBook=new CourseBook();
			coursePlan=new CoursePlan();
			tourPlan=new TourPlan();
			
			S=new ArrayList();
			
			T_search=new ArrayList();
			T_select=new ArrayList();
		}
		
		public bool Connect() {
			try
			{
				connection=new SqlConnection("Data Source="+Properties.DataSource+";"+
			                             "Integrated Security=true;"+
			                             "Initial Catalog="+Properties.InitialCatalog+";");
				connection.Open();
				return true;
			}
			catch
			{
				return false;
			}
		}
		public void Disconnect() {
			connection.Close();
		}
		
		public void Prepare() {
			dataAdapter=new SqlDataAdapter();
			dataAdapter.SelectCommand=new SqlCommand("SELECT "+
			                                         "ID,"+
			                                         "DEPARTURE,DEPARTURE_STATION,"+
			                                         "ARRIVAL,ARRIVAL_STATION," +
			                                         "MON,TUE,WED,THU,FRI,SAT,SUN"+
			                                         " FROM COURSE_BOOK",connection);
			
			dataAdapter1=new SqlDataAdapter();
			dataAdapter1.InsertCommand=new SqlCommand("INSERT INTO COURSE_PLAN "+
			                                          "(ID,"+
			                                          "DEPARTURE,DEPARTURE_STATION,"+
			                                          "ARRIVAL,ARRIVAL_STATION)"+
			                                          " VALUES "+
			                                          "(@0,@1,@2,@3,@4)",connection);
			
			dataAdapter1.InsertCommand.Parameters.Add("@0",SqlDbType.Int,4,"ID");
			dataAdapter1.InsertCommand.Parameters.Add("@1",SqlDbType.DateTime,8,"DEPARTURE");
			dataAdapter1.InsertCommand.Parameters.Add("@2",SqlDbType.NVarChar,50,"DEPARTURE_STATION");
			dataAdapter1.InsertCommand.Parameters.Add("@3",SqlDbType.DateTime,8,"ARRIVAL");
			dataAdapter1.InsertCommand.Parameters.Add("@4",SqlDbType.NVarChar,50,"ARRIVAL_STATION");
			
			dataAdapter2=new SqlDataAdapter();
			dataAdapter2.InsertCommand=new SqlCommand("INSERT INTO TOUR_PLAN "+
			                                          "(ID,ID1,"+
			                                          "DEPARTURE,DEPARTURE_STATION,"+
			                                          "ARRIVAL,ARRIVAL_STATION)"+
			                                          " VALUES "+
			                                          "(@0,@1,@2,@3,@4,@5)",connection);
			
			dataAdapter2.InsertCommand.Parameters.Add("@0",SqlDbType.Int,4,"ID");
			dataAdapter2.InsertCommand.Parameters.Add("@1",SqlDbType.Int,4,"ID1");
			dataAdapter2.InsertCommand.Parameters.Add("@2",SqlDbType.DateTime,8,"DEPARTURE");
			dataAdapter2.InsertCommand.Parameters.Add("@3",SqlDbType.NVarChar,50,"DEPARTURE_STATION");
			dataAdapter2.InsertCommand.Parameters.Add("@4",SqlDbType.DateTime,8,"ARRIVAL");
			dataAdapter2.InsertCommand.Parameters.Add("@5",SqlDbType.NVarChar,50,"ARRIVAL_STATION");
			
			command=new SqlCommand("DELETE FROM COURSE_PLAN",connection);
			command1=new SqlCommand("DELETE FROM TOUR_PLAN",connection);
		}
		
		public void Load() {
			courseBook.Rows.Clear();
			coursePlan.Rows.Clear();
			
			dataAdapter.Fill(courseBook);
			
			string[] dayName=new string[] {"MON","TUE","WED","THU","FRI","SAT","SUN"};
			TimeSpan[] dayOffset=new TimeSpan[] {new TimeSpan(0,0,0,0),
				new TimeSpan(1,0,0,0),
				new TimeSpan(2,0,0,0),
				new TimeSpan(3,0,0,0),
				new TimeSpan(4,0,0,0),
				new TimeSpan(5,0,0,0),
				new TimeSpan(6,0,0,0)};
			TimeSpan[] dayOffset1=new TimeSpan[] {new TimeSpan(1,0,0,0),
				new TimeSpan(2,0,0,0),
				new TimeSpan(3,0,0,0),
				new TimeSpan(4,0,0,0),
				new TimeSpan(5,0,0,0),
				new TimeSpan(6,0,0,0),
				new TimeSpan(7,0,0,0)};
			
			foreach(DataRow row in courseBook.Rows) {
				for(int day=0;day<7;day++) {
					if((int)row[dayName[day]]==1) {
						coursePlan.Rows.Add(new object[] {row["ID"],
						                    	((DateTime)row["DEPARTURE"]).Add(dayOffset[day]),row["DEPARTURE_STATION"],
						                    	(DateTime)row["DEPARTURE"]<(DateTime)row["ARRIVAL"] ?
						                    		((DateTime)row["ARRIVAL"]).Add(dayOffset[day]) :
						                    		((DateTime)row["ARRIVAL"]).Add(dayOffset1[day]),row["ARRIVAL_STATION"]});
					}
				}
			}
			
			courseBook.create_ID_Of_Rows();
			courseBook.create_STATION_Of_Rows();
			
			coursePlan.create_Rows_By_DEPARTURE_STATION();
			coursePlan.create_Rows_By_ARRIVAL_STATION();
		}
		public void Store() {
			foreach(ArrayList t_select in T_select) {
				foreach(int r in t_select) {
					DataRow row=coursePlan.Rows[r];
					
					tourPlan.Rows.Add(new object[] {T_select.IndexOf(t_select),row["ID"],
					                  	row["DEPARTURE"],row["DEPARTURE_STATION"],
					                  	row["ARRIVAL"],row["ARRIVAL_STATION"]});
				}
			}
			
			command.ExecuteNonQuery();
			command1.ExecuteNonQuery();
			
			dataAdapter1.Update(coursePlan);
			dataAdapter2.Update(tourPlan);
		}
		
		public void create_A() {
			A=new bool[coursePlan.Rows.Count,coursePlan.Rows.Count];
			
			for(int i=0;i<coursePlan.Rows.Count;i++) {
				for(int j=0;j<coursePlan.Rows.Count;j++) {
					DataRow row1=coursePlan.Rows[i];
					DataRow row2=coursePlan.Rows[j];
					
					A[i,j]=(string)row2[2]==(string)row1[4] &&
						((DateTime)row2[1]-(DateTime)row1[3])>=Properties.MinWaiting &&
						((DateTime)row2[1]-(DateTime)row1[3])<=Properties.MaxWaiting;
				}
			}
		}
		
		public event ProgressHandler search_TProgress;
		public void search_T() {
			if(search_TProgress!=null) { search_TProgress(0); }
			
			T_search.Clear();
			
			foreach(string s in S) {
				ArrayList R1=coursePlan.get_Rows_By_DEPARTURE_STATION(s);
				ArrayList R2=coursePlan.get_Rows_By_ARRIVAL_STATION(s);
				
				if(R1==null || R2==null) { continue; }
				
				ArrayList T=new ArrayList();
				ArrayList T_prev;
				ArrayList T_next;
				
				T_prev=new ArrayList();
				foreach(int r1 in R1) {
					ArrayList t1=new ArrayList();
					t1.Add(r1);
					
					T_prev.Add(t1);
				}
				
				while(T_prev.Count>0) {
					T_next=new ArrayList();
					
					foreach(ArrayList t_prev in T_prev) {
						int r1=(int)t_prev[0];
						
						int r_prev=(int)t_prev[t_prev.Count-1];
						for(int r_next=0;r_next<coursePlan.Rows.Count;r_next++) {
							if(A[r_prev,r_next]) {
								if(((DateTime)coursePlan.Rows[r_next][3]-(DateTime)coursePlan.Rows[r1][1])<=Properties.MaxTouring) {
									ArrayList t_next=(ArrayList)t_prev.Clone();
									t_next.Add(r_next);
									
									T_next.Add(t_next);
									
									if(R2.Contains(r_next)) { T.Add(t_next); }
								}
							}
						}
					}
					
					T_prev=T_next.Count<Properties.Scope ? T_next : T_next.GetRange(0,Properties.Scope);
				}
				
				T_search.AddRange(T);
				
				if(search_TProgress!=null) { search_TProgress((100*(S.IndexOf(s)+1))/S.Count); }
			}
			
			if(search_TProgress!=null) { search_TProgress(100); }
		}
		
		public event ProgressHandler select_TProgress;
		public void select_T() {
			if(select_TProgress!=null) { select_TProgress(0); }
			
			T_select.Clear();
			
			ArrayList IDsToDo=(ArrayList)courseBook.get_ID_Of_Rows().Clone();;
			ArrayList IDsDone=new ArrayList();
			
			while(IDsToDo.Count>0) {
				int ID=(int)IDsToDo[0];
				
				ArrayList T_filter=new ArrayList();
				foreach(ArrayList t_search in T_search) {
					foreach(int r in t_search) {
						DataRow row=coursePlan.Rows[r];
						
						if(ID==(int)row[0]) {
							T_filter.Add(t_search);
							
							break;
						}
					}
				}
				if(T_filter.Count==0) {
					IDsToDo.Remove(ID);
					if(!IDsDone.Contains(ID)) { IDsDone.Add(ID); }
					
					continue;
				}
				
				double[] P=new double[T_filter.Count];
				for(int i=0;i<T_filter.Count;i++) {
					ArrayList t_filter=(ArrayList)T_filter[i];
					
					int A=0;
					long B=0;
					
					for(int j=0;j<t_filter.Count;j++) {
						DataRow row=coursePlan.Rows[(int)t_filter[j]];
						
						if(IDsDone.Contains(row[0])) {
							B+=((DateTime)row[3]-(DateTime)row[1]).Ticks;
						} else {
							A++;
						}
					}
					for(int j=1;j<t_filter.Count;j++) {
						DataRow row1=coursePlan.Rows[(int)t_filter[j-1]];
						DataRow row2=coursePlan.Rows[(int)t_filter[j]];
						
						B+=((DateTime)row2[1]-(DateTime)row1[3]).Ticks;
					}
					
					P[i]=(double)(A*A)/(double)B;
				}
				
				int k=0;
				for(int l=0;l<T_filter.Count;l++) {
					if(P[l]>P[k]) { k=l; }
				}
				
				foreach(int r in (ArrayList)T_filter[k]) {
					DataRow row=coursePlan.Rows[r];
					
					IDsToDo.Remove(row[0]);
					if(!IDsDone.Contains(row[0])) { IDsDone.Add(row[0]); }
				}
				
				T_select.Add(T_filter[k]);
				
				if(select_TProgress!=null) { select_TProgress((100*IDsDone.Count)/(IDsToDo.Count+IDsDone.Count)); }
			}
			
			if(select_TProgress!=null) { select_TProgress(100); }
		}
	}
}
