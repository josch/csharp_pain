/*
 * User: aputze
 * Date: 28.10.2004
 * Time: 15:50
 */

using System;
using System.Collections;

namespace ViwoTour {
	public class TourData {
		public static int lastId=0;
		public int Id;
		
		public int ProductiveIds;
		public int UnproductiveIds;
		
		public TimeSpan ProductiveDuration;
		public TimeSpan UnproductiveDuration;
		
		public int MaxWorkforce;
		
		public int ProductiveWorkforceRide;
		public int UnproductiveWorkforceRide;
		
		public decimal P;
		
		public TourData() {
			Id=0;
			
			ProductiveIds=0;
			UnproductiveIds=0;
			
			ProductiveDuration=new TimeSpan();
			UnproductiveDuration=new TimeSpan();
			
			P=0.0m;
		}
		
		public void GenId() { Id=++lastId; }
		
		public object Clone() {
			TourData tourData=new TourData();
			tourData.Id=Id;
			
			tourData.ProductiveIds=ProductiveIds;
			tourData.UnproductiveIds=UnproductiveIds;
			
			tourData.ProductiveDuration=ProductiveDuration;
			tourData.UnproductiveDuration=UnproductiveDuration;
			
			return tourData;
		}
	}
	
	public class Tour {
		public TourData tourData;
		
		public ArrayList V;
		public ArrayList E;
		
		public Tour() {
			tourData=new TourData();
			
			V=new ArrayList();
			E=new ArrayList();
		}
		public Tour(TourData tourData) {
			this.tourData=tourData;
			
			V=new ArrayList();
			E=new ArrayList();
		}
		
		public object Clone() {
			Tour tour=new Tour();
			tour.tourData=(TourData)tourData.Clone();
			
			tour.V=(ArrayList)V.Clone();
			tour.E=(ArrayList)E.Clone();
			
			return tour;
		}
	}
}
