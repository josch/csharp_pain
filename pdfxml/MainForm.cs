/*
 * Created by SharpDevelop.
 * User: Johannes
 * Date: 25.05.2005
 * Time: 15:47
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Drawing;
using System.Windows.Forms;
using Acrobat;

namespace pdfxml
{
	/// <summary>
	/// Description of MainForm.
	/// </summary>
	public class MainForm : System.Windows.Forms.Form
	{
		public MainForm()
		{
			AcroPDDoc acrodoc=new AcroPDDoc();
			if(acrodoc.Open(@"E:\Quelltexte\pdfxml\bin\Debug\file.pdf"))
			{
				MessageBox.Show("Öffnen geht");
			}
			else
			{
				MessageBox.Show("Öffnen geht net");
			}
			if(acrodoc.Save(1, @"E:\Quelltexte\pdfxml\bin\Debug\new.xml"))
			{
				MessageBox.Show("Speichern geht");
			}
			else
			{
				MessageBox.Show("Speichern geht net");
			}
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			
			//
			// TODO: Add constructor code after the InitializeComponent() call.
			//
		}
		
		[STAThread]
		public static void Main(string[] args)
		{
			Application.Run(new MainForm());
		}
		
		#region Windows Forms Designer generated code
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			// 
			// Form1
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(292, 266);
			this.Text = "MainForm";
			this.Name = "MainForm";
		}
		#endregion
	}
}
